ALTER TABLE niva.geotagphotos ADD attached_photo bytea NULL;
ALTER TABLE niva.geotagphotos ADD decision_light int4 NULL;
COMMENT ON COLUMN niva.geotagphotos.decision_light IS '1: green, 2: yellow, 3:red';
ALTER TABLE niva.geotagphotos ADD file_path varchar(60) NULL;
ALTER TABLE niva.parc_activities ADD decision_light int4 NULL;
