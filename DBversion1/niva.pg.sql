-- DROP SCHEMA niva;

CREATE SCHEMA niva;

create TABLESPACE niva_DATA LOCATION '/niva_DATA';

create TABLESPACE niva_blob LOCATION '/niva_blob';

--------------------------------------------------

drop sequence if exists niva.niva_SQ cascade;
drop table if exists niva.agency cascade;


drop table if exists niva.declarations cascade;


drop table if exists niva.classifier cascade;


drop table if exists niva.file_template cascade;


drop table if exists niva.predef_col cascade;


drop table if exists niva.template_columns cascade;


drop table if exists niva.data_file cascade;


drop table if exists niva.cover_type cascade;


drop table if exists niva.super_class cascade;


drop table if exists niva.super_class_detail cascade;


drop table if exists niva.cultivation cascade;


drop table if exists niva.classification cascade;


drop table if exists niva.statistics cascade;


drop table if exists niva.parcel_class cascade;


drop table if exists niva.parcel cascade;


drop table if exists niva.geotagphotos cascade;


drop table if exists niva.parc_activities cascade;


drop table if exists niva.activity cascade;


drop table if exists niva.ec_group cascade;


drop table if exists niva.ec_cult cascade;


drop table if exists niva.ec_cult_detail cascade;


drop table if exists niva.eccu_activities cascade;


drop table if exists niva.decision_making cascade;


drop table if exists niva.parcel_decision cascade;


drop table if exists niva.documents cascade;



CREATE SEQUENCE niva.niva_SQ
    INCREMENT BY 1
    START WITH 1
    MAXVALUE 999999999999
    MINVALUE 1;

CREATE TABLE niva.agency
(
    agen_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    name varchar(60)   NOT NULL,
    country varchar(60)   ,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.agency IS 'State paying Agency';

COMMENT ON COLUMN niva.agency.agen_id IS 'primary key';
COMMENT ON COLUMN niva.agency.name IS 'name of agency';
COMMENT ON COLUMN niva.agency.country IS 'country of agency';
CREATE UNIQUE INDEX agen_PK_I
    ON niva.agency (agen_id)
    TABLESPACE pg_default;

ALTER TABLE niva.agency 
    ADD CONSTRAINT agen_PK  PRIMARY KEY 
    USING INDEX agen_PK_I;

CREATE UNIQUE INDEX agenname__UI
    ON niva.agency (name)
    TABLESPACE pg_default;

ALTER TABLE niva.agency 
    ADD CONSTRAINT agenname_uk  UNIQUE 
    USING INDEX agenname__UI;

CREATE TABLE niva.declarations
(
    decl_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    agen_id int4   NOT NULL,
    name varchar(60)   ,
    year int4   ,
    date_time timestamptz   NOT NULL,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.declarations IS 'declarations';

COMMENT ON COLUMN niva.declarations.decl_id IS 'primary key';
COMMENT ON COLUMN niva.declarations.agen_id IS 'fk to agency';
COMMENT ON COLUMN niva.declarations.name IS 'name of declaration';
COMMENT ON COLUMN niva.declarations.year IS 'year of declaration';
COMMENT ON COLUMN niva.declarations.date_time IS 'timestamp';
CREATE UNIQUE INDEX decl_PK_I
    ON niva.declarations (decl_id)
    TABLESPACE pg_default;

ALTER TABLE niva.declarations 
    ADD CONSTRAINT decl_PK  PRIMARY KEY 
    USING INDEX decl_PK_I;

CREATE  INDEX decl_agen_id_FI
    ON niva.declarations (agen_id)
    TABLESPACE pg_default;

CREATE TABLE niva.classifier
(
    clfr_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    fite_id int4   ,
    name varchar(60)   NOT NULL,
    description varchar(60)   ,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.classifier IS 'classifier';

COMMENT ON COLUMN niva.classifier.clfr_id IS 'primary key';
COMMENT ON COLUMN niva.classifier.fite_id IS 'fk to file template';
COMMENT ON COLUMN niva.classifier.name IS 'classifier name';
COMMENT ON COLUMN niva.classifier.description IS 'classifier description';
CREATE UNIQUE INDEX clfr_PK_I
    ON niva.classifier (clfr_id)
    TABLESPACE pg_default;

ALTER TABLE niva.classifier 
    ADD CONSTRAINT clfr_PK  PRIMARY KEY 
    USING INDEX clfr_PK_I;

CREATE  INDEX clfr_fite_id_FI
    ON niva.classifier (fite_id)
    TABLESPACE pg_default;

CREATE TABLE niva.file_template
(
    fite_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    name varchar(60)   NOT NULL,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.file_template IS 'the template  of the file of input data';

COMMENT ON COLUMN niva.file_template.fite_id IS 'primary key';
COMMENT ON COLUMN niva.file_template.name IS 'template general name';
CREATE UNIQUE INDEX fite_PK_I
    ON niva.file_template (fite_id)
    TABLESPACE pg_default;

ALTER TABLE niva.file_template 
    ADD CONSTRAINT fite_PK  PRIMARY KEY 
    USING INDEX fite_PK_I;

CREATE TABLE niva.predef_col
(
    prco_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    column_name varchar(60)   NOT NULL,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.predef_col IS 'predefine columns';

COMMENT ON COLUMN niva.predef_col.prco_id IS 'primary key';
COMMENT ON COLUMN niva.predef_col.column_name IS 'dss column name - internal system name';
CREATE UNIQUE INDEX prco_PK_I
    ON niva.predef_col (prco_id)
    TABLESPACE pg_default;

ALTER TABLE niva.predef_col 
    ADD CONSTRAINT prco_PK  PRIMARY KEY 
    USING INDEX prco_PK_I;

CREATE TABLE niva.template_columns
(
    teco_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    fite_id int4   NOT NULL,
    prco_id int4   NOT NULL,
    clfier_name varchar(60)   NOT NULL,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.template_columns IS 'the columns of the template file';

COMMENT ON COLUMN niva.template_columns.teco_id IS 'primary key';
COMMENT ON COLUMN niva.template_columns.fite_id IS 'fk to file template';
COMMENT ON COLUMN niva.template_columns.prco_id IS 'fk to file predefine columns';
COMMENT ON COLUMN niva.template_columns.clfier_name IS 'classifier name -inputs file name';
CREATE UNIQUE INDEX teco_PK_I
    ON niva.template_columns (teco_id)
    TABLESPACE pg_default;

ALTER TABLE niva.template_columns 
    ADD CONSTRAINT teco_PK  PRIMARY KEY 
    USING INDEX teco_PK_I;

CREATE  INDEX teco_fite_id_FI
    ON niva.template_columns (fite_id)
    TABLESPACE pg_default;

CREATE  INDEX teco_prco_id_FI
    ON niva.template_columns (prco_id)
    TABLESPACE pg_default;

CREATE TABLE niva.data_file
(
    dafi_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    clas_id int4   NOT NULL,
    fite_id int4   NOT NULL,
    filename varchar(60)   NOT NULL,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.data_file IS 'the input data file - classification or other';

COMMENT ON COLUMN niva.data_file.dafi_id IS 'primary key';
COMMENT ON COLUMN niva.data_file.clas_id IS 'fk to classification';
COMMENT ON COLUMN niva.data_file.fite_id IS 'fk to file template';
COMMENT ON COLUMN niva.data_file.filename IS 'filename';
CREATE UNIQUE INDEX dafi_PK_I
    ON niva.data_file (dafi_id)
    TABLESPACE pg_default;

ALTER TABLE niva.data_file 
    ADD CONSTRAINT dafi_PK  PRIMARY KEY 
    USING INDEX dafi_PK_I;

CREATE  INDEX dafi_clas_id_FI
    ON niva.data_file (clas_id)
    TABLESPACE pg_default;

CREATE  INDEX dafi_fite_id_FI
    ON niva.data_file (fite_id)
    TABLESPACE pg_default;

CREATE TABLE niva.cover_type
(
    coty_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    code varchar(30)   NOT NULL,
    name varchar(60)   NOT NULL,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.cover_type IS 'cover_type';

COMMENT ON COLUMN niva.cover_type.coty_id IS 'primary key';
COMMENT ON COLUMN niva.cover_type.code IS 'code of crop';
COMMENT ON COLUMN niva.cover_type.name IS 'crop name';
CREATE UNIQUE INDEX coty_PK_I
    ON niva.cover_type (coty_id)
    TABLESPACE pg_default;

ALTER TABLE niva.cover_type 
    ADD CONSTRAINT coty_PK  PRIMARY KEY 
    USING INDEX coty_PK_I;

CREATE UNIQUE INDEX cotycode_name__UI
    ON niva.cover_type (code, name)
    TABLESPACE pg_default;

ALTER TABLE niva.cover_type 
    ADD CONSTRAINT cotycode_name_uk  UNIQUE 
    USING INDEX cotycode_name__UI;

CREATE TABLE niva.super_class
(
    suca_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    code varchar(30)   NOT NULL,
    name varchar(60)   NOT NULL,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.super_class IS 'super_class';

COMMENT ON COLUMN niva.super_class.suca_id IS 'primary key';
COMMENT ON COLUMN niva.super_class.code IS 'code super class';
COMMENT ON COLUMN niva.super_class.name IS 'name of super class';
CREATE UNIQUE INDEX suca_PK_I
    ON niva.super_class (suca_id)
    TABLESPACE pg_default;

ALTER TABLE niva.super_class 
    ADD CONSTRAINT suca_PK  PRIMARY KEY 
    USING INDEX suca_PK_I;

CREATE UNIQUE INDEX sucacode_name__UI
    ON niva.super_class (code, name)
    TABLESPACE pg_default;

ALTER TABLE niva.super_class 
    ADD CONSTRAINT sucacode_name_uk  UNIQUE 
    USING INDEX sucacode_name__UI;

CREATE TABLE niva.super_class_detail
(
    sucd_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    suca_id int4   NOT NULL,
    cult_id int4   NOT NULL,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.super_class_detail IS 'super_class_detail';

COMMENT ON COLUMN niva.super_class_detail.sucd_id IS 'primary key';
COMMENT ON COLUMN niva.super_class_detail.suca_id IS 'fk to super_class';
COMMENT ON COLUMN niva.super_class_detail.cult_id IS 'fk to cultivation';
CREATE UNIQUE INDEX sucd_PK_I
    ON niva.super_class_detail (sucd_id)
    TABLESPACE pg_default;

ALTER TABLE niva.super_class_detail 
    ADD CONSTRAINT sucd_PK  PRIMARY KEY 
    USING INDEX sucd_PK_I;

CREATE  INDEX sucd_suca_id_FI
    ON niva.super_class_detail (suca_id)
    TABLESPACE pg_default;

CREATE  INDEX sucd_cult_id_FI
    ON niva.super_class_detail (cult_id)
    TABLESPACE pg_default;

CREATE UNIQUE INDEX sucdsuca_id_cult_id__UI
    ON niva.super_class_detail (suca_id, cult_id)
    TABLESPACE pg_default;

ALTER TABLE niva.super_class_detail 
    ADD CONSTRAINT sucdsuca_id_cult_id_uk  UNIQUE 
    USING INDEX sucdsuca_id_cult_id__UI;

CREATE TABLE niva.cultivation
(
    cult_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    coty_id int4   ,
    name varchar(60)   NOT NULL,
    code int4   NOT NULL,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.cultivation IS 'cultivation';

COMMENT ON COLUMN niva.cultivation.cult_id IS 'primary key';
COMMENT ON COLUMN niva.cultivation.coty_id IS 'fk to cover_type';
COMMENT ON COLUMN niva.cultivation.name IS 'cultivation name';
COMMENT ON COLUMN niva.cultivation.code IS 'code of cultivation';
CREATE UNIQUE INDEX cult_PK_I
    ON niva.cultivation (cult_id)
    TABLESPACE pg_default;

ALTER TABLE niva.cultivation 
    ADD CONSTRAINT cult_PK  PRIMARY KEY 
    USING INDEX cult_PK_I;

CREATE  INDEX cult_coty_id_FI
    ON niva.cultivation (coty_id)
    TABLESPACE pg_default;

CREATE UNIQUE INDEX cultcode_name__UI
    ON niva.cultivation (code, name)
    TABLESPACE pg_default;

ALTER TABLE niva.cultivation 
    ADD CONSTRAINT cultcode_name_uk  UNIQUE 
    USING INDEX cultcode_name__UI;

CREATE TABLE niva.classification
(
    clas_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    clfr_id int4   NOT NULL,
    decl_id int4   ,
    fite_id int4   ,
    name varchar(60)   NOT NULL,
    description varchar(60)   ,
    date_time timestamptz   NOT NULL,
    recordtype int4   NOT NULL,
    attached_file bytea   ,
    file_path varchar(60)   ,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_BLOB;

COMMENT ON TABLE niva.classification IS 'classification';

COMMENT ON COLUMN niva.classification.clas_id IS 'primary key';
COMMENT ON COLUMN niva.classification.clfr_id IS 'fk to classifier';
COMMENT ON COLUMN niva.classification.decl_id IS 'fk of declarations';
COMMENT ON COLUMN niva.classification.fite_id IS 'fk to file template';
COMMENT ON COLUMN niva.classification.recordtype IS '0: not import, can edit, 1: import, no edit';
COMMENT ON COLUMN niva.classification.attached_file IS 'upload file';
COMMENT ON COLUMN niva.classification.file_path IS 'path of file';
CREATE UNIQUE INDEX clas_PK_I
    ON niva.classification (clas_id)
    TABLESPACE pg_default;

ALTER TABLE niva.classification 
    ADD CONSTRAINT clas_PK  PRIMARY KEY 
    USING INDEX clas_PK_I;

CREATE  INDEX clas_clfr_id_FI
    ON niva.classification (clfr_id)
    TABLESPACE pg_default;

CREATE  INDEX clas_decl_id_FI
    ON niva.classification (decl_id)
    TABLESPACE pg_default;

CREATE  INDEX clas_fite_id_FI
    ON niva.classification (fite_id)
    TABLESPACE pg_default;

CREATE TABLE niva.statistics
(
    stat_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    clas_id int4   NOT NULL,
    name varchar(60)   NOT NULL,
    description varchar(60)   ,
    value int4   ,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.statistics IS 'Statistics';

COMMENT ON COLUMN niva.statistics.stat_id IS 'primary key';
COMMENT ON COLUMN niva.statistics.clas_id IS 'fk to classification';
COMMENT ON COLUMN niva.statistics.value IS 'value';
CREATE UNIQUE INDEX stat_PK_I
    ON niva.statistics (stat_id)
    TABLESPACE pg_default;

ALTER TABLE niva.statistics 
    ADD CONSTRAINT stat_PK  PRIMARY KEY 
    USING INDEX stat_PK_I;

CREATE  INDEX stat_clas_id_FI
    ON niva.statistics (clas_id)
    TABLESPACE pg_default;

CREATE TABLE niva.parcel_class
(
    pcla_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    clas_id int4   NOT NULL,
    parc_id int4   ,
    cult_id_decl int4   ,
    cult_id_pred int4   ,
    coty_id_decl int4   ,
    coty_id_pred int4   ,
    prob_pred int4   NOT NULL,
    cult_id_pred2 int4   ,
    prob_pred2 int4   NOT NULL,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.parcel_class IS 'classification of parcel';

COMMENT ON COLUMN niva.parcel_class.pcla_id IS 'primary key';
COMMENT ON COLUMN niva.parcel_class.clas_id IS 'fk to classification';
COMMENT ON COLUMN niva.parcel_class.parc_id IS 'fk to parcel';
COMMENT ON COLUMN niva.parcel_class.cult_id_decl IS 'cultivation declared';
COMMENT ON COLUMN niva.parcel_class.cult_id_pred IS 'cultivation predicted';
COMMENT ON COLUMN niva.parcel_class.coty_id_decl IS 'fk to cover_type declared';
COMMENT ON COLUMN niva.parcel_class.coty_id_pred IS 'fk to cover_type predicted';
COMMENT ON COLUMN niva.parcel_class.prob_pred IS 'probability predicted';
COMMENT ON COLUMN niva.parcel_class.cult_id_pred2 IS 'cultivation predicted';
COMMENT ON COLUMN niva.parcel_class.prob_pred2 IS 'probability predicted';
CREATE UNIQUE INDEX pcla_PK_I
    ON niva.parcel_class (pcla_id)
    TABLESPACE pg_default;

ALTER TABLE niva.parcel_class 
    ADD CONSTRAINT pcla_PK  PRIMARY KEY 
    USING INDEX pcla_PK_I;

CREATE  INDEX pcla_clas_id_FI
    ON niva.parcel_class (clas_id)
    TABLESPACE pg_default;

CREATE  INDEX pcla_parc_id_FI
    ON niva.parcel_class (parc_id)
    TABLESPACE pg_default;

CREATE  INDEX pcla_cult_id_decl_FI
    ON niva.parcel_class (cult_id_decl)
    TABLESPACE pg_default;

CREATE  INDEX pcla_cult_id_pred_FI
    ON niva.parcel_class (cult_id_pred)
    TABLESPACE pg_default;

CREATE  INDEX pcla_coty_id_decl_FI
    ON niva.parcel_class (coty_id_decl)
    TABLESPACE pg_default;

CREATE  INDEX pcla_coty_id_pred_FI
    ON niva.parcel_class (coty_id_pred)
    TABLESPACE pg_default;

CREATE  INDEX pcla_cult_id_pred2_FI
    ON niva.parcel_class (cult_id_pred2)
    TABLESPACE pg_default;

CREATE TABLE niva.parcel
(
    parc_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    geom GEOMETRY   NOT NULL,
    prod_code int4   ,
    year int4   NOT NULL,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.parcel IS 'parcel';

COMMENT ON COLUMN niva.parcel.parc_id IS 'primary key';
COMMENT ON COLUMN niva.parcel.geom IS 'geometry of parcel';
COMMENT ON COLUMN niva.parcel.prod_code IS 'producer code';
COMMENT ON COLUMN niva.parcel.year IS 'year';
CREATE UNIQUE INDEX parc_PK_I
    ON niva.parcel (parc_id)
    TABLESPACE pg_default;

ALTER TABLE niva.parcel 
    ADD CONSTRAINT parc_PK  PRIMARY KEY 
    USING INDEX parc_PK_I;

CREATE TABLE niva.geotagphotos
(
    geot_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    parc_id int4   NOT NULL,
    image_path varchar(60)   NOT NULL,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.geotagphotos IS 'Geo Tagged Photographs of class parcel';

COMMENT ON COLUMN niva.geotagphotos.geot_id IS 'primary key';
COMMENT ON COLUMN niva.geotagphotos.parc_id IS 'fk to parcel';
COMMENT ON COLUMN niva.geotagphotos.image_path IS 'path of image';
CREATE UNIQUE INDEX geot_PK_I
    ON niva.geotagphotos (geot_id)
    TABLESPACE pg_default;

ALTER TABLE niva.geotagphotos 
    ADD CONSTRAINT geot_PK  PRIMARY KEY 
    USING INDEX geot_PK_I;

CREATE  INDEX geot_parc_id_FI
    ON niva.geotagphotos (parc_id)
    TABLESPACE pg_default;

CREATE TABLE niva.parc_activities
(
    paac_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    parc_id int4   NOT NULL,
    code varchar(30)   NOT NULL,
    date_act timestamptz   NOT NULL,
    acti_id int4   NOT NULL,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.parc_activities IS 'Activities of parcel';

COMMENT ON COLUMN niva.parc_activities.paac_id IS 'primary key';
COMMENT ON COLUMN niva.parc_activities.parc_id IS 'fk to parcel';
COMMENT ON COLUMN niva.parc_activities.code IS 'code of activity';
COMMENT ON COLUMN niva.parc_activities.date_act IS 'date of activity';
COMMENT ON COLUMN niva.parc_activities.acti_id IS 'fk to activity';
CREATE UNIQUE INDEX paac_PK_I
    ON niva.parc_activities (paac_id)
    TABLESPACE pg_default;

ALTER TABLE niva.parc_activities 
    ADD CONSTRAINT paac_PK  PRIMARY KEY 
    USING INDEX paac_PK_I;

CREATE  INDEX paac_parc_id_FI
    ON niva.parc_activities (parc_id)
    TABLESPACE pg_default;

CREATE  INDEX paac_acti_id_FI
    ON niva.parc_activities (acti_id)
    TABLESPACE pg_default;

CREATE TABLE niva.activity
(
    acti_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    code varchar(30)   NOT NULL,
    name varchar(60)   NOT NULL,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.activity IS 'Activities';

COMMENT ON COLUMN niva.activity.acti_id IS 'primary key';
COMMENT ON COLUMN niva.activity.code IS 'code of activity';
COMMENT ON COLUMN niva.activity.name IS 'activity name';
CREATE UNIQUE INDEX acti_PK_I
    ON niva.activity (acti_id)
    TABLESPACE pg_default;

ALTER TABLE niva.activity 
    ADD CONSTRAINT acti_PK  PRIMARY KEY 
    USING INDEX acti_PK_I;

CREATE TABLE niva.ec_group
(
    ecgr_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    description varchar(60)   ,
    name varchar(60)   NOT NULL,
    recordtype int4   NOT NULL,
    crop_level int4   NOT NULL,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.ec_group IS 'eligibility Criteria group';

COMMENT ON COLUMN niva.ec_group.ecgr_id IS 'primary key';
COMMENT ON COLUMN niva.ec_group.name IS 'ec_group name';
COMMENT ON COLUMN niva.ec_group.recordtype IS '0: not finalize, can edit, 1: final, no edit';
COMMENT ON COLUMN niva.ec_group.crop_level IS '0: Cultivation, 1: Crop Cover(General type)';
CREATE UNIQUE INDEX ecgr_PK_I
    ON niva.ec_group (ecgr_id)
    TABLESPACE pg_default;

ALTER TABLE niva.ec_group 
    ADD CONSTRAINT ecgr_PK  PRIMARY KEY 
    USING INDEX ecgr_PK_I;

CREATE TABLE niva.ec_cult
(
    eccu_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    cult_id int4   ,
    ecgr_id int4   NOT NULL,
    none_match_decision int4   ,
    coty_id int4   ,
    suca_id int4   ,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.ec_cult IS 'Eligibility Criteria of cultivation';

COMMENT ON COLUMN niva.ec_cult.eccu_id IS 'primary key';
COMMENT ON COLUMN niva.ec_cult.cult_id IS 'fk to cultivation';
COMMENT ON COLUMN niva.ec_cult.ecgr_id IS 'fk to Ec_Group';
COMMENT ON COLUMN niva.ec_cult.none_match_decision IS '1: green, 2: yellow, 3:red';
COMMENT ON COLUMN niva.ec_cult.coty_id IS 'fk to cover_type';
COMMENT ON COLUMN niva.ec_cult.suca_id IS 'fk to super_class';
CREATE UNIQUE INDEX eccu_PK_I
    ON niva.ec_cult (eccu_id)
    TABLESPACE pg_default;

ALTER TABLE niva.ec_cult 
    ADD CONSTRAINT eccu_PK  PRIMARY KEY 
    USING INDEX eccu_PK_I;

CREATE  INDEX eccu_cult_id_FI
    ON niva.ec_cult (cult_id)
    TABLESPACE pg_default;

CREATE  INDEX eccu_ecgr_id_FI
    ON niva.ec_cult (ecgr_id)
    TABLESPACE pg_default;

CREATE  INDEX eccu_coty_id_FI
    ON niva.ec_cult (coty_id)
    TABLESPACE pg_default;

CREATE  INDEX eccu_suca_id_FI
    ON niva.ec_cult (suca_id)
    TABLESPACE pg_default;

CREATE TABLE niva.ec_cult_detail
(
    eccd_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    eccu_id int4   NOT NULL,
    ordering_number int4   ,
    agrees_declar bool   NOT NULL,
    comparison_oper int4   NOT NULL,
    probab_thres int4   NOT NULL,
    activities_match int4   NOT NULL,
    decision_light int4   NOT NULL,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.ec_cult_detail IS 'criteria of cultivation';

COMMENT ON COLUMN niva.ec_cult_detail.eccd_id IS 'primary key';
COMMENT ON COLUMN niva.ec_cult_detail.eccu_id IS 'fk to cultivation';
COMMENT ON COLUMN niva.ec_cult_detail.ordering_number IS 'AA (No.) increasing ordering number';
COMMENT ON COLUMN niva.ec_cult_detail.agrees_declar IS '1: agree, 0: disagree declaration';
COMMENT ON COLUMN niva.ec_cult_detail.comparison_oper IS '1: greater, 2: greater equal, 3:less, 4: less equal';
COMMENT ON COLUMN niva.ec_cult_detail.probab_thres IS 'probability threshold ';
COMMENT ON COLUMN niva.ec_cult_detail.activities_match IS 'ACtivities matching eligibity 1:none, 2:all, 3:any';
COMMENT ON COLUMN niva.ec_cult_detail.decision_light IS '1: green, 2: yellow, 3:red';
CREATE UNIQUE INDEX eccd_PK_I
    ON niva.ec_cult_detail (eccd_id)
    TABLESPACE pg_default;

ALTER TABLE niva.ec_cult_detail 
    ADD CONSTRAINT eccd_PK  PRIMARY KEY 
    USING INDEX eccd_PK_I;

CREATE  INDEX eccd_eccu_id_FI
    ON niva.ec_cult_detail (eccu_id)
    TABLESPACE pg_default;

CREATE TABLE niva.eccu_activities
(
    eact_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    acti_id int4   NOT NULL,
    eccu_id int4   NOT NULL,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.eccu_activities IS 'activities of a cultivation criterio';

COMMENT ON COLUMN niva.eccu_activities.eact_id IS 'primary key';
COMMENT ON COLUMN niva.eccu_activities.acti_id IS 'fk to activity';
COMMENT ON COLUMN niva.eccu_activities.eccu_id IS 'fk to criteria';
CREATE UNIQUE INDEX eact_PK_I
    ON niva.eccu_activities (eact_id)
    TABLESPACE pg_default;

ALTER TABLE niva.eccu_activities 
    ADD CONSTRAINT eact_PK  PRIMARY KEY 
    USING INDEX eact_PK_I;

CREATE  INDEX eact_acti_id_FI
    ON niva.eccu_activities (acti_id)
    TABLESPACE pg_default;

CREATE  INDEX eact_eccu_id_FI
    ON niva.eccu_activities (eccu_id)
    TABLESPACE pg_default;

CREATE TABLE niva.decision_making
(
    dema_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    clas_id int4   NOT NULL,
    ecgr_id int4   NOT NULL,
    description varchar(60)   ,
    date_time timestamptz   NOT NULL,
    recordtype int4   NOT NULL,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.decision_making IS 'decision making';

COMMENT ON COLUMN niva.decision_making.dema_id IS 'primary key';
COMMENT ON COLUMN niva.decision_making.clas_id IS 'fk to classification';
COMMENT ON COLUMN niva.decision_making.ecgr_id IS 'fk to ec_group';
COMMENT ON COLUMN niva.decision_making.recordtype IS '0: not run, can edit, 1: run, no edit';
CREATE UNIQUE INDEX dema_PK_I
    ON niva.decision_making (dema_id)
    TABLESPACE pg_default;

ALTER TABLE niva.decision_making 
    ADD CONSTRAINT dema_PK  PRIMARY KEY 
    USING INDEX dema_PK_I;

CREATE  INDEX dema_clas_id_FI
    ON niva.decision_making (clas_id)
    TABLESPACE pg_default;

CREATE  INDEX dema_ecgr_id_FI
    ON niva.decision_making (ecgr_id)
    TABLESPACE pg_default;

CREATE TABLE niva.parcel_decision
(
    pade_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    dema_id int4   NOT NULL,
    pcla_id int4   NOT NULL,
    decision_light int4   NOT NULL,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_DATA;

COMMENT ON TABLE niva.parcel_decision IS 'parcel_class decision';

COMMENT ON COLUMN niva.parcel_decision.pade_id IS 'primary key';
COMMENT ON COLUMN niva.parcel_decision.dema_id IS 'fk to decision_making';
COMMENT ON COLUMN niva.parcel_decision.pcla_id IS 'fk to parcel_class';
COMMENT ON COLUMN niva.parcel_decision.decision_light IS '1: green, 2: yellow, 3:red';
CREATE UNIQUE INDEX pade_PK_I
    ON niva.parcel_decision (pade_id)
    TABLESPACE pg_default;

ALTER TABLE niva.parcel_decision 
    ADD CONSTRAINT pade_PK  PRIMARY KEY 
    USING INDEX pade_PK_I;

CREATE  INDEX pade_dema_id_FI
    ON niva.parcel_decision (dema_id)
    TABLESPACE pg_default;

CREATE  INDEX pade_pcla_id_FI
    ON niva.parcel_decision (pcla_id)
    TABLESPACE pg_default;

CREATE TABLE niva.documents
(
    docu_id integer NOT NULL DEFAULT nextval('niva.niva_SQ'::regclass),
    attached_file bytea   ,
    file_path varchar(60)   ,
    description varchar(60)   ,
    USRINSERT varchar(150)   ,
    DTEINSERT timestamp   ,
    USRUPDATE varchar(150)   ,
    DTEUPDATE timestamp   ,
    ROW_VERSION int4 DEFAULT 0  NOT NULL
)
TABLESPACE niva_BLOB;

COMMENT ON TABLE niva.documents IS 'documents for help';

COMMENT ON COLUMN niva.documents.docu_id IS 'primary key';
COMMENT ON COLUMN niva.documents.attached_file IS 'upload file';
COMMENT ON COLUMN niva.documents.file_path IS 'name of file';
COMMENT ON COLUMN niva.documents.description IS 'description of file';
CREATE UNIQUE INDEX docu_PK_I
    ON niva.documents (docu_id)
    TABLESPACE pg_default;

ALTER TABLE niva.documents 
    ADD CONSTRAINT docu_PK  PRIMARY KEY 
    USING INDEX docu_PK_I;

-- GRANT REFS
/*
*/
-- FOREIGN KEY CONSTRAINTS
ALTER TABLE niva.declarations 
    ADD CONSTRAINT decl_agen_id_FK  FOREIGN KEY (agen_id) 
        REFERENCES niva.agency (agen_id);

ALTER TABLE niva.classifier 
    ADD CONSTRAINT clfr_fite_id_FK  FOREIGN KEY (fite_id) 
        REFERENCES niva.file_template (fite_id);

ALTER TABLE niva.template_columns 
    ADD CONSTRAINT teco_fite_id_FK  FOREIGN KEY (fite_id) 
        REFERENCES niva.file_template (fite_id);

ALTER TABLE niva.template_columns 
    ADD CONSTRAINT teco_prco_id_FK  FOREIGN KEY (prco_id) 
        REFERENCES niva.predef_col (prco_id);

ALTER TABLE niva.data_file 
    ADD CONSTRAINT dafi_clas_id_FK  FOREIGN KEY (clas_id) 
        REFERENCES niva.classification (clas_id);

ALTER TABLE niva.data_file 
    ADD CONSTRAINT dafi_fite_id_FK  FOREIGN KEY (fite_id) 
        REFERENCES niva.file_template (fite_id);

ALTER TABLE niva.super_class_detail 
    ADD CONSTRAINT sucd_suca_id_FK  FOREIGN KEY (suca_id) 
        REFERENCES niva.super_class (suca_id);

ALTER TABLE niva.super_class_detail 
    ADD CONSTRAINT sucd_cult_id_FK  FOREIGN KEY (cult_id) 
        REFERENCES niva.cultivation (cult_id);

ALTER TABLE niva.cultivation 
    ADD CONSTRAINT cult_coty_id_FK  FOREIGN KEY (coty_id) 
        REFERENCES niva.cover_type (coty_id);

ALTER TABLE niva.classification 
    ADD CONSTRAINT clas_clfr_id_FK  FOREIGN KEY (clfr_id) 
        REFERENCES niva.classifier (clfr_id);

ALTER TABLE niva.classification 
    ADD CONSTRAINT clas_decl_id_FK  FOREIGN KEY (decl_id) 
        REFERENCES niva.declarations (decl_id);

ALTER TABLE niva.classification 
    ADD CONSTRAINT clas_fite_id_FK  FOREIGN KEY (fite_id) 
        REFERENCES niva.file_template (fite_id);

ALTER TABLE niva.statistics 
    ADD CONSTRAINT stat_clas_id_FK  FOREIGN KEY (clas_id) 
        REFERENCES niva.classification (clas_id);

ALTER TABLE niva.parcel_class 
    ADD CONSTRAINT pcla_clas_id_FK  FOREIGN KEY (clas_id) 
        REFERENCES niva.classification (clas_id);

ALTER TABLE niva.parcel_class 
    ADD CONSTRAINT pcla_parc_id_FK  FOREIGN KEY (parc_id) 
        REFERENCES niva.parcel (parc_id);

ALTER TABLE niva.parcel_class 
    ADD CONSTRAINT pcla_cult_id_decl_FK  FOREIGN KEY (cult_id_decl) 
        REFERENCES niva.cultivation (cult_id);

ALTER TABLE niva.parcel_class 
    ADD CONSTRAINT pcla_cult_id_pred_FK  FOREIGN KEY (cult_id_pred) 
        REFERENCES niva.cultivation (cult_id);

ALTER TABLE niva.parcel_class 
    ADD CONSTRAINT pcla_coty_id_decl_FK  FOREIGN KEY (coty_id_decl) 
        REFERENCES niva.cover_type (coty_id);

ALTER TABLE niva.parcel_class 
    ADD CONSTRAINT pcla_coty_id_pred_FK  FOREIGN KEY (coty_id_pred) 
        REFERENCES niva.cover_type (coty_id);

ALTER TABLE niva.parcel_class 
    ADD CONSTRAINT pcla_cult_id_pred2_FK  FOREIGN KEY (cult_id_pred2) 
        REFERENCES niva.cultivation (cult_id);

ALTER TABLE niva.geotagphotos 
    ADD CONSTRAINT geot_parc_id_FK  FOREIGN KEY (parc_id) 
        REFERENCES niva.parcel (parc_id);

ALTER TABLE niva.parc_activities 
    ADD CONSTRAINT paac_parc_id_FK  FOREIGN KEY (parc_id) 
        REFERENCES niva.parcel (parc_id);

ALTER TABLE niva.parc_activities 
    ADD CONSTRAINT paac_acti_id_FK  FOREIGN KEY (acti_id) 
        REFERENCES niva.activity (acti_id);

ALTER TABLE niva.ec_cult 
    ADD CONSTRAINT eccu_cult_id_FK  FOREIGN KEY (cult_id) 
        REFERENCES niva.cultivation (cult_id);

ALTER TABLE niva.ec_cult 
    ADD CONSTRAINT eccu_ecgr_id_FK  FOREIGN KEY (ecgr_id) 
        REFERENCES niva.ec_group (ecgr_id);

ALTER TABLE niva.ec_cult 
    ADD CONSTRAINT eccu_coty_id_FK  FOREIGN KEY (coty_id) 
        REFERENCES niva.cultivation (cult_id);

ALTER TABLE niva.ec_cult 
    ADD CONSTRAINT eccu_suca_id_FK  FOREIGN KEY (suca_id) 
        REFERENCES niva.super_class (suca_id);

ALTER TABLE niva.ec_cult_detail 
    ADD CONSTRAINT eccd_eccu_id_FK  FOREIGN KEY (eccu_id) 
        REFERENCES niva.ec_cult (eccu_id);

ALTER TABLE niva.eccu_activities 
    ADD CONSTRAINT eact_acti_id_FK  FOREIGN KEY (acti_id) 
        REFERENCES niva.activity (acti_id);

ALTER TABLE niva.eccu_activities 
    ADD CONSTRAINT eact_eccu_id_FK  FOREIGN KEY (eccu_id) 
        REFERENCES niva.ec_cult_detail (eccd_id);

ALTER TABLE niva.decision_making 
    ADD CONSTRAINT dema_clas_id_FK  FOREIGN KEY (clas_id) 
        REFERENCES niva.classification (clas_id);

ALTER TABLE niva.decision_making 
    ADD CONSTRAINT dema_ecgr_id_FK  FOREIGN KEY (ecgr_id) 
        REFERENCES niva.ec_group (ecgr_id);

ALTER TABLE niva.parcel_decision 
    ADD CONSTRAINT pade_dema_id_FK  FOREIGN KEY (dema_id) 
        REFERENCES niva.decision_making (dema_id);

ALTER TABLE niva.parcel_decision 
    ADD CONSTRAINT pade_pcla_id_FK  FOREIGN KEY (pcla_id) 
        REFERENCES niva.parcel_class (pcla_id);

