CREATE SEQUENCE niva.documents_docu_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

CREATE SEQUENCE niva.tmp_blob_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

CREATE TABLE niva.tmp_blob
(
    id integer NOT NULL DEFAULT nextval('niva.tmp_blob_id_seq'::regclass),
    data bytea,
    row_version integer NOT NULL DEFAULT 0,
    dteinsert timestamp without time zone,
    usrinsert character varying(150) COLLATE pg_catalog."default",
    dteupdate timestamp without time zone,
    usrupdate character varying(150) COLLATE pg_catalog."default",
    CONSTRAINT nitb_pk PRIMARY KEY (id)
)

TABLESPACE pg_default;


CREATE TABLE niva.excel_files
(
    id integer NOT NULL DEFAULT nextval('niva.niva_sq'::regclass),
    filename character varying(120) COLLATE pg_catalog."default" NOT NULL,
    data bytea,
    total_rows integer,
    total_error_rows integer,
    usrinsert character varying(150) COLLATE pg_catalog."default",
    dteinsert timestamp without time zone,
    usrupdate character varying(150) COLLATE pg_catalog."default",
    dteupdate timestamp without time zone,
    row_version integer NOT NULL DEFAULT 0,
    CONSTRAINT exfi_pk PRIMARY KEY (id)
)

TABLESPACE niva_blob;


COMMENT ON TABLE niva.excel_files
    IS 'excel files';

COMMENT ON COLUMN niva.excel_files.id
    IS 'primary key';

COMMENT ON COLUMN niva.excel_files.filename
    IS 'name of file';

COMMENT ON COLUMN niva.excel_files.data
    IS 'upload file';

COMMENT ON COLUMN niva.excel_files.total_rows
    IS 'total rows';

COMMENT ON COLUMN niva.excel_files.total_error_rows
    IS 'total rows';

CREATE TABLE niva.excel_errors
(
    id integer NOT NULL DEFAULT nextval('niva.niva_sq'::regclass),
    exfi_id integer NOT NULL,
    excel_row_num integer,
    err_message character varying(500) COLLATE pg_catalog."default",
    usrinsert character varying(150) COLLATE pg_catalog."default",
    dteinsert timestamp without time zone,
    usrupdate character varying(150) COLLATE pg_catalog."default",
    dteupdate timestamp without time zone,
    row_version integer NOT NULL DEFAULT 0,
    CONSTRAINT exer_pk PRIMARY KEY (id),
    CONSTRAINT exer_exfi_id_fk FOREIGN KEY (exfi_id)
        REFERENCES niva.excel_files (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE niva_data;

COMMENT ON TABLE niva.excel_errors
    IS 'excel errors';

COMMENT ON COLUMN niva.excel_errors.id
    IS 'primary key';

COMMENT ON COLUMN niva.excel_errors.exfi_id
    IS 'fk to excel_files';

COMMENT ON COLUMN niva.excel_errors.excel_row_num
    IS 'excel_row_num';

COMMENT ON COLUMN niva.excel_errors.err_message
    IS 'err_message';
CREATE INDEX exer_exfi_id_fi
    ON niva.excel_errors USING btree
    (exfi_id ASC NULLS LAST)
    TABLESPACE pg_default;

---------------------------------------------------------
ALTER TABLE niva.cultivation
    ADD COLUMN exfi_id integer;

COMMENT ON COLUMN niva.cultivation.exfi_id
    IS 'fk to excel_files';


ALTER TABLE niva.cultivation
    ADD CONSTRAINT cult_code_un UNIQUE (code);

ALTER TABLE niva.cultivation
    ADD CONSTRAINT cult_name_un UNIQUE (name);

ALTER TABLE niva.cultivation
    ADD CONSTRAINT cult_exfi_id_fk FOREIGN KEY (exfi_id)
    REFERENCES niva.excel_files (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

---------------------------------------
ALTER TABLE niva.decision_making
    ALTER COLUMN recordtype SET DEFAULT 0;

ALTER TABLE niva.decision_making
    ALTER COLUMN recordtype DROP NOT NULL;
-----------------------------------------


COMMENT ON COLUMN niva.ec_cult.coty_id
    IS 'fk to cover_type';



COMMENT ON COLUMN niva.ec_cult.suca_id
    IS 'fk to super_class';


ALTER TABLE niva.ec_cult
    ADD CONSTRAINT ec_cult_fk FOREIGN KEY (coty_id)
    REFERENCES niva.cover_type (coty_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
DROP INDEX niva.eccu_coty_id_fi;



COMMENT ON COLUMN niva.ec_cult_detail.activities_match
    IS 'ACtivities matching eligibity 1:none, 2:all, 3:any';



COMMENT ON COLUMN niva.ec_cult_detail.agrees_declar
    IS '1: agree, 0: disagree declaration';


COMMENT ON COLUMN niva.ec_cult_detail.comparison_oper
    IS '1: greater, 2: greater equal, 3:less, 4: less equal';


COMMENT ON COLUMN niva.ec_cult_detail.decision_light
    IS '1: green, 2: yellow, 3:red';


COMMENT ON COLUMN niva.ec_cult_detail.probab_thres
    IS 'probability threshold ';

COMMENT ON COLUMN niva.ec_group.crop_level
    IS '0: Cultivation, 1: Crop Cover(General type)';

ALTER TABLE niva.eccu_activities DROP COLUMN eccu_id;

ALTER TABLE niva.eccu_activities
    ADD COLUMN eccd_id integer NOT NULL;

COMMENT ON COLUMN niva.eccu_activities.eccd_id
    IS 'fk to criteria';


ALTER TABLE niva.eccu_activities
    ADD CONSTRAINT eccu_eccd_fk FOREIGN KEY (eccd_id)
    REFERENCES niva.ec_cult_detail (eccd_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

DROP INDEX if exists niva.eact_eccu_id_fi;

CREATE INDEX eact_eccu_id_fi
    ON niva.eccu_activities USING btree
    (eccd_id ASC NULLS LAST)
    TABLESPACE pg_default;

-----------------------
ALTER TABLE niva.parcel DROP COLUMN dteupdate;

ALTER TABLE niva.parcel DROP COLUMN usrupdate;


ALTER TABLE niva.parcel
    ALTER COLUMN geom DROP NOT NULL;

COMMENT ON COLUMN niva.parcel.geom
    IS 'geometry of parcel';

ALTER TABLE niva.parcel_class DROP COLUMN dteinsert;

ALTER TABLE niva.parcel_class DROP COLUMN dteupdate;

ALTER TABLE niva.parcel_class DROP COLUMN row_version;

ALTER TABLE niva.parcel_class DROP COLUMN usrinsert;

ALTER TABLE niva.parcel_class DROP COLUMN usrupdate;

-- WARNING:
-- The SQL statement below would normally be used to alter the datatype for the cult_id_pred2 column, however,
-- the current datatype cannot be cast to the target datatype so this conversion cannot be made automatically.

 -- ALTER TABLE niva.parcel_class
 --     ALTER COLUMN cult_id_pred2 TYPE integer;

-- WARNING:
-- The SQL statement below would normally be used to alter the datatype for the parc_id column, however,
-- the current datatype cannot be cast to the target datatype so this conversion cannot be made automatically.

 -- ALTER TABLE niva.parcel_class
 --     ALTER COLUMN parc_id TYPE integer;
ALTER TABLE niva.parcel_class
    ALTER COLUMN parc_id SET NOT NULL;

COMMENT ON COLUMN niva.parcel_class.parc_id
    IS 'fk to parcel';

-- WARNING:
-- The SQL statement below would normally be used to alter the datatype for the prob_pred column, however,
-- the current datatype cannot be cast to the target datatype so this conversion cannot be made automatically.

 -- ALTER TABLE niva.parcel_class
 --     ALTER COLUMN prob_pred TYPE double precision;
ALTER TABLE niva.parcel_class
    ALTER COLUMN prob_pred DROP NOT NULL;

COMMENT ON COLUMN niva.parcel_class.prob_pred
    IS 'probability predicted';

-- WARNING:
-- The SQL statement below would normally be used to alter the datatype for the prob_pred2 column, however,
-- the current datatype cannot be cast to the target datatype so this conversion cannot be made automatically.

 -- ALTER TABLE niva.parcel_class
 --     ALTER COLUMN prob_pred2 TYPE double precision;
ALTER TABLE niva.parcel_class
    ALTER COLUMN prob_pred2 DROP NOT NULL;
ALTER TABLE niva.parcel_class DROP CONSTRAINT pcla_cult_id_decl_fk;

ALTER TABLE niva.parcel_class DROP CONSTRAINT pcla_cult_id_pred2_fk;

ALTER TABLE niva.parcel_class DROP CONSTRAINT pcla_cult_id_pred_fk;

ALTER TABLE niva.parcel_class
    ADD CONSTRAINT parcel_class_fk FOREIGN KEY (cult_id_decl)
    REFERENCES niva.cultivation (cult_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

ALTER TABLE niva.parcel_class
    ADD CONSTRAINT parcel_class_fk_1 FOREIGN KEY (cult_id_pred)
    REFERENCES niva.cultivation (cult_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

ALTER TABLE niva.parcel_class
    ADD CONSTRAINT parcel_class_fk_2 FOREIGN KEY (cult_id_pred2)
    REFERENCES niva.cultivation (cult_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;
DROP INDEX niva.pcla_cult_id_pred2_fi;

ALTER TABLE niva.predef_col
    ADD COLUMN system_column_name character varying COLLATE pg_catalog."default";
ALTER TABLE niva.predef_col
    ADD CONSTRAINT predef_col_un UNIQUE (column_name);

ALTER TABLE niva.super_class_detail DROP CONSTRAINT sucdsuca_id_cult_id_uk;
CREATE UNIQUE INDEX sucdsuca_id_cult_id__ui
    ON niva.super_class_detail USING btree
    (suca_id ASC NULLS LAST, cult_id ASC NULLS LAST)
    TABLESPACE pg_default;

ALTER TABLE niva.template_columns
    ADD CONSTRAINT fite_clna_un UNIQUE (fite_id, clfier_name);

ALTER TABLE niva.template_columns
    ADD CONSTRAINT prco_fite_un UNIQUE (fite_id, prco_id);

CREATE OR REPLACE FUNCTION niva.import_crops(
	i_name character varying,
	i_code integer,
	i_excel_id integer)
    RETURNS numeric
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    
AS $BODY$
DECLARE
    n_name VARCHAR(60);   
    n_code INTEGER;
begin
	
	select name
     into n_name
     from niva.cultivation
    where name = i_name;
     if found then
       RAISE EXCEPTION 'Crop Name % Already Exist ', i_name;
     end if;
    
    select code
     into n_code
     from niva.cultivation
    where code = i_code;
     if found then
       RAISE EXCEPTION 'Crop Code % Already Exist ', i_name;
     end if;
    
          INSERT INTO  niva.cultivation( name,
                                  code,
                                  exfi_id
                                )
                            VALUES (i_name,
                                    i_code,
                                    i_excel_id                              
                            );



  return 0;

END;
$BODY$;


ALTER TABLE niva.classification
    ALTER COLUMN recordtype SET DEFAULT 0;

ALTER TABLE niva.classification
    ALTER COLUMN fite_id SET NOT NULL;

COMMENT ON COLUMN niva.classification.fite_id
    IS 'fk to file template';
	


COMMENT ON COLUMN niva.ec_cult_detail.activities_match
    IS 'ACtivities matching eligibity 1:none, 2:all, 3:any';



COMMENT ON COLUMN niva.ec_cult_detail.agrees_declar
    IS '1: agree, 0: disagree declaration';



COMMENT ON COLUMN niva.ec_cult_detail.comparison_oper
    IS '1: greater, 2: greater equal, 3:less, 4: less equal';


COMMENT ON COLUMN niva.ec_cult_detail.decision_light
    IS '1: green, 2: yellow, 3:red';



COMMENT ON COLUMN niva.ec_cult_detail.probab_thres
    IS 'probability threshold ';



COMMENT ON COLUMN niva.ec_group.crop_level
    IS '0: Cultivation, 1: Crop Cover(General type)';

COMMENT ON COLUMN niva.parcel.geom
    IS 'geometry of parcel';	

ALTER TABLE niva.parcel_class
    ALTER COLUMN prob_pred TYPE double precision;

COMMENT ON COLUMN niva.parcel_class.prob_pred
    IS 'probability predicted';

ALTER TABLE niva.parcel_class
    ALTER COLUMN prob_pred2 TYPE double precision;	
