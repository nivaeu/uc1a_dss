ALTER TABLE niva.parcel ADD code int4;
update niva.parcel set code = parc_id;
ALTER TABLE niva.parcel ALTER COLUMN code SET NOT NULL;
