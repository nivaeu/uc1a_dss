
-- Drop table

-- DROP TABLE niva.ec_group_copy;

CREATE TABLE niva.ec_group_copy (
	ecgc_id serial NOT NULL,
	description varchar(60) NULL,
	"name" varchar(60) NOT NULL,
	usrinsert varchar(150) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(150) NULL,
	dteupdate timestamp NULL,
	row_version int4 NOT NULL DEFAULT 0,
	recordtype int4 NOT NULL,
	crop_level int2 NOT NULL,
	CONSTRAINT ecgc_pk PRIMARY KEY (ecgc_id)
)
TABLESPACE niva_data
;

---------------------------------------------------------------------

-- Drop table

-- DROP TABLE niva.ec_cult_copy;

CREATE TABLE niva.ec_cult_copy (
	eccc_id serial NOT NULL,
	cult_id int4 NULL,
	ecgc_id int4 NOT NULL,
	none_match_decision int4 NULL,
	usrinsert varchar(150) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(150) NULL,
	dteupdate timestamp NULL,
	row_version int4 NOT NULL DEFAULT 0,
	coty_id int4 NULL,
	suca_id int4 NULL,
	CONSTRAINT eccc_pk PRIMARY KEY (eccc_id)
)
TABLESPACE niva_data
;
CREATE INDEX eccc_cult_id_fi ON niva.ec_cult_copy USING btree (cult_id);
CREATE INDEX eccc_ecgc_id_fi ON niva.ec_cult_copy USING btree (ecgc_id);
CREATE INDEX eccc_suca_id_fi ON niva.ec_cult_copy USING btree (suca_id);

ALTER TABLE niva.ec_cult_copy ADD CONSTRAINT ec_cult_copy_fk FOREIGN KEY (coty_id) REFERENCES niva.cover_type(coty_id);
ALTER TABLE niva.ec_cult_copy ADD CONSTRAINT eccc_cult_id_fk FOREIGN KEY (cult_id) REFERENCES niva.cultivation(cult_id);
ALTER TABLE niva.ec_cult_copy ADD CONSTRAINT eccc_ecgc_id_fk FOREIGN KEY (ecgc_id) REFERENCES niva.ec_group_copy(ecgc_id);
ALTER TABLE niva.ec_cult_copy ADD CONSTRAINT eccc_suca_id_fk FOREIGN KEY (suca_id) REFERENCES niva.super_class(suca_id);

---------------------------------------------------------------------


-- Drop table

-- DROP TABLE niva.ec_cult_detail_copy;

CREATE TABLE niva.ec_cult_detail_copy (
	ecdc_id serial NOT NULL,
	eccc_id int4 NOT NULL,
	ordering_number int4 NULL,
	agrees_declar int4 NULL,
	comparison_oper int4 NULL,
	probab_thres int4 NULL,
	activities_match int4 NULL,
	decision_light int4 NOT NULL,
	usrinsert varchar(150) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(150) NULL,
	dteupdate timestamp NULL,
	row_version int4 NOT NULL DEFAULT 0,
	agrees_declar2 int4 NULL,
	comparison_oper2 int4 NULL,
	probab_thres2 int4 NULL,
	probab_thres_sum int4 NULL,
	comparison_oper3 int4 NULL,
	CONSTRAINT ecdc_pk PRIMARY KEY (ecdc_id)
)
TABLESPACE niva_data
;
CREATE INDEX ecdc_eccc_id_fi ON niva.ec_cult_detail_copy USING btree (eccc_id);

ALTER TABLE niva.ec_cult_detail_copy ADD CONSTRAINT ecdc_eccc_id_fk FOREIGN KEY (eccc_id) REFERENCES niva.ec_cult_copy(eccc_id);


-----------------------------------
ALTER TABLE niva.decision_making ADD ecgc_id int4 NULL;
ALTER TABLE niva.decision_making ADD CONSTRAINT dema_ecgc_id_fk FOREIGN KEY (ecgc_id) REFERENCES niva.ec_group_copy(ecgc_id);


