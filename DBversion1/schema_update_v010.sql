alter table niva.cover_type alter column code type int4 USING code::integer;




drop function niva.import_land_covers(varchar, varchar, int4);


CREATE OR REPLACE FUNCTION niva.import_land_covers(i_code integer, i_name character varying, i_excel_id integer)
 RETURNS numeric
 LANGUAGE plpgsql
AS $function$
DECLARE
    n_code VARCHAR(30);   
    n_name VARCHAR(60);
begin
	
	select name
     into n_name
     from niva.cover_type 
    where name = i_name;
     if found then
       RAISE EXCEPTION 'Land Cover Name % Already Exist ', i_name;
     end if;
    
    select code
     into n_code
     from niva.cover_type 
    where code = i_code;
     if found then
       RAISE EXCEPTION 'Land Cover Code % Already Exist ', i_name;
     end if;
    
          INSERT INTO  niva.cover_type( code,
                                  name,
                                  exfi_id
                                )
                            VALUES (i_code,
                                    i_name,
                                    i_excel_id                              
                            );



  return 0;

END;
$function$
;