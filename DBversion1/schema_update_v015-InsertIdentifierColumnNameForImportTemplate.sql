
-- MT20210302

DO
$$
BEGIN
	PERFORM system_column_name FROM niva.predef_col
	WHERE system_column_name='identifier';
	IF NOT FOUND THEN
		INSERT INTO niva.predef_col VALUES (206, 'Parcel Identifier', NULL, NULL, NULL, NULL, 1, 'identifier');
	END IF;
END
$$
