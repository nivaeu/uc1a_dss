ALTER TABLE niva.ec_cult_detail ADD acti_id int4 NULL;
COMMENT ON COLUMN niva.ec_cult_detail.acti_id IS 'Fk to Activity';

ALTER TABLE niva.ec_cult_detail_copy ADD acti_id int4 NULL;
COMMENT ON COLUMN niva.ec_cult_detail.acti_id IS 'Fk to Activity';



ALTER TABLE niva.ec_cult_detail ADD CONSTRAINT eccd_acti_id_fk FOREIGN KEY (acti_id) REFERENCES niva.activity(acti_id);

ALTER TABLE niva.ec_cult_detail_copy ADD CONSTRAINT ecdc_acti_id_fk FOREIGN KEY (acti_id) REFERENCES niva.activity(acti_id);


alter table niva.decision_making drop column ecgc_id;

alter table niva.ec_group_copy add column dema_id int4 ;

alter table niva.ec_group_copy add CONSTRAINT ecgc_dema_id_fk FOREIGN KEY (dema_id) references niva.decision_making;