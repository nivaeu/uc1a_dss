
-- DROP SCHEMA subscription;

CREATE SCHEMA subscription;

-- DROP SCHEMA subscription_log;

CREATE SCHEMA subscription_log;

-- DROP SCHEMA user_registration;

CREATE SCHEMA user_registration;

-- DROP SCHEMA user_registration_log;

CREATE SCHEMA user_registration_log;

-- DROP SCHEMA ws_responses;

CREATE SCHEMA ws_responses;


-- DROP SEQUENCE subscription.sprm_id_seq;

CREATE SEQUENCE subscription.sprm_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2140000000
	START 5;


-- Drop table

-- DROP TABLE subscription.blob_temp;

CREATE TABLE subscription.blob_temp (
	id serial NOT NULL,
	"data" bytea NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT bltm_pk PRIMARY KEY (id)
);

-- Drop table

-- DROP TABLE subscription.exceptions;

CREATE TABLE subscription.exceptions (
	ex_descr varchar(120) NOT NULL,
	ex_type int4 NOT NULL
);

-- Drop table

-- DROP TABLE subscription.reports;

CREATE TABLE subscription.reports (
	rprt_id serial NOT NULL,
	rprt_code varchar(30) NOT NULL,
	rprt_valid_from timestamp NOT NULL,
	rprt_name varchar(512) NOT NULL,
	rprt_desc varchar(4000) NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT reports_pk PRIMARY KEY (rprt_id),
	CONSTRAINT reports_rprt_code_rprt_valid_from_key UNIQUE (rprt_code, rprt_valid_from)
);

-- Drop table

-- DROP TABLE subscription.ss_docs;

CREATE TABLE subscription.ss_docs (
	doc_id serial NOT NULL,
	doc_description varchar(250) NULL,
	doc_attach_file bytea NULL,
	doc_filename varchar(200) NOT NULL,
	doc_subs_security_class int4 NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT doc_pk PRIMARY KEY (doc_id)
);

-- Drop table

-- DROP TABLE subscription.ss_external_usrmngmnt_dbs;

CREATE TABLE subscription.ss_external_usrmngmnt_dbs (
	xumd_id int4 NOT NULL,
	xumd_code varchar(60) NOT NULL,
	xumd_description varchar(200) NOT NULL,
	xumd_db_name varchar(60) NOT NULL,
	xumd_db_ip varchar(60) NULL,
	xumd_dbtype varchar(30) NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT xumd_pk PRIMARY KEY (xumd_id),
	CONSTRAINT xumd_uk001 UNIQUE (xumd_code)
);

-- Drop table

-- DROP TABLE subscription.ss_module_groups;

CREATE TABLE subscription.ss_module_groups (
	mogr_id serial NOT NULL,
	mogr_name varchar(120) NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT mogr_pk PRIMARY KEY (mogr_id)
);
CREATE UNIQUE INDEX mogr_name_u ON subscription.ss_module_groups USING btree (mogr_name);

-- Drop table

-- DROP TABLE subscription.ss_modules;

CREATE TABLE subscription.ss_modules (
	modu_id serial NOT NULL,
	modu_name varchar(60) NOT NULL,
	modu_isfree bool NOT NULL,
	modu_ulr_suffix varchar(60) NULL,
	modu_description varchar(500) NULL,
	modu_isforclient bool NOT NULL DEFAULT false,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	modu_ghost_user_allowed bool NULL,
	modu_is_ws_responseinfo_enabled bool NULL DEFAULT true,
	CONSTRAINT modu_pk PRIMARY KEY (modu_id),
	CONSTRAINT modu_uk001 UNIQUE (modu_name)
);
CREATE INDEX ss_modu_description_i ON subscription.ss_modules USING btree (modu_description);

-- Drop table

-- DROP TABLE subscription.ss_properties;

CREATE TABLE subscription.ss_properties (
	prop_key varchar(30) NOT NULL,
	prop_value varchar(20000) NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT prpr_pk PRIMARY KEY (prop_key)
);

-- Drop table

-- DROP TABLE subscription.ss_role_statuses;

CREATE TABLE subscription.ss_role_statuses (
	rost_id int2 NOT NULL,
	rost_description varchar(60) NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT rost_pk PRIMARY KEY (rost_id)
);

-- Drop table

-- DROP TABLE subscription.ss_security_classes;

CREATE TABLE subscription.ss_security_classes (
	scls_id int4 NOT NULL,
	scls_description varchar(500) NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	scls_public_description varchar(500) NULL,
	scls_code int4 NOT NULL,
	CONSTRAINT scls_pk PRIMARY KEY (scls_id),
	CONSTRAINT scls_uk UNIQUE (scls_code)
);

-- Drop table

-- DROP TABLE subscription.ss_subscriber_categories;

CREATE TABLE subscription.ss_subscriber_categories (
	suca_id int4 NOT NULL,
	suca_name varchar(120) NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT suca_pk PRIMARY KEY (suca_id)
);

-- Drop table

-- DROP TABLE subscription.ss_subscriber_statuses;

CREATE TABLE subscription.ss_subscriber_statuses (
	sust_id int2 NOT NULL,
	sust_description varchar(60) NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT sust_pk PRIMARY KEY (sust_id)
);

-- Drop table

-- DROP TABLE subscription.ss_subscribers_for_update;

CREATE TABLE subscription.ss_subscribers_for_update (
	subs_id int4 NOT NULL,
	subs_short_name varchar(60) NOT NULL,
	subs_legal_name varchar(200) NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_subscribers_for_update_pkey PRIMARY KEY (subs_id)
);

-- Drop table

-- DROP TABLE subscription.ss_subscribers_module_group_status;

CREATE TABLE subscription.ss_subscribers_module_group_status (
	smgs_id int2 NOT NULL,
	smgs_description varchar(60) NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT smgs_pk PRIMARY KEY (smgs_id)
);

-- Drop table

-- DROP TABLE subscription.ss_user_history_types;

CREATE TABLE subscription.ss_user_history_types (
	usht_id int2 NOT NULL,
	usht_code varchar(20) NOT NULL,
	usht_description varchar(160) NOT NULL,
	CONSTRAINT usht_pk PRIMARY KEY (usht_id),
	CONSTRAINT usht_uk UNIQUE (usht_code)
);

-- Drop table

-- DROP TABLE subscription.ss_user_login_attempts;

CREATE TABLE subscription.ss_user_login_attempts (
	usla_id serial NOT NULL,
	usla_dteattempt timestamp NOT NULL,
	usla_ip varchar(120) NULL,
	usla_username varchar(250) NOT NULL,
	usla_app varchar(120) NOT NULL,
	usla_authentication_status varchar(120) NOT NULL,
	usla_status_locks bool NOT NULL,
	CONSTRAINT usla_pk PRIMARY KEY (usla_id)
);
CREATE INDEX usla_username_idx ON subscription.ss_user_login_attempts USING btree (usla_username, usla_status_locks);

-- Drop table

-- DROP TABLE subscription.ss_user_session_logoutcodes;

CREATE TABLE subscription.ss_user_session_logoutcodes (
	uslc_id int4 NOT NULL,
	uslc_name varchar(30) NOT NULL,
	uslc_desc varchar(240) NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT uslc_pk PRIMARY KEY (uslc_id)
);

-- Drop table

-- DROP TABLE subscription.ss_user_sessions_old;

CREATE TABLE subscription.ss_user_sessions_old (
	usrs_id int8 NOT NULL,
	usrs_email varchar(120) NOT NULL,
	usrs_ip varchar(120) NULL,
	usrs_session varchar(500) NULL,
	usrs_ssosession varchar(120) NOT NULL,
	usrs_app varchar(120) NOT NULL,
	usrs_loginsse int4 NOT NULL,
	usrs_logoutsse int4 NULL,
	uslc_id int4 NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	usrs_last_ping_timestamp timestamp NULL,
	CONSTRAINT usrs_old_pk PRIMARY KEY (usrs_id)
);
CREATE INDEX ix_usrs_uslc_fk_ss_user_sessions_old ON subscription.ss_user_sessions_old USING btree (uslc_id);
CREATE INDEX user_sessions_old_usrs_email ON subscription.ss_user_sessions_old USING btree (usrs_email);
CREATE INDEX user_sessions_old_usrs_email_session_app ON subscription.ss_user_sessions_old USING btree (usrs_app, usrs_email, usrs_session);
CREATE INDEX user_sessions_old_usrs_session ON subscription.ss_user_sessions_old USING btree (usrs_session);
CREATE INDEX user_sessions_old_usrs_ssosession ON subscription.ss_user_sessions_old USING btree (usrs_ssosession);
CREATE INDEX user_sessions_old_usrs_ssosession_app_u ON subscription.ss_user_sessions_old USING btree (usrs_ssosession, usrs_app);
CREATE INDEX usrs_old_loginsse_i ON subscription.ss_user_sessions_old USING btree (usrs_loginsse, usrs_app);
CREATE INDEX usrs_old_loginsse_ts_i ON subscription.ss_user_sessions_old USING btree (to_timestamp((usrs_loginsse)::double precision), usrs_app);

-- Drop table

-- DROP TABLE subscription.ss_user_statuses;

CREATE TABLE subscription.ss_user_statuses (
	usst_id int2 NOT NULL,
	usst_description varchar(60) NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT usst_pk PRIMARY KEY (usst_id)
);

-- Drop table

-- DROP TABLE subscription.ss_users_inactivation_batch;

CREATE TABLE subscription.ss_users_inactivation_batch (
	usin_id serial NOT NULL,
	usin_result text NULL,
	usin_users_inactivated int4 NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT usin_pk PRIMARY KEY (usin_id)
);

-- Drop table

-- DROP TABLE subscription.system_params;

CREATE TABLE subscription.system_params (
	sprm_id numeric(12) NOT NULL DEFAULT nextval('subscription.sprm_id_seq'::regclass),
	sprm_code varchar(60) NULL,
	sprm_value varchar(200) NULL,
	CONSTRAINT sprm_code_uk UNIQUE (sprm_code),
	CONSTRAINT sprm_pk PRIMARY KEY (sprm_id)
);

-- Drop table

-- DROP TABLE subscription.ss_grouping_modules;

CREATE TABLE subscription.ss_grouping_modules (
	grmo_id serial NOT NULL,
	mogr_id int4 NOT NULL,
	modu_id int4 NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT grmo_pk PRIMARY KEY (grmo_id),
	CONSTRAINT modu_grmo_fk FOREIGN KEY (modu_id) REFERENCES subscription.ss_modules(modu_id),
	CONSTRAINT mogr_grmo_fk FOREIGN KEY (mogr_id) REFERENCES subscription.ss_module_groups(mogr_id)
);
CREATE INDEX ix_modu_grmo_fk_ss_grouping_modules ON subscription.ss_grouping_modules USING btree (modu_id);
CREATE INDEX ix_mogr_grmo_fk_ss_grouping_modules ON subscription.ss_grouping_modules USING btree (mogr_id);
CREATE UNIQUE INDEX role_mogr_id_modu_id_u ON subscription.ss_grouping_modules USING btree (mogr_id, modu_id);

-- Drop table

-- DROP TABLE subscription.ss_module_lock_price;

CREATE TABLE subscription.ss_module_lock_price (
	molp_id serial NOT NULL,
	modu_id int4 NOT NULL,
	molp_remarks text NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT molp_pk PRIMARY KEY (molp_id),
	CONSTRAINT modu_mopl_fk FOREIGN KEY (modu_id) REFERENCES subscription.ss_modules(modu_id)
);
CREATE UNIQUE INDEX modu_id_uk ON subscription.ss_module_lock_price USING btree (modu_id);

-- Drop table

-- DROP TABLE subscription.ss_module_packs;

CREATE TABLE subscription.ss_module_packs (
	mopa_id serial NOT NULL,
	modu_id int4 NOT NULL,
	mopa_code varchar(100) NOT NULL,
	mopa_description varchar(300) NOT NULL,
	mopa_remarks text NULL,
	mopa_is_active bool NOT NULL DEFAULT true,
	mopa_validity_period_type int4 NOT NULL DEFAULT 1,
	mopa_validity_months int4 NOT NULL DEFAULT 0,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	mopa_is_forclient bool NULL DEFAULT true,
	CONSTRAINT mopa_pk PRIMARY KEY (mopa_id),
	CONSTRAINT modu_mopl_fk FOREIGN KEY (modu_id) REFERENCES subscription.ss_modules(modu_id)
);
CREATE UNIQUE INDEX modu_id_id_u ON subscription.ss_module_packs USING btree (modu_id, mopa_id);
CREATE UNIQUE INDEX mopa_code_u ON subscription.ss_module_packs USING btree (modu_id, mopa_code);

-- Drop table

-- DROP TABLE subscription.ss_module_packs_prices;

CREATE TABLE subscription.ss_module_packs_prices (
	mopp_id serial NOT NULL,
	mopa_id int4 NOT NULL,
	mopp_effective_date timestamp NOT NULL,
	mopp_price numeric(14,2) NOT NULL,
	mopp_subs_percent numeric(5,2) NOT NULL,
	mopp_charge_units numeric(14,2) NOT NULL,
	modu_id int4 NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	mopp_is_special bool NOT NULL DEFAULT false,
	CONSTRAINT mopp_pk PRIMARY KEY (mopp_id),
	CONSTRAINT modu_mopp_fk FOREIGN KEY (modu_id) REFERENCES subscription.ss_modules(modu_id),
	CONSTRAINT modu_mopp_fk1 FOREIGN KEY (modu_id, mopa_id) REFERENCES subscription.ss_module_packs(modu_id, mopa_id),
	CONSTRAINT mopa_mopp_fk FOREIGN KEY (mopa_id) REFERENCES subscription.ss_module_packs(mopa_id)
);
CREATE UNIQUE INDEX modu_id_mopp_id_u ON subscription.ss_module_packs_prices USING btree (modu_id, mopp_id);
CREATE UNIQUE INDEX mopa_id_mopp_id_u ON subscription.ss_module_packs_prices USING btree (mopp_id, mopa_id);

-- Drop table

-- DROP TABLE subscription.ss_packs_extras;

CREATE TABLE subscription.ss_packs_extras (
	paex_id serial NOT NULL,
	mopa_id int4 NOT NULL,
	paex_effective_date timestamp NOT NULL,
	paex_is_active bool NOT NULL DEFAULT true,
	paex_rounding_step int4 NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT paex_pk PRIMARY KEY (paex_id),
	CONSTRAINT mopa_paex_fk FOREIGN KEY (mopa_id) REFERENCES subscription.ss_module_packs(mopa_id)
);
CREATE UNIQUE INDEX mopa_id_effective_date_u ON subscription.ss_packs_extras USING btree (mopa_id, paex_effective_date);

-- Drop table

-- DROP TABLE subscription.ss_packs_extras_prices;

CREATE TABLE subscription.ss_packs_extras_prices (
	paep_id serial NOT NULL,
	paex_id int4 NOT NULL,
	paep_charge_units_until numeric(14,2) NOT NULL,
	paep_charge_units_price numeric(14,2) NOT NULL,
	paep_subs_charge_percent numeric(5,2) NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT paep_pk PRIMARY KEY (paep_id),
	CONSTRAINT paex_paep_fk FOREIGN KEY (paex_id) REFERENCES subscription.ss_packs_extras(paex_id)
);
CREATE UNIQUE INDEX paex_id_charge_units_until_u ON subscription.ss_packs_extras_prices USING btree (paex_id, paep_charge_units_until);

-- Drop table

-- DROP TABLE subscription.ss_security_classes_valid_combinations;

CREATE TABLE subscription.ss_security_classes_valid_combinations (
	scvc_id int4 NOT NULL,
	scls_id int4 NOT NULL,
	scls_id_comb int4 NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT scvc_cc CHECK ((scls_id < scls_id_comb)),
	CONSTRAINT scvc_pk PRIMARY KEY (scvc_id),
	CONSTRAINT scvc_uk UNIQUE (scls_id, scls_id_comb),
	CONSTRAINT scvc_sscl_comb_fk FOREIGN KEY (scls_id_comb) REFERENCES subscription.ss_security_classes(scls_id),
	CONSTRAINT scvc_sscl_fk FOREIGN KEY (scls_id) REFERENCES subscription.ss_security_classes(scls_id)
);
CREATE INDEX scvc_sscl_comb_fk_i ON subscription.ss_security_classes_valid_combinations USING btree (scls_id_comb);
CREATE INDEX scvc_sscl_fk_i ON subscription.ss_security_classes_valid_combinations USING btree (scls_id);

-- Drop table

-- DROP TABLE subscription.ss_system_privileges;

CREATE TABLE subscription.ss_system_privileges (
	sypr_id serial NOT NULL,
	modu_id int4 NOT NULL,
	sypr_name varchar(500) NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT sypr_pk PRIMARY KEY (sypr_id),
	CONSTRAINT sypr_uc2 UNIQUE (sypr_id, modu_id),
	CONSTRAINT modu_sype_fk FOREIGN KEY (modu_id) REFERENCES subscription.ss_modules(modu_id)
);
CREATE UNIQUE INDEX modu_id_sypr_name_u ON subscription.ss_system_privileges USING btree (modu_id, sypr_name);

-- Drop table

-- DROP TABLE subscription.ss_system_privileges_security_classes;

CREATE TABLE subscription.ss_system_privileges_security_classes (
	spsc_id serial NOT NULL,
	sypr_id int4 NOT NULL,
	scls_id int4 NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT spsc_pk PRIMARY KEY (spsc_id),
	CONSTRAINT scls_spsc_fk FOREIGN KEY (scls_id) REFERENCES subscription.ss_security_classes(scls_id),
	CONSTRAINT syro_spsc_fk FOREIGN KEY (sypr_id) REFERENCES subscription.ss_system_privileges(sypr_id)
);
CREATE INDEX ss_system_privileges_security_classes_index1 ON subscription.ss_system_privileges_security_classes USING btree (scls_id);
CREATE INDEX ss_system_privileges_security_classes_index2 ON subscription.ss_system_privileges_security_classes USING btree (sypr_id);
CREATE UNIQUE INDEX ss_system_privileges_security_classes_unique_index ON subscription.ss_system_privileges_security_classes USING btree (sypr_id, scls_id);

-- Drop table

-- DROP TABLE subscription.ss_system_roles;

CREATE TABLE subscription.ss_system_roles (
	syro_id serial NOT NULL,
	syro_name varchar(120) NOT NULL,
	syro_description varchar(500) NULL,
	syro_is_multipage_role bool NOT NULL DEFAULT false,
	modu_id int4 NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_system_roles_ui UNIQUE (syro_name, modu_id),
	CONSTRAINT syro_pk PRIMARY KEY (syro_id),
	CONSTRAINT syro_uc2 UNIQUE (syro_id, modu_id),
	CONSTRAINT ss_modu_syro_fk FOREIGN KEY (modu_id) REFERENCES subscription.ss_modules(modu_id)
);
CREATE INDEX ss_modu_syro_fk_i ON subscription.ss_system_roles USING btree (modu_id);
CREATE INDEX ss_syro_description_i ON subscription.ss_system_roles USING btree (syro_description);

-- Drop table

-- DROP TABLE subscription.ss_system_roles_privileges;

CREATE TABLE subscription.ss_system_roles_privileges (
	syrp_id serial NOT NULL,
	syro_id int4 NOT NULL,
	sypr_id int4 NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	modu_id int4 NOT NULL,
	CONSTRAINT syrp_pk PRIMARY KEY (syrp_id),
	CONSTRAINT sypr_syrp_fk FOREIGN KEY (sypr_id) REFERENCES subscription.ss_system_privileges(sypr_id),
	CONSTRAINT sypr_syrp_fk2 FOREIGN KEY (sypr_id, modu_id) REFERENCES subscription.ss_system_privileges(sypr_id, modu_id),
	CONSTRAINT syro_syrp_fk2 FOREIGN KEY (syro_id, modu_id) REFERENCES subscription.ss_system_roles(syro_id, modu_id),
	CONSTRAINT syro_syrp_kf FOREIGN KEY (syro_id) REFERENCES subscription.ss_system_roles(syro_id)
);
CREATE INDEX ix_sypr_syrp_fk_ss_system_roles_privileges ON subscription.ss_system_roles_privileges USING btree (sypr_id);
CREATE INDEX ix_syro_syrp_kf_ss_system_roles_privileges ON subscription.ss_system_roles_privileges USING btree (syro_id);
CREATE UNIQUE INDEX role_syro_id_sypr_id_u ON subscription.ss_system_roles_privileges USING btree (syro_id, sypr_id);
CREATE INDEX sypr_syrp_fk2_i ON subscription.ss_system_roles_privileges USING btree (sypr_id, modu_id);
CREATE INDEX syro_syrp_fk2_i ON subscription.ss_system_roles_privileges USING btree (syro_id, modu_id);

-- Drop table

-- DROP TABLE subscription.ss_user_categories;

CREATE TABLE subscription.ss_user_categories (
	usca_id int4 NOT NULL,
	usca_parent_id int4 NULL,
	usca_left_id int4 NULL,
	usca_right_id int4 NULL,
	usca_name varchar(120) NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT usca_pk PRIMARY KEY (usca_id),
	CONSTRAINT usca_fk FOREIGN KEY (usca_parent_id) REFERENCES subscription.ss_user_categories(usca_id)
);
CREATE INDEX ix_usca_fk_ss_user_categories ON subscription.ss_user_categories USING btree (usca_parent_id);
CREATE UNIQUE INDEX usca_left_id_right_id_u ON subscription.ss_user_categories USING btree (usca_left_id, usca_right_id);

-- Drop table

-- DROP TABLE subscription.ss_user_sessions;

CREATE TABLE subscription.ss_user_sessions (
	usrs_id bigserial NOT NULL,
	usrs_email varchar(120) NOT NULL,
	usrs_ip varchar(120) NULL,
	usrs_session varchar(500) NULL,
	usrs_ssosession varchar(120) NOT NULL,
	usrs_app varchar(120) NOT NULL,
	usrs_loginsse int4 NOT NULL,
	usrs_logoutsse int4 NULL,
	uslc_id int4 NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	usrs_last_ping_timestamp timestamp NULL,
	CONSTRAINT usrs_pk PRIMARY KEY (usrs_id),
	CONSTRAINT usrs_uslc_fk FOREIGN KEY (uslc_id) REFERENCES subscription.ss_user_session_logoutcodes(uslc_id)
);
CREATE INDEX ix_usrs_uslc_fk_ss_user_sessions ON subscription.ss_user_sessions USING btree (uslc_id);
CREATE INDEX user_sessions_usrs_email ON subscription.ss_user_sessions USING btree (usrs_email);
CREATE INDEX user_sessions_usrs_email_session_app ON subscription.ss_user_sessions USING btree (usrs_app, usrs_email, usrs_session);
CREATE INDEX user_sessions_usrs_session ON subscription.ss_user_sessions USING btree (usrs_session);
CREATE INDEX user_sessions_usrs_ssosession ON subscription.ss_user_sessions USING btree (usrs_ssosession);
CREATE INDEX user_sessions_usrs_ssosession_app_u ON subscription.ss_user_sessions USING btree (usrs_ssosession, usrs_app);
CREATE INDEX usrs_loginsse_ts_i ON subscription.ss_user_sessions USING btree (to_timestamp((usrs_loginsse)::double precision), usrs_app);

-- Drop table

-- DROP TABLE subscription.ss_user_session_variables;

CREATE TABLE subscription.ss_user_session_variables (
	usvr_id bigserial NOT NULL,
	usrs_id int8 NOT NULL,
	usvr_key varchar(160) NOT NULL,
	usvr_value varchar(1000) NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ustk_token_uc UNIQUE (usrs_id, usvr_key),
	CONSTRAINT usvr_pk PRIMARY KEY (usvr_id),
	CONSTRAINT usvr_usrs_fk FOREIGN KEY (usrs_id) REFERENCES subscription.ss_user_sessions(usrs_id)
);
CREATE INDEX usvr_usrs_fk_i ON subscription.ss_user_session_variables USING btree (usrs_id);

-- Drop table

-- DROP TABLE subscription.ss_roles;

CREATE TABLE subscription.ss_roles (
	role_id serial NOT NULL,
	subs_id int4 NOT NULL,
	rost_id int2 NULL,
	role_caption varchar(120) NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	role_created_by_system bool NULL DEFAULT false,
	CONSTRAINT role_pk PRIMARY KEY (role_id),
	CONSTRAINT role_subs_uc UNIQUE (role_id, subs_id)
);
CREATE INDEX ix_rost_role_fk_ss_roles ON subscription.ss_roles USING btree (rost_id);
CREATE INDEX ix_subs_role_fk_ss_roles ON subscription.ss_roles USING btree (subs_id);
CREATE INDEX role_caption_i ON subscription.ss_roles USING btree (role_caption);
CREATE UNIQUE INDEX role_subs_id_description_u ON subscription.ss_roles USING btree (subs_id, role_caption);

-- Drop table

-- DROP TABLE subscription.ss_roles_system_roles;

CREATE TABLE subscription.ss_roles_system_roles (
	rosr_id serial NOT NULL,
	role_id int4 NOT NULL,
	syro_id int4 NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT rosr_pk PRIMARY KEY (rosr_id)
);
CREATE INDEX ix_relationship10_ss_roles_system_roles ON subscription.ss_roles_system_roles USING btree (syro_id);
CREATE INDEX ix_relationship9_ss_roles_system_roles ON subscription.ss_roles_system_roles USING btree (role_id);
CREATE UNIQUE INDEX role_role_id_syro_id_u ON subscription.ss_roles_system_roles USING btree (role_id, syro_id);

-- Drop table

-- DROP TABLE subscription.ss_subscriber_departments;

CREATE TABLE subscription.ss_subscriber_departments (
	sude_id serial NOT NULL,
	subs_id int4 NOT NULL,
	sude_code varchar(15) NOT NULL,
	sude_description varchar(250) NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT sude_pk PRIMARY KEY (sude_id),
	CONSTRAINT sude_subs_uk UNIQUE (sude_id, subs_id),
	CONSTRAINT sude_uk UNIQUE (subs_id, sude_code)
);

-- Drop table

-- DROP TABLE subscription.ss_subscriber_external_usrmngmnt_dbs;

CREATE TABLE subscription.ss_subscriber_external_usrmngmnt_dbs (
	sxud_id serial NOT NULL,
	xumd_id int4 NOT NULL,
	subs_id int4 NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT sxud_pk PRIMARY KEY (sxud_id),
	CONSTRAINT sxud_subs_uk UNIQUE (sxud_id, subs_id),
	CONSTRAINT sxud_uk UNIQUE (subs_id, xumd_id)
);
CREATE INDEX subs_sxud_fk ON subscription.ss_subscriber_external_usrmngmnt_dbs USING btree (subs_id);
CREATE INDEX xumd_sxud_fk_i ON subscription.ss_subscriber_external_usrmngmnt_dbs USING btree (xumd_id);

-- Drop table

-- DROP TABLE subscription.ss_subscriber_invitations;

CREATE TABLE subscription.ss_subscriber_invitations (
	suin_id serial NOT NULL,
	subs_id int4 NOT NULL,
	suin_guid varchar(120) NOT NULL,
	suin_email varchar(200) NOT NULL,
	suin_created_when timestamptz NOT NULL DEFAULT now(),
	suin_valid_until timestamptz NOT NULL,
	suin_consumed_when timestamptz NULL,
	suin_accepted_when timestamptz NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT suin_guid_uk UNIQUE (suin_guid),
	CONSTRAINT suin_pk PRIMARY KEY (suin_id)
);
CREATE INDEX ix_subs_suin_id_ss_subscriber_invitations ON subscription.ss_subscriber_invitations USING btree (subs_id);

-- Drop table

-- DROP TABLE subscription.ss_subscriber_module_packs_prices;

CREATE TABLE subscription.ss_subscriber_module_packs_prices (
	smpp_id serial NOT NULL,
	subs_id int4 NOT NULL,
	mopp_id int4 NOT NULL,
	smpp_remarks varchar(500) NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	row_version int4 NOT NULL DEFAULT 0,
	smpp_is_active bool NOT NULL DEFAULT true,
	CONSTRAINT sump_pk PRIMARY KEY (smpp_id)
);
CREATE UNIQUE INDEX smpp_mopp_id_subs_id_u ON subscription.ss_subscriber_module_packs_prices USING btree (mopp_id, subs_id);

-- Drop table

-- DROP TABLE subscription.ss_subscribers;

CREATE TABLE subscription.ss_subscribers (
	subs_id serial NOT NULL,
	sust_id int2 NOT NULL,
	suca_id int4 NOT NULL,
	subs_short_name varchar(60) NOT NULL,
	subs_vat varchar(20) NOT NULL,
	subs_legal_name varchar(200) NOT NULL,
	subs_tax_office varchar(60) NOT NULL,
	subs_occupation varchar(200) NOT NULL,
	subs_phone varchar(20) NOT NULL,
	subs_fax varchar(20) NULL,
	subs_address varchar(120) NOT NULL,
	subs_address_number varchar(10) NOT NULL,
	subs_postal_code varchar(10) NOT NULL,
	subs_city varchar(60) NOT NULL,
	subs_prefecture varchar(60) NULL,
	subs_country varchar(60) NULL,
	subs_email varchar(120) NULL,
	subs_web_site varchar(120) NULL,
	subs_database varchar(500) NULL,
	subs_application_url varchar(60) NULL,
	subs_invoicing_method int2 NOT NULL DEFAULT 2,
	suvc_id int4 NOT NULL,
	subs_company_vat varchar(9) NOT NULL,
	subs_billing_comp_id int4 NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	subs_user_auto_inactivate bool NOT NULL DEFAULT false,
	subs_login_policy int4 NULL,
	subs_legal_name_short varchar(60) NULL,
	CONSTRAINT ss_vat_category_check CHECK (((subs_invoicing_method >= 1) AND (subs_invoicing_method <= 2))),
	CONSTRAINT subs_login_policy_cc CHECK ((subs_login_policy = ANY (ARRAY[0, 1]))),
	CONSTRAINT subs_pk PRIMARY KEY (subs_id),
	CONSTRAINT subs_uk UNIQUE (subs_short_name)
);
CREATE INDEX ix_suca_subs_fk_ss_subscribers ON subscription.ss_subscribers USING btree (suca_id);
CREATE INDEX ix_sust_subs_fk_ss_subscribers ON subscription.ss_subscribers USING btree (sust_id);
CREATE INDEX subs_id_suca_id ON subscription.ss_subscribers USING btree (subs_id, suca_id);
CREATE UNIQUE INDEX subs_vat_u ON subscription.ss_subscribers USING btree (subs_vat);

-- Drop table

-- DROP TABLE subscription.ss_subscribers_module_groups;

CREATE TABLE subscription.ss_subscribers_module_groups (
	sumg_id serial NOT NULL,
	subs_id int4 NOT NULL,
	mogr_id int4 NOT NULL,
	smgs_id int2 NOT NULL,
	sumg_valid_from timestamptz NOT NULL DEFAULT now(),
	sumg_valid_until timestamptz NOT NULL,
	sumg_max_users int2 NOT NULL DEFAULT 0,
	sumg_max_customers int2 NOT NULL DEFAULT 0,
	sumg_max_resources int2 NOT NULL DEFAULT 0,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT sumg_pk PRIMARY KEY (sumg_id),
	CONSTRAINT sumg_uk UNIQUE (subs_id, mogr_id)
);
CREATE INDEX ix_mogr_sumo_fk_ss_subscribers_module_groups ON subscription.ss_subscribers_module_groups USING btree (mogr_id);
CREATE INDEX ix_most_sumo_fk_ss_subscribers_module_groups ON subscription.ss_subscribers_module_groups USING btree (smgs_id);
CREATE INDEX ix_subs_sumo_fk_ss_subscribers_module_groups ON subscription.ss_subscribers_module_groups USING btree (subs_id);
CREATE INDEX subs_id_mogr_id_smgs_id ON subscription.ss_subscribers_module_groups USING btree (subs_id, mogr_id, smgs_id);

-- Drop table

-- DROP TABLE subscription.ss_subscribers_security_classes;

CREATE TABLE subscription.ss_subscribers_security_classes (
	sscl_id serial NOT NULL,
	subs_id int4 NOT NULL,
	scls_id int4 NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT sscl_pk PRIMARY KEY (sscl_id)
);
CREATE INDEX ss_subscribers_security_classes_index1 ON subscription.ss_subscribers_security_classes USING btree (scls_id);
CREATE INDEX ss_subscribers_security_classes_index2 ON subscription.ss_subscribers_security_classes USING btree (subs_id);
CREATE UNIQUE INDEX ss_subscribers_security_classes_unique_index ON subscription.ss_subscribers_security_classes USING btree (subs_id, scls_id);

-- Drop table

-- DROP TABLE subscription.ss_user_external_usrmngmnt_user;

CREATE TABLE subscription.ss_user_external_usrmngmnt_user (
	uxuu_id serial NOT NULL,
	sxud_id int4 NOT NULL,
	subs_id int4 NOT NULL,
	user_id int4 NOT NULL,
	uxuu_external_username varchar(120) NOT NULL,
	uxuu_external_subs_code varchar(60) NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT uxuu_pk PRIMARY KEY (uxuu_id),
	CONSTRAINT uxuu_sxud_external_username_uk UNIQUE (uxuu_external_username, sxud_id),
	CONSTRAINT uxuu_sxud_user_uk UNIQUE (user_id, sxud_id)
);
CREATE INDEX subs_uxuu_fk_i ON subscription.ss_user_external_usrmngmnt_user USING btree (subs_id);
CREATE INDEX sxud_subs_uxuu_fk_i ON subscription.ss_user_external_usrmngmnt_user USING btree (sxud_id, subs_id);
CREATE INDEX sxud_uxuu_fk_i ON subscription.ss_user_external_usrmngmnt_user USING btree (sxud_id);
CREATE INDEX user_subs_uxuu_fk_i ON subscription.ss_user_external_usrmngmnt_user USING btree (user_id, subs_id);
CREATE INDEX user_uxuu_fk_i ON subscription.ss_user_external_usrmngmnt_user USING btree (user_id);

-- Drop table

-- DROP TABLE subscription.ss_user_history;

CREATE TABLE subscription.ss_user_history (
	ushi_id serial NOT NULL,
	usht_id int2 NOT NULL,
	user_id int4 NOT NULL,
	ushi_old_value varchar(1000) NULL,
	ushi_new_value varchar(1000) NULL,
	ushi_date timestamp NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ushi_pk PRIMARY KEY (ushi_id)
);
CREATE INDEX user_ushi_fk_i ON subscription.ss_user_history USING btree (user_id);
CREATE INDEX usht_ushi_fk_i ON subscription.ss_user_history USING btree (usht_id);

-- Drop table

-- DROP TABLE subscription.ss_user_history_dtl;

CREATE TABLE subscription.ss_user_history_dtl (
	ushd_id serial NOT NULL,
	ushi_id int4 NOT NULL,
	usht_id int2 NOT NULL,
	ushi_old_value varchar(1000) NULL,
	ushi_new_value varchar(1000) NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ushd_pk PRIMARY KEY (ushd_id)
);
CREATE INDEX ushi_ushd_fk_i ON subscription.ss_user_history_dtl USING btree (ushi_id);
CREATE INDEX usht_ushd_fk_i ON subscription.ss_user_history_dtl USING btree (usht_id);

-- Drop table

-- DROP TABLE subscription.ss_user_messages;

CREATE TABLE subscription.ss_user_messages (
	umsg_id serial NOT NULL,
	user_id int4 NULL,
	subs_id int4 NULL,
	modu_id int4 NULL,
	umsg_valid_from timestamp NOT NULL DEFAULT now(),
	umsg_valid_until timestamp NOT NULL DEFAULT now(),
	umsg_message varchar(500) NOT NULL,
	umsg_message_type int4 NOT NULL DEFAULT 3,
	umsg_message_location int4 NOT NULL DEFAULT 3,
	umsg_message_is_enabled bool NOT NULL DEFAULT false,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_user_messages_message_location_check CHECK ((umsg_message_location = ANY (ARRAY[1, 2, 3]))),
	CONSTRAINT ss_user_messages_message_type_check CHECK ((umsg_message_type = ANY (ARRAY[1, 2, 3]))),
	CONSTRAINT umsg_pk PRIMARY KEY (umsg_id)
);
CREATE INDEX ss_user_messages_modu_id_fki ON subscription.ss_user_messages USING btree (modu_id);
CREATE INDEX ss_user_messages_subs_id_fki ON subscription.ss_user_messages USING btree (subs_id);
CREATE INDEX ss_user_messages_user_id_fki ON subscription.ss_user_messages USING btree (user_id);

-- Drop table

-- DROP TABLE subscription.ss_user_modules;

CREATE TABLE subscription.ss_user_modules (
	usmo_id serial NOT NULL,
	user_id int4 NOT NULL,
	modu_id int4 NOT NULL,
	usmo_read_mode bool NOT NULL DEFAULT false,
	usmo_valid_until timestamp NULL,
	usmo_from_subscriber int4 NULL,
	mopp_id int4 NULL,
	user_registered_from_id int4 NULL,
	usmo_registration_date timestamp NULL,
	usmo_is_renewal bool NOT NULL DEFAULT false,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT usmo_pk PRIMARY KEY (usmo_id)
);
CREATE INDEX ix_modu_usmo_fk_ss_user_modules ON subscription.ss_user_modules USING btree (modu_id);
CREATE INDEX ix_user_usmo_fk_ss_user_modules ON subscription.ss_user_modules USING btree (user_id);
CREATE INDEX subs_usmo_fk_i ON subscription.ss_user_modules USING btree (usmo_from_subscriber);
CREATE INDEX user_usmo_fk1_i ON subscription.ss_user_modules USING btree (user_registered_from_id);
CREATE UNIQUE INDEX usmo_user_id_modu_id_u ON subscription.ss_user_modules USING btree (user_id, modu_id);

-- Drop table

-- DROP TABLE subscription.ss_user_pwd_history;

CREATE TABLE subscription.ss_user_pwd_history (
	usph_id serial NOT NULL,
	user_id int4 NOT NULL,
	usph_password varchar(120) NOT NULL,
	usph_dtechanged timestamp NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	usph_ip varchar(120) NULL,
	usph_pwd_changed_by_admin bool NULL,
	CONSTRAINT usph_pk PRIMARY KEY (usph_id)
);
CREATE INDEX user_usph_fk_i ON subscription.ss_user_pwd_history USING btree (user_id);

-- Drop table

-- DROP TABLE subscription.ss_user_user_categories;

CREATE TABLE subscription.ss_user_user_categories (
	usuc_id serial NOT NULL,
	user_id int4 NOT NULL,
	usca_id int4 NOT NULL,
	usuc_order int4 NULL DEFAULT 0,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT usuc_pk PRIMARY KEY (usuc_id)
);
CREATE INDEX ix_usca_usuc_fk_ss_user_user_categories ON subscription.ss_user_user_categories USING btree (usca_id);
CREATE INDEX ix_user_usuc_fk_ss_user_user_categories ON subscription.ss_user_user_categories USING btree (user_id);
CREATE UNIQUE INDEX usuc_user_id_usca_id_u ON subscription.ss_user_user_categories USING btree (user_id, usca_id);

-- Drop table

-- DROP TABLE subscription.ss_users;

CREATE TABLE subscription.ss_users (
	user_id serial NOT NULL,
	usst_id int2 NOT NULL,
	subs_id int4 NOT NULL,
	user_is_administrator bool NOT NULL DEFAULT false,
	user_email varchar(120) NOT NULL,
	user_password varchar(120) NOT NULL,
	user_password_salt varchar(60) NOT NULL,
	user_firstname varchar(60) NOT NULL,
	user_surname varchar(60) NOT NULL,
	user_nickname varchar(120) NOT NULL,
	user_vat varchar(20) NULL,
	user_phone varchar(20) NULL,
	user_phone_cell varchar(20) NULL,
	user_address varchar(120) NULL,
	user_address_number varchar(10) NULL,
	user_postal_code varchar(10) NULL,
	user_city varchar(60) NULL,
	user_prefecture varchar(60) NULL,
	user_country varchar(60) NULL,
	user_comments varchar(250) NULL,
	user_portal_id int4 NULL,
	user_registered_from_id int4 NULL,
	user_agree_for_transmission bool NULL DEFAULT false,
	user_registration_date timestamptz NOT NULL DEFAULT now(),
	user_last_login_date timestamptz NULL,
	user_invoice_type int4 NULL DEFAULT 1,
	user_identity_number varchar(10) NULL,
	subs_registered_from_id int4 NULL,
	user_payment_method int4 NULL DEFAULT 1,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	user_active_email varchar(120) NULL,
	user_visible_type int4 NULL DEFAULT 0,
	user_fathername varchar(120) NULL,
	user_ghost_user_id int4 NULL,
	user_login_attempts int4 NOT NULL DEFAULT 0,
	user_password_expiration_date timestamp NULL,
	user_password_algorithm int4 NOT NULL DEFAULT 1,
	user_pwd_changed_by_admin bool NULL,
	user_last_activation_date timestamp NULL,
	user_multiple_sessions_flag int4 NULL,
	user_auto_inactivate bool NOT NULL DEFAULT false,
	CONSTRAINT user_id_mail_uc UNIQUE (user_id, user_email, user_active_email),
	CONSTRAINT user_invoice_type_check CHECK (((user_invoice_type >= 1) AND (user_invoice_type <= 2))),
	CONSTRAINT user_password_check CHECK (((user_password)::text <> ''::text)),
	CONSTRAINT user_pk PRIMARY KEY (user_id),
	CONSTRAINT user_subs_uk UNIQUE (user_id, subs_id)
);
CREATE INDEX ix_subs_users_fk_ss_users ON subscription.ss_users USING btree (subs_id);
CREATE INDEX ix_user_portal_id ON subscription.ss_users USING btree (user_portal_id);
CREATE INDEX ix_usst_user_fk_ss_users ON subscription.ss_users USING btree (usst_id);
CREATE UNIQUE INDEX user_email_u ON subscription.ss_users USING btree (user_email);
CREATE INDEX user_user_vat_index ON subscription.ss_users USING btree (user_vat);
CREATE INDEX users_registered_from_id_index ON subscription.ss_users USING btree (user_registered_from_id);

-- Drop table

-- DROP TABLE subscription.ss_users_inactivation_batch_dtl;

CREATE TABLE subscription.ss_users_inactivation_batch_dtl (
	usid_id serial NOT NULL,
	usin_id int4 NOT NULL,
	user_id int4 NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT usid_pk PRIMARY KEY (usid_id)
);
CREATE INDEX usin_user_fk_i ON subscription.ss_users_inactivation_batch_dtl USING btree (user_id);
CREATE INDEX usin_usid_fk_i ON subscription.ss_users_inactivation_batch_dtl USING btree (usin_id);

-- Drop table

-- DROP TABLE subscription.ss_users_mapping;

CREATE TABLE subscription.ss_users_mapping (
	usma_id serial NOT NULL,
	usma_platform int4 NOT NULL,
	usma_public_email varchar(120) NOT NULL,
	usma_internal_login_name varchar(120) NOT NULL,
	user_id int4 NULL,
	CONSTRAINT usma_internal_login_name_uk UNIQUE (usma_internal_login_name),
	CONSTRAINT usma_pk PRIMARY KEY (usma_id),
	CONSTRAINT usma_platform_public_email_uk UNIQUE (usma_platform, usma_public_email),
	CONSTRAINT usma_public_email_cc CHECK (((usma_public_email)::text <> (usma_internal_login_name)::text)) NOT VALID
);
CREATE INDEX usma_public_email_i ON subscription.ss_users_mapping USING btree (usma_public_email);
CREATE INDEX usma_user_fk2_i ON subscription.ss_users_mapping USING btree (user_id, usma_public_email, usma_internal_login_name);
CREATE UNIQUE INDEX usma_user_fk_i ON subscription.ss_users_mapping USING btree (user_id);

-- Drop table

-- DROP TABLE subscription.ss_users_roles;

CREATE TABLE subscription.ss_users_roles (
	usro_id serial NOT NULL,
	user_id int4 NOT NULL,
	role_id int4 NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	subs_id int4 NOT NULL,
	CONSTRAINT usro_pk PRIMARY KEY (usro_id)
);
CREATE INDEX ix_role_usro_fk_ss_users_roles ON subscription.ss_users_roles USING btree (role_id);
CREATE INDEX ix_user_usro_fk_ss_users_roles ON subscription.ss_users_roles USING btree (user_id);
CREATE INDEX subs_usro_fk_i ON subscription.ss_users_roles USING btree (subs_id);
CREATE UNIQUE INDEX usro_user_id_role_id_u ON subscription.ss_users_roles USING btree (user_id, role_id);

-- Drop table

-- DROP TABLE subscription.ss_users_subscriber_departments;

CREATE TABLE subscription.ss_users_subscriber_departments (
	ussd_id serial NOT NULL,
	user_id int4 NOT NULL,
	sude_id int4 NOT NULL,
	subs_id int4 NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	ussd_user_is_supervisor bool NOT NULL DEFAULT false,
	CONSTRAINT ussd_pk PRIMARY KEY (ussd_id),
	CONSTRAINT ussd_uk UNIQUE (user_id, sude_id)
);
CREATE INDEX subs_ussd_fk_i ON subscription.ss_users_subscriber_departments USING btree (subs_id);
CREATE INDEX sude_ussd_fk2_i ON subscription.ss_users_subscriber_departments USING btree (sude_id, subs_id);
CREATE INDEX sude_ussd_fk_i ON subscription.ss_users_subscriber_departments USING btree (sude_id);
CREATE INDEX user_ussd_fk2_i ON subscription.ss_users_subscriber_departments USING btree (user_id, subs_id);

ALTER TABLE subscription.ss_roles ADD CONSTRAINT rost_role_fk FOREIGN KEY (rost_id) REFERENCES subscription.ss_role_statuses(rost_id);
ALTER TABLE subscription.ss_roles ADD CONSTRAINT subs_role_fk FOREIGN KEY (subs_id) REFERENCES subscription.ss_subscribers(subs_id);

ALTER TABLE subscription.ss_roles_system_roles ADD CONSTRAINT roles_rosr_fk FOREIGN KEY (role_id) REFERENCES subscription.ss_roles(role_id);
ALTER TABLE subscription.ss_roles_system_roles ADD CONSTRAINT syro_rosr FOREIGN KEY (syro_id) REFERENCES subscription.ss_system_roles(syro_id);

ALTER TABLE subscription.ss_subscriber_departments ADD CONSTRAINT subs_sude_fk FOREIGN KEY (subs_id) REFERENCES subscription.ss_subscribers(subs_id);

ALTER TABLE subscription.ss_subscriber_external_usrmngmnt_dbs ADD CONSTRAINT subs_sxud_fk FOREIGN KEY (subs_id) REFERENCES subscription.ss_subscribers(subs_id);
ALTER TABLE subscription.ss_subscriber_external_usrmngmnt_dbs ADD CONSTRAINT xumd_sxud_fk FOREIGN KEY (xumd_id) REFERENCES subscription.ss_external_usrmngmnt_dbs(xumd_id);

ALTER TABLE subscription.ss_subscriber_invitations ADD CONSTRAINT subs_suin_id FOREIGN KEY (subs_id) REFERENCES subscription.ss_subscribers(subs_id);

ALTER TABLE subscription.ss_subscriber_module_packs_prices ADD CONSTRAINT mopp_sump_fk FOREIGN KEY (mopp_id) REFERENCES subscription.ss_module_packs_prices(mopp_id);
ALTER TABLE subscription.ss_subscriber_module_packs_prices ADD CONSTRAINT subs_sump_fk FOREIGN KEY (subs_id) REFERENCES subscription.ss_subscribers(subs_id);

ALTER TABLE subscription.ss_subscribers ADD CONSTRAINT suca_subs_fk FOREIGN KEY (suca_id) REFERENCES subscription.ss_subscriber_categories(suca_id);
ALTER TABLE subscription.ss_subscribers ADD CONSTRAINT sust_subs_fk FOREIGN KEY (sust_id) REFERENCES subscription.ss_subscriber_statuses(sust_id);


ALTER TABLE subscription.ss_subscribers_module_groups ADD CONSTRAINT mogr_sumo_fk FOREIGN KEY (mogr_id) REFERENCES subscription.ss_module_groups(mogr_id);
ALTER TABLE subscription.ss_subscribers_module_groups ADD CONSTRAINT most_sumo_fk FOREIGN KEY (smgs_id) REFERENCES subscription.ss_subscribers_module_group_status(smgs_id);
ALTER TABLE subscription.ss_subscribers_module_groups ADD CONSTRAINT subs_sumo_fk FOREIGN KEY (subs_id) REFERENCES subscription.ss_subscribers(subs_id);

ALTER TABLE subscription.ss_subscribers_security_classes ADD CONSTRAINT scls_sscl_fk FOREIGN KEY (scls_id) REFERENCES subscription.ss_security_classes(scls_id);
ALTER TABLE subscription.ss_subscribers_security_classes ADD CONSTRAINT syro_sscl_fk FOREIGN KEY (subs_id) REFERENCES subscription.ss_subscribers(subs_id);

ALTER TABLE subscription.ss_user_external_usrmngmnt_user ADD CONSTRAINT subs_uxuu_fk FOREIGN KEY (subs_id) REFERENCES subscription.ss_subscribers(subs_id);
ALTER TABLE subscription.ss_user_external_usrmngmnt_user ADD CONSTRAINT sxud_subs_uxuu_fk FOREIGN KEY (sxud_id, subs_id) REFERENCES subscription.ss_subscriber_external_usrmngmnt_dbs(sxud_id, subs_id);
ALTER TABLE subscription.ss_user_external_usrmngmnt_user ADD CONSTRAINT sxud_uxuu_fk FOREIGN KEY (sxud_id) REFERENCES subscription.ss_subscriber_external_usrmngmnt_dbs(sxud_id);
ALTER TABLE subscription.ss_user_external_usrmngmnt_user ADD CONSTRAINT user_subs_uxuu_fk FOREIGN KEY (user_id, subs_id) REFERENCES subscription.ss_users(user_id, subs_id);
ALTER TABLE subscription.ss_user_external_usrmngmnt_user ADD CONSTRAINT user_uxuu_fk FOREIGN KEY (user_id) REFERENCES subscription.ss_users(user_id);

ALTER TABLE subscription.ss_user_history ADD CONSTRAINT user_ushi_fk FOREIGN KEY (user_id) REFERENCES subscription.ss_users(user_id);
ALTER TABLE subscription.ss_user_history ADD CONSTRAINT usht_ushi_fk FOREIGN KEY (usht_id) REFERENCES subscription.ss_user_history_types(usht_id);

ALTER TABLE subscription.ss_user_history_dtl ADD CONSTRAINT ushi_ushd_fk FOREIGN KEY (ushi_id) REFERENCES subscription.ss_user_history(ushi_id);
ALTER TABLE subscription.ss_user_history_dtl ADD CONSTRAINT usht_ushd_fk FOREIGN KEY (usht_id) REFERENCES subscription.ss_user_history_types(usht_id);

ALTER TABLE subscription.ss_user_messages ADD CONSTRAINT ss_user_messages_modu_id_fk FOREIGN KEY (modu_id) REFERENCES subscription.ss_modules(modu_id);
ALTER TABLE subscription.ss_user_messages ADD CONSTRAINT ss_user_messages_subs_id_fk FOREIGN KEY (subs_id) REFERENCES subscription.ss_subscribers(subs_id);
ALTER TABLE subscription.ss_user_messages ADD CONSTRAINT ss_user_messages_user_id_fk FOREIGN KEY (user_id) REFERENCES subscription.ss_users(user_id);

ALTER TABLE subscription.ss_user_modules ADD CONSTRAINT modu_usmo_fk FOREIGN KEY (modu_id) REFERENCES subscription.ss_modules(modu_id);
ALTER TABLE subscription.ss_user_modules ADD CONSTRAINT mopp_usmo_fk FOREIGN KEY (mopp_id) REFERENCES subscription.ss_module_packs_prices(mopp_id);
ALTER TABLE subscription.ss_user_modules ADD CONSTRAINT mopp_usmo_fk1 FOREIGN KEY (modu_id, mopp_id) REFERENCES subscription.ss_module_packs_prices(modu_id, mopp_id);
ALTER TABLE subscription.ss_user_modules ADD CONSTRAINT subs_usmo_fk FOREIGN KEY (usmo_from_subscriber) REFERENCES subscription.ss_subscribers(subs_id);
ALTER TABLE subscription.ss_user_modules ADD CONSTRAINT user_usmo_fk FOREIGN KEY (user_id) REFERENCES subscription.ss_users(user_id);
ALTER TABLE subscription.ss_user_modules ADD CONSTRAINT user_usmo_fk1 FOREIGN KEY (user_registered_from_id) REFERENCES subscription.ss_users(user_id);

ALTER TABLE subscription.ss_user_pwd_history ADD CONSTRAINT user_usph_fk FOREIGN KEY (user_id) REFERENCES subscription.ss_users(user_id);

ALTER TABLE subscription.ss_user_user_categories ADD CONSTRAINT usca_usuc_fk FOREIGN KEY (usca_id) REFERENCES subscription.ss_user_categories(usca_id);
ALTER TABLE subscription.ss_user_user_categories ADD CONSTRAINT user_usuc_fk FOREIGN KEY (user_id) REFERENCES subscription.ss_users(user_id);

ALTER TABLE subscription.ss_users ADD CONSTRAINT subs_user_fk1 FOREIGN KEY (subs_registered_from_id) REFERENCES subscription.ss_subscribers(subs_id);
ALTER TABLE subscription.ss_users ADD CONSTRAINT subs_users_fk FOREIGN KEY (subs_id) REFERENCES subscription.ss_subscribers(subs_id);
ALTER TABLE subscription.ss_users ADD CONSTRAINT usst_user_fk FOREIGN KEY (usst_id) REFERENCES subscription.ss_user_statuses(usst_id);

ALTER TABLE subscription.ss_users_inactivation_batch_dtl ADD CONSTRAINT usin_user_fk FOREIGN KEY (user_id) REFERENCES subscription.ss_users(user_id);
ALTER TABLE subscription.ss_users_inactivation_batch_dtl ADD CONSTRAINT usin_usid_fk FOREIGN KEY (usin_id) REFERENCES subscription.ss_users_inactivation_batch(usin_id);

ALTER TABLE subscription.ss_users_mapping ADD CONSTRAINT usma_user_fk FOREIGN KEY (user_id) REFERENCES subscription.ss_users(user_id);
ALTER TABLE subscription.ss_users_mapping ADD CONSTRAINT usma_user_fk2 FOREIGN KEY (user_id, usma_public_email, usma_internal_login_name) REFERENCES subscription.ss_users(user_id, user_active_email, user_email);

ALTER TABLE subscription.ss_users_roles ADD CONSTRAINT role_usro_fk FOREIGN KEY (role_id) REFERENCES subscription.ss_roles(role_id);
ALTER TABLE subscription.ss_users_roles ADD CONSTRAINT role_usro_fk2 FOREIGN KEY (role_id, subs_id) REFERENCES subscription.ss_roles(role_id, subs_id);
ALTER TABLE subscription.ss_users_roles ADD CONSTRAINT subs_usro_fk FOREIGN KEY (subs_id) REFERENCES subscription.ss_subscribers(subs_id);
ALTER TABLE subscription.ss_users_roles ADD CONSTRAINT user_usro_fk FOREIGN KEY (user_id) REFERENCES subscription.ss_users(user_id);
ALTER TABLE subscription.ss_users_roles ADD CONSTRAINT user_usro_fk2 FOREIGN KEY (user_id, subs_id) REFERENCES subscription.ss_users(user_id, subs_id);

ALTER TABLE subscription.ss_users_subscriber_departments ADD CONSTRAINT subs_ussd_fk FOREIGN KEY (subs_id) REFERENCES subscription.ss_subscribers(subs_id);
ALTER TABLE subscription.ss_users_subscriber_departments ADD CONSTRAINT sude_ussd_fk FOREIGN KEY (sude_id) REFERENCES subscription.ss_subscriber_departments(sude_id);
ALTER TABLE subscription.ss_users_subscriber_departments ADD CONSTRAINT sude_ussd_fk2 FOREIGN KEY (sude_id, subs_id) REFERENCES subscription.ss_subscriber_departments(sude_id, subs_id);
ALTER TABLE subscription.ss_users_subscriber_departments ADD CONSTRAINT user_ussd_fk FOREIGN KEY (user_id) REFERENCES subscription.ss_users(user_id);
ALTER TABLE subscription.ss_users_subscriber_departments ADD CONSTRAINT user_ussd_fk2 FOREIGN KEY (user_id, subs_id) REFERENCES subscription.ss_users(user_id, subs_id);


-- Drop table

-- DROP TABLE subscription_log.reportslog;

CREATE TABLE subscription_log.reportslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	rprt_id int4 NOT NULL,
	rprt_code varchar(30) NULL,
	rprt_valid_from timestamp NULL,
	rprt_name varchar(512) NULL,
	rprt_desc varchar(4000) NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT reportslog_pkey PRIMARY KEY (rprt_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_external_usrmngmnt_dbslog;

CREATE TABLE subscription_log.ss_external_usrmngmnt_dbslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	xumd_id int4 NOT NULL,
	xumd_code varchar(60) NULL,
	xumd_description varchar(200) NULL,
	xumd_db_name varchar(60) NULL,
	xumd_db_ip varchar(60) NULL,
	xumd_dbtype varchar(30) NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_external_usrmngmnt_dbslog_pkey PRIMARY KEY (xumd_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_grouping_moduleslog;

CREATE TABLE subscription_log.ss_grouping_moduleslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	grmo_id int4 NOT NULL,
	mogr_id int4 NULL,
	modu_id int4 NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_grouping_moduleslog_pkey PRIMARY KEY (grmo_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_module_groupslog;

CREATE TABLE subscription_log.ss_module_groupslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	mogr_id int4 NOT NULL,
	mogr_name varchar(120) NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_module_groupslog_pkey PRIMARY KEY (mogr_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_module_packs_priceslog;

CREATE TABLE subscription_log.ss_module_packs_priceslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	mopp_id int4 NOT NULL,
	mopa_id int4 NULL,
	mopp_effective_date timestamp NULL,
	mopp_price numeric(14,2) NULL,
	mopp_subs_percent numeric(5,2) NULL,
	mopp_charge_units numeric(14,2) NULL,
	modu_id int4 NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_module_packs_priceslog_pkey PRIMARY KEY (mopp_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_module_packslog;

CREATE TABLE subscription_log.ss_module_packslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	mopa_id int4 NOT NULL,
	modu_id int4 NULL,
	mopa_code varchar(100) NULL,
	mopa_description varchar(300) NULL,
	mopa_remarks text NULL,
	mopa_is_active bool NULL,
	mopa_validity_period_type int4 NULL,
	mopa_validity_months int4 NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_module_packslog_pkey PRIMARY KEY (mopa_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_moduleslog;

CREATE TABLE subscription_log.ss_moduleslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	modu_id int4 NOT NULL,
	modu_name varchar(60) NULL,
	modu_isfree bool NULL,
	modu_ulr_suffix varchar(60) NULL,
	modu_description varchar(500) NULL,
	modu_isforclient bool NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	modu_ghost_user_allowed bool NULL,
	modu_is_ws_responseinfo_enabled bool NULL DEFAULT true,
	CONSTRAINT ss_moduleslog_pkey PRIMARY KEY (modu_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_packs_extras_priceslog;

CREATE TABLE subscription_log.ss_packs_extras_priceslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	paep_id int4 NOT NULL,
	paex_id int4 NULL,
	paep_charge_units_until numeric(14,2) NULL,
	paep_charge_units_price numeric(14,2) NULL,
	paep_subs_charge_percent numeric(5,2) NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_packs_extras_priceslog_pkey PRIMARY KEY (paep_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_packs_extraslog;

CREATE TABLE subscription_log.ss_packs_extraslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	paex_id int4 NOT NULL,
	mopa_id int4 NULL,
	paex_effective_date timestamp NULL,
	paex_is_active bool NULL,
	paex_rounding_step int4 NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_packs_extraslog_pkey PRIMARY KEY (paex_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_propertieslog;

CREATE TABLE subscription_log.ss_propertieslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	prop_key varchar(30) NOT NULL,
	prop_value varchar(20000) NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_propertieslog_pkey PRIMARY KEY (prop_key, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_role_statuseslog;

CREATE TABLE subscription_log.ss_role_statuseslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	rost_id int2 NOT NULL,
	rost_description varchar(60) NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_role_statuseslog_pkey PRIMARY KEY (rost_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_roles_system_roleslog;

CREATE TABLE subscription_log.ss_roles_system_roleslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	rosr_id int4 NOT NULL,
	role_id int4 NULL,
	syro_id int4 NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_roles_system_roleslog_pkey PRIMARY KEY (rosr_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_roleslog;

CREATE TABLE subscription_log.ss_roleslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	role_id int4 NOT NULL,
	subs_id int4 NULL,
	rost_id int2 NULL,
	role_caption varchar(120) NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	role_created_by_system bool NULL,
	CONSTRAINT ss_roleslog_pkey PRIMARY KEY (role_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_security_classes_valid_combinationslog;

CREATE TABLE subscription_log.ss_security_classes_valid_combinationslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	scvc_id int4 NOT NULL,
	scls_id int4 NULL,
	scls_id_comb int4 NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_security_classes_valid_combinationslog_pkey PRIMARY KEY (scvc_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_security_classeslog;

CREATE TABLE subscription_log.ss_security_classeslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	scls_id int4 NOT NULL,
	scls_description varchar(500) NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	scls_public_description varchar(500) NULL,
	scls_code int4 NULL,
	CONSTRAINT ss_security_classeslog_pkey PRIMARY KEY (scls_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_subscriber_categorieslog;

CREATE TABLE subscription_log.ss_subscriber_categorieslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	suca_id int4 NOT NULL,
	suca_name varchar(120) NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_subscriber_categorieslog_pkey PRIMARY KEY (suca_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_subscriber_departmentslog;

CREATE TABLE subscription_log.ss_subscriber_departmentslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	sude_id int4 NOT NULL,
	subs_id int4 NULL,
	sude_code varchar(15) NULL,
	sude_description varchar(250) NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_subscriber_departmentslog_pkey PRIMARY KEY (sude_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_subscriber_external_usrmngmnt_dbslog;

CREATE TABLE subscription_log.ss_subscriber_external_usrmngmnt_dbslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	sxud_id int4 NOT NULL,
	xumd_id int4 NULL,
	subs_id int4 NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_subscriber_external_usrmngmnt_dbslog_pkey PRIMARY KEY (sxud_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_subscriber_invitationslog;

CREATE TABLE subscription_log.ss_subscriber_invitationslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	suin_id int4 NOT NULL,
	subs_id int4 NULL,
	suin_guid varchar(120) NULL,
	suin_email varchar(200) NULL,
	suin_created_when timestamptz NULL,
	suin_valid_until timestamptz NULL,
	suin_consumed_when timestamptz NULL,
	suin_accepted_when timestamptz NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_subscriber_invitationslog_pkey PRIMARY KEY (suin_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_subscriber_module_packs_priceslog;

CREATE TABLE subscription_log.ss_subscriber_module_packs_priceslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	smpp_id int4 NOT NULL,
	subs_id int4 NULL,
	mopp_id int4 NULL,
	smpp_remarks varchar(500) NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	row_version int4 NULL,
	smpp_is_active bool NULL,
	CONSTRAINT ss_subscriber_module_packs_priceslog_pkey PRIMARY KEY (smpp_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_subscriber_statuseslog;

CREATE TABLE subscription_log.ss_subscriber_statuseslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	sust_id int2 NOT NULL,
	sust_description varchar(60) NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_subscriber_statuseslog_pkey PRIMARY KEY (sust_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_subscribers_for_updatelog;

CREATE TABLE subscription_log.ss_subscribers_for_updatelog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	subs_id int4 NOT NULL,
	subs_short_name varchar(60) NULL,
	subs_legal_name varchar(200) NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_subscribers_for_updatelog_pkey PRIMARY KEY (subs_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_subscribers_module_group_statuslog;

CREATE TABLE subscription_log.ss_subscribers_module_group_statuslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	smgs_id int2 NOT NULL,
	smgs_description varchar(60) NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_subscribers_module_group_statuslog_pkey PRIMARY KEY (smgs_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_subscribers_module_groupslog;

CREATE TABLE subscription_log.ss_subscribers_module_groupslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	sumg_id int4 NOT NULL,
	subs_id int4 NULL,
	mogr_id int4 NULL,
	smgs_id int2 NULL,
	sumg_valid_from timestamptz NULL,
	sumg_valid_until timestamptz NULL,
	sumg_max_users int2 NULL,
	sumg_max_customers int2 NULL,
	sumg_max_resources int2 NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_subscribers_module_groupslog_pkey PRIMARY KEY (sumg_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_subscribers_security_classeslog;

CREATE TABLE subscription_log.ss_subscribers_security_classeslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	sscl_id int4 NOT NULL,
	subs_id int4 NULL,
	scls_id int4 NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_subscribers_security_classeslog_pkey PRIMARY KEY (sscl_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_subscriberslog;

CREATE TABLE subscription_log.ss_subscriberslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	subs_id int4 NOT NULL,
	sust_id int2 NULL,
	suca_id int4 NULL,
	subs_short_name varchar(60) NULL,
	subs_vat varchar(20) NULL,
	subs_legal_name varchar(200) NULL,
	subs_tax_office varchar(60) NULL,
	subs_occupation varchar(200) NULL,
	subs_phone varchar(20) NULL,
	subs_fax varchar(20) NULL,
	subs_address varchar(120) NULL,
	subs_address_number varchar(10) NULL,
	subs_postal_code varchar(10) NULL,
	subs_city varchar(60) NULL,
	subs_prefecture varchar(60) NULL,
	subs_country varchar(60) NULL,
	subs_email varchar(120) NULL,
	subs_web_site varchar(120) NULL,
	subs_database varchar(500) NULL,
	subs_application_url varchar(60) NULL,
	subs_invoicing_method int2 NULL,
	suvc_id int4 NULL,
	subs_company_vat varchar(9) NULL,
	subs_billing_comp_id int4 NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_subscriberslog_pkey PRIMARY KEY (subs_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_system_privileges_security_classeslog;

CREATE TABLE subscription_log.ss_system_privileges_security_classeslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	spsc_id int4 NOT NULL,
	sypr_id int4 NULL,
	scls_id int4 NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_system_privileges_security_classeslog_pkey PRIMARY KEY (spsc_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_system_privilegeslog;

CREATE TABLE subscription_log.ss_system_privilegeslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	sypr_id int4 NOT NULL,
	modu_id int4 NULL,
	sypr_name varchar(500) NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_system_privilegeslog_pkey PRIMARY KEY (sypr_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_system_roles_privilegeslog;

CREATE TABLE subscription_log.ss_system_roles_privilegeslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	syrp_id int4 NOT NULL,
	syro_id int4 NULL,
	sypr_id int4 NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_system_roles_privilegeslog_pkey PRIMARY KEY (syrp_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_system_roleslog;

CREATE TABLE subscription_log.ss_system_roleslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	syro_id int4 NOT NULL,
	syro_name varchar(120) NULL,
	syro_description varchar(500) NULL,
	syro_is_multipage_role bool NULL,
	modu_id int4 NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_system_roleslog_pkey PRIMARY KEY (syro_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_user_categorieslog;

CREATE TABLE subscription_log.ss_user_categorieslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	usca_id int4 NOT NULL,
	usca_parent_id int4 NULL,
	usca_left_id int4 NULL,
	usca_right_id int4 NULL,
	usca_name varchar(120) NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_user_categorieslog_pkey PRIMARY KEY (usca_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_user_external_usrmngmnt_userlog;

CREATE TABLE subscription_log.ss_user_external_usrmngmnt_userlog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	uxuu_id int4 NOT NULL,
	sxud_id int4 NULL,
	subs_id int4 NULL,
	user_id int4 NULL,
	uxuu_external_username varchar(120) NULL,
	uxuu_external_subs_code varchar(60) NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_user_external_usrmngmnt_userlog_pkey PRIMARY KEY (uxuu_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_user_login_attemptslog;

CREATE TABLE subscription_log.ss_user_login_attemptslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	usla_id int4 NOT NULL,
	usla_dteattempt timestamp NULL,
	usla_ip varchar(120) NULL,
	usla_username varchar(250) NULL,
	usla_app varchar(120) NULL,
	usla_authentication_status varchar(120) NULL,
	usla_status_locks bool NULL,
	CONSTRAINT ss_user_login_attemptslog_pkey PRIMARY KEY (usla_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_user_messageslog;

CREATE TABLE subscription_log.ss_user_messageslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	umsg_id int4 NOT NULL,
	user_id int4 NULL,
	subs_id int4 NULL,
	modu_id int4 NULL,
	umsg_valid_from timestamp NULL,
	umsg_valid_until timestamp NULL,
	umsg_message varchar(500) NULL,
	umsg_message_type int4 NULL,
	umsg_message_location int4 NULL,
	umsg_message_is_enabled bool NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_user_messageslog_pkey PRIMARY KEY (umsg_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_user_moduleslog;

CREATE TABLE subscription_log.ss_user_moduleslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	usmo_id int4 NOT NULL,
	user_id int4 NULL,
	modu_id int4 NULL,
	usmo_read_mode bool NULL,
	usmo_valid_until timestamp NULL,
	usmo_from_subscriber int4 NULL,
	mopp_id int4 NULL,
	user_registered_from_id int4 NULL,
	usmo_registration_date timestamp NULL,
	usmo_is_renewal bool NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_user_moduleslog_pkey PRIMARY KEY (usmo_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_user_pwd_historylog;

CREATE TABLE subscription_log.ss_user_pwd_historylog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	usph_id int4 NOT NULL,
	user_id int4 NULL,
	usph_password varchar(120) NULL,
	usph_dtechanged timestamp NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	usph_ip varchar(120) NULL,
	usph_pwd_changed_by_admin bool NULL,
	CONSTRAINT ss_user_pwd_historylog_pkey PRIMARY KEY (usph_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_user_session_logoutcodeslog;

CREATE TABLE subscription_log.ss_user_session_logoutcodeslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	uslc_id int4 NOT NULL,
	uslc_name varchar(30) NULL,
	uslc_desc varchar(240) NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_user_session_logoutcodeslog_pkey PRIMARY KEY (uslc_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_user_session_variableslog;

CREATE TABLE subscription_log.ss_user_session_variableslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	usvr_id int8 NOT NULL,
	usrs_id int8 NULL,
	usvr_key varchar(160) NULL,
	usvr_value varchar(1000) NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_user_session_variableslog_pkey PRIMARY KEY (usvr_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_user_sessionslog;

CREATE TABLE subscription_log.ss_user_sessionslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	usrs_id int8 NOT NULL,
	usrs_email varchar(120) NULL,
	usrs_ip varchar(120) NULL,
	usrs_session varchar(500) NULL,
	usrs_ssosession varchar(120) NULL,
	usrs_app varchar(120) NULL,
	usrs_loginsse int4 NULL,
	usrs_logoutsse int4 NULL,
	uslc_id int4 NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	usrs_last_ping_timestamp timestamp NULL,
	CONSTRAINT ss_user_sessionslog_pkey PRIMARY KEY (usrs_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_user_statuseslog;

CREATE TABLE subscription_log.ss_user_statuseslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	usst_id int2 NOT NULL,
	usst_description varchar(60) NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_user_statuseslog_pkey PRIMARY KEY (usst_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_user_user_categorieslog;

CREATE TABLE subscription_log.ss_user_user_categorieslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	usuc_id int4 NOT NULL,
	user_id int4 NULL,
	usca_id int4 NULL,
	usuc_order int4 NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_user_user_categorieslog_pkey PRIMARY KEY (usuc_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_users_mappinglog;

CREATE TABLE subscription_log.ss_users_mappinglog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	usma_id int4 NOT NULL,
	usma_platform int4 NULL,
	usma_public_email varchar(120) NULL,
	usma_internal_login_name varchar(120) NULL,
	user_id int4 NULL,
	CONSTRAINT ss_users_mappinglog_pkey PRIMARY KEY (usma_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_users_roleslog;

CREATE TABLE subscription_log.ss_users_roleslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	usro_id int4 NOT NULL,
	user_id int4 NULL,
	role_id int4 NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	subs_id int4 NULL,
	CONSTRAINT ss_users_roleslog_pkey PRIMARY KEY (usro_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_users_subscriber_departmentslog;

CREATE TABLE subscription_log.ss_users_subscriber_departmentslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	ussd_id int4 NOT NULL,
	user_id int4 NULL,
	sude_id int4 NULL,
	subs_id int4 NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT ss_users_subscriber_departmentslog_pkey PRIMARY KEY (ussd_id, log_aa)
);

-- Drop table

-- DROP TABLE subscription_log.ss_userslog;

CREATE TABLE subscription_log.ss_userslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	user_id int4 NOT NULL,
	usst_id int2 NULL,
	subs_id int4 NULL,
	user_is_administrator bool NULL,
	user_email varchar(120) NULL,
	user_password varchar(120) NULL,
	user_password_salt varchar(60) NULL,
	user_firstname varchar(60) NULL,
	user_surname varchar(60) NULL,
	user_nickname varchar(120) NULL,
	user_vat varchar(20) NULL,
	user_phone varchar(20) NULL,
	user_phone_cell varchar(20) NULL,
	user_address varchar(120) NULL,
	user_address_number varchar(10) NULL,
	user_postal_code varchar(10) NULL,
	user_city varchar(60) NULL,
	user_prefecture varchar(60) NULL,
	user_country varchar(60) NULL,
	user_comments varchar(250) NULL,
	user_portal_id int4 NULL,
	user_registered_from_id int4 NULL,
	user_agree_for_transmission bool NULL,
	user_registration_date timestamptz NULL,
	user_last_login_date timestamptz NULL,
	user_invoice_type int4 NULL,
	user_identity_number varchar(10) NULL,
	subs_registered_from_id int4 NULL,
	user_payment_method int4 NULL,
	row_version int4 NULL,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	user_active_email varchar(120) NULL,
	user_visible_type int4 NULL,
	user_fathername varchar(120) NULL,
	user_ghost_user_id int4 NULL,
	user_login_attempts int4 NULL,
	user_password_expiration_date timestamp NULL,
	user_password_algorithm int4 NULL,
	user_pwd_changed_by_admin bool NULL,
	user_last_activation_date timestamp NULL,
	user_multiple_sessions_flag int4 NULL,
	CONSTRAINT ss_userslog_pkey PRIMARY KEY (user_id, log_aa)
);



-- Drop table

-- DROP TABLE user_registration.dwr_ede13_afm_efo_dik;

CREATE TABLE user_registration.dwr_ede13_afm_efo_dik (
	efo_id numeric(12) NULL,
	municipality_id_b2 varchar(10) NULL,
	municipality_b2_name varchar(100) NULL,
	"name" varchar(100) NULL,
	lastname varchar(100) NULL,
	firstname varchar(40) NULL,
	fathername varchar(40) NULL,
	idno varchar(10) NULL,
	afm varchar(9) NOT NULL,
	municipality_id_b1 varchar(10) NULL,
	municipality_b1_name varchar(100) NULL,
	taxkodikos_b1 varchar(5) NULL,
	odos_b1 varchar(60) NULL,
	numodos_b1 varchar(9) NULL,
	dikaiomavalue numeric NULL,
	ektash_ha numeric NULL,
	bank_description varchar(100) NULL,
	iban varchar(40) NULL,
	aigoprobata_flag numeric NULL,
	CONSTRAINT dwr_ede13_afm_efo_dik_pkey PRIMARY KEY (afm)
);
CREATE INDEX dwr_ede13_afm_efo_dik_idx ON user_registration.dwr_ede13_afm_efo_dik USING btree (efo_id, municipality_id_b2);
CREATE INDEX dwr_ede13_afm_efo_dik_idx1 ON user_registration.dwr_ede13_afm_efo_dik USING btree (efo_id, afm);
CREATE INDEX dwr_ede13_afm_efo_dik_idx2 ON user_registration.dwr_ede13_afm_efo_dik USING btree (efo_id, dikaiomavalue);
CREATE INDEX dwr_ede13_afm_efo_dik_idx3 ON user_registration.dwr_ede13_afm_efo_dik USING btree (efo_id, aigoprobata_flag);



-- Drop table

-- DROP TABLE user_registration.elot_chars;

CREATE TABLE user_registration.elot_chars (
	greek_char varchar(3) NOT NULL,
	latin_char varchar(3) NOT NULL,
	CONSTRAINT elot_chars_pkey PRIMARY KEY (greek_char)
);

-- Drop table

-- DROP TABLE user_registration.gaee2014_edev13afm;

CREATE TABLE user_registration.gaee2014_edev13afm (
	afm varchar(9) NOT NULL,
	CONSTRAINT gaee2014_edev13afm_pkey PRIMARY KEY (afm)
);

-- Drop table

-- DROP TABLE user_registration.imported_prepared_passwords;

CREATE TABLE user_registration.imported_prepared_passwords (
	password_clear varchar(60) NOT NULL,
	password_salt varchar(60) NULL,
	password_encrypted varchar(120) NULL,
	CONSTRAINT imported_prepared_passwords_idx PRIMARY KEY (password_clear)
);

-- Drop table

-- DROP TABLE user_registration.kea_excel;

CREATE TABLE user_registration.kea_excel (
	subs_short_name varchar(60) NOT NULL,
	subs_vat varchar(20) NOT NULL,
	subs_legal_name varchar(200) NOT NULL,
	subs_phone varchar(20) NOT NULL,
	subs_fax varchar(20) NULL,
	subs_address varchar(120) NOT NULL,
	subs_address_number varchar(10) NOT NULL DEFAULT '-'::character varying,
	subs_postal_code varchar(10) NOT NULL,
	subs_city varchar(60) NOT NULL,
	subs_prefecture varchar(60) NULL,
	subs_email varchar(120) NULL,
	subs_invoicing_method int2 NOT NULL DEFAULT 1,
	suvc_id int4 NOT NULL DEFAULT 1,
	subs_company_vat varchar(9) NOT NULL,
	subs_id int4 NULL,
	CONSTRAINT kea_excel_pkey PRIMARY KEY (subs_vat)
);

-- Drop table

-- DROP TABLE user_registration.modu_vat_class;

CREATE TABLE user_registration.modu_vat_class (
	movc_id int4 NOT NULL,
	movc_description varchar(40) NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	CONSTRAINT movc_cc CHECK (((movc_id >= 1) AND (movc_id <= 3))),
	CONSTRAINT movc_pk PRIMARY KEY (movc_id)
);

-- Drop table

-- DROP TABLE user_registration.network_traffic;

CREATE TABLE user_registration.network_traffic (
	id bigserial NOT NULL,
	refdate timestamp NOT NULL,
	application varchar(60) NOT NULL,
	users int4 NULL,
	inbound numeric NULL,
	outbound numeric NULL,
	subs_id int4 NOT NULL,
	CONSTRAINT netr_pk PRIMARY KEY (id),
	CONSTRAINT netr_uk UNIQUE (refdate, subs_id, application)
);

-- Drop table

-- DROP TABLE user_registration.portal_pay_codes;

CREATE TABLE user_registration.portal_pay_codes (
	popc_id serial NOT NULL,
	popc_sessionid varchar(60) NOT NULL,
	popc_pay_code varchar(60) NOT NULL,
	CONSTRAINT popc_pk PRIMARY KEY (popc_id)
);
CREATE UNIQUE INDEX popc_pay_code_u ON user_registration.portal_pay_codes USING btree (popc_pay_code);
CREATE UNIQUE INDEX popc_sessionid_u ON user_registration.portal_pay_codes USING btree (popc_sessionid);

-- Drop table

-- DROP TABLE user_registration.portal_payment_methods;

CREATE TABLE user_registration.portal_payment_methods (
	popm_id serial NOT NULL,
	popm_description varchar(60) NOT NULL,
	popm_is_cash bool NOT NULL DEFAULT false,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	row_version int4 NOT NULL DEFAULT 0,
	CONSTRAINT popm_pk PRIMARY KEY (popm_id)
);

-- Drop table

-- DROP TABLE user_registration.prepared_passwords;

CREATE TABLE user_registration.prepared_passwords (
	prpa_id serial NOT NULL,
	prpa_password_clear varchar(60) NULL,
	prpa_password varchar(120) NULL,
	prpa_password_salt varchar(60) NULL,
	row_version int4 NOT NULL DEFAULT 0,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	CONSTRAINT prepared_passwords_pkey PRIMARY KEY (prpa_id)
);
CREATE INDEX prpa_password_clear_index ON user_registration.prepared_passwords USING btree (prpa_password_clear);

-- Drop table

-- DROP TABLE user_registration.subs_vat_class;

CREATE TABLE user_registration.subs_vat_class (
	suvc_id int4 NOT NULL,
	suvc_description varchar(40) NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	CONSTRAINT suvc_cc CHECK (((suvc_id >= 1) AND (suvc_id <= 3))),
	CONSTRAINT suvc_pk PRIMARY KEY (suvc_id)
);

ALTER TABLE subscription.ss_subscribers ADD CONSTRAINT suvc_subs_fk FOREIGN KEY (suvc_id) REFERENCES user_registration.subs_vat_class(suvc_id);

-- Drop table

-- DROP TABLE user_registration.ur_gaia_emails;

CREATE TABLE user_registration.ur_gaia_emails (
	gaem_id serial NOT NULL,
	gaem_email varchar(120) NOT NULL,
	gaem_status int2 NOT NULL DEFAULT 0,
	gaem_email_server_response text NULL,
	gaem_dteinsert timestamp NOT NULL DEFAULT now(),
	gaem_dteupdate timestamp NULL,
	gaem_user_tin varchar(9) NULL,
	CONSTRAINT gaem_cc CHECK ((NOT ((gaem_status <> 0) AND (gaem_dteupdate IS NULL)))),
	CONSTRAINT gaem_pk PRIMARY KEY (gaem_id),
	CONSTRAINT gaem_uk UNIQUE (gaem_email)
);

-- Drop table

-- DROP TABLE user_registration.courses_diplomas;

CREATE TABLE user_registration.courses_diplomas (
	codi_id serial NOT NULL,
	codi_description varchar(200) NOT NULL,
	codi_value numeric(14,2) NOT NULL,
	codc_id int4 NOT NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	row_version int4 NOT NULL DEFAULT 0,
	codi_moodle_id int4 NULL,
	movc_id int4 NOT NULL,
	CONSTRAINT codi_pk PRIMARY KEY (codi_id)
);

-- Drop table

-- DROP TABLE user_registration.courses_diplomas_categories;

CREATE TABLE user_registration.courses_diplomas_categories (
	codc_id serial NOT NULL,
	modu_id int4 NOT NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	row_version int4 NOT NULL DEFAULT 0,
	CONSTRAINT codc_pk PRIMARY KEY (codc_id)
);

-- Drop table

-- DROP TABLE user_registration.mapping_ss_modules;

CREATE TABLE user_registration.mapping_ss_modules (
	masm_id serial NOT NULL,
	masm_code varchar(5) NOT NULL,
	modu_id int4 NOT NULL,
	masm_is_sheep_goat bool NOT NULL DEFAULT false,
	masm_value numeric(14,2) NULL,
	row_version int4 NOT NULL DEFAULT 0,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	masm_is_free bool NOT NULL DEFAULT false,
	masm_eas_percent numeric(14,2) NOT NULL DEFAULT 0,
	masm_paseges_percent numeric(14,2) NOT NULL DEFAULT 0,
	movc_id int4 NOT NULL,
	masm_ith_code varchar(20) NULL,
	masm_np_percent numeric(14,2) NOT NULL DEFAULT 0,
	CONSTRAINT mapping_ss_modules_pkey PRIMARY KEY (masm_id),
	CONSTRAINT masm_percent_cc CHECK (((((COALESCE(masm_eas_percent, (0)::numeric) + COALESCE(masm_paseges_percent, (0)::numeric)) + COALESCE(masm_np_percent, (0)::numeric)) = (100)::numeric) OR (((COALESCE(masm_eas_percent, (0)::numeric) + COALESCE(masm_paseges_percent, (0)::numeric)) + COALESCE(masm_np_percent, (0)::numeric)) = (0)::numeric)))
);

-- Drop table

-- DROP TABLE user_registration.mass_user_registration_log;

CREATE TABLE user_registration.mass_user_registration_log (
	murl_id serial NOT NULL,
	subs_id int4 NOT NULL,
	user_id int4 NOT NULL,
	murl_municipality_id_from varchar(10) NULL,
	murl_municipality_id_until varchar(10) NULL,
	murl_name_from varchar(140) NULL,
	murl_name_until varchar(140) NULL,
	row_version int4 NOT NULL DEFAULT 0,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	CONSTRAINT mass_user_registration_log_pkey PRIMARY KEY (murl_id)
);
CREATE INDEX user_murl_fk_i ON user_registration.mass_user_registration_log USING btree (user_id);

-- Drop table

-- DROP TABLE user_registration.module_courses_diplomas;

CREATE TABLE user_registration.module_courses_diplomas (
	mocd_id serial NOT NULL,
	mocd_description varchar(200) NOT NULL,
	mocd_course_or_diploma int4 NOT NULL DEFAULT 1,
	mocd_is_for_eas int4 NOT NULL DEFAULT 1,
	modu_id int4 NOT NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	row_version int4 NOT NULL DEFAULT 0,
	mocd_moodle_id int4 NULL,
	CONSTRAINT module_courses_diplomas_pkey PRIMARY KEY (mocd_id)
);

-- Drop table

-- DROP TABLE user_registration.pay_orders_eas_details;

CREATE TABLE user_registration.pay_orders_eas_details (
	poed_id serial NOT NULL,
	poeh_id int4 NOT NULL,
	user_id int4 NOT NULL,
	modu_id int4 NOT NULL,
	poed_sum_value numeric(14,2) NOT NULL DEFAULT 0,
	poed_eas_value numeric(14,2) NOT NULL DEFAULT 0,
	poed_np_value numeric(14,2) NOT NULL DEFAULT 0,
	user_insert_module varchar(100) NULL,
	date_insert_module timestamp NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	row_version int4 NOT NULL DEFAULT 0,
	poed_net_sum_value numeric(14,2) NOT NULL DEFAULT 0,
	usmo_id int4 NULL,
	usmc_id int4 NULL,
	CONSTRAINT pand_pk PRIMARY KEY (poed_id)
);
CREATE INDEX user_poed_fk_i ON user_registration.pay_orders_eas_details USING btree (user_id);

-- Drop table

-- DROP TABLE user_registration.pay_orders_eas_headers;

CREATE TABLE user_registration.pay_orders_eas_headers (
	poeh_id serial NOT NULL,
	subs_id int4 NOT NULL,
	poeh_pay_code varchar(60) NOT NULL,
	poeh_issue_date timestamp NOT NULL,
	poeh_sum_value numeric(14,2) NOT NULL DEFAULT 0,
	poeh_eas_value numeric(14,2) NOT NULL DEFAULT 0,
	poeh_np_value numeric(14,2) NOT NULL DEFAULT 0,
	poeh_np_final_value numeric(14,2) NULL,
	poeh_np_discpercent numeric(5,2) NULL,
	poeh_is_paid bool NOT NULL DEFAULT false,
	poeh_payment_date timestamp NULL,
	poeh_is_billed bool NOT NULL DEFAULT false,
	poeh_trh_issue_date timestamp NULL,
	poeh_trh_series varchar(5) NULL,
	poeh_trh_seriesno int4 NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	row_version int4 NOT NULL DEFAULT 0,
	poeh_np_vat_value numeric(14,2) NOT NULL DEFAULT 0,
	poeh_net_sum_value numeric(14,2) NOT NULL DEFAULT 0,
	poeh_payment_value numeric(14,2) NOT NULL DEFAULT 0,
	CONSTRAINT payord_noauto_headers_pkey PRIMARY KEY (poeh_id),
	CONSTRAINT poeh_pay_code_uk UNIQUE (poeh_pay_code)
);

-- Drop table

-- DROP TABLE user_registration.portal_pay_orders_details;

CREATE TABLE user_registration.portal_pay_orders_details (
	ppod_id serial NOT NULL,
	ppoh_id int4 NOT NULL,
	modu_id int4 NOT NULL,
	codi_id int4 NULL,
	pord_id int4 NOT NULL,
	ppod_value numeric(14,2) NOT NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	row_version int4 NOT NULL DEFAULT 0,
	ppod_quantity numeric(14,2) NULL,
	ppod_net_value numeric(14,2) NOT NULL,
	CONSTRAINT ppod_pk PRIMARY KEY (ppod_id)
);

-- Drop table

-- DROP TABLE user_registration.portal_pay_orders_headers;

CREATE TABLE user_registration.portal_pay_orders_headers (
	ppoh_id serial NOT NULL,
	user_id int4 NOT NULL,
	porh_id int4 NOT NULL,
	ppoh_pay_code varchar(60) NOT NULL,
	ppoh_issue_date timestamp NOT NULL,
	ppoh_value numeric(14,2) NOT NULL,
	ppoh_payment_date timestamp NULL,
	ppoh_cancel_date timestamp NULL,
	ppoh_trh_issue_date timestamp NULL,
	ppoh_trh_series varchar(5) NULL,
	ppoh_trh_seriesno numeric(8) NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	row_version int4 NOT NULL DEFAULT 0,
	ppoh_is_billed bool NOT NULL DEFAULT false,
	ppoh_is_paid bool NOT NULL DEFAULT false,
	ppoh_is_canceled bool NOT NULL DEFAULT false,
	ppoh_net_value numeric(14,2) NOT NULL,
	ppoh_vat_value numeric(14,2) NOT NULL,
	CONSTRAINT pooh_pay_code_uk UNIQUE (ppoh_pay_code),
	CONSTRAINT pooh_pk PRIMARY KEY (ppoh_id)
);
CREATE INDEX user_ppoh_fk_i ON user_registration.portal_pay_orders_headers USING btree (user_id);

-- Drop table

-- DROP TABLE user_registration.portal_requests_details;

CREATE TABLE user_registration.portal_requests_details (
	pord_id serial NOT NULL,
	porh_id int4 NOT NULL,
	modu_id int4 NOT NULL,
	codi_id int4 NULL,
	pord_value numeric(14,2) NOT NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	row_version int4 NOT NULL DEFAULT 0,
	pord_quantity numeric(14,2) NULL,
	pord_net_value numeric(14,2) NOT NULL,
	CONSTRAINT pore_pk PRIMARY KEY (pord_id)
);

-- Drop table

-- DROP TABLE user_registration.portal_requests_headers;

CREATE TABLE user_registration.portal_requests_headers (
	porh_id serial NOT NULL,
	user_id int4 NOT NULL,
	porh_issue_date_time timestamp NOT NULL,
	porh_value numeric(14,2) NOT NULL,
	popm_id int4 NOT NULL,
	porh_pay_code varchar(60) NOT NULL,
	porh_close_date_time timestamp NULL,
	porh_is_completed_positive bool NOT NULL DEFAULT false,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	row_version int4 NOT NULL DEFAULT 0,
	porh_net_value numeric(14,2) NOT NULL,
	porh_vat_value numeric(14,2) NOT NULL,
	porh_invoice_tin varchar(9) NULL,
	porh_invoice_taof_code varchar(6) NULL,
	porh_invoice_name varchar(40) NULL,
	porh_invoice_city varchar(40) NULL,
	porh_invoice_job varchar(40) NULL,
	porh_invoice_phone varchar(10) NULL,
	porh_invoice_taof_description varchar(80) NULL,
	porh_invoice_area varchar(40) NULL,
	porh_invoice_street varchar(40) NULL,
	porh_invoice_street_no varchar(5) NULL,
	porh_invoice_postal_code varchar(10) NULL,
	CONSTRAINT porh_pk PRIMARY KEY (porh_id)
);
CREATE INDEX user_porh_fk_i ON user_registration.portal_requests_headers USING btree (user_id);

-- Drop table

-- DROP TABLE user_registration.reset_password;

CREATE TABLE user_registration.reset_password (
	rspw_user_random_reset_id varchar(64) NULL,
	rspw_id serial NOT NULL,
	user_id int4 NULL,
	CONSTRAINT rspw_pk_id PRIMARY KEY (rspw_id)
);
CREATE INDEX rspw_user_id_fk_i ON user_registration.reset_password USING btree (user_id);

-- Drop table

-- DROP TABLE user_registration.subs_modu_vats;

CREATE TABLE user_registration.subs_modu_vats (
	sumv_id serial NOT NULL,
	subs_id int4 NOT NULL,
	movc_id int4 NOT NULL,
	suvc_id int4 NOT NULL,
	sumv_description varchar(20) NOT NULL,
	sumv_percent numeric(4,2) NOT NULL,
	sumv_charge_percent numeric(4,2) NULL,
	row_version int4 NOT NULL DEFAULT 0,
	CONSTRAINT sumv_percent CHECK ((sumv_percent = ANY (ARRAY[(0)::numeric, (23)::numeric, (13)::numeric, 6.5, (16)::numeric, (9)::numeric, (5)::numeric]))),
	CONSTRAINT sumv_pk PRIMARY KEY (sumv_id),
	CONSTRAINT sumv_subs_id_movc_id_sucv_id_u UNIQUE (subs_id, movc_id, suvc_id)
);

-- Drop table

-- DROP TABLE user_registration.temp_user_modules;

CREATE TABLE user_registration.temp_user_modules (
	usmo_id serial NOT NULL,
	user_id int4 NOT NULL,
	modu_id int4 NOT NULL,
	usmo_read_mode bool NOT NULL DEFAULT false,
	usmo_valid_until timestamptz NULL,
	usmo_from_subscriber int4 NULL,
	row_version int4 NOT NULL DEFAULT 0,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	usmo_suggested_module bool NOT NULL DEFAULT true,
	usmo_accepted_module bool NOT NULL DEFAULT true,
	usmo_insert_application int2 NOT NULL,
	comp_id int4 NULL,
	subs_registered_from_id int4 NOT NULL,
	poed_id int4 NULL,
	usmo_activation_date timestamp NULL,
	usmo_activation_user varchar(100) NULL,
	usmo_registration_uuid uuid NULL,
	CONSTRAINT usmo_pk PRIMARY KEY (usmo_id)
);
CREATE INDEX ix_modu_usmo_fk_ss_user_modules ON user_registration.temp_user_modules USING btree (modu_id);
CREATE INDEX ix_user_usmo_fk_ss_user_modules ON user_registration.temp_user_modules USING btree (user_id);
CREATE INDEX usmo_poed_id_index ON user_registration.temp_user_modules USING btree (poed_id);
CREATE UNIQUE INDEX usmo_user_id_modu_id_u ON user_registration.temp_user_modules USING btree (user_id, modu_id);

-- Drop table

-- DROP TABLE user_registration.temp_users;

CREATE TABLE user_registration.temp_users (
	user_id serial NOT NULL,
	usst_id int2 NOT NULL,
	subs_id int4 NOT NULL,
	user_is_administrator bool NOT NULL DEFAULT false,
	user_email varchar(120) NOT NULL,
	user_password varchar(120) NOT NULL,
	user_password_salt varchar(60) NOT NULL,
	user_firstname varchar(60) NOT NULL,
	user_surname varchar(60) NOT NULL,
	user_nickname varchar(120) NOT NULL,
	user_vat varchar(20) NULL,
	user_phone varchar(20) NULL,
	user_phone_cell varchar(20) NULL,
	user_address varchar(120) NULL,
	user_address_number varchar(10) NULL,
	user_postal_code varchar(10) NULL,
	user_city varchar(60) NULL,
	user_prefecture varchar(60) NULL,
	user_country varchar(60) NULL,
	user_comments varchar(250) NULL,
	user_portal_id int4 NULL,
	user_registered_from_id int4 NULL,
	user_agree_for_transmission bool NULL DEFAULT false,
	user_registration_date timestamptz NOT NULL DEFAULT now(),
	user_last_login_date timestamptz NULL,
	row_version int4 NOT NULL DEFAULT 0,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	comp_id int4 NULL,
	user_password_clear varchar(60) NOT NULL,
	user_is_registered bool NOT NULL DEFAULT false,
	user_invoice_type int4 NOT NULL DEFAULT 1,
	user_has_sign bool NOT NULL DEFAULT true,
	user_payment_method int4 NOT NULL DEFAULT 1,
	user_insert_application int2 NOT NULL,
	subs_registered_from_id int4 NOT NULL,
	user_identity_number varchar(10) NULL,
	CONSTRAINT temp_user_user_vat_index UNIQUE (user_vat),
	CONSTRAINT user_invoice_type_check CHECK (((user_invoice_type >= 1) AND (user_invoice_type <= 2))),
	CONSTRAINT user_password_check CHECK (((user_password)::text <> ''::text)),
	CONSTRAINT user_payment_method_check CHECK (((user_payment_method >= 0) AND (user_payment_method <= 1))),
	CONSTRAINT user_pk PRIMARY KEY (user_id)
);
CREATE INDEX ix_subs_users_fk_ss_users ON user_registration.temp_users USING btree (subs_id);
CREATE INDEX ix_user_portal_id ON user_registration.temp_users USING btree (user_portal_id);
CREATE INDEX ix_usst_user_fk_ss_users ON user_registration.temp_users USING btree (usst_id);
CREATE INDEX temp_users_idx ON user_registration.temp_users USING btree (subs_id, subs_registered_from_id);
CREATE UNIQUE INDEX user_email_u ON user_registration.temp_users USING btree (user_email);

-- Drop table

-- DROP TABLE user_registration.temp_users_registration_module;

CREATE TABLE user_registration.temp_users_registration_module (
	turm_id serial NOT NULL,
	usst_id int2 NOT NULL,
	subs_id int4 NOT NULL,
	turm_user_email varchar(120) NOT NULL,
	turm_user_password varchar(120) NOT NULL,
	turm_user_password_salt varchar(60) NOT NULL,
	turm_user_firstname varchar(60) NOT NULL,
	turm_user_surname varchar(60) NOT NULL,
	turm_user_fathername varchar(120) NOT NULL,
	turm_user_vat varchar(20) NULL,
	turm_user_phone varchar(20) NULL,
	turm_user_phone_cell varchar(20) NULL,
	turm_user_registration_date timestamp NOT NULL DEFAULT now(),
	row_version int4 NOT NULL DEFAULT 0,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	turm_user_password_clear varchar(60) NOT NULL,
	turm_user_is_registered bool NOT NULL DEFAULT false,
	subs_registered_from_id int4 NOT NULL,
	turm_user_identity_number varchar(10) NULL,
	modu_id int4 NOT NULL,
	turm_user_registration_uuid uuid NOT NULL,
	user_id int4 NULL,
	turm_user_activation_date timestamp NULL,
	turm_user_client_ip varchar(200) NULL,
	turm_user_active_email varchar(120) NOT NULL,
	subscription_activation_user varchar(120) NULL,
	subscription_activation_date timestamp NULL,
	turm_client_pc_id varchar(120) NULL,
	turm_usma_platform int4 NOT NULL,
	is_gaia_user bool NOT NULL DEFAULT false,
	user_registered_from_id int4 NULL,
	turm_registration_type int2 NOT NULL,
	turm_requires_physical_verification int2 NOT NULL DEFAULT 1,
	turm_user_address varchar(120) NULL,
	turm_user_address_number varchar(10) NULL,
	turm_user_postal_code varchar(10) NULL,
	turm_user_city varchar(60) NULL,
	turm_user_prefecture varchar(60) NULL,
	turm_user_country varchar(60) NULL,
	turm_user_comments varchar(250) NULL,
	CONSTRAINT turm_is_gaia_user_check CHECK ((NOT ((is_gaia_user IS TRUE) AND (turm_usma_platform > 0)))),
	CONSTRAINT turm_modu_active_email_uk UNIQUE (modu_id, turm_user_active_email),
	CONSTRAINT turm_pk PRIMARY KEY (turm_id),
	CONSTRAINT turm_subscription_user_activator_cc CHECK ((NOT ((usst_id = 0) AND ((subscription_activation_user IS NULL) OR (subscription_activation_date IS NULL))))),
	CONSTRAINT turm_uk UNIQUE (modu_id, turm_user_vat, turm_usma_platform),
	CONSTRAINT turm_uk2 UNIQUE (modu_id, turm_user_email),
	CONSTRAINT turm_user_active_email_check CHECK (((turm_user_active_email)::text <> ''::text)),
	CONSTRAINT turm_user_email_check CHECK (((turm_user_email)::text <> ''::text)),
	CONSTRAINT turm_user_id_uk UNIQUE (user_id, modu_id),
	CONSTRAINT turm_user_password_check CHECK (((turm_user_password)::text <> ''::text)),
	CONSTRAINT turm_user_registered_check CHECK ((((turm_user_is_registered IS TRUE) AND (user_id IS NOT NULL)) OR ((turm_user_is_registered IS FALSE) AND ((user_id IS NULL) OR (turm_registration_type = ANY (ARRAY[1, 2, 31, 32, 4])))))),
	CONSTRAINT turm_user_vat_check CHECK (((turm_user_vat)::text <> ''::text)),
	CONSTRAINT turm_uuid_uk UNIQUE (turm_user_registration_uuid)
);
CREATE INDEX modu_turm_fk_i ON user_registration.temp_users_registration_module USING btree (modu_id);
CREATE INDEX subs_turm_fk1_i ON user_registration.temp_users_registration_module USING btree (subs_registered_from_id);
CREATE INDEX subs_turm_fk_i ON user_registration.temp_users_registration_module USING btree (subs_id);
CREATE INDEX turm_client_pc_id_i ON user_registration.temp_users_registration_module USING btree (turm_client_pc_id);
CREATE INDEX user_turm_fk2_in ON user_registration.temp_users_registration_module USING btree (user_id, turm_user_email, turm_user_active_email);
CREATE INDEX user_turm_fk3_i ON user_registration.temp_users_registration_module USING btree (user_registered_from_id);
CREATE INDEX usst_turm_fk_i ON user_registration.temp_users_registration_module USING btree (usst_id);

-- Drop table

-- DROP TABLE user_registration.ur_bulk_users_registration_module;

CREATE TABLE user_registration.ur_bulk_users_registration_module (
	burm_id serial NOT NULL,
	usst_id int2 NOT NULL DEFAULT 4,
	subs_id int4 NOT NULL,
	burm_username varchar(120) NULL,
	burm_user_firstname varchar(60) NULL,
	burm_user_surname varchar(60) NOT NULL,
	burm_user_fathername varchar(120) NULL,
	burm_user_vat varchar(20) NOT NULL,
	burm_user_phone varchar(20) NULL,
	burm_user_phone_cell varchar(20) NULL,
	burm_user_registration_date timestamp NOT NULL DEFAULT now(),
	row_version int4 NOT NULL DEFAULT 0,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	burm_user_is_registered bool NOT NULL DEFAULT false,
	subs_registered_from_id int4 NOT NULL,
	user_registered_from_id int4 NOT NULL,
	burm_user_identity_number varchar(10) NULL,
	modu_name varchar(60) NOT NULL,
	user_id int4 NULL,
	burm_user_activation_date timestamp NULL,
	burm_subscription_activation_user varchar(120) NULL,
	burm_subscription_activation_date timestamp NULL,
	burm_usma_platform int4 NOT NULL,
	burm_user_is_charged bool NOT NULL DEFAULT false,
	is_gaia_user bool NOT NULL DEFAULT false,
	CONSTRAINT burm_pk PRIMARY KEY (burm_id),
	CONSTRAINT burm_subscription_user_activator_cc CHECK ((NOT ((usst_id = 0) AND ((burm_subscription_activation_user IS NULL) OR (burm_subscription_activation_date IS NULL))))),
	CONSTRAINT burm_uk UNIQUE (burm_user_vat, modu_name),
	CONSTRAINT burm_uk2 UNIQUE (burm_username, modu_name),
	CONSTRAINT burm_user_vat_check CHECK (((burm_user_vat)::text <> ''::text))
);
CREATE INDEX subs_burm_fk1_i ON user_registration.ur_bulk_users_registration_module USING btree (subs_registered_from_id);
CREATE INDEX subs_burm_fk_i ON user_registration.ur_bulk_users_registration_module USING btree (subs_id);
CREATE INDEX user_burm_fk2_i ON user_registration.ur_bulk_users_registration_module USING btree (user_registered_from_id);
CREATE INDEX user_burm_fk_i ON user_registration.ur_bulk_users_registration_module USING btree (user_id);
CREATE INDEX usst_burm_fk_i ON user_registration.ur_bulk_users_registration_module USING btree (usst_id);

-- Drop table

-- DROP TABLE user_registration.ur_users_ucodes;

CREATE TABLE user_registration.ur_users_ucodes (
	usuc_id serial NOT NULL,
	user_id int4 NOT NULL,
	usuc_ucode varchar(20) NOT NULL,
	usuc_activeflag bool NOT NULL DEFAULT true,
	usuc_replaced_user_name varchar(120) NULL,
	row_version int4 NOT NULL DEFAULT 0,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	CONSTRAINT usuc_pk PRIMARY KEY (usuc_id),
	CONSTRAINT usuc_uk UNIQUE (user_id),
	CONSTRAINT usuc_uk2 UNIQUE (usuc_ucode)
);
CREATE INDEX user_usuc_fk_i ON user_registration.ur_users_ucodes USING btree (user_id);

-- Drop table

-- DROP TABLE user_registration.user_courses_diplomas;

CREATE TABLE user_registration.user_courses_diplomas (
	uscd_id serial NOT NULL,
	user_id int4 NOT NULL,
	codi_id int4 NOT NULL,
	modu_id int4 NOT NULL,
	subs_id int4 NOT NULL,
	uscd_valid_until timestamptz NULL,
	row_version int4 NOT NULL DEFAULT 0,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	CONSTRAINT uscd_pk PRIMARY KEY (uscd_id)
);
CREATE UNIQUE INDEX uscd_user_id_codi_id_u ON user_registration.user_courses_diplomas USING btree (user_id, codi_id);

-- Drop table

-- DROP TABLE user_registration.user_module_cost;

CREATE TABLE user_registration.user_module_cost (
	usmc_id serial NOT NULL,
	usmo_id int4 NOT NULL,
	usmc_net_value numeric(14,2) NULL,
	usmc_issue_date timestamp NULL,
	usmc_invoice_flag int4 NULL DEFAULT 0,
	row_version int4 NOT NULL DEFAULT 0,
	dteinsert timestamp NULL,
	usrinsert varchar(150) NULL,
	dteupdate timestamp NULL,
	usrupdate varchar(150) NULL,
	CONSTRAINT usmc_pk PRIMARY KEY (usmc_id)
);

-- Drop table

-- DROP TABLE user_registration.value_rights_modules_details;

CREATE TABLE user_registration.value_rights_modules_details (
	vrmd_id serial NOT NULL,
	vrmh_id int4 NOT NULL,
	masm_id int4 NOT NULL,
	row_version int4 NOT NULL DEFAULT 0,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	CONSTRAINT value_rights_modules_details_pkey1 PRIMARY KEY (vrmd_id),
	CONSTRAINT vrmh_modu_id_u UNIQUE (vrmh_id, masm_id)
);

-- Drop table

-- DROP TABLE user_registration.value_rights_modules_headers;

CREATE TABLE user_registration.value_rights_modules_headers (
	vrmh_id serial NOT NULL,
	subs_id int4 NOT NULL,
	vrmg_value_rights_from numeric(14,2) NOT NULL,
	vrmh_value_rights_until numeric(14,2) NOT NULL,
	vrmh_include_sheep_goat bool NOT NULL DEFAULT false,
	row_version int4 NOT NULL DEFAULT 0,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	CONSTRAINT value_rights_modules_details_pkey PRIMARY KEY (vrmh_id)
);

ALTER TABLE user_registration.courses_diplomas ADD CONSTRAINT codc_codi_fk FOREIGN KEY (codc_id) REFERENCES user_registration.courses_diplomas_categories(codc_id);
ALTER TABLE user_registration.courses_diplomas ADD CONSTRAINT movc_codi_fk FOREIGN KEY (movc_id) REFERENCES user_registration.modu_vat_class(movc_id);

ALTER TABLE user_registration.courses_diplomas_categories ADD CONSTRAINT modu_codc_fk FOREIGN KEY (modu_id) REFERENCES subscription.ss_modules(modu_id);

ALTER TABLE user_registration.mapping_ss_modules ADD CONSTRAINT modu_masm_fk FOREIGN KEY (modu_id) REFERENCES subscription.ss_modules(modu_id);
ALTER TABLE user_registration.mapping_ss_modules ADD CONSTRAINT movc_masm_fk FOREIGN KEY (movc_id) REFERENCES user_registration.modu_vat_class(movc_id);

ALTER TABLE user_registration.mass_user_registration_log ADD CONSTRAINT subs_murl_fk FOREIGN KEY (subs_id) REFERENCES subscription.ss_subscribers(subs_id);
ALTER TABLE user_registration.mass_user_registration_log ADD CONSTRAINT user_murl_fk FOREIGN KEY (user_id) REFERENCES subscription.ss_users(user_id);

ALTER TABLE user_registration.module_courses_diplomas ADD CONSTRAINT modu_mocd_fk FOREIGN KEY (modu_id) REFERENCES subscription.ss_modules(modu_id);

ALTER TABLE user_registration.pay_orders_eas_details ADD CONSTRAINT modu_poed_fk FOREIGN KEY (modu_id) REFERENCES subscription.ss_modules(modu_id);
ALTER TABLE user_registration.pay_orders_eas_details ADD CONSTRAINT poeh_poed_fk FOREIGN KEY (poeh_id) REFERENCES user_registration.pay_orders_eas_headers(poeh_id);
ALTER TABLE user_registration.pay_orders_eas_details ADD CONSTRAINT user_poed_fk FOREIGN KEY (user_id) REFERENCES subscription.ss_users(user_id);
ALTER TABLE user_registration.pay_orders_eas_details ADD CONSTRAINT usmc_poed_fk FOREIGN KEY (usmc_id) REFERENCES user_registration.user_module_cost(usmc_id);
ALTER TABLE user_registration.pay_orders_eas_details ADD CONSTRAINT usmo_poed_fk FOREIGN KEY (usmo_id) REFERENCES subscription.ss_user_modules(usmo_id);

ALTER TABLE user_registration.pay_orders_eas_headers ADD CONSTRAINT subs_poeh_fk FOREIGN KEY (subs_id) REFERENCES subscription.ss_subscribers(subs_id);

ALTER TABLE user_registration.portal_pay_orders_details ADD CONSTRAINT codi_ppod_fk FOREIGN KEY (codi_id) REFERENCES user_registration.courses_diplomas(codi_id);
ALTER TABLE user_registration.portal_pay_orders_details ADD CONSTRAINT modu_ppod_fk FOREIGN KEY (modu_id) REFERENCES subscription.ss_modules(modu_id);
ALTER TABLE user_registration.portal_pay_orders_details ADD CONSTRAINT pord_ppod_fk FOREIGN KEY (pord_id) REFERENCES user_registration.portal_requests_details(pord_id);
ALTER TABLE user_registration.portal_pay_orders_details ADD CONSTRAINT ppoh_ppod_fk FOREIGN KEY (ppoh_id) REFERENCES user_registration.portal_pay_orders_headers(ppoh_id);

ALTER TABLE user_registration.portal_pay_orders_headers ADD CONSTRAINT porh_ppoh_fk FOREIGN KEY (porh_id) REFERENCES user_registration.portal_requests_headers(porh_id);
ALTER TABLE user_registration.portal_pay_orders_headers ADD CONSTRAINT user_ppoh_fk FOREIGN KEY (user_id) REFERENCES subscription.ss_users(user_id);

ALTER TABLE user_registration.portal_requests_details ADD CONSTRAINT codi_pord_fk FOREIGN KEY (codi_id) REFERENCES user_registration.courses_diplomas(codi_id);
ALTER TABLE user_registration.portal_requests_details ADD CONSTRAINT modu_pord_fk FOREIGN KEY (modu_id) REFERENCES subscription.ss_modules(modu_id);
ALTER TABLE user_registration.portal_requests_details ADD CONSTRAINT porh_pord_pk FOREIGN KEY (porh_id) REFERENCES user_registration.portal_requests_headers(porh_id);

ALTER TABLE user_registration.portal_requests_headers ADD CONSTRAINT popm_porh_fk FOREIGN KEY (popm_id) REFERENCES user_registration.portal_payment_methods(popm_id);
ALTER TABLE user_registration.portal_requests_headers ADD CONSTRAINT user_porh_fk FOREIGN KEY (user_id) REFERENCES subscription.ss_users(user_id);

ALTER TABLE user_registration.reset_password ADD CONSTRAINT rspw_user_id_fk FOREIGN KEY (user_id) REFERENCES subscription.ss_users(user_id);

ALTER TABLE user_registration.subs_modu_vats ADD CONSTRAINT movc_sumv_fk FOREIGN KEY (movc_id) REFERENCES user_registration.modu_vat_class(movc_id);
ALTER TABLE user_registration.subs_modu_vats ADD CONSTRAINT subs_sumv_fk FOREIGN KEY (subs_id) REFERENCES subscription.ss_subscribers(subs_id);
ALTER TABLE user_registration.subs_modu_vats ADD CONSTRAINT suvc_sumv_fk FOREIGN KEY (suvc_id) REFERENCES user_registration.subs_vat_class(suvc_id);

ALTER TABLE user_registration.temp_user_modules ADD CONSTRAINT modu_usmo_fk FOREIGN KEY (modu_id) REFERENCES subscription.ss_modules(modu_id);
ALTER TABLE user_registration.temp_user_modules ADD CONSTRAINT poed_usmo_fk FOREIGN KEY (poed_id) REFERENCES user_registration.pay_orders_eas_details(poed_id);
ALTER TABLE user_registration.temp_user_modules ADD CONSTRAINT subs_usmo_fk FOREIGN KEY (subs_registered_from_id) REFERENCES subscription.ss_subscribers(subs_id);
ALTER TABLE user_registration.temp_user_modules ADD CONSTRAINT user_usso_fk FOREIGN KEY (user_id) REFERENCES user_registration.temp_users(user_id);

ALTER TABLE user_registration.temp_users ADD CONSTRAINT subs_user_fk FOREIGN KEY (subs_id) REFERENCES subscription.ss_subscribers(subs_id);
ALTER TABLE user_registration.temp_users ADD CONSTRAINT subs_user_fk1 FOREIGN KEY (subs_registered_from_id) REFERENCES subscription.ss_subscribers(subs_id);
ALTER TABLE user_registration.temp_users ADD CONSTRAINT usst_user_fk FOREIGN KEY (usst_id) REFERENCES subscription.ss_user_statuses(usst_id);

ALTER TABLE user_registration.temp_users_registration_module ADD CONSTRAINT modu_turm_fk FOREIGN KEY (modu_id) REFERENCES subscription.ss_modules(modu_id);
ALTER TABLE user_registration.temp_users_registration_module ADD CONSTRAINT subs_turm_fk FOREIGN KEY (subs_id) REFERENCES subscription.ss_subscribers(subs_id);
ALTER TABLE user_registration.temp_users_registration_module ADD CONSTRAINT subs_turm_fk1 FOREIGN KEY (subs_registered_from_id) REFERENCES subscription.ss_subscribers(subs_id);
ALTER TABLE user_registration.temp_users_registration_module ADD CONSTRAINT user_turm_fk FOREIGN KEY (user_id) REFERENCES subscription.ss_users(user_id);
ALTER TABLE user_registration.temp_users_registration_module ADD CONSTRAINT user_turm_fk2 FOREIGN KEY (user_id, turm_user_email, turm_user_active_email) REFERENCES subscription.ss_users(user_id, user_email, user_active_email);
ALTER TABLE user_registration.temp_users_registration_module ADD CONSTRAINT user_turm_fk3 FOREIGN KEY (user_registered_from_id) REFERENCES subscription.ss_users(user_id);
ALTER TABLE user_registration.temp_users_registration_module ADD CONSTRAINT usst_turm_fk FOREIGN KEY (usst_id) REFERENCES subscription.ss_user_statuses(usst_id);

ALTER TABLE user_registration.ur_bulk_users_registration_module ADD CONSTRAINT subs_burm_fk FOREIGN KEY (subs_id) REFERENCES subscription.ss_subscribers(subs_id);
ALTER TABLE user_registration.ur_bulk_users_registration_module ADD CONSTRAINT subs_burm_fk1 FOREIGN KEY (subs_registered_from_id) REFERENCES subscription.ss_subscribers(subs_id);
ALTER TABLE user_registration.ur_bulk_users_registration_module ADD CONSTRAINT user_burm_fk FOREIGN KEY (user_id) REFERENCES subscription.ss_users(user_id);
ALTER TABLE user_registration.ur_bulk_users_registration_module ADD CONSTRAINT user_burm_fk2 FOREIGN KEY (user_registered_from_id) REFERENCES subscription.ss_users(user_id);
ALTER TABLE user_registration.ur_bulk_users_registration_module ADD CONSTRAINT usst_burm_fk FOREIGN KEY (usst_id) REFERENCES subscription.ss_user_statuses(usst_id);

ALTER TABLE user_registration.ur_users_ucodes ADD CONSTRAINT user_usuc_fk FOREIGN KEY (user_id) REFERENCES subscription.ss_users(user_id);

ALTER TABLE user_registration.user_courses_diplomas ADD CONSTRAINT codi_uscd_fk FOREIGN KEY (codi_id) REFERENCES user_registration.courses_diplomas(codi_id);
ALTER TABLE user_registration.user_courses_diplomas ADD CONSTRAINT modu_uscd_fk FOREIGN KEY (modu_id) REFERENCES subscription.ss_modules(modu_id);
ALTER TABLE user_registration.user_courses_diplomas ADD CONSTRAINT subs_uscd_fk FOREIGN KEY (subs_id) REFERENCES subscription.ss_subscribers(subs_id);
ALTER TABLE user_registration.user_courses_diplomas ADD CONSTRAINT user_uscd_fk FOREIGN KEY (user_id) REFERENCES subscription.ss_users(user_id);

ALTER TABLE user_registration.user_module_cost ADD CONSTRAINT usmo_usmc_fk FOREIGN KEY (usmo_id) REFERENCES subscription.ss_user_modules(usmo_id);

ALTER TABLE user_registration.value_rights_modules_details ADD CONSTRAINT vrmd_masm_fk FOREIGN KEY (masm_id) REFERENCES user_registration.mapping_ss_modules(masm_id);
ALTER TABLE user_registration.value_rights_modules_details ADD CONSTRAINT vrmh_vrmd_fk FOREIGN KEY (vrmh_id) REFERENCES user_registration.value_rights_modules_headers(vrmh_id);

ALTER TABLE user_registration.value_rights_modules_headers ADD CONSTRAINT subs_vrmh_fk FOREIGN KEY (subs_id) REFERENCES subscription.ss_subscribers(subs_id);



-- Drop table

-- DROP TABLE user_registration_log.courses_diplomas_categorieslog;

CREATE TABLE user_registration_log.courses_diplomas_categorieslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	codc_id int4 NOT NULL,
	modu_id int4 NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	row_version int4 NULL,
	CONSTRAINT courses_diplomas_categorieslog_pkey PRIMARY KEY (codc_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.courses_diplomaslog;

CREATE TABLE user_registration_log.courses_diplomaslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	codi_id int4 NOT NULL,
	codi_description varchar(200) NULL,
	codi_value numeric(14,2) NULL,
	codc_id int4 NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	row_version int4 NULL,
	codi_moodle_id int4 NULL,
	movc_id int4 NULL,
	CONSTRAINT courses_diplomaslog_pkey PRIMARY KEY (codi_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.dwr_ede13_afm_efo_diklog;

CREATE TABLE user_registration_log.dwr_ede13_afm_efo_diklog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	efo_id numeric(12) NULL,
	municipality_id_b2 varchar(10) NULL,
	municipality_b2_name varchar(100) NULL,
	"name" varchar(100) NULL,
	lastname varchar(100) NULL,
	firstname varchar(40) NULL,
	fathername varchar(40) NULL,
	idno varchar(10) NULL,
	afm varchar(9) NOT NULL,
	municipality_id_b1 varchar(10) NULL,
	municipality_b1_name varchar(100) NULL,
	taxkodikos_b1 varchar(5) NULL,
	odos_b1 varchar(60) NULL,
	numodos_b1 varchar(9) NULL,
	dikaiomavalue numeric NULL,
	ektash_ha numeric NULL,
	bank_description varchar(100) NULL,
	iban varchar(40) NULL,
	aigoprobata_flag numeric NULL,
	CONSTRAINT dwr_ede13_afm_efo_diklog_pkey PRIMARY KEY (afm, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.elot_charslog;

CREATE TABLE user_registration_log.elot_charslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	greek_char varchar(3) NOT NULL,
	latin_char varchar(3) NULL,
	CONSTRAINT elot_charslog_pkey PRIMARY KEY (greek_char, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.gaee2014_edev13afmlog;

CREATE TABLE user_registration_log.gaee2014_edev13afmlog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	afm varchar(9) NOT NULL,
	CONSTRAINT gaee2014_edev13afmlog_pkey PRIMARY KEY (afm, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.imported_prepared_passwordslog;

CREATE TABLE user_registration_log.imported_prepared_passwordslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	password_clear varchar(60) NOT NULL,
	password_salt varchar(60) NULL,
	password_encrypted varchar(120) NULL,
	CONSTRAINT imported_prepared_passwordslog_pkey PRIMARY KEY (password_clear, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.kea_excellog;

CREATE TABLE user_registration_log.kea_excellog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	subs_short_name varchar(60) NULL,
	subs_vat varchar(20) NOT NULL,
	subs_legal_name varchar(200) NULL,
	subs_phone varchar(20) NULL,
	subs_fax varchar(20) NULL,
	subs_address varchar(120) NULL,
	subs_address_number varchar(10) NULL,
	subs_postal_code varchar(10) NULL,
	subs_city varchar(60) NULL,
	subs_prefecture varchar(60) NULL,
	subs_email varchar(120) NULL,
	subs_invoicing_method int2 NULL,
	suvc_id int4 NULL,
	subs_company_vat varchar(9) NULL,
	subs_id int4 NULL,
	CONSTRAINT kea_excellog_pkey PRIMARY KEY (subs_vat, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.mapping_ss_modules_log;

CREATE TABLE user_registration_log.mapping_ss_modules_log (
	masm_log_id serial NOT NULL,
	masm_status varchar(50) NULL,
	masm_id int4 NULL,
	masm_code varchar(5) NULL,
	modu_id int4 NULL,
	masm_is_sheep_goat bool NULL DEFAULT false,
	masm_value numeric(14,2) NULL,
	row_version int4 NULL DEFAULT 0,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	masm_is_free bool NULL DEFAULT false
);

-- Drop table

-- DROP TABLE user_registration_log.mass_user_registration_loglog;

CREATE TABLE user_registration_log.mass_user_registration_loglog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	murl_id int4 NOT NULL,
	subs_id int4 NULL,
	user_id int4 NULL,
	murl_municipality_id_from varchar(10) NULL,
	murl_municipality_id_until varchar(10) NULL,
	murl_name_from varchar(140) NULL,
	murl_name_until varchar(140) NULL,
	row_version int4 NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	CONSTRAINT mass_user_registration_loglog_pkey PRIMARY KEY (murl_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.modu_vat_classlog;

CREATE TABLE user_registration_log.modu_vat_classlog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	movc_id int4 NOT NULL,
	movc_description varchar(40) NULL,
	row_version int4 NULL,
	CONSTRAINT modu_vat_classlog_pkey PRIMARY KEY (movc_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.module_courses_diplomaslog;

CREATE TABLE user_registration_log.module_courses_diplomaslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	mocd_id int4 NOT NULL,
	mocd_description varchar(200) NULL,
	mocd_course_or_diploma int4 NULL,
	mocd_is_for_eas int4 NULL,
	modu_id int4 NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	row_version int4 NULL,
	mocd_moodle_id int4 NULL,
	CONSTRAINT module_courses_diplomaslog_pkey PRIMARY KEY (mocd_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.pay_orders_eas_detailslog;

CREATE TABLE user_registration_log.pay_orders_eas_detailslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	poed_id int4 NOT NULL,
	poeh_id int4 NULL,
	user_id int4 NULL,
	modu_id int4 NULL,
	poed_sum_value numeric(14,2) NULL,
	poed_eas_value numeric(14,2) NULL,
	poed_np_value numeric(14,2) NULL,
	user_insert_module varchar(100) NULL,
	date_insert_module timestamp NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	row_version int4 NULL,
	poed_net_sum_value numeric(14,2) NULL,
	CONSTRAINT pay_orders_eas_detailslog_pkey PRIMARY KEY (poed_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.pay_orders_eas_headerslog;

CREATE TABLE user_registration_log.pay_orders_eas_headerslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	poeh_id int4 NOT NULL,
	subs_id int4 NULL,
	poeh_pay_code varchar(60) NULL,
	poeh_issue_date timestamp NULL,
	poeh_sum_value numeric(14,2) NULL,
	poeh_eas_value numeric(14,2) NULL,
	poeh_np_value numeric(14,2) NULL,
	poeh_np_final_value numeric(14,2) NULL,
	poeh_np_discpercent numeric(5,2) NULL,
	poeh_is_paid bool NULL,
	poeh_payment_date timestamp NULL,
	poeh_is_billed bool NULL,
	poeh_trh_issue_date timestamp NULL,
	poeh_trh_series varchar(5) NULL,
	poeh_trh_seriesno int4 NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	row_version int4 NULL,
	poeh_np_vat_value numeric(14,2) NULL,
	poeh_net_sum_value numeric(14,2) NULL,
	poeh_payment_value numeric(14,2) NULL,
	CONSTRAINT pay_orders_eas_headerslog_pkey PRIMARY KEY (poeh_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.portal_pay_codeslog;

CREATE TABLE user_registration_log.portal_pay_codeslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	popc_id int4 NOT NULL,
	popc_sessionid varchar(60) NULL,
	popc_pay_code varchar(60) NULL,
	CONSTRAINT portal_pay_codeslog_pkey PRIMARY KEY (popc_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.portal_pay_orders_detailslog;

CREATE TABLE user_registration_log.portal_pay_orders_detailslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	ppod_id int4 NOT NULL,
	ppoh_id int4 NULL,
	modu_id int4 NULL,
	codi_id int4 NULL,
	pord_id int4 NULL,
	ppod_value numeric(14,2) NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	row_version int4 NULL,
	ppod_quantity numeric(14,2) NULL,
	ppod_net_value numeric(14,2) NULL,
	CONSTRAINT portal_pay_orders_detailslog_pkey PRIMARY KEY (ppod_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.portal_pay_orders_headerslog;

CREATE TABLE user_registration_log.portal_pay_orders_headerslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	ppoh_id int4 NOT NULL,
	user_id int4 NULL,
	porh_id int4 NULL,
	ppoh_pay_code varchar(60) NULL,
	ppoh_issue_date timestamp NULL,
	ppoh_value numeric(14,2) NULL,
	ppoh_payment_date timestamp NULL,
	ppoh_cancel_date timestamp NULL,
	ppoh_trh_issue_date timestamp NULL,
	ppoh_trh_series varchar(5) NULL,
	ppoh_trh_seriesno numeric(8) NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	row_version int4 NULL,
	ppoh_is_billed bool NULL,
	ppoh_is_paid bool NULL,
	ppoh_is_canceled bool NULL,
	ppoh_net_value numeric(14,2) NULL,
	ppoh_vat_value numeric(14,2) NULL,
	CONSTRAINT portal_pay_orders_headerslog_pkey PRIMARY KEY (ppoh_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.portal_payment_methodslog;

CREATE TABLE user_registration_log.portal_payment_methodslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	popm_id int4 NOT NULL,
	popm_description varchar(60) NULL,
	popm_is_cash bool NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	row_version int4 NULL,
	CONSTRAINT portal_payment_methodslog_pkey PRIMARY KEY (popm_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.portal_requests_detailslog;

CREATE TABLE user_registration_log.portal_requests_detailslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	pord_id int4 NOT NULL,
	porh_id int4 NULL,
	modu_id int4 NULL,
	codi_id int4 NULL,
	pord_value numeric(14,2) NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	row_version int4 NULL,
	pord_quantity numeric(14,2) NULL,
	pord_net_value numeric(14,2) NULL,
	CONSTRAINT portal_requests_detailslog_pkey PRIMARY KEY (pord_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.portal_requests_headerslog;

CREATE TABLE user_registration_log.portal_requests_headerslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	porh_id int4 NOT NULL,
	user_id int4 NULL,
	porh_issue_date_time timestamp NULL,
	porh_value numeric(14,2) NULL,
	popm_id int4 NULL,
	porh_pay_code varchar(60) NULL,
	porh_close_date_time timestamp NULL,
	porh_is_completed_positive bool NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	row_version int4 NULL,
	porh_net_value numeric(14,2) NULL,
	porh_vat_value numeric(14,2) NULL,
	porh_invoice_tin varchar(9) NULL,
	porh_invoice_taof_code varchar(6) NULL,
	porh_invoice_name varchar(40) NULL,
	porh_invoice_city varchar(40) NULL,
	porh_invoice_job varchar(40) NULL,
	porh_invoice_phone varchar(10) NULL,
	porh_invoice_taof_description varchar(80) NULL,
	porh_invoice_area varchar(40) NULL,
	porh_invoice_street varchar(40) NULL,
	porh_invoice_street_no varchar(5) NULL,
	porh_invoice_postal_code varchar(10) NULL,
	CONSTRAINT portal_requests_headerslog_pkey PRIMARY KEY (porh_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.prepared_passwordslog;

CREATE TABLE user_registration_log.prepared_passwordslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	prpa_id int4 NOT NULL,
	prpa_password_clear varchar(60) NULL,
	prpa_password varchar(120) NULL,
	prpa_password_salt varchar(60) NULL,
	row_version int4 NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	CONSTRAINT prepared_passwordslog_pkey PRIMARY KEY (prpa_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.reset_passwordlog;

CREATE TABLE user_registration_log.reset_passwordlog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	rspw_user_random_reset_id varchar(64) NULL,
	rspw_id int4 NOT NULL,
	user_id int4 NULL,
	CONSTRAINT reset_passwordlog_pkey PRIMARY KEY (rspw_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.subs_modu_vatslog;

CREATE TABLE user_registration_log.subs_modu_vatslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	sumv_id int4 NOT NULL,
	subs_id int4 NULL,
	movc_id int4 NULL,
	suvc_id int4 NULL,
	sumv_description varchar(20) NULL,
	sumv_percent numeric(4,2) NULL,
	sumv_charge_percent numeric(4,2) NULL,
	row_version int4 NULL,
	CONSTRAINT subs_modu_vatslog_pkey PRIMARY KEY (sumv_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.subs_vat_classlog;

CREATE TABLE user_registration_log.subs_vat_classlog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	suvc_id int4 NOT NULL,
	suvc_description varchar(40) NULL,
	row_version int4 NULL,
	CONSTRAINT subs_vat_classlog_pkey PRIMARY KEY (suvc_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.temp_user_modules_log;

CREATE TABLE user_registration_log.temp_user_modules_log (
	usmo_log_id bigserial not NULL,
	usmo_status varchar(50) NULL,
	usmo_id int4 NULL,
	user_id int4 NULL,
	modu_id int4 NULL,
	usmo_read_mode bool NULL DEFAULT false,
	usmo_valid_until timestamptz NULL,
	usmo_from_subscriber int4 NULL,
	row_version int4 NULL DEFAULT 0,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	usmo_suggested_module bool NULL DEFAULT true,
	usmo_accepted_module bool NULL DEFAULT true,
	usmo_insert_application int2 NULL,
	comp_id int4 NULL,
	subs_registered_from_id int4 NULL,
	usmo_registration_uuid uuid NULL
);

-- Drop table

-- DROP TABLE user_registration_log.temp_users_log;

CREATE TABLE user_registration_log.temp_users_log (
	user_log_id serial not NULL,
	user_status varchar(50) NULL,
	user_id int4 NULL,
	usst_id int2 NULL,
	subs_id int4 NULL,
	user_is_administrator bool NULL DEFAULT false,
	user_email varchar(120) NULL,
	user_password varchar(120) NULL,
	user_password_salt varchar(60) NULL,
	user_firstname varchar(60) NULL,
	user_surname varchar(60) NULL,
	user_nickname varchar(120) NULL,
	user_vat varchar(20) NULL,
	user_phone varchar(20) NULL,
	user_phone_cell varchar(20) NULL,
	user_address varchar(120) NULL,
	user_address_number varchar(10) NULL,
	user_postal_code varchar(10) NULL,
	user_city varchar(60) NULL,
	user_prefecture varchar(60) NULL,
	user_country varchar(60) NULL,
	user_comments varchar(250) NULL,
	user_portal_id int4 NULL,
	user_registered_from_id int4 NULL,
	user_agree_for_transmission bool NULL DEFAULT false,
	user_registration_date timestamptz NULL DEFAULT now(),
	user_last_login_date timestamptz NULL,
	row_version int4 NULL DEFAULT 0,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	comp_id int4 NULL,
	user_password_clear varchar(60) NULL,
	user_is_registered bool NULL DEFAULT false,
	user_invoice_type int4 NULL DEFAULT 1,
	user_has_sign bool NULL DEFAULT true,
	user_payment_method int4 NULL DEFAULT 1,
	user_insert_application int2 NULL,
	subs_registered_from_id int4 NULL,
	user_identity_number varchar(10) NULL
);

-- Drop table

-- DROP TABLE user_registration_log.temp_users_registration_modulelog;

CREATE TABLE user_registration_log.temp_users_registration_modulelog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	turm_id int4 NOT NULL,
	usst_id int2 NULL,
	subs_id int4 NULL,
	turm_user_email varchar(120) NULL,
	turm_user_password varchar(120) NULL,
	turm_user_password_salt varchar(60) NULL,
	turm_user_firstname varchar(60) NULL,
	turm_user_surname varchar(60) NULL,
	turm_user_fathername varchar(120) NULL,
	turm_user_vat varchar(20) NULL,
	turm_user_phone varchar(20) NULL,
	turm_user_phone_cell varchar(20) NULL,
	turm_user_registration_date timestamp NULL,
	row_version int4 NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	turm_user_password_clear varchar(60) NULL,
	turm_user_is_registered bool NULL,
	subs_registered_from_id int4 NULL,
	turm_user_identity_number varchar(10) NULL,
	modu_id int4 NULL,
	turm_user_registration_uuid uuid NULL,
	user_id int4 NULL,
	turm_user_activation_date timestamp NULL,
	turm_user_client_ip varchar(200) NULL,
	turm_user_active_email varchar(120) NULL,
	subscription_activation_user varchar(120) NULL,
	subscription_activation_date timestamp NULL,
	turm_client_pc_id varchar(120) NULL,
	turm_usma_platform int4 NULL,
	is_gaia_user bool NULL,
	user_registered_from_id int4 NULL,
	turm_registration_type int2 NULL,
	CONSTRAINT temp_users_registration_modulelog_pkey PRIMARY KEY (turm_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.ur_bulk_users_registration_modulelog;

CREATE TABLE user_registration_log.ur_bulk_users_registration_modulelog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	burm_id int4 NOT NULL,
	usst_id int2 NULL,
	subs_id int4 NULL,
	burm_username varchar(120) NULL,
	burm_user_firstname varchar(60) NULL,
	burm_user_surname varchar(60) NULL,
	burm_user_fathername varchar(120) NULL,
	burm_user_vat varchar(20) NULL,
	burm_user_phone varchar(20) NULL,
	burm_user_phone_cell varchar(20) NULL,
	burm_user_registration_date timestamp NULL,
	row_version int4 NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	burm_user_is_registered bool NULL,
	subs_registered_from_id int4 NULL,
	user_registered_from_id int4 NULL,
	burm_user_identity_number varchar(10) NULL,
	modu_name varchar(60) NULL,
	user_id int4 NULL,
	burm_user_activation_date timestamp NULL,
	burm_subscription_activation_user varchar(120) NULL,
	burm_subscription_activation_date timestamp NULL,
	burm_usma_platform int4 NULL,
	burm_user_is_charged bool NULL,
	is_gaia_user bool NULL,
	CONSTRAINT ur_bulk_users_registration_modulelog_pkey PRIMARY KEY (burm_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.ur_gaia_emailslog;

CREATE TABLE user_registration_log.ur_gaia_emailslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	gaem_id int4 NOT NULL,
	gaem_email varchar(120) NULL,
	gaem_status int2 NULL,
	gaem_email_server_response text NULL,
	gaem_dteinsert timestamp NULL,
	gaem_dteupdate timestamp NULL,
	gaem_user_tin varchar(9) NULL,
	CONSTRAINT ur_gaia_emailslog_pkey PRIMARY KEY (gaem_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.ur_users_ucodeslog;

CREATE TABLE user_registration_log.ur_users_ucodeslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	usuc_id int4 NOT NULL,
	user_id int4 NULL,
	usuc_ucode varchar(20) NULL,
	usuc_activeflag bool NULL,
	usuc_replaced_user_name varchar(120) NULL,
	row_version int4 NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	CONSTRAINT ur_users_ucodeslog_pkey PRIMARY KEY (usuc_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.user_courses_diplomaslog;

CREATE TABLE user_registration_log.user_courses_diplomaslog (
	log_aa bigserial NOT NULL,
	log_date timestamp NULL,
	log_terminal varchar NULL,
	log_host varchar NULL,
	log_ip_address varchar NULL,
	log_dbuser varchar NULL,
	log_status varchar NULL,
	uscd_id int4 NOT NULL,
	user_id int4 NULL,
	codi_id int4 NULL,
	modu_id int4 NULL,
	subs_id int4 NULL,
	uscd_valid_until timestamptz NULL,
	row_version int4 NULL,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL,
	CONSTRAINT user_courses_diplomaslog_pkey PRIMARY KEY (uscd_id, log_aa)
);

-- Drop table

-- DROP TABLE user_registration_log.value_rights_modules_details_log;

CREATE TABLE user_registration_log.value_rights_modules_details_log (
	vrmd_log_id serial not NULL,
	vrmd_status varchar(50) NULL,
	vrmd_id int4 NULL,
	vrmh_id int4 NULL,
	masm_id int4 NULL,
	row_version int4 NULL DEFAULT 0,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL
);

-- Drop table

-- DROP TABLE user_registration_log.value_rights_modules_headers_log;

CREATE TABLE user_registration_log.value_rights_modules_headers_log (
	vrmh_log_id serial not NULL,
	vrmh_status varchar(50) NULL,
	vrmh_id int4 NULL,
	subs_id int4 NULL,
	vrmg_value_rights_from numeric(14,2) NULL,
	vrmh_value_rights_until numeric(14,2) NULL,
	vrmh_include_sheep_goat bool NULL DEFAULT false,
	row_version int4 NULL DEFAULT 0,
	usrinsert varchar(100) NULL,
	dteinsert timestamp NULL,
	usrupdate varchar(100) NULL,
	dteupdate timestamp NULL
);



-- Drop table

-- DROP TABLE ws_responses.ss_module_wss;

CREATE TABLE ws_responses.ss_module_wss (
	mows_id serial NOT NULL,
	modu_id int4 NOT NULL,
	mows_ws_path varchar(500) NOT NULL,
	CONSTRAINT mows_pk PRIMARY KEY (mows_id),
	CONSTRAINT mows_uk UNIQUE (modu_id, mows_ws_path)
);

-- Drop table

-- DROP TABLE ws_responses.ss_user_session_ws_responses;

CREATE TABLE ws_responses.ss_user_session_ws_responses (
	uswr_id int8 NOT NULL,
	usrs_id int8 NOT NULL,
	mows_id int4 NOT NULL,
	uswr_startts int8 NULL,
	uswr_duration_ms int4 NULL,
	uswr_success bool NULL,
	uswr_interval int4 NOT NULL,
	uswr_error_code varchar(10) NULL,
	subs_id int4 NULL
);

-- Drop table

-- DROP TABLE ws_responses.uswr_20180723;

CREATE TABLE ws_responses.uswr_20180723 (
	CONSTRAINT uswr_20180723_cc CHECK ((uswr_interval = 20180723))
)
INHERITS (ws_responses.ss_user_session_ws_responses);

-- Drop table

-- DROP TABLE ws_responses.uswr_20180725;

CREATE TABLE ws_responses.uswr_20180725 (
	CONSTRAINT uswr_20180725_cc CHECK ((uswr_interval = 20180725))
)
INHERITS (ws_responses.ss_user_session_ws_responses);

-- Drop table

-- DROP TABLE ws_responses.uswr_20180727;

CREATE TABLE ws_responses.uswr_20180727 (
	CONSTRAINT uswr_20180727_cc CHECK ((uswr_interval = 20180727))
)
INHERITS (ws_responses.ss_user_session_ws_responses);

-- Drop table

-- DROP TABLE ws_responses.uswr_20180730;

CREATE TABLE ws_responses.uswr_20180730 (
	CONSTRAINT uswr_20180730_cc CHECK ((uswr_interval = 20180730))
)
INHERITS (ws_responses.ss_user_session_ws_responses);

-- Drop table

-- DROP TABLE ws_responses.uswr_20181212;

CREATE TABLE ws_responses.uswr_20181212 (
	CONSTRAINT uswr_20181212_cc CHECK ((uswr_interval = 20181212))
)
INHERITS (ws_responses.ss_user_session_ws_responses);

-- Drop table

-- DROP TABLE ws_responses.uswr_20190416;

CREATE TABLE ws_responses.uswr_20190416 (
	CONSTRAINT uswr_20190416_cc CHECK ((uswr_interval = 20190416))
)
INHERITS (ws_responses.ss_user_session_ws_responses);

-- Drop table

-- DROP TABLE ws_responses.uswr_20200405;

CREATE TABLE ws_responses.uswr_20200405 (
	CONSTRAINT uswr_20200405_cc CHECK ((uswr_interval = 20200405))
)
INHERITS (ws_responses.ss_user_session_ws_responses);

-- Drop table

-- DROP TABLE ws_responses.uswr_20200722;

CREATE TABLE ws_responses.uswr_20200722 (
	CONSTRAINT uswr_20200722_cc CHECK ((uswr_interval = 20200722))
)
INHERITS (ws_responses.ss_user_session_ws_responses);

-- Drop table

-- DROP TABLE ws_responses.uswr_20200723;

CREATE TABLE ws_responses.uswr_20200723 (
	CONSTRAINT uswr_20200723_cc CHECK ((uswr_interval = 20200723))
)
INHERITS (ws_responses.ss_user_session_ws_responses);

-- Drop table

-- DROP TABLE ws_responses.uswr_20200724;

CREATE TABLE ws_responses.uswr_20200724 (
	CONSTRAINT uswr_20200724_cc CHECK ((uswr_interval = 20200724))
)
INHERITS (ws_responses.ss_user_session_ws_responses);

-- Drop table

-- DROP TABLE ws_responses.uswr_20200727;

CREATE TABLE ws_responses.uswr_20200727 (
	CONSTRAINT uswr_20200727_cc CHECK ((uswr_interval = 20200727))
)
INHERITS (ws_responses.ss_user_session_ws_responses);

-- Drop table

-- DROP TABLE ws_responses.uswr_20200728;

CREATE TABLE ws_responses.uswr_20200728 (
	CONSTRAINT uswr_20200728_cc CHECK ((uswr_interval = 20200728))
)
INHERITS (ws_responses.ss_user_session_ws_responses);

-- Drop table

-- DROP TABLE ws_responses.uswr_20200729;

CREATE TABLE ws_responses.uswr_20200729 (
	CONSTRAINT uswr_20200729_cc CHECK ((uswr_interval = 20200729))
)
INHERITS (ws_responses.ss_user_session_ws_responses);

-- Drop table

-- DROP TABLE ws_responses.uswr_20200731;

CREATE TABLE ws_responses.uswr_20200731 (
	CONSTRAINT uswr_20200731_cc CHECK ((uswr_interval = 20200731))
)
INHERITS (ws_responses.ss_user_session_ws_responses);

---------------------EXTRAS
CREATE OR REPLACE FUNCTION subscription.add_multipage_roles(i_subs_id integer, s_modu_name character varying)
 RETURNS void
 LANGUAGE plpgsql
AS $function$

declare
	wn_dummy integer;
begin


  if i_subs_id is null or
     	s_modu_name is null or s_modu_name = '' THEN
     raise 'Ανεπαρκή στοιχεία στην κλήση της διεργασίας.';
  end if;

  begin
    select subs.subs_id
      into strict wn_dummy
      from subscription.ss_subscribers subs
     where subs.subs_id = i_subs_id;
  exception
    when no_data_found THEN
    	raise 'Δεν βρέθηκε το subs_id ';
    when others then
    	raise;
  end;

  begin
    select modu.modu_id
      into strict wn_dummy
      from subscription.ss_modules modu
     where modu.modu_name = s_modu_name;
  exception
    when no_data_found THEN
    	raise 'Δεν βρέθηκε το s_modu_name ''%''', s_modu_name;
    when others then
    	raise;
  end;


  INSERT INTO subscription.ss_roles(subs_id, rost_id, role_caption, role_created_by_system, usrinsert, dteinsert)
  select distinct sumg.subs_id, 0, syro.syro_description, true, '#system@usermgmnt#', now()
    from subscription.ss_subscribers_module_groups sumg
         join subscription.ss_grouping_modules grmo on grmo.mogr_id = sumg.mogr_id
         join subscription.ss_modules modu on modu.modu_id = grmo.modu_id
       	 join subscription.ss_system_roles syro on syro.modu_id = modu.modu_id
   where sumg.subs_id = i_subs_id
     and modu.modu_name = s_modu_name
     and syro.syro_is_multipage_role is true
     and not exists (select 1
                       from subscription.ss_roles role
                      where role.subs_id = sumg.subs_id
                        and role.role_caption = syro.syro_description )
     and exists (select 1
     			   from subscription.ss_system_roles_privileges syrp
                    join subscription.ss_system_privileges_security_classes spsc on spsc.sypr_id = syrp.sypr_id
                    join subscription.ss_system_privileges sypr on sypr.sypr_id = spsc.sypr_id
                    join subscription.ss_subscribers_security_classes susc on susc.scls_id = spsc.scls_id
                  where syrp.syro_id = syro.syro_id
                    and susc.subs_id = sumg.subs_id
                    and sypr.sypr_name <> (modu.modu_name || '_index_R') );

  INSERT INTO
    subscription.ss_roles_system_roles
  ( role_id, syro_id, usrinsert, dteinsert)
  select role_id, syro_id, '#system@usermgmnt#', now()
    from (
  select distinct role.role_id, syro.syro_id
    from subscription.ss_roles role
          join subscription.ss_system_roles syro on syro.syro_description = role.role_caption
          join subscription.ss_modules modu on syro.modu_id = modu.modu_id
   where role.subs_id = i_subs_id
     and modu.modu_name = s_modu_name
     and syro.syro_is_multipage_role is true
     and exists (select 1
                   from subscription.ss_subscribers_module_groups sumg
                   		join subscription.ss_grouping_modules grmo on grmo.mogr_id = sumg.mogr_id
                  where sumg.subs_id = role.subs_id
                    and grmo.modu_id = modu.modu_id
                 )
     and exists (select 1
     			   from subscription.ss_system_roles_privileges syrp
                    join subscription.ss_system_privileges_security_classes spsc on spsc.sypr_id = syrp.sypr_id
                    join subscription.ss_system_privileges sypr on sypr.sypr_id = spsc.sypr_id
                    join subscription.ss_subscribers_security_classes susc on susc.scls_id = spsc.scls_id
                  where syrp.syro_id = syro.syro_id
                    and susc.subs_id = role.subs_id
                    and sypr.sypr_name <> (modu.modu_name || '_index_R') )
     and not exists (select 1
                       from subscription.ss_roles_system_roles rosr
                      where rosr.role_id = role.role_id
                        and rosr.syro_id = syro.syro_id)) AS foo;

exception
  when others then raise '%', sqlerrm;
end;
$function$
;

------DATA
INSERT INTO subscription.ss_subscriber_categories (suca_id,suca_name,row_version,dteinsert,usrinsert,dteupdate,usrupdate) VALUES 
(1,'Συνεταιρισμός',0,NULL,NULL,NULL,NULL)
,(2,'Ένωση Συνεταιρισμών',0,NULL,NULL,NULL,NULL)
,(3,'Παραγωγός',0,NULL,NULL,NULL,NULL)
,(4,'Γεωργός',0,NULL,NULL,NULL,NULL)
,(5,'Κτηνοτρόφος',0,NULL,NULL,NULL,NULL)
,(6,'Αλιέας',0,NULL,NULL,NULL,NULL)
,(7,'Σύμβουλος',0,NULL,NULL,NULL,NULL)
,(8,'Λογιστικό γραφείο',0,NULL,NULL,NULL,NULL)
,(9,'Γεωπόνος',0,NULL,NULL,NULL,NULL)
,(10,'Κτηνίατρος',0,NULL,NULL,NULL,NULL)
;
INSERT INTO subscription.ss_subscriber_categories (suca_id,suca_name,row_version,dteinsert,usrinsert,dteupdate,usrupdate) VALUES 
(11,'Μελετητής',0,NULL,NULL,NULL,NULL)
,(12,'Επιθεωρητής',0,NULL,NULL,NULL,NULL)
,(13,'Επιχείρηση',0,NULL,NULL,NULL,NULL)
,(14,'Επιχείρηση αγροτικής παραγωγής',0,NULL,NULL,NULL,NULL)
,(15,'Επιχείρηση μεταποιητική-τυποποιητική',0,NULL,NULL,NULL,NULL)
,(16,'Προμήθευτική Επιχείρηση',0,NULL,NULL,NULL,NULL)
,(17,'Εμπορική Επιχείρηση',0,NULL,NULL,NULL,NULL)
,(18,'Χρηματοδοτική - Επενδυτική Επιχείρηση',0,NULL,NULL,NULL,NULL)
,(19,'Δημόσιος φορέας',0,NULL,NULL,NULL,NULL)
,(20,'Τοπική  Αυτοδιοίκηση',0,NULL,NULL,NULL,NULL)
;
INSERT INTO subscription.ss_subscriber_categories (suca_id,suca_name,row_version,dteinsert,usrinsert,dteupdate,usrupdate) VALUES 
(21,'Δημόσια Διοίκηση',0,NULL,NULL,NULL,NULL)
,(22,'Πιστοποιητικός Φορέας',0,NULL,NULL,NULL,NULL)
,(23,'Ελεγκτικός Φορέας',0,NULL,NULL,NULL,NULL)
,(24,'Εκπαιδευτικός φορέας',0,NULL,NULL,NULL,NULL)
,(25,'Ερευνητικός Φορέας',0,NULL,NULL,NULL,NULL)
,(0,'Συστήματος',0,NULL,NULL,NULL,NULL)
;

INSERT INTO subscription.ss_subscriber_statuses (sust_id,sust_description,row_version,dteinsert,usrinsert,dteupdate,usrupdate) VALUES 
(0,'Ενεργός',0,NULL,NULL,NULL,NULL)
,(1,'Μπλοκαρισμένος από τον πάροχο',0,NULL,NULL,NULL,NULL)
,(2,'Διαγραμμένος',0,NULL,NULL,NULL,NULL)
;

INSERT INTO subscription.ss_subscribers_module_group_status (smgs_id,smgs_description,row_version,dteinsert,usrinsert,dteupdate,usrupdate) VALUES 
(0,'Ενεργό',0,NULL,NULL,NULL,NULL)
,(1,'Εκτός Λειτουργίας από τον πάροχο',0,NULL,NULL,NULL,NULL)
,(2,'Εκτός Λειτουργίας από τον διαχειριστή',0,NULL,NULL,NULL,NULL)
;

INSERT INTO subscription.ss_security_classes (scls_id,scls_description,row_version,dteinsert,usrinsert,dteupdate,usrupdate,scls_public_description,scls_code) VALUES 
(0,'ONLY FOR NEUROPUBLIC',0,NULL,NULL,NULL,NULL,'ONLY FOR NEUROPUBLIC',0)
,(1,'ACCESS TO MULTIPLE SUBSCRIBERS DATA',0,NULL,NULL,NULL,NULL,'ACCESS TO MULTIPLE SUBSCRIBERS DATA',1)
,(2,'SINGLE SUBSCRIBER ACCESS',0,NULL,NULL,NULL,NULL,'SINGLE SUBSCRIBER ACCESS',2)
,(3,'SINGLE USER ACCESS',0,NULL,NULL,NULL,NULL,'SINGLE USER ACCESS',3)
,(4,'SINGLE SUBSCRIBER ACCESS PLUS',0,NULL,NULL,NULL,NULL,'SINGLE SUBSCRIBER ACCESS PLUS',4)
,(1000,'USRMNG_ADMIN',0,'2016-06-16 16:44:42.000',NULL,NULL,NULL,'ΔΙΑΧΕΙΡΙΣΤΗΣ ΔΙΑΧΕΙΡΙΣΗΣ ΧΡΗΣΤΩΝ',1000)
,(1002,'USRMNG_LOCAL',0,'2016-06-17 13:14:28.000',NULL,NULL,NULL,'ΤΟΠΙΚΟΣ ΔΙΑΧΕΙΡΙΣΤΗΣ ΔΙΑΧΕΙΡΙΣΗΣ ΧΡΗΣΤΩΝ',1002)
;


INSERT INTO subscription.ss_user_statuses (usst_id,usst_description,row_version,dteinsert,usrinsert,dteupdate,usrupdate) VALUES 
(0,'Ενεργός',0,NULL,NULL,NULL,NULL)
,(1,'Μπλοκαρισμένος από τον πάροχο',0,NULL,NULL,NULL,NULL)
,(2,'Μπλοκαρισμένος από τον διαχειριστή',0,NULL,NULL,NULL,NULL)
,(3,'Διαγραμμένος',0,NULL,NULL,NULL,NULL)
,(4,'Προς Ενεργοποίηση',0,NULL,NULL,NULL,NULL)
,(10,'Κλειδωμένος προσωρινά από το σύστημα',0,NULL,NULL,NULL,NULL)
,(20,'Ανενεργός λόγω Αδράνειας 90 ημερών',0,NULL,NULL,NULL,NULL)
;


INSERT INTO subscription.ss_modules (modu_name,modu_isfree,modu_ulr_suffix,modu_description,modu_isforclient,row_version,dteinsert,usrinsert,dteupdate,usrupdate,modu_ghost_user_allowed,modu_is_ws_responseinfo_enabled) VALUES 
('UsrMng',false,'UsrMng','Διαχείριση Χρηστών',false,0,NULL,NULL,NULL,NULL,NULL,true)
;

INSERT INTO subscription.ss_system_roles (syro_name,syro_description,syro_is_multipage_role,modu_id,row_version,dteinsert,usrinsert,dteupdate,usrupdate) VALUES 
('UsrMng_admin','Ρόλος Διαχειριστή για την εφαρμογή Διαχείριση Συνδρομής',true,(select modu_id from "subscription".ss_modules sm where modu_name like 'UsrMng'),0,NULL,NULL,NULL,NULL)
;

INSERT INTO subscription.ss_role_statuses (rost_id,rost_description,row_version,dteinsert,usrinsert,dteupdate,usrupdate) VALUES 
(0,'Ενεργός',0,NULL,NULL,NULL,NULL)
,(1,'Μπλοκαρισμένος από τον πάροχο',0,NULL,NULL,NULL,NULL)
,(2,'Μπλοκαρισμένος από το διαχειριστή',0,NULL,NULL,NULL,NULL)
,(3,'Διαγραμμένος',0,NULL,NULL,NULL,NULL)
;

--INSERT INTO "subscription".ss_roles (subs_id, rost_id, role_caption, row_version, role_created_by_system) 
--VALUES(12, 0, 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System', 0,  false);


CREATE OR REPLACE VIEW subscription.ss_password_policy_parameters
AS SELECT 1 AS view_key, 1 AS active_flag, 30 AS lock_expiration_mins, 
    90 * 24 * 60 * 60 AS password_expiration_secs, 
    7 * 24 * 60 * 60 AS password_expiration_alert_secs;
	

CREATE OR REPLACE VIEW subscription.ssv_gaiaportal_users
AS SELECT usr.user_id AS view_key, usr.user_id, usr.usst_id, usr.subs_id, 
    usr.user_is_administrator, usr.user_email, usr.user_password, 
    usr.user_password_salt, usr.user_firstname, usr.user_surname, 
    usr.user_nickname, usr.user_vat, usr.user_phone, usr.user_phone_cell, 
    usr.user_address, usr.user_address_number, usr.user_postal_code, 
    usr.user_city, usr.user_prefecture, usr.user_country, usr.user_comments, 
    usr.user_portal_id, usr.user_registered_from_id, 
    usr.user_agree_for_transmission, usr.user_registration_date, 
    usr.user_last_login_date, usr.user_invoice_type, usr.user_identity_number, 
    usr.subs_registered_from_id, usr.user_payment_method, usr.row_version, 
    usr.dteinsert, usr.usrinsert, usr.dteupdate, usr.usrupdate, 
    usr.user_active_email
   FROM subscription.ss_users usr
  WHERE NOT (EXISTS ( SELECT 1
           FROM user_registration.temp_users_registration_module b
          WHERE b.user_id = usr.user_id AND b.is_gaia_user = false)) AND NOT (EXISTS ( SELECT 1
           FROM subscription.ss_subscribers subs
          WHERE subs.subs_id = usr.subs_id AND subs.subs_short_name::text ~~ 'ΠΥΛΗΑ %'::text));

CREATE OR REPLACE FUNCTION subscription.get_encrypted_pwd(i_pwd_algorithm integer, s_pwd character varying)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$
begin
	if i_pwd_algorithm is not null and i_pwd_algorithm = 2 then
    	return subscription.getsha1_custom1(s_pwd);
    ELSE
    	return 'no_supported_pwd_algorithm';
    end if;
end;
$function$
;


CREATE OR REPLACE VIEW subscription.ssv_subscriber_modules
AS SELECT a.subs_id, a.subs_short_name, a.sust_id, a.suca_id, b.sumg_id, 
    b.smgs_id, c.mogr_name, b.sumg_valid_from, b.sumg_valid_until, e.modu_id, 
    e.modu_name, e.modu_ulr_suffix, e.modu_description, a.subs_legal_name
   FROM subscription.ss_subscribers a
   JOIN subscription.ss_subscribers_module_groups b ON b.subs_id = a.subs_id
   JOIN subscription.ss_module_groups c ON c.mogr_id = b.mogr_id
   JOIN subscription.ss_grouping_modules d ON d.mogr_id = c.mogr_id
   JOIN subscription.ss_modules e ON e.modu_id = d.modu_id;


CREATE OR REPLACE VIEW subscription.ssv_user_privilleges
AS SELECT a.user_id, a.user_email, k.modu_id, k.modu_name, j.sypr_name
   FROM subscription.ss_users a
   JOIN subscription.ss_users_roles b ON a.user_id = b.user_id
   JOIN subscription.ss_roles c ON b.role_id = c.role_id
   JOIN subscription.ss_roles_system_roles d ON c.role_id = d.role_id
   JOIN subscription.ss_system_roles e ON d.syro_id = e.syro_id
   JOIN subscription.ss_system_roles_privileges f ON f.syro_id = e.syro_id
   JOIN subscription.ss_subscribers_security_classes h ON h.subs_id = a.subs_id
   JOIN subscription.ss_system_privileges_security_classes i ON i.scls_id = h.scls_id AND i.sypr_id = f.sypr_id
   JOIN subscription.ss_system_privileges j ON j.sypr_id = f.sypr_id AND j.sypr_id = i.sypr_id
   JOIN subscription.ss_modules k ON k.modu_id = j.modu_id
  WHERE c.rost_id = 0;


CREATE OR REPLACE VIEW subscription.ssv_user_modules
AS SELECT usr.user_id, usr.user_email, usr.usst_id, usr.subs_id, 
    subs.subs_short_name, subs.sust_id, subs.suca_id, s.sumg_id, s.smgs_id, 
    s.mogr_name, s.sumg_valid_from, s.sumg_valid_until, 
        CASE
            WHEN s.modu_id IS NOT NULL THEN true
            ELSE false
        END AS subscriber_has_the_module, 
        CASE
            WHEN s.smgs_id IS NOT NULL AND s.smgs_id = 0 THEN true
            ELSE false
        END AS subsciber_module_group_is_active, 
        CASE
            WHEN s.sumg_valid_from IS NOT NULL AND now() >= s.sumg_valid_from AND s.sumg_valid_until IS NOT NULL AND s.sumg_valid_until >= now() THEN true
            ELSE false
        END AS subsciber_module_group_is_valid, 
        CASE
            WHEN usmo.modu_id IS NOT NULL AND (up.modu_name IS NOT NULL OR (modu.modu_name::text = ANY (ARRAY['GAIA-NET'::text, 'GAIA-PEDIA'::text, 'GAIA-LEARNING'::text, 'GAIA-DIPLOMA'::text, 'GAIA-Β.Ι'::text, 'Ferticility'::text, 'SOD'::text, 'fieldMonitor'::text]))) THEN true
            ELSE false
        END AS user_has_the_module, 
    modu.modu_id, modu.modu_name, modu.modu_ulr_suffix, modu.modu_description
   FROM subscription.ss_users usr
   JOIN subscription.ss_subscribers subs ON subs.subs_id = usr.subs_id
  CROSS JOIN subscription.ss_modules modu
   LEFT JOIN subscription.ssv_subscriber_modules s ON s.subs_id = usr.subs_id AND s.modu_id = modu.modu_id
   LEFT JOIN subscription.ss_user_modules usmo ON usmo.user_id = usr.user_id AND usmo.modu_id = modu.modu_id
   LEFT JOIN ( SELECT a.user_email, a.modu_name
   FROM subscription.ssv_user_privilleges a
  GROUP BY a.user_email, a.modu_name) up ON up.user_email::text = usr.user_email::text AND up.modu_name::text = modu.modu_name::text;


CREATE OR REPLACE VIEW subscription.ssv_user_by_id
AS SELECT usr.user_id, usr.user_email AS internal_user_name, 
        CASE
            WHEN usma.usma_public_email IS NOT NULL THEN usma.usma_public_email
            ELSE usr.user_email
        END AS public_user_name, 
    usr.user_active_email, usr.user_vat AS tin, usr.user_surname AS lastname, 
    usr.user_firstname AS firstname, usr.user_fathername AS fathername, 
    subs.subs_id, subs.subs_short_name AS subs_code, 
    subs.subs_legal_name AS subs_description
   FROM subscription.ss_users usr
   JOIN subscription.ss_subscribers subs ON subs.subs_id = usr.subs_id
   LEFT JOIN subscription.ss_users_mapping usma ON usma.user_id = usr.user_id;  
   
  
INSERT INTO user_registration.subs_vat_class (suvc_id,suvc_description,row_version) VALUES 
(1,'Normal',0)
;

INSERT INTO subscription.ss_user_session_logoutcodes (uslc_id,uslc_name,uslc_desc,row_version,dteinsert,usrinsert,dteupdate,usrupdate) VALUES 
(1,'user','logout requested by end-user',0,NULL,NULL,NULL,NULL)
,(2,'expiry','logout requested by web/application server upon session expiry',0,NULL,NULL,NULL,NULL)
,(3,'concurrent','logout by usermngmt application due to concurrent access from newer session past the retention threshold',0,NULL,NULL,NULL,NULL)
;


-- DROP TYPE ws_responses.ws_response_info;

CREATE TYPE ws_responses.ws_response_info AS (
	ws_path varchar,
	ws_startts int8,
	ws_endts int8,
	ws_success bool,
	ws_error_code varchar);


CREATE OR REPLACE FUNCTION ws_responses.insert_user_session_ws_responses_aux(s_modu_name character varying, s_session character varying, t_ws_response_info text, i_subs_id integer DEFAULT NULL::integer)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
declare
  a_ws_response_info VARCHAR[];
  wa_ws_response_info ws_responses.ws_response_info[];
begin

  if coalesce(s_modu_name, '') <> '' and
     coalesce(s_session, '') <> '' and
     coalesce(t_ws_response_info, '') <> '' then

      select string_to_array(t_ws_response_info, '#@#')
        into a_ws_response_info;

      select array_agg(r) 
        into wa_ws_response_info
        from (
      select row(a[1]::varchar, a[2]::bigint, a[3]::bigint, a[4]::boolean, coalesce(a[5], '')::varchar)::ws_responses.ws_response_info as r
      from (select string_to_array(ws_string, ',') as a
              from (select unnest(a_ws_response_info) as ws_string) as foo) as foo1) as f002;

      perform ws_responses.insert_user_session_ws_responses (
        s_modu_name,
        s_session,
        wa_ws_response_info,
        i_subs_id);

  end if;
exception
  when others then raise '%', sqlerrm;
end;
$function$
;


CREATE OR REPLACE FUNCTION ws_responses.create_and_drop_past_day_uswr_partition(i_partition_key integer, i_last_days_to_keep integer)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
declare
  partition_created BOOLEAN;
begin
  partition_created := ws_responses.create_uswr_partition(i_partition_key);
  if partition_created then
    perform ws_responses.drop_uswr_day_partitions(i_last_days_to_keep);
  end if;
END;
$function$
;


CREATE OR REPLACE FUNCTION ws_responses.insert_user_session_ws_responses(s_modu_name character varying, s_session character varying, a_ws_response_info ws_responses.ws_response_info[], i_subs_id integer DEFAULT NULL::integer)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
declare
  wn_modu_id integer;
  wn_usrs_id BIGINT;
  wn_interval integer := to_char(now(), 'YYYYMMDD')::INTEGER;
  wn_startts_ms BIGINT := round(EXTRACT(epoch from now())*1000);
  wn_partiotions_to_keep integer := 10;
begin
  select modu.modu_id
    into strict wn_modu_id
    from subscription.ss_modules modu
   where modu.modu_name = s_modu_name;

  select usrs.usrs_id
    into strict wn_usrs_id
    from subscription.ss_user_sessions usrs
   where usrs.usrs_session = s_session
     and usrs.usrs_app = s_modu_name ;

  insert into ws_responses.ss_module_wss(modu_id, mows_ws_path)
  with ws_resp_info as (select distinct ws.ws_path
                FROM  unnest(a_ws_response_info) ws)
  select wn_modu_id, ws.ws_path
    from ws_resp_info ws
   where not exists (select 1
                       from ws_responses.ss_module_wss mows
                      where mows.modu_id = wn_modu_id
                        and mows.mows_ws_path = ws.ws_path
                     );

  perform ws_responses.create_and_drop_past_day_uswr_partition(wn_interval, wn_partiotions_to_keep);
  insert into ws_responses.ss_user_session_ws_responses(
      usrs_id,
      mows_id,
      uswr_startts,
      uswr_duration_ms,
      uswr_success,
      uswr_interval,
      uswr_error_code,
      subs_id)
  with ws_resp_info as (select ws.ws_path, ws.ws_startts, ws.ws_endts, ws.ws_success, ws.ws_error_code
                FROM  unnest(a_ws_response_info) ws)
  select wn_usrs_id, mows.mows_id,
         wn_startts_ms,
         (ws.ws_endts - ws.ws_startts), ws.ws_success,
         wn_interval,
         case
         	when coalesce(ws.ws_error_code, '') = '' then null
          	else ws.ws_error_code
         end as error_code,
         i_subs_id
    from ws_resp_info ws
      join ws_responses.ss_module_wss mows on
          mows.mows_ws_path = ws.ws_path and
          mows.modu_id = wn_modu_id;

exception
  when others then raise '%', sqlerrm;
end;
$function$
;
CREATE OR REPLACE FUNCTION ws_responses.ss_user_session_ws_responses_bi_tr_f()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
DECLARE
  wn_cnt integer;
  wn_interval integer;
  ws_insrt_qry VARCHAR;
  ws_table_name VARCHAR;
  ws_schema_name varchar;
BEGIN
    wn_interval := new.uswr_interval;
    ws_schema_name := 'ws_responses';
    ws_table_name := 'uswr_' || wn_interval::VARCHAR;

	ws_insrt_qry := 'insert into ' || ws_schema_name || '.' || ws_table_name || '
    				 (usrs_id, mows_id, uswr_startts, uswr_duration_ms, uswr_success, uswr_interval, uswr_error_code, subs_id)
                     values ($1.usrs_id, $1.mows_id, $1.uswr_startts, $1.uswr_duration_ms, $1.uswr_success, $1.uswr_interval, $1.uswr_error_code, $1.subs_id)';

    execute ws_insrt_qry using NEW;
    RETURN NULL;
END;
$function$
;
CREATE OR REPLACE FUNCTION ws_responses.create_uswr_partition(i_partition_key integer)
 RETURNS boolean
 LANGUAGE plpgsql
AS $function$
DECLARE
	wn_cnt integer;
    s_schema_name VARCHAR := 'ws_responses';
    s_table_name varchar;
BEGIN
    s_table_name := 'uswr_'||i_partition_key::varchar;
    select count(1)
      into wn_cnt
      from information_schema.tables a
     where a.table_schema = s_schema_name
       and a.table_name = s_table_name
       and a.table_type = 'BASE TABLE';

	if wn_cnt = 0 then

      select count(1)
        into wn_cnt
        from information_schema.sequences a
       where a.sequence_schema = s_schema_name
         and a.sequence_name = s_table_name||'_id_seq';

	  if wn_cnt = 0 then
	   execute '
        CREATE SEQUENCE '||s_schema_name||'.'||s_table_name||'_id_seq
          INCREMENT 1 MINVALUE 1
          MAXVALUE 9223372036854775807 START 1
          CACHE 1 CYCLE;';

      end if;

	   execute '
       CREATE TABLE '||s_schema_name||'.'||s_table_name||' (
        ) INHERITS (ws_responses.ss_user_session_ws_responses)
        ;

        alter table '||s_schema_name||'.'||s_table_name||'
          add constraint '||s_table_name||'_cc check (uswr_interval = '||i_partition_key||');

        ALTER TABLE '||s_schema_name||'.'||s_table_name||'
          ALTER COLUMN uswr_id SET DEFAULT nextval('''||s_schema_name||'.'||s_table_name||'_id_seq''::regclass);
       ';
       
       return true;
	else
       return false;
    end if;
    
END;
$function$
;

CREATE OR REPLACE FUNCTION ws_responses.drop_uswr_day_partitions(i_last_days_to_keep integer)
 RETURNS void
 LANGUAGE plpgsql
AS $function$
declare
  tbls record;
BEGIN
  for tbls in (
    select 'uswr_' || to_char(a, 'YYYYMMDD') as table_name
--  	  from generate_series(now() - (i_last_days_to_keep + interval '5 days'), now() - i_last_days_to_keep, interval '1 day') a
  	  from generate_series(now() - (concat(i_last_days_to_keep+5,' days')::interval), now() - (concat(i_last_days_to_keep,' days')::interval), interval '1 day') a
    ) loop

    begin
      execute 'drop table ws_responses.' || tbls.table_name;
    exception
      when undefined_table then
    end;

    begin
      execute 'drop sequence ws_responses.' || tbls.table_name || '_id_seq';
    exception
      when undefined_table then
    end;

  end loop;
END;
$function$
;

-- DROP TRIGGER ss_user_session_ws_responses_bi_tr ON ws_responses.ss_user_session_ws_responses;

create trigger ss_user_session_ws_responses_bi_tr before
insert
    on
    ws_responses.ss_user_session_ws_responses for each row execute function ws_responses.ss_user_session_ws_responses_bi_tr_f();


-- Drop table

-- DROP TABLE ws_responses.dual;

CREATE TABLE ws_responses.dual (
	dummy varchar(50) NULL
);
