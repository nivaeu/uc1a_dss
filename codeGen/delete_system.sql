DELETE 
FROM subscription.ss_grouping_modules 
WHERE 
    modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva');


DELETE 
FROM subscription.ss_system_roles_privileges 
WHERE 
    syro_id in (SELECT syro_id FROM subscription.ss_system_roles where modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));


insert into subscription.exceptions(ex_descr,ex_type)
select 'ss_roles_bud_notallowed_tr', rosr.role_id
  from subscription.ss_roles_system_roles rosr
  	join subscription.ss_roles role on role.role_id = rosr.role_id
 WHERE rosr.syro_id in (select syro.syro_id from subscription.ss_system_roles syro WHERE syro.modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'))
   and role.role_created_by_system is true;

DELETE
  FROM subscription.ss_roles_system_roles rosr
 WHERE rosr.syro_id in (select syro.syro_id from subscription.ss_system_roles syro WHERE syro.modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'))
   and exists (select 1 from subscription.ss_roles role where role.role_id = rosr.role_id and role.role_created_by_system is true);

delete from subscription.exceptions;



DELETE 
FROM subscription.ss_system_roles WHERE modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva');	

DELETE 
  FROM subscription.ss_system_privileges_security_classes a
 where a.sypr_id in (select b.sypr_id from subscription.ss_system_privileges b WHERE b.sypr_name like 'Niva%');

DELETE 
FROM subscription.ss_system_privileges WHERE sypr_name like 'Niva%';

DELETE 
FROM subscription.ss_modules WHERE modu_name='Niva';