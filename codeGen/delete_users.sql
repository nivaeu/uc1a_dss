DELETE 
FROM subscription.ss_users_roles 
WHERE 	user_id in (SELECT user_id FROM subscription.ss_users where user_email like '%@Niva');

insert into subscription.exceptions (ex_descr, ex_type)
 select 'ss_roles_bud_notallowed_tr', role_id
   from subscription.ss_roles
  where subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name like '%Niva');

DELETE 
FROM subscription.ss_roles_system_roles 
WHERE 
    role_id in (
        SELECT role_id 
        FROM subscription.ss_roles 
            where subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name like '%Niva') );
			    
DELETE 
FROM subscription.ss_roles 
    where subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name like '%Niva');

delete from subscription.exceptions
 where ex_descr = 'ss_roles_bud_notallowed_tr';

DELETE 
FROM subscription.ss_user_modules
WHERE 
    user_id in (SELECT user_id FROM subscription.ss_users where user_email like '%@Niva');
	


DELETE
FROM subscription.ss_subscriber_invitations
WHERE
	user_id in 
	(SELECT user_id
	 FROM subscription.ss_users
	WHERE	subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'));

DELETE 
FROM subscription.ss_users
WHERE
	subs_id in
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva');

DELETE 
FROM subscription.ss_subscribers_module_groups WHERE subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name = 'TEST_SUBSCRIBER_FOR_Niva');

DELETE 
FROM subscription.ss_subscribers 
WHERE subs_short_name = 'TEST_SUBSCRIBER_FOR_Niva';
