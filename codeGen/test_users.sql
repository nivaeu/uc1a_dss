-- Test data for user management
-- add subscriber
INSERT INTO subscription.ss_subscribers
	(sust_id, suca_id, subs_short_name, subs_vat, subs_legal_name, subs_tax_office, subs_occupation, subs_phone, subs_address, subs_address_number, subs_postal_code, subs_city, suvc_id, subs_company_vat)
	SELECT 
		0, 1, 'TEST_SUBSCRIBER_FOR_Niva', '113192115', 'TEST_SUBSCRIBER_FOR_Niva', 'TAX_OFFICE', '-', '210', 'subs_address', '9', 'aa', 'aa', 1, '113192115'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_subscribers WHERE subs_short_name = 'TEST_SUBSCRIBER_FOR_Niva');

INSERT INTO subscription.ss_subscribers_module_groups(
            subs_id, mogr_id, smgs_id, sumg_valid_from, sumg_valid_until, 
            sumg_max_users, sumg_max_customers)
  SELECT
	(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name = 'TEST_SUBSCRIBER_FOR_Niva'),
	(SELECT mogr_id FROM subscription.ss_module_groups WHERE mogr_name='ALL MODULES'), -- module group
	0, -- subscriber_module_group_status
	'20120101',
	'21000101',
	100,100
    WHERE NOT EXISTS (
        SELECT * 
        FROM subscription.ss_subscribers_module_groups 
        WHERE 
            subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name = 'TEST_SUBSCRIBER_FOR_Niva')
            AND
            mogr_id in (SELECT mogr_id FROM subscription.ss_module_groups WHERE mogr_name='ALL MODULES') );


--Associate subscriber with Security class 2
INSERT INTO subscription.ss_subscribers_security_classes
        (subs_id, scls_id)
SELECT 
    (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name = 'TEST_SUBSCRIBER_FOR_Niva'),
    2
WHERE NOT EXISTS (SELECT * FROM  subscription.ss_subscribers_security_classes 
                  WHERE subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name = 'TEST_SUBSCRIBER_FOR_Niva') AND scls_id = 2);


select subscription.add_multipage_roles(subs.subs_id, 'Niva')
  from subscription.ss_subscribers subs
 where subs.subs_short_name = 'TEST_SUBSCRIBER_FOR_Niva';


-- add user: superadmin
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'superadmin@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'superadmin',
		'superadmin',
		'superadmin'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='superadmin@Niva');

-- associate user 'superadmin@Niva' and role 'Ρόλος SuperAdmin για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'superadmin@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος SuperAdmin για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'superadmin@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος SuperAdmin για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'superadmin@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'superadmin@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'superadmin@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: admin
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'admin@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'admin',
		'admin',
		'admin'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='admin@Niva');

-- associate user 'admin@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'admin@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'admin@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'admin@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'admin@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'admin@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: guest
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'guest@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'guest',
		'guest',
		'guest'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='guest@Niva');

-- associate user 'guest@Niva' and role 'Ρόλος Επισκέπτη για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'guest@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Επισκέπτη για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'guest@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Επισκέπτη για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'guest@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'guest@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'guest@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u1
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u1@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u1',
		'u1',
		'u1'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u1@Niva');

-- associate user 'u1@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u1@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u1@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u1@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u1@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u1@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u2
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u2@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u2',
		'u2',
		'u2'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u2@Niva');

-- associate user 'u2@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u2@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u2@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u2@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u2@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u2@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u3
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u3@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u3',
		'u3',
		'u3'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u3@Niva');

-- associate user 'u3@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u3@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u3@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u3@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u3@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u3@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u4
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u4@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u4',
		'u4',
		'u4'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u4@Niva');

-- associate user 'u4@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u4@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u4@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u4@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u4@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u4@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u5
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u5@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u5',
		'u5',
		'u5'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u5@Niva');

-- associate user 'u5@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u5@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u5@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u5@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u5@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u5@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u6
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u6@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u6',
		'u6',
		'u6'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u6@Niva');

-- associate user 'u6@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u6@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u6@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u6@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u6@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u6@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u7
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u7@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u7',
		'u7',
		'u7'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u7@Niva');

-- associate user 'u7@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u7@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u7@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u7@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u7@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u7@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u8
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u8@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u8',
		'u8',
		'u8'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u8@Niva');

-- associate user 'u8@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u8@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u8@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u8@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u8@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u8@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u9
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u9@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u9',
		'u9',
		'u9'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u9@Niva');

-- associate user 'u9@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u9@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u9@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u9@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u9@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u9@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u10
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u10@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u10',
		'u10',
		'u10'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u10@Niva');

-- associate user 'u10@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u10@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u10@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u10@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u10@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u10@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u11
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u11@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u11',
		'u11',
		'u11'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u11@Niva');

-- associate user 'u11@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u11@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u11@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u11@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u11@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u11@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u12
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u12@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u12',
		'u12',
		'u12'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u12@Niva');

-- associate user 'u12@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u12@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u12@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u12@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u12@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u12@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u13
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u13@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u13',
		'u13',
		'u13'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u13@Niva');

-- associate user 'u13@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u13@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u13@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u13@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u13@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u13@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u14
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u14@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u14',
		'u14',
		'u14'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u14@Niva');

-- associate user 'u14@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u14@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u14@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u14@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u14@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u14@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u15
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u15@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u15',
		'u15',
		'u15'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u15@Niva');

-- associate user 'u15@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u15@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u15@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u15@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u15@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u15@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u16
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u16@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u16',
		'u16',
		'u16'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u16@Niva');

-- associate user 'u16@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u16@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u16@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u16@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u16@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u16@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u17
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u17@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u17',
		'u17',
		'u17'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u17@Niva');

-- associate user 'u17@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u17@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u17@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u17@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u17@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u17@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u18
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u18@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u18',
		'u18',
		'u18'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u18@Niva');

-- associate user 'u18@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u18@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u18@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u18@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u18@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u18@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u19
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u19@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u19',
		'u19',
		'u19'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u19@Niva');

-- associate user 'u19@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u19@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u19@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u19@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u19@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u19@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u20
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u20@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u20',
		'u20',
		'u20'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u20@Niva');

-- associate user 'u20@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u20@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u20@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u20@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u20@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u20@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u21
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u21@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u21',
		'u21',
		'u21'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u21@Niva');

-- associate user 'u21@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u21@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u21@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u21@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u21@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u21@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u22
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u22@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u22',
		'u22',
		'u22'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u22@Niva');

-- associate user 'u22@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u22@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u22@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u22@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u22@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u22@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u23
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u23@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u23',
		'u23',
		'u23'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u23@Niva');

-- associate user 'u23@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u23@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u23@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u23@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u23@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u23@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u24
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u24@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u24',
		'u24',
		'u24'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u24@Niva');

-- associate user 'u24@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u24@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u24@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u24@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u24@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u24@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u25
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u25@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u25',
		'u25',
		'u25'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u25@Niva');

-- associate user 'u25@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u25@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u25@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u25@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u25@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u25@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u26
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u26@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u26',
		'u26',
		'u26'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u26@Niva');

-- associate user 'u26@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u26@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u26@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u26@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u26@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u26@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u27
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u27@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u27',
		'u27',
		'u27'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u27@Niva');

-- associate user 'u27@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u27@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u27@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u27@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u27@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u27@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u28
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u28@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u28',
		'u28',
		'u28'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u28@Niva');

-- associate user 'u28@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u28@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u28@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u28@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u28@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u28@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u29
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u29@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u29',
		'u29',
		'u29'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u29@Niva');

-- associate user 'u29@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u29@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u29@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u29@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u29@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u29@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u30
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u30@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u30',
		'u30',
		'u30'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u30@Niva');

-- associate user 'u30@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u30@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u30@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u30@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u30@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u30@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u31
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u31@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u31',
		'u31',
		'u31'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u31@Niva');

-- associate user 'u31@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u31@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u31@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u31@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u31@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u31@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u32
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u32@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u32',
		'u32',
		'u32'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u32@Niva');

-- associate user 'u32@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u32@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u32@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u32@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u32@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u32@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u33
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u33@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u33',
		'u33',
		'u33'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u33@Niva');

-- associate user 'u33@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u33@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u33@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u33@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u33@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u33@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u34
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u34@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u34',
		'u34',
		'u34'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u34@Niva');

-- associate user 'u34@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u34@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u34@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u34@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u34@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u34@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u35
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u35@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u35',
		'u35',
		'u35'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u35@Niva');

-- associate user 'u35@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u35@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u35@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u35@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u35@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u35@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u36
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u36@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u36',
		'u36',
		'u36'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u36@Niva');

-- associate user 'u36@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u36@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u36@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u36@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u36@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u36@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u37
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u37@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u37',
		'u37',
		'u37'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u37@Niva');

-- associate user 'u37@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u37@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u37@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u37@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u37@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u37@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u38
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u38@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u38',
		'u38',
		'u38'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u38@Niva');

-- associate user 'u38@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u38@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u38@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u38@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u38@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u38@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u39
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u39@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u39',
		'u39',
		'u39'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u39@Niva');

-- associate user 'u39@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u39@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u39@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u39@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u39@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u39@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u40
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u40@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u40',
		'u40',
		'u40'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u40@Niva');

-- associate user 'u40@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u40@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u40@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u40@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u40@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u40@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u41
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u41@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u41',
		'u41',
		'u41'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u41@Niva');

-- associate user 'u41@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u41@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u41@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u41@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u41@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u41@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u42
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u42@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u42',
		'u42',
		'u42'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u42@Niva');

-- associate user 'u42@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u42@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u42@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u42@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u42@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u42@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u43
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u43@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u43',
		'u43',
		'u43'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u43@Niva');

-- associate user 'u43@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u43@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u43@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u43@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u43@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u43@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u44
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u44@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u44',
		'u44',
		'u44'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u44@Niva');

-- associate user 'u44@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u44@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u44@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u44@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u44@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u44@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u45
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u45@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u45',
		'u45',
		'u45'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u45@Niva');

-- associate user 'u45@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u45@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u45@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u45@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u45@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u45@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u46
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u46@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u46',
		'u46',
		'u46'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u46@Niva');

-- associate user 'u46@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u46@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u46@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u46@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u46@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u46@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u47
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u47@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u47',
		'u47',
		'u47'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u47@Niva');

-- associate user 'u47@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u47@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u47@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u47@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u47@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u47@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u48
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u48@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u48',
		'u48',
		'u48'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u48@Niva');

-- associate user 'u48@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u48@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u48@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u48@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u48@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u48@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u49
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u49@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u49',
		'u49',
		'u49'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u49@Niva');

-- associate user 'u49@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u49@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u49@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u49@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u49@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u49@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u50
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u50@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u50',
		'u50',
		'u50'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u50@Niva');

-- associate user 'u50@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u50@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u50@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u50@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u50@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u50@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u51
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u51@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u51',
		'u51',
		'u51'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u51@Niva');

-- associate user 'u51@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u51@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u51@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u51@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u51@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u51@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u52
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u52@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u52',
		'u52',
		'u52'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u52@Niva');

-- associate user 'u52@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u52@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u52@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u52@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u52@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u52@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u53
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u53@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u53',
		'u53',
		'u53'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u53@Niva');

-- associate user 'u53@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u53@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u53@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u53@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u53@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u53@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u54
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u54@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u54',
		'u54',
		'u54'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u54@Niva');

-- associate user 'u54@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u54@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u54@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u54@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u54@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u54@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u55
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u55@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u55',
		'u55',
		'u55'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u55@Niva');

-- associate user 'u55@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u55@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u55@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u55@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u55@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u55@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u56
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u56@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u56',
		'u56',
		'u56'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u56@Niva');

-- associate user 'u56@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u56@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u56@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u56@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u56@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u56@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u57
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u57@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u57',
		'u57',
		'u57'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u57@Niva');

-- associate user 'u57@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u57@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u57@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u57@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u57@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u57@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u58
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u58@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u58',
		'u58',
		'u58'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u58@Niva');

-- associate user 'u58@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u58@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u58@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u58@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u58@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u58@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u59
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u59@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u59',
		'u59',
		'u59'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u59@Niva');

-- associate user 'u59@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u59@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u59@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u59@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u59@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u59@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u60
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u60@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u60',
		'u60',
		'u60'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u60@Niva');

-- associate user 'u60@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u60@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u60@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u60@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u60@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u60@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u61
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u61@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u61',
		'u61',
		'u61'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u61@Niva');

-- associate user 'u61@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u61@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u61@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u61@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u61@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u61@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u62
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u62@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u62',
		'u62',
		'u62'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u62@Niva');

-- associate user 'u62@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u62@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u62@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u62@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u62@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u62@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u63
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u63@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u63',
		'u63',
		'u63'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u63@Niva');

-- associate user 'u63@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u63@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u63@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u63@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u63@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u63@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u64
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u64@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u64',
		'u64',
		'u64'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u64@Niva');

-- associate user 'u64@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u64@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u64@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u64@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u64@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u64@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u65
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u65@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u65',
		'u65',
		'u65'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u65@Niva');

-- associate user 'u65@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u65@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u65@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u65@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u65@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u65@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u66
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u66@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u66',
		'u66',
		'u66'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u66@Niva');

-- associate user 'u66@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u66@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u66@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u66@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u66@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u66@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u67
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u67@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u67',
		'u67',
		'u67'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u67@Niva');

-- associate user 'u67@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u67@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u67@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u67@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u67@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u67@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u68
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u68@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u68',
		'u68',
		'u68'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u68@Niva');

-- associate user 'u68@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u68@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u68@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u68@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u68@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u68@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u69
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u69@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u69',
		'u69',
		'u69'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u69@Niva');

-- associate user 'u69@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u69@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u69@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u69@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u69@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u69@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u70
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u70@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u70',
		'u70',
		'u70'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u70@Niva');

-- associate user 'u70@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u70@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u70@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u70@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u70@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u70@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u71
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u71@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u71',
		'u71',
		'u71'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u71@Niva');

-- associate user 'u71@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u71@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u71@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u71@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u71@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u71@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u72
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u72@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u72',
		'u72',
		'u72'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u72@Niva');

-- associate user 'u72@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u72@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u72@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u72@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u72@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u72@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u73
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u73@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u73',
		'u73',
		'u73'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u73@Niva');

-- associate user 'u73@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u73@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u73@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u73@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u73@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u73@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u74
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u74@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u74',
		'u74',
		'u74'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u74@Niva');

-- associate user 'u74@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u74@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u74@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u74@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u74@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u74@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u75
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u75@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u75',
		'u75',
		'u75'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u75@Niva');

-- associate user 'u75@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u75@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u75@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u75@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u75@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u75@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u76
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u76@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u76',
		'u76',
		'u76'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u76@Niva');

-- associate user 'u76@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u76@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u76@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u76@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u76@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u76@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u77
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u77@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u77',
		'u77',
		'u77'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u77@Niva');

-- associate user 'u77@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u77@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u77@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u77@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u77@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u77@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u78
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u78@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u78',
		'u78',
		'u78'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u78@Niva');

-- associate user 'u78@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u78@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u78@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u78@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u78@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u78@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u79
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u79@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u79',
		'u79',
		'u79'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u79@Niva');

-- associate user 'u79@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u79@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u79@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u79@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u79@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u79@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u80
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u80@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u80',
		'u80',
		'u80'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u80@Niva');

-- associate user 'u80@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u80@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u80@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u80@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u80@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u80@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u81
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u81@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u81',
		'u81',
		'u81'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u81@Niva');

-- associate user 'u81@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u81@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u81@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u81@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u81@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u81@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u82
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u82@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u82',
		'u82',
		'u82'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u82@Niva');

-- associate user 'u82@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u82@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u82@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u82@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u82@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u82@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u83
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u83@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u83',
		'u83',
		'u83'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u83@Niva');

-- associate user 'u83@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u83@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u83@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u83@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u83@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u83@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u84
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u84@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u84',
		'u84',
		'u84'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u84@Niva');

-- associate user 'u84@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u84@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u84@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u84@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u84@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u84@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u85
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u85@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u85',
		'u85',
		'u85'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u85@Niva');

-- associate user 'u85@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u85@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u85@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u85@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u85@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u85@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u86
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u86@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u86',
		'u86',
		'u86'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u86@Niva');

-- associate user 'u86@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u86@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u86@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u86@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u86@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u86@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u87
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u87@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u87',
		'u87',
		'u87'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u87@Niva');

-- associate user 'u87@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u87@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u87@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u87@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u87@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u87@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u88
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u88@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u88',
		'u88',
		'u88'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u88@Niva');

-- associate user 'u88@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u88@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u88@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u88@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u88@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u88@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u89
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u89@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u89',
		'u89',
		'u89'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u89@Niva');

-- associate user 'u89@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u89@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u89@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u89@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u89@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u89@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u90
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u90@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u90',
		'u90',
		'u90'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u90@Niva');

-- associate user 'u90@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u90@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u90@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u90@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u90@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u90@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u91
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u91@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u91',
		'u91',
		'u91'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u91@Niva');

-- associate user 'u91@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u91@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u91@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u91@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u91@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u91@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u92
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u92@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u92',
		'u92',
		'u92'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u92@Niva');

-- associate user 'u92@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u92@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u92@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u92@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u92@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u92@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u93
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u93@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u93',
		'u93',
		'u93'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u93@Niva');

-- associate user 'u93@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u93@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u93@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u93@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u93@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u93@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u94
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u94@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u94',
		'u94',
		'u94'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u94@Niva');

-- associate user 'u94@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u94@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u94@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u94@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u94@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u94@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u95
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u95@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u95',
		'u95',
		'u95'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u95@Niva');

-- associate user 'u95@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u95@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u95@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u95@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u95@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u95@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u96
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u96@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u96',
		'u96',
		'u96'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u96@Niva');

-- associate user 'u96@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u96@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u96@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u96@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u96@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u96@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u97
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u97@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u97',
		'u97',
		'u97'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u97@Niva');

-- associate user 'u97@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u97@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u97@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u97@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u97@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u97@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u98
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u98@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u98',
		'u98',
		'u98'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u98@Niva');

-- associate user 'u98@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u98@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u98@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u98@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u98@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u98@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u99
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u99@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u99',
		'u99',
		'u99'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u99@Niva');

-- associate user 'u99@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u99@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u99@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u99@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u99@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u99@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

-- add user: u100
INSERT INTO subscription.ss_users
	(subs_id, usst_id, user_email, user_password, user_password_salt, user_firstname, user_surname, user_nickname)
	SELECT 
		(SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva'), 
		0,
		'u100@Niva',
		'567a57d0e6412f016c4b70eafbf09c319b07ec8d1cbdaa7f8eb2d907ba05c344',
		'foo',
		'u100',
		'u100',
		'u100'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_users WHERE user_email='u100@Niva');

-- associate user 'u100@Niva' and role 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System'
INSERT INTO subscription.ss_users_roles
	(user_id, role_id, subs_id)
	SELECT 
		(SELECT user_id FROM subscription.ss_users where user_email = 'u100@Niva'), 
		role_id, subs_id
          FROM subscription.ss_roles 
         WHERE ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))
	   AND NOT EXISTS (SELECT * 
		FROM subscription.ss_users_roles 
		WHERE 
			user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u100@Niva')
		and     role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' )))
			);

-- associate user 'u100@Niva' with application 'Niva'
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'u100@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'u100@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
				);

--associate admin user with user management application
INSERT INTO subscription.ss_user_modules(
            user_id, modu_id)
    SELECT 
	(SELECT user_id FROM subscription.ss_users where user_email = 'admin@Niva'), 
	(SELECT modu_id FROM subscription.ss_modules WHERE modu_name='UsrMng')
    	WHERE NOT EXISTS (SELECT * FROM subscription.ss_user_modules
			  WHERE 
				  user_id in (SELECT user_id FROM subscription.ss_users where user_email = 'admin@Niva')
			      and  modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='UsrMng')
				);



--Associate Admin Role With UserMng Admin SystemRole
INSERT INTO subscription.ss_roles_system_roles
	(role_id, syro_id, usrinsert, dteinsert)
	SELECT 
		(SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))),
        (SELECT syro_id FROM subscription.ss_system_roles where syro_name = 'UsrMng_admin' and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='UsrMng')),
        '#system@usermgmnt#', now()
	WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_roles_system_roles 
			  WHERE 
				role_id in (SELECT role_id FROM subscription.ss_roles where ( role_caption = 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System' and subs_id in (SELECT subs_id FROM subscription.ss_subscribers WHERE subs_short_name='TEST_SUBSCRIBER_FOR_Niva' ))) 
			    and syro_id in (SELECT syro_id FROM subscription.ss_system_roles where syro_name = 'UsrMng_admin' and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='UsrMng')));

