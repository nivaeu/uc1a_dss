-- jndi-name="java:/niva_usermgmnt"
begin transaction;

--add module group with only this new application
INSERT INTO subscription.ss_module_groups        
	(mogr_name)
	SELECT 'ALL MODULES'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_module_groups WHERE mogr_name='ALL MODULES');


-- add application
INSERT INTO subscription.ss_modules 
	(modu_name, modu_description, modu_isfree, modu_ulr_suffix) 
	SELECT 'Niva', 'Use Case 1a: Decision Support System', false, 'Niva'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_modules WHERE modu_name='Niva');

UPDATE subscription.ss_modules
   SET modu_description='Use Case 1a: Decision Support System'
   WHERE modu_name = 'Niva';


-- associate application with module
INSERT INTO subscription.ss_grouping_modules 
    (mogr_id, modu_id) 
    SELECT
        (SELECT mogr_id FROM subscription.ss_module_groups WHERE mogr_name='ALL MODULES'), 
        (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
    WHERE NOT EXISTS (
    SELECT * FROM subscription.ss_grouping_modules 
        WHERE 
              mogr_id in (SELECT mogr_id FROM subscription.ss_module_groups WHERE mogr_name='ALL MODULES')
        AND   modu_id in (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));
-- add superadmin system role
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id, syro_is_multipage_role)
	SELECT 'Niva_superadmin', 'Ρόλος SuperAdmin για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'), true
	WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles WHERE syro_name='Niva_superadmin'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));
-- add admin system role
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id, syro_is_multipage_role)
	SELECT 'Niva_admin', 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'), true
	WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles WHERE syro_name='Niva_admin'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));
-- add guest system role
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id, syro_is_multipage_role)
	SELECT 'Niva_guest', 'Ρόλος Επισκέπτη για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'), true
	WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles WHERE syro_name='Niva_guest'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- delete any previously assigned Security Classes in any privilege of module 'Niva'
delete from subscription.ss_system_privileges_security_classes a
 where a.sypr_id in (select b.sypr_id 
                       from subscription.ss_system_privileges b
                       	join subscription.ss_modules c on c.modu_id = b.modu_id
                      where c.modu_name = 'Niva');

INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id, syro_is_multipage_role)
	SELECT 'Niva_superadmin', 'Ρόλος SuperAdmin για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'), true
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_superadmin'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));
-- delete any privilege under this system role
delete from subscription.ss_system_roles_privileges syrp
 where syro_id in (SELECT syro_id FROM subscription.ss_system_roles where syro_name='Niva_superadmin' and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id, syro_is_multipage_role)
	SELECT 'Niva_admin', 'Ρόλος Διαχειριστή για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'), true
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_admin'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));
-- delete any privilege under this system role
delete from subscription.ss_system_roles_privileges syrp
 where syro_id in (SELECT syro_id FROM subscription.ss_system_roles where syro_name='Niva_admin' and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id, syro_is_multipage_role)
	SELECT 'Niva_guest', 'Ρόλος Επισκέπτη για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'), true
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_guest'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));
-- delete any privilege under this system role
delete from subscription.ss_system_roles_privileges syrp
 where syro_id in (SELECT syro_id FROM subscription.ss_system_roles where syro_name='Niva_guest' and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id, syro_is_multipage_role)
	SELECT 'Niva_WebServices', 'Niva: Δικαίωμα κλήσης Web Services',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'), true
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_WebServices'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));
-- delete any privilege under this system role
delete from subscription.ss_system_roles_privileges syrp
 where syro_id in (SELECT syro_id FROM subscription.ss_system_roles where syro_name='Niva_WebServices' and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- Add privilege Niva_Agency_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Agency_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Agency_R');

-- Add a system role for privilege 'Niva_Agency_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Agency_R', 'Δικαίωμα ανάγνωσης στην οθόνη Paying Agency για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Agency_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Paying Agency για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Agency_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Paying Agency για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Agency_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Agency_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Agency_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Agency_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Agency_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Agency_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Agency_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Agency_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Agency_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Agency_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Agency_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Agency_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Agency_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_Agency_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Agency_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Agency_W');

-- Add a system role for privilege 'Niva_Agency_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Agency_W', 'Δικαίωμα εγγραφής στην οθόνη Paying Agency για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Agency_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Paying Agency για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Agency_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Paying Agency για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Agency_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Agency_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Agency_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Agency_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Agency_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Agency_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Agency_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Agency_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Agency_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Agency_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Agency_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Agency_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Agency_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Agency_D');

-- Add a system role for privilege 'Niva_Agency_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Agency_D', 'Δικαίωμα διαγραφής στην οθόνη Paying Agency για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Agency_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Paying Agency για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Agency_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Paying Agency για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Agency_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Agency_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Agency_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Agency_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Agency_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Agency_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Agency_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Agency_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Agency_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Agency_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Agency_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Classification_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Classification_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Classification_R');

-- Add a system role for privilege 'Niva_Classification_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Classification_R', 'Δικαίωμα ανάγνωσης στην οθόνη Classification για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Classification_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Classification για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Classification_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Classification για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Classification_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Classification_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Classification_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Classification_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classification_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Classification_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classification_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classification_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classification_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classification_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classification_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classification_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classification_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_Classification_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Classification_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Classification_W');

-- Add a system role for privilege 'Niva_Classification_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Classification_W', 'Δικαίωμα εγγραφής στην οθόνη Classification για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Classification_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Classification για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Classification_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Classification για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Classification_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Classification_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Classification_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Classification_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classification_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Classification_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classification_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classification_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classification_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classification_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classification_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Classification_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Classification_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Classification_D');

-- Add a system role for privilege 'Niva_Classification_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Classification_D', 'Δικαίωμα διαγραφής στην οθόνη Classification για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Classification_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Classification για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Classification_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Classification για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Classification_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Classification_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Classification_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Classification_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classification_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Classification_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classification_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classification_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classification_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classification_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classification_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Classifier_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Classifier_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Classifier_R');

-- Add a system role for privilege 'Niva_Classifier_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Classifier_R', 'Δικαίωμα ανάγνωσης στην οθόνη Classification Engine για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Classifier_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Classification Engine για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Classifier_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Classification Engine για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Classifier_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Classifier_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Classifier_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Classifier_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Classifier_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_Classifier_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Classifier_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Classifier_W');

-- Add a system role for privilege 'Niva_Classifier_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Classifier_W', 'Δικαίωμα εγγραφής στην οθόνη Classification Engine για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Classifier_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Classification Engine για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Classifier_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Classification Engine για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Classifier_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Classifier_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Classifier_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Classifier_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Classifier_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Classifier_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Classifier_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Classifier_D');

-- Add a system role for privilege 'Niva_Classifier_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Classifier_D', 'Δικαίωμα διαγραφής στην οθόνη Classification Engine για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Classifier_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Classification Engine για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Classifier_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Classification Engine για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Classifier_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Classifier_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Classifier_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Classifier_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Classifier_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Classifier_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_CoverType_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_CoverType_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_CoverType_R');

-- Add a system role for privilege 'Niva_CoverType_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_CoverType_R', 'Δικαίωμα ανάγνωσης στην οθόνη Land Cover για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_CoverType_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Land Cover για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_CoverType_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Land Cover για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_CoverType_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_CoverType_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_CoverType_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_CoverType_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_CoverType_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_CoverType_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_CoverType_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_CoverType_W');

-- Add a system role for privilege 'Niva_CoverType_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_CoverType_W', 'Δικαίωμα εγγραφής στην οθόνη Land Cover για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_CoverType_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Land Cover για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_CoverType_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Land Cover για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_CoverType_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_CoverType_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_CoverType_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_CoverType_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_CoverType_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_CoverType_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_CoverType_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_CoverType_D');

-- Add a system role for privilege 'Niva_CoverType_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_CoverType_D', 'Δικαίωμα διαγραφής στην οθόνη Land Cover για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_CoverType_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Land Cover για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_CoverType_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Land Cover για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_CoverType_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_CoverType_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_CoverType_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_CoverType_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_CoverType_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_CoverType_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Cultivation_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Cultivation_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Cultivation_R');

-- Add a system role for privilege 'Niva_Cultivation_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Cultivation_R', 'Δικαίωμα ανάγνωσης στην οθόνη Crops List για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Cultivation_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Crops List για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Cultivation_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Crops List για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Cultivation_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Cultivation_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Cultivation_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Cultivation_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Cultivation_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_Cultivation_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Cultivation_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Cultivation_W');

-- Add a system role for privilege 'Niva_Cultivation_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Cultivation_W', 'Δικαίωμα εγγραφής στην οθόνη Crops List για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Cultivation_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Crops List για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Cultivation_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Crops List για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Cultivation_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Cultivation_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Cultivation_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Cultivation_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Cultivation_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Cultivation_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Cultivation_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Cultivation_D');

-- Add a system role for privilege 'Niva_Cultivation_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Cultivation_D', 'Δικαίωμα διαγραφής στην οθόνη Crops List για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Cultivation_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Crops List για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Cultivation_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Crops List για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Cultivation_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Cultivation_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Cultivation_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Cultivation_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Cultivation_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Cultivation_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_DecisionMaking_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_DecisionMaking_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_DecisionMaking_R');

-- Add a system role for privilege 'Niva_DecisionMaking_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_DecisionMaking_R', 'Δικαίωμα ανάγνωσης στην οθόνη Decision Making για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_DecisionMaking_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Decision Making για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_DecisionMaking_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Decision Making για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_DecisionMaking_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_DecisionMaking_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_DecisionMaking_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_DecisionMaking_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMaking_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_DecisionMaking_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMaking_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMaking_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMaking_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMaking_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMaking_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMaking_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMaking_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_DecisionMaking_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_DecisionMaking_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_DecisionMaking_W');

-- Add a system role for privilege 'Niva_DecisionMaking_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_DecisionMaking_W', 'Δικαίωμα εγγραφής στην οθόνη Decision Making για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_DecisionMaking_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Decision Making για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_DecisionMaking_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Decision Making για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_DecisionMaking_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_DecisionMaking_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_DecisionMaking_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_DecisionMaking_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMaking_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_DecisionMaking_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMaking_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMaking_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMaking_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMaking_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMaking_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_DecisionMaking_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_DecisionMaking_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_DecisionMaking_D');

-- Add a system role for privilege 'Niva_DecisionMaking_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_DecisionMaking_D', 'Δικαίωμα διαγραφής στην οθόνη Decision Making για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_DecisionMaking_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Decision Making για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_DecisionMaking_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Decision Making για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_DecisionMaking_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_DecisionMaking_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_DecisionMaking_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_DecisionMaking_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMaking_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_DecisionMaking_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMaking_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMaking_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMaking_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMaking_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMaking_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_DecisionMakingRO_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_DecisionMakingRO_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_DecisionMakingRO_R');

-- Add a system role for privilege 'Niva_DecisionMakingRO_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_DecisionMakingRO_R', 'Δικαίωμα ανάγνωσης στην οθόνη Field Map για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_DecisionMakingRO_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Field Map για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_DecisionMakingRO_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Field Map για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_DecisionMakingRO_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_DecisionMakingRO_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_DecisionMakingRO_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_DecisionMakingRO_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMakingRO_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_DecisionMakingRO_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMakingRO_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMakingRO_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMakingRO_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMakingRO_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMakingRO_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMakingRO_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMakingRO_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_DecisionMakingRO_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_DecisionMakingRO_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_DecisionMakingRO_W');

-- Add a system role for privilege 'Niva_DecisionMakingRO_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_DecisionMakingRO_W', 'Δικαίωμα εγγραφής στην οθόνη Field Map για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_DecisionMakingRO_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Field Map για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_DecisionMakingRO_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Field Map για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_DecisionMakingRO_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_DecisionMakingRO_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_DecisionMakingRO_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_DecisionMakingRO_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMakingRO_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_DecisionMakingRO_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMakingRO_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMakingRO_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMakingRO_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMakingRO_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMakingRO_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_DecisionMakingRO_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_DecisionMakingRO_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_DecisionMakingRO_D');

-- Add a system role for privilege 'Niva_DecisionMakingRO_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_DecisionMakingRO_D', 'Δικαίωμα διαγραφής στην οθόνη Field Map για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_DecisionMakingRO_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Field Map για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_DecisionMakingRO_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Field Map για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_DecisionMakingRO_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_DecisionMakingRO_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_DecisionMakingRO_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_DecisionMakingRO_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMakingRO_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_DecisionMakingRO_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMakingRO_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMakingRO_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMakingRO_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMakingRO_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_DecisionMakingRO_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_EcGroup_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_EcGroup_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_EcGroup_R');

-- Add a system role for privilege 'Niva_EcGroup_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_EcGroup_R', 'Δικαίωμα ανάγνωσης στην οθόνη Eligibility Criteria για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_EcGroup_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Eligibility Criteria για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_EcGroup_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Eligibility Criteria για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_EcGroup_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_EcGroup_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_EcGroup_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_EcGroup_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcGroup_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_EcGroup_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcGroup_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcGroup_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcGroup_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcGroup_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcGroup_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcGroup_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcGroup_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_EcGroup_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_EcGroup_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_EcGroup_W');

-- Add a system role for privilege 'Niva_EcGroup_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_EcGroup_W', 'Δικαίωμα εγγραφής στην οθόνη Eligibility Criteria για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_EcGroup_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Eligibility Criteria για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_EcGroup_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Eligibility Criteria για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_EcGroup_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_EcGroup_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_EcGroup_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_EcGroup_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcGroup_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_EcGroup_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcGroup_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcGroup_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcGroup_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcGroup_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcGroup_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_EcGroup_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_EcGroup_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_EcGroup_D');

-- Add a system role for privilege 'Niva_EcGroup_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_EcGroup_D', 'Δικαίωμα διαγραφής στην οθόνη Eligibility Criteria για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_EcGroup_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Eligibility Criteria για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_EcGroup_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Eligibility Criteria για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_EcGroup_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_EcGroup_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_EcGroup_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_EcGroup_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcGroup_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_EcGroup_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcGroup_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcGroup_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcGroup_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcGroup_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_EcGroup_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_FileTemplate_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_FileTemplate_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_FileTemplate_R');

-- Add a system role for privilege 'Niva_FileTemplate_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_FileTemplate_R', 'Δικαίωμα ανάγνωσης στην οθόνη Data Import Template για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_FileTemplate_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Data Import Template για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_FileTemplate_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Data Import Template για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_FileTemplate_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_FileTemplate_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_FileTemplate_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FileTemplate_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FileTemplate_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_FileTemplate_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_FileTemplate_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_FileTemplate_W');

-- Add a system role for privilege 'Niva_FileTemplate_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_FileTemplate_W', 'Δικαίωμα εγγραφής στην οθόνη Data Import Template για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_FileTemplate_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Data Import Template για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_FileTemplate_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Data Import Template για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_FileTemplate_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_FileTemplate_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_FileTemplate_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FileTemplate_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FileTemplate_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_FileTemplate_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_FileTemplate_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_FileTemplate_D');

-- Add a system role for privilege 'Niva_FileTemplate_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_FileTemplate_D', 'Δικαίωμα διαγραφής στην οθόνη Data Import Template για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_FileTemplate_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Data Import Template για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_FileTemplate_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Data Import Template για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_FileTemplate_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_FileTemplate_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_FileTemplate_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FileTemplate_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FileTemplate_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileTemplate_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Document_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Document_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Document_R');

-- Add a system role for privilege 'Niva_Document_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Document_R', 'Δικαίωμα ανάγνωσης στην οθόνη Documents για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Document_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Documents για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Document_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Documents για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Document_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Document_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Document_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Document_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Document_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_Document_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Document_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Document_W');

-- Add a system role for privilege 'Niva_Document_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Document_W', 'Δικαίωμα εγγραφής στην οθόνη Documents για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Document_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Documents για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Document_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Documents για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Document_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Document_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Document_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Document_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Document_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Document_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Document_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Document_D');

-- Add a system role for privilege 'Niva_Document_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Document_D', 'Δικαίωμα διαγραφής στην οθόνη Documents για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Document_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Documents για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Document_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Documents για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Document_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Document_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Document_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Document_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Document_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Document_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_FileDirPath_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_FileDirPath_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_FileDirPath_R');

-- Add a system role for privilege 'Niva_FileDirPath_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_FileDirPath_R', 'Δικαίωμα ανάγνωσης στην οθόνη Files and Directories Preferences για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_FileDirPath_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Files and Directories Preferences για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_FileDirPath_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Files and Directories Preferences για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_FileDirPath_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_FileDirPath_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_FileDirPath_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FileDirPath_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileDirPath_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FileDirPath_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileDirPath_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileDirPath_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileDirPath_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileDirPath_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileDirPath_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileDirPath_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileDirPath_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_FileDirPath_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_FileDirPath_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_FileDirPath_W');

-- Add a system role for privilege 'Niva_FileDirPath_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_FileDirPath_W', 'Δικαίωμα εγγραφής στην οθόνη Files and Directories Preferences για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_FileDirPath_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Files and Directories Preferences για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_FileDirPath_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Files and Directories Preferences για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_FileDirPath_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_FileDirPath_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_FileDirPath_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FileDirPath_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileDirPath_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FileDirPath_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileDirPath_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileDirPath_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileDirPath_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileDirPath_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileDirPath_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_FileDirPath_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_FileDirPath_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_FileDirPath_D');

-- Add a system role for privilege 'Niva_FileDirPath_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_FileDirPath_D', 'Δικαίωμα διαγραφής στην οθόνη Files and Directories Preferences για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_FileDirPath_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Files and Directories Preferences για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_FileDirPath_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Files and Directories Preferences για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_FileDirPath_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_FileDirPath_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_FileDirPath_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FileDirPath_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileDirPath_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FileDirPath_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileDirPath_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileDirPath_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileDirPath_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileDirPath_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FileDirPath_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Integrateddecision_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Integrateddecision_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Integrateddecision_R');

-- Add a system role for privilege 'Niva_Integrateddecision_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Integrateddecision_R', 'Δικαίωμα ανάγνωσης στην οθόνη Integrateddecision για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Integrateddecision_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Integrateddecision για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Integrateddecision_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Integrateddecision για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Integrateddecision_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Integrateddecision_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Integrateddecision_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Integrateddecision_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Integrateddecision_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Integrateddecision_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Integrateddecision_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Integrateddecision_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Integrateddecision_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Integrateddecision_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Integrateddecision_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Integrateddecision_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Integrateddecision_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_Integrateddecision_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Integrateddecision_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Integrateddecision_W');

-- Add a system role for privilege 'Niva_Integrateddecision_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Integrateddecision_W', 'Δικαίωμα εγγραφής στην οθόνη Integrateddecision για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Integrateddecision_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Integrateddecision για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Integrateddecision_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Integrateddecision για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Integrateddecision_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Integrateddecision_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Integrateddecision_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Integrateddecision_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Integrateddecision_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Integrateddecision_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Integrateddecision_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Integrateddecision_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Integrateddecision_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Integrateddecision_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Integrateddecision_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Integrateddecision_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Integrateddecision_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Integrateddecision_D');

-- Add a system role for privilege 'Niva_Integrateddecision_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Integrateddecision_D', 'Δικαίωμα διαγραφής στην οθόνη Integrateddecision για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Integrateddecision_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Integrateddecision για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Integrateddecision_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Integrateddecision για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Integrateddecision_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Integrateddecision_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Integrateddecision_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Integrateddecision_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Integrateddecision_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Integrateddecision_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Integrateddecision_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Integrateddecision_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Integrateddecision_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Integrateddecision_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Integrateddecision_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_ParcelGP_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_ParcelGP_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_ParcelGP_R');

-- Add a system role for privilege 'Niva_ParcelGP_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_ParcelGP_R', 'Δικαίωμα ανάγνωσης στην οθόνη Parcels Geotagged Photos Decisions για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_ParcelGP_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Parcels Geotagged Photos Decisions για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_ParcelGP_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Parcels Geotagged Photos Decisions για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_ParcelGP_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_ParcelGP_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_ParcelGP_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelGP_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelGP_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelGP_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelGP_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelGP_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelGP_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelGP_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelGP_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelGP_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelGP_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_ParcelGP_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_ParcelGP_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_ParcelGP_W');

-- Add a system role for privilege 'Niva_ParcelGP_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_ParcelGP_W', 'Δικαίωμα εγγραφής στην οθόνη Parcels Geotagged Photos Decisions για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_ParcelGP_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Parcels Geotagged Photos Decisions για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_ParcelGP_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Parcels Geotagged Photos Decisions για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_ParcelGP_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_ParcelGP_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_ParcelGP_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelGP_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelGP_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelGP_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelGP_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelGP_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelGP_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelGP_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelGP_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_ParcelGP_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_ParcelGP_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_ParcelGP_D');

-- Add a system role for privilege 'Niva_ParcelGP_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_ParcelGP_D', 'Δικαίωμα διαγραφής στην οθόνη Parcels Geotagged Photos Decisions για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_ParcelGP_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Parcels Geotagged Photos Decisions για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_ParcelGP_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Parcels Geotagged Photos Decisions για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_ParcelGP_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_ParcelGP_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_ParcelGP_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelGP_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelGP_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelGP_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelGP_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelGP_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelGP_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelGP_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelGP_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_AgrisnapUsers_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_AgrisnapUsers_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_AgrisnapUsers_R');

-- Add a system role for privilege 'Niva_AgrisnapUsers_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_AgrisnapUsers_R', 'Δικαίωμα ανάγνωσης στην οθόνη Agrisnap Users για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_AgrisnapUsers_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Agrisnap Users για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_AgrisnapUsers_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Agrisnap Users για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_AgrisnapUsers_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_AgrisnapUsers_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_AgrisnapUsers_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AgrisnapUsers_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUsers_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AgrisnapUsers_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUsers_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUsers_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUsers_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUsers_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUsers_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUsers_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUsers_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_AgrisnapUsers_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_AgrisnapUsers_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_AgrisnapUsers_W');

-- Add a system role for privilege 'Niva_AgrisnapUsers_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_AgrisnapUsers_W', 'Δικαίωμα εγγραφής στην οθόνη Agrisnap Users για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_AgrisnapUsers_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Agrisnap Users για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_AgrisnapUsers_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Agrisnap Users για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_AgrisnapUsers_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_AgrisnapUsers_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_AgrisnapUsers_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AgrisnapUsers_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUsers_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AgrisnapUsers_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUsers_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUsers_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUsers_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUsers_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUsers_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_AgrisnapUsers_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_AgrisnapUsers_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_AgrisnapUsers_D');

-- Add a system role for privilege 'Niva_AgrisnapUsers_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_AgrisnapUsers_D', 'Δικαίωμα διαγραφής στην οθόνη Agrisnap Users για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_AgrisnapUsers_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Agrisnap Users για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_AgrisnapUsers_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Agrisnap Users για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_AgrisnapUsers_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_AgrisnapUsers_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_AgrisnapUsers_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AgrisnapUsers_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUsers_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_AgrisnapUsers_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUsers_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUsers_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUsers_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUsers_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_AgrisnapUsers_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_ParcelsIssues_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_ParcelsIssues_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_ParcelsIssues_R');

-- Add a system role for privilege 'Niva_ParcelsIssues_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_ParcelsIssues_R', 'Δικαίωμα ανάγνωσης στην οθόνη ParcelsIssues για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_ParcelsIssues_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη ParcelsIssues για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_ParcelsIssues_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη ParcelsIssues για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_ParcelsIssues_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_ParcelsIssues_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_ParcelsIssues_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelsIssues_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsIssues_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelsIssues_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsIssues_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsIssues_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsIssues_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsIssues_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsIssues_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsIssues_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsIssues_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_ParcelsIssues_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_ParcelsIssues_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_ParcelsIssues_W');

-- Add a system role for privilege 'Niva_ParcelsIssues_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_ParcelsIssues_W', 'Δικαίωμα εγγραφής στην οθόνη ParcelsIssues για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_ParcelsIssues_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη ParcelsIssues για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_ParcelsIssues_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη ParcelsIssues για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_ParcelsIssues_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_ParcelsIssues_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_ParcelsIssues_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelsIssues_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsIssues_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelsIssues_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsIssues_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsIssues_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsIssues_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsIssues_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsIssues_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_ParcelsIssues_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_ParcelsIssues_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_ParcelsIssues_D');

-- Add a system role for privilege 'Niva_ParcelsIssues_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_ParcelsIssues_D', 'Δικαίωμα διαγραφής στην οθόνη ParcelsIssues για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_ParcelsIssues_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη ParcelsIssues για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_ParcelsIssues_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη ParcelsIssues για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_ParcelsIssues_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_ParcelsIssues_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_ParcelsIssues_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelsIssues_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsIssues_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelsIssues_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsIssues_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsIssues_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsIssues_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsIssues_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelsIssues_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Producers_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Producers_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Producers_R');

-- Add a system role for privilege 'Niva_Producers_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Producers_R', 'Δικαίωμα ανάγνωσης στην οθόνη Producers για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Producers_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Producers για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Producers_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Producers για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Producers_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Producers_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Producers_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Producers_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producers_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Producers_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producers_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producers_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producers_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producers_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producers_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producers_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producers_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_Producers_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Producers_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Producers_W');

-- Add a system role for privilege 'Niva_Producers_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Producers_W', 'Δικαίωμα εγγραφής στην οθόνη Producers για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Producers_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Producers για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Producers_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Producers για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Producers_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Producers_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Producers_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Producers_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producers_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Producers_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producers_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producers_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producers_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producers_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producers_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Producers_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Producers_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Producers_D');

-- Add a system role for privilege 'Niva_Producers_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Producers_D', 'Δικαίωμα διαγραφής στην οθόνη Producers για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Producers_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Producers για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Producers_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Producers για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Producers_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Producers_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Producers_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Producers_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producers_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Producers_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producers_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producers_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producers_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producers_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Producers_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Dashboard_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Dashboard_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Dashboard_R');

-- Add a system role for privilege 'Niva_Dashboard_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Dashboard_R', 'Δικαίωμα ανάγνωσης στην οθόνη Parcels για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Dashboard_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Parcels για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Dashboard_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Parcels για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Dashboard_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Dashboard_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Dashboard_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Dashboard_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Dashboard_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Dashboard_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Dashboard_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Dashboard_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Dashboard_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Dashboard_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Dashboard_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Dashboard_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Dashboard_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_Dashboard_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Dashboard_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Dashboard_W');

-- Add a system role for privilege 'Niva_Dashboard_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Dashboard_W', 'Δικαίωμα εγγραφής στην οθόνη Parcels για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Dashboard_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Parcels για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Dashboard_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Parcels για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Dashboard_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Dashboard_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Dashboard_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Dashboard_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Dashboard_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Dashboard_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Dashboard_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Dashboard_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Dashboard_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Dashboard_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Dashboard_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_Dashboard_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_Dashboard_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_Dashboard_D');

-- Add a system role for privilege 'Niva_Dashboard_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_Dashboard_D', 'Δικαίωμα διαγραφής στην οθόνη Parcels για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_Dashboard_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Parcels για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_Dashboard_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Parcels για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_Dashboard_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_Dashboard_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_Dashboard_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Dashboard_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Dashboard_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_Dashboard_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Dashboard_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Dashboard_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Dashboard_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Dashboard_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_Dashboard_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_ParcelFMIS_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_ParcelFMIS_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_ParcelFMIS_R');

-- Add a system role for privilege 'Niva_ParcelFMIS_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_ParcelFMIS_R', 'Δικαίωμα ανάγνωσης στην οθόνη Parcels για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_ParcelFMIS_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη Parcels για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_ParcelFMIS_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη Parcels για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_ParcelFMIS_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_ParcelFMIS_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_ParcelFMIS_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelFMIS_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelFMIS_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelFMIS_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelFMIS_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelFMIS_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelFMIS_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelFMIS_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelFMIS_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelFMIS_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelFMIS_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_ParcelFMIS_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_ParcelFMIS_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_ParcelFMIS_W');

-- Add a system role for privilege 'Niva_ParcelFMIS_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_ParcelFMIS_W', 'Δικαίωμα εγγραφής στην οθόνη Parcels για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_ParcelFMIS_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη Parcels για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_ParcelFMIS_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη Parcels για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_ParcelFMIS_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_ParcelFMIS_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_ParcelFMIS_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelFMIS_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelFMIS_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelFMIS_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelFMIS_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelFMIS_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelFMIS_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelFMIS_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelFMIS_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_ParcelFMIS_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_ParcelFMIS_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_ParcelFMIS_D');

-- Add a system role for privilege 'Niva_ParcelFMIS_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_ParcelFMIS_D', 'Δικαίωμα διαγραφής στην οθόνη Parcels για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_ParcelFMIS_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη Parcels για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_ParcelFMIS_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη Parcels για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_ParcelFMIS_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_ParcelFMIS_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_ParcelFMIS_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelFMIS_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelFMIS_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_ParcelFMIS_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelFMIS_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelFMIS_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelFMIS_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelFMIS_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_ParcelFMIS_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_FmisUser_R

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_FmisUser_R'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_FmisUser_R');

-- Add a system role for privilege 'Niva_FmisUser_R'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_FmisUser_R', 'Δικαίωμα ανάγνωσης στην οθόνη FMIS Users για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_FmisUser_R'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα ανάγνωσης στην οθόνη FMIS Users για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_FmisUser_R'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα ανάγνωσης στην οθόνη FMIS Users για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_FmisUser_R');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_FmisUser_R'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_FmisUser_R') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FmisUser_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FmisUser_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FmisUser_R' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FmisUser_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FmisUser_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FmisUser_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FmisUser_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FmisUser_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the guest system role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FmisUser_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
                        WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_guest' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                          and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FmisUser_R' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
                       );

-- Add privilege Niva_FmisUser_W

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_FmisUser_W'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_FmisUser_W');

-- Add a system role for privilege 'Niva_FmisUser_W'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_FmisUser_W', 'Δικαίωμα εγγραφής στην οθόνη FMIS Users για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_FmisUser_W'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα εγγραφής στην οθόνη FMIS Users για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_FmisUser_W'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα εγγραφής στην οθόνη FMIS Users για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_FmisUser_W');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_FmisUser_W'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_FmisUser_W') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FmisUser_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FmisUser_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FmisUser_W' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FmisUser_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FmisUser_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FmisUser_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FmisUser_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FmisUser_W' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_FmisUser_D

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_FmisUser_D'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_FmisUser_D');

-- Add a system role for privilege 'Niva_FmisUser_D'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_FmisUser_D', 'Δικαίωμα διαγραφής στην οθόνη FMIS Users για την εφαρμογή Use Case 1a: Decision Support System',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_FmisUser_D'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Δικαίωμα διαγραφής στην οθόνη FMIS Users για την εφαρμογή Use Case 1a: Decision Support System',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_FmisUser_D'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Δικαίωμα διαγραφής στην οθόνη FMIS Users για την εφαρμογή Use Case 1a: Decision Support System';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_FmisUser_D');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_FmisUser_D'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_FmisUser_D') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FmisUser_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FmisUser_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_FmisUser_D' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FmisUser_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the superadmin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FmisUser_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_superadmin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FmisUser_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- associate the admin role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FmisUser_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_admin' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_FmisUser_D' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);


-- Add privilege Niva_getParcelDecision

INSERT INTO subscription.ss_system_privileges 
	(modu_id, sypr_name) 
	SELECT 
		(SELECT modu_id from subscription.ss_modules where modu_name='Niva'), 
		'Niva_getParcelDecision'
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_privileges WHERE sypr_name='Niva_getParcelDecision');

-- Add a system role for privilege 'Niva_getParcelDecision'
INSERT INTO subscription.ss_system_roles
	(syro_name, syro_description, modu_id)
	SELECT 'Niva_getParcelDecision', 'Niva: Δικαίωμα κλήσης του Web Service getParcelDecision',
               (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
	WHERE NOT EXISTS (SELECT * FROM subscription.ss_system_roles WHERE syro_name='Niva_getParcelDecision'
                            and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'));

-- update description of system role for privilege if exists
update subscription.ss_system_roles
   set syro_description = 'Niva: Δικαίωμα κλήσης του Web Service getParcelDecision',
   	   dteupdate = now(),
       usrupdate = '#system.sql of Niva#'
 where syro_name = 'Niva_getParcelDecision'
   and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva')
   and syro_description != 'Niva: Δικαίωμα κλήσης του Web Service getParcelDecision';

-- delete any previusly assigned Security Classes in this privilege
delete
from subscription.ss_system_privileges_security_classes cl
where
   cl.sypr_id in (select sypr_id from subscription.ss_system_privileges where sypr_name='Niva_getParcelDecision');

-- associate Sustem privilege with Security Classes

INSERT INTO subscription.ss_system_privileges_security_classes
        (sypr_id, scls_id)
SELECT 
    (SELECT a.sypr_id FROM subscription.ss_system_privileges a where a.sypr_name='Niva_getParcelDecision'),
    scls.scls_id
 FROM subscription.ss_security_classes scls
WHERE scls.scls_id = 2
  AND NOT EXISTS (SELECT * FROM  subscription.ss_system_privileges_security_classes spsc
                  WHERE spsc.sypr_id = (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr where sypr.sypr_name='Niva_getParcelDecision') AND spsc.scls_id = scls.scls_id);


-- associate the role with the privilege
INSERT INTO subscription.ss_system_roles_privileges
	(syro_id, sypr_id, modu_id)
	SELECT 
		(SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_getParcelDecision' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
		(SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_getParcelDecision' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
                (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
         WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_getParcelDecision' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_getParcelDecision' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- delete the privilege from superadmin system role
delete from subscription.ss_system_roles_privileges
 where syro_id = (SELECT syro_id FROM subscription.ss_system_roles where syro_name='Niva_superadmin' and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'))
   and sypr_id = (SELECT sypr_id FROM subscription.ss_system_privileges WHERE sypr_name = 'Niva_getParcelDecision');

-- delete the privilege from admin system role
delete from subscription.ss_system_roles_privileges
 where syro_id = (SELECT syro_id FROM subscription.ss_system_roles where syro_name='Niva_admin' and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'))
   and sypr_id = (SELECT sypr_id FROM subscription.ss_system_privileges WHERE sypr_name = 'Niva_getParcelDecision');

-- delete the privilege from guest system role
delete from subscription.ss_system_roles_privileges
 where syro_id = (SELECT syro_id FROM subscription.ss_system_roles where syro_name='Niva_guest' and modu_id = (SELECT modu_id FROM subscription.ss_modules WHERE modu_name='Niva'))
   and sypr_id = (SELECT sypr_id FROM subscription.ss_system_privileges WHERE sypr_name = 'Niva_getParcelDecision');
INSERT INTO subscription.ss_system_roles_privileges
    (syro_id, sypr_id, modu_id)
    SELECT (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_WebServices' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')), 
           (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_getParcelDecision' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')),
           (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva')
     WHERE NOT EXISTS (SELECT 1 FROM subscription.ss_system_roles_privileges syrp
				WHERE syrp.syro_id in (SELECT syro.syro_id FROM subscription.ss_system_roles syro where syro.syro_name='Niva_WebServices' and syro.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				  and syrp.sypr_id in (SELECT sypr.sypr_id FROM subscription.ss_system_privileges sypr WHERE sypr.sypr_name = 'Niva_getParcelDecision' and sypr.modu_id = (select modu.modu_id from subscription.ss_modules modu where modu.modu_name='Niva'))
				);

-- add roles for multipage system roles to every subscriber has access to module 'Niva'
select subscription.add_multipage_roles(a.subs_id, a.modu_name)
  from 
(select distinct subs.subs_id, modu.modu_name
  from subscription.ss_subscribers subs
  	join subscription.ss_subscribers_module_groups sumg on sumg.subs_id = subs.subs_id
    join subscription.ss_grouping_modules grmo on grmo.mogr_id = sumg.mogr_id
    join subscription.ss_modules modu on modu.modu_id = grmo.modu_id
 where modu.modu_name = 'Niva') a;
 

-- Insert wsPaths for logging 
insert into ws_responses.ss_module_wss(modu_id, mows_ws_path)
with paths AS (
select unnest(
ARRAY[
'Login/loginUser',
'Menu/getMenu',
'Menu/getBannerMessages',
'Menu/getLoginPageMessages',
'Menu/getAuditInfoUsers',
'MainService/synchronizeChangesWithDb_Agency',
'MainService/synchronizeChangesWithDb_Classification',
'MainService/synchronizeChangesWithDb_Classifier',
'MainService/synchronizeChangesWithDb_CoverType',
'MainService/synchronizeChangesWithDb_Cultivation',
'MainService/synchronizeChangesWithDb_DecisionMaking',
'MainService/synchronizeChangesWithDb_DecisionMakingRO',
'MainService/synchronizeChangesWithDb_EcGroup',
'MainService/synchronizeChangesWithDb_FileTemplate',
'MainService/synchronizeChangesWithDb_Document',
'MainService/synchronizeChangesWithDb_FileDirPath',
'MainService/synchronizeChangesWithDb_Integrateddecision',
'MainService/synchronizeChangesWithDb_ParcelGP',
'MainService/synchronizeChangesWithDb_AgrisnapUsers',
'MainService/synchronizeChangesWithDb_ParcelsIssues',
'MainService/synchronizeChangesWithDb_Producers',
'MainService/synchronizeChangesWithDb_Dashboard',
'MainService/synchronizeChangesWithDb_ParcelFMIS',
'MainService/synchronizeChangesWithDb_FmisUser',
'MainService/checkProducerStatus',
'MainService/getProducerStatus',
'MainService/getLogoProvider',
'MainService/finalizationEcGroup',
'MainService/runningDecisionMaking',
'MainService/importingClassification',
'MainService/getPhotoRequests',
'MainService/uploadPhoto',
'MainService/getBRERunsResultsList',
'MainService/getBRERunResults',
'MainService/getFMISRequests',
'MainService/uploadFMIS',
'MainService/updateIntegratedDecisionsNIssues',
'MainService/updateIntegratedDecisionsNIssuesFromDema',
'MainService/actionPushToCommonsAPI',
'MainService/importExcel_CoverType_impCotys_id',
'MainService/importExcel_CoverType_impCotys_id_SpecsAndSample',
'MainService/importExcel_Cultivation_impCults_id',
'MainService/importExcel_Cultivation_impCults_id_SpecsAndSample',
'AgrisnapUsers/findAllByIds',
'AgrisnapUsers/findByAgrisnapUsersId',
'AgrisnapUsers/findLazyAgrisnapUsers',
'AgrisnapUsers/delByAgrisnapUsersId',
'Classification/findAllByIds',
'Classification/findByClasId',
'Classification/findLazyClassification',
'Classification/getAttachedFile',
'Classification/setAttachedFile',
'Classification/findAllByCriteriaRange_forLov',
'Classification/delByClasId',
'DecisionMaking/findAllByIds',
'DecisionMaking/findByDemaId',
'DecisionMaking/findLazyDecisionMaking',
'DecisionMaking/findLazyDecisionMakingRO',
'DecisionMaking/findAllByCriteriaRange_forLov',
'DecisionMaking/delByDemaId',
'EcGroup/findAllByIds',
'EcGroup/findByEcgrId',
'EcGroup/findLazyEcGroup',
'EcGroup/findAllByCriteriaRange_forLov',
'EcGroup/delByEcgrId',
'FileTemplate/findAllByIds',
'FileTemplate/findByFiteId',
'FileTemplate/findLazyFileTemplate',
'FileTemplate/findAllByCriteriaRange_forLov',
'FileTemplate/delByFiteId',
'FmisUser/findAllByIds',
'FmisUser/findByFmisUsersId',
'FmisUser/findLazyFmisUser',
'FmisUser/delByFmisUsersId',
'Integrateddecision/findAllByIds',
'Integrateddecision/findByIntegrateddecisionsId',
'Integrateddecision/findLazyIntegrateddecision',
'Integrateddecision/findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision',
'Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen',
'Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter',
'Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter_grid',
'Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter_geoJsonFile',
'Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter_excelFile',
'Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow',
'Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter',
'Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter_grid',
'Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter_geoJsonFile',
'Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter_excelFile',
'Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed',
'Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter',
'Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter_grid',
'Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter_geoJsonFile',
'Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter_excelFile',
'Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined',
'Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter',
'Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter_grid',
'Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter_geoJsonFile',
'Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter_excelFile',
'Integrateddecision/findAllByCriteriaRange_DashboardGrpIntegrateddecision',
'Integrateddecision/delByIntegrateddecisionsId',
'ParcelClas/findAllByIds',
'ParcelClas/findByPclaId',
'ParcelClas/findLazyParcelGP',
'ParcelClas/findLazyDashboard',
'ParcelClas/findLazyParcelFMIS',
'ParcelClas/findAllByCriteriaRange_ClassificationGrpParcelClas',
'ParcelClas/findAllByCriteriaRange_forLov',
'ParcelClas/delByPclaId',
'ParcelsIssues/findAllByIds',
'ParcelsIssues/findByParcelsIssuesId',
'ParcelsIssues/findLazyParcelsIssues',
'ParcelsIssues/findAllByCriteriaRange_ParcelGPGrpParcelsIssues',
'ParcelsIssues/findAllByCriteriaRange_DashboardGrpParcelsIssues',
'ParcelsIssues/findAllByCriteriaRange_ParcelFMISGrpParcelsIssues',
'ParcelsIssues/delByParcelsIssuesId',
'Producers/findAllByIds',
'Producers/findByProducersId',
'Producers/findLazyProducers',
'Producers/findAllByCriteriaRange_forLov',
'Producers/delByProducersId'
]) as ws_path) 
select modu.modu_id, paths.ws_path
  from subscription.ss_modules modu
  	cross join paths
 where modu.modu_name = 'Niva'
   and not exists (select 1
                     from ws_responses.ss_module_wss m
                    where m.modu_id = modu.modu_id
                      and m.mows_ws_path = paths.ws_path
                   );


end transaction;
