package gr.neuropublic.Niva.servicesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import gr.neuropublic.exceptions.GenericApplicationException;
import gr.neuropublic.exceptions.UserApplicationException;
import gr.neuropublic.functional.Action;
import gr.neuropublic.functional.Action1Ex;
import gr.neuropublic.functional.Func;
import gr.neuropublic.functional.Func1Ex;
import gr.neuropublic.mutil.base.Pair;
import gr.neuropublic.enums.DataBaseType;
import gr.neuropublic.Niva.serviceInputsBase.*;
import gr.neuropublic.base.ExcelFileUtils;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public abstract class MainServiceBase extends AbstractService implements  IMainServiceBase, Serializable {

    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    protected EntityManager getEntityManager() {
        return em;
    }

    protected DataBaseType getDataBaseType() {
        return DataBaseType.POSTGRESSQL;
    }

    private final Logger logger = LoggerFactory.getLogger(MainServiceBase.class);

    @EJB
    private gr.neuropublic.Niva.facades.IAgencyFacade.ILocal agencyFacade;

    public gr.neuropublic.Niva.facades.IAgencyFacade.ILocal getAgencyFacade()
    {
        return agencyFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IAgrisnapUsersFacade.ILocal agrisnapUsersFacade;

    public gr.neuropublic.Niva.facades.IAgrisnapUsersFacade.ILocal getAgrisnapUsersFacade()
    {
        return agrisnapUsersFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IAgrisnapUsersProducersFacade.ILocal agrisnapUsersProducersFacade;

    public gr.neuropublic.Niva.facades.IAgrisnapUsersProducersFacade.ILocal getAgrisnapUsersProducersFacade()
    {
        return agrisnapUsersProducersFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IClassificationFacade.ILocal classificationFacade;

    public gr.neuropublic.Niva.facades.IClassificationFacade.ILocal getClassificationFacade()
    {
        return classificationFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IClassifierFacade.ILocal classifierFacade;

    public gr.neuropublic.Niva.facades.IClassifierFacade.ILocal getClassifierFacade()
    {
        return classifierFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ICoverTypeFacade.ILocal coverTypeFacade;

    public gr.neuropublic.Niva.facades.ICoverTypeFacade.ILocal getCoverTypeFacade()
    {
        return coverTypeFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ICultivationFacade.ILocal cultivationFacade;

    public gr.neuropublic.Niva.facades.ICultivationFacade.ILocal getCultivationFacade()
    {
        return cultivationFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IDbmanagementlogFacade.ILocal dbmanagementlogFacade;

    public gr.neuropublic.Niva.facades.IDbmanagementlogFacade.ILocal getDbmanagementlogFacade()
    {
        return dbmanagementlogFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IDecisionMakingFacade.ILocal decisionMakingFacade;

    public gr.neuropublic.Niva.facades.IDecisionMakingFacade.ILocal getDecisionMakingFacade()
    {
        return decisionMakingFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IDocumentFacade.ILocal documentFacade;

    public gr.neuropublic.Niva.facades.IDocumentFacade.ILocal getDocumentFacade()
    {
        return documentFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IEcCultFacade.ILocal ecCultFacade;

    public gr.neuropublic.Niva.facades.IEcCultFacade.ILocal getEcCultFacade()
    {
        return ecCultFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IEcCultCopyFacade.ILocal ecCultCopyFacade;

    public gr.neuropublic.Niva.facades.IEcCultCopyFacade.ILocal getEcCultCopyFacade()
    {
        return ecCultCopyFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IEcCultDetailFacade.ILocal ecCultDetailFacade;

    public gr.neuropublic.Niva.facades.IEcCultDetailFacade.ILocal getEcCultDetailFacade()
    {
        return ecCultDetailFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IEcCultDetailCopyFacade.ILocal ecCultDetailCopyFacade;

    public gr.neuropublic.Niva.facades.IEcCultDetailCopyFacade.ILocal getEcCultDetailCopyFacade()
    {
        return ecCultDetailCopyFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IEcGroupFacade.ILocal ecGroupFacade;

    public gr.neuropublic.Niva.facades.IEcGroupFacade.ILocal getEcGroupFacade()
    {
        return ecGroupFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IEcGroupCopyFacade.ILocal ecGroupCopyFacade;

    public gr.neuropublic.Niva.facades.IEcGroupCopyFacade.ILocal getEcGroupCopyFacade()
    {
        return ecGroupCopyFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IExcelErrorFacade.ILocal excelErrorFacade;

    public gr.neuropublic.Niva.facades.IExcelErrorFacade.ILocal getExcelErrorFacade()
    {
        return excelErrorFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IExcelFileFacade.ILocal excelFileFacade;

    public gr.neuropublic.Niva.facades.IExcelFileFacade.ILocal getExcelFileFacade()
    {
        return excelFileFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IFileDirPathFacade.ILocal fileDirPathFacade;

    public gr.neuropublic.Niva.facades.IFileDirPathFacade.ILocal getFileDirPathFacade()
    {
        return fileDirPathFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IFileTemplateFacade.ILocal fileTemplateFacade;

    public gr.neuropublic.Niva.facades.IFileTemplateFacade.ILocal getFileTemplateFacade()
    {
        return fileTemplateFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IFmisDecisionFacade.ILocal fmisDecisionFacade;

    public gr.neuropublic.Niva.facades.IFmisDecisionFacade.ILocal getFmisDecisionFacade()
    {
        return fmisDecisionFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IFmisUploadFacade.ILocal fmisUploadFacade;

    public gr.neuropublic.Niva.facades.IFmisUploadFacade.ILocal getFmisUploadFacade()
    {
        return fmisUploadFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IFmisUserFacade.ILocal fmisUserFacade;

    public gr.neuropublic.Niva.facades.IFmisUserFacade.ILocal getFmisUserFacade()
    {
        return fmisUserFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IGpDecisionFacade.ILocal gpDecisionFacade;

    public gr.neuropublic.Niva.facades.IGpDecisionFacade.ILocal getGpDecisionFacade()
    {
        return gpDecisionFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IGpRequestsFacade.ILocal gpRequestsFacade;

    public gr.neuropublic.Niva.facades.IGpRequestsFacade.ILocal getGpRequestsFacade()
    {
        return gpRequestsFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IGpRequestsContextsFacade.ILocal gpRequestsContextsFacade;

    public gr.neuropublic.Niva.facades.IGpRequestsContextsFacade.ILocal getGpRequestsContextsFacade()
    {
        return gpRequestsContextsFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IGpRequestsProducersFacade.ILocal gpRequestsProducersFacade;

    public gr.neuropublic.Niva.facades.IGpRequestsProducersFacade.ILocal getGpRequestsProducersFacade()
    {
        return gpRequestsProducersFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IGpUploadFacade.ILocal gpUploadFacade;

    public gr.neuropublic.Niva.facades.IGpUploadFacade.ILocal getGpUploadFacade()
    {
        return gpUploadFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IIntegrateddecisionFacade.ILocal integrateddecisionFacade;

    public gr.neuropublic.Niva.facades.IIntegrateddecisionFacade.ILocal getIntegrateddecisionFacade()
    {
        return integrateddecisionFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IParcelClasFacade.ILocal parcelClasFacade;

    public gr.neuropublic.Niva.facades.IParcelClasFacade.ILocal getParcelClasFacade()
    {
        return parcelClasFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IParcelDecisionFacade.ILocal parcelDecisionFacade;

    public gr.neuropublic.Niva.facades.IParcelDecisionFacade.ILocal getParcelDecisionFacade()
    {
        return parcelDecisionFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IParcelsIssuesFacade.ILocal parcelsIssuesFacade;

    public gr.neuropublic.Niva.facades.IParcelsIssuesFacade.ILocal getParcelsIssuesFacade()
    {
        return parcelsIssuesFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IParcelsIssuesActivitiesFacade.ILocal parcelsIssuesActivitiesFacade;

    public gr.neuropublic.Niva.facades.IParcelsIssuesActivitiesFacade.ILocal getParcelsIssuesActivitiesFacade()
    {
        return parcelsIssuesActivitiesFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IParcelsIssuesStatusPerDemaFacade.ILocal parcelsIssuesStatusPerDemaFacade;

    public gr.neuropublic.Niva.facades.IParcelsIssuesStatusPerDemaFacade.ILocal getParcelsIssuesStatusPerDemaFacade()
    {
        return parcelsIssuesStatusPerDemaFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IPredefColFacade.ILocal predefColFacade;

    public gr.neuropublic.Niva.facades.IPredefColFacade.ILocal getPredefColFacade()
    {
        return predefColFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IProducersFacade.ILocal producersFacade;

    public gr.neuropublic.Niva.facades.IProducersFacade.ILocal getProducersFacade()
    {
        return producersFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.IStatisticFacade.ILocal statisticFacade;

    public gr.neuropublic.Niva.facades.IStatisticFacade.ILocal getStatisticFacade()
    {
        return statisticFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ISuperClasFacade.ILocal superClasFacade;

    public gr.neuropublic.Niva.facades.ISuperClasFacade.ILocal getSuperClasFacade()
    {
        return superClasFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ISuperClassDetailFacade.ILocal superClassDetailFacade;

    public gr.neuropublic.Niva.facades.ISuperClassDetailFacade.ILocal getSuperClassDetailFacade()
    {
        return superClassDetailFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ISystemconfigurationFacade.ILocal systemconfigurationFacade;

    public gr.neuropublic.Niva.facades.ISystemconfigurationFacade.ILocal getSystemconfigurationFacade()
    {
        return systemconfigurationFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITemplateColumnFacade.ILocal templateColumnFacade;

    public gr.neuropublic.Niva.facades.ITemplateColumnFacade.ILocal getTemplateColumnFacade()
    {
        return templateColumnFacade;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;

    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade()
    {
        return tmpBlobFacade;
    }


    public gr.neuropublic.validators.ICheckedEntity createEntityFromJson(Map<String,Object> keysMap, String entityName, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.base.CryptoUtils crypto, Map<String,Set<String>> allowed_entity_fields) {
        if ("Agency".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return agencyFacade.createFromJson(keysMap, crypto, _changeStatus, json, allowedFields);
        }
        if ("AgrisnapUsers".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return agrisnapUsersFacade.createFromJson(keysMap, crypto, _changeStatus, json, allowedFields);
        }
        if ("AgrisnapUsersProducers".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return agrisnapUsersProducersFacade.createFromJson(keysMap, crypto, _changeStatus, json, agrisnapUsersFacade, producersFacade, allowedFields);
        }
        if ("Classification".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return classificationFacade.createFromJson(keysMap, crypto, _changeStatus, json, classifierFacade, fileTemplateFacade, allowedFields);
        }
        if ("Classifier".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return classifierFacade.createFromJson(keysMap, crypto, _changeStatus, json, allowedFields);
        }
        if ("CoverType".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return coverTypeFacade.createFromJson(keysMap, crypto, _changeStatus, json, excelFileFacade, allowedFields);
        }
        if ("Cultivation".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return cultivationFacade.createFromJson(keysMap, crypto, _changeStatus, json, coverTypeFacade, excelFileFacade, allowedFields);
        }
        if ("Dbmanagementlog".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return dbmanagementlogFacade.createFromJson(keysMap, crypto, _changeStatus, json, allowedFields);
        }
        if ("DecisionMaking".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return decisionMakingFacade.createFromJson(keysMap, crypto, _changeStatus, json, classificationFacade, ecGroupFacade, allowedFields);
        }
        if ("Document".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return documentFacade.createFromJson(keysMap, crypto, _changeStatus, json, allowedFields);
        }
        if ("EcCult".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return ecCultFacade.createFromJson(keysMap, crypto, _changeStatus, json, cultivationFacade, ecGroupFacade, coverTypeFacade, superClasFacade, allowedFields);
        }
        if ("EcCultCopy".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return ecCultCopyFacade.createFromJson(keysMap, crypto, _changeStatus, json, cultivationFacade, ecGroupCopyFacade, coverTypeFacade, superClasFacade, allowedFields);
        }
        if ("EcCultDetail".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return ecCultDetailFacade.createFromJson(keysMap, crypto, _changeStatus, json, ecCultFacade, allowedFields);
        }
        if ("EcCultDetailCopy".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return ecCultDetailCopyFacade.createFromJson(keysMap, crypto, _changeStatus, json, ecCultCopyFacade, allowedFields);
        }
        if ("EcGroup".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return ecGroupFacade.createFromJson(keysMap, crypto, _changeStatus, json, allowedFields);
        }
        if ("EcGroupCopy".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return ecGroupCopyFacade.createFromJson(keysMap, crypto, _changeStatus, json, decisionMakingFacade, allowedFields);
        }
        if ("ExcelError".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return excelErrorFacade.createFromJson(keysMap, crypto, _changeStatus, json, excelFileFacade, allowedFields);
        }
        if ("ExcelFile".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return excelFileFacade.createFromJson(keysMap, crypto, _changeStatus, json, allowedFields);
        }
        if ("FileDirPath".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return fileDirPathFacade.createFromJson(keysMap, crypto, _changeStatus, json, allowedFields);
        }
        if ("FileTemplate".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return fileTemplateFacade.createFromJson(keysMap, crypto, _changeStatus, json, allowedFields);
        }
        if ("FmisDecision".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return fmisDecisionFacade.createFromJson(keysMap, crypto, _changeStatus, json, parcelsIssuesFacade, cultivationFacade, coverTypeFacade, allowedFields);
        }
        if ("FmisUpload".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return fmisUploadFacade.createFromJson(keysMap, crypto, _changeStatus, json, parcelsIssuesFacade, allowedFields);
        }
        if ("FmisUser".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return fmisUserFacade.createFromJson(keysMap, crypto, _changeStatus, json, allowedFields);
        }
        if ("GpDecision".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return gpDecisionFacade.createFromJson(keysMap, crypto, _changeStatus, json, parcelsIssuesFacade, cultivationFacade, coverTypeFacade, allowedFields);
        }
        if ("GpRequests".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return gpRequestsFacade.createFromJson(keysMap, crypto, _changeStatus, json, agrisnapUsersFacade, allowedFields);
        }
        if ("GpRequestsContexts".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return gpRequestsContextsFacade.createFromJson(keysMap, crypto, _changeStatus, json, gpRequestsProducersFacade, parcelsIssuesFacade, allowedFields);
        }
        if ("GpRequestsProducers".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return gpRequestsProducersFacade.createFromJson(keysMap, crypto, _changeStatus, json, gpRequestsFacade, producersFacade, allowedFields);
        }
        if ("GpUpload".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return gpUploadFacade.createFromJson(keysMap, crypto, _changeStatus, json, gpRequestsContextsFacade, allowedFields);
        }
        if ("Integrateddecision".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return integrateddecisionFacade.createFromJson(keysMap, crypto, _changeStatus, json, parcelClasFacade, decisionMakingFacade, allowedFields);
        }
        if ("ParcelClas".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return parcelClasFacade.createFromJson(keysMap, crypto, _changeStatus, json, classificationFacade, cultivationFacade, coverTypeFacade, allowedFields);
        }
        if ("ParcelDecision".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return parcelDecisionFacade.createFromJson(keysMap, crypto, _changeStatus, json, decisionMakingFacade, parcelClasFacade, allowedFields);
        }
        if ("ParcelsIssues".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return parcelsIssuesFacade.createFromJson(keysMap, crypto, _changeStatus, json, parcelClasFacade, allowedFields);
        }
        if ("ParcelsIssuesActivities".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return parcelsIssuesActivitiesFacade.createFromJson(keysMap, crypto, _changeStatus, json, parcelsIssuesFacade, allowedFields);
        }
        if ("ParcelsIssuesStatusPerDema".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return parcelsIssuesStatusPerDemaFacade.createFromJson(keysMap, crypto, _changeStatus, json, parcelsIssuesFacade, allowedFields);
        }
        if ("PredefCol".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return predefColFacade.createFromJson(keysMap, crypto, _changeStatus, json, allowedFields);
        }
        if ("Producers".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return producersFacade.createFromJson(keysMap, crypto, _changeStatus, json, allowedFields);
        }
        if ("Statistic".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return statisticFacade.createFromJson(keysMap, crypto, _changeStatus, json, classificationFacade, allowedFields);
        }
        if ("SuperClas".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return superClasFacade.createFromJson(keysMap, crypto, _changeStatus, json, allowedFields);
        }
        if ("SuperClassDetail".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return superClassDetailFacade.createFromJson(keysMap, crypto, _changeStatus, json, superClasFacade, cultivationFacade, allowedFields);
        }
        if ("Systemconfiguration".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return systemconfigurationFacade.createFromJson(keysMap, crypto, _changeStatus, json, allowedFields);
        }
        if ("TemplateColumn".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return templateColumnFacade.createFromJson(keysMap, crypto, _changeStatus, json, fileTemplateFacade, predefColFacade, allowedFields);
        }
        if ("TmpBlob".equals(entityName)){
            Set<String> allowedFields = allowed_entity_fields.get(entityName);
            return tmpBlobFacade.createFromJson(keysMap, crypto, _changeStatus, json, allowedFields);
        }
        
        throw new GenericApplicationException("Unknown entity name");
    }

    public String getLogoProvider(UserSession usrSession) {
        try {
            String logoProviderUrl = "img/gaia-logo.jpg";
            return logoProviderUrl;
        } catch (Exception ex) {
            throw new GenericApplicationException(ex.getMessage());
        }
        
    }

    public List<String> finalizationEcGroup(Integer ecgrId, UserSession usrSession) 
    {
        throw new GenericApplicationException("Not Implemented function finalizationEcGroup");
    }

    public List<String> runningDecisionMaking(Integer demaId, UserSession usrSession) 
    {
        throw new GenericApplicationException("Not Implemented function runningDecisionMaking");
    }

    public List<String> importingClassification(Integer classId, UserSession usrSession) 
    {
        throw new GenericApplicationException("Not Implemented function importingClassification");
    }

    public String getPhotoRequests(String agrisnapUid) 
    {
        throw new GenericApplicationException("Not Implemented function getPhotoRequests");
    }

    public String uploadPhoto(String hash, String attachment, String data, String environment, Integer id) 
    {
        throw new GenericApplicationException("Not Implemented function uploadPhoto");
    }

    public String getBRERunsResultsList(Boolean info, UserSession usrSession) 
    {
        throw new GenericApplicationException("Not Implemented function getBRERunsResultsList");
    }

    public String getBRERunResults(Boolean info, Integer id, UserSession usrSession) 
    {
        throw new GenericApplicationException("Not Implemented function getBRERunResults");
    }

    public String getFMISRequests(String fmisUid) 
    {
        throw new GenericApplicationException("Not Implemented function getFMISRequests");
    }

    public String uploadFMIS(String fmisUid, Integer id, String attachment, String metadata) 
    {
        throw new GenericApplicationException("Not Implemented function uploadFMIS");
    }

    public List<String> updateIntegratedDecisionsNIssues(Integer pclaId, UserSession usrSession) 
    {
        throw new GenericApplicationException("Not Implemented function updateIntegratedDecisionsNIssues");
    }

    public List<String> updateIntegratedDecisionsNIssuesFromDema(Integer demaId, UserSession usrSession) 
    {
        throw new GenericApplicationException("Not Implemented function updateIntegratedDecisionsNIssuesFromDema");
    }

    public List<String> actionPushToCommonsAPI(Integer demaId, UserSession usrSession) 
    {
        throw new GenericApplicationException("Not Implemented function actionPushToCommonsAPI");
    }


    private final  Set<String> allowedEntitiesFor_insert_Agency = new HashSet<String>(Arrays.asList("Agency"));

    private final  Set<String> allowedEntitiesFor_delete_Agency = new HashSet<String>(Arrays.asList("Agency"));

    private final  Set<String> allowedEntitiesFor_update_Agency = new HashSet<String>(Arrays.asList("Agency"));

    private final Map<String, Set<String>> allowed_entity_fields_Agency = new HashMap<String, Set<String>>(){{
        put("Agency",new HashSet<String>(Arrays.asList("agenId", "name", "country", "rowVersion", "srid")));
    }};

    protected List<ChangeToCommit<ICheckedEntity>> getChangesToCommit_Agency(String userName, String jsonData, Map<String, Object> keysMap, UserSession usrSession) {
        if (!usrSession.privileges.contains("Niva_Agency_W"))
            throw new DatabaseGenericException("NO_WRITE_ACCESS");
        List<ChangeToCommit<ICheckedEntity>> ret = super.getChangesToCommit__generic(userName, jsonData, keysMap, usrSession.crypto, allowed_entity_fields_Agency);

        for (ChangeToCommit<ICheckedEntity> cc : ret) {
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE)  && !usrSession.privileges.contains("Niva_Agency_D")){
                throw new DatabaseGenericException("NO_DELETE_ACCESS");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.NEW) && !allowedEntitiesFor_insert_Agency.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_INSERTION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE) && !allowedEntitiesFor_delete_Agency.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_DELETION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.UPDATE) && !allowedEntitiesFor_update_Agency.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_UPDATE");
            }
        }
        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SaveResponse synchronizeChangesWithDbJS_Agency(String userName, String jsonData, UserSession usrSession)
    {

        Map<String,Object> keysMap = new HashMap<>(); 
            
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.COMMIT);
        List<ChangeToCommit<ICheckedEntity>> changesToCommit = getChangesToCommit_Agency(userName, jsonData, keysMap, usrSession);
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.AUTO);
           
         
        // Post Data to DB
        synchronizeChangesWithDb_internal(usrSession, changesToCommit, false);
        
        SaveResponse ret = new SaveResponse(); 
        ret.newEntitiesIds = getNewEntitiesIds(keysMap, usrSession.crypto);
       
        return ret;
    }


    private final  Set<String> allowedEntitiesFor_insert_Classification = new HashSet<String>(Arrays.asList("Classification"));

    private final  Set<String> allowedEntitiesFor_delete_Classification = new HashSet<String>(Arrays.asList("Classification"));

    private final  Set<String> allowedEntitiesFor_update_Classification = new HashSet<String>(Arrays.asList("Classification"));

    private final Map<String, Set<String>> allowed_entity_fields_Classification = new HashMap<String, Set<String>>(){{
        put("Classification",new HashSet<String>(Arrays.asList("clasId", "name", "description", "dateTime", "rowVersion", "recordtype", "filePath", "attachedFile", "year", "isImported", "statisticCollection", "parcelClasCollection", "decisionMakingCollection", "clfrId", "fiteId")));
        put("ParcelClas",new HashSet<String>(Arrays.asList("pclaId", "probPred", "probPred2", "prodCode", "parcIdentifier", "parcCode", "geom4326", "integrateddecisionCollection", "parcelDecisionCollection", "parcelsIssuesCollection", "clasId", "cultIdDecl", "cultIdPred", "cotyIdDecl", "cotyIdPred", "cultIdPred2")));
    }};

    protected List<ChangeToCommit<ICheckedEntity>> getChangesToCommit_Classification(String userName, String jsonData, Map<String, Object> keysMap, UserSession usrSession) {
        if (!usrSession.privileges.contains("Niva_Classification_W"))
            throw new DatabaseGenericException("NO_WRITE_ACCESS");
        List<ChangeToCommit<ICheckedEntity>> ret = super.getChangesToCommit__generic(userName, jsonData, keysMap, usrSession.crypto, allowed_entity_fields_Classification);

        for (ChangeToCommit<ICheckedEntity> cc : ret) {
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE)  && !usrSession.privileges.contains("Niva_Classification_D")){
                throw new DatabaseGenericException("NO_DELETE_ACCESS");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.NEW) && !allowedEntitiesFor_insert_Classification.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_INSERTION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE) && !allowedEntitiesFor_delete_Classification.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_DELETION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.UPDATE) && !allowedEntitiesFor_update_Classification.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_UPDATE");
            }
        }
        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SaveResponse synchronizeChangesWithDbJS_Classification(String userName, String jsonData, UserSession usrSession)
    {

        Map<String,Object> keysMap = new HashMap<>(); 
            
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.COMMIT);
        List<ChangeToCommit<ICheckedEntity>> changesToCommit = getChangesToCommit_Classification(userName, jsonData, keysMap, usrSession);
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.AUTO);
           
         
        // Post Data to DB
        synchronizeChangesWithDb_internal(usrSession, changesToCommit, false);
        
        SaveResponse ret = new SaveResponse(); 
        ret.newEntitiesIds = getNewEntitiesIds(keysMap, usrSession.crypto);
       
        return ret;
    }


    private final  Set<String> allowedEntitiesFor_insert_Classifier = new HashSet<String>(Arrays.asList("Classifier"));

    private final  Set<String> allowedEntitiesFor_delete_Classifier = new HashSet<String>(Arrays.asList("Classifier"));

    private final  Set<String> allowedEntitiesFor_update_Classifier = new HashSet<String>(Arrays.asList("Classifier"));

    private final Map<String, Set<String>> allowed_entity_fields_Classifier = new HashMap<String, Set<String>>(){{
        put("Classifier",new HashSet<String>(Arrays.asList("clfrId", "name", "description", "rowVersion", "classificationCollection")));
    }};

    protected List<ChangeToCommit<ICheckedEntity>> getChangesToCommit_Classifier(String userName, String jsonData, Map<String, Object> keysMap, UserSession usrSession) {
        if (!usrSession.privileges.contains("Niva_Classifier_W"))
            throw new DatabaseGenericException("NO_WRITE_ACCESS");
        List<ChangeToCommit<ICheckedEntity>> ret = super.getChangesToCommit__generic(userName, jsonData, keysMap, usrSession.crypto, allowed_entity_fields_Classifier);

        for (ChangeToCommit<ICheckedEntity> cc : ret) {
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE)  && !usrSession.privileges.contains("Niva_Classifier_D")){
                throw new DatabaseGenericException("NO_DELETE_ACCESS");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.NEW) && !allowedEntitiesFor_insert_Classifier.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_INSERTION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE) && !allowedEntitiesFor_delete_Classifier.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_DELETION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.UPDATE) && !allowedEntitiesFor_update_Classifier.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_UPDATE");
            }
        }
        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SaveResponse synchronizeChangesWithDbJS_Classifier(String userName, String jsonData, UserSession usrSession)
    {

        Map<String,Object> keysMap = new HashMap<>(); 
            
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.COMMIT);
        List<ChangeToCommit<ICheckedEntity>> changesToCommit = getChangesToCommit_Classifier(userName, jsonData, keysMap, usrSession);
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.AUTO);
           
         
        // Post Data to DB
        synchronizeChangesWithDb_internal(usrSession, changesToCommit, false);
        
        SaveResponse ret = new SaveResponse(); 
        ret.newEntitiesIds = getNewEntitiesIds(keysMap, usrSession.crypto);
       
        return ret;
    }


    private final  Set<String> allowedEntitiesFor_insert_CoverType = new HashSet<String>(Arrays.asList("CoverType"));

    private final  Set<String> allowedEntitiesFor_delete_CoverType = new HashSet<String>(Arrays.asList("CoverType"));

    private final  Set<String> allowedEntitiesFor_update_CoverType = new HashSet<String>(Arrays.asList("CoverType"));

    private final Map<String, Set<String>> allowed_entity_fields_CoverType = new HashMap<String, Set<String>>(){{
        put("CoverType",new HashSet<String>(Arrays.asList("cotyId", "code", "name", "rowVersion", "cultivationCollection", "ecCultCollection", "fmisDecisionCollection", "gpDecisionCollection", "ecCultCopyCollection", "parcelClascoty_id_declCollection", "parcelClascoty_id_predCollection", "exfiId")));
    }};

    protected List<ChangeToCommit<ICheckedEntity>> getChangesToCommit_CoverType(String userName, String jsonData, Map<String, Object> keysMap, UserSession usrSession) {
        if (!usrSession.privileges.contains("Niva_CoverType_W"))
            throw new DatabaseGenericException("NO_WRITE_ACCESS");
        List<ChangeToCommit<ICheckedEntity>> ret = super.getChangesToCommit__generic(userName, jsonData, keysMap, usrSession.crypto, allowed_entity_fields_CoverType);

        for (ChangeToCommit<ICheckedEntity> cc : ret) {
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE)  && !usrSession.privileges.contains("Niva_CoverType_D")){
                throw new DatabaseGenericException("NO_DELETE_ACCESS");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.NEW) && !allowedEntitiesFor_insert_CoverType.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_INSERTION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE) && !allowedEntitiesFor_delete_CoverType.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_DELETION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.UPDATE) && !allowedEntitiesFor_update_CoverType.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_UPDATE");
            }
        }
        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SaveResponse synchronizeChangesWithDbJS_CoverType(String userName, String jsonData, UserSession usrSession)
    {

        Map<String,Object> keysMap = new HashMap<>(); 
            
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.COMMIT);
        List<ChangeToCommit<ICheckedEntity>> changesToCommit = getChangesToCommit_CoverType(userName, jsonData, keysMap, usrSession);
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.AUTO);
           
         
        // Post Data to DB
        synchronizeChangesWithDb_internal(usrSession, changesToCommit, false);
        
        SaveResponse ret = new SaveResponse(); 
        ret.newEntitiesIds = getNewEntitiesIds(keysMap, usrSession.crypto);
       
        return ret;
    }


    private final  Set<String> allowedEntitiesFor_insert_Cultivation = new HashSet<String>(Arrays.asList("Cultivation"));

    private final  Set<String> allowedEntitiesFor_delete_Cultivation = new HashSet<String>(Arrays.asList("Cultivation"));

    private final  Set<String> allowedEntitiesFor_update_Cultivation = new HashSet<String>(Arrays.asList("Cultivation"));

    private final Map<String, Set<String>> allowed_entity_fields_Cultivation = new HashMap<String, Set<String>>(){{
        put("Cultivation",new HashSet<String>(Arrays.asList("cultId", "name", "code", "rowVersion", "ecCultCollection", "fmisDecisionCollection", "superClassDetailCollection", "gpDecisionCollection", "ecCultCopyCollection", "parcelClascult_id_declCollection", "parcelClascult_id_predCollection", "parcelClascult_id_pred2Collection", "cotyId", "exfiId")));
    }};

    protected List<ChangeToCommit<ICheckedEntity>> getChangesToCommit_Cultivation(String userName, String jsonData, Map<String, Object> keysMap, UserSession usrSession) {
        if (!usrSession.privileges.contains("Niva_Cultivation_W"))
            throw new DatabaseGenericException("NO_WRITE_ACCESS");
        List<ChangeToCommit<ICheckedEntity>> ret = super.getChangesToCommit__generic(userName, jsonData, keysMap, usrSession.crypto, allowed_entity_fields_Cultivation);

        for (ChangeToCommit<ICheckedEntity> cc : ret) {
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE)  && !usrSession.privileges.contains("Niva_Cultivation_D")){
                throw new DatabaseGenericException("NO_DELETE_ACCESS");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.NEW) && !allowedEntitiesFor_insert_Cultivation.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_INSERTION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE) && !allowedEntitiesFor_delete_Cultivation.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_DELETION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.UPDATE) && !allowedEntitiesFor_update_Cultivation.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_UPDATE");
            }
        }
        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SaveResponse synchronizeChangesWithDbJS_Cultivation(String userName, String jsonData, UserSession usrSession)
    {

        Map<String,Object> keysMap = new HashMap<>(); 
            
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.COMMIT);
        List<ChangeToCommit<ICheckedEntity>> changesToCommit = getChangesToCommit_Cultivation(userName, jsonData, keysMap, usrSession);
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.AUTO);
           
         
        // Post Data to DB
        synchronizeChangesWithDb_internal(usrSession, changesToCommit, false);
        
        SaveResponse ret = new SaveResponse(); 
        ret.newEntitiesIds = getNewEntitiesIds(keysMap, usrSession.crypto);
       
        return ret;
    }


    private final  Set<String> allowedEntitiesFor_insert_DecisionMaking = new HashSet<String>(Arrays.asList("DecisionMaking"));

    private final  Set<String> allowedEntitiesFor_delete_DecisionMaking = new HashSet<String>(Arrays.asList("DecisionMaking"));

    private final  Set<String> allowedEntitiesFor_update_DecisionMaking = new HashSet<String>(Arrays.asList("DecisionMaking", "ParcelDecision"));

    private final Map<String, Set<String>> allowed_entity_fields_DecisionMaking = new HashMap<String, Set<String>>(){{
        put("DecisionMaking",new HashSet<String>(Arrays.asList("demaId", "description", "dateTime", "rowVersion", "recordtype", "hasBeenRun", "hasPopulatedIntegratedDecisionsAndIssues", "integrateddecisionCollection", "ecGroupCopyCollection", "parcelDecisionCollection", "clasId", "ecgrId")));
        put("ParcelDecision",new HashSet<String>(Arrays.asList("padeId", "decisionLight", "rowVersion", "demaId", "pclaId")));
        put("EcGroupCopy",new HashSet<String>(Arrays.asList("ecgcId", "description", "name", "rowVersion", "recordtype", "cropLevel", "ecCultCopyCollection", "demaId")));
        put("EcCultCopy",new HashSet<String>(Arrays.asList("ecccId", "noneMatchDecision", "rowVersion", "ecCultDetailCopyCollection", "cultId", "ecgcId", "cotyId", "sucaId")));
        put("EcCultDetailCopy",new HashSet<String>(Arrays.asList("ecdcId", "orderingNumber", "agreesDeclar", "comparisonOper", "probabThres", "decisionLight", "rowVersion", "agreesDeclar2", "comparisonOper2", "probabThres2", "probabThresSum", "comparisonOper3", "ecccId")));
    }};

    protected List<ChangeToCommit<ICheckedEntity>> getChangesToCommit_DecisionMaking(String userName, String jsonData, Map<String, Object> keysMap, UserSession usrSession) {
        if (!usrSession.privileges.contains("Niva_DecisionMaking_W"))
            throw new DatabaseGenericException("NO_WRITE_ACCESS");
        List<ChangeToCommit<ICheckedEntity>> ret = super.getChangesToCommit__generic(userName, jsonData, keysMap, usrSession.crypto, allowed_entity_fields_DecisionMaking);

        for (ChangeToCommit<ICheckedEntity> cc : ret) {
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE)  && !usrSession.privileges.contains("Niva_DecisionMaking_D")){
                throw new DatabaseGenericException("NO_DELETE_ACCESS");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.NEW) && !allowedEntitiesFor_insert_DecisionMaking.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_INSERTION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE) && !allowedEntitiesFor_delete_DecisionMaking.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_DELETION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.UPDATE) && !allowedEntitiesFor_update_DecisionMaking.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_UPDATE");
            }
        }
        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SaveResponse synchronizeChangesWithDbJS_DecisionMaking(String userName, String jsonData, UserSession usrSession)
    {

        Map<String,Object> keysMap = new HashMap<>(); 
            
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.COMMIT);
        List<ChangeToCommit<ICheckedEntity>> changesToCommit = getChangesToCommit_DecisionMaking(userName, jsonData, keysMap, usrSession);
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.AUTO);
           
         
        // Post Data to DB
        synchronizeChangesWithDb_internal(usrSession, changesToCommit, false);
        
        SaveResponse ret = new SaveResponse(); 
        ret.newEntitiesIds = getNewEntitiesIds(keysMap, usrSession.crypto);
       
        return ret;
    }


    private final  Set<String> allowedEntitiesFor_insert_DecisionMakingRO = new HashSet<String>();

    private final  Set<String> allowedEntitiesFor_delete_DecisionMakingRO = new HashSet<String>();

    private final  Set<String> allowedEntitiesFor_update_DecisionMakingRO = new HashSet<String>();

    private final Map<String, Set<String>> allowed_entity_fields_DecisionMakingRO = new HashMap<String, Set<String>>(){{
        put("DecisionMaking",new HashSet<String>(Arrays.asList("demaId", "description", "dateTime", "rowVersion", "recordtype", "hasBeenRun", "hasPopulatedIntegratedDecisionsAndIssues", "integrateddecisionCollection", "ecGroupCopyCollection", "parcelDecisionCollection", "clasId", "ecgrId")));
        put("Integrateddecision",new HashSet<String>(Arrays.asList("integrateddecisionsId", "decisionCode", "dteUpdate", "usrUpdate", "pclaId", "demaId")));
    }};

    protected List<ChangeToCommit<ICheckedEntity>> getChangesToCommit_DecisionMakingRO(String userName, String jsonData, Map<String, Object> keysMap, UserSession usrSession) {
        if (!usrSession.privileges.contains("Niva_DecisionMakingRO_W"))
            throw new DatabaseGenericException("NO_WRITE_ACCESS");
        List<ChangeToCommit<ICheckedEntity>> ret = super.getChangesToCommit__generic(userName, jsonData, keysMap, usrSession.crypto, allowed_entity_fields_DecisionMakingRO);

        for (ChangeToCommit<ICheckedEntity> cc : ret) {
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE)  && !usrSession.privileges.contains("Niva_DecisionMakingRO_D")){
                throw new DatabaseGenericException("NO_DELETE_ACCESS");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.NEW) && !allowedEntitiesFor_insert_DecisionMakingRO.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_INSERTION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE) && !allowedEntitiesFor_delete_DecisionMakingRO.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_DELETION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.UPDATE) && !allowedEntitiesFor_update_DecisionMakingRO.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_UPDATE");
            }
        }
        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SaveResponse synchronizeChangesWithDbJS_DecisionMakingRO(String userName, String jsonData, UserSession usrSession)
    {

        Map<String,Object> keysMap = new HashMap<>(); 
            
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.COMMIT);
        List<ChangeToCommit<ICheckedEntity>> changesToCommit = getChangesToCommit_DecisionMakingRO(userName, jsonData, keysMap, usrSession);
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.AUTO);
           
         
        // Post Data to DB
        synchronizeChangesWithDb_internal(usrSession, changesToCommit, false);
        
        SaveResponse ret = new SaveResponse(); 
        ret.newEntitiesIds = getNewEntitiesIds(keysMap, usrSession.crypto);
       
        return ret;
    }


    private final  Set<String> allowedEntitiesFor_insert_EcGroup = new HashSet<String>(Arrays.asList("EcGroup", "EcCult", "EcCultDetail"));

    private final  Set<String> allowedEntitiesFor_delete_EcGroup = new HashSet<String>(Arrays.asList("EcGroup", "EcCult", "EcCultDetail"));

    private final  Set<String> allowedEntitiesFor_update_EcGroup = new HashSet<String>(Arrays.asList("EcGroup", "EcCult", "EcCultDetail"));

    private final Map<String, Set<String>> allowed_entity_fields_EcGroup = new HashMap<String, Set<String>>(){{
        put("EcGroup",new HashSet<String>(Arrays.asList("ecgrId", "description", "name", "rowVersion", "recordtype", "cropLevel", "ecCultCollection", "decisionMakingCollection")));
        put("EcCult",new HashSet<String>(Arrays.asList("eccuId", "noneMatchDecision", "rowVersion", "ecCultDetailCollection", "cultId", "ecgrId", "cotyId", "sucaId")));
        put("EcCultDetail",new HashSet<String>(Arrays.asList("eccdId", "orderingNumber", "agreesDeclar", "comparisonOper", "probabThres", "decisionLight", "rowVersion", "agreesDeclar2", "comparisonOper2", "probabThres2", "probabThresSum", "comparisonOper3", "eccuId")));
    }};

    protected List<ChangeToCommit<ICheckedEntity>> getChangesToCommit_EcGroup(String userName, String jsonData, Map<String, Object> keysMap, UserSession usrSession) {
        if (!usrSession.privileges.contains("Niva_EcGroup_W"))
            throw new DatabaseGenericException("NO_WRITE_ACCESS");
        List<ChangeToCommit<ICheckedEntity>> ret = super.getChangesToCommit__generic(userName, jsonData, keysMap, usrSession.crypto, allowed_entity_fields_EcGroup);

        for (ChangeToCommit<ICheckedEntity> cc : ret) {
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE)  && !usrSession.privileges.contains("Niva_EcGroup_D")){
                throw new DatabaseGenericException("NO_DELETE_ACCESS");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.NEW) && !allowedEntitiesFor_insert_EcGroup.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_INSERTION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE) && !allowedEntitiesFor_delete_EcGroup.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_DELETION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.UPDATE) && !allowedEntitiesFor_update_EcGroup.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_UPDATE");
            }
        }
        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SaveResponse synchronizeChangesWithDbJS_EcGroup(String userName, String jsonData, UserSession usrSession)
    {

        Map<String,Object> keysMap = new HashMap<>(); 
            
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.COMMIT);
        List<ChangeToCommit<ICheckedEntity>> changesToCommit = getChangesToCommit_EcGroup(userName, jsonData, keysMap, usrSession);
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.AUTO);
           
         
        // Post Data to DB
        synchronizeChangesWithDb_internal(usrSession, changesToCommit, false);
        
        SaveResponse ret = new SaveResponse(); 
        ret.newEntitiesIds = getNewEntitiesIds(keysMap, usrSession.crypto);
       
        return ret;
    }


    private final  Set<String> allowedEntitiesFor_insert_FileTemplate = new HashSet<String>(Arrays.asList("FileTemplate"));

    private final  Set<String> allowedEntitiesFor_delete_FileTemplate = new HashSet<String>(Arrays.asList("FileTemplate"));

    private final  Set<String> allowedEntitiesFor_update_FileTemplate = new HashSet<String>(Arrays.asList("FileTemplate", "TemplateColumn"));

    private final Map<String, Set<String>> allowed_entity_fields_FileTemplate = new HashMap<String, Set<String>>(){{
        put("FileTemplate",new HashSet<String>(Arrays.asList("fiteId", "name", "rowVersion", "templateColumnCollection", "classificationCollection")));
        put("TemplateColumn",new HashSet<String>(Arrays.asList("tecoId", "clfierName", "rowVersion", "fiteId", "prcoId")));
    }};

    protected List<ChangeToCommit<ICheckedEntity>> getChangesToCommit_FileTemplate(String userName, String jsonData, Map<String, Object> keysMap, UserSession usrSession) {
        if (!usrSession.privileges.contains("Niva_FileTemplate_W"))
            throw new DatabaseGenericException("NO_WRITE_ACCESS");
        List<ChangeToCommit<ICheckedEntity>> ret = super.getChangesToCommit__generic(userName, jsonData, keysMap, usrSession.crypto, allowed_entity_fields_FileTemplate);

        for (ChangeToCommit<ICheckedEntity> cc : ret) {
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE)  && !usrSession.privileges.contains("Niva_FileTemplate_D")){
                throw new DatabaseGenericException("NO_DELETE_ACCESS");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.NEW) && !allowedEntitiesFor_insert_FileTemplate.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_INSERTION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE) && !allowedEntitiesFor_delete_FileTemplate.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_DELETION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.UPDATE) && !allowedEntitiesFor_update_FileTemplate.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_UPDATE");
            }
        }
        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SaveResponse synchronizeChangesWithDbJS_FileTemplate(String userName, String jsonData, UserSession usrSession)
    {

        Map<String,Object> keysMap = new HashMap<>(); 
            
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.COMMIT);
        List<ChangeToCommit<ICheckedEntity>> changesToCommit = getChangesToCommit_FileTemplate(userName, jsonData, keysMap, usrSession);
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.AUTO);
           
         
        // Post Data to DB
        synchronizeChangesWithDb_internal(usrSession, changesToCommit, false);
        
        SaveResponse ret = new SaveResponse(); 
        ret.newEntitiesIds = getNewEntitiesIds(keysMap, usrSession.crypto);
       
        return ret;
    }


    private final  Set<String> allowedEntitiesFor_insert_Document = new HashSet<String>(Arrays.asList("Document"));

    private final  Set<String> allowedEntitiesFor_delete_Document = new HashSet<String>(Arrays.asList("Document"));

    private final  Set<String> allowedEntitiesFor_update_Document = new HashSet<String>(Arrays.asList("Document"));

    private final Map<String, Set<String>> allowed_entity_fields_Document = new HashMap<String, Set<String>>(){{
        put("Document",new HashSet<String>(Arrays.asList("docuId", "attachedFile", "filePath", "description", "rowVersion")));
    }};

    protected List<ChangeToCommit<ICheckedEntity>> getChangesToCommit_Document(String userName, String jsonData, Map<String, Object> keysMap, UserSession usrSession) {
        if (!usrSession.privileges.contains("Niva_Document_W"))
            throw new DatabaseGenericException("NO_WRITE_ACCESS");
        List<ChangeToCommit<ICheckedEntity>> ret = super.getChangesToCommit__generic(userName, jsonData, keysMap, usrSession.crypto, allowed_entity_fields_Document);

        for (ChangeToCommit<ICheckedEntity> cc : ret) {
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE)  && !usrSession.privileges.contains("Niva_Document_D")){
                throw new DatabaseGenericException("NO_DELETE_ACCESS");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.NEW) && !allowedEntitiesFor_insert_Document.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_INSERTION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE) && !allowedEntitiesFor_delete_Document.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_DELETION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.UPDATE) && !allowedEntitiesFor_update_Document.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_UPDATE");
            }
        }
        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SaveResponse synchronizeChangesWithDbJS_Document(String userName, String jsonData, UserSession usrSession)
    {

        Map<String,Object> keysMap = new HashMap<>(); 
            
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.COMMIT);
        List<ChangeToCommit<ICheckedEntity>> changesToCommit = getChangesToCommit_Document(userName, jsonData, keysMap, usrSession);
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.AUTO);
           
         
        // Post Data to DB
        synchronizeChangesWithDb_internal(usrSession, changesToCommit, false);
        
        SaveResponse ret = new SaveResponse(); 
        ret.newEntitiesIds = getNewEntitiesIds(keysMap, usrSession.crypto);
       
        return ret;
    }


    private final  Set<String> allowedEntitiesFor_insert_FileDirPath = new HashSet<String>(Arrays.asList("FileDirPath"));

    private final  Set<String> allowedEntitiesFor_delete_FileDirPath = new HashSet<String>(Arrays.asList("FileDirPath"));

    private final  Set<String> allowedEntitiesFor_update_FileDirPath = new HashSet<String>(Arrays.asList("FileDirPath"));

    private final Map<String, Set<String>> allowed_entity_fields_FileDirPath = new HashMap<String, Set<String>>(){{
        put("FileDirPath",new HashSet<String>(Arrays.asList("fidrId", "directory", "rowVersion", "fileType")));
    }};

    protected List<ChangeToCommit<ICheckedEntity>> getChangesToCommit_FileDirPath(String userName, String jsonData, Map<String, Object> keysMap, UserSession usrSession) {
        if (!usrSession.privileges.contains("Niva_FileDirPath_W"))
            throw new DatabaseGenericException("NO_WRITE_ACCESS");
        List<ChangeToCommit<ICheckedEntity>> ret = super.getChangesToCommit__generic(userName, jsonData, keysMap, usrSession.crypto, allowed_entity_fields_FileDirPath);

        for (ChangeToCommit<ICheckedEntity> cc : ret) {
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE)  && !usrSession.privileges.contains("Niva_FileDirPath_D")){
                throw new DatabaseGenericException("NO_DELETE_ACCESS");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.NEW) && !allowedEntitiesFor_insert_FileDirPath.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_INSERTION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE) && !allowedEntitiesFor_delete_FileDirPath.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_DELETION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.UPDATE) && !allowedEntitiesFor_update_FileDirPath.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_UPDATE");
            }
        }
        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SaveResponse synchronizeChangesWithDbJS_FileDirPath(String userName, String jsonData, UserSession usrSession)
    {

        Map<String,Object> keysMap = new HashMap<>(); 
            
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.COMMIT);
        List<ChangeToCommit<ICheckedEntity>> changesToCommit = getChangesToCommit_FileDirPath(userName, jsonData, keysMap, usrSession);
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.AUTO);
           
         
        // Post Data to DB
        synchronizeChangesWithDb_internal(usrSession, changesToCommit, false);
        
        SaveResponse ret = new SaveResponse(); 
        ret.newEntitiesIds = getNewEntitiesIds(keysMap, usrSession.crypto);
       
        return ret;
    }


    private final  Set<String> allowedEntitiesFor_insert_Integrateddecision = new HashSet<String>(Arrays.asList("Integrateddecision"));

    private final  Set<String> allowedEntitiesFor_delete_Integrateddecision = new HashSet<String>(Arrays.asList("Integrateddecision"));

    private final  Set<String> allowedEntitiesFor_update_Integrateddecision = new HashSet<String>(Arrays.asList("Integrateddecision"));

    private final Map<String, Set<String>> allowed_entity_fields_Integrateddecision = new HashMap<String, Set<String>>(){{
        put("Integrateddecision",new HashSet<String>(Arrays.asList("integrateddecisionsId", "decisionCode", "dteUpdate", "usrUpdate", "pclaId", "demaId")));
    }};

    protected List<ChangeToCommit<ICheckedEntity>> getChangesToCommit_Integrateddecision(String userName, String jsonData, Map<String, Object> keysMap, UserSession usrSession) {
        if (!usrSession.privileges.contains("Niva_Integrateddecision_W"))
            throw new DatabaseGenericException("NO_WRITE_ACCESS");
        List<ChangeToCommit<ICheckedEntity>> ret = super.getChangesToCommit__generic(userName, jsonData, keysMap, usrSession.crypto, allowed_entity_fields_Integrateddecision);

        for (ChangeToCommit<ICheckedEntity> cc : ret) {
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE)  && !usrSession.privileges.contains("Niva_Integrateddecision_D")){
                throw new DatabaseGenericException("NO_DELETE_ACCESS");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.NEW) && !allowedEntitiesFor_insert_Integrateddecision.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_INSERTION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE) && !allowedEntitiesFor_delete_Integrateddecision.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_DELETION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.UPDATE) && !allowedEntitiesFor_update_Integrateddecision.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_UPDATE");
            }
        }
        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SaveResponse synchronizeChangesWithDbJS_Integrateddecision(String userName, String jsonData, UserSession usrSession)
    {

        Map<String,Object> keysMap = new HashMap<>(); 
            
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.COMMIT);
        List<ChangeToCommit<ICheckedEntity>> changesToCommit = getChangesToCommit_Integrateddecision(userName, jsonData, keysMap, usrSession);
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.AUTO);
           
         
        // Post Data to DB
        synchronizeChangesWithDb_internal(usrSession, changesToCommit, false);
        
        SaveResponse ret = new SaveResponse(); 
        ret.newEntitiesIds = getNewEntitiesIds(keysMap, usrSession.crypto);
       
        return ret;
    }


    private final  Set<String> allowedEntitiesFor_insert_ParcelGP = new HashSet<String>(Arrays.asList("GpDecision"));

    private final  Set<String> allowedEntitiesFor_delete_ParcelGP = new HashSet<String>();

    private final  Set<String> allowedEntitiesFor_update_ParcelGP = new HashSet<String>(Arrays.asList("ParcelClas", "ParcelsIssues", "GpDecision", "GpRequestsContexts", "GpUpload"));

    private final Map<String, Set<String>> allowed_entity_fields_ParcelGP = new HashMap<String, Set<String>>(){{
        put("ParcelClas",new HashSet<String>(Arrays.asList("pclaId", "probPred", "probPred2", "prodCode", "parcIdentifier", "parcCode", "geom4326", "integrateddecisionCollection", "parcelDecisionCollection", "parcelsIssuesCollection", "clasId", "cultIdDecl", "cultIdPred", "cotyIdDecl", "cotyIdPred", "cultIdPred2")));
        put("ParcelsIssues",new HashSet<String>(Arrays.asList("parcelsIssuesId", "dteCreated", "status", "dteStatusUpdate", "rowVersion", "typeOfIssue", "active", "fmisDecisionCollection", "gpDecisionCollection", "gpRequestsContextsCollection", "fmisUploadCollection", "parcelsIssuesStatusPerDemaCollection", "parcelsIssuesActivitiesCollection", "pclaId")));
        put("GpDecision",new HashSet<String>(Arrays.asList("gpDecisionsId", "cropOk", "landcoverOk", "dteInsert", "usrInsert", "rowVersion", "parcelsIssuesId", "cultId", "cotyId")));
        put("GpRequestsContexts",new HashSet<String>(Arrays.asList("gpRequestsContextsId", "type", "label", "comment", "geomHexewkb", "referencepoint", "rowVersion", "hash", "gpUploadCollection", "gpRequestsProducersId", "parcelsIssuesId")));
        put("GpUpload",new HashSet<String>(Arrays.asList("gpUploadsId", "data", "environment", "dteUpload", "hash", "image", "gpRequestsContextsId")));
    }};

    protected List<ChangeToCommit<ICheckedEntity>> getChangesToCommit_ParcelGP(String userName, String jsonData, Map<String, Object> keysMap, UserSession usrSession) {
        if (!usrSession.privileges.contains("Niva_ParcelGP_W"))
            throw new DatabaseGenericException("NO_WRITE_ACCESS");
        List<ChangeToCommit<ICheckedEntity>> ret = super.getChangesToCommit__generic(userName, jsonData, keysMap, usrSession.crypto, allowed_entity_fields_ParcelGP);

        for (ChangeToCommit<ICheckedEntity> cc : ret) {
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE)  && !usrSession.privileges.contains("Niva_ParcelGP_D")){
                throw new DatabaseGenericException("NO_DELETE_ACCESS");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.NEW) && !allowedEntitiesFor_insert_ParcelGP.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_INSERTION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE) && !allowedEntitiesFor_delete_ParcelGP.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_DELETION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.UPDATE) && !allowedEntitiesFor_update_ParcelGP.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_UPDATE");
            }
        }
        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SaveResponse synchronizeChangesWithDbJS_ParcelGP(String userName, String jsonData, UserSession usrSession)
    {

        Map<String,Object> keysMap = new HashMap<>(); 
            
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.COMMIT);
        List<ChangeToCommit<ICheckedEntity>> changesToCommit = getChangesToCommit_ParcelGP(userName, jsonData, keysMap, usrSession);
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.AUTO);
           
         
        // Post Data to DB
        synchronizeChangesWithDb_internal(usrSession, changesToCommit, false);
        
        SaveResponse ret = new SaveResponse(); 
        ret.newEntitiesIds = getNewEntitiesIds(keysMap, usrSession.crypto);
       
        return ret;
    }


    private final  Set<String> allowedEntitiesFor_insert_AgrisnapUsers = new HashSet<String>(Arrays.asList("AgrisnapUsers", "AgrisnapUsersProducers"));

    private final  Set<String> allowedEntitiesFor_delete_AgrisnapUsers = new HashSet<String>(Arrays.asList("AgrisnapUsers", "AgrisnapUsersProducers"));

    private final  Set<String> allowedEntitiesFor_update_AgrisnapUsers = new HashSet<String>(Arrays.asList("AgrisnapUsers", "AgrisnapUsersProducers"));

    private final Map<String, Set<String>> allowed_entity_fields_AgrisnapUsers = new HashMap<String, Set<String>>(){{
        put("AgrisnapUsers",new HashSet<String>(Arrays.asList("agrisnapName", "rowVersion", "agrisnapUsersId", "agrisnapUid", "agrisnapUsersProducersCollection", "gpRequestsCollection")));
        put("AgrisnapUsersProducers",new HashSet<String>(Arrays.asList("dteRelCreated", "dteRelCanceled", "agrisnapUsersProducersId", "rowVersion", "agrisnapUsersId", "producersId")));
    }};

    protected List<ChangeToCommit<ICheckedEntity>> getChangesToCommit_AgrisnapUsers(String userName, String jsonData, Map<String, Object> keysMap, UserSession usrSession) {
        if (!usrSession.privileges.contains("Niva_AgrisnapUsers_W"))
            throw new DatabaseGenericException("NO_WRITE_ACCESS");
        List<ChangeToCommit<ICheckedEntity>> ret = super.getChangesToCommit__generic(userName, jsonData, keysMap, usrSession.crypto, allowed_entity_fields_AgrisnapUsers);

        for (ChangeToCommit<ICheckedEntity> cc : ret) {
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE)  && !usrSession.privileges.contains("Niva_AgrisnapUsers_D")){
                throw new DatabaseGenericException("NO_DELETE_ACCESS");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.NEW) && !allowedEntitiesFor_insert_AgrisnapUsers.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_INSERTION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE) && !allowedEntitiesFor_delete_AgrisnapUsers.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_DELETION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.UPDATE) && !allowedEntitiesFor_update_AgrisnapUsers.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_UPDATE");
            }
        }
        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SaveResponse synchronizeChangesWithDbJS_AgrisnapUsers(String userName, String jsonData, UserSession usrSession)
    {

        Map<String,Object> keysMap = new HashMap<>(); 
            
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.COMMIT);
        List<ChangeToCommit<ICheckedEntity>> changesToCommit = getChangesToCommit_AgrisnapUsers(userName, jsonData, keysMap, usrSession);
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.AUTO);
           
         
        // Post Data to DB
        synchronizeChangesWithDb_internal(usrSession, changesToCommit, false);
        
        SaveResponse ret = new SaveResponse(); 
        ret.newEntitiesIds = getNewEntitiesIds(keysMap, usrSession.crypto);
       
        return ret;
    }


    private final  Set<String> allowedEntitiesFor_insert_ParcelsIssues = new HashSet<String>(Arrays.asList("ParcelsIssues", "GpDecision", "GpRequestsContexts", "GpUpload", "ParcelsIssuesActivities"));

    private final  Set<String> allowedEntitiesFor_delete_ParcelsIssues = new HashSet<String>(Arrays.asList("ParcelsIssues", "GpDecision", "GpRequestsContexts", "GpUpload", "ParcelsIssuesActivities"));

    private final  Set<String> allowedEntitiesFor_update_ParcelsIssues = new HashSet<String>(Arrays.asList("ParcelsIssues", "GpDecision", "GpRequestsContexts", "GpUpload", "ParcelsIssuesActivities"));

    private final Map<String, Set<String>> allowed_entity_fields_ParcelsIssues = new HashMap<String, Set<String>>(){{
        put("ParcelsIssues",new HashSet<String>(Arrays.asList("parcelsIssuesId", "dteCreated", "status", "dteStatusUpdate", "rowVersion", "typeOfIssue", "active", "fmisDecisionCollection", "gpDecisionCollection", "gpRequestsContextsCollection", "fmisUploadCollection", "parcelsIssuesStatusPerDemaCollection", "parcelsIssuesActivitiesCollection", "pclaId")));
        put("GpDecision",new HashSet<String>(Arrays.asList("gpDecisionsId", "cropOk", "landcoverOk", "dteInsert", "usrInsert", "rowVersion", "parcelsIssuesId", "cultId", "cotyId")));
        put("GpRequestsContexts",new HashSet<String>(Arrays.asList("gpRequestsContextsId", "type", "label", "comment", "geomHexewkb", "referencepoint", "rowVersion", "hash", "gpUploadCollection", "gpRequestsProducersId", "parcelsIssuesId")));
        put("GpUpload",new HashSet<String>(Arrays.asList("gpUploadsId", "data", "environment", "dteUpload", "hash", "image", "gpRequestsContextsId")));
        put("ParcelsIssuesActivities",new HashSet<String>(Arrays.asList("parcelsIssuesActivitiesId", "rowVersion", "type", "dteTimestamp", "typeExtrainfo", "parcelsIssuesId")));
    }};

    protected List<ChangeToCommit<ICheckedEntity>> getChangesToCommit_ParcelsIssues(String userName, String jsonData, Map<String, Object> keysMap, UserSession usrSession) {
        if (!usrSession.privileges.contains("Niva_ParcelsIssues_W"))
            throw new DatabaseGenericException("NO_WRITE_ACCESS");
        List<ChangeToCommit<ICheckedEntity>> ret = super.getChangesToCommit__generic(userName, jsonData, keysMap, usrSession.crypto, allowed_entity_fields_ParcelsIssues);

        for (ChangeToCommit<ICheckedEntity> cc : ret) {
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE)  && !usrSession.privileges.contains("Niva_ParcelsIssues_D")){
                throw new DatabaseGenericException("NO_DELETE_ACCESS");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.NEW) && !allowedEntitiesFor_insert_ParcelsIssues.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_INSERTION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE) && !allowedEntitiesFor_delete_ParcelsIssues.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_DELETION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.UPDATE) && !allowedEntitiesFor_update_ParcelsIssues.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_UPDATE");
            }
        }
        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SaveResponse synchronizeChangesWithDbJS_ParcelsIssues(String userName, String jsonData, UserSession usrSession)
    {

        Map<String,Object> keysMap = new HashMap<>(); 
            
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.COMMIT);
        List<ChangeToCommit<ICheckedEntity>> changesToCommit = getChangesToCommit_ParcelsIssues(userName, jsonData, keysMap, usrSession);
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.AUTO);
           
         
        // Post Data to DB
        synchronizeChangesWithDb_internal(usrSession, changesToCommit, false);
        
        SaveResponse ret = new SaveResponse(); 
        ret.newEntitiesIds = getNewEntitiesIds(keysMap, usrSession.crypto);
       
        return ret;
    }


    private final  Set<String> allowedEntitiesFor_insert_Producers = new HashSet<String>(Arrays.asList("Producers"));

    private final  Set<String> allowedEntitiesFor_delete_Producers = new HashSet<String>(Arrays.asList("Producers"));

    private final  Set<String> allowedEntitiesFor_update_Producers = new HashSet<String>(Arrays.asList("Producers"));

    private final Map<String, Set<String>> allowed_entity_fields_Producers = new HashMap<String, Set<String>>(){{
        put("Producers",new HashSet<String>(Arrays.asList("prodName", "rowVersion", "prodCode", "producersId", "gpRequestsProducersCollection", "agrisnapUsersProducersCollection")));
    }};

    protected List<ChangeToCommit<ICheckedEntity>> getChangesToCommit_Producers(String userName, String jsonData, Map<String, Object> keysMap, UserSession usrSession) {
        if (!usrSession.privileges.contains("Niva_Producers_W"))
            throw new DatabaseGenericException("NO_WRITE_ACCESS");
        List<ChangeToCommit<ICheckedEntity>> ret = super.getChangesToCommit__generic(userName, jsonData, keysMap, usrSession.crypto, allowed_entity_fields_Producers);

        for (ChangeToCommit<ICheckedEntity> cc : ret) {
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE)  && !usrSession.privileges.contains("Niva_Producers_D")){
                throw new DatabaseGenericException("NO_DELETE_ACCESS");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.NEW) && !allowedEntitiesFor_insert_Producers.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_INSERTION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE) && !allowedEntitiesFor_delete_Producers.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_DELETION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.UPDATE) && !allowedEntitiesFor_update_Producers.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_UPDATE");
            }
        }
        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SaveResponse synchronizeChangesWithDbJS_Producers(String userName, String jsonData, UserSession usrSession)
    {

        Map<String,Object> keysMap = new HashMap<>(); 
            
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.COMMIT);
        List<ChangeToCommit<ICheckedEntity>> changesToCommit = getChangesToCommit_Producers(userName, jsonData, keysMap, usrSession);
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.AUTO);
           
         
        // Post Data to DB
        synchronizeChangesWithDb_internal(usrSession, changesToCommit, false);
        
        SaveResponse ret = new SaveResponse(); 
        ret.newEntitiesIds = getNewEntitiesIds(keysMap, usrSession.crypto);
       
        return ret;
    }


    private final  Set<String> allowedEntitiesFor_insert_Dashboard = new HashSet<String>();

    private final  Set<String> allowedEntitiesFor_delete_Dashboard = new HashSet<String>();

    private final  Set<String> allowedEntitiesFor_update_Dashboard = new HashSet<String>(Arrays.asList("ParcelClas", "ParcelDecision", "Integrateddecision"));

    private final Map<String, Set<String>> allowed_entity_fields_Dashboard = new HashMap<String, Set<String>>(){{
        put("ParcelClas",new HashSet<String>(Arrays.asList("pclaId", "probPred", "probPred2", "prodCode", "parcIdentifier", "parcCode", "geom4326", "integrateddecisionCollection", "parcelDecisionCollection", "parcelsIssuesCollection", "clasId", "cultIdDecl", "cultIdPred", "cotyIdDecl", "cotyIdPred", "cultIdPred2")));
        put("ParcelDecision",new HashSet<String>(Arrays.asList("padeId", "decisionLight", "rowVersion", "demaId", "pclaId")));
        put("ParcelsIssues",new HashSet<String>(Arrays.asList("parcelsIssuesId", "dteCreated", "status", "dteStatusUpdate", "rowVersion", "typeOfIssue", "active", "fmisDecisionCollection", "gpDecisionCollection", "gpRequestsContextsCollection", "fmisUploadCollection", "parcelsIssuesStatusPerDemaCollection", "parcelsIssuesActivitiesCollection", "pclaId")));
        put("GpDecision",new HashSet<String>(Arrays.asList("gpDecisionsId", "cropOk", "landcoverOk", "dteInsert", "usrInsert", "rowVersion", "parcelsIssuesId", "cultId", "cotyId")));
        put("FmisDecision",new HashSet<String>(Arrays.asList("fmisDecisionsId", "cropOk", "landcoverOk", "dteInsert", "usrInsert", "rowVersion", "parcelsIssuesId", "cultId", "cotyId")));
        put("Integrateddecision",new HashSet<String>(Arrays.asList("integrateddecisionsId", "decisionCode", "dteUpdate", "usrUpdate", "pclaId", "demaId")));
    }};

    protected List<ChangeToCommit<ICheckedEntity>> getChangesToCommit_Dashboard(String userName, String jsonData, Map<String, Object> keysMap, UserSession usrSession) {
        if (!usrSession.privileges.contains("Niva_Dashboard_W"))
            throw new DatabaseGenericException("NO_WRITE_ACCESS");
        List<ChangeToCommit<ICheckedEntity>> ret = super.getChangesToCommit__generic(userName, jsonData, keysMap, usrSession.crypto, allowed_entity_fields_Dashboard);

        for (ChangeToCommit<ICheckedEntity> cc : ret) {
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE)  && !usrSession.privileges.contains("Niva_Dashboard_D")){
                throw new DatabaseGenericException("NO_DELETE_ACCESS");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.NEW) && !allowedEntitiesFor_insert_Dashboard.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_INSERTION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE) && !allowedEntitiesFor_delete_Dashboard.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_DELETION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.UPDATE) && !allowedEntitiesFor_update_Dashboard.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_UPDATE");
            }
        }
        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SaveResponse synchronizeChangesWithDbJS_Dashboard(String userName, String jsonData, UserSession usrSession)
    {

        Map<String,Object> keysMap = new HashMap<>(); 
            
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.COMMIT);
        List<ChangeToCommit<ICheckedEntity>> changesToCommit = getChangesToCommit_Dashboard(userName, jsonData, keysMap, usrSession);
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.AUTO);
           
         
        // Post Data to DB
        synchronizeChangesWithDb_internal(usrSession, changesToCommit, false);
        
        SaveResponse ret = new SaveResponse(); 
        ret.newEntitiesIds = getNewEntitiesIds(keysMap, usrSession.crypto);
       
        return ret;
    }


    private final  Set<String> allowedEntitiesFor_insert_ParcelFMIS = new HashSet<String>(Arrays.asList("FmisDecision"));

    private final  Set<String> allowedEntitiesFor_delete_ParcelFMIS = new HashSet<String>();

    private final  Set<String> allowedEntitiesFor_update_ParcelFMIS = new HashSet<String>(Arrays.asList("ParcelClas", "ParcelsIssues", "FmisDecision", "FmisUpload"));

    private final Map<String, Set<String>> allowed_entity_fields_ParcelFMIS = new HashMap<String, Set<String>>(){{
        put("ParcelClas",new HashSet<String>(Arrays.asList("pclaId", "probPred", "probPred2", "prodCode", "parcIdentifier", "parcCode", "geom4326", "integrateddecisionCollection", "parcelDecisionCollection", "parcelsIssuesCollection", "clasId", "cultIdDecl", "cultIdPred", "cotyIdDecl", "cotyIdPred", "cultIdPred2")));
        put("ParcelsIssues",new HashSet<String>(Arrays.asList("parcelsIssuesId", "dteCreated", "status", "dteStatusUpdate", "rowVersion", "typeOfIssue", "active", "fmisDecisionCollection", "gpDecisionCollection", "gpRequestsContextsCollection", "fmisUploadCollection", "parcelsIssuesStatusPerDemaCollection", "parcelsIssuesActivitiesCollection", "pclaId")));
        put("FmisDecision",new HashSet<String>(Arrays.asList("fmisDecisionsId", "cropOk", "landcoverOk", "dteInsert", "usrInsert", "rowVersion", "parcelsIssuesId", "cultId", "cotyId")));
        put("FmisUpload",new HashSet<String>(Arrays.asList("fmisUploadsId", "metadata", "dteUpload", "docfile", "usrUpload", "parcelsIssuesId")));
    }};

    protected List<ChangeToCommit<ICheckedEntity>> getChangesToCommit_ParcelFMIS(String userName, String jsonData, Map<String, Object> keysMap, UserSession usrSession) {
        if (!usrSession.privileges.contains("Niva_ParcelFMIS_W"))
            throw new DatabaseGenericException("NO_WRITE_ACCESS");
        List<ChangeToCommit<ICheckedEntity>> ret = super.getChangesToCommit__generic(userName, jsonData, keysMap, usrSession.crypto, allowed_entity_fields_ParcelFMIS);

        for (ChangeToCommit<ICheckedEntity> cc : ret) {
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE)  && !usrSession.privileges.contains("Niva_ParcelFMIS_D")){
                throw new DatabaseGenericException("NO_DELETE_ACCESS");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.NEW) && !allowedEntitiesFor_insert_ParcelFMIS.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_INSERTION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE) && !allowedEntitiesFor_delete_ParcelFMIS.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_DELETION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.UPDATE) && !allowedEntitiesFor_update_ParcelFMIS.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_UPDATE");
            }
        }
        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SaveResponse synchronizeChangesWithDbJS_ParcelFMIS(String userName, String jsonData, UserSession usrSession)
    {

        Map<String,Object> keysMap = new HashMap<>(); 
            
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.COMMIT);
        List<ChangeToCommit<ICheckedEntity>> changesToCommit = getChangesToCommit_ParcelFMIS(userName, jsonData, keysMap, usrSession);
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.AUTO);
           
         
        // Post Data to DB
        synchronizeChangesWithDb_internal(usrSession, changesToCommit, false);
        
        SaveResponse ret = new SaveResponse(); 
        ret.newEntitiesIds = getNewEntitiesIds(keysMap, usrSession.crypto);
       
        return ret;
    }


    private final  Set<String> allowedEntitiesFor_insert_FmisUser = new HashSet<String>(Arrays.asList("FmisUser"));

    private final  Set<String> allowedEntitiesFor_delete_FmisUser = new HashSet<String>(Arrays.asList("FmisUser"));

    private final  Set<String> allowedEntitiesFor_update_FmisUser = new HashSet<String>(Arrays.asList("FmisUser"));

    private final Map<String, Set<String>> allowed_entity_fields_FmisUser = new HashMap<String, Set<String>>(){{
        put("FmisUser",new HashSet<String>(Arrays.asList("fmisName", "rowVersion", "fmisUsersId", "fmisUid")));
    }};

    protected List<ChangeToCommit<ICheckedEntity>> getChangesToCommit_FmisUser(String userName, String jsonData, Map<String, Object> keysMap, UserSession usrSession) {
        if (!usrSession.privileges.contains("Niva_FmisUser_W"))
            throw new DatabaseGenericException("NO_WRITE_ACCESS");
        List<ChangeToCommit<ICheckedEntity>> ret = super.getChangesToCommit__generic(userName, jsonData, keysMap, usrSession.crypto, allowed_entity_fields_FmisUser);

        for (ChangeToCommit<ICheckedEntity> cc : ret) {
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE)  && !usrSession.privileges.contains("Niva_FmisUser_D")){
                throw new DatabaseGenericException("NO_DELETE_ACCESS");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.NEW) && !allowedEntitiesFor_insert_FmisUser.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_INSERTION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.DELETE) && !allowedEntitiesFor_delete_FmisUser.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_DELETION");
            }
            if ((cc.getStatus() == ChangeToCommit.ChangeStatus.UPDATE) && !allowedEntitiesFor_update_FmisUser.contains(cc.getEntity().getEntityName())) {
                throw new DatabaseGenericException("INVALID_ENTITY_UPDATE");
            }
        }
        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SaveResponse synchronizeChangesWithDbJS_FmisUser(String userName, String jsonData, UserSession usrSession)
    {

        Map<String,Object> keysMap = new HashMap<>(); 
            
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.COMMIT);
        List<ChangeToCommit<ICheckedEntity>> changesToCommit = getChangesToCommit_FmisUser(userName, jsonData, keysMap, usrSession);
        getEntityManager().setFlushMode(javax.persistence.FlushModeType.AUTO);
           
         
        // Post Data to DB
        synchronizeChangesWithDb_internal(usrSession, changesToCommit, false);
        
        SaveResponse ret = new SaveResponse(); 
        ret.newEntitiesIds = getNewEntitiesIds(keysMap, usrSession.crypto);
       
        return ret;
    }


    private class ImportExcel_CoverType_impCotys_id_ExcelRow {
        int _rowNum;
        public Integer code;
        public String name;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public ExcelFile importExcel_CoverType_impCotys_id(byte[] excelData, String excelFilename, UserSession usrSession) 
    {
        ExcelFile excelFile = saveExcelFileData(excelData, excelFilename, usrSession);

        // read and validate excel data
        ArrayList<ImportExcel_CoverType_impCotys_id_ExcelRow> excelRows = new ArrayList<>();
        try {
            excelRows = importExcel_CoverType_impCotys_id_ReadExcelData(excelFile, true, usrSession);
                
        } catch (Exception ex) {
            throw new gr.neuropublic.exceptions.GenericApplicationException(ex.getMessage());
        }
            
        if (excelFile.getTotalRows() == null || excelFile.getTotalErrorRows() == null)
            throw new GenericApplicationException("ImportExcel_MessageBox_GenericError_Message");
            
        if (excelFile.getTotalRows() == 0) 
            throw new GenericApplicationException("ImportExcel_MessageBox_NoRecordsError_Message");
            
        if (excelFile.getTotalRows() > 0 && excelFile.getTotalErrorRows() == 0) {
            // call PerRowAction
            for (ImportExcel_CoverType_impCotys_id_ExcelRow row : excelRows) {
                executeDbProcedure("{ call niva.import_land_covers(?,?,?) }", 
                        row.code,
                        row.name,
                        excelFile.getId());            
            }
            
        }

        return excelFile;        
    }

    private ArrayList<ImportExcel_CoverType_impCotys_id_ExcelRow> importExcel_CoverType_impCotys_id_ReadExcelData(ExcelFile excelFile, boolean skipFirstRow, UserSession usrSession) throws IOException, InvalidFormatException
    {
        ArrayList<ImportExcel_CoverType_impCotys_id_ExcelRow> ret = new ArrayList<>();
        byte[] excelData = excelFile.getData();
        Date currentDate = gr.neuropublic.utils.DateUtil.today();

        // read Excel Data
        InputStream s = new ByteArrayInputStream(excelData);
        Workbook workbook =  WorkbookFactory.create(s);
            
        Sheet sheet0 = workbook.getSheetAt(0);
            
        int startIndex = skipFirstRow ? 1 : 0;
        int endIndex = sheet0.getLastRowNum();
            
        int errorRows = 0;
        boolean rowHasError = false;
        Pair<Boolean,Integer> cell_code = null;
        Pair<Boolean,String> cell_name = null;

        for(int i=startIndex; i<=endIndex; i++) {
            Row row = sheet0.getRow(i);
            rowHasError = false;
            cell_code = null;
            cell_name = null;
                
            if (row == null) {
                rowHasError = true;
                saveExcelError(i+1, excelFile, usrSession, "Η γραμμή " + (i+1) + " είναι κενή.");
            } else {
                for(Cell cell : row) {
                    if (cell.getColumnIndex() == 0) {
                        cell_code = ExcelFileUtils.validateAndGetCellValue_Integer(cell, null, null, null);
                        if (!cell_code.a) {
                            rowHasError = true;
                            saveExcelError(i+1, excelFile, usrSession, 
                                    "Μη αποδεκτή τιμή για την κολόνα 'Code'. Η τιμή πρέπει να είναι αριθμός τύπου Integer.");
                        }
                    }
                    if (cell.getColumnIndex() == 1) {
                        cell_name = ExcelFileUtils.validateAndGetCellValue_String(cell, 60, null);
                        if (!cell_name.a) {
                            rowHasError = true;
                            saveExcelError(i+1, excelFile, usrSession, 
                                    "Μη αποδεκτή τιμή για την κολόνα 'Name'. Η τιμή πρέπει να είναι τύπου 'Κείμενο', μέγιστου μήκους 60.");
                        }
                    }
                }

                // Check mandatory fields
                if (cell_code == null || (cell_code.a && cell_code.b == null) ) {
                    rowHasError = true;
                    saveExcelError(i+1, excelFile, usrSession, "Δεν επιτρέπονται κενές τιμές για την κολόνα 'Code'.");
                }
                if (cell_name == null || (cell_name.a && cell_name.b == null) ) {
                    rowHasError = true;
                    saveExcelError(i+1, excelFile, usrSession, "Δεν επιτρέπονται κενές τιμές για την κολόνα 'Name'.");
                }
            }
                
            if (rowHasError) {
                errorRows++;
            } else {
                ImportExcel_CoverType_impCotys_id_ExcelRow retRow = new ImportExcel_CoverType_impCotys_id_ExcelRow();
                retRow._rowNum = i+1;
                retRow.code = cell_code != null ? cell_code.b : null;
                retRow.name = cell_name != null ? cell_name.b : null;
                ret.add(retRow);
            }
        }

        saveExcelFileResults(excelFile, endIndex - startIndex + 1, errorRows);

        return ret;
    }



    private class ImportExcel_Cultivation_impCults_id_ExcelRow {
        int _rowNum;
        public String name;
        public Integer code;
        public String land_cover_name;
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public ExcelFile importExcel_Cultivation_impCults_id(byte[] excelData, String excelFilename, UserSession usrSession) 
    {
        ExcelFile excelFile = saveExcelFileData(excelData, excelFilename, usrSession);

        // read and validate excel data
        ArrayList<ImportExcel_Cultivation_impCults_id_ExcelRow> excelRows = new ArrayList<>();
        try {
            excelRows = importExcel_Cultivation_impCults_id_ReadExcelData(excelFile, true, usrSession);
                
        } catch (Exception ex) {
            throw new gr.neuropublic.exceptions.GenericApplicationException(ex.getMessage());
        }
            
        if (excelFile.getTotalRows() == null || excelFile.getTotalErrorRows() == null)
            throw new GenericApplicationException("ImportExcel_MessageBox_GenericError_Message");
            
        if (excelFile.getTotalRows() == 0) 
            throw new GenericApplicationException("ImportExcel_MessageBox_NoRecordsError_Message");
            
        if (excelFile.getTotalRows() > 0 && excelFile.getTotalErrorRows() == 0) {
            // call PerRowAction
            for (ImportExcel_Cultivation_impCults_id_ExcelRow row : excelRows) {
                executeDbProcedure("{ call niva.import_crops(?,?,?,?) }", 
                        row.name,
                        row.code,
                        row.land_cover_name,
                        excelFile.getId());            
            }
            
        }

        return excelFile;        
    }

    private ArrayList<ImportExcel_Cultivation_impCults_id_ExcelRow> importExcel_Cultivation_impCults_id_ReadExcelData(ExcelFile excelFile, boolean skipFirstRow, UserSession usrSession) throws IOException, InvalidFormatException
    {
        ArrayList<ImportExcel_Cultivation_impCults_id_ExcelRow> ret = new ArrayList<>();
        byte[] excelData = excelFile.getData();
        Date currentDate = gr.neuropublic.utils.DateUtil.today();

        // read Excel Data
        InputStream s = new ByteArrayInputStream(excelData);
        Workbook workbook =  WorkbookFactory.create(s);
            
        Sheet sheet0 = workbook.getSheetAt(0);
            
        int startIndex = skipFirstRow ? 1 : 0;
        int endIndex = sheet0.getLastRowNum();
            
        int errorRows = 0;
        boolean rowHasError = false;
        Pair<Boolean,String> cell_name = null;
        Pair<Boolean,Integer> cell_code = null;
        Pair<Boolean,String> cell_land_cover_name = null;

        for(int i=startIndex; i<=endIndex; i++) {
            Row row = sheet0.getRow(i);
            rowHasError = false;
            cell_name = null;
            cell_code = null;
            cell_land_cover_name = null;
                
            if (row == null) {
                rowHasError = true;
                saveExcelError(i+1, excelFile, usrSession, "Η γραμμή " + (i+1) + " είναι κενή.");
            } else {
                for(Cell cell : row) {
                    if (cell.getColumnIndex() == 0) {
                        cell_name = ExcelFileUtils.validateAndGetCellValue_String(cell, 200, null);
                        if (!cell_name.a) {
                            rowHasError = true;
                            saveExcelError(i+1, excelFile, usrSession, 
                                    "Μη αποδεκτή τιμή για την κολόνα 'Crop Name'. Η τιμή πρέπει να είναι τύπου 'Κείμενο', μέγιστου μήκους 200.");
                        }
                    }
                    if (cell.getColumnIndex() == 1) {
                        cell_code = ExcelFileUtils.validateAndGetCellValue_Integer(cell, null, null, null);
                        if (!cell_code.a) {
                            rowHasError = true;
                            saveExcelError(i+1, excelFile, usrSession, 
                                    "Μη αποδεκτή τιμή για την κολόνα 'Crop Code'. Η τιμή πρέπει να είναι αριθμός τύπου Integer.");
                        }
                    }
                    if (cell.getColumnIndex() == 2) {
                        cell_land_cover_name = ExcelFileUtils.validateAndGetCellValue_String(cell, 60, null);
                        if (!cell_land_cover_name.a) {
                            rowHasError = true;
                            saveExcelError(i+1, excelFile, usrSession, 
                                    "Μη αποδεκτή τιμή για την κολόνα 'Land Cover Name'. Η τιμή πρέπει να είναι τύπου 'Κείμενο', μέγιστου μήκους 60.");
                        }
                    }
                }

                // Check mandatory fields
                if (cell_name == null || (cell_name.a && cell_name.b == null) ) {
                    rowHasError = true;
                    saveExcelError(i+1, excelFile, usrSession, "Δεν επιτρέπονται κενές τιμές για την κολόνα 'Crop Name'.");
                }
                if (cell_code == null || (cell_code.a && cell_code.b == null) ) {
                    rowHasError = true;
                    saveExcelError(i+1, excelFile, usrSession, "Δεν επιτρέπονται κενές τιμές για την κολόνα 'Crop Code'.");
                }
            }
                
            if (rowHasError) {
                errorRows++;
            } else {
                ImportExcel_Cultivation_impCults_id_ExcelRow retRow = new ImportExcel_Cultivation_impCults_id_ExcelRow();
                retRow._rowNum = i+1;
                retRow.name = cell_name != null ? cell_name.b : null;
                retRow.code = cell_code != null ? cell_code.b : null;
                retRow.land_cover_name = cell_land_cover_name != null ? cell_land_cover_name.b : null;
                ret.add(retRow);
            }
        }

        saveExcelFileResults(excelFile, endIndex - startIndex + 1, errorRows);

        return ret;
    }


    protected ExcelFile saveExcelFileData(byte[] excelData, String excelFilename, UserSession usrSession) {
        ExcelFile excelFile = getExcelFileFacade().initRow();
        excelFile.setData(excelData);
        excelFile.setFilename(excelFilename);
        
        saveEntityToDB(usrSession, excelFile);

        return excelFile;
    }

    protected void saveExcelFileResults(final ExcelFile excelFile, int totalRows, int errorRows) {
        excelFile.setTotalRows(totalRows);
        excelFile.setTotalErrorRows(errorRows);
        
        saveToDBGeneric(new Action() { public void lambda() {
            getEntityManager().persist(excelFile);
            getEntityManager().flush();
        }});
    }

    protected void saveExcelError(Integer excelRowNum, ExcelFile excelFile, UserSession usrSession, String errMsg) {
        ExcelError excelError = getExcelErrorFacade().initRow();
        excelError.setExfiId(excelFile);
        excelError.setExcelRowNum(excelRowNum);
        excelError.setErrMessage(errMsg);
        
        saveEntityToDB(usrSession, excelError);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
