package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class ParcelClasBaseFacade extends AbstractFacade<ParcelClas> implements  IParcelClasBaseFacade, /*.ILocal, IParcelClasBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(ParcelClasBaseFacade.class);

    public ParcelClasBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public ParcelClas initRow() {
        return new ParcelClas(getNextSequenceValue());
    }

    public void getTransientFields(List<ParcelClas> entities){
    }

    @Override
    public ParcelClas createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.IClassificationFacade.ILocal classificationFacade, gr.neuropublic.Niva.facades.ICultivationFacade.ILocal cultivationFacade, gr.neuropublic.Niva.facades.ICoverTypeFacade.ILocal coverTypeFacade, Set<String> allowedFields) {
        ParcelClas ret = null;
        if (!json.containsKey("pclaId") || json.get("pclaId") == null)
            throw new GenericApplicationException("Error in ParcelClasBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("pclaId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("pclaId"));
                //ret = this.findByPclaId(id);
                ret = getEntityManager().find(ParcelClas.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:ParcelClas:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("probPred") && allowedFields.contains("probPred")) {
            Object  obj = json.get("probPred");
            Double probPred = null;
            if (obj != null) {
                if (obj instanceof Double) {
                    probPred = (Double)obj;
                } else if (obj instanceof Long) {
                    probPred = ((Long)obj).doubleValue();
                } else {
                    throw new GenericApplicationException("Error in  CreateFromJson. probPred with value '"+obj.toString()+"' cannot be converted to Double");
                }
            }
            ret.setProbPred(probPred);
        }
        if (json.containsKey("probPred2") && allowedFields.contains("probPred2")) {
            Object  obj = json.get("probPred2");
            Double probPred2 = null;
            if (obj != null) {
                if (obj instanceof Double) {
                    probPred2 = (Double)obj;
                } else if (obj instanceof Long) {
                    probPred2 = ((Long)obj).doubleValue();
                } else {
                    throw new GenericApplicationException("Error in  CreateFromJson. probPred2 with value '"+obj.toString()+"' cannot be converted to Double");
                }
            }
            ret.setProbPred2(probPred2);
        }
        if (json.containsKey("prodCode") && allowedFields.contains("prodCode")) {
            Long prodCode = (Long)json.get("prodCode");
            ret.setProdCode(prodCode != null ? prodCode.intValue() : null);
        }
        if (json.containsKey("parcIdentifier") && allowedFields.contains("parcIdentifier")) {
            ret.setParcIdentifier((String)json.get("parcIdentifier"));
        }
        if (json.containsKey("parcCode") && allowedFields.contains("parcCode")) {
            ret.setParcCode((String)json.get("parcCode"));
        }
        if (json.containsKey("geom4326") && allowedFields.contains("geom4326")) {
            JSONObject obj = (JSONObject) json.get("geom4326");
            if (obj == null) {
                ret.setGeom4326(null);
            } else {
                GeometryJSON gjson = new GeometryJSON(6);
                try {
                    Geometry geo = gjson.read(obj.toJSONString());
                    geo.setSRID(2100);
                    ret.setGeom4326(geo);
                } catch (IOException ex) {
                    logger.error("Error in CreateFromJson",ex);
                    throw new GenericApplicationException(ex.getMessage());
                }
            }
        }
        if (json.containsKey("clasId") && allowedFields.contains("clasId")) {
            JSONObject clasId = (JSONObject)json.get("clasId");
            if (clasId == null) {
                ret.setClasId(null);
            } else {
                if (!clasId.containsKey("clasId") || clasId.get("clasId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. clasId is not null but PK is missing or null");
                } 
                Classification clasId_db = null;
                String temp_id = (String)clasId.get("clasId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. clasId.clasId is negative but not in the keysMap dictionary");
                    }
                    clasId_db = (Classification)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)clasId.get("clasId"));
                    clasId_db = classificationFacade.findByClasId(id);
                    if (clasId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity Classification with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setClasId(clasId_db);
            }
        }
        if (json.containsKey("cultIdDecl") && allowedFields.contains("cultIdDecl")) {
            JSONObject cultIdDecl = (JSONObject)json.get("cultIdDecl");
            if (cultIdDecl == null) {
                ret.setCultIdDecl(null);
            } else {
                if (!cultIdDecl.containsKey("cultId") || cultIdDecl.get("cultId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. cultIdDecl is not null but PK is missing or null");
                } 
                Cultivation cultIdDecl_db = null;
                String temp_id = (String)cultIdDecl.get("cultId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. cultIdDecl.cultId is negative but not in the keysMap dictionary");
                    }
                    cultIdDecl_db = (Cultivation)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)cultIdDecl.get("cultId"));
                    cultIdDecl_db = cultivationFacade.findByCultId(id);
                    if (cultIdDecl_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity Cultivation with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setCultIdDecl(cultIdDecl_db);
            }
        }
        if (json.containsKey("cultIdPred") && allowedFields.contains("cultIdPred")) {
            JSONObject cultIdPred = (JSONObject)json.get("cultIdPred");
            if (cultIdPred == null) {
                ret.setCultIdPred(null);
            } else {
                if (!cultIdPred.containsKey("cultId") || cultIdPred.get("cultId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. cultIdPred is not null but PK is missing or null");
                } 
                Cultivation cultIdPred_db = null;
                String temp_id = (String)cultIdPred.get("cultId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. cultIdPred.cultId is negative but not in the keysMap dictionary");
                    }
                    cultIdPred_db = (Cultivation)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)cultIdPred.get("cultId"));
                    cultIdPred_db = cultivationFacade.findByCultId(id);
                    if (cultIdPred_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity Cultivation with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setCultIdPred(cultIdPred_db);
            }
        }
        if (json.containsKey("cotyIdDecl") && allowedFields.contains("cotyIdDecl")) {
            JSONObject cotyIdDecl = (JSONObject)json.get("cotyIdDecl");
            if (cotyIdDecl == null) {
                ret.setCotyIdDecl(null);
            } else {
                if (!cotyIdDecl.containsKey("cotyId") || cotyIdDecl.get("cotyId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. cotyIdDecl is not null but PK is missing or null");
                } 
                CoverType cotyIdDecl_db = null;
                String temp_id = (String)cotyIdDecl.get("cotyId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. cotyIdDecl.cotyId is negative but not in the keysMap dictionary");
                    }
                    cotyIdDecl_db = (CoverType)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)cotyIdDecl.get("cotyId"));
                    cotyIdDecl_db = coverTypeFacade.findByCotyId(id);
                    if (cotyIdDecl_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity CoverType with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setCotyIdDecl(cotyIdDecl_db);
            }
        }
        if (json.containsKey("cotyIdPred") && allowedFields.contains("cotyIdPred")) {
            JSONObject cotyIdPred = (JSONObject)json.get("cotyIdPred");
            if (cotyIdPred == null) {
                ret.setCotyIdPred(null);
            } else {
                if (!cotyIdPred.containsKey("cotyId") || cotyIdPred.get("cotyId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. cotyIdPred is not null but PK is missing or null");
                } 
                CoverType cotyIdPred_db = null;
                String temp_id = (String)cotyIdPred.get("cotyId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. cotyIdPred.cotyId is negative but not in the keysMap dictionary");
                    }
                    cotyIdPred_db = (CoverType)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)cotyIdPred.get("cotyId"));
                    cotyIdPred_db = coverTypeFacade.findByCotyId(id);
                    if (cotyIdPred_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity CoverType with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setCotyIdPred(cotyIdPred_db);
            }
        }
        if (json.containsKey("cultIdPred2") && allowedFields.contains("cultIdPred2")) {
            JSONObject cultIdPred2 = (JSONObject)json.get("cultIdPred2");
            if (cultIdPred2 == null) {
                ret.setCultIdPred2(null);
            } else {
                if (!cultIdPred2.containsKey("cultId") || cultIdPred2.get("cultId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. cultIdPred2 is not null but PK is missing or null");
                } 
                Cultivation cultIdPred2_db = null;
                String temp_id = (String)cultIdPred2.get("cultId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. cultIdPred2.cultId is negative but not in the keysMap dictionary");
                    }
                    cultIdPred2_db = (Cultivation)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)cultIdPred2.get("cultId"));
                    cultIdPred2_db = cultivationFacade.findByCultId(id);
                    if (cultIdPred2_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity Cultivation with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setCultIdPred2(cultIdPred2_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelClas> findAllByIds(List<Integer> ids) {

        List<ParcelClas> ret = (List<ParcelClas>) em.createQuery("SELECT x FROM ParcelClas x left join fetch x.clasId x_clasId left join fetch x.cotyIdDecl x_cotyIdDecl left join fetch x.cultIdDecl x_cultIdDecl left join fetch x.cultIdPred x_cultIdPred left join fetch x.cultIdPred2 x_cultIdPred2  WHERE x.pclaId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM ParcelClas x WHERE x.pclaId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public ParcelClas findByPclaId(Integer pclaId) {
        List<ParcelClas> results = (List<ParcelClas>) em.createQuery("SELECT x FROM ParcelClas x left join fetch x.clasId x_clasId left join fetch x.cotyIdDecl x_cotyIdDecl left join fetch x.cultIdDecl x_cultIdDecl left join fetch x.cultIdPred x_cultIdPred left join fetch x.cultIdPred2 x_cultIdPred2  WHERE x.pclaId = :pclaId").
            setParameter("pclaId", pclaId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByPclaId(Integer pclaId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM ParcelClas x WHERE x.pclaId = :pclaId")
    .setParameter("pclaId", pclaId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelClas> findLazyParcelGP(Integer prodCode, String parcIdentifier, String parcCode, Integer clasId_clasId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findLazyParcelGP(false, prodCode, parcIdentifier, parcCode, clasId_clasId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelClas> findLazyParcelGP(boolean noTransient, Integer prodCode, String parcIdentifier, String parcCode, Integer clasId_clasId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM ParcelClas x  left join fetch x.clasId x_clasId left join fetch x.cotyIdDecl x_cotyIdDecl left join fetch x.cultIdDecl x_cultIdDecl left join fetch x.cultIdPred x_cultIdPred left join fetch x.cultIdPred2 x_cultIdPred2 WHERE (x.prodCode = :prodCode) AND (x.parcIdentifier like :parcIdentifier) AND (x.parcCode like :parcCode) AND (x.clasId.clasId = :clasId_clasId) ORDER BY x.parcIdentifier ";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            if (prodCode == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":prodCode");
            } else {
                paramSets.put("prodCode", prodCode);
            }

            if (parcIdentifier == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":parcIdentifier");
            } else {
                paramSets.put("parcIdentifier", parcIdentifier+"%");
            }

            if (parcCode == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":parcCode");
            } else {
                paramSets.put("parcCode", parcCode+"%");
            }

            if (clasId_clasId == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":clasId_clasId");
            } else {
                paramSets.put("clasId_clasId", clasId_clasId);
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.pclaId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.pclaId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<ParcelClas> ret = (List<ParcelClas>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findLazyParcelGP", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findLazyParcelGP_count(Integer prodCode, String parcIdentifier, String parcCode, Integer clasId_clasId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM ParcelClas x WHERE (x.prodCode = :prodCode) AND (x.parcIdentifier like :parcIdentifier) AND (x.parcCode like :parcCode) AND (x.clasId.clasId = :clasId_clasId) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            if (prodCode == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":prodCode");
            } else {
                paramSets.put("prodCode", prodCode);
            }

            if (parcIdentifier == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":parcIdentifier");
            } else {
                paramSets.put("parcIdentifier", parcIdentifier+"%");
            }

            if (parcCode == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":parcCode");
            } else {
                paramSets.put("parcCode", parcCode+"%");
            }

            if (clasId_clasId == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":clasId_clasId");
            } else {
                paramSets.put("clasId_clasId", clasId_clasId);
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findLazyParcelGP_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelClas> findLazyDashboard(Integer prodCode, String parcIdentifier, String parcCode, Integer clasId_clasId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findLazyDashboard(false, prodCode, parcIdentifier, parcCode, clasId_clasId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelClas> findLazyDashboard(boolean noTransient, Integer prodCode, String parcIdentifier, String parcCode, Integer clasId_clasId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM ParcelClas x  left join fetch x.clasId x_clasId left join fetch x.cotyIdDecl x_cotyIdDecl left join fetch x.cultIdDecl x_cultIdDecl left join fetch x.cultIdPred x_cultIdPred left join fetch x.cultIdPred2 x_cultIdPred2 WHERE (x.prodCode = :prodCode) AND (x.parcIdentifier like :parcIdentifier) AND (x.parcCode like :parcCode) AND (x.clasId.clasId = :clasId_clasId) ORDER BY x.parcIdentifier, x.parcCode ";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            if (prodCode == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":prodCode");
            } else {
                paramSets.put("prodCode", prodCode);
            }

            if (parcIdentifier == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":parcIdentifier");
            } else {
                paramSets.put("parcIdentifier", parcIdentifier+"%");
            }

            if (parcCode == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":parcCode");
            } else {
                paramSets.put("parcCode", parcCode+"%");
            }

            if (clasId_clasId == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":clasId_clasId");
            } else {
                paramSets.put("clasId_clasId", clasId_clasId);
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.pclaId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.pclaId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<ParcelClas> ret = (List<ParcelClas>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findLazyDashboard", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findLazyDashboard_count(Integer prodCode, String parcIdentifier, String parcCode, Integer clasId_clasId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM ParcelClas x WHERE (x.prodCode = :prodCode) AND (x.parcIdentifier like :parcIdentifier) AND (x.parcCode like :parcCode) AND (x.clasId.clasId = :clasId_clasId) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            if (prodCode == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":prodCode");
            } else {
                paramSets.put("prodCode", prodCode);
            }

            if (parcIdentifier == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":parcIdentifier");
            } else {
                paramSets.put("parcIdentifier", parcIdentifier+"%");
            }

            if (parcCode == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":parcCode");
            } else {
                paramSets.put("parcCode", parcCode+"%");
            }

            if (clasId_clasId == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":clasId_clasId");
            } else {
                paramSets.put("clasId_clasId", clasId_clasId);
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findLazyDashboard_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelClas> findLazyParcelFMIS(Integer prodCode, String parcIdentifier, String parcCode, Integer clasId_clasId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findLazyParcelFMIS(false, prodCode, parcIdentifier, parcCode, clasId_clasId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelClas> findLazyParcelFMIS(boolean noTransient, Integer prodCode, String parcIdentifier, String parcCode, Integer clasId_clasId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM ParcelClas x  left join fetch x.clasId x_clasId left join fetch x.cotyIdDecl x_cotyIdDecl left join fetch x.cultIdDecl x_cultIdDecl left join fetch x.cultIdPred x_cultIdPred left join fetch x.cultIdPred2 x_cultIdPred2 WHERE (x.prodCode = :prodCode) AND (x.parcIdentifier like :parcIdentifier) AND (x.parcCode like :parcCode) AND (x.clasId.clasId = :clasId_clasId) ORDER BY x.parcIdentifier, x.parcCode ";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            if (prodCode == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":prodCode");
            } else {
                paramSets.put("prodCode", prodCode);
            }

            if (parcIdentifier == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":parcIdentifier");
            } else {
                paramSets.put("parcIdentifier", parcIdentifier+"%");
            }

            if (parcCode == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":parcCode");
            } else {
                paramSets.put("parcCode", parcCode+"%");
            }

            if (clasId_clasId == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":clasId_clasId");
            } else {
                paramSets.put("clasId_clasId", clasId_clasId);
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.pclaId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.pclaId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<ParcelClas> ret = (List<ParcelClas>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findLazyParcelFMIS", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findLazyParcelFMIS_count(Integer prodCode, String parcIdentifier, String parcCode, Integer clasId_clasId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM ParcelClas x WHERE (x.prodCode = :prodCode) AND (x.parcIdentifier like :parcIdentifier) AND (x.parcCode like :parcCode) AND (x.clasId.clasId = :clasId_clasId) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            if (prodCode == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":prodCode");
            } else {
                paramSets.put("prodCode", prodCode);
            }

            if (parcIdentifier == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":parcIdentifier");
            } else {
                paramSets.put("parcIdentifier", parcIdentifier+"%");
            }

            if (parcCode == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":parcCode");
            } else {
                paramSets.put("parcCode", parcCode+"%");
            }

            if (clasId_clasId == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":clasId_clasId");
            } else {
                paramSets.put("clasId_clasId", clasId_clasId);
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findLazyParcelFMIS_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelClas> findAllByCriteriaRange_ClassificationGrpParcelClas(Integer clasId_clasId, String fsch_parcIdentifier, String fsch_parcCode, Integer fsch_prodCode, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_ClassificationGrpParcelClas(false, clasId_clasId, fsch_parcIdentifier, fsch_parcCode, fsch_prodCode, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelClas> findAllByCriteriaRange_ClassificationGrpParcelClas(boolean noTransient, Integer clasId_clasId, String fsch_parcIdentifier, String fsch_parcCode, Integer fsch_prodCode, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM ParcelClas x  left join fetch x.clasId x_clasId left join fetch x.cotyIdDecl x_cotyIdDecl left join fetch x.cultIdDecl x_cultIdDecl left join fetch x.cultIdPred x_cultIdPred left join fetch x.cultIdPred2 x_cultIdPred2 WHERE ((1=1) AND (x.clasId.clasId = :clasId_clasId)) AND (x.parcIdentifier like :fsch_parcIdentifier) AND (x.parcCode like :fsch_parcCode) AND (x.prodCode = :fsch_prodCode) ORDER BY x.parcIdentifier";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("clasId_clasId", clasId_clasId);

            if (fsch_parcIdentifier == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_parcIdentifier");
            } else {
                paramSets.put("fsch_parcIdentifier", fsch_parcIdentifier+"%");
            }

            if (fsch_parcCode == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_parcCode");
            } else {
                paramSets.put("fsch_parcCode", fsch_parcCode+"%");
            }

            if (fsch_prodCode == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_prodCode");
            } else {
                paramSets.put("fsch_prodCode", fsch_prodCode);
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.pclaId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.pclaId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<ParcelClas> ret = (List<ParcelClas>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_ClassificationGrpParcelClas", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_ClassificationGrpParcelClas_count(Integer clasId_clasId, String fsch_parcIdentifier, String fsch_parcCode, Integer fsch_prodCode, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM ParcelClas x WHERE ((1=1) AND (x.clasId.clasId = :clasId_clasId)) AND (x.parcIdentifier like :fsch_parcIdentifier) AND (x.parcCode like :fsch_parcCode) AND (x.prodCode = :fsch_prodCode) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("clasId_clasId", clasId_clasId);

            if (fsch_parcIdentifier == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_parcIdentifier");
            } else {
                paramSets.put("fsch_parcIdentifier", fsch_parcIdentifier+"%");
            }

            if (fsch_parcCode == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_parcCode");
            } else {
                paramSets.put("fsch_parcCode", fsch_parcCode+"%");
            }

            if (fsch_prodCode == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_prodCode");
            } else {
                paramSets.put("fsch_prodCode", fsch_prodCode);
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_ClassificationGrpParcelClas_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelClas> findAllByCriteriaRange_forLov(Double fsch_ParcelClasLov_probPred, Double fsch_ParcelClasLov_probPred2, Integer fsch_ParcelClasLov_prodCode, String fsch_ParcelClasLov_parcIdentifier, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_forLov(false, fsch_ParcelClasLov_probPred, fsch_ParcelClasLov_probPred2, fsch_ParcelClasLov_prodCode, fsch_ParcelClasLov_parcIdentifier, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelClas> findAllByCriteriaRange_forLov(boolean noTransient, Double fsch_ParcelClasLov_probPred, Double fsch_ParcelClasLov_probPred2, Integer fsch_ParcelClasLov_prodCode, String fsch_ParcelClasLov_parcIdentifier, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM ParcelClas x  left join fetch x.clasId x_clasId left join fetch x.cotyIdDecl x_cotyIdDecl left join fetch x.cultIdDecl x_cultIdDecl left join fetch x.cultIdPred x_cultIdPred left join fetch x.cultIdPred2 x_cultIdPred2 WHERE (1=1) AND (x.probPred = :fsch_ParcelClasLov_probPred) AND (x.probPred2 = :fsch_ParcelClasLov_probPred2) AND (x.prodCode = :fsch_ParcelClasLov_prodCode) AND (x.parcIdentifier like :fsch_ParcelClasLov_parcIdentifier) ORDER BY x.pclaId";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            if (fsch_ParcelClasLov_probPred == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_ParcelClasLov_probPred");
            } else {
                paramSets.put("fsch_ParcelClasLov_probPred", fsch_ParcelClasLov_probPred);
            }

            if (fsch_ParcelClasLov_probPred2 == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_ParcelClasLov_probPred2");
            } else {
                paramSets.put("fsch_ParcelClasLov_probPred2", fsch_ParcelClasLov_probPred2);
            }

            if (fsch_ParcelClasLov_prodCode == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_ParcelClasLov_prodCode");
            } else {
                paramSets.put("fsch_ParcelClasLov_prodCode", fsch_ParcelClasLov_prodCode);
            }

            if (fsch_ParcelClasLov_parcIdentifier == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_ParcelClasLov_parcIdentifier");
            } else {
                paramSets.put("fsch_ParcelClasLov_parcIdentifier", fsch_ParcelClasLov_parcIdentifier+"%");
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.pclaId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.pclaId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<ParcelClas> ret = (List<ParcelClas>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_forLov", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_forLov_count(Double fsch_ParcelClasLov_probPred, Double fsch_ParcelClasLov_probPred2, Integer fsch_ParcelClasLov_prodCode, String fsch_ParcelClasLov_parcIdentifier, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM ParcelClas x WHERE (1=1) AND (x.probPred = :fsch_ParcelClasLov_probPred) AND (x.probPred2 = :fsch_ParcelClasLov_probPred2) AND (x.prodCode = :fsch_ParcelClasLov_prodCode) AND (x.parcIdentifier like :fsch_ParcelClasLov_parcIdentifier) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            if (fsch_ParcelClasLov_probPred == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_ParcelClasLov_probPred");
            } else {
                paramSets.put("fsch_ParcelClasLov_probPred", fsch_ParcelClasLov_probPred);
            }

            if (fsch_ParcelClasLov_probPred2 == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_ParcelClasLov_probPred2");
            } else {
                paramSets.put("fsch_ParcelClasLov_probPred2", fsch_ParcelClasLov_probPred2);
            }

            if (fsch_ParcelClasLov_prodCode == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_ParcelClasLov_prodCode");
            } else {
                paramSets.put("fsch_ParcelClasLov_prodCode", fsch_ParcelClasLov_prodCode);
            }

            if (fsch_ParcelClasLov_parcIdentifier == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_ParcelClasLov_parcIdentifier");
            } else {
                paramSets.put("fsch_ParcelClasLov_parcIdentifier", fsch_ParcelClasLov_parcIdentifier+"%");
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_forLov_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
