package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class ParcelsIssuesActivitiesBaseFacade extends AbstractFacade<ParcelsIssuesActivities> implements  IParcelsIssuesActivitiesBaseFacade, /*.ILocal, IParcelsIssuesActivitiesBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(ParcelsIssuesActivitiesBaseFacade.class);

    public ParcelsIssuesActivitiesBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public ParcelsIssuesActivities initRow() {
        return new ParcelsIssuesActivities(getNextSequenceValue());
    }

    public void getTransientFields(List<ParcelsIssuesActivities> entities){
    }

    @Override
    public ParcelsIssuesActivities createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.IParcelsIssuesFacade.ILocal parcelsIssuesFacade, Set<String> allowedFields) {
        ParcelsIssuesActivities ret = null;
        if (!json.containsKey("parcelsIssuesActivitiesId") || json.get("parcelsIssuesActivitiesId") == null)
            throw new GenericApplicationException("Error in ParcelsIssuesActivitiesBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("parcelsIssuesActivitiesId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("parcelsIssuesActivitiesId"));
                //ret = this.findByParcelsIssuesActivitiesId(id);
                ret = getEntityManager().find(ParcelsIssuesActivities.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:ParcelsIssuesActivities:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : ParcelsIssuesActivities");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("type") && allowedFields.contains("type")) {
            Long type = (Long)json.get("type");
            ret.setType(type != null ? type.shortValue() : null);
        }
        if (json.containsKey("dteTimestamp") && allowedFields.contains("dteTimestamp")) {
            String dteTimestamp = (String)json.get("dteTimestamp");
            try {
                Date jsonDate = gr.neuropublic.utils.DateUtil.parseISO8601Date(dteTimestamp);
                Date entityDate = ret.getDteTimestamp();
                if (jsonDate!= null && !jsonDate.equals(entityDate)) {
                    ret.setDteTimestamp(jsonDate);
                } else {
                    if (jsonDate == null && entityDate != null) {
                        ret.setDteTimestamp(jsonDate);
                    }
                }
            } catch (ParseException ex) {
                throw new GenericApplicationException(ex.getMessage());
            }
        }
        if (json.containsKey("typeExtrainfo") && allowedFields.contains("typeExtrainfo")) {
            ret.setTypeExtrainfo((String)json.get("typeExtrainfo"));
        }
        if (json.containsKey("parcelsIssuesId") && allowedFields.contains("parcelsIssuesId")) {
            JSONObject parcelsIssuesId = (JSONObject)json.get("parcelsIssuesId");
            if (parcelsIssuesId == null) {
                ret.setParcelsIssuesId(null);
            } else {
                if (!parcelsIssuesId.containsKey("parcelsIssuesId") || parcelsIssuesId.get("parcelsIssuesId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. parcelsIssuesId is not null but PK is missing or null");
                } 
                ParcelsIssues parcelsIssuesId_db = null;
                String temp_id = (String)parcelsIssuesId.get("parcelsIssuesId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. parcelsIssuesId.parcelsIssuesId is negative but not in the keysMap dictionary");
                    }
                    parcelsIssuesId_db = (ParcelsIssues)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)parcelsIssuesId.get("parcelsIssuesId"));
                    parcelsIssuesId_db = parcelsIssuesFacade.findByParcelsIssuesId(id);
                    if (parcelsIssuesId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity ParcelsIssues with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setParcelsIssuesId(parcelsIssuesId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelsIssuesActivities> findAllByIds(List<Integer> ids) {

        List<ParcelsIssuesActivities> ret = (List<ParcelsIssuesActivities>) em.createQuery("SELECT x FROM ParcelsIssuesActivities x left join fetch x.parcelsIssuesId x_parcelsIssuesId  WHERE x.parcelsIssuesActivitiesId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM ParcelsIssuesActivities x WHERE x.parcelsIssuesActivitiesId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public ParcelsIssuesActivities findByParcelsIssuesActivitiesId(Integer parcelsIssuesActivitiesId) {
        List<ParcelsIssuesActivities> results = (List<ParcelsIssuesActivities>) em.createQuery("SELECT x FROM ParcelsIssuesActivities x left join fetch x.parcelsIssuesId x_parcelsIssuesId  WHERE x.parcelsIssuesActivitiesId = :parcelsIssuesActivitiesId").
            setParameter("parcelsIssuesActivitiesId", parcelsIssuesActivitiesId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByParcelsIssuesActivitiesId(Integer parcelsIssuesActivitiesId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM ParcelsIssuesActivities x WHERE x.parcelsIssuesActivitiesId = :parcelsIssuesActivitiesId")
    .setParameter("parcelsIssuesActivitiesId", parcelsIssuesActivitiesId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelsIssuesActivities> findAllByCriteriaRange_ParcelsIssuesGrpParcelsIssuesActivities(Integer parcelsIssuesId_parcelsIssuesId, Short fsch_type, Date fsch_dteTimestamp, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_ParcelsIssuesGrpParcelsIssuesActivities(false, parcelsIssuesId_parcelsIssuesId, fsch_type, fsch_dteTimestamp, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelsIssuesActivities> findAllByCriteriaRange_ParcelsIssuesGrpParcelsIssuesActivities(boolean noTransient, Integer parcelsIssuesId_parcelsIssuesId, Short fsch_type, Date fsch_dteTimestamp, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM ParcelsIssuesActivities x  left join fetch x.parcelsIssuesId x_parcelsIssuesId WHERE ((1=1) AND (x.parcelsIssuesId.parcelsIssuesId = :parcelsIssuesId_parcelsIssuesId)) AND (x.type = :fsch_type) AND (x.dteTimestamp = :fsch_dteTimestamp) ORDER BY x.type, x.parcelsIssuesActivitiesId";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("parcelsIssuesId_parcelsIssuesId", parcelsIssuesId_parcelsIssuesId);

            if (fsch_type == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_type");
            } else {
                paramSets.put("fsch_type", fsch_type);
            }

            if (fsch_dteTimestamp == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_dteTimestamp");
            } else {
                paramSets.put("fsch_dteTimestamp", fsch_dteTimestamp);
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.parcelsIssuesActivitiesId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.parcelsIssuesActivitiesId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<ParcelsIssuesActivities> ret = (List<ParcelsIssuesActivities>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_ParcelsIssuesGrpParcelsIssuesActivities", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_ParcelsIssuesGrpParcelsIssuesActivities_count(Integer parcelsIssuesId_parcelsIssuesId, Short fsch_type, Date fsch_dteTimestamp, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM ParcelsIssuesActivities x WHERE ((1=1) AND (x.parcelsIssuesId.parcelsIssuesId = :parcelsIssuesId_parcelsIssuesId)) AND (x.type = :fsch_type) AND (x.dteTimestamp = :fsch_dteTimestamp) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("parcelsIssuesId_parcelsIssuesId", parcelsIssuesId_parcelsIssuesId);

            if (fsch_type == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_type");
            } else {
                paramSets.put("fsch_type", fsch_type);
            }

            if (fsch_dteTimestamp == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_dteTimestamp");
            } else {
                paramSets.put("fsch_dteTimestamp", fsch_dteTimestamp);
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_ParcelsIssuesGrpParcelsIssuesActivities_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
