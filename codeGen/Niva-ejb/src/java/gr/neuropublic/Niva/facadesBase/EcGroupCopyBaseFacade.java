package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class EcGroupCopyBaseFacade extends AbstractFacade<EcGroupCopy> implements  IEcGroupCopyBaseFacade, /*.ILocal, IEcGroupCopyBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(EcGroupCopyBaseFacade.class);

    public EcGroupCopyBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public EcGroupCopy initRow() {
        return new EcGroupCopy(getNextSequenceValue());
    }

    public void getTransientFields(List<EcGroupCopy> entities){
    }

    @Override
    public EcGroupCopy createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.IDecisionMakingFacade.ILocal decisionMakingFacade, Set<String> allowedFields) {
        EcGroupCopy ret = null;
        if (!json.containsKey("ecgcId") || json.get("ecgcId") == null)
            throw new GenericApplicationException("Error in EcGroupCopyBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("ecgcId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("ecgcId"));
                //ret = this.findByEcgcId(id);
                ret = getEntityManager().find(EcGroupCopy.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:EcGroupCopy:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("description") && allowedFields.contains("description")) {
            ret.setDescription((String)json.get("description"));
        }
        if (json.containsKey("name") && allowedFields.contains("name")) {
            ret.setName((String)json.get("name"));
        }
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : EcGroupCopy");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("recordtype") && allowedFields.contains("recordtype")) {
            Long recordtype = (Long)json.get("recordtype");
            ret.setRecordtype(recordtype != null ? recordtype.intValue() : null);
        }
        if (json.containsKey("cropLevel") && allowedFields.contains("cropLevel")) {
            Long cropLevel = (Long)json.get("cropLevel");
            ret.setCropLevel(cropLevel != null ? cropLevel.shortValue() : null);
        }
        if (json.containsKey("demaId") && allowedFields.contains("demaId")) {
            JSONObject demaId = (JSONObject)json.get("demaId");
            if (demaId == null) {
                ret.setDemaId(null);
            } else {
                if (!demaId.containsKey("demaId") || demaId.get("demaId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. demaId is not null but PK is missing or null");
                } 
                DecisionMaking demaId_db = null;
                String temp_id = (String)demaId.get("demaId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. demaId.demaId is negative but not in the keysMap dictionary");
                    }
                    demaId_db = (DecisionMaking)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)demaId.get("demaId"));
                    demaId_db = decisionMakingFacade.findByDemaId(id);
                    if (demaId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity DecisionMaking with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setDemaId(demaId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.ec_group_copy_ecgc_id_seq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcGroupCopy> findAllByIds(List<Integer> ids) {

        List<EcGroupCopy> ret = (List<EcGroupCopy>) em.createQuery("SELECT x FROM EcGroupCopy x left join fetch x.demaId x_demaId  WHERE x.ecgcId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM EcGroupCopy x WHERE x.ecgcId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public EcGroupCopy findByEcgcId(Integer ecgcId) {
        List<EcGroupCopy> results = (List<EcGroupCopy>) em.createQuery("SELECT x FROM EcGroupCopy x left join fetch x.demaId x_demaId  WHERE x.ecgcId = :ecgcId").
            setParameter("ecgcId", ecgcId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByEcgcId(Integer ecgcId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM EcGroupCopy x WHERE x.ecgcId = :ecgcId")
    .setParameter("ecgcId", ecgcId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcGroupCopy> findAllByCriteriaRange_DecisionMakingGrpEcGroupCopy(Integer demaId_demaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_DecisionMakingGrpEcGroupCopy(false, demaId_demaId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcGroupCopy> findAllByCriteriaRange_DecisionMakingGrpEcGroupCopy(boolean noTransient, Integer demaId_demaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM EcGroupCopy x  left join fetch x.demaId x_demaId WHERE ((1=1) AND (x.demaId.demaId = :demaId_demaId)) ORDER BY x.description";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("demaId_demaId", demaId_demaId);

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.ecgcId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.ecgcId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<EcGroupCopy> ret = (List<EcGroupCopy>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DecisionMakingGrpEcGroupCopy", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_DecisionMakingGrpEcGroupCopy_count(Integer demaId_demaId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x) FROM EcGroupCopy x WHERE ((1=1) AND (x.demaId.demaId = :demaId_demaId)) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("demaId_demaId", demaId_demaId);


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DecisionMakingGrpEcGroupCopy_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
