package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class GpUploadBaseFacade extends AbstractFacade<GpUpload> implements  IGpUploadBaseFacade, /*.ILocal, IGpUploadBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(GpUploadBaseFacade.class);

    public GpUploadBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public GpUpload initRow() {
        return new GpUpload(getNextSequenceValue());
    }

    public void getTransientFields(List<GpUpload> entities){
    }

    @Override
    public GpUpload createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.IGpRequestsContextsFacade.ILocal gpRequestsContextsFacade, Set<String> allowedFields) {
        GpUpload ret = null;
        if (!json.containsKey("gpUploadsId") || json.get("gpUploadsId") == null)
            throw new GenericApplicationException("Error in GpUploadBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("gpUploadsId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("gpUploadsId"));
                //ret = this.findByGpUploadsId(id);
                ret = getEntityManager().find(GpUpload.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:GpUpload:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("data") && allowedFields.contains("data")) {
            ret.setData((String)json.get("data"));
        }
        if (json.containsKey("environment") && allowedFields.contains("environment")) {
            ret.setEnvironment((String)json.get("environment"));
        }
        if (json.containsKey("dteUpload") && allowedFields.contains("dteUpload")) {
            String dteUpload = (String)json.get("dteUpload");
            try {
                Date jsonDate = gr.neuropublic.utils.DateUtil.parseISO8601Date(dteUpload);
                Date entityDate = ret.getDteUpload();
                if (jsonDate!= null && !jsonDate.equals(entityDate)) {
                    ret.setDteUpload(jsonDate);
                } else {
                    if (jsonDate == null && entityDate != null) {
                        ret.setDteUpload(jsonDate);
                    }
                }
            } catch (ParseException ex) {
                throw new GenericApplicationException(ex.getMessage());
            }
        }
        if (json.containsKey("hash") && allowedFields.contains("hash")) {
            ret.setHash((String)json.get("hash"));
        }
        if (json.containsKey("image") && allowedFields.contains("image")) {
            String image = (String)json.get("image");
            if (image != null) {
                
                TmpBlob tmpBlob = getTmpBlobFacade().findById(crypto.DecryptInteger(image));
                if (tmpBlob!= null) {
                    ret.setImage(tmpBlob.getData());
                } else {
                    throw new RuntimeException("TEMP_BLOB_DELETED");
                }    } else {
                ret.setImage(null);
            }
        }
        if (json.containsKey("gpRequestsContextsId") && allowedFields.contains("gpRequestsContextsId")) {
            JSONObject gpRequestsContextsId = (JSONObject)json.get("gpRequestsContextsId");
            if (gpRequestsContextsId == null) {
                ret.setGpRequestsContextsId(null);
            } else {
                if (!gpRequestsContextsId.containsKey("gpRequestsContextsId") || gpRequestsContextsId.get("gpRequestsContextsId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. gpRequestsContextsId is not null but PK is missing or null");
                } 
                GpRequestsContexts gpRequestsContextsId_db = null;
                String temp_id = (String)gpRequestsContextsId.get("gpRequestsContextsId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. gpRequestsContextsId.gpRequestsContextsId is negative but not in the keysMap dictionary");
                    }
                    gpRequestsContextsId_db = (GpRequestsContexts)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)gpRequestsContextsId.get("gpRequestsContextsId"));
                    gpRequestsContextsId_db = gpRequestsContextsFacade.findByGpRequestsContextsId(id);
                    if (gpRequestsContextsId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity GpRequestsContexts with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setGpRequestsContextsId(gpRequestsContextsId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<GpUpload> findAllByIds(List<Integer> ids) {

        List<GpUpload> ret = (List<GpUpload>) em.createQuery("SELECT x FROM GpUpload x left join fetch x.gpRequestsContextsId x_gpRequestsContextsId left join fetch x_gpRequestsContextsId.parcelsIssuesId x_gpRequestsContextsId_parcelsIssuesId left join fetch x_gpRequestsContextsId_parcelsIssuesId.pclaId x_gpRequestsContextsId_parcelsIssuesId_pclaId  WHERE x.gpUploadsId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM GpUpload x WHERE x.gpUploadsId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public GpUpload findByGpUploadsId(Integer gpUploadsId) {
        List<GpUpload> results = (List<GpUpload>) em.createQuery("SELECT x FROM GpUpload x left join fetch x.gpRequestsContextsId x_gpRequestsContextsId left join fetch x_gpRequestsContextsId.parcelsIssuesId x_gpRequestsContextsId_parcelsIssuesId left join fetch x_gpRequestsContextsId_parcelsIssuesId.pclaId x_gpRequestsContextsId_parcelsIssuesId_pclaId  WHERE x.gpUploadsId = :gpUploadsId").
            setParameter("gpUploadsId", gpUploadsId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByGpUploadsId(Integer gpUploadsId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM GpUpload x WHERE x.gpUploadsId = :gpUploadsId")
    .setParameter("gpUploadsId", gpUploadsId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<GpUpload> findAllByCriteriaRange_ParcelGPGrpGpUpload(Integer gpRequestsContextsId_gpRequestsContextsId, Date fsch_dteUpload, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_ParcelGPGrpGpUpload(false, gpRequestsContextsId_gpRequestsContextsId, fsch_dteUpload, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<GpUpload> findAllByCriteriaRange_ParcelGPGrpGpUpload(boolean noTransient, Integer gpRequestsContextsId_gpRequestsContextsId, Date fsch_dteUpload, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM GpUpload x  left join fetch x.gpRequestsContextsId x_gpRequestsContextsId left join fetch x_gpRequestsContextsId.parcelsIssuesId x_gpRequestsContextsId_parcelsIssuesId left join fetch x_gpRequestsContextsId_parcelsIssuesId.pclaId x_gpRequestsContextsId_parcelsIssuesId_pclaId WHERE ((1=1) AND (x.gpRequestsContextsId.gpRequestsContextsId = :gpRequestsContextsId_gpRequestsContextsId)) AND (x.dteUpload = :fsch_dteUpload) ORDER BY x.gpUploadsId ";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("gpRequestsContextsId_gpRequestsContextsId", gpRequestsContextsId_gpRequestsContextsId);

            if (fsch_dteUpload == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_dteUpload");
            } else {
                paramSets.put("fsch_dteUpload", fsch_dteUpload);
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.gpUploadsId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.gpUploadsId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<GpUpload> ret = (List<GpUpload>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_ParcelGPGrpGpUpload", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_ParcelGPGrpGpUpload_count(Integer gpRequestsContextsId_gpRequestsContextsId, Date fsch_dteUpload, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM GpUpload x WHERE ((1=1) AND (x.gpRequestsContextsId.gpRequestsContextsId = :gpRequestsContextsId_gpRequestsContextsId)) AND (x.dteUpload = :fsch_dteUpload) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("gpRequestsContextsId_gpRequestsContextsId", gpRequestsContextsId_gpRequestsContextsId);

            if (fsch_dteUpload == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_dteUpload");
            } else {
                paramSets.put("fsch_dteUpload", fsch_dteUpload);
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_ParcelGPGrpGpUpload_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<GpUpload> findAllByCriteriaRange_ParcelsIssuesGrpGpUpload(Integer gpRequestsContextsId_gpRequestsContextsId, String fsch_data, String fsch_environment, Date fsch_dteUpload, String fsch_hash, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_ParcelsIssuesGrpGpUpload(false, gpRequestsContextsId_gpRequestsContextsId, fsch_data, fsch_environment, fsch_dteUpload, fsch_hash, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<GpUpload> findAllByCriteriaRange_ParcelsIssuesGrpGpUpload(boolean noTransient, Integer gpRequestsContextsId_gpRequestsContextsId, String fsch_data, String fsch_environment, Date fsch_dteUpload, String fsch_hash, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM GpUpload x  left join fetch x.gpRequestsContextsId x_gpRequestsContextsId left join fetch x_gpRequestsContextsId.parcelsIssuesId x_gpRequestsContextsId_parcelsIssuesId left join fetch x_gpRequestsContextsId_parcelsIssuesId.pclaId x_gpRequestsContextsId_parcelsIssuesId_pclaId WHERE ((1=1) AND (x.gpRequestsContextsId.gpRequestsContextsId = :gpRequestsContextsId_gpRequestsContextsId)) AND (x.data like :fsch_data) AND (x.environment like :fsch_environment) AND (x.dteUpload = :fsch_dteUpload) AND (x.hash like :fsch_hash) ORDER BY x.data, x.gpUploadsId";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("gpRequestsContextsId_gpRequestsContextsId", gpRequestsContextsId_gpRequestsContextsId);

            if (fsch_data == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_data");
            } else {
                paramSets.put("fsch_data", fsch_data+"%");
            }

            if (fsch_environment == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_environment");
            } else {
                paramSets.put("fsch_environment", fsch_environment+"%");
            }

            if (fsch_dteUpload == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_dteUpload");
            } else {
                paramSets.put("fsch_dteUpload", fsch_dteUpload);
            }

            if (fsch_hash == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_hash");
            } else {
                paramSets.put("fsch_hash", fsch_hash+"%");
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.gpUploadsId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.gpUploadsId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<GpUpload> ret = (List<GpUpload>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_ParcelsIssuesGrpGpUpload", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_ParcelsIssuesGrpGpUpload_count(Integer gpRequestsContextsId_gpRequestsContextsId, String fsch_data, String fsch_environment, Date fsch_dteUpload, String fsch_hash, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM GpUpload x WHERE ((1=1) AND (x.gpRequestsContextsId.gpRequestsContextsId = :gpRequestsContextsId_gpRequestsContextsId)) AND (x.data like :fsch_data) AND (x.environment like :fsch_environment) AND (x.dteUpload = :fsch_dteUpload) AND (x.hash like :fsch_hash) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("gpRequestsContextsId_gpRequestsContextsId", gpRequestsContextsId_gpRequestsContextsId);

            if (fsch_data == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_data");
            } else {
                paramSets.put("fsch_data", fsch_data+"%");
            }

            if (fsch_environment == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_environment");
            } else {
                paramSets.put("fsch_environment", fsch_environment+"%");
            }

            if (fsch_dteUpload == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_dteUpload");
            } else {
                paramSets.put("fsch_dteUpload", fsch_dteUpload);
            }

            if (fsch_hash == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_hash");
            } else {
                paramSets.put("fsch_hash", fsch_hash+"%");
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_ParcelsIssuesGrpGpUpload_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
