package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class EcCultCopyBaseFacade extends AbstractFacade<EcCultCopy> implements  IEcCultCopyBaseFacade, /*.ILocal, IEcCultCopyBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(EcCultCopyBaseFacade.class);

    public EcCultCopyBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public EcCultCopy initRow() {
        return new EcCultCopy(getNextSequenceValue());
    }

    public void getTransientFields(List<EcCultCopy> entities){
    }

    @Override
    public EcCultCopy createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.ICultivationFacade.ILocal cultivationFacade, gr.neuropublic.Niva.facades.IEcGroupCopyFacade.ILocal ecGroupCopyFacade, gr.neuropublic.Niva.facades.ICoverTypeFacade.ILocal coverTypeFacade, gr.neuropublic.Niva.facades.ISuperClasFacade.ILocal superClasFacade, Set<String> allowedFields) {
        EcCultCopy ret = null;
        if (!json.containsKey("ecccId") || json.get("ecccId") == null)
            throw new GenericApplicationException("Error in EcCultCopyBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("ecccId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("ecccId"));
                //ret = this.findByEcccId(id);
                ret = getEntityManager().find(EcCultCopy.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:EcCultCopy:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("noneMatchDecision") && allowedFields.contains("noneMatchDecision")) {
            Long noneMatchDecision = (Long)json.get("noneMatchDecision");
            ret.setNoneMatchDecision(noneMatchDecision != null ? noneMatchDecision.intValue() : null);
        }
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : EcCultCopy");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("cultId") && allowedFields.contains("cultId")) {
            JSONObject cultId = (JSONObject)json.get("cultId");
            if (cultId == null) {
                ret.setCultId(null);
            } else {
                if (!cultId.containsKey("cultId") || cultId.get("cultId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. cultId is not null but PK is missing or null");
                } 
                Cultivation cultId_db = null;
                String temp_id = (String)cultId.get("cultId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. cultId.cultId is negative but not in the keysMap dictionary");
                    }
                    cultId_db = (Cultivation)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)cultId.get("cultId"));
                    cultId_db = cultivationFacade.findByCultId(id);
                    if (cultId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity Cultivation with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setCultId(cultId_db);
            }
        }
        if (json.containsKey("ecgcId") && allowedFields.contains("ecgcId")) {
            JSONObject ecgcId = (JSONObject)json.get("ecgcId");
            if (ecgcId == null) {
                ret.setEcgcId(null);
            } else {
                if (!ecgcId.containsKey("ecgcId") || ecgcId.get("ecgcId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. ecgcId is not null but PK is missing or null");
                } 
                EcGroupCopy ecgcId_db = null;
                String temp_id = (String)ecgcId.get("ecgcId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. ecgcId.ecgcId is negative but not in the keysMap dictionary");
                    }
                    ecgcId_db = (EcGroupCopy)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)ecgcId.get("ecgcId"));
                    ecgcId_db = ecGroupCopyFacade.findByEcgcId(id);
                    if (ecgcId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity EcGroupCopy with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setEcgcId(ecgcId_db);
            }
        }
        if (json.containsKey("cotyId") && allowedFields.contains("cotyId")) {
            JSONObject cotyId = (JSONObject)json.get("cotyId");
            if (cotyId == null) {
                ret.setCotyId(null);
            } else {
                if (!cotyId.containsKey("cotyId") || cotyId.get("cotyId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. cotyId is not null but PK is missing or null");
                } 
                CoverType cotyId_db = null;
                String temp_id = (String)cotyId.get("cotyId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. cotyId.cotyId is negative but not in the keysMap dictionary");
                    }
                    cotyId_db = (CoverType)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)cotyId.get("cotyId"));
                    cotyId_db = coverTypeFacade.findByCotyId(id);
                    if (cotyId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity CoverType with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setCotyId(cotyId_db);
            }
        }
        if (json.containsKey("sucaId") && allowedFields.contains("sucaId")) {
            JSONObject sucaId = (JSONObject)json.get("sucaId");
            if (sucaId == null) {
                ret.setSucaId(null);
            } else {
                if (!sucaId.containsKey("sucaId") || sucaId.get("sucaId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. sucaId is not null but PK is missing or null");
                } 
                SuperClas sucaId_db = null;
                String temp_id = (String)sucaId.get("sucaId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. sucaId.sucaId is negative but not in the keysMap dictionary");
                    }
                    sucaId_db = (SuperClas)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)sucaId.get("sucaId"));
                    sucaId_db = superClasFacade.findBySucaId(id);
                    if (sucaId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity SuperClas with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setSucaId(sucaId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.ec_cult_copy_eccc_id_seq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCultCopy> findAllByIds(List<Integer> ids) {

        List<EcCultCopy> ret = (List<EcCultCopy>) em.createQuery("SELECT x FROM EcCultCopy x left join fetch x.cotyId x_cotyId left join fetch x.cultId x_cultId left join fetch x.ecgcId x_ecgcId left join fetch x_cultId.cotyId x_cultId_cotyId left join fetch x_ecgcId.demaId x_ecgcId_demaId  WHERE x.ecccId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM EcCultCopy x WHERE x.ecccId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public EcCultCopy findByEcccId(Integer ecccId) {
        List<EcCultCopy> results = (List<EcCultCopy>) em.createQuery("SELECT x FROM EcCultCopy x left join fetch x.cotyId x_cotyId left join fetch x.cultId x_cultId left join fetch x.ecgcId x_ecgcId left join fetch x_cultId.cotyId x_cultId_cotyId left join fetch x_ecgcId.demaId x_ecgcId_demaId  WHERE x.ecccId = :ecccId").
            setParameter("ecccId", ecccId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByEcccId(Integer ecccId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM EcCultCopy x WHERE x.ecccId = :ecccId")
    .setParameter("ecccId", ecccId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCultCopy> findAllByCriteriaRange_DecisionMakingGrpEcCultCopy(Integer ecgcId_ecgcId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_DecisionMakingGrpEcCultCopy(false, ecgcId_ecgcId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCultCopy> findAllByCriteriaRange_DecisionMakingGrpEcCultCopy(boolean noTransient, Integer ecgcId_ecgcId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM EcCultCopy x  left join fetch x.cotyId x_cotyId left join fetch x.cultId x_cultId left join fetch x.ecgcId x_ecgcId left join fetch x_cultId.cotyId x_cultId_cotyId left join fetch x_ecgcId.demaId x_ecgcId_demaId WHERE ((1=1) AND (x.ecgcId.ecgcId = :ecgcId_ecgcId)) ORDER BY x.noneMatchDecision";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("ecgcId_ecgcId", ecgcId_ecgcId);

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.ecccId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.ecccId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<EcCultCopy> ret = (List<EcCultCopy>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DecisionMakingGrpEcCultCopy", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_DecisionMakingGrpEcCultCopy_count(Integer ecgcId_ecgcId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x) FROM EcCultCopy x WHERE ((1=1) AND (x.ecgcId.ecgcId = :ecgcId_ecgcId)) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("ecgcId_ecgcId", ecgcId_ecgcId);


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DecisionMakingGrpEcCultCopy_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCultCopy> findAllByCriteriaRange_DecisionMakingEcCultCopyCover(Integer ecgcId_ecgcId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_DecisionMakingEcCultCopyCover(false, ecgcId_ecgcId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCultCopy> findAllByCriteriaRange_DecisionMakingEcCultCopyCover(boolean noTransient, Integer ecgcId_ecgcId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM EcCultCopy x  left join fetch x.cotyId x_cotyId left join fetch x.cultId x_cultId left join fetch x.ecgcId x_ecgcId left join fetch x_cultId.cotyId x_cultId_cotyId left join fetch x_ecgcId.demaId x_ecgcId_demaId WHERE ((1=1) AND (x.ecgcId.ecgcId = :ecgcId_ecgcId)) ORDER BY x.noneMatchDecision";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("ecgcId_ecgcId", ecgcId_ecgcId);

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.ecccId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.ecccId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<EcCultCopy> ret = (List<EcCultCopy>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DecisionMakingEcCultCopyCover", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_DecisionMakingEcCultCopyCover_count(Integer ecgcId_ecgcId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x) FROM EcCultCopy x WHERE ((1=1) AND (x.ecgcId.ecgcId = :ecgcId_ecgcId)) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("ecgcId_ecgcId", ecgcId_ecgcId);


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DecisionMakingEcCultCopyCover_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCultCopy> findAllByCriteriaRange_DecisionMakingEcCultCopySuper(Integer ecgcId_ecgcId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_DecisionMakingEcCultCopySuper(false, ecgcId_ecgcId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCultCopy> findAllByCriteriaRange_DecisionMakingEcCultCopySuper(boolean noTransient, Integer ecgcId_ecgcId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM EcCultCopy x  left join fetch x.cotyId x_cotyId left join fetch x.cultId x_cultId left join fetch x.ecgcId x_ecgcId left join fetch x_cultId.cotyId x_cultId_cotyId left join fetch x_ecgcId.demaId x_ecgcId_demaId WHERE ((1=1) AND (x.ecgcId.ecgcId = :ecgcId_ecgcId)) ORDER BY x.noneMatchDecision";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("ecgcId_ecgcId", ecgcId_ecgcId);

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.ecccId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.ecccId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<EcCultCopy> ret = (List<EcCultCopy>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DecisionMakingEcCultCopySuper", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_DecisionMakingEcCultCopySuper_count(Integer ecgcId_ecgcId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x) FROM EcCultCopy x WHERE ((1=1) AND (x.ecgcId.ecgcId = :ecgcId_ecgcId)) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("ecgcId_ecgcId", ecgcId_ecgcId);


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DecisionMakingEcCultCopySuper_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
