package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class EcGroupBaseFacade extends AbstractFacade<EcGroup> implements  IEcGroupBaseFacade, /*.ILocal, IEcGroupBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(EcGroupBaseFacade.class);

    public EcGroupBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public EcGroup initRow() {
        return new EcGroup(getNextSequenceValue());
    }

    public void getTransientFields(List<EcGroup> entities){
    }

    @Override
    public EcGroup createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, Set<String> allowedFields) {
        EcGroup ret = null;
        if (!json.containsKey("ecgrId") || json.get("ecgrId") == null)
            throw new GenericApplicationException("Error in EcGroupBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("ecgrId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("ecgrId"));
                //ret = this.findByEcgrId(id);
                ret = getEntityManager().find(EcGroup.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:EcGroup:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("description") && allowedFields.contains("description")) {
            ret.setDescription((String)json.get("description"));
        }
        if (json.containsKey("name") && allowedFields.contains("name")) {
            ret.setName((String)json.get("name"));
        }
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : EcGroup");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("recordtype") && allowedFields.contains("recordtype")) {
            Long recordtype = (Long)json.get("recordtype");
            ret.setRecordtype(recordtype != null ? recordtype.intValue() : null);
        }
        if (json.containsKey("cropLevel") && allowedFields.contains("cropLevel")) {
            Long cropLevel = (Long)json.get("cropLevel");
            ret.setCropLevel(cropLevel != null ? cropLevel.shortValue() : null);
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcGroup> findAllByIds(List<Integer> ids) {

        List<EcGroup> ret = (List<EcGroup>) em.createQuery("SELECT x FROM EcGroup x   WHERE x.ecgrId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM EcGroup x WHERE x.ecgrId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public EcGroup findByEcgrId(Integer ecgrId) {
        List<EcGroup> results = (List<EcGroup>) em.createQuery("SELECT x FROM EcGroup x   WHERE x.ecgrId = :ecgrId").
            setParameter("ecgrId", ecgrId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByEcgrId(Integer ecgrId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM EcGroup x WHERE x.ecgrId = :ecgrId")
    .setParameter("ecgrId", ecgrId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcGroup> findLazyEcGroup(String description, String name, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findLazyEcGroup(false, description, name, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcGroup> findLazyEcGroup(boolean noTransient, String description, String name, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM EcGroup x   WHERE (x.description like :description) AND (x.name like :name) ";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            if (description == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":description");
            } else {
                paramSets.put("description", description+"%");
            }

            if (name == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":name");
            } else {
                paramSets.put("name", name+"%");
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.ecgrId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.ecgrId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<EcGroup> ret = (List<EcGroup>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findLazyEcGroup", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findLazyEcGroup_count(String description, String name, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM EcGroup x WHERE (x.description like :description) AND (x.name like :name) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            if (description == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":description");
            } else {
                paramSets.put("description", description+"%");
            }

            if (name == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":name");
            } else {
                paramSets.put("name", name+"%");
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findLazyEcGroup_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcGroup> findAllByCriteriaRange_forLov(String fsch_EcGroupLov_description, String fsch_EcGroupLov_name, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_forLov(false, fsch_EcGroupLov_description, fsch_EcGroupLov_name, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcGroup> findAllByCriteriaRange_forLov(boolean noTransient, String fsch_EcGroupLov_description, String fsch_EcGroupLov_name, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM EcGroup x   WHERE (1=1) AND (x.description like :fsch_EcGroupLov_description) AND (x.name like :fsch_EcGroupLov_name) ORDER BY x.description, x.ecgrId";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            if (fsch_EcGroupLov_description == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_EcGroupLov_description");
            } else {
                paramSets.put("fsch_EcGroupLov_description", fsch_EcGroupLov_description+"%");
            }

            if (fsch_EcGroupLov_name == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_EcGroupLov_name");
            } else {
                paramSets.put("fsch_EcGroupLov_name", fsch_EcGroupLov_name+"%");
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.ecgrId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.ecgrId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<EcGroup> ret = (List<EcGroup>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_forLov", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_forLov_count(String fsch_EcGroupLov_description, String fsch_EcGroupLov_name, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM EcGroup x WHERE (1=1) AND (x.description like :fsch_EcGroupLov_description) AND (x.name like :fsch_EcGroupLov_name) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            if (fsch_EcGroupLov_description == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_EcGroupLov_description");
            } else {
                paramSets.put("fsch_EcGroupLov_description", fsch_EcGroupLov_description+"%");
            }

            if (fsch_EcGroupLov_name == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_EcGroupLov_name");
            } else {
                paramSets.put("fsch_EcGroupLov_name", fsch_EcGroupLov_name+"%");
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_forLov_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
