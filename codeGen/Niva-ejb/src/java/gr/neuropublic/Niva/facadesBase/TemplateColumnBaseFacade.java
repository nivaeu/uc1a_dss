package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class TemplateColumnBaseFacade extends AbstractFacade<TemplateColumn> implements  ITemplateColumnBaseFacade, /*.ILocal, ITemplateColumnBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(TemplateColumnBaseFacade.class);

    public TemplateColumnBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public TemplateColumn initRow() {
        return new TemplateColumn(getNextSequenceValue());
    }

    public void getTransientFields(List<TemplateColumn> entities){
    }

    @Override
    public TemplateColumn createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.IFileTemplateFacade.ILocal fileTemplateFacade, gr.neuropublic.Niva.facades.IPredefColFacade.ILocal predefColFacade, Set<String> allowedFields) {
        TemplateColumn ret = null;
        if (!json.containsKey("tecoId") || json.get("tecoId") == null)
            throw new GenericApplicationException("Error in TemplateColumnBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("tecoId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("tecoId"));
                //ret = this.findByTecoId(id);
                ret = getEntityManager().find(TemplateColumn.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:TemplateColumn:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("clfierName") && allowedFields.contains("clfierName")) {
            ret.setClfierName((String)json.get("clfierName"));
        }
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : TemplateColumn");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("fiteId") && allowedFields.contains("fiteId")) {
            JSONObject fiteId = (JSONObject)json.get("fiteId");
            if (fiteId == null) {
                ret.setFiteId(null);
            } else {
                if (!fiteId.containsKey("fiteId") || fiteId.get("fiteId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. fiteId is not null but PK is missing or null");
                } 
                FileTemplate fiteId_db = null;
                String temp_id = (String)fiteId.get("fiteId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. fiteId.fiteId is negative but not in the keysMap dictionary");
                    }
                    fiteId_db = (FileTemplate)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)fiteId.get("fiteId"));
                    fiteId_db = fileTemplateFacade.findByFiteId(id);
                    if (fiteId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity FileTemplate with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setFiteId(fiteId_db);
            }
        }
        if (json.containsKey("prcoId") && allowedFields.contains("prcoId")) {
            JSONObject prcoId = (JSONObject)json.get("prcoId");
            if (prcoId == null) {
                ret.setPrcoId(null);
            } else {
                if (!prcoId.containsKey("prcoId") || prcoId.get("prcoId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. prcoId is not null but PK is missing or null");
                } 
                PredefCol prcoId_db = null;
                String temp_id = (String)prcoId.get("prcoId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. prcoId.prcoId is negative but not in the keysMap dictionary");
                    }
                    prcoId_db = (PredefCol)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)prcoId.get("prcoId"));
                    prcoId_db = predefColFacade.findByPrcoId(id);
                    if (prcoId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity PredefCol with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setPrcoId(prcoId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<TemplateColumn> findAllByIds(List<Integer> ids) {

        List<TemplateColumn> ret = (List<TemplateColumn>) em.createQuery("SELECT x FROM TemplateColumn x left join fetch x.fiteId x_fiteId left join fetch x.prcoId x_prcoId  WHERE x.tecoId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM TemplateColumn x WHERE x.tecoId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public TemplateColumn findByFiteId_PrcoId(FileTemplate fiteId, PredefCol prcoId) {
        List<TemplateColumn> results = (List<TemplateColumn>) em.createQuery("SELECT x FROM TemplateColumn x left join fetch x.fiteId x_fiteId left join fetch x.prcoId x_prcoId  WHERE x.fiteId.fiteId = :fiteId AND x.prcoId.prcoId = :prcoId").
            setParameter("fiteId", fiteId!=null?fiteId.getFiteId():null).
            setParameter("prcoId", prcoId!=null?prcoId.getPrcoId():null).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public TemplateColumn findByFiteId_PrcoId(Integer fiteId, Integer prcoId) {
        List<TemplateColumn> results = (List<TemplateColumn>) em.createQuery("SELECT x FROM TemplateColumn x left join fetch x.fiteId x_fiteId left join fetch x.prcoId x_prcoId  WHERE x.fiteId.fiteId = :fiteId AND x.prcoId.prcoId = :prcoId").
            setParameter("fiteId", fiteId).
            setParameter("prcoId", prcoId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByFiteId_PrcoId(FileTemplate fiteId, PredefCol prcoId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM TemplateColumn x WHERE x.fiteId.fiteId = :fiteId AND x.prcoId.prcoId = :prcoId")
    .setParameter("fiteId", fiteId!=null?fiteId.getFiteId():null).
            setParameter("prcoId", prcoId!=null?prcoId.getPrcoId():null);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    public int delByFiteId_PrcoId(Integer fiteId, Integer prcoId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM TemplateColumn x WHERE x.fiteId.fiteId = :fiteId AND x.prcoId.prcoId = :prcoId")
    .setParameter("fiteId", fiteId).
            setParameter("prcoId", prcoId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public TemplateColumn findByTecoId(Integer tecoId) {
        List<TemplateColumn> results = (List<TemplateColumn>) em.createQuery("SELECT x FROM TemplateColumn x left join fetch x.fiteId x_fiteId left join fetch x.prcoId x_prcoId  WHERE x.tecoId = :tecoId").
            setParameter("tecoId", tecoId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByTecoId(Integer tecoId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM TemplateColumn x WHERE x.tecoId = :tecoId")
    .setParameter("tecoId", tecoId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<TemplateColumn> findAllByCriteriaRange_FileTemplateGrpTemplateColumn(Integer fiteId_fiteId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_FileTemplateGrpTemplateColumn(false, fiteId_fiteId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<TemplateColumn> findAllByCriteriaRange_FileTemplateGrpTemplateColumn(boolean noTransient, Integer fiteId_fiteId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM TemplateColumn x  left join fetch x.fiteId x_fiteId left join fetch x.prcoId x_prcoId WHERE ((1=1) AND (x.fiteId.fiteId = :fiteId_fiteId)) ORDER BY x.clfierName";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("fiteId_fiteId", fiteId_fiteId);

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.tecoId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.tecoId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<TemplateColumn> ret = (List<TemplateColumn>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_FileTemplateGrpTemplateColumn", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_FileTemplateGrpTemplateColumn_count(Integer fiteId_fiteId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x) FROM TemplateColumn x WHERE ((1=1) AND (x.fiteId.fiteId = :fiteId_fiteId)) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("fiteId_fiteId", fiteId_fiteId);


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_FileTemplateGrpTemplateColumn_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
