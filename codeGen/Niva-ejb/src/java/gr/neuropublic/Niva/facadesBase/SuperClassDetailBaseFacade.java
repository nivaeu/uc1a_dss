package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class SuperClassDetailBaseFacade extends AbstractFacade<SuperClassDetail> implements  ISuperClassDetailBaseFacade, /*.ILocal, ISuperClassDetailBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(SuperClassDetailBaseFacade.class);

    public SuperClassDetailBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public SuperClassDetail initRow() {
        return new SuperClassDetail(getNextSequenceValue());
    }

    public void getTransientFields(List<SuperClassDetail> entities){
    }

    @Override
    public SuperClassDetail createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.ISuperClasFacade.ILocal superClasFacade, gr.neuropublic.Niva.facades.ICultivationFacade.ILocal cultivationFacade, Set<String> allowedFields) {
        SuperClassDetail ret = null;
        if (!json.containsKey("sucdId") || json.get("sucdId") == null)
            throw new GenericApplicationException("Error in SuperClassDetailBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("sucdId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("sucdId"));
                //ret = this.findBySucdId(id);
                ret = getEntityManager().find(SuperClassDetail.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:SuperClassDetail:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : SuperClassDetail");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("sucaId") && allowedFields.contains("sucaId")) {
            JSONObject sucaId = (JSONObject)json.get("sucaId");
            if (sucaId == null) {
                ret.setSucaId(null);
            } else {
                if (!sucaId.containsKey("sucaId") || sucaId.get("sucaId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. sucaId is not null but PK is missing or null");
                } 
                SuperClas sucaId_db = null;
                String temp_id = (String)sucaId.get("sucaId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. sucaId.sucaId is negative but not in the keysMap dictionary");
                    }
                    sucaId_db = (SuperClas)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)sucaId.get("sucaId"));
                    sucaId_db = superClasFacade.findBySucaId(id);
                    if (sucaId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity SuperClas with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setSucaId(sucaId_db);
            }
        }
        if (json.containsKey("cultId") && allowedFields.contains("cultId")) {
            JSONObject cultId = (JSONObject)json.get("cultId");
            if (cultId == null) {
                ret.setCultId(null);
            } else {
                if (!cultId.containsKey("cultId") || cultId.get("cultId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. cultId is not null but PK is missing or null");
                } 
                Cultivation cultId_db = null;
                String temp_id = (String)cultId.get("cultId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. cultId.cultId is negative but not in the keysMap dictionary");
                    }
                    cultId_db = (Cultivation)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)cultId.get("cultId"));
                    cultId_db = cultivationFacade.findByCultId(id);
                    if (cultId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity Cultivation with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setCultId(cultId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<SuperClassDetail> findAllByIds(List<Integer> ids) {

        List<SuperClassDetail> ret = (List<SuperClassDetail>) em.createQuery("SELECT x FROM SuperClassDetail x   WHERE x.sucdId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM SuperClassDetail x WHERE x.sucdId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public SuperClassDetail findBySucdId(Integer sucdId) {
        List<SuperClassDetail> results = (List<SuperClassDetail>) em.createQuery("SELECT x FROM SuperClassDetail x   WHERE x.sucdId = :sucdId").
            setParameter("sucdId", sucdId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delBySucdId(Integer sucdId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM SuperClassDetail x WHERE x.sucdId = :sucdId")
    .setParameter("sucdId", sucdId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
