package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class CultivationBaseFacade extends AbstractFacade<Cultivation> implements  ICultivationBaseFacade, /*.ILocal, ICultivationBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(CultivationBaseFacade.class);

    public CultivationBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Cultivation initRow() {
        return new Cultivation(getNextSequenceValue());
    }

    public void getTransientFields(List<Cultivation> entities){
    }

    @Override
    public Cultivation createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.ICoverTypeFacade.ILocal coverTypeFacade, gr.neuropublic.Niva.facades.IExcelFileFacade.ILocal excelFileFacade, Set<String> allowedFields) {
        Cultivation ret = null;
        if (!json.containsKey("cultId") || json.get("cultId") == null)
            throw new GenericApplicationException("Error in CultivationBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("cultId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("cultId"));
                //ret = this.findByCultId(id);
                ret = getEntityManager().find(Cultivation.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:Cultivation:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("name") && allowedFields.contains("name")) {
            ret.setName((String)json.get("name"));
        }
        if (json.containsKey("code") && allowedFields.contains("code")) {
            Long code = (Long)json.get("code");
            ret.setCode(code != null ? code.intValue() : null);
        }
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : Cultivation");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("cotyId") && allowedFields.contains("cotyId")) {
            JSONObject cotyId = (JSONObject)json.get("cotyId");
            if (cotyId == null) {
                ret.setCotyId(null);
            } else {
                if (!cotyId.containsKey("cotyId") || cotyId.get("cotyId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. cotyId is not null but PK is missing or null");
                } 
                CoverType cotyId_db = null;
                String temp_id = (String)cotyId.get("cotyId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. cotyId.cotyId is negative but not in the keysMap dictionary");
                    }
                    cotyId_db = (CoverType)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)cotyId.get("cotyId"));
                    cotyId_db = coverTypeFacade.findByCotyId(id);
                    if (cotyId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity CoverType with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setCotyId(cotyId_db);
            }
        }
        if (json.containsKey("exfiId") && allowedFields.contains("exfiId")) {
            JSONObject exfiId = (JSONObject)json.get("exfiId");
            if (exfiId == null) {
                ret.setExfiId(null);
            } else {
                if (!exfiId.containsKey("id") || exfiId.get("id") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. exfiId is not null but PK is missing or null");
                } 
                ExcelFile exfiId_db = null;
                String temp_id = (String)exfiId.get("id");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. exfiId.id is negative but not in the keysMap dictionary");
                    }
                    exfiId_db = (ExcelFile)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)exfiId.get("id"));
                    exfiId_db = excelFileFacade.findById(id);
                    if (exfiId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity ExcelFile with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setExfiId(exfiId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Cultivation> findAllByIds(List<Integer> ids) {

        List<Cultivation> ret = (List<Cultivation>) em.createQuery("SELECT x FROM Cultivation x left join fetch x.cotyId x_cotyId  WHERE x.cultId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM Cultivation x WHERE x.cultId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Cultivation findByCode(Integer code) {
        List<Cultivation> results = (List<Cultivation>) em.createQuery("SELECT x FROM Cultivation x left join fetch x.cotyId x_cotyId  WHERE x.code = :code").
            setParameter("code", code).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByCode(Integer code) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM Cultivation x WHERE x.code = :code")
    .setParameter("code", code);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Cultivation findByName(String name) {
        List<Cultivation> results = (List<Cultivation>) em.createQuery("SELECT x FROM Cultivation x left join fetch x.cotyId x_cotyId  WHERE x.name = :name").
            setParameter("name", name).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByName(String name) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM Cultivation x WHERE x.name = :name")
    .setParameter("name", name);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Cultivation findByCultId(Integer cultId) {
        List<Cultivation> results = (List<Cultivation>) em.createQuery("SELECT x FROM Cultivation x left join fetch x.cotyId x_cotyId  WHERE x.cultId = :cultId").
            setParameter("cultId", cultId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByCultId(Integer cultId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM Cultivation x WHERE x.cultId = :cultId")
    .setParameter("cultId", cultId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Cultivation> findAllByCriteriaRange_forLov(String fsch_CultivationLov_name, Integer fsch_CultivationLov_code, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_forLov(false, fsch_CultivationLov_name, fsch_CultivationLov_code, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Cultivation> findAllByCriteriaRange_forLov(boolean noTransient, String fsch_CultivationLov_name, Integer fsch_CultivationLov_code, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM Cultivation x  left join fetch x.cotyId x_cotyId WHERE (1=1) AND (x.name like :fsch_CultivationLov_name) AND (x.code = :fsch_CultivationLov_code) ORDER BY x.name";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            if (fsch_CultivationLov_name == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_CultivationLov_name");
            } else {
                paramSets.put("fsch_CultivationLov_name", fsch_CultivationLov_name+"%");
            }

            if (fsch_CultivationLov_code == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_CultivationLov_code");
            } else {
                paramSets.put("fsch_CultivationLov_code", fsch_CultivationLov_code);
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.cultId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.cultId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<Cultivation> ret = (List<Cultivation>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_forLov", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_forLov_count(String fsch_CultivationLov_name, Integer fsch_CultivationLov_code, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM Cultivation x WHERE (1=1) AND (x.name like :fsch_CultivationLov_name) AND (x.code = :fsch_CultivationLov_code) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            if (fsch_CultivationLov_name == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_CultivationLov_name");
            } else {
                paramSets.put("fsch_CultivationLov_name", fsch_CultivationLov_name+"%");
            }

            if (fsch_CultivationLov_code == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_CultivationLov_code");
            } else {
                paramSets.put("fsch_CultivationLov_code", fsch_CultivationLov_code);
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_forLov_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Cultivation> findAllByCriteriaRange_CultivationGrpCultivation(String fsch_name, Integer fsch_code, Integer fsch_cotyId_cotyId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_CultivationGrpCultivation(false, fsch_name, fsch_code, fsch_cotyId_cotyId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Cultivation> findAllByCriteriaRange_CultivationGrpCultivation(boolean noTransient, String fsch_name, Integer fsch_code, Integer fsch_cotyId_cotyId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM Cultivation x  left join fetch x.cotyId x_cotyId WHERE (1=1) AND (x.name like :fsch_name) AND (x.code = :fsch_code) AND (x.cotyId.cotyId = :fsch_cotyId_cotyId) ORDER BY x.name, x.cultId ";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            if (fsch_name == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_name");
            } else {
                paramSets.put("fsch_name", fsch_name+"%");
            }

            if (fsch_code == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_code");
            } else {
                paramSets.put("fsch_code", fsch_code);
            }

            if (fsch_cotyId_cotyId == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_cotyId_cotyId");
            } else {
                paramSets.put("fsch_cotyId_cotyId", fsch_cotyId_cotyId);
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.cultId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.cultId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<Cultivation> ret = (List<Cultivation>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_CultivationGrpCultivation", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_CultivationGrpCultivation_count(String fsch_name, Integer fsch_code, Integer fsch_cotyId_cotyId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM Cultivation x WHERE (1=1) AND (x.name like :fsch_name) AND (x.code = :fsch_code) AND (x.cotyId.cotyId = :fsch_cotyId_cotyId) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            if (fsch_name == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_name");
            } else {
                paramSets.put("fsch_name", fsch_name+"%");
            }

            if (fsch_code == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_code");
            } else {
                paramSets.put("fsch_code", fsch_code);
            }

            if (fsch_cotyId_cotyId == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_cotyId_cotyId");
            } else {
                paramSets.put("fsch_cotyId_cotyId", fsch_cotyId_cotyId);
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_CultivationGrpCultivation_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public List<Integer> findAllByCriteriaRange_CultivationGrpCultivation_getIds(String fsch_name, Integer fsch_code, Integer fsch_cotyId_cotyId, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x.cultId  FROM Cultivation x WHERE (1=1) AND (x.name like :fsch_name) AND (x.code = :fsch_code) AND (x.cotyId.cotyId = :fsch_cotyId_cotyId) ";
                
            if (excludedEntities != null && !excludedEntities.isEmpty()) {
                String exludedIds = "";
                for(String __id:excludedEntities) {
                    exludedIds += "," + __id;
                }
                exludedIds = "(1=1) AND (x.cultId not in (" + exludedIds.substring(1) + "))";
                jpql = jpql.replace("(1=1)", exludedIds);
            }
            
            
            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            if (fsch_name == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_name");
            } else {
                paramSets.put("fsch_name", fsch_name+"%");
            }

            if (fsch_code == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_code");
            } else {
                paramSets.put("fsch_code", fsch_code);
            }

            if (fsch_cotyId_cotyId == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_cotyId_cotyId");
            } else {
                paramSets.put("fsch_cotyId_cotyId", fsch_cotyId_cotyId);
            }

            
            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }

            List<Integer> ret = (List<Integer>)q.getResultList();
        
            return ret;
        } catch (Exception e) {
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
