package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class FmisUploadBaseFacade extends AbstractFacade<FmisUpload> implements  IFmisUploadBaseFacade, /*.ILocal, IFmisUploadBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(FmisUploadBaseFacade.class);

    public FmisUploadBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public FmisUpload initRow() {
        return new FmisUpload(getNextSequenceValue());
    }

    public void getTransientFields(List<FmisUpload> entities){
    }

    @Override
    public FmisUpload createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.IParcelsIssuesFacade.ILocal parcelsIssuesFacade, Set<String> allowedFields) {
        FmisUpload ret = null;
        if (!json.containsKey("fmisUploadsId") || json.get("fmisUploadsId") == null)
            throw new GenericApplicationException("Error in FmisUploadBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("fmisUploadsId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("fmisUploadsId"));
                //ret = this.findByFmisUploadsId(id);
                ret = getEntityManager().find(FmisUpload.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:FmisUpload:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("metadata") && allowedFields.contains("metadata")) {
            ret.setMetadata((String)json.get("metadata"));
        }
        if (json.containsKey("dteUpload") && allowedFields.contains("dteUpload")) {
            String dteUpload = (String)json.get("dteUpload");
            try {
                Date jsonDate = gr.neuropublic.utils.DateUtil.parseISO8601Date(dteUpload);
                Date entityDate = ret.getDteUpload();
                if (jsonDate!= null && !jsonDate.equals(entityDate)) {
                    ret.setDteUpload(jsonDate);
                } else {
                    if (jsonDate == null && entityDate != null) {
                        ret.setDteUpload(jsonDate);
                    }
                }
            } catch (ParseException ex) {
                throw new GenericApplicationException(ex.getMessage());
            }
        }
        if (json.containsKey("docfile") && allowedFields.contains("docfile")) {
            String docfile = (String)json.get("docfile");
            if (docfile != null) {
                
                TmpBlob tmpBlob = getTmpBlobFacade().findById(crypto.DecryptInteger(docfile));
                if (tmpBlob!= null) {
                    ret.setDocfile(tmpBlob.getData());
                } else {
                    throw new RuntimeException("TEMP_BLOB_DELETED");
                }    } else {
                ret.setDocfile(null);
            }
        }
        if (json.containsKey("usrUpload") && allowedFields.contains("usrUpload")) {
            ret.setUsrUpload((String)json.get("usrUpload"));
        }
        if (json.containsKey("parcelsIssuesId") && allowedFields.contains("parcelsIssuesId")) {
            JSONObject parcelsIssuesId = (JSONObject)json.get("parcelsIssuesId");
            if (parcelsIssuesId == null) {
                ret.setParcelsIssuesId(null);
            } else {
                if (!parcelsIssuesId.containsKey("parcelsIssuesId") || parcelsIssuesId.get("parcelsIssuesId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. parcelsIssuesId is not null but PK is missing or null");
                } 
                ParcelsIssues parcelsIssuesId_db = null;
                String temp_id = (String)parcelsIssuesId.get("parcelsIssuesId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. parcelsIssuesId.parcelsIssuesId is negative but not in the keysMap dictionary");
                    }
                    parcelsIssuesId_db = (ParcelsIssues)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)parcelsIssuesId.get("parcelsIssuesId"));
                    parcelsIssuesId_db = parcelsIssuesFacade.findByParcelsIssuesId(id);
                    if (parcelsIssuesId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity ParcelsIssues with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setParcelsIssuesId(parcelsIssuesId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<FmisUpload> findAllByIds(List<Integer> ids) {

        List<FmisUpload> ret = (List<FmisUpload>) em.createQuery("SELECT x FROM FmisUpload x left join fetch x.parcelsIssuesId x_parcelsIssuesId left join fetch x_parcelsIssuesId.pclaId x_parcelsIssuesId_pclaId  WHERE x.fmisUploadsId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM FmisUpload x WHERE x.fmisUploadsId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public FmisUpload findByFmisUploadsId(Integer fmisUploadsId) {
        List<FmisUpload> results = (List<FmisUpload>) em.createQuery("SELECT x FROM FmisUpload x left join fetch x.parcelsIssuesId x_parcelsIssuesId left join fetch x_parcelsIssuesId.pclaId x_parcelsIssuesId_pclaId  WHERE x.fmisUploadsId = :fmisUploadsId").
            setParameter("fmisUploadsId", fmisUploadsId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByFmisUploadsId(Integer fmisUploadsId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM FmisUpload x WHERE x.fmisUploadsId = :fmisUploadsId")
    .setParameter("fmisUploadsId", fmisUploadsId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<FmisUpload> findAllByCriteriaRange_ParcelFMISGrpFmisUpload(Integer parcelsIssuesId_parcelsIssuesId, Date fsch_dteUpload, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_ParcelFMISGrpFmisUpload(false, parcelsIssuesId_parcelsIssuesId, fsch_dteUpload, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<FmisUpload> findAllByCriteriaRange_ParcelFMISGrpFmisUpload(boolean noTransient, Integer parcelsIssuesId_parcelsIssuesId, Date fsch_dteUpload, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM FmisUpload x  left join fetch x.parcelsIssuesId x_parcelsIssuesId left join fetch x_parcelsIssuesId.pclaId x_parcelsIssuesId_pclaId WHERE ((1=1) AND (x.parcelsIssuesId.parcelsIssuesId = :parcelsIssuesId_parcelsIssuesId)) AND (x.dteUpload = :fsch_dteUpload) ORDER BY x.fmisUploadsId ";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("parcelsIssuesId_parcelsIssuesId", parcelsIssuesId_parcelsIssuesId);

            if (fsch_dteUpload == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_dteUpload");
            } else {
                paramSets.put("fsch_dteUpload", fsch_dteUpload);
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.fmisUploadsId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.fmisUploadsId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<FmisUpload> ret = (List<FmisUpload>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_ParcelFMISGrpFmisUpload", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_ParcelFMISGrpFmisUpload_count(Integer parcelsIssuesId_parcelsIssuesId, Date fsch_dteUpload, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM FmisUpload x WHERE ((1=1) AND (x.parcelsIssuesId.parcelsIssuesId = :parcelsIssuesId_parcelsIssuesId)) AND (x.dteUpload = :fsch_dteUpload) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("parcelsIssuesId_parcelsIssuesId", parcelsIssuesId_parcelsIssuesId);

            if (fsch_dteUpload == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_dteUpload");
            } else {
                paramSets.put("fsch_dteUpload", fsch_dteUpload);
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_ParcelFMISGrpFmisUpload_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
