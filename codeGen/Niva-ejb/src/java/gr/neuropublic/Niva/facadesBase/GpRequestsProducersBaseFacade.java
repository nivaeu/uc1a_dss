package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class GpRequestsProducersBaseFacade extends AbstractFacade<GpRequestsProducers> implements  IGpRequestsProducersBaseFacade, /*.ILocal, IGpRequestsProducersBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(GpRequestsProducersBaseFacade.class);

    public GpRequestsProducersBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public GpRequestsProducers initRow() {
        return new GpRequestsProducers(getNextSequenceValue());
    }

    public void getTransientFields(List<GpRequestsProducers> entities){
    }

    @Override
    public GpRequestsProducers createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.IGpRequestsFacade.ILocal gpRequestsFacade, gr.neuropublic.Niva.facades.IProducersFacade.ILocal producersFacade, Set<String> allowedFields) {
        GpRequestsProducers ret = null;
        if (!json.containsKey("gpRequestsProducersId") || json.get("gpRequestsProducersId") == null)
            throw new GenericApplicationException("Error in GpRequestsProducersBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("gpRequestsProducersId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("gpRequestsProducersId"));
                //ret = this.findByGpRequestsProducersId(id);
                ret = getEntityManager().find(GpRequestsProducers.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:GpRequestsProducers:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : GpRequestsProducers");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("notes") && allowedFields.contains("notes")) {
            ret.setNotes((String)json.get("notes"));
        }
        if (json.containsKey("gpRequestsId") && allowedFields.contains("gpRequestsId")) {
            JSONObject gpRequestsId = (JSONObject)json.get("gpRequestsId");
            if (gpRequestsId == null) {
                ret.setGpRequestsId(null);
            } else {
                if (!gpRequestsId.containsKey("gpRequestsId") || gpRequestsId.get("gpRequestsId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. gpRequestsId is not null but PK is missing or null");
                } 
                GpRequests gpRequestsId_db = null;
                String temp_id = (String)gpRequestsId.get("gpRequestsId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. gpRequestsId.gpRequestsId is negative but not in the keysMap dictionary");
                    }
                    gpRequestsId_db = (GpRequests)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)gpRequestsId.get("gpRequestsId"));
                    gpRequestsId_db = gpRequestsFacade.findByGpRequestsId(id);
                    if (gpRequestsId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity GpRequests with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setGpRequestsId(gpRequestsId_db);
            }
        }
        if (json.containsKey("producersId") && allowedFields.contains("producersId")) {
            JSONObject producersId = (JSONObject)json.get("producersId");
            if (producersId == null) {
                ret.setProducersId(null);
            } else {
                if (!producersId.containsKey("producersId") || producersId.get("producersId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. producersId is not null but PK is missing or null");
                } 
                Producers producersId_db = null;
                String temp_id = (String)producersId.get("producersId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. producersId.producersId is negative but not in the keysMap dictionary");
                    }
                    producersId_db = (Producers)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)producersId.get("producersId"));
                    producersId_db = producersFacade.findByProducersId(id);
                    if (producersId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity Producers with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setProducersId(producersId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<GpRequestsProducers> findAllByIds(List<Integer> ids) {

        List<GpRequestsProducers> ret = (List<GpRequestsProducers>) em.createQuery("SELECT x FROM GpRequestsProducers x   WHERE x.gpRequestsProducersId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM GpRequestsProducers x WHERE x.gpRequestsProducersId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public GpRequestsProducers findByGpRequestsId_ProducersId(GpRequests gpRequestsId, Producers producersId) {
        List<GpRequestsProducers> results = (List<GpRequestsProducers>) em.createQuery("SELECT x FROM GpRequestsProducers x   WHERE x.gpRequestsId.gpRequestsId = :gpRequestsId AND x.producersId.producersId = :producersId").
            setParameter("gpRequestsId", gpRequestsId!=null?gpRequestsId.getGpRequestsId():null).
            setParameter("producersId", producersId!=null?producersId.getProducersId():null).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public GpRequestsProducers findByGpRequestsId_ProducersId(Integer gpRequestsId, Integer producersId) {
        List<GpRequestsProducers> results = (List<GpRequestsProducers>) em.createQuery("SELECT x FROM GpRequestsProducers x   WHERE x.gpRequestsId.gpRequestsId = :gpRequestsId AND x.producersId.producersId = :producersId").
            setParameter("gpRequestsId", gpRequestsId).
            setParameter("producersId", producersId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByGpRequestsId_ProducersId(GpRequests gpRequestsId, Producers producersId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM GpRequestsProducers x WHERE x.gpRequestsId.gpRequestsId = :gpRequestsId AND x.producersId.producersId = :producersId")
    .setParameter("gpRequestsId", gpRequestsId!=null?gpRequestsId.getGpRequestsId():null).
            setParameter("producersId", producersId!=null?producersId.getProducersId():null);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    public int delByGpRequestsId_ProducersId(Integer gpRequestsId, Integer producersId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM GpRequestsProducers x WHERE x.gpRequestsId.gpRequestsId = :gpRequestsId AND x.producersId.producersId = :producersId")
    .setParameter("gpRequestsId", gpRequestsId).
            setParameter("producersId", producersId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public GpRequestsProducers findByGpRequestsProducersId(Integer gpRequestsProducersId) {
        List<GpRequestsProducers> results = (List<GpRequestsProducers>) em.createQuery("SELECT x FROM GpRequestsProducers x   WHERE x.gpRequestsProducersId = :gpRequestsProducersId").
            setParameter("gpRequestsProducersId", gpRequestsProducersId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByGpRequestsProducersId(Integer gpRequestsProducersId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM GpRequestsProducers x WHERE x.gpRequestsProducersId = :gpRequestsProducersId")
    .setParameter("gpRequestsProducersId", gpRequestsProducersId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<GpRequestsProducers> findAllByCriteriaRange_forLov(String fsch_GpRequestsProducersLov_notes, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_forLov(false, fsch_GpRequestsProducersLov_notes, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<GpRequestsProducers> findAllByCriteriaRange_forLov(boolean noTransient, String fsch_GpRequestsProducersLov_notes, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM GpRequestsProducers x   WHERE (1=1) AND (x.notes like :fsch_GpRequestsProducersLov_notes) ORDER BY x.notes, x.gpRequestsProducersId";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            if (fsch_GpRequestsProducersLov_notes == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_GpRequestsProducersLov_notes");
            } else {
                paramSets.put("fsch_GpRequestsProducersLov_notes", fsch_GpRequestsProducersLov_notes+"%");
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.gpRequestsProducersId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.gpRequestsProducersId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<GpRequestsProducers> ret = (List<GpRequestsProducers>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_forLov", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_forLov_count(String fsch_GpRequestsProducersLov_notes, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM GpRequestsProducers x WHERE (1=1) AND (x.notes like :fsch_GpRequestsProducersLov_notes) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            if (fsch_GpRequestsProducersLov_notes == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_GpRequestsProducersLov_notes");
            } else {
                paramSets.put("fsch_GpRequestsProducersLov_notes", fsch_GpRequestsProducersLov_notes+"%");
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_forLov_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
