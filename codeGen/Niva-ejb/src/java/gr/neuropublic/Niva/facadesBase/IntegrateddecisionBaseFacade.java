package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class IntegrateddecisionBaseFacade extends AbstractFacade<Integrateddecision> implements  IIntegrateddecisionBaseFacade, /*.ILocal, IIntegrateddecisionBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(IntegrateddecisionBaseFacade.class);

    public IntegrateddecisionBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Integrateddecision initRow() {
        return new Integrateddecision(getNextSequenceValue());
    }

    public void getTransientFields(List<Integrateddecision> entities){
    }

    @Override
    public Integrateddecision createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.IParcelClasFacade.ILocal parcelClasFacade, gr.neuropublic.Niva.facades.IDecisionMakingFacade.ILocal decisionMakingFacade, Set<String> allowedFields) {
        Integrateddecision ret = null;
        if (!json.containsKey("integrateddecisionsId") || json.get("integrateddecisionsId") == null)
            throw new GenericApplicationException("Error in IntegrateddecisionBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("integrateddecisionsId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("integrateddecisionsId"));
                //ret = this.findByIntegrateddecisionsId(id);
                ret = getEntityManager().find(Integrateddecision.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:Integrateddecision:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("decisionCode") && allowedFields.contains("decisionCode")) {
            Long decisionCode = (Long)json.get("decisionCode");
            ret.setDecisionCode(decisionCode != null ? decisionCode.shortValue() : null);
        }
        if (json.containsKey("dteUpdate") && allowedFields.contains("dteUpdate")) {
            String dteUpdate = (String)json.get("dteUpdate");
            try {
                Date jsonDate = gr.neuropublic.utils.DateUtil.parseISO8601Date(dteUpdate);
                Date entityDate = ret.getDteUpdate();
                if (jsonDate!= null && !jsonDate.equals(entityDate)) {
                    ret.setDteUpdate(jsonDate);
                } else {
                    if (jsonDate == null && entityDate != null) {
                        ret.setDteUpdate(jsonDate);
                    }
                }
            } catch (ParseException ex) {
                throw new GenericApplicationException(ex.getMessage());
            }
        }
        if (json.containsKey("usrUpdate") && allowedFields.contains("usrUpdate")) {
            ret.setUsrUpdate((String)json.get("usrUpdate"));
        }
        if (json.containsKey("pclaId") && allowedFields.contains("pclaId")) {
            JSONObject pclaId = (JSONObject)json.get("pclaId");
            if (pclaId == null) {
                ret.setPclaId(null);
            } else {
                if (!pclaId.containsKey("pclaId") || pclaId.get("pclaId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. pclaId is not null but PK is missing or null");
                } 
                ParcelClas pclaId_db = null;
                String temp_id = (String)pclaId.get("pclaId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. pclaId.pclaId is negative but not in the keysMap dictionary");
                    }
                    pclaId_db = (ParcelClas)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)pclaId.get("pclaId"));
                    pclaId_db = parcelClasFacade.findByPclaId(id);
                    if (pclaId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity ParcelClas with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setPclaId(pclaId_db);
            }
        }
        if (json.containsKey("demaId") && allowedFields.contains("demaId")) {
            JSONObject demaId = (JSONObject)json.get("demaId");
            if (demaId == null) {
                ret.setDemaId(null);
            } else {
                if (!demaId.containsKey("demaId") || demaId.get("demaId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. demaId is not null but PK is missing or null");
                } 
                DecisionMaking demaId_db = null;
                String temp_id = (String)demaId.get("demaId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. demaId.demaId is negative but not in the keysMap dictionary");
                    }
                    demaId_db = (DecisionMaking)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)demaId.get("demaId"));
                    demaId_db = decisionMakingFacade.findByDemaId(id);
                    if (demaId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity DecisionMaking with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setDemaId(demaId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Integrateddecision> findAllByIds(List<Integer> ids) {

        List<Integrateddecision> ret = (List<Integrateddecision>) em.createQuery("SELECT x FROM Integrateddecision x left join fetch x.demaId x_demaId left join fetch x.pclaId x_pclaId left join fetch x_pclaId.cultIdDecl x_pclaId_cultIdDecl left join fetch x_pclaId.cultIdPred x_pclaId_cultIdPred left join fetch x_pclaId.cultIdPred2 x_pclaId_cultIdPred2  WHERE x.integrateddecisionsId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM Integrateddecision x WHERE x.integrateddecisionsId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Integrateddecision findByPclaId_DemaId(ParcelClas pclaId, DecisionMaking demaId) {
        List<Integrateddecision> results = (List<Integrateddecision>) em.createQuery("SELECT x FROM Integrateddecision x left join fetch x.demaId x_demaId left join fetch x.pclaId x_pclaId left join fetch x_pclaId.cultIdDecl x_pclaId_cultIdDecl left join fetch x_pclaId.cultIdPred x_pclaId_cultIdPred left join fetch x_pclaId.cultIdPred2 x_pclaId_cultIdPred2  WHERE x.pclaId.pclaId = :pclaId AND x.demaId.demaId = :demaId").
            setParameter("pclaId", pclaId!=null?pclaId.getPclaId():null).
            setParameter("demaId", demaId!=null?demaId.getDemaId():null).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Integrateddecision findByPclaId_DemaId(Integer pclaId, Integer demaId) {
        List<Integrateddecision> results = (List<Integrateddecision>) em.createQuery("SELECT x FROM Integrateddecision x left join fetch x.demaId x_demaId left join fetch x.pclaId x_pclaId left join fetch x_pclaId.cultIdDecl x_pclaId_cultIdDecl left join fetch x_pclaId.cultIdPred x_pclaId_cultIdPred left join fetch x_pclaId.cultIdPred2 x_pclaId_cultIdPred2  WHERE x.pclaId.pclaId = :pclaId AND x.demaId.demaId = :demaId").
            setParameter("pclaId", pclaId).
            setParameter("demaId", demaId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByPclaId_DemaId(ParcelClas pclaId, DecisionMaking demaId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM Integrateddecision x WHERE x.pclaId.pclaId = :pclaId AND x.demaId.demaId = :demaId")
    .setParameter("pclaId", pclaId!=null?pclaId.getPclaId():null).
            setParameter("demaId", demaId!=null?demaId.getDemaId():null);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    public int delByPclaId_DemaId(Integer pclaId, Integer demaId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM Integrateddecision x WHERE x.pclaId.pclaId = :pclaId AND x.demaId.demaId = :demaId")
    .setParameter("pclaId", pclaId).
            setParameter("demaId", demaId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Integrateddecision findByIntegrateddecisionsId(Integer integrateddecisionsId) {
        List<Integrateddecision> results = (List<Integrateddecision>) em.createQuery("SELECT x FROM Integrateddecision x left join fetch x.demaId x_demaId left join fetch x.pclaId x_pclaId left join fetch x_pclaId.cultIdDecl x_pclaId_cultIdDecl left join fetch x_pclaId.cultIdPred x_pclaId_cultIdPred left join fetch x_pclaId.cultIdPred2 x_pclaId_cultIdPred2  WHERE x.integrateddecisionsId = :integrateddecisionsId").
            setParameter("integrateddecisionsId", integrateddecisionsId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByIntegrateddecisionsId(Integer integrateddecisionsId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM Integrateddecision x WHERE x.integrateddecisionsId = :integrateddecisionsId")
    .setParameter("integrateddecisionsId", integrateddecisionsId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Integrateddecision> findLazyIntegrateddecision(Short decisionCode, Date dteUpdate, String usrUpdate, Integer pclaId_pclaId, Integer demaId_demaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findLazyIntegrateddecision(false, decisionCode, dteUpdate, usrUpdate, pclaId_pclaId, demaId_demaId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Integrateddecision> findLazyIntegrateddecision(boolean noTransient, Short decisionCode, Date dteUpdate, String usrUpdate, Integer pclaId_pclaId, Integer demaId_demaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM Integrateddecision x  left join fetch x.demaId x_demaId left join fetch x.pclaId x_pclaId left join fetch x_pclaId.cultIdDecl x_pclaId_cultIdDecl left join fetch x_pclaId.cultIdPred x_pclaId_cultIdPred left join fetch x_pclaId.cultIdPred2 x_pclaId_cultIdPred2 WHERE (x.decisionCode = :decisionCode) AND (x.dteUpdate = :dteUpdate) AND (x.usrUpdate like :usrUpdate) AND (x.pclaId.pclaId = :pclaId_pclaId) AND (x.demaId.demaId = :demaId_demaId)";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            if (decisionCode == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":decisionCode");
            } else {
                paramSets.put("decisionCode", decisionCode);
            }

            if (dteUpdate == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":dteUpdate");
            } else {
                paramSets.put("dteUpdate", dteUpdate);
            }

            if (usrUpdate == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":usrUpdate");
            } else {
                paramSets.put("usrUpdate", usrUpdate+"%");
            }

            if (pclaId_pclaId == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":pclaId_pclaId");
            } else {
                paramSets.put("pclaId_pclaId", pclaId_pclaId);
            }

            if (demaId_demaId == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":demaId_demaId");
            } else {
                paramSets.put("demaId_demaId", demaId_demaId);
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.integrateddecisionsId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.integrateddecisionsId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<Integrateddecision> ret = (List<Integrateddecision>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findLazyIntegrateddecision", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findLazyIntegrateddecision_count(Short decisionCode, Date dteUpdate, String usrUpdate, Integer pclaId_pclaId, Integer demaId_demaId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM Integrateddecision x WHERE (x.decisionCode = :decisionCode) AND (x.dteUpdate = :dteUpdate) AND (x.usrUpdate like :usrUpdate) AND (x.pclaId.pclaId = :pclaId_pclaId) AND (x.demaId.demaId = :demaId_demaId)";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            if (decisionCode == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":decisionCode");
            } else {
                paramSets.put("decisionCode", decisionCode);
            }

            if (dteUpdate == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":dteUpdate");
            } else {
                paramSets.put("dteUpdate", dteUpdate);
            }

            if (usrUpdate == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":usrUpdate");
            } else {
                paramSets.put("usrUpdate", usrUpdate+"%");
            }

            if (pclaId_pclaId == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":pclaId_pclaId");
            } else {
                paramSets.put("pclaId_pclaId", pclaId_pclaId);
            }

            if (demaId_demaId == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":demaId_demaId");
            } else {
                paramSets.put("demaId_demaId", demaId_demaId);
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findLazyIntegrateddecision_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Integrateddecision> findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision(Integer demaId_demaId, Short fsch_decisionCode, Integer fsch_prodCode, String fsch_Code, String fsch_Parcel_Identifier, Integer fsch_CultDeclCode_cultId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision(false, demaId_demaId, fsch_decisionCode, fsch_prodCode, fsch_Code, fsch_Parcel_Identifier, fsch_CultDeclCode_cultId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Integrateddecision> findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision(boolean noTransient, Integer demaId_demaId, Short fsch_decisionCode, Integer fsch_prodCode, String fsch_Code, String fsch_Parcel_Identifier, Integer fsch_CultDeclCode_cultId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM Integrateddecision x  left join fetch x.demaId x_demaId left join fetch x.pclaId x_pclaId left join fetch x_pclaId.cultIdDecl x_pclaId_cultIdDecl left join fetch x_pclaId.cultIdPred x_pclaId_cultIdPred left join fetch x_pclaId.cultIdPred2 x_pclaId_cultIdPred2 WHERE ((1=1) AND (x.demaId.demaId = :demaId_demaId)) AND (x.decisionCode = :fsch_decisionCode) AND (x.pclaId.prodCode = :fsch_prodCode) AND (x.pclaId.parcCode like :fsch_Code) AND (x.pclaId.parcIdentifier like :fsch_Parcel_Identifier) AND (x.pclaId.cultIdDecl.code = :fsch_CultDeclCode_cultId) ORDER BY x.dteUpdate, x.pclaId.parcIdentifier ";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("demaId_demaId", demaId_demaId);

            if (fsch_decisionCode == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_decisionCode");
            } else {
                paramSets.put("fsch_decisionCode", fsch_decisionCode);
            }

            if (fsch_prodCode == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_prodCode");
            } else {
                paramSets.put("fsch_prodCode", fsch_prodCode);
            }

            if (fsch_Code == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_Code");
            } else {
                paramSets.put("fsch_Code", fsch_Code+"%");
            }

            if (fsch_Parcel_Identifier == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_Parcel_Identifier");
            } else {
                paramSets.put("fsch_Parcel_Identifier", fsch_Parcel_Identifier+"%");
            }

            if (fsch_CultDeclCode_cultId == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_CultDeclCode_cultId");
            } else {
                paramSets.put("fsch_CultDeclCode_cultId", fsch_CultDeclCode_cultId);
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.integrateddecisionsId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.integrateddecisionsId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<Integrateddecision> ret = (List<Integrateddecision>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision_count(Integer demaId_demaId, Short fsch_decisionCode, Integer fsch_prodCode, String fsch_Code, String fsch_Parcel_Identifier, Integer fsch_CultDeclCode_cultId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM Integrateddecision x WHERE ((1=1) AND (x.demaId.demaId = :demaId_demaId)) AND (x.decisionCode = :fsch_decisionCode) AND (x.pclaId.prodCode = :fsch_prodCode) AND (x.pclaId.parcCode like :fsch_Code) AND (x.pclaId.parcIdentifier like :fsch_Parcel_Identifier) AND (x.pclaId.cultIdDecl.code = :fsch_CultDeclCode_cultId) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("demaId_demaId", demaId_demaId);

            if (fsch_decisionCode == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_decisionCode");
            } else {
                paramSets.put("fsch_decisionCode", fsch_decisionCode);
            }

            if (fsch_prodCode == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_prodCode");
            } else {
                paramSets.put("fsch_prodCode", fsch_prodCode);
            }

            if (fsch_Code == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_Code");
            } else {
                paramSets.put("fsch_Code", fsch_Code+"%");
            }

            if (fsch_Parcel_Identifier == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_Parcel_Identifier");
            } else {
                paramSets.put("fsch_Parcel_Identifier", fsch_Parcel_Identifier+"%");
            }

            if (fsch_CultDeclCode_cultId == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_CultDeclCode_cultId");
            } else {
                paramSets.put("fsch_CultDeclCode_cultId", fsch_CultDeclCode_cultId);
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Integrateddecision> findAllByCriteriaRange_DashboardGrpIntegrateddecision(Integer pclaId_pclaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_DashboardGrpIntegrateddecision(false, pclaId_pclaId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Integrateddecision> findAllByCriteriaRange_DashboardGrpIntegrateddecision(boolean noTransient, Integer pclaId_pclaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM Integrateddecision x  left join fetch x.demaId x_demaId left join fetch x.pclaId x_pclaId left join fetch x_pclaId.cultIdDecl x_pclaId_cultIdDecl left join fetch x_pclaId.cultIdPred x_pclaId_cultIdPred left join fetch x_pclaId.cultIdPred2 x_pclaId_cultIdPred2 WHERE ((1=1) AND (x.pclaId.pclaId = :pclaId_pclaId)) ORDER BY x.decisionCode";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("pclaId_pclaId", pclaId_pclaId);

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.integrateddecisionsId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.integrateddecisionsId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<Integrateddecision> ret = (List<Integrateddecision>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DashboardGrpIntegrateddecision", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_DashboardGrpIntegrateddecision_count(Integer pclaId_pclaId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x) FROM Integrateddecision x WHERE ((1=1) AND (x.pclaId.pclaId = :pclaId_pclaId)) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("pclaId_pclaId", pclaId_pclaId);


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DashboardGrpIntegrateddecision_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
