package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class AgrisnapUsersProducersBaseFacade extends AbstractFacade<AgrisnapUsersProducers> implements  IAgrisnapUsersProducersBaseFacade, /*.ILocal, IAgrisnapUsersProducersBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(AgrisnapUsersProducersBaseFacade.class);

    public AgrisnapUsersProducersBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public AgrisnapUsersProducers initRow() {
        return new AgrisnapUsersProducers(getNextSequenceValue());
    }

    public void getTransientFields(List<AgrisnapUsersProducers> entities){
    }

    @Override
    public AgrisnapUsersProducers createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.IAgrisnapUsersFacade.ILocal agrisnapUsersFacade, gr.neuropublic.Niva.facades.IProducersFacade.ILocal producersFacade, Set<String> allowedFields) {
        AgrisnapUsersProducers ret = null;
        if (!json.containsKey("agrisnapUsersProducersId") || json.get("agrisnapUsersProducersId") == null)
            throw new GenericApplicationException("Error in AgrisnapUsersProducersBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("agrisnapUsersProducersId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("agrisnapUsersProducersId"));
                //ret = this.findByAgrisnapUsersProducersId(id);
                ret = getEntityManager().find(AgrisnapUsersProducers.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:AgrisnapUsersProducers:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("dteRelCreated") && allowedFields.contains("dteRelCreated")) {
            String dteRelCreated = (String)json.get("dteRelCreated");
            try {
                Date jsonDate = gr.neuropublic.utils.DateUtil.parseISO8601Date(dteRelCreated);
                Date entityDate = ret.getDteRelCreated();
                if (jsonDate!= null && !jsonDate.equals(entityDate)) {
                    ret.setDteRelCreated(jsonDate);
                } else {
                    if (jsonDate == null && entityDate != null) {
                        ret.setDteRelCreated(jsonDate);
                    }
                }
            } catch (ParseException ex) {
                throw new GenericApplicationException(ex.getMessage());
            }
        }
        if (json.containsKey("dteRelCanceled") && allowedFields.contains("dteRelCanceled")) {
            String dteRelCanceled = (String)json.get("dteRelCanceled");
            try {
                Date jsonDate = gr.neuropublic.utils.DateUtil.parseISO8601Date(dteRelCanceled);
                Date entityDate = ret.getDteRelCanceled();
                if (jsonDate!= null && !jsonDate.equals(entityDate)) {
                    ret.setDteRelCanceled(jsonDate);
                } else {
                    if (jsonDate == null && entityDate != null) {
                        ret.setDteRelCanceled(jsonDate);
                    }
                }
            } catch (ParseException ex) {
                throw new GenericApplicationException(ex.getMessage());
            }
        }
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : AgrisnapUsersProducers");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("agrisnapUsersId") && allowedFields.contains("agrisnapUsersId")) {
            JSONObject agrisnapUsersId = (JSONObject)json.get("agrisnapUsersId");
            if (agrisnapUsersId == null) {
                ret.setAgrisnapUsersId(null);
            } else {
                if (!agrisnapUsersId.containsKey("agrisnapUsersId") || agrisnapUsersId.get("agrisnapUsersId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. agrisnapUsersId is not null but PK is missing or null");
                } 
                AgrisnapUsers agrisnapUsersId_db = null;
                String temp_id = (String)agrisnapUsersId.get("agrisnapUsersId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. agrisnapUsersId.agrisnapUsersId is negative but not in the keysMap dictionary");
                    }
                    agrisnapUsersId_db = (AgrisnapUsers)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)agrisnapUsersId.get("agrisnapUsersId"));
                    agrisnapUsersId_db = agrisnapUsersFacade.findByAgrisnapUsersId(id);
                    if (agrisnapUsersId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity AgrisnapUsers with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setAgrisnapUsersId(agrisnapUsersId_db);
            }
        }
        if (json.containsKey("producersId") && allowedFields.contains("producersId")) {
            JSONObject producersId = (JSONObject)json.get("producersId");
            if (producersId == null) {
                ret.setProducersId(null);
            } else {
                if (!producersId.containsKey("producersId") || producersId.get("producersId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. producersId is not null but PK is missing or null");
                } 
                Producers producersId_db = null;
                String temp_id = (String)producersId.get("producersId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. producersId.producersId is negative but not in the keysMap dictionary");
                    }
                    producersId_db = (Producers)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)producersId.get("producersId"));
                    producersId_db = producersFacade.findByProducersId(id);
                    if (producersId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity Producers with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setProducersId(producersId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<AgrisnapUsersProducers> findAllByIds(List<Integer> ids) {

        List<AgrisnapUsersProducers> ret = (List<AgrisnapUsersProducers>) em.createQuery("SELECT x FROM AgrisnapUsersProducers x left join fetch x.agrisnapUsersId x_agrisnapUsersId left join fetch x.producersId x_producersId  WHERE x.agrisnapUsersProducersId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM AgrisnapUsersProducers x WHERE x.agrisnapUsersProducersId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public AgrisnapUsersProducers findByAgrisnapUsersId_AgrisnapUsersProducersId(AgrisnapUsers agrisnapUsersId, Integer agrisnapUsersProducersId) {
        List<AgrisnapUsersProducers> results = (List<AgrisnapUsersProducers>) em.createQuery("SELECT x FROM AgrisnapUsersProducers x left join fetch x.agrisnapUsersId x_agrisnapUsersId left join fetch x.producersId x_producersId  WHERE x.agrisnapUsersId.agrisnapUsersId = :agrisnapUsersId AND x.agrisnapUsersProducersId = :agrisnapUsersProducersId").
            setParameter("agrisnapUsersId", agrisnapUsersId!=null?agrisnapUsersId.getAgrisnapUsersId():null).
            setParameter("agrisnapUsersProducersId", agrisnapUsersProducersId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public AgrisnapUsersProducers findByAgrisnapUsersId_AgrisnapUsersProducersId(Integer agrisnapUsersId, Integer agrisnapUsersProducersId) {
        List<AgrisnapUsersProducers> results = (List<AgrisnapUsersProducers>) em.createQuery("SELECT x FROM AgrisnapUsersProducers x left join fetch x.agrisnapUsersId x_agrisnapUsersId left join fetch x.producersId x_producersId  WHERE x.agrisnapUsersId.agrisnapUsersId = :agrisnapUsersId AND x.agrisnapUsersProducersId = :agrisnapUsersProducersId").
            setParameter("agrisnapUsersId", agrisnapUsersId).
            setParameter("agrisnapUsersProducersId", agrisnapUsersProducersId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByAgrisnapUsersId_AgrisnapUsersProducersId(AgrisnapUsers agrisnapUsersId, Integer agrisnapUsersProducersId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM AgrisnapUsersProducers x WHERE x.agrisnapUsersId.agrisnapUsersId = :agrisnapUsersId AND x.agrisnapUsersProducersId = :agrisnapUsersProducersId")
    .setParameter("agrisnapUsersId", agrisnapUsersId!=null?agrisnapUsersId.getAgrisnapUsersId():null).
            setParameter("agrisnapUsersProducersId", agrisnapUsersProducersId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    public int delByAgrisnapUsersId_AgrisnapUsersProducersId(Integer agrisnapUsersId, Integer agrisnapUsersProducersId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM AgrisnapUsersProducers x WHERE x.agrisnapUsersId.agrisnapUsersId = :agrisnapUsersId AND x.agrisnapUsersProducersId = :agrisnapUsersProducersId")
    .setParameter("agrisnapUsersId", agrisnapUsersId).
            setParameter("agrisnapUsersProducersId", agrisnapUsersProducersId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public AgrisnapUsersProducers findByAgrisnapUsersProducersId(Integer agrisnapUsersProducersId) {
        List<AgrisnapUsersProducers> results = (List<AgrisnapUsersProducers>) em.createQuery("SELECT x FROM AgrisnapUsersProducers x left join fetch x.agrisnapUsersId x_agrisnapUsersId left join fetch x.producersId x_producersId  WHERE x.agrisnapUsersProducersId = :agrisnapUsersProducersId").
            setParameter("agrisnapUsersProducersId", agrisnapUsersProducersId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByAgrisnapUsersProducersId(Integer agrisnapUsersProducersId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM AgrisnapUsersProducers x WHERE x.agrisnapUsersProducersId = :agrisnapUsersProducersId")
    .setParameter("agrisnapUsersProducersId", agrisnapUsersProducersId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<AgrisnapUsersProducers> findAllByCriteriaRange_AgrisnapUsersGrpAgrisnapUsersProducers(Integer agrisnapUsersId_agrisnapUsersId, Date fsch_dteRelCreated, Date fsch_dteRelCanceled, Integer fsch_producersId_producersId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_AgrisnapUsersGrpAgrisnapUsersProducers(false, agrisnapUsersId_agrisnapUsersId, fsch_dteRelCreated, fsch_dteRelCanceled, fsch_producersId_producersId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<AgrisnapUsersProducers> findAllByCriteriaRange_AgrisnapUsersGrpAgrisnapUsersProducers(boolean noTransient, Integer agrisnapUsersId_agrisnapUsersId, Date fsch_dteRelCreated, Date fsch_dteRelCanceled, Integer fsch_producersId_producersId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM AgrisnapUsersProducers x  left join fetch x.agrisnapUsersId x_agrisnapUsersId left join fetch x.producersId x_producersId WHERE ((1=1) AND (x.agrisnapUsersId.agrisnapUsersId = :agrisnapUsersId_agrisnapUsersId)) AND (x.dteRelCreated = :fsch_dteRelCreated) AND (x.dteRelCanceled = :fsch_dteRelCanceled) AND (x.producersId.producersId = :fsch_producersId_producersId) ORDER BY x.producersId.producersId";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("agrisnapUsersId_agrisnapUsersId", agrisnapUsersId_agrisnapUsersId);

            if (fsch_dteRelCreated == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_dteRelCreated");
            } else {
                paramSets.put("fsch_dteRelCreated", fsch_dteRelCreated);
            }

            if (fsch_dteRelCanceled == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_dteRelCanceled");
            } else {
                paramSets.put("fsch_dteRelCanceled", fsch_dteRelCanceled);
            }

            if (fsch_producersId_producersId == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_producersId_producersId");
            } else {
                paramSets.put("fsch_producersId_producersId", fsch_producersId_producersId);
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.agrisnapUsersProducersId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.agrisnapUsersProducersId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<AgrisnapUsersProducers> ret = (List<AgrisnapUsersProducers>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_AgrisnapUsersGrpAgrisnapUsersProducers", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_AgrisnapUsersGrpAgrisnapUsersProducers_count(Integer agrisnapUsersId_agrisnapUsersId, Date fsch_dteRelCreated, Date fsch_dteRelCanceled, Integer fsch_producersId_producersId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM AgrisnapUsersProducers x WHERE ((1=1) AND (x.agrisnapUsersId.agrisnapUsersId = :agrisnapUsersId_agrisnapUsersId)) AND (x.dteRelCreated = :fsch_dteRelCreated) AND (x.dteRelCanceled = :fsch_dteRelCanceled) AND (x.producersId.producersId = :fsch_producersId_producersId) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("agrisnapUsersId_agrisnapUsersId", agrisnapUsersId_agrisnapUsersId);

            if (fsch_dteRelCreated == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_dteRelCreated");
            } else {
                paramSets.put("fsch_dteRelCreated", fsch_dteRelCreated);
            }

            if (fsch_dteRelCanceled == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_dteRelCanceled");
            } else {
                paramSets.put("fsch_dteRelCanceled", fsch_dteRelCanceled);
            }

            if (fsch_producersId_producersId == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_producersId_producersId");
            } else {
                paramSets.put("fsch_producersId_producersId", fsch_producersId_producersId);
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_AgrisnapUsersGrpAgrisnapUsersProducers_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
