package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class FileTemplateBaseFacade extends AbstractFacade<FileTemplate> implements  IFileTemplateBaseFacade, /*.ILocal, IFileTemplateBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(FileTemplateBaseFacade.class);

    public FileTemplateBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public FileTemplate initRow() {
        return new FileTemplate(getNextSequenceValue());
    }

    public void getTransientFields(List<FileTemplate> entities){
    }

    @Override
    public FileTemplate createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, Set<String> allowedFields) {
        FileTemplate ret = null;
        if (!json.containsKey("fiteId") || json.get("fiteId") == null)
            throw new GenericApplicationException("Error in FileTemplateBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("fiteId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("fiteId"));
                //ret = this.findByFiteId(id);
                ret = getEntityManager().find(FileTemplate.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:FileTemplate:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("name") && allowedFields.contains("name")) {
            ret.setName((String)json.get("name"));
        }
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : FileTemplate");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<FileTemplate> findAllByIds(List<Integer> ids) {

        List<FileTemplate> ret = (List<FileTemplate>) em.createQuery("SELECT x FROM FileTemplate x   WHERE x.fiteId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM FileTemplate x WHERE x.fiteId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public FileTemplate findByName(String name) {
        List<FileTemplate> results = (List<FileTemplate>) em.createQuery("SELECT x FROM FileTemplate x   WHERE x.name = :name").
            setParameter("name", name).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByName(String name) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM FileTemplate x WHERE x.name = :name")
    .setParameter("name", name);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public FileTemplate findByFiteId(Integer fiteId) {
        List<FileTemplate> results = (List<FileTemplate>) em.createQuery("SELECT x FROM FileTemplate x   WHERE x.fiteId = :fiteId").
            setParameter("fiteId", fiteId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByFiteId(Integer fiteId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM FileTemplate x WHERE x.fiteId = :fiteId")
    .setParameter("fiteId", fiteId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<FileTemplate> findLazyFileTemplate(String name, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findLazyFileTemplate(false, name, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<FileTemplate> findLazyFileTemplate(boolean noTransient, String name, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM FileTemplate x   WHERE (x.name like :name)";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            if (name == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":name");
            } else {
                paramSets.put("name", name+"%");
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.fiteId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.fiteId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<FileTemplate> ret = (List<FileTemplate>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findLazyFileTemplate", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findLazyFileTemplate_count(String name, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM FileTemplate x WHERE (x.name like :name)";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            if (name == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":name");
            } else {
                paramSets.put("name", name+"%");
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findLazyFileTemplate_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<FileTemplate> findAllByCriteriaRange_forLov(String fsch_FileTemplateLov_name, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_forLov(false, fsch_FileTemplateLov_name, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<FileTemplate> findAllByCriteriaRange_forLov(boolean noTransient, String fsch_FileTemplateLov_name, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM FileTemplate x   WHERE (1=1) AND (x.name like :fsch_FileTemplateLov_name) ORDER BY x.name, x.fiteId";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            if (fsch_FileTemplateLov_name == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_FileTemplateLov_name");
            } else {
                paramSets.put("fsch_FileTemplateLov_name", fsch_FileTemplateLov_name+"%");
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.fiteId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.fiteId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<FileTemplate> ret = (List<FileTemplate>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_forLov", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_forLov_count(String fsch_FileTemplateLov_name, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM FileTemplate x WHERE (1=1) AND (x.name like :fsch_FileTemplateLov_name) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            if (fsch_FileTemplateLov_name == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_FileTemplateLov_name");
            } else {
                paramSets.put("fsch_FileTemplateLov_name", fsch_FileTemplateLov_name+"%");
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_forLov_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
