package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class EcCultBaseFacade extends AbstractFacade<EcCult> implements  IEcCultBaseFacade, /*.ILocal, IEcCultBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(EcCultBaseFacade.class);

    public EcCultBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public EcCult initRow() {
        return new EcCult(getNextSequenceValue());
    }

    public void getTransientFields(List<EcCult> entities){
    }

    @Override
    public EcCult createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.ICultivationFacade.ILocal cultivationFacade, gr.neuropublic.Niva.facades.IEcGroupFacade.ILocal ecGroupFacade, gr.neuropublic.Niva.facades.ICoverTypeFacade.ILocal coverTypeFacade, gr.neuropublic.Niva.facades.ISuperClasFacade.ILocal superClasFacade, Set<String> allowedFields) {
        EcCult ret = null;
        if (!json.containsKey("eccuId") || json.get("eccuId") == null)
            throw new GenericApplicationException("Error in EcCultBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("eccuId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("eccuId"));
                //ret = this.findByEccuId(id);
                ret = getEntityManager().find(EcCult.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:EcCult:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("noneMatchDecision") && allowedFields.contains("noneMatchDecision")) {
            Long noneMatchDecision = (Long)json.get("noneMatchDecision");
            ret.setNoneMatchDecision(noneMatchDecision != null ? noneMatchDecision.intValue() : null);
        }
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : EcCult");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("cultId") && allowedFields.contains("cultId")) {
            JSONObject cultId = (JSONObject)json.get("cultId");
            if (cultId == null) {
                ret.setCultId(null);
            } else {
                if (!cultId.containsKey("cultId") || cultId.get("cultId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. cultId is not null but PK is missing or null");
                } 
                Cultivation cultId_db = null;
                String temp_id = (String)cultId.get("cultId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. cultId.cultId is negative but not in the keysMap dictionary");
                    }
                    cultId_db = (Cultivation)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)cultId.get("cultId"));
                    cultId_db = cultivationFacade.findByCultId(id);
                    if (cultId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity Cultivation with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setCultId(cultId_db);
            }
        }
        if (json.containsKey("ecgrId") && allowedFields.contains("ecgrId")) {
            JSONObject ecgrId = (JSONObject)json.get("ecgrId");
            if (ecgrId == null) {
                ret.setEcgrId(null);
            } else {
                if (!ecgrId.containsKey("ecgrId") || ecgrId.get("ecgrId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. ecgrId is not null but PK is missing or null");
                } 
                EcGroup ecgrId_db = null;
                String temp_id = (String)ecgrId.get("ecgrId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. ecgrId.ecgrId is negative but not in the keysMap dictionary");
                    }
                    ecgrId_db = (EcGroup)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)ecgrId.get("ecgrId"));
                    ecgrId_db = ecGroupFacade.findByEcgrId(id);
                    if (ecgrId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity EcGroup with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setEcgrId(ecgrId_db);
            }
        }
        if (json.containsKey("cotyId") && allowedFields.contains("cotyId")) {
            JSONObject cotyId = (JSONObject)json.get("cotyId");
            if (cotyId == null) {
                ret.setCotyId(null);
            } else {
                if (!cotyId.containsKey("cotyId") || cotyId.get("cotyId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. cotyId is not null but PK is missing or null");
                } 
                CoverType cotyId_db = null;
                String temp_id = (String)cotyId.get("cotyId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. cotyId.cotyId is negative but not in the keysMap dictionary");
                    }
                    cotyId_db = (CoverType)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)cotyId.get("cotyId"));
                    cotyId_db = coverTypeFacade.findByCotyId(id);
                    if (cotyId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity CoverType with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setCotyId(cotyId_db);
            }
        }
        if (json.containsKey("sucaId") && allowedFields.contains("sucaId")) {
            JSONObject sucaId = (JSONObject)json.get("sucaId");
            if (sucaId == null) {
                ret.setSucaId(null);
            } else {
                if (!sucaId.containsKey("sucaId") || sucaId.get("sucaId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. sucaId is not null but PK is missing or null");
                } 
                SuperClas sucaId_db = null;
                String temp_id = (String)sucaId.get("sucaId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. sucaId.sucaId is negative but not in the keysMap dictionary");
                    }
                    sucaId_db = (SuperClas)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)sucaId.get("sucaId"));
                    sucaId_db = superClasFacade.findBySucaId(id);
                    if (sucaId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity SuperClas with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setSucaId(sucaId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCult> findAllByIds(List<Integer> ids) {

        List<EcCult> ret = (List<EcCult>) em.createQuery("SELECT x FROM EcCult x left join fetch x.cotyId x_cotyId left join fetch x.cultId x_cultId left join fetch x.ecgrId x_ecgrId left join fetch x_cultId.cotyId x_cultId_cotyId  WHERE x.eccuId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM EcCult x WHERE x.eccuId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public EcCult findByEccuId(Integer eccuId) {
        List<EcCult> results = (List<EcCult>) em.createQuery("SELECT x FROM EcCult x left join fetch x.cotyId x_cotyId left join fetch x.cultId x_cultId left join fetch x.ecgrId x_ecgrId left join fetch x_cultId.cotyId x_cultId_cotyId  WHERE x.eccuId = :eccuId").
            setParameter("eccuId", eccuId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByEccuId(Integer eccuId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM EcCult x WHERE x.eccuId = :eccuId")
    .setParameter("eccuId", eccuId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCult> findAllByCriteriaRange_EcGroupGrpEcCult(Integer ecgrId_ecgrId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_EcGroupGrpEcCult(false, ecgrId_ecgrId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCult> findAllByCriteriaRange_EcGroupGrpEcCult(boolean noTransient, Integer ecgrId_ecgrId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM EcCult x  left join fetch x.cotyId x_cotyId left join fetch x.cultId x_cultId left join fetch x.ecgrId x_ecgrId left join fetch x_cultId.cotyId x_cultId_cotyId WHERE ((1=1) AND (x.ecgrId.ecgrId = :ecgrId_ecgrId)) ORDER BY x.noneMatchDecision";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("ecgrId_ecgrId", ecgrId_ecgrId);

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.eccuId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.eccuId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<EcCult> ret = (List<EcCult>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_EcGroupGrpEcCult", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_EcGroupGrpEcCult_count(Integer ecgrId_ecgrId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x) FROM EcCult x WHERE ((1=1) AND (x.ecgrId.ecgrId = :ecgrId_ecgrId)) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("ecgrId_ecgrId", ecgrId_ecgrId);


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_EcGroupGrpEcCult_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCult> findAllByCriteriaRange_EcGroupEcCultCover(Integer ecgrId_ecgrId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_EcGroupEcCultCover(false, ecgrId_ecgrId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCult> findAllByCriteriaRange_EcGroupEcCultCover(boolean noTransient, Integer ecgrId_ecgrId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM EcCult x  left join fetch x.cotyId x_cotyId left join fetch x.cultId x_cultId left join fetch x.ecgrId x_ecgrId left join fetch x_cultId.cotyId x_cultId_cotyId WHERE ((1=1) AND (x.ecgrId.ecgrId = :ecgrId_ecgrId)) ORDER BY x.noneMatchDecision";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("ecgrId_ecgrId", ecgrId_ecgrId);

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.eccuId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.eccuId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<EcCult> ret = (List<EcCult>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_EcGroupEcCultCover", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_EcGroupEcCultCover_count(Integer ecgrId_ecgrId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x) FROM EcCult x WHERE ((1=1) AND (x.ecgrId.ecgrId = :ecgrId_ecgrId)) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("ecgrId_ecgrId", ecgrId_ecgrId);


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_EcGroupEcCultCover_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCult> findAllByCriteriaRange_EcGroupEcCultSuper(Integer ecgrId_ecgrId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_EcGroupEcCultSuper(false, ecgrId_ecgrId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<EcCult> findAllByCriteriaRange_EcGroupEcCultSuper(boolean noTransient, Integer ecgrId_ecgrId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM EcCult x  left join fetch x.cotyId x_cotyId left join fetch x.cultId x_cultId left join fetch x.ecgrId x_ecgrId left join fetch x_cultId.cotyId x_cultId_cotyId WHERE ((1=1) AND (x.ecgrId.ecgrId = :ecgrId_ecgrId)) ORDER BY x.noneMatchDecision";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("ecgrId_ecgrId", ecgrId_ecgrId);

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.eccuId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.eccuId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<EcCult> ret = (List<EcCult>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_EcGroupEcCultSuper", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_EcGroupEcCultSuper_count(Integer ecgrId_ecgrId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x) FROM EcCult x WHERE ((1=1) AND (x.ecgrId.ecgrId = :ecgrId_ecgrId)) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("ecgrId_ecgrId", ecgrId_ecgrId);


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_EcGroupEcCultSuper_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
