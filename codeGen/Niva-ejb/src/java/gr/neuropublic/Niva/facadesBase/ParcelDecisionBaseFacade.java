package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class ParcelDecisionBaseFacade extends AbstractFacade<ParcelDecision> implements  IParcelDecisionBaseFacade, /*.ILocal, IParcelDecisionBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(ParcelDecisionBaseFacade.class);

    public ParcelDecisionBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public ParcelDecision initRow() {
        return new ParcelDecision(getNextSequenceValue());
    }

    public void getTransientFields(List<ParcelDecision> entities){
    }

    @Override
    public ParcelDecision createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.IDecisionMakingFacade.ILocal decisionMakingFacade, gr.neuropublic.Niva.facades.IParcelClasFacade.ILocal parcelClasFacade, Set<String> allowedFields) {
        ParcelDecision ret = null;
        if (!json.containsKey("padeId") || json.get("padeId") == null)
            throw new GenericApplicationException("Error in ParcelDecisionBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("padeId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("padeId"));
                //ret = this.findByPadeId(id);
                ret = getEntityManager().find(ParcelDecision.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:ParcelDecision:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("decisionLight") && allowedFields.contains("decisionLight")) {
            Long decisionLight = (Long)json.get("decisionLight");
            ret.setDecisionLight(decisionLight != null ? decisionLight.intValue() : null);
        }
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : ParcelDecision");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("demaId") && allowedFields.contains("demaId")) {
            JSONObject demaId = (JSONObject)json.get("demaId");
            if (demaId == null) {
                ret.setDemaId(null);
            } else {
                if (!demaId.containsKey("demaId") || demaId.get("demaId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. demaId is not null but PK is missing or null");
                } 
                DecisionMaking demaId_db = null;
                String temp_id = (String)demaId.get("demaId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. demaId.demaId is negative but not in the keysMap dictionary");
                    }
                    demaId_db = (DecisionMaking)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)demaId.get("demaId"));
                    demaId_db = decisionMakingFacade.findByDemaId(id);
                    if (demaId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity DecisionMaking with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setDemaId(demaId_db);
            }
        }
        if (json.containsKey("pclaId") && allowedFields.contains("pclaId")) {
            JSONObject pclaId = (JSONObject)json.get("pclaId");
            if (pclaId == null) {
                ret.setPclaId(null);
            } else {
                if (!pclaId.containsKey("pclaId") || pclaId.get("pclaId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. pclaId is not null but PK is missing or null");
                } 
                ParcelClas pclaId_db = null;
                String temp_id = (String)pclaId.get("pclaId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. pclaId.pclaId is negative but not in the keysMap dictionary");
                    }
                    pclaId_db = (ParcelClas)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)pclaId.get("pclaId"));
                    pclaId_db = parcelClasFacade.findByPclaId(id);
                    if (pclaId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity ParcelClas with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setPclaId(pclaId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelDecision> findAllByIds(List<Integer> ids) {

        List<ParcelDecision> ret = (List<ParcelDecision>) em.createQuery("SELECT x FROM ParcelDecision x left join fetch x.demaId x_demaId left join fetch x.pclaId x_pclaId left join fetch x_demaId.ecgrId x_demaId_ecgrId left join fetch x_pclaId.cultIdDecl x_pclaId_cultIdDecl left join fetch x_pclaId.cultIdPred x_pclaId_cultIdPred left join fetch x_pclaId.cultIdPred2 x_pclaId_cultIdPred2  WHERE x.padeId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM ParcelDecision x WHERE x.padeId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public ParcelDecision findByPadeId(Integer padeId) {
        List<ParcelDecision> results = (List<ParcelDecision>) em.createQuery("SELECT x FROM ParcelDecision x left join fetch x.demaId x_demaId left join fetch x.pclaId x_pclaId left join fetch x_demaId.ecgrId x_demaId_ecgrId left join fetch x_pclaId.cultIdDecl x_pclaId_cultIdDecl left join fetch x_pclaId.cultIdPred x_pclaId_cultIdPred left join fetch x_pclaId.cultIdPred2 x_pclaId_cultIdPred2  WHERE x.padeId = :padeId").
            setParameter("padeId", padeId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByPadeId(Integer padeId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM ParcelDecision x WHERE x.padeId = :padeId")
    .setParameter("padeId", padeId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelDecision> findAllByCriteriaRange_DecisionMakingGrpParcelDecision(Integer demaId_demaId, Integer fsch_decisionLight, Integer fsch_prodCode, String fsch_Code, String fsch_Parcel_Identifier, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_DecisionMakingGrpParcelDecision(false, demaId_demaId, fsch_decisionLight, fsch_prodCode, fsch_Code, fsch_Parcel_Identifier, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelDecision> findAllByCriteriaRange_DecisionMakingGrpParcelDecision(boolean noTransient, Integer demaId_demaId, Integer fsch_decisionLight, Integer fsch_prodCode, String fsch_Code, String fsch_Parcel_Identifier, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM ParcelDecision x  left join fetch x.demaId x_demaId left join fetch x.pclaId x_pclaId left join fetch x_demaId.ecgrId x_demaId_ecgrId left join fetch x_pclaId.cultIdDecl x_pclaId_cultIdDecl left join fetch x_pclaId.cultIdPred x_pclaId_cultIdPred left join fetch x_pclaId.cultIdPred2 x_pclaId_cultIdPred2 WHERE ((1=1) AND (x.demaId.demaId = :demaId_demaId)) AND (x.decisionLight = :fsch_decisionLight) AND (x.pclaId.prodCode = :fsch_prodCode) AND (x.pclaId.parcCode like :fsch_Code) AND (x.pclaId.parcIdentifier like :fsch_Parcel_Identifier) ORDER BY x.decisionLight, x.pclaId.parcIdentifier ";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("demaId_demaId", demaId_demaId);

            if (fsch_decisionLight == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_decisionLight");
            } else {
                paramSets.put("fsch_decisionLight", fsch_decisionLight);
            }

            if (fsch_prodCode == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_prodCode");
            } else {
                paramSets.put("fsch_prodCode", fsch_prodCode);
            }

            if (fsch_Code == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_Code");
            } else {
                paramSets.put("fsch_Code", fsch_Code+"%");
            }

            if (fsch_Parcel_Identifier == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_Parcel_Identifier");
            } else {
                paramSets.put("fsch_Parcel_Identifier", fsch_Parcel_Identifier+"%");
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.padeId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.padeId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<ParcelDecision> ret = (List<ParcelDecision>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DecisionMakingGrpParcelDecision", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_DecisionMakingGrpParcelDecision_count(Integer demaId_demaId, Integer fsch_decisionLight, Integer fsch_prodCode, String fsch_Code, String fsch_Parcel_Identifier, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM ParcelDecision x WHERE ((1=1) AND (x.demaId.demaId = :demaId_demaId)) AND (x.decisionLight = :fsch_decisionLight) AND (x.pclaId.prodCode = :fsch_prodCode) AND (x.pclaId.parcCode like :fsch_Code) AND (x.pclaId.parcIdentifier like :fsch_Parcel_Identifier) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("demaId_demaId", demaId_demaId);

            if (fsch_decisionLight == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_decisionLight");
            } else {
                paramSets.put("fsch_decisionLight", fsch_decisionLight);
            }

            if (fsch_prodCode == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_prodCode");
            } else {
                paramSets.put("fsch_prodCode", fsch_prodCode);
            }

            if (fsch_Code == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_Code");
            } else {
                paramSets.put("fsch_Code", fsch_Code+"%");
            }

            if (fsch_Parcel_Identifier == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_Parcel_Identifier");
            } else {
                paramSets.put("fsch_Parcel_Identifier", fsch_Parcel_Identifier+"%");
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DecisionMakingGrpParcelDecision_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelDecision> findAllByCriteriaRange_DashboardGrpParcelDecision(Integer pclaId_pclaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_DashboardGrpParcelDecision(false, pclaId_pclaId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelDecision> findAllByCriteriaRange_DashboardGrpParcelDecision(boolean noTransient, Integer pclaId_pclaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM ParcelDecision x  left join fetch x.demaId x_demaId left join fetch x.pclaId x_pclaId left join fetch x_demaId.ecgrId x_demaId_ecgrId left join fetch x_pclaId.cultIdDecl x_pclaId_cultIdDecl left join fetch x_pclaId.cultIdPred x_pclaId_cultIdPred left join fetch x_pclaId.cultIdPred2 x_pclaId_cultIdPred2 WHERE ((1=1) AND (x.pclaId.pclaId = :pclaId_pclaId)) AND (x.demaId.hasBeenRun = true) AND (x.demaId.hasPopulatedIntegratedDecisionsAndIssues = true) ORDER BY x.demaId.dateTime DESC ";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("pclaId_pclaId", pclaId_pclaId);

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.padeId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.padeId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<ParcelDecision> ret = (List<ParcelDecision>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DashboardGrpParcelDecision", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_DashboardGrpParcelDecision_count(Integer pclaId_pclaId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM ParcelDecision x WHERE ((1=1) AND (x.pclaId.pclaId = :pclaId_pclaId)) AND (x.demaId.hasBeenRun = true) AND (x.demaId.hasPopulatedIntegratedDecisionsAndIssues = true) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("pclaId_pclaId", pclaId_pclaId);


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DashboardGrpParcelDecision_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
