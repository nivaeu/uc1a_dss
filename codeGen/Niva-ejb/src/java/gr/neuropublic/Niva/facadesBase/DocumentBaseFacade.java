package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class DocumentBaseFacade extends AbstractFacade<Document> implements  IDocumentBaseFacade, /*.ILocal, IDocumentBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(DocumentBaseFacade.class);

    public DocumentBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Document initRow() {
        return new Document(getNextSequenceValue());
    }

    public void getTransientFields(List<Document> entities){
    }

    @Override
    public Document createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, Set<String> allowedFields) {
        Document ret = null;
        if (!json.containsKey("docuId") || json.get("docuId") == null)
            throw new GenericApplicationException("Error in DocumentBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("docuId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("docuId"));
                //ret = this.findByDocuId(id);
                ret = getEntityManager().find(Document.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:Document:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("attachedFile") && allowedFields.contains("attachedFile")) {
            String attachedFile = (String)json.get("attachedFile");
            if (attachedFile != null) {
                
                TmpBlob tmpBlob = getTmpBlobFacade().findById(crypto.DecryptInteger(attachedFile));
                if (tmpBlob!= null) {
                    ret.setAttachedFile(tmpBlob.getData());
                } else {
                    throw new RuntimeException("TEMP_BLOB_DELETED");
                }    } else {
                ret.setAttachedFile(null);
            }
        }
        if (json.containsKey("filePath") && allowedFields.contains("filePath")) {
            ret.setFilePath((String)json.get("filePath"));
        }
        if (json.containsKey("description") && allowedFields.contains("description")) {
            ret.setDescription((String)json.get("description"));
        }
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : Document");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.documents_docu_id_seq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Document> findAllByIds(List<Integer> ids) {

        List<Document> ret = (List<Document>) em.createQuery("SELECT x FROM Document x   WHERE x.docuId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM Document x WHERE x.docuId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Document findByDocuId(Integer docuId) {
        List<Document> results = (List<Document>) em.createQuery("SELECT x FROM Document x   WHERE x.docuId = :docuId").
            setParameter("docuId", docuId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByDocuId(Integer docuId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM Document x WHERE x.docuId = :docuId")
    .setParameter("docuId", docuId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Document> findAllByCriteriaRange_DocumentGrpDocument(int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_DocumentGrpDocument(false, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Document> findAllByCriteriaRange_DocumentGrpDocument(boolean noTransient, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM Document x   WHERE (1=1) ORDER BY x.attachedFile, x.docuId";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.docuId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.docuId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<Document> ret = (List<Document>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DocumentGrpDocument", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_DocumentGrpDocument_count(List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM Document x WHERE (1=1) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();

            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DocumentGrpDocument_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
