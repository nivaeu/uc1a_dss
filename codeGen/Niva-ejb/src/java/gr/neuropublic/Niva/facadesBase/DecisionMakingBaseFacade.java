package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class DecisionMakingBaseFacade extends AbstractFacade<DecisionMaking> implements  IDecisionMakingBaseFacade, /*.ILocal, IDecisionMakingBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(DecisionMakingBaseFacade.class);

    public DecisionMakingBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public DecisionMaking initRow() {
        return new DecisionMaking(getNextSequenceValue());
    }

    public void getTransientFields(List<DecisionMaking> entities){
    }

    @Override
    public DecisionMaking createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.IClassificationFacade.ILocal classificationFacade, gr.neuropublic.Niva.facades.IEcGroupFacade.ILocal ecGroupFacade, Set<String> allowedFields) {
        DecisionMaking ret = null;
        if (!json.containsKey("demaId") || json.get("demaId") == null)
            throw new GenericApplicationException("Error in DecisionMakingBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("demaId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("demaId"));
                //ret = this.findByDemaId(id);
                ret = getEntityManager().find(DecisionMaking.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:DecisionMaking:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("description") && allowedFields.contains("description")) {
            ret.setDescription((String)json.get("description"));
        }
        if (json.containsKey("dateTime") && allowedFields.contains("dateTime")) {
            String dateTime = (String)json.get("dateTime");
            try {
                Date jsonDate = gr.neuropublic.utils.DateUtil.parseISO8601Date(dateTime);
                Date entityDate = ret.getDateTime();
                if (jsonDate!= null && !jsonDate.equals(entityDate)) {
                    ret.setDateTime(jsonDate);
                } else {
                    if (jsonDate == null && entityDate != null) {
                        ret.setDateTime(jsonDate);
                    }
                }
            } catch (ParseException ex) {
                throw new GenericApplicationException(ex.getMessage());
            }
        }
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : DecisionMaking");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("recordtype") && allowedFields.contains("recordtype")) {
            Long recordtype = (Long)json.get("recordtype");
            ret.setRecordtype(recordtype != null ? recordtype.intValue() : null);
        }
        if (json.containsKey("hasBeenRun") && allowedFields.contains("hasBeenRun")) {
            ret.setHasBeenRun((Boolean)json.get("hasBeenRun"));
        }
        if (json.containsKey("hasPopulatedIntegratedDecisionsAndIssues") && allowedFields.contains("hasPopulatedIntegratedDecisionsAndIssues")) {
            ret.setHasPopulatedIntegratedDecisionsAndIssues((Boolean)json.get("hasPopulatedIntegratedDecisionsAndIssues"));
        }
        if (json.containsKey("clasId") && allowedFields.contains("clasId")) {
            JSONObject clasId = (JSONObject)json.get("clasId");
            if (clasId == null) {
                ret.setClasId(null);
            } else {
                if (!clasId.containsKey("clasId") || clasId.get("clasId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. clasId is not null but PK is missing or null");
                } 
                Classification clasId_db = null;
                String temp_id = (String)clasId.get("clasId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. clasId.clasId is negative but not in the keysMap dictionary");
                    }
                    clasId_db = (Classification)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)clasId.get("clasId"));
                    clasId_db = classificationFacade.findByClasId(id);
                    if (clasId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity Classification with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setClasId(clasId_db);
            }
        }
        if (json.containsKey("ecgrId") && allowedFields.contains("ecgrId")) {
            JSONObject ecgrId = (JSONObject)json.get("ecgrId");
            if (ecgrId == null) {
                ret.setEcgrId(null);
            } else {
                if (!ecgrId.containsKey("ecgrId") || ecgrId.get("ecgrId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. ecgrId is not null but PK is missing or null");
                } 
                EcGroup ecgrId_db = null;
                String temp_id = (String)ecgrId.get("ecgrId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. ecgrId.ecgrId is negative but not in the keysMap dictionary");
                    }
                    ecgrId_db = (EcGroup)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)ecgrId.get("ecgrId"));
                    ecgrId_db = ecGroupFacade.findByEcgrId(id);
                    if (ecgrId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity EcGroup with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setEcgrId(ecgrId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<DecisionMaking> findAllByIds(List<Integer> ids) {

        List<DecisionMaking> ret = (List<DecisionMaking>) em.createQuery("SELECT x FROM DecisionMaking x left join fetch x.clasId x_clasId left join fetch x.ecgrId x_ecgrId  WHERE x.demaId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM DecisionMaking x WHERE x.demaId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public DecisionMaking findByDemaId(Integer demaId) {
        List<DecisionMaking> results = (List<DecisionMaking>) em.createQuery("SELECT x FROM DecisionMaking x left join fetch x.clasId x_clasId left join fetch x.ecgrId x_ecgrId  WHERE x.demaId = :demaId").
            setParameter("demaId", demaId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByDemaId(Integer demaId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM DecisionMaking x WHERE x.demaId = :demaId")
    .setParameter("demaId", demaId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<DecisionMaking> findLazyDecisionMaking(String description, Date dateTime, Integer clasId_clasId, Integer ecgrId_ecgrId, Boolean hasBeenRun, Boolean hasPopulatedIntegratedDecisionsAndIssues, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findLazyDecisionMaking(false, description, dateTime, clasId_clasId, ecgrId_ecgrId, hasBeenRun, hasPopulatedIntegratedDecisionsAndIssues, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<DecisionMaking> findLazyDecisionMaking(boolean noTransient, String description, Date dateTime, Integer clasId_clasId, Integer ecgrId_ecgrId, Boolean hasBeenRun, Boolean hasPopulatedIntegratedDecisionsAndIssues, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM DecisionMaking x  left join fetch x.clasId x_clasId left join fetch x.ecgrId x_ecgrId WHERE (x.description like :description) AND (x.dateTime = :dateTime) AND (x.clasId.clasId = :clasId_clasId) AND (x.ecgrId.ecgrId = :ecgrId_ecgrId) AND (x.hasBeenRun=:hasBeenRun) AND (x.hasPopulatedIntegratedDecisionsAndIssues=:hasPopulatedIntegratedDecisionsAndIssues)  ";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            if (description == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":description");
            } else {
                paramSets.put("description", description+"%");
            }

            if (dateTime == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":dateTime");
            } else {
                paramSets.put("dateTime", dateTime);
            }

            if (clasId_clasId == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":clasId_clasId");
            } else {
                paramSets.put("clasId_clasId", clasId_clasId);
            }

            if (ecgrId_ecgrId == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":ecgrId_ecgrId");
            } else {
                paramSets.put("ecgrId_ecgrId", ecgrId_ecgrId);
            }

            if (hasBeenRun == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":hasBeenRun");
            } else {
                paramSets.put("hasBeenRun", hasBeenRun);
            }

            if (hasPopulatedIntegratedDecisionsAndIssues == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":hasPopulatedIntegratedDecisionsAndIssues");
            } else {
                paramSets.put("hasPopulatedIntegratedDecisionsAndIssues", hasPopulatedIntegratedDecisionsAndIssues);
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.demaId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.demaId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<DecisionMaking> ret = (List<DecisionMaking>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findLazyDecisionMaking", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findLazyDecisionMaking_count(String description, Date dateTime, Integer clasId_clasId, Integer ecgrId_ecgrId, Boolean hasBeenRun, Boolean hasPopulatedIntegratedDecisionsAndIssues, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM DecisionMaking x WHERE (x.description like :description) AND (x.dateTime = :dateTime) AND (x.clasId.clasId = :clasId_clasId) AND (x.ecgrId.ecgrId = :ecgrId_ecgrId) AND (x.hasBeenRun=:hasBeenRun) AND (x.hasPopulatedIntegratedDecisionsAndIssues=:hasPopulatedIntegratedDecisionsAndIssues)  ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            if (description == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":description");
            } else {
                paramSets.put("description", description+"%");
            }

            if (dateTime == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":dateTime");
            } else {
                paramSets.put("dateTime", dateTime);
            }

            if (clasId_clasId == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":clasId_clasId");
            } else {
                paramSets.put("clasId_clasId", clasId_clasId);
            }

            if (ecgrId_ecgrId == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":ecgrId_ecgrId");
            } else {
                paramSets.put("ecgrId_ecgrId", ecgrId_ecgrId);
            }

            if (hasBeenRun == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":hasBeenRun");
            } else {
                paramSets.put("hasBeenRun", hasBeenRun);
            }

            if (hasPopulatedIntegratedDecisionsAndIssues == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":hasPopulatedIntegratedDecisionsAndIssues");
            } else {
                paramSets.put("hasPopulatedIntegratedDecisionsAndIssues", hasPopulatedIntegratedDecisionsAndIssues);
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findLazyDecisionMaking_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<DecisionMaking> findLazyDecisionMakingRO(String demaId_description, Date dateTime, Integer clasId_clasId, Integer ecgrId_ecgrId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findLazyDecisionMakingRO(false, demaId_description, dateTime, clasId_clasId, ecgrId_ecgrId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<DecisionMaking> findLazyDecisionMakingRO(boolean noTransient, String demaId_description, Date dateTime, Integer clasId_clasId, Integer ecgrId_ecgrId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM DecisionMaking x  left join fetch x.clasId x_clasId left join fetch x.ecgrId x_ecgrId WHERE (x.description like :demaId_description) AND (x.dateTime = :dateTime) AND (x.clasId.clasId = :clasId_clasId) AND (x.ecgrId.ecgrId = :ecgrId_ecgrId) AND (x.hasPopulatedIntegratedDecisionsAndIssues=true)  ";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            if (demaId_description == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":demaId_description");
            } else {
                paramSets.put("demaId_description", demaId_description);
            }

            if (dateTime == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":dateTime");
            } else {
                paramSets.put("dateTime", dateTime);
            }

            if (clasId_clasId == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":clasId_clasId");
            } else {
                paramSets.put("clasId_clasId", clasId_clasId);
            }

            if (ecgrId_ecgrId == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":ecgrId_ecgrId");
            } else {
                paramSets.put("ecgrId_ecgrId", ecgrId_ecgrId);
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.demaId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.demaId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<DecisionMaking> ret = (List<DecisionMaking>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findLazyDecisionMakingRO", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findLazyDecisionMakingRO_count(String demaId_description, Date dateTime, Integer clasId_clasId, Integer ecgrId_ecgrId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM DecisionMaking x WHERE (x.description like :demaId_description) AND (x.dateTime = :dateTime) AND (x.clasId.clasId = :clasId_clasId) AND (x.ecgrId.ecgrId = :ecgrId_ecgrId) AND (x.hasPopulatedIntegratedDecisionsAndIssues=true)  ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            if (demaId_description == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":demaId_description");
            } else {
                paramSets.put("demaId_description", demaId_description);
            }

            if (dateTime == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":dateTime");
            } else {
                paramSets.put("dateTime", dateTime);
            }

            if (clasId_clasId == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":clasId_clasId");
            } else {
                paramSets.put("clasId_clasId", clasId_clasId);
            }

            if (ecgrId_ecgrId == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":ecgrId_ecgrId");
            } else {
                paramSets.put("ecgrId_ecgrId", ecgrId_ecgrId);
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findLazyDecisionMakingRO_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<DecisionMaking> findAllByCriteriaRange_forLov(String fsch_DecisionMakingLov_description, Date fsch_DecisionMakingLov_dateTime, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_forLov(false, fsch_DecisionMakingLov_description, fsch_DecisionMakingLov_dateTime, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<DecisionMaking> findAllByCriteriaRange_forLov(boolean noTransient, String fsch_DecisionMakingLov_description, Date fsch_DecisionMakingLov_dateTime, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM DecisionMaking x  left join fetch x.clasId x_clasId left join fetch x.ecgrId x_ecgrId WHERE (1=1) AND (x.description like :fsch_DecisionMakingLov_description) AND (x.dateTime = :fsch_DecisionMakingLov_dateTime) ORDER BY x.description, x.demaId";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            if (fsch_DecisionMakingLov_description == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_DecisionMakingLov_description");
            } else {
                paramSets.put("fsch_DecisionMakingLov_description", fsch_DecisionMakingLov_description+"%");
            }

            if (fsch_DecisionMakingLov_dateTime == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_DecisionMakingLov_dateTime");
            } else {
                paramSets.put("fsch_DecisionMakingLov_dateTime", fsch_DecisionMakingLov_dateTime);
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.demaId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.demaId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<DecisionMaking> ret = (List<DecisionMaking>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_forLov", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_forLov_count(String fsch_DecisionMakingLov_description, Date fsch_DecisionMakingLov_dateTime, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM DecisionMaking x WHERE (1=1) AND (x.description like :fsch_DecisionMakingLov_description) AND (x.dateTime = :fsch_DecisionMakingLov_dateTime) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            if (fsch_DecisionMakingLov_description == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_DecisionMakingLov_description");
            } else {
                paramSets.put("fsch_DecisionMakingLov_description", fsch_DecisionMakingLov_description+"%");
            }

            if (fsch_DecisionMakingLov_dateTime == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_DecisionMakingLov_dateTime");
            } else {
                paramSets.put("fsch_DecisionMakingLov_dateTime", fsch_DecisionMakingLov_dateTime);
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_forLov_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
