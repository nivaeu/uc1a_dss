package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class ParcelsIssuesBaseFacade extends AbstractFacade<ParcelsIssues> implements  IParcelsIssuesBaseFacade, /*.ILocal, IParcelsIssuesBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(ParcelsIssuesBaseFacade.class);

    public ParcelsIssuesBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public ParcelsIssues initRow() {
        return new ParcelsIssues(getNextSequenceValue());
    }

    public void getTransientFields(List<ParcelsIssues> entities){
    }

    @Override
    public ParcelsIssues createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.IParcelClasFacade.ILocal parcelClasFacade, Set<String> allowedFields) {
        ParcelsIssues ret = null;
        if (!json.containsKey("parcelsIssuesId") || json.get("parcelsIssuesId") == null)
            throw new GenericApplicationException("Error in ParcelsIssuesBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("parcelsIssuesId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("parcelsIssuesId"));
                //ret = this.findByParcelsIssuesId(id);
                ret = getEntityManager().find(ParcelsIssues.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:ParcelsIssues:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("dteCreated") && allowedFields.contains("dteCreated")) {
            String dteCreated = (String)json.get("dteCreated");
            try {
                Date jsonDate = gr.neuropublic.utils.DateUtil.parseISO8601Date(dteCreated);
                Date entityDate = ret.getDteCreated();
                if (jsonDate!= null && !jsonDate.equals(entityDate)) {
                    ret.setDteCreated(jsonDate);
                } else {
                    if (jsonDate == null && entityDate != null) {
                        ret.setDteCreated(jsonDate);
                    }
                }
            } catch (ParseException ex) {
                throw new GenericApplicationException(ex.getMessage());
            }
        }
        if (json.containsKey("status") && allowedFields.contains("status")) {
            Long status = (Long)json.get("status");
            ret.setStatus(status != null ? status.shortValue() : null);
        }
        if (json.containsKey("dteStatusUpdate") && allowedFields.contains("dteStatusUpdate")) {
            String dteStatusUpdate = (String)json.get("dteStatusUpdate");
            try {
                Date jsonDate = gr.neuropublic.utils.DateUtil.parseISO8601Date(dteStatusUpdate);
                Date entityDate = ret.getDteStatusUpdate();
                if (jsonDate!= null && !jsonDate.equals(entityDate)) {
                    ret.setDteStatusUpdate(jsonDate);
                } else {
                    if (jsonDate == null && entityDate != null) {
                        ret.setDteStatusUpdate(jsonDate);
                    }
                }
            } catch (ParseException ex) {
                throw new GenericApplicationException(ex.getMessage());
            }
        }
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : ParcelsIssues");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("typeOfIssue") && allowedFields.contains("typeOfIssue")) {
            Long typeOfIssue = (Long)json.get("typeOfIssue");
            ret.setTypeOfIssue(typeOfIssue != null ? typeOfIssue.shortValue() : null);
        }
        if (json.containsKey("active") && allowedFields.contains("active")) {
            ret.setActive((Boolean)json.get("active"));
        }
        if (json.containsKey("pclaId") && allowedFields.contains("pclaId")) {
            JSONObject pclaId = (JSONObject)json.get("pclaId");
            if (pclaId == null) {
                ret.setPclaId(null);
            } else {
                if (!pclaId.containsKey("pclaId") || pclaId.get("pclaId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. pclaId is not null but PK is missing or null");
                } 
                ParcelClas pclaId_db = null;
                String temp_id = (String)pclaId.get("pclaId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. pclaId.pclaId is negative but not in the keysMap dictionary");
                    }
                    pclaId_db = (ParcelClas)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)pclaId.get("pclaId"));
                    pclaId_db = parcelClasFacade.findByPclaId(id);
                    if (pclaId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity ParcelClas with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setPclaId(pclaId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelsIssues> findAllByIds(List<Integer> ids) {

        List<ParcelsIssues> ret = (List<ParcelsIssues>) em.createQuery("SELECT x FROM ParcelsIssues x left join fetch x.pclaId x_pclaId  WHERE x.parcelsIssuesId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM ParcelsIssues x WHERE x.parcelsIssuesId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public ParcelsIssues findByParcelsIssuesId(Integer parcelsIssuesId) {
        List<ParcelsIssues> results = (List<ParcelsIssues>) em.createQuery("SELECT x FROM ParcelsIssues x left join fetch x.pclaId x_pclaId  WHERE x.parcelsIssuesId = :parcelsIssuesId").
            setParameter("parcelsIssuesId", parcelsIssuesId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByParcelsIssuesId(Integer parcelsIssuesId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM ParcelsIssues x WHERE x.parcelsIssuesId = :parcelsIssuesId")
    .setParameter("parcelsIssuesId", parcelsIssuesId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelsIssues> findLazyParcelsIssues(Date dteCreated, Short status, Date dteStatusUpdate, Integer pclaId_pclaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findLazyParcelsIssues(false, dteCreated, status, dteStatusUpdate, pclaId_pclaId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelsIssues> findLazyParcelsIssues(boolean noTransient, Date dteCreated, Short status, Date dteStatusUpdate, Integer pclaId_pclaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM ParcelsIssues x  left join fetch x.pclaId x_pclaId WHERE (x.dteCreated = :dteCreated) AND (x.status = :status) AND (x.dteStatusUpdate = :dteStatusUpdate) AND (x.pclaId.pclaId = :pclaId_pclaId)";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            if (dteCreated == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":dteCreated");
            } else {
                paramSets.put("dteCreated", dteCreated);
            }

            if (status == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":status");
            } else {
                paramSets.put("status", status);
            }

            if (dteStatusUpdate == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":dteStatusUpdate");
            } else {
                paramSets.put("dteStatusUpdate", dteStatusUpdate);
            }

            if (pclaId_pclaId == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":pclaId_pclaId");
            } else {
                paramSets.put("pclaId_pclaId", pclaId_pclaId);
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.parcelsIssuesId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.parcelsIssuesId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<ParcelsIssues> ret = (List<ParcelsIssues>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findLazyParcelsIssues", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findLazyParcelsIssues_count(Date dteCreated, Short status, Date dteStatusUpdate, Integer pclaId_pclaId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM ParcelsIssues x WHERE (x.dteCreated = :dteCreated) AND (x.status = :status) AND (x.dteStatusUpdate = :dteStatusUpdate) AND (x.pclaId.pclaId = :pclaId_pclaId)";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            if (dteCreated == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":dteCreated");
            } else {
                paramSets.put("dteCreated", dteCreated);
            }

            if (status == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":status");
            } else {
                paramSets.put("status", status);
            }

            if (dteStatusUpdate == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":dteStatusUpdate");
            } else {
                paramSets.put("dteStatusUpdate", dteStatusUpdate);
            }

            if (pclaId_pclaId == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":pclaId_pclaId");
            } else {
                paramSets.put("pclaId_pclaId", pclaId_pclaId);
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findLazyParcelsIssues_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelsIssues> findAllByCriteriaRange_ParcelGPGrpParcelsIssues(Integer pclaId_pclaId, Date fsch_dteCreated, Short fsch_status, Date fsch_dteStatusUpdate, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_ParcelGPGrpParcelsIssues(false, pclaId_pclaId, fsch_dteCreated, fsch_status, fsch_dteStatusUpdate, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelsIssues> findAllByCriteriaRange_ParcelGPGrpParcelsIssues(boolean noTransient, Integer pclaId_pclaId, Date fsch_dteCreated, Short fsch_status, Date fsch_dteStatusUpdate, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM ParcelsIssues x  left join fetch x.pclaId x_pclaId WHERE ((1=1) AND (x.pclaId.pclaId = :pclaId_pclaId)) AND (x.dteCreated = :fsch_dteCreated) AND (x.status = :fsch_status) AND (x.dteStatusUpdate = :fsch_dteStatusUpdate) ORDER BY x.dteCreated, x.parcelsIssuesId ";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("pclaId_pclaId", pclaId_pclaId);

            if (fsch_dteCreated == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_dteCreated");
            } else {
                paramSets.put("fsch_dteCreated", fsch_dteCreated);
            }

            if (fsch_status == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_status");
            } else {
                paramSets.put("fsch_status", fsch_status);
            }

            if (fsch_dteStatusUpdate == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_dteStatusUpdate");
            } else {
                paramSets.put("fsch_dteStatusUpdate", fsch_dteStatusUpdate);
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.parcelsIssuesId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.parcelsIssuesId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<ParcelsIssues> ret = (List<ParcelsIssues>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_ParcelGPGrpParcelsIssues", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_ParcelGPGrpParcelsIssues_count(Integer pclaId_pclaId, Date fsch_dteCreated, Short fsch_status, Date fsch_dteStatusUpdate, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM ParcelsIssues x WHERE ((1=1) AND (x.pclaId.pclaId = :pclaId_pclaId)) AND (x.dteCreated = :fsch_dteCreated) AND (x.status = :fsch_status) AND (x.dteStatusUpdate = :fsch_dteStatusUpdate) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("pclaId_pclaId", pclaId_pclaId);

            if (fsch_dteCreated == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_dteCreated");
            } else {
                paramSets.put("fsch_dteCreated", fsch_dteCreated);
            }

            if (fsch_status == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_status");
            } else {
                paramSets.put("fsch_status", fsch_status);
            }

            if (fsch_dteStatusUpdate == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_dteStatusUpdate");
            } else {
                paramSets.put("fsch_dteStatusUpdate", fsch_dteStatusUpdate);
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_ParcelGPGrpParcelsIssues_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelsIssues> findAllByCriteriaRange_DashboardGrpParcelsIssues(Integer pclaId_pclaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_DashboardGrpParcelsIssues(false, pclaId_pclaId, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelsIssues> findAllByCriteriaRange_DashboardGrpParcelsIssues(boolean noTransient, Integer pclaId_pclaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM ParcelsIssues x  left join fetch x.pclaId x_pclaId WHERE ((1=1) AND (x.pclaId.pclaId = :pclaId_pclaId)) ORDER BY x.dteStatusUpdate DESC ";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("pclaId_pclaId", pclaId_pclaId);

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.parcelsIssuesId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.parcelsIssuesId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<ParcelsIssues> ret = (List<ParcelsIssues>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DashboardGrpParcelsIssues", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_DashboardGrpParcelsIssues_count(Integer pclaId_pclaId, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM ParcelsIssues x WHERE ((1=1) AND (x.pclaId.pclaId = :pclaId_pclaId)) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("pclaId_pclaId", pclaId_pclaId);


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_DashboardGrpParcelsIssues_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelsIssues> findAllByCriteriaRange_ParcelFMISGrpParcelsIssues(Integer pclaId_pclaId, Date fsch_dteCreated, Short fsch_status, Date fsch_dteStatusUpdate, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_ParcelFMISGrpParcelsIssues(false, pclaId_pclaId, fsch_dteCreated, fsch_status, fsch_dteStatusUpdate, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<ParcelsIssues> findAllByCriteriaRange_ParcelFMISGrpParcelsIssues(boolean noTransient, Integer pclaId_pclaId, Date fsch_dteCreated, Short fsch_status, Date fsch_dteStatusUpdate, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM ParcelsIssues x  left join fetch x.pclaId x_pclaId WHERE ((1=1) AND (x.pclaId.pclaId = :pclaId_pclaId)) AND (x.dteCreated = :fsch_dteCreated) AND (x.status = :fsch_status) AND (x.dteStatusUpdate = :fsch_dteStatusUpdate) ORDER BY x.dteCreated, x.parcelsIssuesId ";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            paramSets.put("pclaId_pclaId", pclaId_pclaId);

            if (fsch_dteCreated == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_dteCreated");
            } else {
                paramSets.put("fsch_dteCreated", fsch_dteCreated);
            }

            if (fsch_status == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_status");
            } else {
                paramSets.put("fsch_status", fsch_status);
            }

            if (fsch_dteStatusUpdate == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_dteStatusUpdate");
            } else {
                paramSets.put("fsch_dteStatusUpdate", fsch_dteStatusUpdate);
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.parcelsIssuesId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.parcelsIssuesId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<ParcelsIssues> ret = (List<ParcelsIssues>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_ParcelFMISGrpParcelsIssues", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_ParcelFMISGrpParcelsIssues_count(Integer pclaId_pclaId, Date fsch_dteCreated, Short fsch_status, Date fsch_dteStatusUpdate, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM ParcelsIssues x WHERE ((1=1) AND (x.pclaId.pclaId = :pclaId_pclaId)) AND (x.dteCreated = :fsch_dteCreated) AND (x.status = :fsch_status) AND (x.dteStatusUpdate = :fsch_dteStatusUpdate) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            paramSets.put("pclaId_pclaId", pclaId_pclaId);

            if (fsch_dteCreated == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_dteCreated");
            } else {
                paramSets.put("fsch_dteCreated", fsch_dteCreated);
            }

            if (fsch_status == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_status");
            } else {
                paramSets.put("fsch_status", fsch_status);
            }

            if (fsch_dteStatusUpdate == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_dteStatusUpdate");
            } else {
                paramSets.put("fsch_dteStatusUpdate", fsch_dteStatusUpdate);
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_ParcelFMISGrpParcelsIssues_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
