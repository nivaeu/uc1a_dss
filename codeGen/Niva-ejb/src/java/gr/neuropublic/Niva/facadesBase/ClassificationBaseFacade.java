package gr.neuropublic.Niva.facadesBase;

import java.util.List;
import gr.neuropublic.base.AbstractFacade;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.Niva.services.UserSession;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.persistence.Query;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ejb.Remove;
import javax.ejb.EJB;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import gr.neuropublic.base.SaveResponse;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;

import java.util.Calendar;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.text.ParseException;
import javax.persistence.LockModeType;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class ClassificationBaseFacade extends AbstractFacade<Classification> implements  IClassificationBaseFacade, /*.ILocal, IClassificationBaseFacade.IRemote, */ Serializable {


    @PersistenceContext(unitName = "NivaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }


    @EJB
    private gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal tmpBlobFacade;
    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }


    private  final Logger logger = LoggerFactory.getLogger(ClassificationBaseFacade.class);

    public ClassificationBaseFacade() {
        super();
    }



    @Override
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Classification initRow() {
        return new Classification(getNextSequenceValue());
    }

    public void getTransientFields(List<Classification> entities){
    }

    @Override
    public Classification createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, JSONObject json, gr.neuropublic.Niva.facades.IClassifierFacade.ILocal classifierFacade, gr.neuropublic.Niva.facades.IFileTemplateFacade.ILocal fileTemplateFacade, Set<String> allowedFields) {
        Classification ret = null;
        if (!json.containsKey("clasId") || json.get("clasId") == null)
            throw new GenericApplicationException("Error in ClassificationBaseFacade.createFromJson(): id is missing or null");
        switch (_changeStatus) {
            case NEW:
                ret = initRow();
                keysMap.put(json.get("clasId").toString(), ret);
                break;
            case UPDATE:
            case DELETE:
                Integer id = crypto.DecryptInteger((String)json.get("clasId"));
                //ret = this.findByClasId(id);
                ret = getEntityManager().find(Classification.class, id, LockModeType.PESSIMISTIC_WRITE);
                if (ret == null)
                    throw new GenericApplicationException("recordHasBeedDeleted:Classification:"+id);
                break;
            default:
                throw new GenericApplicationException("Unexpected _changeStatus value :'"+_changeStatus+"'");
        }
            
        if (_changeStatus==gr.neuropublic.base.ChangeToCommit.ChangeStatus.DELETE)
            return ret;
        if (json.containsKey("name") && allowedFields.contains("name")) {
            ret.setName((String)json.get("name"));
        }
        if (json.containsKey("description") && allowedFields.contains("description")) {
            ret.setDescription((String)json.get("description"));
        }
        if (json.containsKey("dateTime") && allowedFields.contains("dateTime")) {
            String dateTime = (String)json.get("dateTime");
            try {
                Date jsonDate = gr.neuropublic.utils.DateUtil.parseISO8601Date(dateTime);
                Date entityDate = ret.getDateTime();
                if (jsonDate!= null && !jsonDate.equals(entityDate)) {
                    ret.setDateTime(jsonDate);
                } else {
                    if (jsonDate == null && entityDate != null) {
                        ret.setDateTime(jsonDate);
                    }
                }
            } catch (ParseException ex) {
                throw new GenericApplicationException(ex.getMessage());
            }
        }
        if (json.containsKey("rowVersion") && json.get("rowVersion") != null && allowedFields.contains("rowVersion")) {
            Long rowVersion = (Long)json.get("rowVersion");
            Integer db_row_vsersion = ret.getRowVersion() != null ? ret.getRowVersion() : 0;
            if (rowVersion < db_row_vsersion) {
                logger.info("OPTIMISTIC LOCK for entity : Classification");
                throw new GenericApplicationException("optimisticLockException");
            }
            ret.setRowVersion(rowVersion != null ? rowVersion.intValue() : null);
        } else {
            if (_changeStatus == gr.neuropublic.base.ChangeToCommit.ChangeStatus.UPDATE)
                throw new GenericApplicationException("row_version field not set");
        }
        if (json.containsKey("recordtype") && allowedFields.contains("recordtype")) {
            Long recordtype = (Long)json.get("recordtype");
            ret.setRecordtype(recordtype != null ? recordtype.intValue() : null);
        }
        if (json.containsKey("filePath") && allowedFields.contains("filePath")) {
            ret.setFilePath((String)json.get("filePath"));
        }
        if (json.containsKey("attachedFile") && allowedFields.contains("attachedFile")) {
            String attachedFile = (String)json.get("attachedFile");
            if (attachedFile != null) {
                
                TmpBlob tmpBlob = getTmpBlobFacade().findById(crypto.DecryptInteger(attachedFile));
                if (tmpBlob!= null) {
                    ret.setAttachedFile(tmpBlob.getData());
                } else {
                    throw new RuntimeException("TEMP_BLOB_DELETED");
                }    } else {
                ret.setAttachedFile(null);
            }
        }
        if (json.containsKey("year") && allowedFields.contains("year")) {
            Long year = (Long)json.get("year");
            ret.setYear(year != null ? year.shortValue() : null);
        }
        if (json.containsKey("isImported") && allowedFields.contains("isImported")) {
            ret.setIsImported((Boolean)json.get("isImported"));
        }
        if (json.containsKey("clfrId") && allowedFields.contains("clfrId")) {
            JSONObject clfrId = (JSONObject)json.get("clfrId");
            if (clfrId == null) {
                ret.setClfrId(null);
            } else {
                if (!clfrId.containsKey("clfrId") || clfrId.get("clfrId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. clfrId is not null but PK is missing or null");
                } 
                Classifier clfrId_db = null;
                String temp_id = (String)clfrId.get("clfrId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. clfrId.clfrId is negative but not in the keysMap dictionary");
                    }
                    clfrId_db = (Classifier)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)clfrId.get("clfrId"));
                    clfrId_db = classifierFacade.findByClfrId(id);
                    if (clfrId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity Classifier with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setClfrId(clfrId_db);
            }
        }
        if (json.containsKey("fiteId") && allowedFields.contains("fiteId")) {
            JSONObject fiteId = (JSONObject)json.get("fiteId");
            if (fiteId == null) {
                ret.setFiteId(null);
            } else {
                if (!fiteId.containsKey("fiteId") || fiteId.get("fiteId") == null) {
                    throw new GenericApplicationException("Error in  CreateFromJson. fiteId is not null but PK is missing or null");
                } 
                FileTemplate fiteId_db = null;
                String temp_id = (String)fiteId.get("fiteId");

                if (temp_id.startsWith("TEMP_ID_")) {
                    if (!keysMap.containsKey(temp_id)) {
                        throw new GenericApplicationException("Error in  CreateFromJson. fiteId.fiteId is negative but not in the keysMap dictionary");
                    }
                    fiteId_db = (FileTemplate)keysMap.get(temp_id);
                } else {
                    Integer id = crypto.DecryptInteger((String)fiteId.get("fiteId"));
                    fiteId_db = fileTemplateFacade.findByFiteId(id);
                    if (fiteId_db == null) {
                        throw new GenericApplicationException("Error in  CreateFromJson. No entity FileTemplate with PK values = '" + id.toString() + "' found in the database");
                    }
                }


                ret.setFiteId(fiteId_db);
            }
        }    

        return ret;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int getNextSequenceValue()
    {
        EntityManager em = getEntityManager();

        Query query = em.createNativeQuery("SELECT nextval('niva.niva_sq')");
        BigInteger nextValBI = (BigInteger) query.getSingleResult();

    	return nextValBI.intValue();
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Classification> findAllByIds(List<Integer> ids) {

        List<Classification> ret = (List<Classification>) em.createQuery("SELECT x FROM Classification x left join fetch x.clfrId x_clfrId left join fetch x.fiteId x_fiteId  WHERE x.clasId IN (:ids)").
            setParameter("ids", ids).
            getResultList();

        getTransientFields(ret);
        return ret;
    }

    public int delAllByIds(List<Integer> ids) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM Classification x WHERE x.clasId IN (:ids)")
    .setParameter("ids", ids);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Classification findByClasId(Integer clasId) {
        List<Classification> results = (List<Classification>) em.createQuery("SELECT x FROM Classification x left join fetch x.clfrId x_clfrId left join fetch x.fiteId x_fiteId  WHERE x.clasId = :clasId").
            setParameter("clasId", clasId).
            getResultList();
        int size = results.size();
        if (size==0)
            return null;
        if (size == 1) {
            getTransientFields(results);
            return results.get(0);
        }
        throw new GenericApplicationException("query returned more than one results");
    }

    public int delByClasId(Integer clasId) {                                                                                                             
        Query deleteQuery = em.createQuery("DELETE FROM Classification x WHERE x.clasId = :clasId")
    .setParameter("clasId", clasId);
        return deleteQuery.executeUpdate();                                                                                                         
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Classification> findLazyClassification(String name, Date dateTime, Integer clfrId_clfrId, Short year, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findLazyClassification(false, name, dateTime, clfrId_clfrId, year, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Classification> findLazyClassification(boolean noTransient, String name, Date dateTime, Integer clfrId_clfrId, Short year, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM Classification x  left join fetch x.clfrId x_clfrId left join fetch x.fiteId x_fiteId WHERE (x.name like :name) AND (x.dateTime = :dateTime) AND (x.clfrId.clfrId = :clfrId_clfrId) AND (x.year = :year)  ";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            if (name == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":name");
            } else {
                paramSets.put("name", name+"%");
            }

            if (dateTime == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":dateTime");
            } else {
                paramSets.put("dateTime", dateTime);
            }

            if (clfrId_clfrId == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":clfrId_clfrId");
            } else {
                paramSets.put("clfrId_clfrId", clfrId_clfrId);
            }

            if (year == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":year");
            } else {
                paramSets.put("year", year);
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.clasId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.clasId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<Classification> ret = (List<Classification>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findLazyClassification", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findLazyClassification_count(String name, Date dateTime, Integer clfrId_clfrId, Short year, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM Classification x WHERE (x.name like :name) AND (x.dateTime = :dateTime) AND (x.clfrId.clfrId = :clfrId_clfrId) AND (x.year = :year)  ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            if (name == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":name");
            } else {
                paramSets.put("name", name+"%");
            }

            if (dateTime == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":dateTime");
            } else {
                paramSets.put("dateTime", dateTime);
            }

            if (clfrId_clfrId == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":clfrId_clfrId");
            } else {
                paramSets.put("clfrId_clfrId", clfrId_clfrId);
            }

            if (year == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":year");
            } else {
                paramSets.put("year", year);
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findLazyClassification_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Classification> findAllByCriteriaRange_forLov(String fsch_ClassificationLov_name, String fsch_ClassificationLov_description, Date fsch_ClassificationLov_dateTime, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        return findAllByCriteriaRange_forLov(false, fsch_ClassificationLov_name, fsch_ClassificationLov_description, fsch_ClassificationLov_dateTime, range, recordCount, sortField, sortOrder, excludedEntities);
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public List<Classification> findAllByCriteriaRange_forLov(boolean noTransient, String fsch_ClassificationLov_name, String fsch_ClassificationLov_description, Date fsch_ClassificationLov_dateTime, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities) 
    {
        try {
            String jpql = "SELECT x FROM Classification x  left join fetch x.clfrId x_clfrId left join fetch x.fiteId x_fiteId WHERE (1=1) AND (x.name like :fsch_ClassificationLov_name) AND (x.description like :fsch_ClassificationLov_description) AND (x.dateTime = :fsch_ClassificationLov_dateTime) AND (x.isImported=true) ORDER BY x.name, x.clasId";
            jpql = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpql, excludedEntities);
           
            Map<String,Object> paramSets = new HashMap<>();
            //add params or remove optional inputs from the where expression if their value is null
            if (fsch_ClassificationLov_name == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_ClassificationLov_name");
            } else {
                paramSets.put("fsch_ClassificationLov_name", fsch_ClassificationLov_name+"%");
            }

            if (fsch_ClassificationLov_description == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_ClassificationLov_description");
            } else {
                paramSets.put("fsch_ClassificationLov_description", fsch_ClassificationLov_description+"%");
            }

            if (fsch_ClassificationLov_dateTime == null) {
                jpql = gr.neuropublic.utils.EjbUtil.excludeParam(jpql, ":fsch_ClassificationLov_dateTime");
            } else {
                paramSets.put("fsch_ClassificationLov_dateTime", fsch_ClassificationLov_dateTime);
            }

            
            if (sortField != null) {
                boolean validSqlColumnName = sortField.matches("^[a-zA-Z_\\$][a-zA-Z0-9_\\$\\.]*$");
                if (validSqlColumnName) {
                    if (jpql.matches("(?i)^.*order\\s+by.*$")) {
                        jpql = jpql.replaceFirst("(?i)^(.*order\\s+by\\s+(x\\.)?).*$", "$1"+sortField+ (sortOrder ? " ASC" : " DESC") + ", x.clasId");
                    } else {
                        jpql = jpql + " order by x." + sortField + (sortOrder ? " ASC" : " DESC")  + ", x.clasId" ;
                    }
                }
            }

            jpql = jpql.replace('{', '(').replace('}', ')');
            Query q = em.createQuery(jpql);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                q.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            
            //Find the total record count and add to the row count
            if (recordCount != null) {
                recordCount[0] = -1;
            }

            //Set up the pagination range
            if (range != null) {
                q.setMaxResults(range[1] - range[0]);
                q.setFirstResult(range[0]);
            }

            List<Classification> ret = (List<Classification>)q.getResultList();
            if(!noTransient)
                getTransientFields(ret);
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_forLov", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public int findAllByCriteriaRange_forLov_count(String fsch_ClassificationLov_name, String fsch_ClassificationLov_description, Date fsch_ClassificationLov_dateTime, List<String> excludedEntities) 
    {
        try {
            String jpqlCount = "SELECT COUNT(x)  FROM Classification x WHERE (1=1) AND (x.name like :fsch_ClassificationLov_name) AND (x.description like :fsch_ClassificationLov_description) AND (x.dateTime = :fsch_ClassificationLov_dateTime) AND (x.isImported=true) ";
            jpqlCount = gr.neuropublic.utils.EjbUtil.addExcludedIds(jpqlCount, excludedEntities);

            Map<String,Object> paramSets = new HashMap<>();
            ////add params or remove optional inputs from the where expression if their value is null
            if (fsch_ClassificationLov_name == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_ClassificationLov_name");
            } else {
                paramSets.put("fsch_ClassificationLov_name", fsch_ClassificationLov_name+"%");
            }

            if (fsch_ClassificationLov_description == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_ClassificationLov_description");
            } else {
                paramSets.put("fsch_ClassificationLov_description", fsch_ClassificationLov_description+"%");
            }

            if (fsch_ClassificationLov_dateTime == null) {
                jpqlCount = gr.neuropublic.utils.EjbUtil.excludeParam(jpqlCount, ":fsch_ClassificationLov_dateTime");
            } else {
                paramSets.put("fsch_ClassificationLov_dateTime", fsch_ClassificationLov_dateTime);
            }


            jpqlCount = jpqlCount.replace('{', '(').replace('}', ')');
            Query countQuery = getEntityManager().createQuery(jpqlCount);
            
            //set input parameters
            for ( Map.Entry<String,Object> paramSet : paramSets.entrySet() ) {
                countQuery.setParameter(paramSet.getKey(), paramSet.getValue());
            }
            int ret = ((Long) countQuery.getSingleResult()).intValue();
            return ret;
        } catch (Exception e) {
            logger.error("Error in findAllByCriteriaRange_forLov_count", e);        
            DatabaseGenericException newExc = new DatabaseGenericException(e.getMessage());
            newExc.initCause(e);
            throw newExc;
        }
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
