//771BFAF21FE023D390AD2BAC8B886EA9
package gr.neuropublic.Niva.services;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Remote;

@javax.ejb.Singleton
@javax.ejb.Startup
@Local(ISessionsCacheService.ILocal.class)
@Remote(ISessionsCacheService.IRemote.class)
public class SessionsCacheService extends gr.neuropublic.Niva.servicesBase.SessionsCacheServiceBase implements gr.neuropublic.Niva.services.ISessionsCacheService {
    
    @EJB
    private IUserManagementService.ILocal usrMngSrv;
    public IUserManagementService.ILocal getUserManagementService() {
        return usrMngSrv;
    }
    public void setUserManagementService(IUserManagementService.ILocal val) {
        usrMngSrv = val;
    }
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
