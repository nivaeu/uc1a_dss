//21396AA442BCD24703AC460F394B46EB
package gr.neuropublic.Niva.services;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;


@Stateless
@Local (IUserManagementService.ILocal.class)
@Remote(IUserManagementService.IRemote.class)
public class UserManagementService extends gr.neuropublic.Niva.servicesBase.UserManagementServiceBase implements gr.neuropublic.Niva.services.IUserManagementService{
    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
