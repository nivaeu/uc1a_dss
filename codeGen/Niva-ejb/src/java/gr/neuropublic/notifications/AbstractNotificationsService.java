package gr.neuropublic.notifications;

/*
GoogleCredential from google-api-java-client is depricated so we use 
GoogleCredentials from google-auth-library 
*/
//import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.auth.oauth2.AccessToken;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import gr.neuropublic.base.AbstractService;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author st_krommydas
 */
//https://github.com/firebase/quickstart-java/blob/0dec937756dc72c7350e782f73f79a63233bb9a2/messaging/src/main/java/com/google/firebase/quickstart/Messaging.java
//https://firebase.google.com/docs/cloud-messaging/concept-options
public abstract class AbstractNotificationsService extends AbstractService implements IAbstractNotificationsService {

    private final Logger logger = LoggerFactory.getLogger(AbstractNotificationsService.class);

    private final String FCM_BASE_URL = "https://fcm.googleapis.com";  
    private final String FCM_MESSAGING_SCOPE = "https://www.googleapis.com/auth/firebase.messaging";
    private final String[] FCM_SCOPES = { FCM_MESSAGING_SCOPE };    
    private final String FCM_MESSAGE_KEY = "message";
    private final String ALL_DEVICES_TOPIC = "allDevices";
    
    //depricated
    //private GoogleCredential googleCredential;
    private GoogleCredentials googleCredentials;

    private boolean verboseFlag = false;
    public boolean getVerboseFlag() {
        return verboseFlag;
    }
    public void setVerboseFlag(boolean verboseFlag) {
        this.verboseFlag = verboseFlag;
    }
    
    protected String getFirebaseProjectId() {
        return "";
    }
    protected String getFirebaseAccountFileName() {
        return "";
    }

    private String getFcmSendEndpoint() {
        return "/v1/projects/" + getFirebaseProjectId() + "/messages:send";
    }
    
    public FcmResponse sendNotificationToAllDevices(String title, String body, Date expireDate) {
        return sendNotificationMessageToAllDevices(title, body, expireDate, null);
    }

    public FcmResponse sendNotificationMessageToAllDevices(String title, String body, Date expireDate, JsonObject data) {
        return sendNotificationMessage(title, body, new FcmTopicTarget(ALL_DEVICES_TOPIC), expireDate, data);
    }

    public FcmResponse sendNotification(String title, String body, AbstractFcmTarget target, Date expireDate) {
        return sendNotificationMessage(title, body, target, expireDate, null);
    }

    public FcmResponse sendNotificationMessage(String title, String body, AbstractFcmTarget target, Date expireDate, JsonObject data) {
        if (title == null || body == null || target == null)
            throw new GenericApplicationException("In Notification messages title, body and target cannot be null.");
        
        try {
            JsonObject notificationMessage = buildNotificationMessage(title, body, target, expireDate, data);
            
            if (verboseFlag) {
                System.out.println("FCM request body for message using common notification object:");
                prettyPrint(notificationMessage);
            }

            return sendFcmMessage(notificationMessage);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new GenericApplicationException(ex.getMessage());
        }
    }  

    public JsonObject getMessageData(String data) {
        if (data == null)
            return null;
        
        JsonObject jData = new JsonParser().parse(data).getAsJsonObject();
        
        return jData;
    }
    
    /**
     * Construct the body of a notification message request.
     *
     * @return JSON of notification message.
     */
    private JsonObject buildNotificationMessage(String title, String body, AbstractFcmTarget target, Date expireDate, JsonObject data) {
        JsonObject jNotification = new JsonObject();
        
        jNotification.addProperty("title", title);
        jNotification.addProperty("body", body);

        //JsonObject jData = data != null ? data : new JsonObject();
        //jData.addProperty("click_action", "FLUTTER_NOTIFICATION_CLICK");

        JsonObject jMessage = new JsonObject();
        jMessage.addProperty(target.name, target.value);

        jMessage.add("notification", jNotification);
        if (data != null)
          jMessage.add("data", data );

        // Android Override Payload
        jMessage.add("android", buildAndroidOverridePayload(expireDate));

        //Apns Override Payload for iOs devices
        jMessage.add("apns", buildApnsOverridePayload(expireDate));

        JsonObject jFcm = new JsonObject();
        jFcm.add(FCM_MESSAGE_KEY, jMessage);

        return jFcm;
    }

    
    /**
     * Build the android payload that will customize how a message is received on Android.
     *
     * @return android payload of an FCM request.
     */
    private JsonObject buildAndroidOverridePayload(Date expireDate) {
        JsonObject androidPayload = new JsonObject();
        androidPayload.addProperty("priority", "high");

        String ttl = getAndroidTtl(expireDate);
        if (ttl != null)
          androidPayload.addProperty("ttl", ttl);
        
        
        return androidPayload;
    }    

    private String getAndroidTtl(Date expireDate) {
        if (expireDate == null)
            return null;
        long nowMilis = new Date().getTime();
        long expireMilis = expireDate.getTime();
        
        // if ttl is 0s then fcm does this:
        // deliver emmidiately if possible, else discarded from fcm
        if (expireMilis <= nowMilis)
            return "0s";
        
        long timeToLiveInSecs = (expireMilis - nowMilis) / 1000;
        
        // 2419200 is the max period (28 days) and it corresponds to the maximum period of time for which FCM stores and attempts to deliver the message
        return String.valueOf(timeToLiveInSecs > 2419200 ? 2419200 : timeToLiveInSecs) + "s";
    }

    private String getApnsExpiration(Date expireDate) {
        //A UNIX epoch date expressed in seconds (UTC)
        if (expireDate == null)
            return null;
        long nowMilis = new Date().getTime();
        long expireMilis = expireDate.getTime();
        
        //If the value is 0, APNs treats the notification as if it expires immediately and does not store the notification or attempt to redeliver it
       
        long timeToLiveInMilis = expireMilis <= nowMilis ? 0 : (expireMilis - nowMilis);
        
        // A UNIX epoch date expressed in seconds (UTC). This header identifies the date when the notification is no longer valid and can be discarded
        return String.valueOf((System.currentTimeMillis() + timeToLiveInMilis) / 1000);
    }
    
    /**
     * Build the apns payload that will customize how a message is received on iOS.
     *
     * @return apns payload of an FCM request.
     */
    private JsonObject buildApnsOverridePayload(Date expireDate) {
        JsonObject apnsPayload = new JsonObject();
        
        JsonObject apnsHeaders = new JsonObject();
        apnsHeaders.addProperty("apns-priority", "10");
        String apnsExpiration = getApnsExpiration(expireDate);
        if (apnsExpiration != null)
          apnsHeaders.addProperty("apns-expiration", apnsExpiration);

        apnsPayload.add("headers", apnsHeaders);
        
        return apnsPayload;
    }
  
    /**
     * Send request to FCM message using HTTP.
     *
     * @param fcmMessage Body of the HTTP request.
     * @throws IOException
     */
    private FcmResponse sendFcmMessage(JsonObject fcmMessage) throws IOException {
        HttpURLConnection connection = getFcmConnection();
        connection.setDoOutput(true);
        OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
        writer.write(fcmMessage.toString());
        writer.flush();
        writer.close();

        FcmResponse fcmResp = new FcmResponse(connection.getResponseCode(), connection.getResponseMessage());
      
        try{
            if (fcmResp.isSuccess()) {
                String response = inputstreamToString(connection.getInputStream());
                fcmResp.setResponseString(response);
                if (verboseFlag) {
                    System.out.println("Message sent to Firebase for delivery, response:");
                    System.out.println(response);
                }
            } else {
                String response = inputstreamToString(connection.getErrorStream());
                fcmResp.setResponseString(response);
                if (verboseFlag) {
                    System.out.println("Unable to send message to Firebase:");
                    System.out.println(response);
                }
            }

        } catch(Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
        }

        return fcmResp;
    }

    /**
     * Retrieve a valid access token that can be use to authorize requests to the FCM REST
     * API.
     *
     * @return Access token.
     * @throws IOException
     */
    // [START retrieve_access_token]
    private String getFcmAccessToken() throws IOException {

        if (this.googleCredentials == null) {
            String serverConfUrl = System.getProperty("jboss.server.config.dir") ;
            String path = new File(serverConfUrl, getFirebaseAccountFileName()).toString();  
            if(new File(path).exists()) {  
                this.googleCredentials = GoogleCredentials
                    .fromStream(new FileInputStream(path))
                    .createScoped(Arrays.asList(FCM_SCOPES));
            } else {
                String msg = "File: " + path + " doesn't exist";
                logger.error(msg);
                throw new IOException(msg);
            }
        }

        this.googleCredentials.refresh();//refreshToken();

        AccessToken accessToken = googleCredentials.getAccessToken();
        if (accessToken == null) {
            logger.error("Null AccessToken");
            throw new GenericApplicationException("AccessToken from GoogleCredentials is null.");
        }
        
        return accessToken.getTokenValue();//getAccessToken();
        
    }
    // [END retrieve_access_token]    

    /**
     * Create HttpURLConnection that can be used for both retrieving and publishing.
     *
     * @return Base HttpURLConnection.
     * @throws IOException
     */
    private HttpURLConnection getFcmConnection() throws IOException {
      // [START use_access_token]
      URL url = new URL(FCM_BASE_URL + getFcmSendEndpoint());
      HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
      httpURLConnection.setRequestMethod("POST");
      httpURLConnection.setRequestProperty("Authorization", "Bearer " + getFcmAccessToken());
      httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
      return httpURLConnection;
      // [END use_access_token]
    }


    /**
     * Read contents of InputStream into String.
     *
     * @param inputStream InputStream to read.
     * @return String containing contents of InputStream.
     * @throws IOException
     */
    private String inputstreamToString(InputStream inputStream) throws IOException {
      StringBuilder stringBuilder = new StringBuilder();
      Scanner scanner = new Scanner(inputStream, "UTF-8");
      while (scanner.hasNext()) {
        stringBuilder.append(scanner.nextLine());
      }
      return stringBuilder.toString();
    }

    /**
     * Pretty print a JsonObject.
     *
     * @param jsonObject JsonObject to pretty print.
     */
    private void prettyPrint(JsonObject jsonObject) {
      Gson gson = new GsonBuilder().setPrettyPrinting().create();
      System.out.println(gson.toJson(jsonObject) + "\n");
    }


}
