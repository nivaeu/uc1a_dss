package gr.neuropublic.notifications;

import gr.neuropublic.base.AbstractService;
import gr.neuropublic.exceptions.GenericApplicationException;
import gr.neuropublic.functional.Func;
import gr.neuropublic.enums.DataBaseType;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author st_krommydas
 */
@Stateless
@Local (gr.neuropublic.notifications.IUsrMngNotificationsService.ILocal.class)
@Remote(gr.neuropublic.notifications.IUsrMngNotificationsService.IRemote.class)
public class UsrMngNotificationsService extends AbstractService implements gr.neuropublic.notifications.IUsrMngNotificationsService {

    @PersistenceContext(unitName = "UserManagementPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected DataBaseType getDataBaseType() {
        return DataBaseType.POSTGRESSQL;
    }

    private final Logger logger = LoggerFactory.getLogger(UsrMngNotificationsService.class);    
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    @Override
    public void saveFcmUserTokenToDb(final Integer userId, final String userName, final String appName, final String token) 
    {
        try {
            executeDbProcedure(getEntityManager(),
                "{call notifications.register_fcm_user_token(?, ?, ?, ?)}",
                appName,
                userId,
                token,
                userName);
        } catch (Exception ex) {
            logger.error("Error received calling notifications.register_fcm_user_token: " +
                    ex.getMessage(), ex);
            throw new GenericApplicationException(ex.getMessage());
        }
    }

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    @Override
    public List<FcmUserToken> getUserTokensByUserEmail(final String userEmail, final String appName) {
        return 
        saveToDBGeneric(new Func<List<FcmUserToken>>() {@Override public List<FcmUserToken> lambda() {

            String sqlQuery =   "select futk.futk_id, futk.futk_registration_token \n" +
                                "  from subscription.ss_users usr \n" +
                                "    join notifications.fcm_user_tokens futk on futk.user_id = usr.user_id \n" +
                                "    join subscription.ss_modules modu on modu.modu_id = futk.modu_id \n" +
                                " where futk.futk_is_deleted is false \n" +
                                "   and usr.user_email = :userEmail \n" +
                                "   and modu.modu_name = :appName";
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("userEmail", userEmail);
            query.setParameter("appName", appName);

            List<Object[]> results = query.getResultList();
            if (results == null || results.size()==0)
                return new ArrayList<>();
            
            List<FcmUserToken> userTokens = new ArrayList<>();
            
            for (Object[] result : results) {
                FcmUserToken token = new FcmUserToken();
                token.id = (Integer)result[0];
                token.token = (String)result[1];
                
                userTokens.add(token);
            }
            
            return userTokens;
        }});
    }        

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    @Override
    public String getUserNameByTin(final String userSubsCode, final String userTin) {
        return 
        saveToDBGeneric(new Func<String>() {@Override public String lambda() {

            String sqlQuery =   " SELECT user_email " +
                                "   FROM subscription.ss_users usr " +
                                "   join subscription.ss_subscribers subs on subs.subs_id = usr.subs_id " +
                                "  WHERE usr.user_vat = :userTin and subs.subs_short_name = :userSubsCode ";
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("userTin", userTin);
            query.setParameter("userSubsCode", userSubsCode);
            List<String> users = query.getResultList();
            if (users.isEmpty())
                return null;
            
            if (users.size() == 2) {
                throw new GenericApplicationException("Two many users returned by user_tin");
            }
            
            String user = users.get(0);
            
            return user;
        }});
    }        
}
