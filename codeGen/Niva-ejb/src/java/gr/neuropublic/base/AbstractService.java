/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.base;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import gr.neuropublic.base.ChangeToCommit;
import gr.neuropublic.exceptions.DatabaseConstraintViolationException;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.exceptions.ValidationException;
import gr.neuropublic.functional.Action;
import gr.neuropublic.functional.Func;
import gr.neuropublic.mutil.base.Pair;
import gr.neuropublic.enums.DataBaseType;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.validation.ConstraintViolation;
import gr.neuropublic.validators.ICheckedEntity;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import javax.persistence.FlushModeType;
import javax.persistence.Query;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import gr.neuropublic.functional.Action1Ex;
import gr.neuropublic.functional.ActionEx;
import gr.neuropublic.functional.Func1Ex;
import gr.neuropublic.functional.FuncEx;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.DriverManager;
import java.util.Properties;
import org.apache.ddlutils.Platform;
import org.apache.ddlutils.PlatformFactory;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import org.geotools.geojson.geom.GeometryJSON;
import org.hibernate.spatial.dialect.postgis.PGGeometryValueExtractor;
import org.hibernate.spatial.dialect.oracle.SDOGeometryValueExtractor;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.service.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.Session;

/**
 *
 * @author gmamais (generated)
 */
public abstract class AbstractService  extends AbstractExceptionHandler implements IAbstractService{
    
    private static final Logger logger = Logger.getLogger(AbstractService.class.getName());
    
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    protected abstract EntityManager getEntityManager();

    protected abstract DataBaseType getDataBaseType();

    public ConnectionProvider getConnectionProvider(EntityManager em) {
            Session session = em.unwrap(Session.class);
            SessionFactoryImplementor sfi = (SessionFactoryImplementor) session.getSessionFactory();
            ConnectionProvider cp = sfi.getConnectionProvider();
            return cp;
    }
    
    protected static void cloneEntities(List<ChangeToCommit<ICheckedEntity>> changes) {
        Map<String,ICheckedEntity> alreadyCloned = new HashMap<String,ICheckedEntity>();
        for(ChangeToCommit<ICheckedEntity> cur:changes) {
            cur.setEntity(cur.getEntity().clone(alreadyCloned));
        }
    }
    
    
    protected void synchronizeChangesWithDb_internal(final UserSession usrSession, final List<ChangeToCommit<ICheckedEntity>> changes, final boolean bCloneEntities) {
        synchronizeChangesWithDb_internal(usrSession.usrEmail, usrSession.getClientIp0(), changes, bCloneEntities);
    }
    
    protected void synchronizeChangesWithDb_internal(final String userName, final List<ChangeToCommit<ICheckedEntity>> changes, final boolean bCloneEntities) {
        synchronizeChangesWithDb_internal(userName, null, changes, bCloneEntities);
    }

    private void synchronizeChangesWithDb_internal(final String userName, final String clientIp, final List<ChangeToCommit<ICheckedEntity>> changes, final boolean bCloneEntities)
    {
        saveToDBGeneric(new Action() {public void lambda() {
            if (bCloneEntities) {
                cloneEntities(changes);
            }
            for(ChangeToCommit<ICheckedEntity> cur:changes) {
                ICheckedEntity clone = null;
                switch (cur.getStatus()) {
                    case NEW:
                        cur.getEntity().setInsertUser(userName, clientIp);
                        clone = cur.getEntity();
                        getEntityManager().persist(clone);
                        getEntityManager().flush();
                        getEntityManager().refresh(clone);
                        cur.setEntity(clone);
                        break;
                    case UPDATE:
                        ICheckedEntity mergedEntity = cur.getEntity();
                        cur.getEntity().setUpdateUser(userName, clientIp);
                        if (bCloneEntities) {
                            mergedEntity = (ICheckedEntity)getEntityManager().merge(cur.getEntity());
                        } 
                        getEntityManager().flush();
                        getEntityManager().refresh(mergedEntity);
                        cur.setEntity(mergedEntity);
                        break;
                    case DELETE:
                        Object mergedObject = cur.getEntity();
                        if (bCloneEntities) 
                            mergedObject = getEntityManager().merge(cur.getEntity());
                        getEntityManager().remove(mergedObject);
                        getEntityManager().flush();
                        break;
                    default:
                        throw new IllegalStateException("Unsupported change status");
                }
            }
        }});    
            
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void synchronizeChangesWithDbNoClone(String userName, List<ChangeToCommit<ICheckedEntity>> changes)
    {
        synchronizeChangesWithDb_internal(userName, changes, false);
    }
	
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void synchronizeChangesWithDb(String userName, List<ChangeToCommit<ICheckedEntity>> changes)
    {
        synchronizeChangesWithDb_internal(userName, changes, true);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void saveEntityToDB(final UserSession usrSession, final ICheckedEntity entity) {
        saveEntityToDB(usrSession.usrEmail, usrSession.getClientIp0(), entity);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void saveEntityToDB(final String userName, final ICheckedEntity entity) {
        saveEntityToDB(userName, null, entity);
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    private void saveEntityToDB(final String userName, final String clientIp, final ICheckedEntity entity)
    {
        saveToDBGeneric(new Action() { public void lambda() {
            entity.setInsertUser(userName, clientIp);
            getEntityManager().persist(entity);
            getEntityManager().flush();
        }});
    }
    

    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void flushEntities()
    {
        try {
            getEntityManager().flush();
        } catch (javax.validation.ConstraintViolationException cve) {

            System.out.println("ConstraintViolationException =" + cve);

            StringBuffer sb = new StringBuffer("");
            for (ConstraintViolation currentViolation : cve.getConstraintViolations()) {

                sb.append(currentViolation.getMessage()).append("%%");

            }
            throw new ValidationException(sb.toString());

        } catch (javax.persistence.PersistenceException e) {

            for (Throwable t = e.getCause(); t != null; t = t.getCause()) {

                logger.info("!!! Exception:" + t);
                if (t instanceof org.hibernate.exception.ConstraintViolationException) {

                    String constraintName = getConstraintFromException((org.hibernate.exception.ConstraintViolationException) t);
                    logger.info("constraintName =" + constraintName);

                    throw new DatabaseConstraintViolationException(constraintName, null);
                } else if (t instanceof org.hibernate.StaleObjectStateException ) {

                    throw new DatabaseConstraintViolationException("optimisticLockException", null);
                }
            }

            throw new DatabaseGenericException(e.getMessage());

        } catch (Exception e) {

            logger.info("!!! Exception during persist " + e);
            throw new DatabaseGenericException(e.getMessage());
        }
            
    }
    
    

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void removeEntity(String userName, ICheckedEntity entityToBeRemobed)
    {
        try {
			Object mergedObject = getEntityManager().merge(entityToBeRemobed);
			getEntityManager().remove(mergedObject);
            getEntityManager().flush();
        } catch (javax.validation.ConstraintViolationException cve) {

            System.out.println("ConstraintViolationException =" + cve);

            StringBuffer sb = new StringBuffer("");
            for (ConstraintViolation currentViolation : cve.getConstraintViolations()) {

                sb.append(currentViolation.getMessage()).append("%%");

            }
            throw new ValidationException(sb.toString());

        } catch (javax.persistence.PersistenceException e) {

            for (Throwable t = e.getCause(); t != null; t = t.getCause()) {

                logger.info("!!! Exception:" + t);
                if (t instanceof org.hibernate.exception.ConstraintViolationException) {

                    String constraintName = getConstraintFromException((org.hibernate.exception.ConstraintViolationException) t);
                    logger.info("constraintName =" + constraintName);

                    throw new DatabaseConstraintViolationException(constraintName, null);
                } else if (t instanceof org.hibernate.StaleObjectStateException ) {

                    throw new DatabaseConstraintViolationException("optimisticLockException", null);
                }
            }

            throw new DatabaseGenericException(e.getMessage());

        } catch (Exception e) {

            logger.info("!!! Exception during persist " + e);
            throw new DatabaseGenericException(e.getMessage());
        }
            
    }
    
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Object getSingleResultOrNull(final String jpql, final Pair<String, Object> ... params) {
        return 
        saveToDBGeneric(new Func<Object>() {@Override public Object lambda() {        
            Query q = getEntityManager().createQuery(jpql);
            for(int i=0 ; i<params.length; i++) {
                q.setParameter(params[i].a, params[i].b);
            }        
            List<Object> results = q.getResultList();
            int size = results.size();
            if (size==0)
                return null;
            if (size == 1) {
                return results.get(0);
            }
            String prms = "";
            for(int i=0 ; i<params.length; i++) {
                prms += params[i].a + ":" + (params[i].b != null ? params[i].b.toString():"null") + "\n";
            }        
            throw new DatabaseGenericException("query returned more than one results. Query is\n" + jpql + "\n\nParams are:\n"+prms+"\n\n");
        }});
    }


    @Override
    public List<SqlQueryRow> executeSqlQuery(final String sqlQuery, final Object ... params ) {
        
        return 
        saveToDBGeneric(new Func<List<SqlQueryRow>>() {@Override public List<SqlQueryRow> lambda() {        
            List<SqlQueryRow> ret = new ArrayList<SqlQueryRow>();
            Session session = getEntityManager().unwrap(Session.class);
            SessionFactoryImplementor sfi = (SessionFactoryImplementor) session.getSessionFactory();

            ConnectionProvider cp = sfi.getConnectionProvider();
            Connection cnn = null;
            
            try {
                cnn = cp.getConnection();
                PreparedStatement st = cnn.prepareStatement(sqlQuery);  
                for(int i=0 ; i<params.length; i++) {
                    BigInteger bi = params[i] instanceof BigInteger ? (BigInteger)params[i] : null;
                    if (bi != null) {
                        st.setObject(i+1, new BigDecimal(bi));
                    } else {
                        st.setObject(i+1, params[i]);
                    }
                }
                ResultSet rs = st.executeQuery();
                ResultSetMetaData rsmd = rs.getMetaData();
                int cols = rsmd.getColumnCount();
                Object columnValue;
                while (rs.next()) {
                    SqlQueryRow row = new SqlQueryRow();
                    for(int c=1; c<=cols; c++) {
                        columnValue = rs.getObject(c);
                        if (columnValue != null) {
                            String className = columnValue.getClass().getCanonicalName();
                            if ("org.postgis.PGgeometry".equals(className)) {
                                PGGeometryValueExtractor ve = new PGGeometryValueExtractor();
                                com.vividsolutions.jts.geom.Geometry jtsGeom = ve.toJTS(columnValue);
                                columnValue = jtsGeom;
                            } else if ("oracle.sql.STRUCT".equals(className)) {
                                SDOGeometryValueExtractor ve = new SDOGeometryValueExtractor();
                                com.vividsolutions.jts.geom.Geometry jtsGeom = ve.toJTS(columnValue);
                                columnValue = jtsGeom;
                            }
                        }

                        row.put(rsmd.getColumnName(c), columnValue);
                    }
                    ret.add(row);
                }
            } catch (SQLException ex) {
                Logger.getLogger(AbstractService.class.getName()).log(Level.SEVERE, null, ex);
                DatabaseGenericException newException = new DatabaseGenericException(ex.getMessage());
                throw newException;
            } catch (Exception ex) {
                Logger.getLogger(AbstractService.class.getName()).log(Level.SEVERE, null, ex);
            
                DatabaseGenericException newException = new DatabaseGenericException(ex.getMessage());
                throw newException;
            } finally {
                try {
                    cp.closeConnection(cnn);
                } catch (SQLException ex){
                    DatabaseGenericException newException = new DatabaseGenericException(ex.getMessage());
                    throw newException;
                }
            }
            return ret;
        }});
    }

    public void setObject(DataBaseType dataBaseType, PreparedStatement p, int parmIndex, Object x0 ) throws SQLException {
        
        Object x = x0 instanceof BigInteger ? new BigDecimal((BigInteger)x0) : x0;
        Geometry geo = x0 instanceof Geometry ? (Geometry)x0 : null;
        java.util.Date d = x0 instanceof java.util.Date ? (java.util.Date)x0 : null;

        if (geo != null) {
            if (dataBaseType == DataBaseType.ORACLE) {
                org.hibernate.spatial.dialect.oracle.DefaultConnectionFinder a = new org.hibernate.spatial.dialect.oracle.DefaultConnectionFinder();
                org.hibernate.spatial.dialect.oracle.OracleJDBCTypeFactory b = new org.hibernate.spatial.dialect.oracle.OracleJDBCTypeFactory(a);
                org.hibernate.spatial.dialect.oracle.SDOGeometryValueBinder vb = new org.hibernate.spatial.dialect.oracle.SDOGeometryValueBinder(b);
                vb.bind(p, geo, parmIndex, null);
            } else if (dataBaseType == DataBaseType.POSTGRESSQL) {
                org.hibernate.spatial.dialect.postgis.PGGeometryValueBinder vb = new org.hibernate.spatial.dialect.postgis.PGGeometryValueBinder();
                vb.bind(p, geo, parmIndex, null);
            } else {
                throw new DatabaseGenericException("Unsupported database type");
            }
        } else if (d!=null) {
            p.setDate(parmIndex, new java.sql.Date(d.getTime()));
        } else {
            p.setObject(parmIndex, x);
        }

    }

    public void setObject(DataBaseType dataBaseType, NamedParameterPreparedStatement p, String parameter, Object x0 ) throws SQLException {
        for (Integer i : p.getParameterIndexes(parameter)) {
            setObject(dataBaseType, p.getDelegate(), i, x0);
        }
    }

    
    @Override
    public List<SqlQueryRow> executeSqlQuery2(final Connection externalJdbcConnection, final String sqlQuery, final ArrayList<Pair<String, Object>> params ) {
        final DataBaseType dataBaseType = getDataBaseType();
        return 
        saveToDBGeneric(new Func<List<SqlQueryRow>>() {@Override public List<SqlQueryRow> lambda() {        
            List<SqlQueryRow> ret = new ArrayList<SqlQueryRow>();
            Session session = getEntityManager().unwrap(Session.class);
            SessionFactoryImplementor sfi = (SessionFactoryImplementor) session.getSessionFactory();

            ConnectionProvider cp = sfi.getConnectionProvider();
            Connection cnn = null;
            
            try {
                cnn = externalJdbcConnection == null ? cp.getConnection() : externalJdbcConnection;
                //PreparedStatement st = cnn.prepareStatement(sqlQuery);  
                NamedParameterPreparedStatement p= NamedParameterPreparedStatement.createNamedParameterPreparedStatement(cnn, sqlQuery);
                for(int i=0 ; i<params.size(); i++) {
                    if (p.hasParameter(params.get(i).a)) {
                        setObject(dataBaseType, p, params.get(i).a, params.get(i).b);
                    }
                }
                ResultSet rs = p.executeQuery();
                ResultSetMetaData rsmd = rs.getMetaData();
                int cols = rsmd.getColumnCount();
                Object columnValue;
                while (rs.next()) {
                    SqlQueryRow row = new SqlQueryRow();
                    for(int c=1; c<=cols; c++) {
                        columnValue = rs.getObject(c);
                        if (columnValue != null) {
                            String className = columnValue.getClass().getCanonicalName();
                            if ("org.postgis.PGgeometry".equals(className)) {
                                PGGeometryValueExtractor ve = new PGGeometryValueExtractor();
                                com.vividsolutions.jts.geom.Geometry jtsGeom = ve.toJTS(columnValue);
                                columnValue = jtsGeom;
                            } else if ("oracle.sql.STRUCT".equals(className)) {
                                SDOGeometryValueExtractor ve = new SDOGeometryValueExtractor();
                                com.vividsolutions.jts.geom.Geometry jtsGeom = ve.toJTS(columnValue);
                                columnValue = jtsGeom;
                            }
                        }

                        row.put(rsmd.getColumnName(c), columnValue);
                    }
                    ret.add(row);
                }
            } catch (SQLException ex) {
                Logger.getLogger(AbstractService.class.getName()).log(Level.SEVERE, null, ex);
            
                DatabaseGenericException newException = new DatabaseGenericException(ex.getMessage());
                throw newException;
            } catch (Exception ex) {
                Logger.getLogger(AbstractService.class.getName()).log(Level.SEVERE, null, ex);
            
                DatabaseGenericException newException = new DatabaseGenericException(ex.getMessage());
                throw newException;
            }  
            finally {
                try {
                    if (externalJdbcConnection == null) {
                        cp.closeConnection(cnn);
                    }
                    else {
                        // the externalJdbcConnection will be closed by caller
                    }
                } catch (SQLException ex){
                    Logger.getLogger(AbstractService.class.getName()).log(Level.SEVERE, null, ex);
                    DatabaseGenericException newException = new DatabaseGenericException(ex.getMessage());
                    throw newException;
                }
            }
            return ret;
        }});
        
    }
   
    
    
    private Connection createNewJdbcConnection(
            String jdbcDriver, 
            String jdbcConnectionURL,
            String user, 
            String pass) throws SQLException 
    {
        try {
            Class.forName(jdbcDriver);
            Platform platform = PlatformFactory.createNewPlatformInstance(jdbcDriver, jdbcConnectionURL);
            Properties props = new Properties();
            props.setProperty("user", user);
            props.setProperty("password", pass);
            Connection connection = DriverManager.getConnection(jdbcConnectionURL, props);
        
            return connection;
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AbstractService.class.getName()).log(Level.SEVERE, null, ex);
            DatabaseGenericException newException = new DatabaseGenericException(ex.getMessage());
            throw newException;
        }
    }
    

    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public List<SqlQueryRow> executeSqlQuery2(
            final String jdbcDriver, 
            final String jdbcConnectionURL,
            final String user, 
            final String pass,
            final String sqlQuery, 
            final ArrayList<Pair<String, Object>> params ) 
    {
        Connection connection = null;
        try {
            connection = createNewJdbcConnection(jdbcDriver, jdbcConnectionURL, user, pass);
            return executeSqlQuery2(connection, sqlQuery, params);
        } catch (SQLException ex) {
            Logger.getLogger(AbstractService.class.getName()).log(Level.SEVERE, null, ex);
            DatabaseGenericException newException = new DatabaseGenericException(ex.getMessage());
            throw newException;
        } finally {
            try {
                if (connection!=null && !connection.isClosed()) {
                    connection.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(AbstractService.class.getName()).log(Level.SEVERE, null, ex);
                DatabaseGenericException newException = new DatabaseGenericException(ex.getMessage());
                throw newException;
            }
        }
    }
        
    
    
    
    @Override
    public List<SqlQueryRow> executeSqlQuery2(final String sqlQuery, final ArrayList<Pair<String, Object>> params ) {
        return executeSqlQuery2(null, sqlQuery, params);
    }
    
    
    @Override
    public List<SqlQueryRow> executeSqlQuery2(final String sqlQuery, final Pair<String, Object> ... params ) {
        ArrayList<Pair<String, Object>> params2 = new ArrayList<>();
        for(int i=0 ; i<params.length; i++) {
            params2.add(params[i]);
        }
        return executeSqlQuery2(null, sqlQuery, params2);
    }
    
    
    @Override
    public Integer executeSqlDml(final String sql, final Object ... params ) {
        return 
        executeToDbGeneric(getEntityManager(), new Func1Ex<Connection, Integer>(){
            @Override
            public Integer lambda(Connection conn) throws SQLException {
                Integer ret = 0;
                PreparedStatement st = conn.prepareStatement(sql);  
                for(int i=0 ; i<params.length; i++) {
                    //st.setObject(i+1, params[i]);
                    setObject(getDataBaseType(), st, i+1, params[i]);
                }
                ret = st.executeUpdate(); 
                st.close();
                return ret;
            }
        });
    }


    @Override
    public void executeDbProcedure(final String sql, final Object ... params ) {
        executeDbProcedure(getEntityManager(), sql, params);
    }
    @Override
    public void executeDbProcedure(final EntityManager em, final String sql, final Object ... params ) {
        executeToDbGeneric(em, new Action1Ex<Connection>() {
            @Override public void lambda(Connection conn) throws SQLException {
                CallableStatement st = conn.prepareCall(sql);  
                for(int i=0 ; i<params.length; i++) {
                    if (params[i] != null) {
                        if (params[i].getClass().equals(java.util.Date.class)) {
                            st.setDate(i+1, new java.sql.Date(((Date)params[i]).getTime()));
                        } else if (params[i].getClass().equals(BigInteger.class)) {
                            st.setObject(i+1, new BigDecimal((BigInteger)params[i]));
                        } else {
                            st.setObject(i+1, params[i]);
                        }
                    } else {
                        st.setNull(i+1, java.sql.Types.NULL);
                    }
                }
                st.execute();
                st.close();
            }
        });
    }

    public void executeToDbGeneric(final Action1Ex<Connection> javaClosure) {
        executeToDbGeneric(getEntityManager(), javaClosure, new Action1Ex<String> () {
            @Override
            public void lambda(String errMsg) throws Exception {
            }
        });
    }
    public void executeToDbGeneric(final EntityManager em, final Action1Ex<Connection> javaClosure) {
        executeToDbGeneric(em, javaClosure, new Action1Ex<String> () {
            @Override
            public void lambda(String errMsg) throws Exception {
            }
        });
    }

    public void executeToDbGeneric(final EntityManager em, final Action1Ex<Connection> javaClosure, Action1Ex<String> onError) {
        saveToDBGeneric(new ActionEx() {@Override public void lambda() throws Exception {   
            ConnectionProvider cp = getConnectionProvider(em);
            Connection conn = null;
            try {
                conn = cp.getConnection();
                javaClosure.lambda(conn);
            } finally {
                cp.closeConnection(conn);
            }
        }}, onError);
    }

    public <TRes> TRes executeToDbGeneric(final Func1Ex<Connection, TRes> javaClosure) {
        return executeToDbGeneric(getEntityManager(), javaClosure, new Action1Ex<String>() {
            @Override
            public void lambda(String errMsg) throws Exception {
            }
        });
    }
    public <TRes> TRes executeToDbGeneric(final EntityManager em, final Func1Ex<Connection, TRes> javaClosure) {
        return executeToDbGeneric(em, javaClosure, new Action1Ex<String>() {
            @Override
            public void lambda(String errMsg) throws Exception {
            }
        });
    }

    public <TRes> TRes executeToDbGeneric(final EntityManager em, final Func1Ex<Connection, TRes> javaClosure, Action1Ex<String> onError) {
        return
        saveToDBGeneric(new FuncEx<TRes>() {@Override public TRes lambda() throws Exception {   
            ConnectionProvider cp = getConnectionProvider(em);
            Connection conn = null;
            try {
                conn = cp.getConnection();
                return javaClosure.lambda(conn);
            } finally {
                cp.closeConnection(conn);
            }
        }}, onError);
    }
    
//    public  abstract gr.neuropublic.validators.ICheckedEntity createEntityFromJson(Map<String,Object> keysMap, String entiyName, gr.neuropublic.base.ChangeToCommit.ChangeStatus status, org.json.simple.JSONObject json, gr.neuropublic.base.CryptoUtils crypto, Map<String,Set<String>> allowed_entity_fields);
    @Override
    public ICheckedEntity createEntityFromJson(Map<String, Object> keysMap, String entiyName, gr.neuropublic.base.ChangeToCommit.ChangeStatus status, JSONObject json, gr.neuropublic.base.CryptoUtils crypto, Map<String, Set<String>> allowed_entity_fields) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    

    protected List<ChangeToCommit<ICheckedEntity>> getChangesToCommit__generic(String userName, String jsonData, Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, Map<String, Set<String>> allowed_entity_fields)
    {
        List<ChangeToCommit<ICheckedEntity>> changesToCommit = new ArrayList<ChangeToCommit<ICheckedEntity>>();
        JSONObject obj= (JSONObject)JSONValue.parse(jsonData);
        JSONObject params = (JSONObject)obj.get("params");
        JSONArray changes = (JSONArray)params.get("data");
        Collections.sort(changes, new JsChangeComparator());
        for(Object curChange: changes) {
            JSONObject cur = (JSONObject)curChange;
            Long status = (Long)cur.get("status");
            String entityName = (String)cur.get("entityName");
            Long when = (Long)cur.get("when");
            JSONObject entity = (JSONObject)cur.get("entity");
            ChangeToCommit.ChangeStatus chStatus;
            switch (status.intValue()) {
                case 0 : 
                    chStatus = ChangeToCommit.ChangeStatus.NEW;
                    break;
                case 1 : 
                    chStatus = ChangeToCommit.ChangeStatus.UPDATE;
                    break;
                case 2 : 
                    chStatus = ChangeToCommit.ChangeStatus.DELETE;
                    break;
                default:
                    throw new RuntimeException("unexpected status value '"+status+"'");
                    
            }
            
            ICheckedEntity javaEntity = createEntityFromJson(keysMap, entityName,  chStatus, entity, crypto, allowed_entity_fields);
            
            Date whenAsDate = new Date(when);
            ChangeToCommit<ICheckedEntity> changeToCommit = new ChangeToCommit<ICheckedEntity>(javaEntity, whenAsDate, chStatus);
            changesToCommit.add(changeToCommit);
        }
        Collections.sort(changesToCommit);
        return changesToCommit;
    }    
    
    protected ArrayList<SaveResponse.NewEntityId> getNewEntitiesIds(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto) {
        ArrayList<SaveResponse.NewEntityId> newEntitiesIds = new ArrayList<SaveResponse.NewEntityId>();
        for (Entry<String, Object> ent : keysMap.entrySet()) {
            ICheckedEntity e = (ICheckedEntity)ent.getValue();
            SaveResponse.NewEntityId entry = new SaveResponse.NewEntityId();
            entry.entityName = e.getEntityName();
            entry.databaseId = e.getPrimaryKeyValueEncrypted(crypto);
            entry.tempId = ent.getKey();
            newEntitiesIds.add(entry);
        }    
        return newEntitiesIds;
    }
    
    
    public static class JsChangeComparator implements Comparator<Object> {

        @Override
        public int compare(Object o1, Object o2) {
            JSONObject cur1 = (JSONObject)o1;
            JSONObject cur2 = (JSONObject)o2;
            Long when1 = (Long)cur1.get("when");
            Long when2 = (Long)cur2.get("when");
            
            return when1.compareTo(when2);
        }
        
    }

    
    public void initializeDatabaseSession(UserSession cx) {
        
    }

}
