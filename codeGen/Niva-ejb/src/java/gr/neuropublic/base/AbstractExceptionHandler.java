/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.base;

import gr.neuropublic.exceptions.DatabaseConstraintViolationException;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.exceptions.ValidationException;
import gr.neuropublic.exceptions.GenericApplicationException;
import gr.neuropublic.exceptions.UserApplicationException;
import gr.neuropublic.functional.Action;
import gr.neuropublic.functional.Action1Ex;
import gr.neuropublic.functional.ActionEx;
import gr.neuropublic.functional.Func;
import gr.neuropublic.functional.FuncEx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.validation.ConstraintViolation;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.transaction.RollbackException;

/**
 *
 * @author gmamais
 */
public abstract class AbstractExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(AbstractService.class);
    
    public void saveToDBGeneric(Action javaClosure) {
        saveToDBGeneric(javaClosure, new Action1Ex<String>() {
            @Override
            public void lambda(String errMsg) {
            }
        });
    }
    
    public void saveToDBGeneric(final Action javaClosure, Action1Ex<String> onError) {
        saveToDBGeneric(new ActionEx() {
            @Override
            public void lambda() throws Exception {
                javaClosure.lambda();
            }
        }, onError);
    }
    
    public void saveToDBGeneric(ActionEx javaClosure, Action1Ex<String> onError) {
        
        try {
            javaClosure.lambda();
            
        } catch (javax.validation.ConstraintViolationException cve) {
            StringBuffer sb = new StringBuffer("");
            for (ConstraintViolation currentViolation : cve.getConstraintViolations()) {

                sb.append(currentViolation.getMessage()).append("%%");

            }
            try { onError.lambda(sb.toString()); } catch (Exception x) {}
            throw new ValidationException(sb.toString());

        } catch (javax.persistence.PersistenceException e) {

            for (Throwable t = e.getCause(); t != null; t = t.getCause()) {

                if (t instanceof org.hibernate.exception.ConstraintViolationException) {

                    String constraintName = getConstraintFromException((org.hibernate.exception.ConstraintViolationException) t);

                    try { onError.lambda(constraintName); } catch (Exception x) {}
                    throw new DatabaseConstraintViolationException(constraintName, null);
                } else if (t instanceof org.hibernate.StaleObjectStateException ) {

                    try { onError.lambda("optimisticLockException"); } catch (Exception x) {}
                    throw new DatabaseConstraintViolationException("optimisticLockException", null);
                } else if (t instanceof org.hibernate.exception.SQLGrammarException && t.getMessage() != null && t.getMessage().startsWith("ORA-01031")) {
                    try { onError.lambda("insufficientPrivilegesException"); } catch (Exception x) {}
                    throw new DatabaseGenericException("insufficientPrivilegesException");
                }
            }
            
            logger.error("PersistenceException", e);
            String exMessage = parseDbExceptionMessage(e.getMessage());
            try { onError.lambda(exMessage); } catch (Exception x) {}
            throw new DatabaseGenericException(exMessage);

        } catch (java.sql.SQLIntegrityConstraintViolationException e) {
            String constraintName = getIntegrityConstraintFromException((java.sql.SQLIntegrityConstraintViolationException)e);
            try { onError.lambda(constraintName); } catch (Exception x) {}
            throw new DatabaseConstraintViolationException(constraintName, null);
           
        } catch (NullPointerException e) {
            logger.error("NullPointerException", e);
            try { onError.lambda("NullPointerException"); } catch (Exception x) {}
            throw new GenericApplicationException(GenericApplicationException.getExceptionStack("NullPointerException", e));
        } catch (UserApplicationException e) {
            String exMsg = parseDbExceptionMessage(e.getMessage());
            try { onError.lambda(exMsg); } catch (Exception x) {}
            throw new UserApplicationException(exMsg);
        } catch (Exception e) {

            for (Throwable t = e.getCause(); t != null; t = t.getCause()) {

                if (t instanceof org.hibernate.exception.GenericJDBCException){
                    String exMessage = parseDbExceptionMessage(t.getMessage());
                    try { onError.lambda(exMessage); } catch (Exception x) {}
                    throw new DatabaseGenericException(exMessage);
                } else if (t instanceof org.hibernate.exception.ConstraintViolationException) {

                    String constraintName = getConstraintFromException((org.hibernate.exception.ConstraintViolationException) t);
                    try { onError.lambda(constraintName); } catch (Exception x) {}
                    throw new DatabaseConstraintViolationException(constraintName, null);
                } else if (t instanceof org.hibernate.exception.SQLGrammarException && t.getMessage() != null && t.getMessage().startsWith("ORA-01031")) {
                    try { onError.lambda("insufficientPrivilegesException"); } catch (Exception x) {}
                    throw new DatabaseGenericException("insufficientPrivilegesException");
                } 
            }

            if (e instanceof java.sql.SQLException){
                java.sql.SQLException sqlException = (java.sql.SQLException) e;
                if (isNoLogException(sqlException)) {
                    String exMsg = parseDbExceptionMessage(sqlException.getMessage());
                    try { onError.lambda(exMsg); } catch (Exception x) {}
                    throw new UserApplicationException(exMsg);
                }
            }

            logger.error("!!! Exception during persist ", e);
            String exMessage = parseDbExceptionMessage(e.getMessage());
            try { onError.lambda(exMessage); } catch (Exception x) {}
            throw new DatabaseGenericException(exMessage);
        }
        
    }
    
    public <TRes> TRes saveToDBGeneric(Func<TRes> javaClosure) {
        
        return saveToDBGeneric(javaClosure, new Action1Ex<String>() {
            @Override
            public void lambda(String err) {
            }
        });
    }
    

    public <TRes> TRes saveToDBGeneric(final Func<TRes> javaClosure, Action1Ex<String> onError) {
        return saveToDBGeneric(new FuncEx<TRes>() {
            @Override
            public TRes lambda() throws Exception {
                return javaClosure.lambda();
            }},
            onError    
        );
    }
    
    public <TRes> TRes saveToDBGeneric(FuncEx<TRes> javaClosure, Action1Ex<String> onError) {
        
        try {
            return javaClosure.lambda();
            
        }         
        catch (javax.validation.ConstraintViolationException cve) {

            StringBuffer sb = new StringBuffer("");
            for (ConstraintViolation currentViolation : cve.getConstraintViolations()) {

                sb.append(currentViolation.getMessage()).append("%%");

            }
            try { onError.lambda(sb.toString()); } catch (Exception x) {}
            throw new ValidationException(sb.toString());

        } catch (javax.persistence.PersistenceException e) {

            for (Throwable t = e.getCause(); t != null; t = t.getCause()) {

                if (t instanceof org.hibernate.exception.ConstraintViolationException) {

                    String constraintName = getConstraintFromException((org.hibernate.exception.ConstraintViolationException) t);
                    try { onError.lambda(constraintName); } catch (Exception x) {}
                    throw new DatabaseConstraintViolationException(constraintName, null);
                } else if (t instanceof org.hibernate.StaleObjectStateException ) {

                    try { onError.lambda("optimisticLockException"); } catch (Exception x) {}
                    throw new DatabaseConstraintViolationException("optimisticLockException", null);
                } else if (t instanceof org.hibernate.exception.SQLGrammarException && t.getMessage() != null && t.getMessage().startsWith("ORA-01031")) {
                    try { onError.lambda("insufficientPrivilegesException"); } catch (Exception x) {}
                    throw new DatabaseGenericException("insufficientPrivilegesException");
                }
            }

            logger.error("PersistenceException", e);
            String exMsg = parseDbExceptionMessage(e.getMessage());
            try { onError.lambda(exMsg); } catch (Exception x) {}
            throw new DatabaseGenericException(exMsg);

        } catch (java.sql.SQLIntegrityConstraintViolationException e) {
            String constraintName = getIntegrityConstraintFromException((java.sql.SQLIntegrityConstraintViolationException)e);
            try { onError.lambda(constraintName); } catch (Exception x) {}
            throw new DatabaseConstraintViolationException(constraintName, null);
           
        } catch (NullPointerException e) {
            try { onError.lambda("NullPointerException"); } catch (Exception x) {}
            logger.error("NullPointerException", e);
            throw new GenericApplicationException(GenericApplicationException.getExceptionStack("NullPointerException",e));
        } catch (UserApplicationException e) {
            String exMsg = parseDbExceptionMessage(e.getMessage());
            try { onError.lambda(exMsg); } catch (Exception x) {}
            throw new UserApplicationException(exMsg);
        } catch (Exception e) {
            
            for (Throwable t = e.getCause(); t != null; t = t.getCause()) {

                if (t instanceof org.hibernate.exception.GenericJDBCException){
                    String errMsg = parseDbExceptionMessage(t.getMessage());
                    try { onError.lambda(errMsg); } catch (Exception x) {}
                    throw new DatabaseGenericException(errMsg);
                } else if (t instanceof org.hibernate.exception.ConstraintViolationException) {

                    String constraintName = getConstraintFromException((org.hibernate.exception.ConstraintViolationException) t);
                    try { onError.lambda(constraintName); } catch (Exception x) {}
                    throw new DatabaseConstraintViolationException(constraintName, null);
                } else if (t instanceof org.hibernate.exception.SQLGrammarException && t.getMessage() != null && t.getMessage().startsWith("ORA-01031")) {
                    try { onError.lambda("insufficientPrivilegesException"); } catch (Exception x) {}
                    throw new DatabaseGenericException("insufficientPrivilegesException");
                } 
            }
            if (e instanceof java.sql.SQLException){
                java.sql.SQLException sqlException = (java.sql.SQLException) e;
                if (isNoLogException(sqlException)) {
                    String exMsg = parseDbExceptionMessage(sqlException.getMessage());
                    try { onError.lambda(exMsg); } catch (Exception x) {}
                    throw new UserApplicationException(exMsg);
                }
            }
            
            logger.error("!!! Exception during persist ",e);
            String exMsg = parseDbExceptionMessage(e.getMessage());
            try { onError.lambda(exMsg); } catch (Exception x) {}
            throw new DatabaseGenericException(exMsg);
        }
        
    }
    
    private static boolean isNoLogException(java.sql.SQLException sqlEx){
        return isPostgresNoLogException(sqlEx) || isOracleNoLogException(sqlEx);
    }
    private static boolean isPostgresNoLogException(java.sql.SQLException sqlEx){
        if (sqlEx.getSQLState() != null && sqlEx.getSQLState().equals("NOLOG"))
            return true;
        return false;
    }
    private static boolean isOracleNoLogException(java.sql.SQLException sqlEx){
        if (sqlEx.getSQLState() != null && sqlEx.getSQLState().equals("72000") && sqlEx.getErrorCode() == 20222)
            return true;
        return false;
    }
    
    private static String parseDbExceptionMessage(String errMessage){
        String exMessage = GenericApplicationException.searchOracleApplicationError(errMessage);
        return GenericApplicationException.parsePostgresqlError(exMessage);
    }

    public static String getConstraintFromException(org.hibernate.exception.ConstraintViolationException constraintException) {
        
        String constraintName = constraintException.getConstraintName();
        if (constraintName != null) {
            return constraintName;
        }
        
        String message = constraintException.getMessage();
        if (message != null) {
            constraintName = getOraConstraintName("ORA-02290", message);
            if (constraintName != null)
                return constraintName;
            constraintName = getOraConstraintName("ORA-00001", message);
            if (constraintName != null)
                return constraintName;
		}
        
        return message;
    
    }

    public static String getIntegrityConstraintFromException(java.sql.SQLIntegrityConstraintViolationException constraintException) {

        String message = constraintException.getMessage();
        if (message != null) {
            String constraintName = getOraConstraintName("ORA-00001", message);
            if (constraintName != null)
                return constraintName;
            constraintName = getOraConstraintName("ORA-02290", message);
            if (constraintName != null)
                return constraintName;
        }

        return message;

    }

    private static String getOraConstraintName(String errorCode, String dbMessage){
        if(errorCode == null || dbMessage == null)
            return null;

        String searchWord = errorCode + ":"; //e.g. "ORA-00001:"
        Integer idx = dbMessage.indexOf(searchWord);
        if (idx != -1) {
            String[] parts = dbMessage.split("\\(");
            return parts[1].substring(0, parts[1].indexOf(")"));
        }

        return null;
    }
}
