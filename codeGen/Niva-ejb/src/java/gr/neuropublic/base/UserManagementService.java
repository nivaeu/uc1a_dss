package gr.neuropublic.base;

import javax.ejb.EJB;
import gr.neuropublic.base.IUserManagementService;
import gr.neuropublic.base.WsResponseInfo;
import gr.neuropublic.exceptions.GenericApplicationException;
import gr.neuropublic.functional.Action;
import gr.neuropublic.functional.Func;
import gr.neuropublic.mutil.base.Pair;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.XMLConstants;
import javax.xml.namespace.QName;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class UserManagementService  extends AbstractExceptionHandler  implements SOAPHandler<SOAPMessageContext> {
    
    @PersistenceContext(unitName = "UserManagementPU")
    private EntityManager em;

    protected EntityManager getEntityManager() {
        return em;
    }

    private static final Logger logger = LoggerFactory.getLogger(UserManagementService.class);
    
    @EJB
    private IMemCacheService.ILocal memCacheService;
    public IMemCacheService.ILocal getMemCacheService() {
        return memCacheService;
    }
    public void setMemCacheService(IMemCacheService.ILocal memCacheService) {
        this.memCacheService = memCacheService;
    }
    
    private Integer lockExpirationMinutes = 30;
    private Integer passwordExpirationAlertDays = 7;

    protected void getPasswordPolicyParameters() {
        saveToDBGeneric(new Action() {@Override public void  lambda() {
            String sqlQuery = "SELECT cast(COALESCE(lock_expiration_mins, 30) as integer) as lockExpirationMinutes, " +
                              "       cast(round(COALESCE(password_expiration_alert_secs/60/60/24, 7)) as integer) as passwordExpirationAlertDays" +
                              "  FROM subscription.ss_password_policy_parameters WHERE active_flag=1";
                    
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            List<Object[]> result = query.getResultList();
            if (result == null || result.size()==0){
                lockExpirationMinutes = 30;
                passwordExpirationAlertDays = 7;
            } else {
                lockExpirationMinutes = Integer.valueOf(result.get(0)[0].toString());
                passwordExpirationAlertDays = Integer.valueOf(result.get(0)[1].toString());
            }
        }});
    }
    
    
    /*
     * Remove concurrent sessions
     */
    public void clearUserSsoSessions(String sessionId) {
        String user_email = getUserByActiveSessionId(sessionId);
        if (user_email != null)
            _clearUserSessions(user_email, null, true, 1);
    }
    protected void clearOldUserSessions(String user_email, String appName) {
        _clearUserSessions(user_email, appName, false, 3);
    }
    protected void clearOldUserSsoSessions(String user_email) {
        _clearUserSessions(user_email, null, true, 3);
    }
    protected void _clearUserSessions(final String user_email, final String appName, final Boolean clearAll, final Integer logoutCode) {
        saveToDBGeneric(new Action() {@Override public void lambda() {
            String sqlQuery = "SELECT usrs_session as session "
                            + "FROM subscription.ss_user_sessions usrs "
                            + "WHERE usrs_email=:user_email "
                            + "  and not exists (select 1 from subscription.ss_users usr where COALESCE(usr.user_multiple_sessions_flag,0) = 1 and usr.user_email = usrs.usrs_email)"
                            + "  AND usrs_logoutsse IS NULL ";
            if (!clearAll) {
                sqlQuery = sqlQuery + "AND usrs_app=:appName ";
            } else {
                sqlQuery = sqlQuery 
                            + "UNION "
                            + "SELECT usrs_ssosession "
                            + "FROM subscription.ss_user_sessions usrs "
                            + "WHERE usrs_email=:user_email "
                            + "  and not exists (select 1 from subscription.ss_users usr where COALESCE(usr.user_multiple_sessions_flag,0) = 1 and usr.user_email = usrs.usrs_email)"
                            + "  AND usrs_logoutsse IS NULL ";
            }
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("user_email", user_email);
            if (!clearAll)
                query.setParameter("appName", appName);
            List<String> oldSessions = query.getResultList();
            
            if (oldSessions.isEmpty())
                return;
            //1. update database
            Integer length = oldSessions.size();
            String sessionsList = "(";
            for(int i=0; i<length-1;i++) {
                sessionsList += "'" + oldSessions.get(i) + "', ";
            }
            sessionsList += "'" + oldSessions.get(length-1) + "')";
            String updateSql = "UPDATE subscription.ss_user_sessions "
                    + "SET  usrs_logoutsse = EXTRACT(EPOCH FROM NOW()), "
                    + "     uslc_id = :logoutCode "
                    + "WHERE usrs_logoutsse IS NULL AND usrs_session  in " + sessionsList;
            Query updateQuery = getEntityManager().createNativeQuery(updateSql);
            updateQuery.setParameter("logoutCode", logoutCode);
            updateQuery.executeUpdate();
            //2. remove from memcache
            
            for(String oldSessionId:oldSessions) {
                getMemCacheService().deleteKey(oldSessionId);
            }
            
        }});
    }
    
    
    protected UserModuleView getUserModuleView(final String user_email, final String appName) {
        return 
        saveToDBGeneric(new Func<UserModuleView>() {@Override public UserModuleView  lambda() {
            String sqlQuery = "SELECT sust_id, smgs_id, sumg_valid_from, sumg_valid_until, subscriber_has_the_module, user_has_the_module, usst_id, subs_short_name, subs_id,sumg_id, mogr_name " +
                             "FROM subscription.ssv_user_modules "                            +
                             "WHERE user_email = :user_email AND modu_name = :modu_name " +
                              " AND subscriber_has_the_module IS NOT NULL " +
                              " AND user_has_the_module IS NOT NULL" +
                             " order by subsciber_module_group_is_active desc, subsciber_module_group_is_valid desc ";
                    
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("user_email", user_email);
            query.setParameter("modu_name", appName);
            List<Object[]> result = query.getResultList();
            if (result == null || result.size()==0)
                return null;
            UserModuleView ret = new UserModuleView();
            ret.sust_id = (Short)result.get(0)[0];
            ret.smgs_id = (Short)result.get(0)[1];
            ret.sumg_valid_from = (Timestamp)result.get(0)[2];
            ret.sumg_valid_until = (Timestamp)result.get(0)[3];
            ret.subscriber_has_the_module = (Boolean)result.get(0)[4];
            ret.user_has_the_module = (Boolean)result.get(0)[5];
            return ret;
        }});
    }
    
    protected Integer getUsersMappingPlatform() {
        return null;
    }
    
    /*
     * returns null only if public email exists in the usma_internal_login_name column
     */
    public String getUserLoginNameByPublicEmail(final String publicEmail) {
        final Integer mappingPlatform = getUsersMappingPlatform();
        if (mappingPlatform == null)
            return publicEmail;
        return 
        saveToDBGeneric(new Func<String>() {@Override public String  lambda() {
            //1 Check if public user e-mail exists in the private_login_name of the mapping table.
            //  if yes login attempt should fail.
            String sqlQuery = "select count(1) from subscription.ss_users_mapping where usma_platform = :usma_platform AND usma_internal_login_name = :usma_internal_login_name";
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("usma_platform", mappingPlatform);
            query.setParameter("usma_internal_login_name", publicEmail);
            BigInteger count = (BigInteger)query.getSingleResult();
            if (count.equals(BigInteger.ONE)) {
                return null;
            }
            
            //1 Check if public user e-mail exists in the public_user_email of the mapping table.
            sqlQuery = "select usma_internal_login_name from subscription.ss_users_mapping where usma_platform = :usma_platform AND usma_public_email = :usma_public_email";
            query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("usma_platform", mappingPlatform);
            query.setParameter("usma_public_email", publicEmail);
            List<String> privateEmailList = (List<String>)query.getResultList();
            if (privateEmailList.size() == 0) {
                return publicEmail;
            } else {
                return privateEmailList.get(0);
            }
        }});
    }

    public void setUserPassword(final Integer userId, final String publicUserName, final String password, final String loggedInUserName) {
        setUserPassword(userId, publicUserName, password, loggedInUserName, false, "");
    }    
    public void setUserPassword(final Integer userId, final String publicUserName, final String password, final String loggedInUserName, final boolean isPwdChangedByAdmin, final String clientIp) {
        
        saveToDBGeneric( new Action() {@Override public void lambda() {
            Integer usrChangeType = isPwdChangedByAdmin ? 1 : 0;
            
            EntityManager em = getEntityManager();
            Query query = em.createNativeQuery("select cast(subscription.password_set("
                 + "CAST(:i_user_id AS integer), "
                 + "CAST(:s_public_username AS character varying), "
                 + "CAST(:s_pwd AS character varying), "
                 + "CAST(:s_usrupdate AS character varying),"
                 + "CAST(:i_usr_change_type as integer) ,"
                 + "CAST(:s_caller as character varying) ,"
                 + "CAST(:s_ip as character varying) ) as varchar)");
 
    		query.setParameter("i_user_id", userId);
    		query.setParameter("s_public_username", publicUserName);
    		query.setParameter("s_pwd", password);
    		query.setParameter("s_usrupdate", loggedInUserName);
                query.setParameter("i_usr_change_type", usrChangeType);
                query.setParameter("s_caller", "NeuroCodeUsrMng");
                query.setParameter("s_ip", clientIp);
                
            query.getResultList();
            
        }});
    }
    
    public Integer getUserIdByUserName(final String userName) {
        return 
        saveToDBGeneric(new Func<Integer>() {@Override public Integer  lambda() {
            String sqlQuery = "SELECT user_id FROM subscription.ss_users WHERE user_email=:user_email";
                    
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("user_email", userName);
            List<Integer> result = query.getResultList();
            if (result == null || result.size()==0)
                return null;
            return (Integer)result.get(0);
        }});
    }
    
    protected User getUser(final String user_email, final String userPwd) {
        return 
        saveToDBGeneric(new Func<User>() {@Override public User  lambda() {
            String sqlQuery = "SELECT a.user_password, a.user_password_salt, a.usst_id, " +
                              "       COALESCE(cast(extract( day from date_trunc('day', a.user_password_expiration_date) - date_trunc('day', now())) as integer), cast(100 as integer)) as user_password_expiration_days, " +
                              "       COALESCE(a.user_password_algorithm, 1), subscription.get_encrypted_pwd(cast(COALESCE(a.user_password_algorithm, 1) as integer), :userPwd), " +
                              "       subs.subs_short_name " +
                              "  FROM subscription.ss_users a " +
                              "     join subscription.ss_subscribers subs on subs.subs_id = a.subs_id" +
                              " WHERE user_email=:user_email";
                    
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("user_email", user_email);
            query.setParameter("userPwd", userPwd);
            List<Object[]> result = query.getResultList();
            if (result == null || result.size()==0)
                return null;
            User ret = new User();
            ret.encryptedPassword = (String)result.get(0)[0];
            ret.salt = (String)result.get(0)[1];
            ret.status = (Short)result.get(0)[2];
            ret.pwdExpirationDays = Integer.valueOf(result.get(0)[3].toString());
            ret.pwdAlgorithm = Integer.valueOf(result.get(0)[4].toString());
            ret.encryptedInsertedPwd = (String)result.get(0)[5];
            ret.subsCode = (String)result.get(0)[6];
            return ret;
        }});
    }
    
    private User getUser(final String user_subs_code, final String user_email, final String userPwd) {
        return 
        saveToDBGeneric(new Func<User>() {@Override public User  lambda() {
            String sqlQuery = "SELECT user_password, user_password_salt, usst_id, " +
                              "       COALESCE(cast(extract( day from date_trunc('day', a.user_password_expiration_date) - date_trunc('day', now())) as integer), cast(100 as integer)) as user_password_expiration_days, " +
                              "       COALESCE(user_password_algorithm, 1), subscription.get_encrypted_pwd(cast(COALESCE(user_password_algorithm, 1) as integer), :userPwd) " +
                              "  FROM subscription.ss_users a " +
                              "    join subscription.ss_subscribers subs on subs.subs_id = a.subs_id " +
                              " WHERE a.user_email=:user_email " +
                              "   and subs.subs_short_name = :user_subs_code ";
                    
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("user_subs_code", user_subs_code);
            query.setParameter("user_email", user_email);
            query.setParameter("userPwd", userPwd);
            List<Object[]> result = query.getResultList();
            if (result == null || result.size()==0)
                return null;
            User ret = new User();
            ret.encryptedPassword = (String)result.get(0)[0];
            ret.salt = (String)result.get(0)[1];
            ret.status = (Short)result.get(0)[2];
            ret.pwdExpirationDays = Integer.valueOf(result.get(0)[3].toString());
            ret.pwdAlgorithm = Integer.valueOf(result.get(0)[4].toString());
            ret.encryptedInsertedPwd = (String)result.get(0)[5];
            return ret;
        }});
    }
    
    protected Application getApplication(final String appName) {
        return 
        saveToDBGeneric(new Func<Application>() {@Override public Application  lambda() {
            String sqlQuery = "SELECT a.modu_isFree FROM subscription.ss_modules a WHERE a.modu_name = :appName";
                    
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("appName", appName);
            List<Boolean> result = query.getResultList();
            if (result == null || result.size()==0)
                return null;
            Application ret = new Application();
            ret.isFree = result.get(0);
            return ret;
        }});
    }
    protected String getUserByActiveSsoSessionId(final String ssoSessionId) {
        return 
        saveToDBGeneric(new Func<String>() {@Override public String  lambda() {
            String sqlQuery = "SELECT distinct " +
                              "       case" +
                              "         when ghost_usr.user_id is null or modu.modu_ghost_user_allowed is false or modu.modu_ghost_user_allowed is null then s.usrs_email " +
                              "        	else ghost_usr.user_email " +
                              "        end as userEmail " +
                              "  FROM subscription.ss_user_sessions s " +
                              "     join subscription.ss_modules modu on modu.modu_name = s.usrs_app" +
                              "     join subscription.ss_users usr on s.usrs_email = usr.user_email " +
                              "     left join subscription.ss_users ghost_usr on usr.user_ghost_user_id = ghost_usr.user_id " +
                              " where s.usrs_ssosession = :ssoSessionId " +
                              "   and s.usrs_logoutsse is null";
                    
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("ssoSessionId", ssoSessionId);
            List<String> result = query.getResultList();
            if (result == null || result.size()==0)
                return null;
            return result.get(0);
        }});
    }

    protected String getUserByActiveSessionId(final String sessionId) {
        return getUserByActiveSessionId(sessionId, false);
    }
    protected String getUserByActiveSessionId(final String sessionId, final boolean checkNotExpired) {
        return 
        saveToDBGeneric(new Func<String>() {@Override public String  lambda() {
            String sqlQuery = "SELECT distinct " +
                              "       case" +
                              "         when ghost_usr.user_id is null or modu.modu_ghost_user_allowed is false or modu.modu_ghost_user_allowed is null then s.usrs_email " +
                              "        	else ghost_usr.user_email " +
                              "        end as userEmail " +
                              "  FROM subscription.ss_user_sessions s " +
                              "     join subscription.ss_modules modu on modu.modu_name = s.usrs_app" +
                              "     join subscription.ss_users usr on s.usrs_email = usr.user_email " +
                              "     left join subscription.ss_users ghost_usr on usr.user_ghost_user_id = ghost_usr.user_id " +
                              " where s.usrs_session = :sessionId " +
                              "   and s.usrs_logoutsse is null " ;
            if (checkNotExpired) {
                sqlQuery = sqlQuery + " and ((now() - s.usrs_last_ping_timestamp))  <(2 /(24 * 60)) ";
            }
                    
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("sessionId", sessionId);
            List<String> result = query.getResultList();
            if (result == null || result.size()==0)
                return null;
            return result.get(0);
        }});
    }

    public Boolean getAppWsResponeInfoEnabled(final String modu_name) {
        return 
        saveToDBGeneric(new Func<Boolean>() {@Override public Boolean lambda() {
            Boolean isWsResponeInfoEnabled = new Boolean(false);
            String sqlQuery = 
                    "select coalesce(modu.modu_is_ws_responseinfo_enabled, false) "
                    + "from subscription.ss_modules as modu "
                    + "where modu.modu_name = :modu_name ";
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("modu_name", modu_name);
            isWsResponeInfoEnabled = (Boolean)query.getSingleResult();
            return isWsResponeInfoEnabled;
        }});
        
    }
    
    public void insertWsResponseInfo(final String sessionId, final String modu_name, final Integer subs_id, final List<WsResponseInfo> wsResponseInfoList) {
        try {
            if (sessionId != null && modu_name != null && wsResponseInfoList != null && wsResponseInfoList.size() > 0) {
                String a_ws_response_info = "";
                for (int i = 0; i < wsResponseInfoList.size(); i++){
                    a_ws_response_info = (a_ws_response_info.equals("") ? "" : a_ws_response_info + "#@#") +
                            wsResponseInfoList.get(i).path + "," + 
                            wsResponseInfoList.get(i).startTs.toString() + "," + 
                            wsResponseInfoList.get(i).endTs.toString() + "," +
                            wsResponseInfoList.get(i).success.toString() + "," +
                            (wsResponseInfoList.get(i).errorCode != null ? wsResponseInfoList.get(i).errorCode : "");
                }

                String updateSql = "select cast(ws_responses.insert_user_session_ws_responses_aux("
                        + " :s_modu_name, :s_session, :a_ws_response_info, :i_subs_id) as varchar) ";
                Query updateQuery = getEntityManager().createNativeQuery(updateSql);
                updateQuery.setParameter("s_modu_name", modu_name);
                updateQuery.setParameter("s_session", sessionId);
                updateQuery.setParameter("a_ws_response_info", a_ws_response_info);
                updateQuery.setParameter("i_subs_id", subs_id);
                updateQuery.getResultList();
            }
        } catch (Exception e) {
            logger.warn("Error inserting WsResponseInfo:", e);
        }
    }

    public List<UserBannerMessage> getUserBannerMessages(final String sessionId, final Integer user_id, final Integer subs_id, final String modu_name) {
        return 
        saveToDBGeneric(new Func<List<UserBannerMessage>>() {@Override public List<UserBannerMessage>  lambda() {
            
            if (sessionId != null){
                String updateSql = "UPDATE subscription.ss_user_sessions "
                        + "SET  usrs_last_ping_timestamp = now() "
                        + "WHERE usrs_session = :sessionId ";
                Query updateQuery = getEntityManager().createNativeQuery(updateSql);
                updateQuery.setParameter("sessionId", sessionId);
                updateQuery.executeUpdate();
            }
            
            String sqlQuery = 
                "SELECT umsg_message as message, umsg_message_type as type, umsg_message_location as location\n" +
                "FROM subscription.ss_user_messages um\n" +
                "	LEFT OUTER JOIN subscription.ss_modules m ON um.modu_id = m.modu_id\n" +
                "WHERE\n" +
                "     um.umsg_message_is_enabled is TRUE\n" +
                "AND (NOW() BETWEEN  um.umsg_valid_from AND  um.umsg_valid_until)\n" +
                "AND (um.user_id is null OR user_id = :user_id)\n" +
                "AND (um.subs_id is null OR subs_id = :subs_id)\n" +
                "AND (um.modu_id is null OR m.modu_name = :modu_name)";
                    
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("user_id", user_id);
            query.setParameter("subs_id", subs_id);
            query.setParameter("modu_name", modu_name);
            List<Object[]> queryResult = query.getResultList();
            List<UserBannerMessage> retResult = new ArrayList<>();
            for (Object[] row : queryResult) {
                UserBannerMessage ubmsg = new UserBannerMessage();
                ubmsg.message = (String)row[0];
                ubmsg.type = (Integer)row[1];
                ubmsg.location = (Integer)row[2];
                retResult.add(ubmsg);
            }
            return retResult;
        }});
    }
    
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public AuthenticationStatus getUserAuthorizationStatus (final String userSubsCode, final String userEmail, String userPassword, final String app, final String privilege){
        User usr = getUser(userSubsCode, userEmail, userPassword);
        if (usr == null)
            return AuthenticationStatus.ACCOUNT_DOESNOT_EXIST;
        if (!usr.checkPassword(userPassword))
            return AuthenticationStatus.WRONG_PASSWORD;
        if (usr.status != 0)
            return AuthenticationStatus.USER_STATUS_NOT_ACTIVE;
        UserModuleView userMod = getUserModuleView(userEmail, app);
        if (userMod == null) 
            return AuthenticationStatus.USER_NOT_ASSOC_WITH_APP;
        if (userMod.sust_id == null || userMod.sust_id != 0)   
            return AuthenticationStatus.SUBSCRIBER_STATUS_NOT_ACTIVE;
        if (!userMod.subscriber_has_the_module)   
            return AuthenticationStatus.SUBSCRIBER_NOT_ASSOC_WITH_APP;
        if (userMod.smgs_id == null || userMod.smgs_id != 0)   
            return AuthenticationStatus.SUBSCRIBER_MODULEGROUP_NOT_ACTIVE;
        if (!userMod.user_has_the_module)   
            return AuthenticationStatus.USER_NOT_ASSOC_WITH_APP_ALTHOUGH_SUBSCRPTN_OK;
        Timestamp now = new Timestamp(System.currentTimeMillis());
        if ( userMod.sumg_valid_from == null || (now.before(userMod.sumg_valid_from)) || userMod.sumg_valid_until == null || (userMod.sumg_valid_until.before(now))) 
            return AuthenticationStatus.SUBSCRIBER_MODULEGROUP_TEMPRL_MISMATCH;
        
        return
            saveToDBGeneric(new Func<AuthenticationStatus>() {@Override public AuthenticationStatus lambda() {
                String sqlQuery = " SELECT count(1) " +
                           "   FROM subscription.ssv_user_privilleges  p " +
                           "  WHERE p.modu_name = :moduName" +
                           "    AND p.user_email = :userEmail" +
                           "    AND p.sypr_name = :privilege";

                Query query = getEntityManager().createNativeQuery(sqlQuery);
                query.setParameter("moduName", app);
                query.setParameter("userEmail", userEmail);
                query.setParameter("privilege", privilege);
                BigInteger cnt = (BigInteger)query.getSingleResult();

                if (BigInteger.ZERO.equals(cnt)) 
                    return AuthenticationStatus.AUTH_ERROR;
                    
                return AuthenticationStatus.SUCCESS;
            }});
        
    }
    
    public boolean isValidLoginSubscriber(String subsCode) {
        return true;
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Pair<AuthenticationStatus, Integer> getUserAuthenticationStatus_WithoutSessionClear(final String email, final String userPassword, final String app, 
            final Boolean checkPwdExpiration, final Boolean setsSsoValue, final Pair<Boolean, String> checkLoginSubscriberInfo) {
    
        return getUserAuthenticationStatus(email, userPassword, app, 
            checkPwdExpiration, false, setsSsoValue, checkLoginSubscriberInfo);
    
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Pair<AuthenticationStatus, Integer> getUserAuthenticationStatus(final String email, final String userPassword, final String app, 
            final Boolean checkPwdExpiration, final Boolean setsSsoValue, final Pair<Boolean, String> checkLoginSubscriberInfo) {
    
        return getUserAuthenticationStatus(email, userPassword, app, 
            checkPwdExpiration, true, setsSsoValue, checkLoginSubscriberInfo);
    
    }
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    private Pair<AuthenticationStatus, Integer> getUserAuthenticationStatus(final String email, final String userPassword, final String app, 
            final Boolean checkPwdExpiration, final Boolean clearOldSessions, final Boolean setsSsoValue, final Pair<Boolean, String> checkLoginSubscriberInfo) {
        getPasswordPolicyParameters();
        
        if (checkLoginSubscriberInfo.a) {
            if (!isValidLoginSubscriber(checkLoginSubscriberInfo.b)) {
                return Pair.create(AuthenticationStatus.INVALID_SUBSCRIBER, 0);
            }
        }
        
        User usr = getUser(email, userPassword);
        
        if (usr == null)
            return Pair.create(AuthenticationStatus.ACCOUNT_DOESNOT_EXIST, 0);

        if (checkLoginSubscriberInfo.a) {
            if (checkLoginSubscriberInfo.b != null && checkLoginSubscriberInfo.b.compareTo(usr.subsCode) != 0) {
                return Pair.create(AuthenticationStatus.WRONG_LOGIN_SUBSCRIBER, 0);
            } else if (checkLoginSubscriberInfo.b == null) {
                if (!isValidLoginSubscriber(usr.subsCode)) {
                    return Pair.create(AuthenticationStatus.INVALID_SUBSCRIBER, 0);
                }
            }
        }

        if (usr.status == 10){
            if (checkAndSetlockExpired(email)){
                usr.status = 0;
            } else {
                return Pair.create(AuthenticationStatus.USER_LOCKED_BY_ATTEMPTS_LIMIT, 0);
            }
        }
        if (!usr.checkPassword(userPassword))
            return Pair.create(AuthenticationStatus.WRONG_PASSWORD, 0);
        if (usr.status != 0)
            return Pair.create(AuthenticationStatus.USER_STATUS_NOT_ACTIVE, 0);
        if (checkPwdExpiration && usr.pwdExpirationDays <= 0)
            return Pair.create(AuthenticationStatus.USER_PASSWORD_HAS_EXPIRED, 0);
        
        Application application = getApplication(app);
        if (application == null)
            return Pair.create(AuthenticationStatus.UNKNOWN_APP, 0);
        if (application.isFree)
            return Pair.create(AuthenticationStatus.SUCCESS, 0);
       
        UserModuleView userMod = getUserModuleView(email, app);
        if (userMod == null) 
            return Pair.create(AuthenticationStatus.USER_NOT_ASSOC_WITH_APP, 0);

        if (userMod.sust_id == null || userMod.sust_id != 0)   
            return Pair.create(AuthenticationStatus.SUBSCRIBER_STATUS_NOT_ACTIVE, 0);
        if (!userMod.subscriber_has_the_module)   
            return Pair.create(AuthenticationStatus.SUBSCRIBER_NOT_ASSOC_WITH_APP, 0);
        if (userMod.smgs_id == null || userMod.smgs_id != 0)   
            return Pair.create(AuthenticationStatus.SUBSCRIBER_MODULEGROUP_NOT_ACTIVE, 0);
        if (!userMod.user_has_the_module)   
            return Pair.create(AuthenticationStatus.USER_NOT_ASSOC_WITH_APP_ALTHOUGH_SUBSCRPTN_OK, 0);

        Timestamp now = new Timestamp(System.currentTimeMillis());
        if (userMod.sumg_valid_from == null || (now.before(userMod.sumg_valid_from)) || userMod.sumg_valid_until == null || (userMod.sumg_valid_until.before(now)) ) 
            return Pair.create(AuthenticationStatus.SUBSCRIBER_MODULEGROUP_TEMPRL_MISMATCH, 0);

        if (clearOldSessions) {
            if (setsSsoValue) {
                clearOldUserSsoSessions(email);
            } else {
                clearOldUserSessions(email, app);
            }
        }
        
        Integer pwdDays = usr.pwdExpirationDays <= passwordExpirationAlertDays ? usr.pwdExpirationDays : 100;
        return Pair.create(AuthenticationStatus.SUCCESS, pwdDays);
        
    }    
    
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Pair<AuthenticationStatus, Integer> getUserAuthenticationStatusFromSoap(
            final String userEmail, 
            final String userPassword, 
            final String appName, 
            final String privilege,
            final Boolean checkPwdExpiration, 
            final Boolean setsSsoValue) 
    {
        Pair<AuthenticationStatus, Integer> ret;
        String email = getUserByActiveSessionId(userPassword, true);
        if ( email != null && !email.equals(userEmail)) {
            return Pair.create(AuthenticationStatus.AUTH_ERROR, 100);
        }
        if (email != null && email.equals(userEmail)) {
            ret = Pair.create(AuthenticationStatus.SUCCESS, 100); 
        } else {
            ret = getUserAuthenticationStatus_WithoutSessionClear(userEmail, userPassword, appName, checkPwdExpiration, setsSsoValue, new Pair<Boolean, String>(false, null));
        }
        if (ret.a == AuthenticationStatus.SUCCESS) {
            String sqlQuery = " SELECT count(1) " +
                       "   FROM subscription.ssv_user_privilleges  p " +
                       "  WHERE p.modu_name = :moduName" +
                       "    AND p.user_email = :userEmail" +
                       "    AND p.sypr_name = :privilege";
                    
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("moduName", appName);
            query.setParameter("userEmail", userEmail);
            query.setParameter("privilege", privilege);
            BigInteger cnt = (BigInteger)query.getSingleResult();
            
            if (BigInteger.ZERO.equals(cnt)) {
                ret.a = AuthenticationStatus.AUTH_ERROR;
            }
        }
        return ret;
    }    
    
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Pair<Pair<AuthenticationStatus,Integer>, String> getUserAuthenticationStatusBasedOnSso(final String ssoSessionId, final String app) {
        getPasswordPolicyParameters();
        String email = getUserByActiveSsoSessionId(ssoSessionId);
        if (email == null) {
            return Pair.create(Pair.create(AuthenticationStatus.SSO_SESSION_HAS_EXPIRED,0), "");
        }
        
        User usr = getUser(email, "sso");
        if (usr == null)
            return Pair.create(Pair.create(AuthenticationStatus.ACCOUNT_DOESNOT_EXIST,0), "");

        if (usr.status == 10){
            if (checkAndSetlockExpired(email)){
                usr.status = 0;
            } else {
                return Pair.create(Pair.create(AuthenticationStatus.USER_LOCKED_BY_ATTEMPTS_LIMIT,0), "");
            }
        }
        if (usr.status != 0)
            return Pair.create(Pair.create(AuthenticationStatus.USER_STATUS_NOT_ACTIVE,0),email);
        if (usr.pwdExpirationDays <= 0)
            return Pair.create(Pair.create(AuthenticationStatus.USER_PASSWORD_HAS_EXPIRED,0), "");
        
        Application application = getApplication(app);
        if (application == null)
            return Pair.create(Pair.create(AuthenticationStatus.UNKNOWN_APP,0),email);
        if (application.isFree)
            return Pair.create(Pair.create(AuthenticationStatus.SUCCESS,0),email);
       
        UserModuleView userMod = getUserModuleView(email, app);
        if (userMod == null) 
            return Pair.create(Pair.create(AuthenticationStatus.USER_NOT_ASSOC_WITH_APP,0),email);

        if (userMod.sust_id == null || userMod.sust_id != 0)   
            return Pair.create(Pair.create(AuthenticationStatus.SUBSCRIBER_STATUS_NOT_ACTIVE,0),email);
        if (!userMod.subscriber_has_the_module)   
            return Pair.create(Pair.create(AuthenticationStatus.SUBSCRIBER_NOT_ASSOC_WITH_APP,0),email);
        if (userMod.smgs_id == null || userMod.smgs_id != 0)   
            return Pair.create(Pair.create(AuthenticationStatus.SUBSCRIBER_MODULEGROUP_NOT_ACTIVE,0),email);
        if (!userMod.user_has_the_module)   
            return Pair.create(Pair.create(AuthenticationStatus.USER_NOT_ASSOC_WITH_APP_ALTHOUGH_SUBSCRPTN_OK,0),email);

        Timestamp now = new Timestamp(System.currentTimeMillis());
        if (userMod.sumg_valid_from == null || (now.before(userMod.sumg_valid_from)) || userMod.sumg_valid_until == null || (userMod.sumg_valid_until.before(now))) 
            return Pair.create(Pair.create(AuthenticationStatus.SUBSCRIBER_MODULEGROUP_TEMPRL_MISMATCH,0),email);
        clearOldUserSessions(email, app);
        
        Integer pwdDays = usr.pwdExpirationDays <= passwordExpirationAlertDays ? usr.pwdExpirationDays : 100;
        return Pair.create(Pair.create(AuthenticationStatus.SUCCESS,pwdDays), email);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public Pair<AuthenticationStatus, Integer> getExternalUserAuthenticationStatus(final String email, final String app) {
        //getPasswordPolicyParameters();
        User usr = getUser(email, "-");
        
        if (usr == null)
            return Pair.create(AuthenticationStatus.ACCOUNT_DOESNOT_EXIST, 0);
        if (usr.status == 10){
            if (checkAndSetlockExpired(email)){
                usr.status = 0;
            } else {
                return Pair.create(AuthenticationStatus.USER_LOCKED_BY_ATTEMPTS_LIMIT, 0);
            }
        }
        if (usr.status != 0)
            return Pair.create(AuthenticationStatus.USER_STATUS_NOT_ACTIVE, 0);
        
        Application application = getApplication(app);
        if (application == null)
            return Pair.create(AuthenticationStatus.UNKNOWN_APP, 0);
        if (application.isFree)
            return Pair.create(AuthenticationStatus.SUCCESS, 0);
       
        UserModuleView userMod = getUserModuleView(email, app);
        if (userMod == null) 
            return Pair.create(AuthenticationStatus.USER_NOT_ASSOC_WITH_APP, 0);

        if (userMod.sust_id == null || userMod.sust_id != 0)   
            return Pair.create(AuthenticationStatus.SUBSCRIBER_STATUS_NOT_ACTIVE, 0);
        if (!userMod.subscriber_has_the_module)   
            return Pair.create(AuthenticationStatus.SUBSCRIBER_NOT_ASSOC_WITH_APP, 0);
        if (userMod.smgs_id == null || userMod.smgs_id != 0)   
            return Pair.create(AuthenticationStatus.SUBSCRIBER_MODULEGROUP_NOT_ACTIVE, 0);
        if (!userMod.user_has_the_module)   
            return Pair.create(AuthenticationStatus.USER_NOT_ASSOC_WITH_APP_ALTHOUGH_SUBSCRPTN_OK, 0);

        Timestamp now = new Timestamp(System.currentTimeMillis());
        if (userMod.sumg_valid_from == null || (now.before(userMod.sumg_valid_from)) || userMod.sumg_valid_until == null || (userMod.sumg_valid_until.before(now))) 
            return Pair.create(AuthenticationStatus.SUBSCRIBER_MODULEGROUP_TEMPRL_MISMATCH, 0);

        //clearOldUserSsoSessions(email);
        //clearOldUserSessions(email, app);
        
        
        Integer pwdDays = 100;
        return Pair.create(AuthenticationStatus.SUCCESS, pwdDays);
        
    }    
        
    
    protected UserSession createUserSession(            
            final Integer userId, 
            final String usrEmail,
            final String userVat,
            final Integer subsId,
            final String subsCode,
            final String subsDescription,
            final Integer fromSubsId,
            final String appName,
            final List<String> privileges,
            final List<Integer> subSecClasses,
            final String userSalt,
            final String clientIp) {
        return new UserSession(userId, usrEmail, userVat, subsId, subsCode, subsDescription, fromSubsId, appName, privileges, subSecClasses, userSalt, clientIp);
    }
    
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public UserSession getUserSession(final String sessionId) {
        return 
        saveToDBGeneric(new Func<UserSession>() {@Override public UserSession  lambda() {
            String sqlQuery =   "SELECT distinct s.usrs_app as appName, " +
                                "	case " +
                                "           when ghost_usr.user_id is null or modu.modu_ghost_user_allowed is false or modu.modu_ghost_user_allowed is null then s.usrs_email " +
                                "           else ghost_usr.user_email " +
                                "       end as userEmail, " +
                                "       s.usrs_ip " +
                                "  FROM  subscription.ss_user_sessions s " +
                                "       join subscription.ss_modules modu on modu.modu_name = s.usrs_app " +
                                "       join subscription.ss_users usr on s.usrs_email = usr.user_email " +
                                "       left join subscription.ss_users ghost_usr on usr.user_ghost_user_id = ghost_usr.user_id " +
                                " WHERE " +
                                "   s.usrs_logoutsse IS NULL" +
                                "   AND s.usrs_session = :sessionId ";
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("sessionId", sessionId);
            List<Object[]> users = query.getResultList();
            if (users.isEmpty())
                return null;
            
            if (users.size() == 2) {
                throw new GenericApplicationException("Two many session entries");
            }
            
            Object[] userRow = users.get(0);
            String appName = (String)userRow[0];
            String userEmail=(String)userRow[1];
            String clientIp = (String)userRow[2];

            sqlQuery = " SELECT distinct p.sypr_name as privName " +
                       "   FROM subscription.ssv_user_privilleges  p " +
                       "  WHERE p.modu_name = :moduName" +
                       "    AND p.user_email = :userEmail";
                    
            query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("moduName", appName);
            query.setParameter("userEmail", userEmail);
            List<String> results = query.getResultList();
            if (results.isEmpty())
                return null;
            List<String> privileges = new ArrayList<String>();
            for(String row:results) {
                privileges.add((String)row);
            }
            
            sqlQuery = "select u.user_id as userId, u.subs_id as subsId, u.user_vat as userVar, um.usmo_from_subscriber as fromSubsId, u.user_password_salt as userSalt " +
                        " from subscription.ss_users u " +
                        "   LEFT JOIN (select usmo.user_id, usmo.usmo_from_subscriber " +
                        "                from subscription.ss_user_modules usmo " +
                        "                   join subscription.ss_modules m ON m.modu_id = usmo.modu_id " +
                        "               where m.modu_name = :appName) um ON u.user_id = um.user_id " +
                        " WHERE u.user_email = :userEmail ";
            
            query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("appName", appName);
            query.setParameter("userEmail", userEmail);
            Object[] row = (Object[])query.getSingleResult();
            
            Integer userId = (Integer)row[0];
            Integer subsId = (Integer)row[1];
            String userVat = (String)row[2];
            Integer fromSubsId = (Integer)row[3];
            String userSalt = (String)row[4];
            
            sqlQuery = "SELECT scls_id " +
                       "FROM subscription.ss_subscribers_security_classes " +
                       "WHERE subs_id = :subsId ";
            query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("subsId", subsId);
            List<Integer> subSecClasses = query.getResultList();
            

            sqlQuery = "select s.subs_short_name, s.subs_legal_name "
                    + "from subscription.ss_subscribers s "
                    + "where s.subs_id = :subsId ";
            
            query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("subsId", subsId);
            Object[] subsRow = (Object[])query.getSingleResult();
            
            String subscode = (String)subsRow[0];
            String subsDescription = (String)subsRow[1];
            UserSession ret = createUserSession(userId, userEmail, userVat, subsId, subscode, subsDescription, fromSubsId, appName, privileges, subSecClasses, userSalt, clientIp);
            ret.setSessionId(sessionId);
            return ret;
        }});
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void makeNoteOfSuccessfulLogin(  final String     userName, 
                                            final String     userIp, 
                                            final String     userSessionId, 
                                            final String     ssoSessionId,
                                            final String     appName) 
    {
        saveToDBGeneric(new Action() {@Override public void lambda() {
            String sqlQuery = "INSERT INTO subscription.ss_user_sessions "+
                             "(usrs_email, usrs_ip, usrs_session, usrs_ssosession, usrs_app, usrs_loginsse, dteinsert, usrs_last_ping_timestamp) "+
                             "VALUES(:userName,:userIp,:userSessionId,:ssoSessionId,:appName,:secondsSinceTheEpoch, now(), now())";
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("userName", userName);
            query.setParameter("userIp", userIp);
            query.setParameter("userSessionId", userSessionId);
            query.setParameter("ssoSessionId", ssoSessionId);
            query.setParameter("appName", appName);
            Long secondsSinceTheEpoch = (System.currentTimeMillis() / 1000l);
            query.setParameter("secondsSinceTheEpoch", secondsSinceTheEpoch);
            query.executeUpdate();
        }});
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void makeNoteOfUnSuccessfulLogin(final String     userName, 
                                            final String     userIp, 
                                            final String     appName,
                                            final String     authenticationStatus,
                                            final boolean    statusLocks) 
    {
        saveToDBGeneric(new Action() {@Override public void lambda() {
            String sqlQuery = "INSERT INTO subscription.ss_user_login_attempts "+
                             "(usla_dteattempt, usla_ip, usla_username, usla_app, usla_authentication_status, usla_status_locks) "+
                             "VALUES(now(), :userIp, :userName, :appName, :authenticationStatus, :statusLocks)";
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("userIp", userIp);
            query.setParameter("userName", userName);
            query.setParameter("appName", appName);
            query.setParameter("authenticationStatus", authenticationStatus);
            query.setParameter("statusLocks", statusLocks);
            query.executeUpdate();
        }});
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public boolean checkAndSetlockExpired( final String     userName) 
    {
        return 
        saveToDBGeneric(new Func<Boolean>() {@Override public Boolean lambda() {
            String sqlQuery = "select COALESCE( cast( min(floor(EXTRACT(epoch from (now() - usla.usla_dteattempt) ) / 60)) as INTEGER), cast(0 as integer)) as minutes_locked " +
                              "  from subscription.ss_user_login_attempts usla " +
                              " where usla.usla_username = :userName " +
                              "   and usla.usla_status_locks is true ";
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("userName", userName);
            Integer minutesLocked =  Integer.valueOf(query.getSingleResult().toString());
            Boolean lockExpired = minutesLocked > lockExpirationMinutes;
            
            if (lockExpired) {
                sqlQuery = " update subscription.ss_users " +
                           "    set usst_id = 0, " +
                           "        user_login_attempts = 0 " +
                           "  where usst_id = 10 " +
                           "    and user_email = :userName ";
                Query query2 = getEntityManager().createNativeQuery(sqlQuery);
                query2.setParameter("userName", userName);
                query2.executeUpdate();
            }
            
            return lockExpired;
        }});
    }

    void _logoutUserBySessionId(  final String     sessionId, final Integer logoutCode) 
    {
        saveToDBGeneric(new Action() {@Override public void lambda() {
            //String sqlQuery = "UPDATE subscription.ss_user_sessions SET usrs_logoutsse=:secondsSinceTheEpoch, uslc_id=:logoutCode WHERE usrs_session=:sessionId AND usrs_logoutsse IS NULL";
            String sqlQuery="UPDATE subscription.ss_user_sessions usrs " +
                            "   SET usrs_logoutsse = :secondsSinceTheEpoch, " +
                            "   	   uslc_id = :logoutCode " +
                            "  from (select a.usrs_id " +
                            "          from subscription.ss_user_sessions a " +
                            "         where a.usrs_logoutsse is null " +
                            "           and a.usrs_session = :sessionId  " +
                            "        union " +
                            "        select b.usrs_id " +
                            "          from subscription.ss_user_sessions a " +
                            "            join subscription.ss_user_sessions b " +
                            "                 on b.usrs_app = a.usrs_app " +
                            "                and b.usrs_email = a.usrs_email " +
                            "         where a.usrs_logoutsse is null " +
                            "           and a.usrs_session = :sessionId  " +
                            "           and b.usrs_logoutsse is null " +
                            "           and b.usrs_session = '-' " + // only for external sso Cookies that will never be deleted from browser
                            "           and a.usrs_session <> b.usrs_session) as sessions " +
                            " where usrs.usrs_id = sessions.usrs_id ";
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            Long secondsSinceTheEpoch = (System.currentTimeMillis() / 1000l);
            query.setParameter("secondsSinceTheEpoch", secondsSinceTheEpoch);
            query.setParameter("logoutCode", logoutCode);
            query.setParameter("sessionId", sessionId);
            query.executeUpdate();
        }});
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void logoutUserBySessionId(  final String     sessionId) 
    {
        _logoutUserBySessionId(sessionId, 1);
    }
    
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void userSessionExpired(final String sessionId) 
    {
        _logoutUserBySessionId(sessionId, 2);
    }
    
    
    
    
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public ProducerCheckStatus getProducerStatus(final String producerVat, final String appName, final Integer fromSubsId) {
        return 
        saveToDBGeneric(new Func<ProducerCheckStatus>() {@Override public ProducerCheckStatus  lambda() {
            String sqlQuery = "select count(1) from subscription.ssv_gaiaportal_users u where u.subs_id = 2 and u.user_vat = :producerVat";

            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("producerVat", producerVat);
            BigInteger nCount = (BigInteger)query.getSingleResult();
            if (BigInteger.ZERO.equals(nCount))
                return ProducerCheckStatus.USER_NOT_REGISTERED;
            
            sqlQuery = "select  um.usmo_from_subscriber as fromSubsId "
                    + "from subscription.ss_user_modules um "
                    + "join subscription.ss_users u ON um.user_id = u.user_id "
                    + "JOIN subscription.ss_modules m ON m.modu_id = um.modu_id "
                    + "where "
                    + "	u.user_vat = :producerVat "
                    + " AND m.modu_name = :appName "
                    + "	AND (modu_isfree is true OR (now() <= um.usmo_valid_until)) ";
            
            query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("producerVat", producerVat);
            query.setParameter("appName", appName);
            List<Integer> rows = query.getResultList();
            if (rows.size() == 0)
                return ProducerCheckStatus.USER_NOT_REGISTERED_IN_APP;
            if (!fromSubsId.equals(rows.get(0)))
                return ProducerCheckStatus.USER_NOT_REGISTERED_IN_APP_BY_SUBSCRIBER;
            
            return ProducerCheckStatus.OK;
        }});
    }
    
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public void checkProducerStatus(final String producerVat, final String appName, final String appTitle, final Integer fromSubsId) {
        gr.neuropublic.base.ProducerCheckStatus checkStatus = getProducerStatus(producerVat, appName, fromSubsId);
        if (gr.neuropublic.base.ProducerCheckStatus.USER_NOT_REGISTERED.equals(checkStatus))
            throw new RuntimeException("USER_NOT_REGISTERED('"+producerVat+"')");
        if (gr.neuropublic.base.ProducerCheckStatus.USER_NOT_REGISTERED_IN_APP.equals(checkStatus))
            throw new RuntimeException("USER_NOT_REGISTERED_IN_APP('"+appTitle+"','"+ producerVat +"')");
        if (gr.neuropublic.base.ProducerCheckStatus.USER_NOT_REGISTERED_IN_APP_BY_SUBSCRIBER.equals(checkStatus))
            throw new RuntimeException("USER_NOT_REGISTERED_IN_APP_BY_SUBSCRIBER('"+appTitle+"','" + producerVat + "')");
    }
    
    static class UserModuleView {
        public Short sust_id;
        public Short smgs_id;
        public Boolean subscriber_has_the_module;
        public Boolean user_has_the_module;
        public Timestamp sumg_valid_from;
        public Timestamp sumg_valid_until;
    }
    
    static class User {
        public String subsCode;
        public String encryptedPassword;
        public String salt;
        public Short status;
        public Integer pwdExpirationDays;
        public Integer pwdAlgorithm;
        public String encryptedInsertedPwd;
        boolean checkPassword(String rawPassword) {
            if (pwdAlgorithm != null && pwdAlgorithm != 1){
                return encryptedInsertedPwd.equals(encryptedPassword);
            }
            Sha256Hash hash = new Sha256Hash(rawPassword,salt, 1000000);
            return hash.toHex().equals(encryptedPassword);
        }
    }

    static class Application {
        public Boolean isFree;
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public gr.neuropublic.base.User getUserById(final Integer userId) {
        return 
        saveToDBGeneric(new Func<gr.neuropublic.base.User>() {@Override public gr.neuropublic.base.User  lambda() {

            String sqlQuery =   " SELECT internal_user_name, public_user_name, user_active_email, " +
                                "        tin, lastname, firstname, fathername, subs_id, subs_code, subs_description " +
                                "   FROM subscription.ssv_user_by_id usr " +
                                "  WHERE usr.user_id = :userId ";
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("userId", userId);
            List<Object[]> users = query.getResultList();
            if (users.isEmpty())
                return null;
            
            if (users.size() == 2) {
                throw new GenericApplicationException("Two many users return by user_id");
            }
            
            Object[] userRow = users.get(0);
           
            String internalUserName = (String)userRow[0];
            String publicUserName = getUsersMappingPlatform() != null ? (String)userRow[1] : internalUserName; 
            String activeEmail = (String)userRow[2];
            String tin = (String)userRow[3]; 
            String lastname = (String)userRow[4]; 
            String firstname = (String)userRow[5];
            String fathername = (String)userRow[6];
            Integer subsId = (Integer)userRow[7];
            String subsCode = (String)userRow[8];
            String subsDescription = (String)userRow[9];
                    
            gr.neuropublic.base.User ret = new gr.neuropublic.base.User(userId, internalUserName, publicUserName, activeEmail, tin, 
                lastname, firstname, fathername, subsId, subsCode, subsDescription);
            return ret;
        }});
    }

    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public SqlQueryRow getAuditInfoUsers(final String userInsert, final String userUpdate, final boolean showUsernameIfNotFound, final boolean showUsername) {
        return 
        saveToDBGeneric(new Func<SqlQueryRow>() {@Override public SqlQueryRow lambda() {

            SqlQueryRow ret = new SqlQueryRow();
            String userInsertLastName = showUsernameIfNotFound && (userInsert != null) ? userInsert : "";
            String userInsertFirstName = "";
            String userInsertUsername = "";
            String userUpdateLastName = showUsernameIfNotFound && (userUpdate != null) ? userUpdate : "";
            String userUpdateFirstName = "";
            String userUpdateUsername = "";
            
            String sqlQuery =   " select usr.user_email, usr.user_surname, usr.user_firstname " +
                                "   from subscription.ss_users usr " +
                                "  where usr.user_email in (:userInsert, :userUpdate) ";
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("userInsert", userInsert);
            query.setParameter("userUpdate", userUpdate);
            List<Object[]> users = query.getResultList();
            for (Object[] row : users) {
                if (((String)row[0]).equals(userInsert)) {
                    userInsertLastName = row[1] != null ? (String)row[1] : "";
                    userInsertFirstName = row[2] != null ? (String)row[2] : "";
                    userInsertUsername = showUsername ? userInsert : "";
                }
                if (((String)row[0]).equals(userUpdate)) {
                    userUpdateLastName = row[1] != null ? (String)row[1] : "";
                    userUpdateFirstName = row[2] != null ? (String)row[2] : "";
                    userUpdateUsername = showUsername ? userUpdate : "";
                }
            }
            
            ret.put("userInsertLastName", userInsertLastName);
            ret.put("userInsertFirstName", userInsertFirstName);
            ret.put("userInsertUsername", userInsertUsername);
            ret.put("userUpdateLastName", userUpdateLastName);
            ret.put("userUpdateFirstName", userUpdateFirstName);
            ret.put("userUpdateUsername", userUpdateUsername);
            
            return ret;
        }});
    }
    
    
    public String createExternalUserBy_Tin_SubsCode(
            final String appName, 
            final String subsCode, 
            final String userRole, 
            final String userTin, 
            final String userInsert, 
            final String userEmail, 
            final String userFirstName, 
            final String userLastName, 
            final String userFatherName, 
            final String userPhone, 
            final String userPhoneCell, 
            final String userIdentityNumber, 
            final String userAddress, 
            final String userAddressNumber, 
            final String userPostalCode, 
            final String userCity, 
            final String userPrefecture, 
            final String userCountry) {
        return
        saveToDBGeneric( new Func<String>() {@Override public String lambda() {
            // Create User and add module and role if not exists
            String sqlQuery =
                "select cast(subscription.create_external_user_by_tin("
                 + "CAST(:s_user_module AS character varying), "
                 + "CAST(:s_user_subs_code AS character varying), "
                 + "CAST(:s_user_role AS character varying), "
                 + "CAST(:s_user_tin AS character varying), "
                 + "CAST(:s_usrinsert AS character varying), "
                 + "CAST(:s_user_email AS character varying), "
                 + "CAST(:s_user_firstname AS character varying), "
                 + "CAST(:s_user_surname AS character varying), "
                 + "CAST(:s_user_fathername AS character varying), "
                 + "CAST(:s_user_phone AS character varying), "
                 + "CAST(:s_user_phone_cell AS character varying), "
                 + "CAST(:s_user_identity_number AS character varying), "
                 + "CAST(:s_user_address AS character varying), "
                 + "CAST(:s_user_address_number AS character varying), "
                 + "CAST(:s_user_postal_code AS character varying), "
                 + "CAST(:s_user_city AS character varying), "
                 + "CAST(:s_user_prefecture AS character varying), "
                 + "CAST(:s_user_country AS character varying) "
                 + ") as varchar)";

            Query upQquery = getEntityManager().createNativeQuery(sqlQuery);

    		upQquery.setParameter("s_user_module", appName);
    		upQquery.setParameter("s_user_subs_code", subsCode);
    		upQquery.setParameter("s_user_role", userRole);
    		upQquery.setParameter("s_user_tin", userTin);
    		upQquery.setParameter("s_usrinsert", userInsert);
    		upQquery.setParameter("s_user_email", userEmail);
    		upQquery.setParameter("s_user_firstname", userFirstName);
    		upQquery.setParameter("s_user_surname", userLastName);
    		upQquery.setParameter("s_user_fathername", userFatherName);
    		upQquery.setParameter("s_user_phone", userPhone);
    		upQquery.setParameter("s_user_phone_cell", userPhoneCell);
    		upQquery.setParameter("s_user_identity_number", userIdentityNumber);
    		upQquery.setParameter("s_user_address", userAddress);
    		upQquery.setParameter("s_user_address_number", userAddressNumber);
    		upQquery.setParameter("s_user_postal_code", userPostalCode);
    		upQquery.setParameter("s_user_city", userCity);
    		upQquery.setParameter("s_user_prefecture", userPrefecture);
    		upQquery.setParameter("s_user_country", userCountry);
                
            upQquery.getResultList();

            // Get created user
            sqlQuery =  " select usr.user_email " +
                        "  from subscription.ss_users usr " +
                        "        join subscription.ss_subscribers subs using(subs_id) " +
                        " where subs.subs_short_name = :subsCode " +
                        "   and usr.user_vat = :userTin ";
            Query query = getEntityManager().createNativeQuery(sqlQuery);
            query.setParameter("subsCode", subsCode);
            query.setParameter("userTin", userTin);
            String loginName = query.getSingleResult().toString();
            
            return loginName;
        }});        
        
    }    
    
    
    /**
     ***************************************************************************************
     *  SOAP HANDLING
     ***************************************************************************************
     */
    
    private static final QName QNAME_WSSE_USERNAMETOKEN = new QName( XMLConstants.DEFAULT_NS_PREFIX, "UsernameToken");
    private static final QName QNAME_WSSE_USERNAME = new QName( XMLConstants.DEFAULT_NS_PREFIX, "Username");
    private static final QName QNAME_WSSE_PASSWORD = new QName( XMLConstants.DEFAULT_NS_PREFIX, "Password");

    /**
     * Entry point.
     *
     * if false the request returns SOAP Error
     * if true the request is delegated to web service method
     *
     * @param context
     * @return
     */
    @Override
    public boolean handleMessage(SOAPMessageContext context) {

        Boolean outbound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        //if message is inbound
        if ((outbound != null) && (!outbound.booleanValue())) {
            return handleInboundMessage(context);
        }

        return true;
    }

    /**
     * Reads username password from SOAP Header and authenticate user
     *
     * @param context
     * @return
     */
    private boolean handleInboundMessage(SOAPMessageContext context) {

        //Get credential data from soap header
        WsCall ret = getUsernamePasswordFromHeader(context);

        if (ret == null || ret.userName == null || ret.appName == null || ret.userPass == null || ret.wsName == null) {
            generateSOAPErrMessage(context.getMessage(), "102", "AUTHENTICATION_ERROR", "Invalid SOAP Header structure");
            return false;
        }
        Pair<AuthenticationStatus, Integer> authStatus;
        authStatus = getUserAuthenticationStatusFromSoap(ret.userName, ret.userPass,  ret.appName, ret.appName + "_" + ret.wsName, false, false);
        if (!(authStatus.a == AuthenticationStatus.SUCCESS)) {
            generateSOAPErrMessage(context.getMessage(), "101", "ACCESS_DENIED", authStatus.a.toString());
            return false;
        }

        context.put("userName",ret.userName);
        context.setScope("userName", MessageContext.Scope.APPLICATION);

        return true;
    }

    public abstract String getAppName();

    /**
     * returns an array with username and password
     *
     * @param context
     * @return
     * @throws SOAPException
     */
    
    public static class WsCall {
        public String appName;
        public String wsName;
        public String userName;
        public String userPass;
    }
    private WsCall getUsernamePasswordFromHeader(SOAPMessageContext context) {

        try {
            WsCall ret = new WsCall();

            //List<String> returnList = new ArrayList<>();
            if (context == null || context.getMessage() ==null || context.getMessage().getSOAPBody() == null) {
                return null;
            }
            
            Iterator<?> itBody = context.getMessage().getSOAPBody().getChildElements();

            while (itBody.hasNext()) {
                Object isBody = itBody.next();
                if (isBody instanceof SOAPElement) {
                    SOAPElement body = (SOAPElement)isBody;
                    ret.appName = getAppName();
                    ret.wsName = body.getElementName().getLocalName();
                    break;
                }
            }
            
            
            SOAPHeader header = context.getMessage().getSOAPHeader();
            Iterator<?> headerElements = header.examineAllHeaderElements();
            while (headerElements.hasNext()) {
                SOAPHeaderElement headerElement = (SOAPHeaderElement) headerElements
                        .next();
                if (headerElement.getElementName().getLocalName()
                        .equals("Security")) {
                    SOAPHeaderElement securityElement = headerElement;
                    Iterator<?> it2 = securityElement.getChildElements();
                    while (it2.hasNext()) {
                        Node soapNode = (Node) it2.next();
                        if (soapNode instanceof SOAPElement) {
                            SOAPElement element = (SOAPElement) soapNode;
                            QName elementQname = element.getElementQName();
                            if (QNAME_WSSE_USERNAMETOKEN.getLocalPart().equals(elementQname.getLocalPart())) {
                                SOAPElement usernameTokenElement = element;
                                ret.userName = getFirstChildElementValue(usernameTokenElement, QNAME_WSSE_USERNAME);
                                ret.userPass = getFirstChildElementValue(usernameTokenElement, QNAME_WSSE_PASSWORD);
                                break;
                            }
                        }

                    }
                }
            }

            //if list size is not 2 the header parsing has failed


            return ret;

        } catch (SOAPException e) {

            generateSOAPErrMessage(context.getMessage(), "102", "AUTHENTICATION_ERROR", "Invalid SOAP Header structure");
            return null;
        }
    }

    /**
     * Returns value for specific XML element
     *
     * @param soapElement
     * @param qNameToFind
     * @return
     */
    private String getFirstChildElementValue(SOAPElement soapElement, QName qNameToFind) {

        String value = null;
        Iterator<?> it2 = soapElement.getChildElements();
        while (it2.hasNext()) {
            Node soapNode = (Node) it2.next();
            if (soapNode instanceof SOAPElement) {
                SOAPElement element = (SOAPElement) soapNode;
                QName elementQname = element.getElementQName();
                if (qNameToFind.getLocalPart().equals(elementQname.getLocalPart())) {

                    value = element.getValue();
                    break;
                }
            }
        }

        return value;
    }

    /**
     * Generate a SOAP error message
     *
     * @param msg
     *            The SOAP message
     * @param code
     *            The error code
     * @param reason
     *            The reason for the error
     */
    private void generateSOAPErrMessage(SOAPMessage msg, String code, String reason, String detail) {

        try {
            SOAPBody soapBody = msg.getSOAPPart().getEnvelope().getBody();
            SOAPFault soapFault = soapBody.addFault();
            soapFault.setFaultCode(code);
            soapFault.setFaultString(reason);

            // Manually crate a failure element in order to guarentee that this
            // authentication handler returns the same type of soap fault as the
            // rest
            // of the application
            QName failureElement = new QName("http://neuropublic.gr/namespaces/np", "Failure", "np");
            QName codeElement = new QName("http://neuropublic.gr/namespaces/np","Code","np");
            QName reasonElement = new QName("http://neuropublic.gr/namespaces/np","Reason","np");
            QName detailElement = new QName("http://neuropublic.gr/namespaces/np","Detail","np");

            soapFault.addDetail().addDetailEntry(failureElement)
                    .addChildElement(codeElement).addTextNode(code)
                    .getParentElement().addChildElement(reasonElement)
                    .addTextNode(reason).getParentElement()
                    .addChildElement(detailElement).addTextNode(detail);

            throw new SOAPFaultException(soapFault);

        } catch (SOAPException e) {

            logger.error("SOAPException during setting SOAP Fault :"+e.getMessage());
        }
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        return false;
    }

    @Override
    public void close(MessageContext context) {
    }

    @Override
    public Set<QName> getHeaders() {
        return null;
    }
    
    
}
