package gr.neuropublic.base;

import gr.neuropublic.persutil.JPUtil;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;
import java.math.BigInteger;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.PostActivate;
import javax.ejb.PrePassivate;
import gr.neuropublic.exceptions.DatabaseConstraintViolationException;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.exceptions.ValidationException;
import java.util.ArrayList;
import java.util.Set;
import javax.ejb.Remove;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.validation.ConstraintViolation;
import org.hibernate.exception.ConstraintViolationException;

import static gr.neuropublic.mutil.base.Util.castToShortWithChecks;

public abstract class AbstractFacade<T extends gr.neuropublic.validators.ICheckedEntity> implements IFacade<T> {

    private static final Logger logger = Logger.getLogger(AbstractFacade.class.getName());

    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    protected abstract EntityManager getEntityManager();


    public abstract T initRow();
	
	

}