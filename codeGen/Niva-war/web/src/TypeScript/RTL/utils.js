/// <reference path="../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="npTypes.ts"/>
/// <reference path="services.ts" />
/// <reference path="Controllers/controllerInput.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Tuple2 = (function () {
    function Tuple2(a, b) {
        this.a = a;
        this.b = b;
    }
    return Tuple2;
})();
var Tuple3 = (function () {
    function Tuple3(a, b, c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    return Tuple3;
})();
var SelectItem = (function () {
    function SelectItem(value, label) {
        this.value = value;
        this.label = label;
    }
    return SelectItem;
})();
var ExpiredArrayResource = (function () {
    function ExpiredArrayResource() {
        this.dictionary = {};
    }
    ExpiredArrayResource.prototype.getArrayResource = function (key, closure, timeoutInMilliseconds) {
        if (timeoutInMilliseconds === void 0) { timeoutInMilliseconds = 1800000000; }
        var self = this;
        var ret = self.dictionary[key];
        var currentTimeInMs = (new Date).getTime();
        if ((ret !== undefined) && (currentTimeInMs < ret.a + timeoutInMilliseconds)) {
            return ret.b;
        }
        var newArray = [];
        self.dictionary[key] = new Tuple2(currentTimeInMs, newArray);
        closure(newArray);
        return newArray;
    };
    return ExpiredArrayResource;
})();
var Utils;
(function (Utils) {
    Utils.timeoutForWaitDialogsAndInterclickDelaysInMilliseconds = 2000;
    function getTimeInMS() { return (new Date).getTime(); }
    Utils.getTimeInMS = getTimeInMS;
    function convertStringToDate(dateValue, format) {
        return Date.parseString(dateValue, format);
    }
    Utils.convertStringToDate = convertStringToDate;
    function convertDateToString(dateValue, dateFormat) {
        return dateValue.format(dateFormat);
    }
    Utils.convertDateToString = convertDateToString;
    function replaceHtmlChars(htmlString) {
        if (isVoid(htmlString))
            return htmlString;
        return htmlString
            .replace(new RegExp("<br/>", 'g'), " ")
            .replace(new RegExp("&#160;", 'g'), " ")
            .replace(new RegExp("&quot;", 'g'), "\"")
            .replace(new RegExp("&amp;", 'g'), "&")
            .replace(new RegExp("&#39;", 'g'), "'")
            .replace(new RegExp("&lt;", 'g'), "<")
            .replace(new RegExp("&gt;", 'g'), ">");
    }
    Utils.replaceHtmlChars = replaceHtmlChars;
    function AddToArrayIfNotExistsAndReturnIt(list, item) {
        var cachedSE = _.find(list, function (iterSE) {
            return iterSE.getKey() === item.getKey();
        });
        if (cachedSE !== undefined)
            return cachedSE;
        list.push(item);
        return list.slice(-1)[0];
    }
    Utils.AddToArrayIfNotExistsAndReturnIt = AddToArrayIfNotExistsAndReturnIt;
    function showWaitWindow(scope, Plato, $timeout, immediate) {
        if (immediate === void 0) { immediate = false; }
        var isFirstWaitDialog = scope.globals.nInFlightRequests === 0;
        $timeout(function () {
            if (isFirstWaitDialog) {
                var dialogOptions = new NpTypes.DialogOptions();
                dialogOptions.width = '20em';
                dialogOptions.autoOpen = true;
                dialogOptions.className = 'plzWaitDialog';
                dialogOptions.showCloseButton = false;
                $timeout(function () {
                    if (scope.globals.nInFlightRequests >= 1) {
                        Plato.showDialog(scope.$root, 'WaitDialog_Title', "/" + scope.globals.appName + '/partials/wait.html?rev=' + scope.globals.version, dialogOptions);
                    }
                }, 0);
            }
        }, immediate ? 0 : Utils.timeoutForWaitDialogsAndInterclickDelaysInMilliseconds);
        scope.globals.nInFlightRequests++;
    }
    Utils.showWaitWindow = showWaitWindow;
    function showExcelFileImportResultsDlg($scope, Plato, excelFileId, excelFileEntityName, excelErrorEntityName) {
        var dialogOptions = new NpTypes.DialogOptions();
        dialogOptions.title = "ImportExvel_ResultsDlg_Title";
        dialogOptions.width = "800px";
        dialogOptions.className = "PageController";
        dialogOptions.customParams = {
            'excelFileId': excelFileId,
            'ExcelFileEntityName': excelFileEntityName,
            'ExcelErrorEntityName': excelErrorEntityName
        };
        Plato.showDialog($scope, dialogOptions.title, "/" + $scope.globals.appName + '/partials/ExcelFileImportResultsDlg.html?rev=' + $scope.globals.version, dialogOptions);
    }
    Utils.showExcelFileImportResultsDlg = showExcelFileImportResultsDlg;
    function UpdateMainWindowWidthAndMainButtonPlacement($scope) {
        var element = $('.resizable1')[0];
        var navigationWidth = element.offsetWidth;
        var parentWidth = element.parentElement.offsetWidth;
        var remainingSpace = parentWidth - navigationWidth;
        var resizable2Elem = $('.resizable2')[0];
        //var main = $((<any>element).nextElementSibling);
        var main = resizable2Elem != undefined ? $(resizable2Elem) : $(element.nextElementSibling);
        main.css("left", navigationWidth);
        main.css("width", remainingSpace);
        $('.TopFormArea').css('width', 'calc( 100% - ' + (navigationWidth + 17) + 'px )'); // 17 for the horizontal sbar
        $('.fixedContainer').css('width', (parseInt($('.TopFormArea').css('width')) - 52) + 'px'); // subtract 52px for scrollbar buttons
        // Trigger resizes of all grids
        $scope.modelNavigation.epochOfLastResize = Utils.getTimeInMS();
    }
    Utils.UpdateMainWindowWidthAndMainButtonPlacement = UpdateMainWindowWidthAndMainButtonPlacement;
    function precise_round(num, decimals, defIfNan) {
        if (defIfNan === void 0) { defIfNan = 0; }
        if (num === undefined || num === null) {
            return defIfNan;
        }
        if (!isNaN(num)) {
            return Math.round(num * Math.pow(10, decimals)) / Math.pow(10, decimals);
        }
        return defIfNan;
    }
    Utils.precise_round = precise_round;
})(Utils || (Utils = {}));
var IconKind;
(function (IconKind) {
    IconKind[IconKind["INFO"] = 0] = "INFO";
    IconKind[IconKind["WARNING"] = 1] = "WARNING";
    IconKind[IconKind["ERROR"] = 2] = "ERROR";
    IconKind[IconKind["QUESTION"] = 3] = "QUESTION";
})(IconKind || (IconKind = {}));
var MsgBoxDialogOptions = (function (_super) {
    __extends(MsgBoxDialogOptions, _super);
    function MsgBoxDialogOptions() {
        _super.apply(this, arguments);
        this.message = "Default message";
        this.iconKind = IconKind.INFO;
        this.buttons = [];
    }
    return MsgBoxDialogOptions;
})(NpTypes.DialogOptions);
function isVoid(x) {
    if (x === undefined || x === null)
        return true;
    return false;
}
function messageBox($scope, Plato, titleMessage, message, iconKind, buttons, indexSelected, indexCancel, width) {
    if (indexSelected === void 0) { indexSelected = undefined; }
    if (indexCancel === void 0) { indexCancel = undefined; }
    if (width === void 0) { width = '30em'; }
    var dialogOptions = new MsgBoxDialogOptions();
    dialogOptions.width = width;
    dialogOptions.autoOpen = true;
    dialogOptions.message = message;
    dialogOptions.iconKind = iconKind;
    dialogOptions.buttons = buttons;
    dialogOptions.indexSelected = indexSelected;
    dialogOptions.indexCancel = indexCancel;
    dialogOptions.showCloseButton = (indexCancel !== undefined);
    dialogOptions.width = width;
    dialogOptions.className = "MsgBoxClass";
    Plato.showDialog($scope, titleMessage, "/" + $scope.globals.appName + '/partials/messageBox.html?rev=' + $scope.globals.version, dialogOptions);
    return dialogOptions;
}
function supports_html5_storage() {
    try {
        return 'sessionStorage' in window && window['sessionStorage'] !== null;
    }
    catch (e) {
        return false;
    }
}
function getNumberOfWatchers() {
    var root = $(document.getElementsByTagName('body'));
    var watchers = [];
    var f = function (element) {
        if (element.data().hasOwnProperty('$scope')) {
            angular.forEach(element.data().$scope.$$watchers, function (watcher) {
                watchers.push(watcher);
            });
        }
        angular.forEach(element.children(), function (childElement) {
            f($(childElement));
        });
    };
    f(root);
    return watchers.length;
}
;
// Dynamically generated input dialogs
var InputDialogVariable = (function () {
    function InputDialogVariable(config) {
        var _this = this;
        ['varName', 'label', 'initial', 'required', 'validateFunc'].forEach(function (name) {
            _this[name] = config[name];
        });
    }
    return InputDialogVariable;
})();
var InputDialogText = (function (_super) {
    __extends(InputDialogText, _super);
    function InputDialogText(config) {
        _super.call(this, config);
    }
    return InputDialogText;
})(InputDialogVariable);
var InputDialogNumber = (function (_super) {
    __extends(InputDialogNumber, _super);
    function InputDialogNumber(config) {
        var _this = this;
        _super.call(this, config);
        ['min', 'max', 'decimals', 'decSep', 'thSep'].forEach(function (name) {
            _this[name] = config[name];
        });
    }
    return InputDialogNumber;
})(InputDialogVariable);
var InputDialogDate = (function (_super) {
    __extends(InputDialogDate, _super);
    function InputDialogDate(config) {
        var _this = this;
        _super.call(this, config);
        ['required', 'validateFunc', 'format', 'mindate', 'maxdate'].forEach(function (name) {
            _this[name] = config[name];
        });
    }
    return InputDialogDate;
})(InputDialogVariable);
var InputDialogOptions = (function (_super) {
    __extends(InputDialogOptions, _super);
    function InputDialogOptions() {
        _super.apply(this, arguments);
    }
    return InputDialogOptions;
})(NpTypes.DialogOptions);
var g_countDialogs = 0;
function inputDialog($scope, Plato, strTitle, htmlContentIntro, variables, buttons, indexSelected, indexCancel, width) {
    if (indexSelected === void 0) { indexSelected = undefined; }
    if (indexCancel === void 0) { indexCancel = undefined; }
    if (width === void 0) { width = '30em'; }
    g_countDialogs += 1;
    var dialogOptions = new InputDialogOptions();
    dialogOptions.width = width;
    dialogOptions.autoOpen = true;
    dialogOptions.className = "GenericInputDialog";
    dialogOptions.variables = variables;
    dialogOptions.buttons = buttons;
    dialogOptions.indexSelected = indexSelected;
    dialogOptions.indexCancel = indexCancel;
    dialogOptions.showCloseButton = false;
    dialogOptions.width = width;
    dialogOptions.countDialogs = g_countDialogs;
    var html = "<div data-ng-controller='controllerInput' style='margin: 0.5em 1em;'>"
        + "<div data-on-key-down='triggerCancel()' data-keys='[27]'>"
        + htmlContentIntro;
    var error;
    var validators = [];
    variables.forEach(function (v) {
        if (!isVoid(error))
            return;
        if (isVoid(v.varName)) {
            error = "Wrong usage of inputDialog: variable name is undefined...";
            return;
        }
        var modelVarname = 'model.' + v.varName;
        var modelUIVarname = 'model._' + v.varName;
        var targetDirective;
        var extras = Sprintf.sprintf(" data-np-required='%s' ", v.required ? "true" : "false");
        if (!isVoid(v.validateFunc)) {
            validators.push('validate_' + v.varName);
            extras += Sprintf.sprintf(" data-np-validate='validate_%s' ", v.varName);
        }
        if (v instanceof InputDialogText) {
            targetDirective = 'text';
        }
        else if (v instanceof InputDialogNumber) {
            targetDirective = 'number';
            ['min', 'max', 'decimals', 'decSep', 'thSep'].forEach(function (option) {
                if (!isVoid(v[option]))
                    extras += Sprintf.sprintf(" data-np-%s='%s' ", option, v[option]);
            });
        }
        else if (v instanceof InputDialogDate) {
            targetDirective = 'date';
            ['format', 'min-date', 'max-date'].forEach(function (option) {
                var accessor = option.replace('-', '_');
                if (!isVoid(v[accessor]))
                    extras += Sprintf.sprintf(" data-np-%s='%s' ", option, v[accessor]);
            });
        }
        else {
            error = "Only a descendant class of InputDialogType can be passed to inputDialog";
            return;
        }
        html += Sprintf.sprintf("\n<br><span><b>%s</b></span>", v.label);
        html += Sprintf.sprintf("\n<input data-np-%s='dummy' data-ng-model='%s' data-np-ui-model='%s' ", targetDirective, modelVarname, modelUIVarname);
        html += extras + ">";
        html += "\n<br>";
    });
    if (!isVoid(error)) {
        alert(error);
        return;
    }
    html += "\n&nbsp;<br>";
    var count = 0;
    buttons.forEach(function (v) {
        var suffix = Sprintf.sprintf("_%d_%d", dialogOptions.countDialogs, count);
        html += Sprintf.sprintf("\n<button id='autoBtn_%s' %s data-ng-click='click_%s()'>%s</button>", suffix, (count === dialogOptions.indexSelected) ? " data-ng-disabled='!model.allOK' " : "", suffix, v.a);
        count += 1;
    });
    html += "\n</div>\n</div>";
    Plato.showDialogHTML($scope, strTitle, html, 'controllerInput', Controllers.controllerInput, dialogOptions);
    return dialogOptions;
}
function fallback(x, v) {
    return isVoid(x) ? v : x;
}
//# sourceMappingURL=utils.js.map