/// <reference path="../DefinitelyTyped/jquery/jquery.d.ts" />
/// <reference path="../DefinitelyTyped/underscore/underscore.d.ts" />
function ngGridFlexibleHeightPlugin(opts) {
    var self = this;
    self.grid = null;
    self.scope = null;
    self.opts = opts;
    self.init = function (scope, grid, services) {
        self.domUtilityService = services.DomUtilityService;
        self.grid = grid;
        self.scope = scope;
        var recalcHeightForData = function () {
            setTimeout(innerRecalcForData, 1);
        };
        var innerRecalcForData = function () {
            var gridId = self.grid.gridId;
            var footerPanelSel = '.' + gridId + ' .ngFooterPanel';
            var extraHeight = self.grid.$topPanel.height() + $(footerPanelSel).height();
            var naturalHeight = self.grid.$canvas.height() + 1;
            if (self.opts !== null) {
                if (self.opts.minHeight !== null && (naturalHeight + extraHeight) < self.opts.minHeight) {
                    naturalHeight = self.opts.minHeight - extraHeight - 2;
                }
                if (!isVoid(self.opts.maxViewPortHeight)) {
                    naturalHeight = Math.min(naturalHeight, self.opts.maxViewPortHeight);
                }
            }
            var totalColumnsWidth = _.reduce(scope.$parent.columns, function (memo, column) {
                return memo + column.width;
            }, 0);
            var newViewportHeight = naturalHeight + 17;
            if (!self.scope.baseViewportHeight || self.scope.baseViewportHeight !== newViewportHeight) {
                self.grid.$viewport.css('height', newViewportHeight + 'px');
                self.grid.$root.css('height', (newViewportHeight + extraHeight) + 'px');
                self.scope.baseViewportHeight = newViewportHeight;
                self.domUtilityService.UpdateGridLayout(self.scope, self.grid);
            }
        };
        self.scope.catHashKeys = function () {
            var hash = '', idx;
            for (idx in self.scope.renderedRows) {
                hash += self.scope.renderedRows[idx].$$hashKey;
            }
            return hash;
        };
        self.scope.$watch('catHashKeys()', innerRecalcForData);
        self.scope.$watch(self.grid.config.data, recalcHeightForData);
    };
}
//# sourceMappingURL=ng-grid-flexible-height.js.map