/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />
/**
 * p.tsagkis 4/2016
 * options related to the export format
 * of uor map. Possible values pdf,kml,jpeg,png,geotiff
 */
module Controllers {

    export class MapExportModel {
      
    }

    export interface MapExportScope extends NpTypes.IApplicationScope {
        model: MapExportModel;
        exportFunc: ()=>void;
        nevermind: ()=>void;
    }

    export class controllerExportMap {

        constructor(public $scope: MapExportScope, $timeout:ng.ITimeoutService) {
            var dialogOptions: NpTypes.DialogOptions =
                <NpTypes.DialogOptions>$scope.globals.findAndRemoveDialogOptionByClassName("MapExportDialog");
            var npMapThis: NpGeo.NpMap = (<any>dialogOptions).npMapThis;
            var self = this;
            $scope.model = new MapExportModel();

            $scope.nevermind = function() {
                dialogOptions.jquiDialog.dialog("close");
            }
            $scope.exportFunc = function () {
                var elm = document.getElementById("mapoutputformattype");
                var fileExt = "";
                var format = (<any>elm).value;
                if (format === 'kml') {
                    fileExt = format;
                } else {
                    fileExt = format.split("/")[1];
                }
                npMapThis.executeInlineWms(format, fileExt);
                dialogOptions.jquiDialog.dialog("close");
            }
        }
    }
}
