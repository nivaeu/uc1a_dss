/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />
var Controllers;
(function (Controllers) {
    var LayerExclusionState = (function () {
        function LayerExclusionState(name, enabled, sqlLayer) {
            this.name = name;
            this.enabled = enabled;
            this.sqlLayer = sqlLayer;
        }
        return LayerExclusionState;
    })();
    Controllers.LayerExclusionState = LayerExclusionState;
    var LayerExclusionModel = (function () {
        function LayerExclusionModel() {
            this.layers = [];
        }
        return LayerExclusionModel;
    })();
    Controllers.LayerExclusionModel = LayerExclusionModel;
    var controllerExclusiveLayers = (function () {
        function controllerExclusiveLayers($scope, $timeout) {
            this.$scope = $scope;
            var dialogOptions = $scope.globals.findAndRemoveDialogOptionByClassName("ChooseExclusiveLayersDialog");
            var self = this;
            var npMapThis = dialogOptions.npMapThis;
            $scope.model = new LayerExclusionModel();
            $scope.model.layers =
                npMapThis.layers.
                    filter(function (l) { return l instanceof NpGeoLayers.SqlVectorLayer; }).
                    filter(function (l) {
                    if (l.exclusive === true || l.exclusive === undefined)
                        return true;
                    // return  l.exclusive === undefined;
                }).
                    map(function (l) {
                    var isUsedForExclusions = NpGeoLayers.getSetting(l.layerId + l.label, "isUsedForExclusions", true);
                    l.setUsedForExclusions(isUsedForExclusions);
                    return new LayerExclusionState(l.label, isUsedForExclusions, l);
                });
            $scope.nevermind = function () {
                dialogOptions.jquiDialog.dialog("close");
            };
            $scope.change = function (l) {
                l.sqlLayer.setUsedForExclusions(l.enabled); // Update sessionStorage info
            };
        }
        return controllerExclusiveLayers;
    })();
    Controllers.controllerExclusiveLayers = controllerExclusiveLayers;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerExclusiveLayers.js.map