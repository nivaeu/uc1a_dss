/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />
/**
 * p.tsagkis 6/2016
 * display a window containing legend data
 * of the map
 */
var Controllers;
(function (Controllers) {
    var LegendRow = (function () {
        function LegendRow(title, color) {
            this.title = title;
            this.color = color;
        }
        return LegendRow;
    })();
    Controllers.LegendRow = LegendRow;
    var LegendModel = (function () {
        function LegendModel() {
            this.rows = [];
        }
        return LegendModel;
    })();
    Controllers.LegendModel = LegendModel;
    var controllerLegend = (function () {
        function controllerLegend($scope, $timeout) {
            this.$scope = $scope;
            var dialogOptions = $scope.globals.findAndRemoveDialogOptionByClassName("LegendDialog");
            var npMapThis = dialogOptions.npMapThis;
            var self = this;
            var legendData = dialogOptions.legendData;
            //console.log("legendData3", legendData);
            var controllerEntries = legendData[0].entries;
            var legendHtmlTbl = "<table>";
            for (var i = 0; i < controllerEntries.length; i++) {
                legendHtmlTbl = legendHtmlTbl + "<tr><td colspan='2'><b>" + controllerEntries[i].title + "</b></td></tr>";
                var childEntries = controllerEntries[i].childs;
                for (var f = 0; f < childEntries.length; f++) {
                    //console.log("childEntries[f].color=====", NpGeoGlobals.rgba2hexAndOpacity(childEntries[f].color));
                    legendHtmlTbl = legendHtmlTbl +
                        "<tr>" +
                        "<td>" + childEntries[f].title + "</td>" +
                        //"<td style=\"background-color:" + NpGeoGlobals.rgba2hexAndOpacity(childEntries[f].color)[0].hex + "; border: 1px solid #000000; ; text-align:center;  height: 50px; width: 50px;\"></td>" +
                        "<td style=\"background-color:" + childEntries[f].color + "; border: 10px solid #eee; text-align:center;  height: 40px; width: 40px;\"></td>" +
                        "</tr>";
                }
            }
            legendHtmlTbl = legendHtmlTbl + "</table>";
            document.getElementById("legenddivid").innerHTML = legendHtmlTbl;
            $scope.model = new LegendModel();
            $scope.model.rows = legendData[0].entries;
            $scope.nevermind = function () {
                dialogOptions.jquiDialog.dialog("close");
            };
        }
        return controllerLegend;
    })();
    Controllers.controllerLegend = controllerLegend;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerLegend.js.map