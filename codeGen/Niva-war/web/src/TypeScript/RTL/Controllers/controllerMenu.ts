﻿/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../npTypes.ts" />
/// <reference path="../services.ts" />

module Controllers {

    export interface IMenuScope extends NpTypes.IApplicationScope {
        getSelectedClass(current: number): string;
        getMenuParentClass(label: string): string;
        onMenuClick(currentNumber: number, url: string, label: string, menuId: string): void;
    }

    export class controllerMenu {

        constructor(
            public $scope: IMenuScope,
            public $timeout: ng.ITimeoutService,
            private Plato: Services.INpDialogMaker,
            private NavigationService: Services.NavigationService
        )
        {
            var self = this;
            $scope.getSelectedClass = (current: number): string => {
                return self.getSelectedClass(current);
            }
            $scope.getMenuParentClass = (label: string): string => {
                return self.getMenuParentClass(label);
            }
            $scope.onMenuClick = (currentNumber: number, url: string, label: string, menuId: string): void => {
                self.onMenuClick(currentNumber, url, label, menuId);
            }

            // We want to resize the menu and main area after the element has been displayed or not
            $scope.$watch(() => { return angular.element('.resizable1').is(':visible') },
                (newValue: boolean, oldValue: boolean) => {
                    if (newValue !== oldValue) {
                        Utils.UpdateMainWindowWidthAndMainButtonPlacement($scope);
                    }
                });

        }
        public getSelectedClass(current: number): string {
            var self = this;
            if (current === self.$scope.modelNavigation.current)
                return 'selected';
            else
                return '';
        }
        public getMenuParentClass(label: string): string {
            return this.$scope.modelNavigation.btnState[label] ? "open NpMenuButton" : "NpMenuButton";
        }

        public onMenuClick(currentNumber: number, url: string, label: string, menuId: string): void {
            var self = this;
            var doActualNavigation = (pageScopeYouAreLeavingFrom?: NpTypes.IApplicationScope) => {
                self.$scope.modelNavigation.current = currentNumber;
                self.$scope.modelNavigation.steps.splice(1);
                self.$scope.modelNavigation.steps.push(new NpTypes.BreadcrumbStep(url, menuId, label, null));

                self.NavigationService.go(url, menuId);
            }

            var doNavigation: boolean = !self.$scope.modelNavigation.steps.some(step => step.menuId === menuId);

            if (doNavigation) {
                if (self.$scope.modelNavigation.onNavigation === undefined)
                    doActualNavigation();
                else
                    self.$scope.modelNavigation.onNavigation(doActualNavigation);
            }
        }
    }
}
