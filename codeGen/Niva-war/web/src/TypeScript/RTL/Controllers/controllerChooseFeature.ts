/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />

module Controllers {

    export class FeatureRow {
        constructor(
            public area:string,
            public areaValue:number,
            public origIdx:number)
        {}
    }

    export class ChooseFeatureModel {
        rows: Array<FeatureRow> = [];
        dataSetIdx: number = -1;
    }

    export interface IChooseFeatureScope extends NpTypes.IApplicationScope {
        model: ChooseFeatureModel;
        nevermind: ()=>void;
        preview: (idx:number)=>void;
        select: (idx:number)=>void;
    }

    export class controllerChooseFeature {

        constructor(
            public $scope: IChooseFeatureScope,
            public Plato: Services.INpDialogMaker,
            $timeout:ng.ITimeoutService)
        {
            var dialogOptions: NpTypes.DialogOptions =
                <NpTypes.DialogOptions>$scope.globals.findAndRemoveDialogOptionByClassName("ChooseFeatureDialog");
            var npMapThis:NpGeo.NpMap = (<any>dialogOptions).npMapThis;
            var featuresRemaining:any = (<any>dialogOptions).featuresRemaining;

            var self = this;
            $scope.model = new ChooseFeatureModel();
            featuresRemaining.forEach((x, origIdx) => {
                //console.log("x", x);
                var area;
                var geometry = x.a.getGeometry();
                if (geometry instanceof ol.geom.Polygon) {
                    var poly: ol.geom.Polygon = <ol.geom.Polygon>geometry;
                    area = npMapThis.formatArea(poly, true).replace(/ /g, "&nbsp;");
                    var label = "Digitized Feature";//if null. means unmanaged layer. the temp digitised geometry should be the one
                    if (!isVoid(x.b)) label = x.b.npName;
                    area += "&nbsp;(" + label + ")";
                    //area += "&nbsp;(" + npMapThis.getLayerLabelById(x.a.get('layerId')) + ")";
                    //if (!isVoid(x.a.get('layerId'))) {
                    //    area += "&nbsp;(" + npMapThis.getLayerLabelById(x.a.get('layerId')) + ")";
                    //}
                    $scope.model.rows.push(new FeatureRow(area, area, origIdx));
                } else if (geometry instanceof ol.geom.MultiPolygon) {
                    var poly: ol.geom.MultiPolygon = <ol.geom.MultiPolygon>geometry;
                    area = npMapThis.formatArea(poly, true).replace(/ /g, "&nbsp;");
                    var label = "Digitized Feature";//if null. means unmanaged layer. the temp digitised geometry should be the one
                    if (!isVoid(x.b)) label = x.b.npName;
                    area += "&nbsp;(" + label + ")";
                    $scope.model.rows.push(new FeatureRow(area, area, origIdx));

                } else if (geometry instanceof ol.geom.LineString) {
                    var line: ol.geom.LineString = <ol.geom.LineString>geometry;
                    area = npMapThis.formatLength(line).replace(/ /g, "&nbsp;");
                    var label = "Digitized Feature";//if null. means unmanaged layer. the temp digitised geometry should be the one
                    if (!isVoid(x.b)) label = x.b.npName;
                    area += "&nbsp;(" + label + ")";
                    $scope.model.rows.push(new FeatureRow(area, area, origIdx));

                } else if (geometry instanceof ol.geom.Point) {
                    var poly: ol.geom.Point = <ol.geom.Point>geometry;
                    area = 0 + 'm<sup>2</sup>'; //there is no fucking area when points,lines
                    var label = "Digitized Feature";//if null. means unmanaged layer. the temp digitised geometry should be the one
                    if (!isVoid(x.b)) label = x.b.npName;
                    area += "&nbsp;(" + label + ")";
                    //area += "&nbsp;(" + npMapThis.getLayerLabelById(x.a.get('layerId')) + ")";
                    //if (!isVoid(x.a.get('layerId'))) {
                    //    area += "&nbsp;(" + npMapThis.getLayerLabelById(x.a.get('layerId')) + ")";
                    //}
                    $scope.model.rows.push(new FeatureRow(area, area, origIdx));
                }
            });

            $scope.model.rows.sort((a:FeatureRow, b:FeatureRow) => {
                return b.areaValue - a.areaValue;
            });

            $scope.nevermind = function() {
                dialogOptions.jquiDialog.dialog("close");
            }

            function commonWork(f: ol.Collection<ol.Feature>, idx: number) {
                var g = featuresRemaining[idx].a.getGeometry();
                var newFeature = new ol.Feature();
                newFeature.setGeometryName("");
                newFeature.setGeometry(g);
                f.clear();
                //console.log("newFeature", newFeature);
                f.push(newFeature);
            }

            $scope.preview = function(idx:number) {
                npMapThis.featureOverlayCrop.getSource().clear();
                commonWork(npMapThis.featureOverlayCrop.getSource().getFeaturesCollection(), idx);
                $scope.model.dataSetIdx = idx;
            };

            $scope.select = function() {
                npMapThis.featureOverlayCrop.getSource().clear();
                npMapThis.selectInteraction.getFeatures().clear();
                commonWork(npMapThis.selectInteraction.getFeatures(), $scope.model.dataSetIdx);
                npMapThis.clearInteractions();
                npMapThis.addSelectInteraction();
                var selectedFeatures = npMapThis.selectInteraction.getFeatures();
                var feature = featuresRemaining[$scope.model.dataSetIdx].a;
                var layer = featuresRemaining[$scope.model.dataSetIdx].b;
                selectedFeatures.push(feature);

                if (!isVoid(layer) && !isVoid((<any>layer).getSqlVectorLayer)) {
                    (<any>layer).getSqlVectorLayer().onSelectFeature(feature, layer);
                } else {
                    var html = npMapThis.getFeatureLabel(feature, false);
                    if (!isVoid(html)) {
                        messageBox(
                            $scope,
                            Plato,
                            "Info",
                            html,
                            IconKind.INFO,
                            [
                                new Tuple2("OK", () => {})
                            ], 0, 0, 'auto');
                    }
                }
                dialogOptions.jquiDialog.dialog("close");
            };
        }
    }
}
