/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../DefinitelyTyped/jqueryui/jqueryui.d.ts" />
/// <reference path="../npTypes.ts" />
var Controllers;
(function (Controllers) {
    var controllerWait = (function () {
        function controllerWait($scope) {
            this.$scope = $scope;
            var dialogOptions = $scope.globals.findAndRemoveDialogOptionByClassName("plzWaitDialog");
            //$scope.globals = globals;
            // This doesn't work deterministically - so we use
            // a $('.plzWaitDialog').remove() in the main periodic 
            // $digest loop (inside main.ts)
            //var timerId = window.setInterval(
            //    () => {
            //        if ($scope.globals.nInFlightRequests === 0) {
            //            window.clearInterval(timerId);
            //            dialogOptions.closeDialog();
            //        }
            //    },
            //    1000);
        }
        return controllerWait;
    })();
    Controllers.controllerWait = controllerWait;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerWait.js.map