/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />

module Controllers {

    export class Lvl2GeometryRow {
        constructor(
            public area:string,
            public areaValue: number,
            public label: string,
            public valid: string,
            public intGeoms: ol.geom.Geometry,
            public gtype: string,  
            public origIdx: number)
        {}
    }

    export class ChooseLvl2GeomModel {
        rows: Array<Lvl2GeometryRow> = [];
        dataSetIdx: number = -1;
    }

    export interface IChooseLvl2GeomScope extends NpTypes.IApplicationScope {
        model: ChooseLvl2GeomModel;
        nevermind: () => void;
        init: () => void;
        preview: (idx: number) => void;
        previewInt: (idx: number) => void;
        select: (idx:number)=>void;
    }
    
    export class controllerChooseLevel2Geom {
        constructor(
            public $scope: IChooseLvl2GeomScope, 
            $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker
            
            
        )
        {
            var dialogOptions: NpTypes.DialogOptions =
                <NpTypes.DialogOptions>$scope.globals.findAndRemoveDialogOptionByClassName("ChooseLevel2GeomDialog");
            var npMapThis:NpGeo.NpMap = (<any>dialogOptions).npMapThis;
            var polygonsRemaining: any = (<any>dialogOptions).polygonsRemaining;

            var labelForGeoms: any = [];
            var intGeoms: any = [];
            var gtype: any = [];
            var valid: any = [];

            var imgPathDown: string = "/" + $scope.globals.appName + "/img/arrow_show.png";
            var imgPathUp: string = "/" + $scope.globals.appName + "/img/arrow_hide.png";

            var self = this;
            $scope.model = new ChooseLvl2GeomModel();
            polygonsRemaining.forEach((x, origIdx) => {
                labelForGeoms.push(x.entity.data['shortdesc']);
                intGeoms.push(x.entity.data['intgeom']);
                gtype.push(x.entity.data['gtype']);
                valid.push(x.entity.data['valid']);
                var coordinateSystem = fallback(npMapThis.coordinateSystem, 'EPSG:2100');
                var transGeom = x.getGeometry().clone().transform(
                    ol.proj.get('EPSG:3857'),
                    ol.proj.get(coordinateSystem)
                );
                var area = transGeom.getArea();
                var output;
                var textColor = 'black'
                //if invalid or not polygon , mark the text as red
                if (valid[origIdx] !== 'TRUE' || gtype[origIdx] !== 2003) {
                    textColor = 'red';
                }
                
                if (area > 10000) {
                    output = (Math.round(area / 1000000 * 100) / 100) + '&nbsp;' + 'km<sup>2</sup> (' + x.entity.data['shortdesc']+' )'; //+
                        //'<img align="right" width= "16" height= "16" src= "' + imgPathDown + '"  id= "btninfodiv_' + origIdx + '" >' +
                        //'<p style="display: none;color:' + textColor+'" id="infodiv_' + origIdx + '">' +
                        //' (<b>ilot_no:</b>' + labelForGeoms[origIdx] + ',<br/><b>valid:</b>' + valid[origIdx] +
                        //',<b>cover_id:</b>' + coverIds[origIdx] + ',<br/><b>gtype:</b>' + gtype[origIdx] + ')' +
                        //'</p>';
                } else {
                    output = (Math.round(area * 100) / 100) + '&nbsp;' + 'm<sup>2</sup> (' + x.entity.data['shortdesc'] + ' )'; //+
                        //'<img align="right" width= "16" height= "16" src= "' + imgPathDown + '"  id= "btninfodiv_' + origIdx + '" >' +
                        //'<p style="display: none;color:' + textColor + '" id="infodiv_' + origIdx + '">' +
                        //' (<b>ilot_no:</b>' + labelForGeoms[origIdx] + ',<br/><b>valid:</b>' + valid[origIdx] +
                        //',<b>cover_id:</b>' + coverIds[origIdx] + ',<br/><b>gtype:</b>' + gtype[origIdx] + ')' +
                        //'</p>';
                }
                $scope.model.rows.push(new Lvl2GeometryRow(output, area, labelForGeoms, intGeoms, valid, gtype, origIdx));
            });

            $scope.model.rows.sort((a: Lvl2GeometryRow, b: Lvl2GeometryRow) => {
                return b.areaValue - a.areaValue;
            });

    

            //$timeout(function () {
            //    polygonsRemaining.forEach((x, origIdx) => {
            //        //toggle the info paragraph
            //        $('#btninfodiv_' + origIdx).click(function () {
            //            $('#infodiv_' + origIdx).toggle("slow", function () {
            //                var divId = "infodiv_" + origIdx;
            //                var btnDivId = "btninfodiv_" + origIdx;
            //               //toggle the img button
            //                if ($('#' + divId).is(":hidden")) {
            //                    $('#' + btnDivId).attr("src", imgPathDown);
            //                }
            //                if ($('#' + divId).is(":visible")) {
            //                    $('#' + btnDivId).attr("src", imgPathUp);
            //                }
            //            });
            //        });
            //    })

           // });

            $scope.nevermind = function() {
                dialogOptions.jquiDialog.dialog("close");
            }

            function commonWork(f: ol.Collection<ol.Feature>, idx:number) {
                //var croppedFeature = NpGeoGlobals.coordinates_to_OL3feature(polygonsRemaining[idx]);
                var croppedFeature = polygonsRemaining[idx];
                var g = croppedFeature.getGeometry();
                var newFeature = new ol.Feature();
                newFeature.setGeometryName("");
                newFeature.setGeometry(g);
                f.clear();
                f.push(newFeature);
                npMapThis.enterDrawMode();
            }



            $scope.previewInt = function (idx: number) {
             npMapThis.featureOverlayCrop.getSource().clear();
            //need to loop through the checkboxes and find those checked
                polygonsRemaining.forEach((x, origIdx) => {
                    var isChecked = $('#lvl2CheckGeom_' + origIdx).is(':checked');
                    if (isChecked === true) {
                        var geom = new ol.geom[intGeoms[origIdx].type](intGeoms[origIdx].coordinates);
                        var coordinateSystem = fallback(npMapThis.coordinateSystem, 'EPSG:3857')
                        geom.transform(
                            ol.proj.get(coordinateSystem),
                            ol.proj.get('EPSG:3857')
                        );
                        var newFeature = new ol.Feature();
                        newFeature.setGeometryName("");
                        newFeature.setGeometry(geom);
                        npMapThis.featureOverlayCrop.getSource().addFeature(newFeature);
                        var isValid = isGeoJstsValid(polygonsRemaining[origIdx].getGeometry());
                        var geomType = polygonsRemaining[origIdx].getGeometry().getType();
                        if (isValid !== true || geomType !== 'Polygon') {
                            messageBox(
                                $scope,
                                Plato,
                                "Μη επιτρεπτή γεωμετρία",
                                "Προκύπτει μη αποδεκτή γεωμετρία. (τύπος:" + geomType + ", εγκυρότητα:" + isValid + ")",
                                IconKind.ERROR,
                                [
                                    new Tuple2("OK", () => { })
                                ], 0, 0, '19em');
                            $('#lvl2Geom_' + origIdx).prop('disabled', true);
                        } else
                            $('#lvl2Geom_' + origIdx).prop('disabled', false);
                    } else {
                        $('#lvl2Geom_' + origIdx).prop('disabled', true);
                    }
                });

                
            }
     
            $scope.select = function(id) {
                npMapThis.featureOverlayCrop.getSource().clear();
                commonWork(npMapThis.featureOverlayDraw.getSource().getFeaturesCollection(), id);
                dialogOptions.jquiDialog.dialog("close");
                npMapThis.intersectOverlapsCustom();
            };
        }
    }
}
