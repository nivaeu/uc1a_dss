/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />

module Controllers {

    export class ExcGeometryRow {
        constructor(
            public area:string,
            public areaValue:number,
            public origIdx: number,
            public layerLabel: string)
        {}
    }

    export class ExcChooseGeomModel {
        rows: Array<ExcGeometryRow> = [];
        dataSetIdx: number = -1;
    }

    export interface IChooseExcGeomScope extends NpTypes.IApplicationScope {
        model: ExcChooseGeomModel;
        nevermind: ()=>void;
        preview: (idx:number)=>void;
        select: (idx:number)=>void;
    }
    
    export class controllerChooseExclusiveGeom {

        constructor(
            public $scope: IChooseExcGeomScope,
            public Plato: Services.INpDialogMaker,
            $timeout:ng.ITimeoutService)
        {
            var dialogOptions: NpTypes.DialogOptions =
                <NpTypes.DialogOptions>$scope.globals.findAndRemoveDialogOptionByClassName("ChooseExcGeomDialog");
            var npMapThis:NpGeo.NpMap = (<any>dialogOptions).npMapThis;
            var polygonsRemaining: any = (<any>dialogOptions).polygonsRemaining;
            var displayLayer: any = (<any>dialogOptions).displayLayer;
            var startUpGeom: any = (<any>dialogOptions).startUpGeom;

            var self = this;
            $scope.model = new ExcChooseGeomModel();
            polygonsRemaining.forEach((x, origIdx) => {
                var coordinateSystem = fallback(npMapThis.coordinateSystem, 'EPSG:2100');
                var transGeom = x.getGeometry().clone().transform(
                    ol.proj.get('EPSG:3857'),
                    ol.proj.get(coordinateSystem)
                );
                var area = transGeom.getArea();
                //var area = x.getArea();
                var output;
                //output = (Math.round(area / 1000000 * 100) / 100) + '&nbsp;' + 'km<sup>2</sup>';
                //@p.tsagkis venizelou & nantin asked to get all areas as HA and not m2. 
                // Stupid???? Maybe...... it is not my desicion anyway.
                if (area > 10000) {
                    output = (Math.round(area / 1000000 * 100) / 100) + '&nbsp;' + 'km<sup>2</sup>. (' + displayLayer[origIdx]+' )';
                } else {
                    output = (Math.round(area * 100) / 100) + '&nbsp;' + 'm<sup>2</sup>. (' + displayLayer[origIdx] + ' )';
                }
                $scope.model.rows.push(new ExcGeometryRow(output, area, origIdx, displayLayer[origIdx]));
            });
            $scope.model.rows.sort((a: ExcGeometryRow, b: ExcGeometryRow) => {
                return b.areaValue - a.areaValue;
            });


            $scope.nevermind = function() {
                dialogOptions.jquiDialog.dialog("close");
            }

            function commonWork(f: ol.Collection<ol.Feature>, idx: number, isChecked: boolean) {
                var geomCoordsA = NpGeoGlobals.OL3feature_to_coordinates(new ol.Feature(startUpGeom)); 
                var geomCoordsB = polygonsRemaining[idx];
                //console.log("geomCoordsA", geomCoordsA)
                //console.log("geomCoordsB", geomCoordsB)
                //var cropped;
                if (isChecked === true) {//get the diff
                    startUpGeom = NpGeoGlobals.coordinates_to_OL3feature(subtractPolygonsFromPolygon(geomCoordsA, geomCoordsB)).getGeometry();
                } else if (isChecked === false) {//or else get the union
                    startUpGeom = NpGeoGlobals.coordinates_to_OL3feature(mergePolygons(geomCoordsA, geomCoordsB)).getGeometry();
                }

                //console.log("cropped", cropped); 
                //startUpGeom = NpGeoGlobals.coordinates_to_OL3feature(cropped).getGeometry(); 
                f.clear();
                f.push(new ol.Feature(startUpGeom));
                
                npMapThis.enterDrawMode();
            }

            $scope.preview = function (idx: number) {
                var isChecked = $('#exclLyr_' + idx).is(':checked');
                npMapThis.featureOverlayCrop.getSource().clear();
                var features = npMapThis.featureOverlayCrop.getSource().getFeaturesCollection();
                if (features === null) {
                    features = new ol.Collection<ol.Feature>();
                } 
                commonWork(features, idx, isChecked);
                //var checkCoords = polygonsRemaining[idx].getCoordinates();
               // var directionsClockwise = polygonsRemaining[idx].map(x => NpGeoGlobals.isClockWise(x)).filter(x => x === false);
                console.log("polygonsRemaining[idx]", polygonsRemaining[idx]);
                var currentFeats = npMapThis.featureOverlayCrop.getSource().getFeaturesCollection();
                console.log("currentFeats.item(0).getGeometry()", currentFeats.item(0).getGeometry());
                if (currentFeats.getLength() > 1 ||
                    typeof (currentFeats.item(0).getGeometry()) === 'undefined' ||
                    isGeoJstsValid(currentFeats.item(0).getGeometry()) === false ||
                    currentFeats.item(0).getGeometry().getType() !== 'Polygon'
                ) {
                    messageBox(
                        $scope,
                        Plato,
                        "Μη επιτρεπτή γεωμετρία",
                        "Η γεωμετρία που προκύπτει δεν ειναι αποδεκτή.",
                        IconKind.ERROR, 
                        [
                            new Tuple2("OK", () => {})
                        ], 0, 0, '19em');
                    $scope.model.dataSetIdx = -1;
                } else
                    $scope.model.dataSetIdx = idx;
            };

            $scope.select = function () {
                var features = npMapThis.featureOverlayCrop.getSource().getFeaturesCollection();
                console.log("features",features);
                var newFeature = new ol.Feature();
                newFeature.setGeometryName("");
                newFeature.setGeometry(features.item(0).getGeometry());
                //var newFeature = new ol.Feature();
                //croppedFeature = NpGeoGlobals.coordinates_to_OL3feature(cropped);
               // newFeature.setGeometryName("");
               // newFeature.setGeometry(croppedFeature.getGeometry());
               //// console.log("new feat geom type is======", newFeature.getGeometry().getType());
               // f.clear();
               // f.push(newFeature);
                //npMapThis.enterDrawMode(); 
                var f = npMapThis.featureOverlayDraw.getSource().getFeaturesCollection();
                f.clear();
                f.push(newFeature);
                npMapThis.featureOverlayCrop.getSource().clear();
                //commonWork(npMapThis.featureOverlayDraw.getSource().getFeaturesCollection(), $scope.model.dataSetIdx,true);
                dialogOptions.jquiDialog.dialog("close");
                npMapThis.enterDrawMode(); 
            };
        }
    }
}
