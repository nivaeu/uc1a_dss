/// <reference path="../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="npTypes.ts"/>
/// <reference path="services.ts" />
/// <reference path="Controllers/controllerInput.ts" />

class Tuple2<A, B> {
    constructor(public a:A, public b:B) {}
}

class Tuple3<A, B, C> {
    constructor(public a:A, public b:B, public c:C) {}
}

class SelectItem<T> {
    constructor(
        public value: T,
        public label: any) {}
}

class ExpiredArrayResource<T> {
    dictionary: { [key: string]: Tuple2<number, Array<SelectItem<T>>> } = {};
    public getArrayResource(
        key: string,
        closure: (arr: Array<SelectItem<T>>) => void,
        timeoutInMilliseconds: number = 1800000000): Array<SelectItem<T>>
    {
        var self = this;
        var ret = self.dictionary[key];
        var currentTimeInMs = (new Date).getTime();
        if ((ret !== undefined) && (currentTimeInMs<ret.a+timeoutInMilliseconds)) {
            return ret.b;
        }
        var newArray: Array<SelectItem<T>> = [];

        self.dictionary[key] = new Tuple2(currentTimeInMs, newArray);
        closure(newArray);
        return newArray;
    }
}


module Utils {

    export var timeoutForWaitDialogsAndInterclickDelaysInMilliseconds = 2000;

    export function getTimeInMS() { return (new Date).getTime() } 

    export function convertStringToDate(dateValue:string, format:string): Date {
        return (<any>Date).parseString(dateValue, format);

    }
    export function convertDateToString(dateValue: Date, dateFormat: string): string {
        return (<any>dateValue).format(dateFormat);

    }
    export function replaceHtmlChars(htmlString: string): string {
        if (isVoid(htmlString))
            return htmlString;
        return htmlString
            .replace(new RegExp("<br/>", 'g'), " ")
            .replace(new RegExp("&#160;", 'g'), " ")
            .replace(new RegExp("&quot;", 'g'), "\"")
            .replace(new RegExp("&amp;", 'g'), "&")
            .replace(new RegExp("&#39;", 'g'), "'")
            .replace(new RegExp("&lt;", 'g'), "<")
            .replace(new RegExp("&gt;", 'g'), ">");
    }
    export function AddToArrayIfNotExistsAndReturnIt<T extends NpTypes.IBaseEntity>(list: Array<T>, item: T): T {
        var cachedSE = _.find(list, function (iterSE:T) : boolean {
            return iterSE.getKey() === item.getKey();
        });
        if (cachedSE !== undefined)
            return cachedSE;
        list.push(item);

        return list.slice(-1)[0];
    }

    export function showWaitWindow(scope:NpTypes.IApplicationScope, Plato:Services.INpDialogMaker, $timeout, immediate:boolean=false)
    {
        var isFirstWaitDialog = scope.globals.nInFlightRequests === 0;
        $timeout(function() {
            if (isFirstWaitDialog) {
                var dialogOptions = new NpTypes.DialogOptions();
                dialogOptions.width = '20em';
                dialogOptions.autoOpen = true;
                dialogOptions.className = 'plzWaitDialog';
                dialogOptions.showCloseButton = false;

                $timeout(function() {
                    if (scope.globals.nInFlightRequests>=1) {
                        Plato.showDialog(
                            (<any>scope).$root,
                            'WaitDialog_Title',
                            "/"+scope.globals.appName+'/partials/wait.html?rev=' + scope.globals.version,
                            dialogOptions);
                    }
                }, 0);
            }
        }, immediate ? 0 : Utils.timeoutForWaitDialogsAndInterclickDelaysInMilliseconds);
        scope.globals.nInFlightRequests++;
    }

    export function showExcelFileImportResultsDlg($scope: NpTypes.IApplicationScope, Plato: Services.INpDialogMaker, excelFileId: string, excelFileEntityName: string, excelErrorEntityName: string): void {
        var dialogOptions = new NpTypes.DialogOptions();
        dialogOptions.title = "ImportExvel_ResultsDlg_Title";
        dialogOptions.width = "800px";
        dialogOptions.className = "PageController";
        dialogOptions.customParams = {
            'excelFileId': excelFileId,
            'ExcelFileEntityName': excelFileEntityName,
            'ExcelErrorEntityName': excelErrorEntityName
        };
        Plato.showDialog($scope,
            dialogOptions.title,
            "/" + $scope.globals.appName + '/partials/ExcelFileImportResultsDlg.html?rev=' + $scope.globals.version,
            dialogOptions);
    }


    export function UpdateMainWindowWidthAndMainButtonPlacement($scope:ng.IScope)
    {
        var element = $('.resizable1')[0];
        var navigationWidth:number = (<any>element).offsetWidth;
        var parentWidth:number = (<any>element).parentElement.offsetWidth;
        var remainingSpace:number = parentWidth - navigationWidth;
        var resizable2Elem = $('.resizable2')[0];
        //var main = $((<any>element).nextElementSibling);
        var main = resizable2Elem != undefined ? $(resizable2Elem) : $((<any>element).nextElementSibling);
        main.css("left", navigationWidth);
        main.css("width", remainingSpace);
        $('.TopFormArea').css('width', 'calc( 100% - ' + (navigationWidth+17) + 'px )'); // 17 for the horizontal sbar
        $('.fixedContainer').css('width', 
            (parseInt($('.TopFormArea').css('width')) - 52) + 'px'); // subtract 52px for scrollbar buttons
        // Trigger resizes of all grids
        (<any>$scope).modelNavigation.epochOfLastResize = Utils.getTimeInMS();
    }

    export function precise_round(num: number, decimals: number, defIfNan: number= 0) {
        if (num === undefined || num === null) {
            return defIfNan;
        }
        if (!isNaN(num)) {
            return Math.round(num * Math.pow(10, decimals)) / Math.pow(10, decimals);
        }
        return defIfNan;
    }
}

enum IconKind { INFO=0, WARNING=1, ERROR=2, QUESTION=3 }

class MsgBoxDialogOptions extends NpTypes.DialogOptions {
    message:string = "Default message";
    iconKind:IconKind = IconKind.INFO;
    buttons:Array<Tuple2<string, ()=>void>> = [];
    indexSelected:number;
    indexCancel:number;
}

function isVoid(x: any): boolean
{
    if (x === undefined || x === null)
        return true;
    return false;
}

function messageBox(
    $scope: NpTypes.IApplicationScope,
    Plato:Services.INpDialogMaker,
    titleMessage:string,
    message:string,
    iconKind:IconKind,
    buttons:Array<Tuple2<string, ()=>void>>,
    indexSelected:number = undefined,
    indexCancel:number = undefined,
    width:string = '30em')
{
    var dialogOptions:MsgBoxDialogOptions = new MsgBoxDialogOptions();
    dialogOptions.width = width;
    dialogOptions.autoOpen = true;
    dialogOptions.message = message;
    dialogOptions.iconKind = iconKind;
    dialogOptions.buttons = buttons;
    dialogOptions.indexSelected = indexSelected;
    dialogOptions.indexCancel = indexCancel;
    dialogOptions.showCloseButton = (indexCancel !== undefined);
    dialogOptions.width = width;
    dialogOptions.className = "MsgBoxClass";
    Plato.showDialog(
        $scope,
        titleMessage,
        "/"+$scope.globals.appName+'/partials/messageBox.html?rev=' + $scope.globals.version,
        dialogOptions);
    return dialogOptions;
}

function supports_html5_storage() {
    try {
        return 'sessionStorage' in window && window['sessionStorage'] !== null;
    } catch (e) {
        return false;
    }
}

function getNumberOfWatchers():number {
    var root = $(document.getElementsByTagName('body'));
    var watchers = [];

    var f = function (element) {
        if (element.data().hasOwnProperty('$scope')) {
            angular.forEach(element.data().$scope.$$watchers, function (watcher) {
                watchers.push(watcher);
            });
        }

        angular.forEach(element.children(), function (childElement) {
            f($(childElement));
        });
    };

    f(root);

    return watchers.length;
};

// Dynamically generated input dialogs

class InputDialogVariable {
    varName:string;
    label:string;
    required:boolean;
    validateFunc:(inputData:any, dummy:any)=>NpTypes.ValidationResult;
    constructor(
        config:{
            varName:string;
            label:string;
            initial?:any;
            required?:boolean;
            validateFunc?:(inputData:any, dummy:any)=>NpTypes.ValidationResult;
        })
    {
        ['varName', 'label', 'initial', 'required', 'validateFunc'].forEach((name:string) => {
            this[name] = config[name];
        });
    }
}

class InputDialogText extends InputDialogVariable {
    initial:string;
    constructor(
        config: {
            varName:string;
            label:string;
            initial?:string;
            required?:boolean;
            validateFunc?:(text:string, dummy:any)=>NpTypes.ValidationResult;
        })
    {
        super(config);
    }
}

class InputDialogNumber extends InputDialogVariable {
    initial:number;
    min:number;
    max:number;
    decimals:number;
    decSep:string;
    thSep:string;
    constructor(
        config: {
            varName:string;
            label:string;
            initial?:number;
            required?:boolean;
            validateFunc?:(data:number, dummy:any)=>NpTypes.ValidationResult;
            min?:number;
            max?:number;
            decimals?:number;
            decSep?:string;
            thSep?:string;
        })
    {
        super(config);
        ['min', 'max', 'decimals', 'decSep', 'thSep'].forEach((name:string) => {
            this[name] = config[name];
        });
    }
}

class InputDialogDate extends InputDialogVariable {
    initial:Date;
    format:string;
    mindate:string;
    maxdate:string;
    constructor(
        config: {
            varName:string;
            label:string;
            initial?:Date;
            required?:boolean;
            validateFunc?:(text:Date, dummy:any)=>NpTypes.ValidationResult;
            format:string;
            min_date?:string;
            max_date?:string;
        })
    {
        super(config);
        ['required', 'validateFunc', 'format', 'mindate', 'maxdate'].forEach((name:string) => {
            this[name] = config[name];
        });
    }
}

class InputDialogOptions extends NpTypes.DialogOptions {
    variables:InputDialogVariable[];
    buttons:Tuple2<string, (model:any)=>void>[];
    indexSelected:number;
    indexCancel:number;
    countDialogs:number;
}

var g_countDialogs = 0;

function inputDialog(
    $scope: NpTypes.IApplicationScope,
    Plato:Services.INpDialogMaker,
    strTitle:string,
    htmlContentIntro:string,
    variables:InputDialogVariable[],
    buttons:Tuple2<string, (model:any)=>void>[],
    indexSelected:number = undefined,
    indexCancel:number = undefined,
    width:string = '30em')
{
    g_countDialogs += 1;

    var dialogOptions = new InputDialogOptions();
    dialogOptions.width = width;
    dialogOptions.autoOpen = true;
    dialogOptions.className = "GenericInputDialog";
    dialogOptions.variables = variables;
    dialogOptions.buttons = buttons;
    dialogOptions.indexSelected = indexSelected;
    dialogOptions.indexCancel = indexCancel;
    dialogOptions.showCloseButton = false;
    dialogOptions.width = width;
    dialogOptions.countDialogs = g_countDialogs;
    var html =
        "<div data-ng-controller='controllerInput' style='margin: 0.5em 1em;'>"
        +"<div data-on-key-down='triggerCancel()' data-keys='[27]'>"
        +htmlContentIntro;
    var error;
    var validators = [];
    variables.forEach((v:InputDialogVariable) => {
        if (!isVoid(error))
            return;
        if (isVoid(v.varName)) {
            error = "Wrong usage of inputDialog: variable name is undefined...";
            return;
        }
        var modelVarname = 'model.' + v.varName;
        var modelUIVarname = 'model._' + v.varName;
        var targetDirective;
        var extras = Sprintf.sprintf(" data-np-required='%s' ", v.required?"true":"false");
        if (!isVoid(v.validateFunc)) {
            validators.push('validate_' + v.varName);
            extras += Sprintf.sprintf(" data-np-validate='validate_%s' ", v.varName);
        }
        if (v instanceof InputDialogText) {
            targetDirective = 'text';
        } else if (v instanceof InputDialogNumber) {
            targetDirective = 'number';
            ['min', 'max', 'decimals', 'decSep', 'thSep'].forEach((option:string) => {
                if (!isVoid(v[option]))
                    extras += Sprintf.sprintf(" data-np-%s='%s' ", option, v[option]);
            });
        } else if (v instanceof InputDialogDate) {
            targetDirective = 'date';
            ['format', 'min-date', 'max-date'].forEach((option:string) => {
                var accessor = option.replace('-', '_');
                if (!isVoid(v[accessor]))
                    extras += Sprintf.sprintf(" data-np-%s='%s' ", option, v[accessor]);
            });
        } else {
            error = "Only a descendant class of InputDialogType can be passed to inputDialog";
            return;
        }
        html += Sprintf.sprintf(
            "\n<br><span><b>%s</b></span>", v.label);
        html += Sprintf.sprintf(
            "\n<input data-np-%s='dummy' data-ng-model='%s' data-np-ui-model='%s' ",
            targetDirective, modelVarname, modelUIVarname);
        html += extras + ">";
        html += "\n<br>";
    });
    if (!isVoid(error)) {
        alert(error);
        return;
    }
    html += "\n&nbsp;<br>";
    var count=0;
    buttons.forEach((v:Tuple2<string, (model:any)=>void>) => {
        var suffix = Sprintf.sprintf("_%d_%d", dialogOptions.countDialogs, count);
        html += Sprintf.sprintf(
            "\n<button id='autoBtn_%s' %s data-ng-click='click_%s()'>%s</button>",
            suffix,
            (count === dialogOptions.indexSelected)?" data-ng-disabled='!model.allOK' ":"",
            suffix,
            v.a);
        count += 1;
    });

    html += "\n</div>\n</div>";

    Plato.showDialogHTML(
        $scope,
        strTitle,
        html,
        'controllerInput',
        Controllers.controllerInput,
        dialogOptions);
    return dialogOptions;
}

function fallback(x, v) {
    return isVoid(x)?v:x;
}
