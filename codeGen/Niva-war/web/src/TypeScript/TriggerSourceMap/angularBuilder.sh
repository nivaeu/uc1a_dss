#!/bin/bash

RED='\e[0;31m'
GREEN='\e[0;32m'
CYAN='\e[0;36m'
NORMAL='\e[0m' 

usage() {
    EXE=$(basename $0)
    echo -e "Usage: $EXE <options>"
    echo -e "Where options are:"
    echo -e "\t${GREEN}-n${NORMAL}\tInvoke NeuroCode only"
    echo -e "\t${GREEN}-t${NORMAL}\tBuild TypeScript only (and rsync to JBOSS)"
    echo -e "\t${GREEN}-j${NORMAL}\tBuild Java and deploy .ear to JBoss"
    echo -e "\t${GREEN}-a${NORMAL}\tBuild all 3 steps above (shortcut for -ntj)"
    echo -e "\t${GREEN}-r${NORMAL}\tBuild TS in release mode (single output .js)"
    echo -e "\t${GREEN}-x${NORMAL}\tRun NeuroCode in experimental mode"
    echo -e "\t${GREEN}-h${NORMAL}\tThis help message."
    echo -e "\nFor example, ${CYAN}${EXE} -ntj${NORMAL} will invoke NeuroCode, build TypeScript"
    echo -e "(in debug mode) and then build Java. Another way is \"${CYAN}$EXE -a${NORMAL}\""
    echo -e "which is a shortcut that does the same. ${CYAN}$EXE -rntj${NORMAL} will also do"
    echo -e "the same, but will build TypeScript in release mode. The most useful option is"
    echo -e "${RED}$EXE -t${NORMAL} which will build only TypeScript (in debug mode)"
    echo -e "and will then rsync the result to JBOSS. To do that, it needs the env var"
    echo -e "JBOSS_HOME to point to the local JBOSS folder, where it will detect "
    echo -e "the last deployment folder of this application, and rsync the web/ folder's"
    echo -e "contents over it."
}

# Hudson Is King
if [ "$HOSTNAME" == "gaia-hudson.neuropublic.gr" -o "$HOSTNAME" == "avalon" ] ; then
    export PATH=$PATH:/home/ttsiod/bin:/home/ttsiod/bin.local
    export PATH=/opt/NeuroCode2/bin:$PATH
fi

TARGET=debug
RUN_NEUROCODE=0
RUN_TYPESCRIPT=0
RUN_JAVA=0
NEUROCODE_MODE=normal
while getopts "ntjrxah" o ; do
    case "${o}" in
        n)
            RUN_NEUROCODE=1
            ;;
        t)
            RUN_TYPESCRIPT=1
            ;;
        j)
            RUN_JAVA=1
            ;;
        r)
            TARGET=release
            RUN_TYPESCRIPT=1
            ;;
        x)
            NEUROCODE_MODE=experimental
            RUN_NEUROCODE=1
            ;;
        a)
            RUN_NEUROCODE=1
            RUN_TYPESCRIPT=1
            RUN_JAVA=1
            ;;
        h)
            usage
            ;;
        *)
            usage
            ;;
    esac
done

if [ "$HOSTNAME" == "gaia-hudson.neuropublic.gr" ] ; then
    TARGET=release
fi

if [ $RUN_NEUROCODE -eq 0 -a $RUN_TYPESCRIPT -eq 0 -a $RUN_JAVA -eq 0 ] ; then
    echo -e "\nNo build options specified... Aborting."
    echo -e "(Use -h to see the available options)"
    exit 1
fi

# Allow overriding NeuroCode path with env var
[ -z "$NEUROCODE" ] && NEUROCODE=NeuroCode.exe

if "$NEUROCODE" -v | grep State >/dev/null ; then
    MODE=$("$NEUROCODE" -v)
else
    #if [ -f /opt/NeuroCode/bin/RTL/cashflow-war/src/java/gr.neuropublic/jsf/base/TableLazyDataModel.java ] ; then
    #    MODE=Stateless
    #else
    #    MODE=Stateful
    #fi
    MODE="unknown generator"
fi

VERIFY=project.xml
if [ ! -f $VERIFY ] ; then
    echo This folder is missing $VERIFY - aborting...
    exit 1
fi

APP=$(grep '<WebApplication' project.xml  | sed 's,^.*[\t ]Name\s*=\s*"\([^"]*\)".*,\1,')
CODEGEN=$(grep CodeGenerationFolder project.xml  | sed 's,[<>],@,g' | awk -F@ '{print $3}')
if [ ! -d $CODEGEN ] ; then
    echo No CodeGenerationFolder in project.xml ...
    exit 1
fi

echo 
echo Detected application \"$APP\" and performing $TARGET build...
echo 

svn status | grep '^M' && {
    echo
    echo NOTE: Compared to the repos, there is at least one modification...
    echo
    if [ "$HOSTNAME" == "gaia-hudson.neuropublic.gr" ] ; then
        echo Not a clean workdir, call Thanassis.
        exit 1
    fi
}

if [ "$HOSTNAME" == "gaia-hudson.neuropublic.gr" ] ; then
    if [ ! -f environment.xml ] ; then
        cat environment*sample* | sed 's,^.*PublishFolder.*$,<PublishFolder>/var/tmp</PublishFolder>,' > environment.xml
    fi
else
    if [ ! -f environment.xml ] ; then
        echo You must create environment.xml first.
        exit 1
    fi
fi

if [ $RUN_NEUROCODE -eq 1 ] ; then
    echo '#############################'
    echo '         NeuroCode'
    echo '#############################'
    if [ "$HOSTNAME" == "gaia-hudson.neuropublic.gr" ] ; then
        echo Hudson has already cleaned up. Running NeuroCode...
    else
        echo Running cleanNeuroCodeFiles.py ...
        [ ! -f $CODEGEN/Entities.bin ] && { ./cleanNeuroCodeFiles.py > clean.log ; }
    fi

    echo Running NeuroCode...

    if [ -f generateAndPatch.sh ] ; then
        # Use custom codegen process, if it exists
        if [ "$NEUROCODE_MODE" == "experimental" ] ; then
            echo You asked for an experimental build, but there is
            echo a local generateAndPatch.sh with a custom build process...
            echo
            echo Aborting...
            exit 1
        fi
        ./generateAndPatch.sh || exit 1
    else
        ${NEUROCODE} project.xml -js -${NEUROCODE_MODE} || exit 1
        if [ -f patch.sh ] ; then
            # Use custom patch process, if it exists
            ./patch.sh || exit 1
        fi
    fi
    [ -f logo.jpg ] && { cp logo.jpg */*-war/web/img/gaia-management.jpg || exit 1 ; }
fi

SVNVERSION=$(svnversion -c | cut -d ":" -f2)
if pwd | grep hudson >/dev/null ; then
    APPNAME=$(pwd | sed 's,/workspace$,,;s,^.*/,,')
else
    APPNAME=$(pwd | sed 's,^.*/,,')
fi

cd $CODEGEN || { echo Build failure, call Thanassis... ; exit 1 ; }

# Patch login.html with revision and timestamp

LOGINS=$(find . -name login.html.template | grep war/src | wc -l)
if [ $LOGINS -ne 1 ] ; then
    echo I need exactly one login.html.template ...
    find . -name login.html.template | grep war/src
    exit 1
fi
DATEPATCH=$(LC_ALL=C date | sed "s,^,<!-- @@Built src revision ${SVNVERSION}\, with $MODE on ,;s,$, -->,")
LOGINHTML=$(find . -name login.html.template | grep war/src)
grep -v @@Built $LOGINHTML | awk "{ if (NR==1) { print \"$DATEPATCH\\n\"; } ; print \$0; }" > /var/tmp/newlogin.html.$$ && mv  /var/tmp/newlogin.html.$$ $LOGINHTML

# Patch build.properties

if [ "$HOSTNAME" == "gaia-hudson.neuropublic.gr" ] ; then
    if [ -f build.properties ] ; then
        cat build.properties | sed 's,publish.dir=.*$,publish.dir=/var/tmp,' > a && mv a build.properties
    fi
fi

if [ $RUN_TYPESCRIPT -eq 1 ] ; then

    echo '#############################'
    echo '        TypeScript'
    echo '#############################'

    cd ${APP}-war/src/TypeScript/ ||  exit 1
    ./TriggerSourceMap/pythonMaker.py || exit 1

    if [ `uname` == "Darwin" ]; then
        [ -z "$PROCESSORS" ] && PROCESSORS=$(sysctl -n hw.ncpu)
    else 	
	    [ -z "$PROCESSORS" ] && PROCESSORS=$(cat /proc/cpuinfo | grep ^processor | wc -l)
    fi
    time make -j${PROCESSORS} ${TARGET} || exit 1

    cd - || exit 1

    if [ -f ../logoProvider.jpg ] ; then
        cp ../logoProvider.jpg ${APP}-war/web/img/gaia-logo.jpg || exit 1
    fi

    if [ -f ../logoProviderBig.png ] ; then
        cp ../logoProviderBig.png ${APP}-war/web/img/logoBig.png || exit 1
    fi

    if [ "$HOSTNAME" != "gaia-hudson.neuropublic.gr" ] && [ "$TARGET" != "release" ] ; then
        cd ${APP}-war/src/ ||  exit 1
        echo Creating list of all .ts files for Sublime Text project in...
        echo $(pwd)/root.ts
        find . -type f -iname \*.ts | while read ANS ; do echo "/// <reference path=\"$ANS\" />" ; done | grep -v root.ts > ./root.ts
        cd .. || exit 1
        cat > $APP.sublime-project <<OEF
{
	"folders":
	[
		{
			"follow_symlinks": true,
			"path": "src"
		}
	],
	"settings":
	{
		"typescript":
		[
			"src/root.ts"
		]
	}
}
OEF
        cd .. || exit 1
    fi

    if [ -f ../undoTsPatches.sh ] ; then
        ( cd .. ; ./undoTsPatches.sh )
    fi
fi

if [ $RUN_JAVA -eq 1 ] ; then

    echo
    echo '#############################'
    echo '            Java'
    echo '#############################'

    if [ "$HOSTNAME" == "gaia-hudson.neuropublic.gr" ] || [ "$TARGET" == "release" ] ; then
        echo Invoking ant targeting EAP
        ant -Dpersistence-xml-file-to-use=persistence_eap.xml
    else
        echo Invoking ant
        ant
    fi
    EXITCODE=$?

    # Undo changes in login.html and build.properties

    LOGIN=$(find . -name login.html)
    cp ${LOGIN} ${LOGIN}.patch
    svn revert ${LOGIN} 2>/dev/null
    svn revert build.properties 2>/dev/null

    if [ $EXITCODE -ne 0 ] ; then
        exit $EXITCODE
    fi

    if [ "$HOSTNAME" == "gaia-hudson.neuropublic.gr" ] ; then
        cp system.sql /var/tmp/system.sql.$APP
        cp queries_definition.sql /var/tmp/queries_definition.sql.$APP
        cp ../Properties/* /var/tmp/ 2>/dev/null
    fi

    echo Built "$DATEPATCH"
fi

if [ $RUN_JAVA -eq 1 ] ; then
    if [ "$HOSTNAME" == "gaia-hudson.neuropublic.gr" ] ; then
        echo Now deploy in EAP...
    else
        echo Now deploy in normal jboss...
    fi

    if [ -f ../undoJavaPatches.sh ] ; then
        ( cd .. ; ./undoJavaPatches.sh )
    fi
else
    if [ "$HOSTNAME" != "gaia-hudson.neuropublic.gr" ] ; then
        if [ $RUN_TYPESCRIPT -eq 1 ] ; then
            cd .. || exit 1
            ./angularRapidDeploy.py
        fi
    fi
fi
