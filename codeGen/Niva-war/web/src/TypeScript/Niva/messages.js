/// <reference path="../RTL/utils.ts" />
var Language;
(function (Language) {
    Language[Language["el"] = 0] = "el";
    Language[Language["en"] = 1] = "en";
})(Language || (Language = {}));
var Formatter = (function () {
    function Formatter() {
    }
    Formatter.dateToStrig = function (inDate, activeLang) {
        switch (activeLang) {
            case Language.el:
                return Utils.convertDateToString(inDate, "dd/MM/yyyy");
            case Language.en:
                return Utils.convertDateToString(inDate, "MM/dd/yyyy");
            default:
                return Utils.convertDateToString(inDate, "dd/MM/yyyy");
        }
    };
    Formatter._d = function (date) {
        return Utils.convertStringToDate(date, "dd-MM-yyyy");
    };
    return Formatter;
})();
var Messages_el = (function () {
    function Messages_el() {
    }
    Messages_el.PersistenceErrorOccured = function () {
        return "Σφάλμα Καταχώρησης.";
    };
    Messages_el.Previous = function () {
        return "Προηγούμενο";
    };
    Messages_el.Next = function () {
        return "Επόμενο";
    };
    Messages_el.SaveButtonLabel = function () {
        return "Καταχώρηση";
    };
    Messages_el.ReturnButtonLabel = function () {
        return "Επιστροφή";
    };
    Messages_el.CancelButtonLabel = function () {
        return "Ακύρωση";
    };
    Messages_el.DeleteButtonLabel = function () {
        return "Διαγραφή";
    };
    Messages_el.NewButtonLabel = function () {
        return "Νέo";
    };
    Messages_el.MenuApplicationsTitle = function () {
        return "Εφαρμογές";
    };
    Messages_el.MenuSettingsTitle = function () {
        return "Ρυθμίσεις";
    };
    Messages_el.MenuAppSettingsTitle = function () {
        return "Ρυθμίσεις Εφαρμογής";
    };
    Messages_el.AppSettingsTitle = function () {
        return "Ρυθμίσεις Εφαρμογής";
    };
    Messages_el.ThemeChoiceTabTitle = function () {
        return "Επιλογή Theme";
    };
    Messages_el.MenuCustomerTitle = function () {
        return "Πελάτες";
    };
    Messages_el.CEVCustomerTitle = function () {
        return "Πελάτης";
    };
    Messages_el.LoggedInUserLabel = function () {
        return "Σύνδεση ως";
    };
    Messages_el.LogoutLinkTitle = function () {
        return "Αποσύνδεση";
    };
    Messages_el.DatatableEmptyMessage = function () {
        return "Δεν υπάρχουν εγγραφές με τα συγκεκριμένα κριτήρια";
    };
    Messages_el.DatatableEntries = function () {
        return "Εγγραφές";
    };
    Messages_el.ViewExpirationDialogBody = function () {
        return "Λόγω αδράνειας στη σελίδα η συναλλαγή του χρήστη έχει λήξει. Πληκτρολογήστε ΟΚ για να πλοηγηθείτε στην αρχική σελίδα.";
    };
    Messages_el.ViewExpirationDialogTitle = function () {
        return "Η σελίδα έχει λήξει ...";
    };
    Messages_el.DeleteDialogMsg = function () {
        return "Η εγγραφή που επιλέξατε θα διαγραφεί οριστικά! Θέλετε να συνεχίσετε;";
    };
    Messages_el.WarningMsgHeader = function () {
        return "Προειδοποιητικό Μήνυμα!";
    };
    Messages_el.UncommittedChangesMsg = function () {
        return "Δεν υπάρχουν αλλαγές προς αποθήκευση";
    };
    Messages_el.EntityCreationMsg = function (p0) {
        return "Εισαγωγή Εγγραφής: " + p0 + "";
    };
    Messages_el.EntityRemovalMsg = function (p0) {
        return "Διαγραφή Εγγραφής: " + p0 + "";
    };
    Messages_el.EntityUpdateMsg = function (p0) {
        return "Ενημέρωση Εγγραφής: " + p0 + "";
    };
    Messages_el.EntityCreationError = function (p0) {
        return "Σφάλμα Εισαγωγής Εγγραφής: " + p0 + "";
    };
    Messages_el.EntityRemovalError = function (p0) {
        return "Σφάλμα Διαγραφής Εγγραφής: " + p0 + "";
    };
    Messages_el.EntityUpdateError = function (p0) {
        return "Σφάλμα Ενημέρωσης Εγγραφής: " + p0 + "";
    };
    Messages_el.EntitySuccessSave = function () {
        return "Η αποθήκευση ολοκληρώθηκε με επιτυχία.";
    };
    Messages_el.EntitySuccessDeletion = function () {
        return "Η διαγραφή ολοκληρώθηκε με επιτυχία.";
    };
    Messages_el.EntitySuccessDeletion2 = function () {
        return "Η εγγραφή διαγράφτηκε με επιτυχία.";
    };
    Messages_el.EntityDeletionWarningMsg = function () {
        return "Οριστική Διαγραφή Εγγραφής. Θέλετε να συνεχίσετε;";
    };
    Messages_el.ExceptionInsideValidationErrorMessage = function (p0, p1) {
        return "Σημειώθηκε σφάλμα κατά την εκτέλεση της μεθόδου επικύρωσης (" + p0 + "), μήνυμα σφάλματος: " + p1 + ".";
    };
    Messages_el.NoPendingChanges = function () {
        return "Δεν υπάρχουν αλλαγές προς αποθήκευση";
    };
    Messages_el.SearchNoResults = function () {
        return "Δεν βρέθηκαν εγγραφές για τα κριτήρια αναζήτησης που δώσατε.";
    };
    Messages_el.optimisticLockException = function () {
        return "Η εγγραφή έχει μεταβληθεί από άλλον χρήστη.";
    };
    Messages_el.insufficientPrivilegesException = function () {
        return "Ανεπαρκή Δικαιώματα.";
    };
    Messages_el.INVALID_VAT = function (sVat) {
        return "Το ΑΦΜ <b>" + sVat + "</b> δεν είναι έγκυρο.";
    };
    Messages_el.USER_NOT_REGISTERED = function (sVat) {
        return "Ο παραγωγός με το ΑΦΜ <b>" + sVat + "</b> δεν είναι μέλος του GAIA Επιχειρείν.";
    };
    Messages_el.USER_NOT_REGISTERED_IN_APP = function (sAppName, sVat) {
        return "Ο παραγωγός με το ΑΦΜ <b>" + sVat + "</b> δεν έχει πρόσβαση στην υπηρεσία " + sAppName + " του GAIA Επιχειρείν.";
    };
    Messages_el.USER_NOT_REGISTERED_IN_APP_BY_SUBSCRIBER = function (sAppName, sVat) {
        return "Η εγγραφή στην υπηρεσία " + sAppName + " για τον παραγωγό με ΑΦΜ <b>" + sVat + "</b> έχει γίνει από άλλο φορέα.";
    };
    Messages_el.TOO_MANY_RESULT_ENTITIES_FOR_EXCEL = function (p0) {
        return "Οι εγγραφές ξεπερνούν το όριο των " + p0 + ". Χρησιμοποιήστε φίλτρο για να περιορίσετε το πλήθος εγγραφών.";
    };
    Messages_el.NEW_PASSWORD_SET = function () {
        return "Επιτυχής αλλαγή του κωδικού πρόσβασης.";
    };
    Messages_el.maxCheckedIdsToInsert_in_MultipleInsertBtn_exceeded = function (p0) {
        return "Ξεπεράστηκε το όριο (" + p0 + ") νέων εγγραφών για εισαγωγή.";
    };
    Messages_el.HttpRequestTimeOutMessage = function () {
        return "Ξεπεράστηκε το χρονικό όριο αναμονής απάντησης από τον διακομιστή. Τα δεδομένα της οθόνης δεν είναι ενημερωμένα.";
    };
    Messages_el.ServerCommunicationError = function () {
        return "Σφάλμα στην επικοινωνία με τον server.";
    };
    Messages_el.SessionExpired = function () {
        return "Η συνεδρία έχει λήξει.";
    };
    Messages_el.OnbeforeunloadMsg = function () {
        return "Αν συνεχίσετε, όλες οι αλλαγές που δεν έχετε σώσει θα χαθούν!\n\n";
    };
    Messages_el.EntityById_NotFoundMsg = function (pk) {
        return "Η εγγραφή με PK:" + pk + " δεν βρέθηκε στη βάση δεδομένων";
    };
    Messages_el.EntityByCode_NotFoundMsg = function () {
        return "Δεν βρέθηκε εγγραφή στη βάση δεδομένων";
    };
    Messages_el.EntityByCode_TooManyRowsForCodeMsg = function (codeValue) {
        return "Βρέθηκαν περισσότερες από μία εγγραφές για το '" + codeValue + "'";
    };
    Messages_el.EntityByCode_NoRecordForCodeMsg = function (codeValue) {
        return "Δεν βρέθηκε εγγραφή για το '" + codeValue + "'";
    };
    Messages_el.SaveChangesWarningMsg = function () {
        return "Θέλετε να σωθούν οι αλλαγές;";
    };
    Messages_el.NoChangesToSaveMsg = function () {
        return "Δεν υπάρχουν αλλαγές προς αποθήκευση";
    };
    Messages_el.OnFailuredSaveMsg = function () {
        return "Εντοπίστηκαν τα παρακάτω σφάλματα";
    };
    Messages_el.FieldValidationErrorMsg = function (fieldName) {
        return "Λάθος τιμή στο πεδίο <b>" + fieldName + "</b>";
    };
    Messages_el.FieldValidation_RequiredMsg1 = function (fieldName) {
        return "Το πεδίο <b>" + fieldName + "</b> είναι υποχρεωτικό";
    };
    Messages_el.FieldValidation_RequiredMsg2 = function () {
        return "Υποχρεωτικό πεδίο";
    };
    Messages_el.FieldValidation_MaxLengthMsg = function (fieldName, fieldValue, iMaxLength) {
        return "Το πεδίο <b>" + fieldName + "</b> (" + fieldValue + ") πρέπει να είναι μέχρι " + iMaxLength + " χαρακτήρες";
    };
    Messages_el.FieldValidation_MinMaxValueMsg = function (fieldName, fieldValue, maxVal, minVal) {
        return "Το πεδίο <b>" + fieldName + "</b> (" + fieldValue + ") πρέπει να είναι μεταξύ " + minVal + " και " + maxVal + "";
    };
    Messages_el.FieldValidation_MinValueMsg = function (fieldName, fieldValue, minVal) {
        return "Το πεδίο <b>" + fieldName + "</b> (" + fieldValue + ") πρέπει να είναι μεγαλύτερο από " + minVal + "";
    };
    Messages_el.FieldValidation_MaxValueMsg = function (fieldName, fieldValue, maxVal) {
        return "Το πεδίο <b>" + fieldName + "</b> (" + fieldValue + ") πρέπει να είναι μικρότερο από " + maxVal + "";
    };
    Messages_el.FieldValidation_MaxDecimalsMsg = function (fieldName, fieldValue, maxDecimals) {
        return "Το πεδίο <b>" + fieldName + "</b> (" + fieldValue + ") πρέπει να έχει μέχρι " + maxDecimals + " δεκαδικά ψηφία";
    };
    Messages_el.FieldValidation_NumbersOnlyMsg = function () {
        return "Μόνο αριθμοί";
    };
    Messages_el.FieldValidation_IntegersOnlyMsg = function () {
        return "Μόνο ακέραιοι αριθμοί";
    };
    Messages_el.FieldValidation_DecimalsMsg = function (iDecimals) {
        return "Μόνο αριθμοί, " + iDecimals + " δεκαδικά ψηφία και μία υποδιαστολή";
    };
    Messages_el.FieldValidation_Numbers_Allowed_MaxMsg = function (val) {
        return "Επιτρεπτές τιμές: Έως " + val + "";
    };
    Messages_el.FieldValidation_Numbers_Allowed_MinMsg = function (val) {
        return "Επιτρεπτές τιμές: Από " + val + "";
    };
    Messages_el.FieldValidation_Numbers_Allowed_MinMaxMsg = function (val1, val2) {
        return "Επιτρεπτές τιμές: Από " + val1 + " έως " + val2 + "";
    };
    Messages_el.FieldValidation_DateFormatMsg = function (validFormat) {
        return "Σε μορφή: " + validFormat + "";
    };
    Messages_el.FieldValidation_DateFromMsg = function (val) {
        return "Από: " + val + "";
    };
    Messages_el.FieldValidation_DateToMsg = function (val) {
        return "έως: " + val + "";
    };
    Messages_el.FieldValidation_DateFromToMsg = function (val1, val2) {
        return "Από: " + val1 + " έως: " + val2 + "";
    };
    Messages_el.FieldValidation_Blob_FileTypeMsg = function () {
        return "Επιτρέπονται μόνο αρχεία τύπου:";
    };
    Messages_el.FieldValidation_Blob_FileSizeMsg = function (maxSize) {
        return "Επιτρέπονται αρχεία έως: " + maxSize + " KB";
    };
    Messages_el.BlobUploadErrorMsg = function () {
        return "Δεν ήταν δυνατή η μεταφόρτωση του αρχείου";
    };
    Messages_el.BlobReadFileErrorMsg = function () {
        return "Σφάλμα κατά την διάρκεια ανάγνωσης του αρχείου...";
    };
    Messages_el.BlobReadFileCanceledMsg = function () {
        return "Η διαδικασία ανάγνωσης του αρχείου ακυρώθηκε...";
    };
    Messages_el.BlobUploadFailedMsg = function () {
        return "Σφάλμα κατά την διάρκεια μεταφόρτωσης του αρχείου...";
    };
    Messages_el.BlobUploadCanceledMsg = function () {
        return "Η διαδικασία μεταφόρτωσης του αρχείου ακυρώθηκε...";
    };
    Messages_el.PasswordExpirarionAlertInBannerMsg = function (iDays) {
        return "Ο κωδικός σας θα λήξει σε " + iDays + " μέρες. Παρακαλώ φροντίστε να τον αλλάξετε εγκαίρως.";
    };
    Messages_el.PasswordExpirarionAlertInBannerMsg_OneDay = function () {
        return "Ο κωδικός σας θα λήξει σε μία μέρα! Παρακαλώ φροντίστε να τον αλλάξετε εγκαίρως.";
    };
    Messages_el.ChangePasswdDialog_Null_PasswordsMsg = function () {
        return "Τα πεδία με τους κωδικούς δεν μπορούν να είναι άδεια!";
    };
    Messages_el.ChangePasswdDialog_Null_UserMsg = function () {
        return "Το πεδίο 'Χρήστης' δεν μπορεί να είναι άδειο!";
    };
    Messages_el.ChangePasswdDialog_Null_NewPasswordsDifMsg = function () {
        return "Τα δύο πεδία με τον νέο κωδικό πρέπει να έχουν την ίδια τιμή!";
    };
    Messages_el.ChangePasswdDialog_SuccessMsg = function () {
        return "Ο ορισμός νέου κωδικού ήταν επιτυχής.";
    };
    Messages_el.ImportExcel_MessageBox_Success_Message = function (fileName, totalRows) {
        return "Η εισαγωγή του αρχείου '" + fileName + "' ολοκληρώθηκε με επιτυχία.<br>Πλήθος γραμμών : " + totalRows + "";
    };
    Messages_el.ImportExcel_MessageBox_GenericError_Message = function () {
        return "Παρουσιάστηκε σφάλμα κατά την επεξεργασία του αρχείου.";
    };
    Messages_el.ImportExcel_MessageBox_NoRecordsError_Message = function () {
        return "Το αρχείο δεν περιλαμβάνει εγγραφές";
    };
    Messages_el.USER_STATUS_NOT_ACTIVE = function () {
        return "Ο λογαριασμός σας δεν είναι ενεργός";
    };
    Messages_el.SUBSCRIBER_NOT_ASSOC_WITH_APP = function () {
        return "Ο φορέας δεν έχει άδεια χρήσης της εφαρμογής";
    };
    Messages_el.USER_NOT_ASSOC_WITH_APP_ALTHOUGH_SUBSCRPTN_OK = function () {
        return "Ο λογαριασμός σας δεν έχει άδεια χρήσης της εφαρμογής";
    };
    Messages_el.USER_NOT_ASSOC_WITH_APP = function () {
        return "Ο λογαριασμός σας δεν έχει άδεια χρήσης της εφαρμογής";
    };
    Messages_el.SUBSCRIBER_STATUS_NOT_ACTIVE = function () {
        return "Ο λογαριασμός φορέα δεν είναι ενεργός";
    };
    Messages_el.SUBSCRIBER_MODULEGROUP_NOT_ACTIVE = function () {
        return "Το πακέτο εφαρμογών δεν έχει ενεργοποιηθεί";
    };
    Messages_el.SUBSCRIBER_MODULEGROUP_TEMPRL_MISMATCH = function () {
        return "Η περίοδος λειτουργίας του πακέτου εφαρμογών δεν είναι έγκυρη";
    };
    Messages_el.UNKNOWN_APP = function () {
        return "Άγνωστη εφαρμογή";
    };
    Messages_el.AUTH_ERROR = function () {
        return "Σφάλμα κατά την ταυτοποίηση";
    };
    Messages_el.WRONG_PASSWORD = function () {
        return "Ο λογαριασμός ή ο κωδικός πρόσβασης που δώσατε δεν είναι έγκυρος";
    };
    Messages_el.ACCOUNT_DOESNOT_EXIST = function () {
        return "Ο λογαριασμός ή ο κωδικός πρόσβασης που δώσατε δεν είναι έγκυρος";
    };
    Messages_el.USER_LOCKED_BY_ATTEMPTS_LIMIT = function () {
        return "Ξεπεράσατε το όριο ανεπιτυχών προσπαθειών σύνδεσης. Ο λογαριασμός σας θα παραμείνει κλειδωμένος για τα επόμενα 30 λεπτά. Δοκιμάστε αργότερα.";
    };
    Messages_el.USER_PASSWORD_HAS_EXPIRED = function () {
        return "Ο κωδικός σας έχει λήξει. Παρακαλώ αλλάξτε τον κωδικό σας.";
    };
    Messages_el.INVALID_SUBSCRIBER = function () {
        return "Μη έγκυρος Φορέας";
    };
    Messages_el.WRONG_LOGIN_SUBSCRIBER = function () {
        return "Λανθασμένος Φορέας Εισόδου";
    };
    Messages_el.GetAllIds_TotalItemsMsg = function (iTotalItems) {
        return "Η συγκεκριμένη λειτουργία δεν μπορεί να πραγματοποιηθεί γιατί οι προς επιλογή/αποεπιλογή εγγραφές είναι πάρα πολλές (" + iTotalItems + ").<br>Περιορίστε τις επιστρεφόμενες εγγραφές από τα φίλτρα αναζήτησης έτσι ώστε οι συνολικές εγγραφές να είναι λιγότερες από 10.000.";
    };
    Messages_el.clfr_fite_id_fk = function () {
        return "clfr_fite_id_fk";
    };
    Messages_el.pcla_clas_id_fk = function () {
        return "pcla_clas_id_fk";
    };
    Messages_el.dafi_clas_id_fk = function () {
        return "dafi_clas_id_fk";
    };
    Messages_el.pade_dema_id_fk = function () {
        return "pade_dema_id_fk";
    };
    Messages_el.sucdsuca_id_cult_id__ui = function () {
        return "sucdsuca_id_cult_id__ui";
    };
    Messages_el.cult_name_un = function () {
        return "cult_name_un";
    };
    Messages_el.cult_code_un = function () {
        return "cult_code_un";
    };
    Messages_el.file_dir_path_un = function () {
        return "file_dir_path_un";
    };
    Messages_el.super_class_code_un = function () {
        return "super_class_code_un";
    };
    Messages_el.super_class_name_un = function () {
        return "super_class_name_un";
    };
    Messages_el.prco_fite_un = function () {
        return "prco_fite_un";
    };
    Messages_el.fite_clna_un = function () {
        return "fite_clna_un";
    };
    Messages_el.file_template_name_un = function () {
        return "file_template_name_un";
    };
    Messages_el.activity_code_un = function () {
        return "activity_code_un";
    };
    Messages_el.activity_name_un = function () {
        return "activity_name_un";
    };
    Messages_el.cover_type_code_un = function () {
        return "cover_type_code_un";
    };
    Messages_el.cover_type_name_un = function () {
        return "cover_type_name_un";
    };
    Messages_el.strings = {};
    return Messages_el;
})();
var Messages_en = (function () {
    function Messages_en() {
    }
    Messages_en.PersistenceErrorOccured = function () {
        return "Persistence Error.";
    };
    Messages_en.Previous = function () {
        return "Previous";
    };
    Messages_en.Next = function () {
        return "Next";
    };
    Messages_en.SaveButtonLabel = function () {
        return "SaveButtonLabel";
    };
    Messages_en.ReturnButtonLabel = function () {
        return "ReturnButtonLabel";
    };
    Messages_en.CancelButtonLabel = function () {
        return "CancelButtonLabel";
    };
    Messages_en.DeleteButtonLabel = function () {
        return "DeleteButtonLabel";
    };
    Messages_en.NewButtonLabel = function () {
        return "NewButtonLabel";
    };
    Messages_en.MenuApplicationsTitle = function () {
        return "MenuApplicationsTitle";
    };
    Messages_en.MenuSettingsTitle = function () {
        return "MenuSettingsTitle";
    };
    Messages_en.MenuAppSettingsTitle = function () {
        return "MenuAppSettingsTitle";
    };
    Messages_en.AppSettingsTitle = function () {
        return "AppSettingsTitle";
    };
    Messages_en.ThemeChoiceTabTitle = function () {
        return "ThemeChoiceTabTitle";
    };
    Messages_en.MenuCustomerTitle = function () {
        return "MenuCustomerTitle";
    };
    Messages_en.CEVCustomerTitle = function () {
        return "CEVCustomerTitle";
    };
    Messages_en.LoggedInUserLabel = function () {
        return "LoggedInUserLabel";
    };
    Messages_en.LogoutLinkTitle = function () {
        return "LogoutLinkTitle";
    };
    Messages_en.DatatableEmptyMessage = function () {
        return "There are no records with these criteria";
    };
    Messages_en.DatatableEntries = function () {
        return "DatatableEntries";
    };
    Messages_en.ViewExpirationDialogBody = function () {
        return "Due to inactivity on the page, the user's transaction has expired. Type OK to navigate to the home page.";
    };
    Messages_en.ViewExpirationDialogTitle = function () {
        return "The page has expired ...";
    };
    Messages_en.DeleteDialogMsg = function () {
        return "The record you have selected will be permanently deleted! Would you like to proceed;";
    };
    Messages_en.WarningMsgHeader = function () {
        return "Warning Message!";
    };
    Messages_en.UncommittedChangesMsg = function () {
        return "No changes to save";
    };
    Messages_en.EntityCreationMsg = function (p0) {
        return "Insert Record: " + p0 + "";
    };
    Messages_en.EntityRemovalMsg = function (p0) {
        return "Delete Record: " + p0 + "";
    };
    Messages_en.EntityUpdateMsg = function (p0) {
        return "Update Record: " + p0 + "";
    };
    Messages_en.EntityCreationError = function (p0) {
        return "Insert Record Error: " + p0 + "";
    };
    Messages_en.EntityRemovalError = function (p0) {
        return "Delete Record Error: " + p0 + "";
    };
    Messages_en.EntityUpdateError = function (p0) {
        return "Update Record Error: " + p0 + "";
    };
    Messages_en.EntitySuccessSave = function () {
        return "Save successfully completed.";
    };
    Messages_en.EntitySuccessDeletion = function () {
        return "Deletion successfully completed.";
    };
    Messages_en.EntitySuccessDeletion2 = function () {
        return "The record deleted successfully.";
    };
    Messages_en.EntityDeletionWarningMsg = function () {
        return "Permanent record deletion. Would you like to proceed?";
    };
    Messages_en.ExceptionInsideValidationErrorMessage = function (p0, p1) {
        return "An error occurred while performing the validation method (" + p0 + "), error message: " + p1 + ".";
    };
    Messages_en.NoPendingChanges = function () {
        return "There are no changes to save";
    };
    Messages_en.SearchNoResults = function () {
        return "No records were found for your search criteria.";
    };
    Messages_en.optimisticLockException = function () {
        return "The record has been changed by another user.";
    };
    Messages_en.insufficientPrivilegesException = function () {
        return "Insufficient Privileges";
    };
    Messages_en.INVALID_VAT = function (sVat) {
        return "INVALID_VAT";
    };
    Messages_en.USER_NOT_REGISTERED = function (sVat) {
        return "USER_NOT_REGISTERED";
    };
    Messages_en.USER_NOT_REGISTERED_IN_APP = function (sAppName, sVat) {
        return "USER_NOT_REGISTERED_IN_APP";
    };
    Messages_en.USER_NOT_REGISTERED_IN_APP_BY_SUBSCRIBER = function (sAppName, sVat) {
        return "USER_NOT_REGISTERED_IN_APP_BY_SUBSCRIBER";
    };
    Messages_en.TOO_MANY_RESULT_ENTITIES_FOR_EXCEL = function (p0) {
        return "The records exceed the limit of " + p0 + ". Use filter to limit the number of records.";
    };
    Messages_en.NEW_PASSWORD_SET = function () {
        return "Successful password change.";
    };
    Messages_en.maxCheckedIdsToInsert_in_MultipleInsertBtn_exceeded = function (p0) {
        return "New Record insertion limit (" + p0 + ") exceeded.";
    };
    Messages_en.HttpRequestTimeOutMessage = function () {
        return "The server waiting time limit was exceeded. Form data are not updated.";
    };
    Messages_en.ServerCommunicationError = function () {
        return "Error communicating with the server.";
    };
    Messages_en.SessionExpired = function () {
        return "The session has been expired.";
    };
    Messages_en.OnbeforeunloadMsg = function () {
        return "If you continue, any changes you have not saved will be lost!\n\n";
    };
    Messages_en.EntityById_NotFoundMsg = function (pk) {
        return "The record with PK:" + pk + " was not found in the database";
    };
    Messages_en.EntityByCode_NotFoundMsg = function () {
        return "No record was not found in the database";
    };
    Messages_en.EntityByCode_TooManyRowsForCodeMsg = function (codeValue) {
        return "More than one records found for '" + codeValue + "'";
    };
    Messages_en.EntityByCode_NoRecordForCodeMsg = function (codeValue) {
        return "No record found for '" + codeValue + "'";
    };
    Messages_en.SaveChangesWarningMsg = function () {
        return "Do you want to save the changes?";
    };
    Messages_en.NoChangesToSaveMsg = function () {
        return "No changes to save";
    };
    Messages_en.OnFailuredSaveMsg = function () {
        return "The following errors were identified";
    };
    Messages_en.FieldValidationErrorMsg = function (fieldName) {
        return "Invalid value in field <b>" + fieldName + "</b>";
    };
    Messages_en.FieldValidation_RequiredMsg1 = function (fieldName) {
        return "The <b>" + fieldName + "</b> field is required";
    };
    Messages_en.FieldValidation_RequiredMsg2 = function () {
        return "Required field";
    };
    Messages_en.FieldValidation_MaxLengthMsg = function (fieldName, fieldValue, iMaxLength) {
        return "The <b>" + fieldName + "</b> field (" + fieldValue + ") must be up to " + iMaxLength + " characters";
    };
    Messages_en.FieldValidation_MinMaxValueMsg = function (fieldName, fieldValue, maxVal, minVal) {
        return "The <b>" + fieldName + "</b> field (" + fieldValue + ") must be between " + minVal + " and " + maxVal + "";
    };
    Messages_en.FieldValidation_MinValueMsg = function (fieldName, fieldValue, minVal) {
        return "The <b>" + fieldName + "</b> field (" + fieldValue + ") must be greater than " + minVal + "";
    };
    Messages_en.FieldValidation_MaxValueMsg = function (fieldName, fieldValue, maxVal) {
        return "The <b>" + fieldName + "</b> field (" + fieldValue + ") must be less than " + maxVal + "";
    };
    Messages_en.FieldValidation_MaxDecimalsMsg = function (fieldName, fieldValue, maxDecimals) {
        return "The <b>" + fieldName + "</b> field (" + fieldValue + ") must have up to " + maxDecimals + " decimals";
    };
    Messages_en.FieldValidation_NumbersOnlyMsg = function () {
        return "Numbers only";
    };
    Messages_en.FieldValidation_IntegersOnlyMsg = function () {
        return "Integer numbers only";
    };
    Messages_en.FieldValidation_DecimalsMsg = function (iDecimals) {
        return "Numbers only, " + iDecimals + " decimals and one decimal point";
    };
    Messages_en.FieldValidation_Numbers_Allowed_MaxMsg = function (val) {
        return "Allowed values: To " + val + "";
    };
    Messages_en.FieldValidation_Numbers_Allowed_MinMsg = function (val) {
        return "Allowed values: From " + val + "";
    };
    Messages_en.FieldValidation_Numbers_Allowed_MinMaxMsg = function (val1, val2) {
        return "Allowed values: From " + val1 + " to " + val2 + "";
    };
    Messages_en.FieldValidation_DateFormatMsg = function (validFormat) {
        return "Valid format: " + validFormat + "";
    };
    Messages_en.FieldValidation_DateFromMsg = function (val) {
        return "From: " + val + "";
    };
    Messages_en.FieldValidation_DateToMsg = function (val) {
        return "to: " + val + "";
    };
    Messages_en.FieldValidation_DateFromToMsg = function (val1, val2) {
        return "From: " + val1 + " to: " + val2 + "";
    };
    Messages_en.FieldValidation_Blob_FileTypeMsg = function () {
        return "Allowed:";
    };
    Messages_en.FieldValidation_Blob_FileSizeMsg = function (maxSize) {
        return "Files allowed up to: " + maxSize + " KB";
    };
    Messages_en.BlobUploadErrorMsg = function () {
        return "Unable to upload file";
    };
    Messages_en.BlobReadFileErrorMsg = function () {
        return "Error while reading the file...";
    };
    Messages_en.BlobReadFileCanceledMsg = function () {
        return "The process of reading the file was canceled...";
    };
    Messages_en.BlobUploadFailedMsg = function () {
        return "Error while uploading the file...";
    };
    Messages_en.BlobUploadCanceledMsg = function () {
        return "The process of uploading the file was canceled...";
    };
    Messages_en.PasswordExpirarionAlertInBannerMsg = function (iDays) {
        return "Your password will expire in " + iDays + " days. Please be sure to change it in time.";
    };
    Messages_en.PasswordExpirarionAlertInBannerMsg_OneDay = function () {
        return "Your password will expire in one day! Please be sure to change it in time.";
    };
    Messages_en.ChangePasswdDialog_Null_PasswordsMsg = function () {
        return "Password fields cannot be null!";
    };
    Messages_en.ChangePasswdDialog_Null_UserMsg = function () {
        return "The field 'User' cannot be null!";
    };
    Messages_en.ChangePasswdDialog_Null_NewPasswordsDifMsg = function () {
        return "The two new password fields must have the same value!";
    };
    Messages_en.ChangePasswdDialog_SuccessMsg = function () {
        return "New password was set successfully.";
    };
    Messages_en.ImportExcel_MessageBox_Success_Message = function (fileName, totalRows) {
        return "File '" + fileName + "' import completed successfully.<br>Total rows imported: " + totalRows + "";
    };
    Messages_en.ImportExcel_MessageBox_GenericError_Message = function () {
        return "An error occurred while processing the file.";
    };
    Messages_en.ImportExcel_MessageBox_NoRecordsError_Message = function () {
        return "The file does not contain records";
    };
    Messages_en.USER_STATUS_NOT_ACTIVE = function () {
        return "Your account is not active";
    };
    Messages_en.SUBSCRIBER_NOT_ASSOC_WITH_APP = function () {
        return "The subscriber has no usage license of this application";
    };
    Messages_en.USER_NOT_ASSOC_WITH_APP_ALTHOUGH_SUBSCRPTN_OK = function () {
        return "Your account has no usage license of this application";
    };
    Messages_en.USER_NOT_ASSOC_WITH_APP = function () {
        return "Your account has no usage license of this application";
    };
    Messages_en.SUBSCRIBER_STATUS_NOT_ACTIVE = function () {
        return "The subscriber's account is not active";
    };
    Messages_en.SUBSCRIBER_MODULEGROUP_NOT_ACTIVE = function () {
        return "The application package is not active";
    };
    Messages_en.SUBSCRIBER_MODULEGROUP_TEMPRL_MISMATCH = function () {
        return "Application package usage period is not valid";
    };
    Messages_en.UNKNOWN_APP = function () {
        return "Unknown application";
    };
    Messages_en.AUTH_ERROR = function () {
        return "Authentication error";
    };
    Messages_en.WRONG_PASSWORD = function () {
        return "The account or password you entered is invalid";
    };
    Messages_en.ACCOUNT_DOESNOT_EXIST = function () {
        return "The account or password you entered is invalid";
    };
    Messages_en.USER_LOCKED_BY_ATTEMPTS_LIMIT = function () {
        return "You have exceeded the limit of unsuccessful login attempts. Your account will remain locked for the next 30 minutes. Try it later.";
    };
    Messages_en.USER_PASSWORD_HAS_EXPIRED = function () {
        return "Your password has expired. Please change your password.";
    };
    Messages_en.INVALID_SUBSCRIBER = function () {
        return "Invalid Subscriber";
    };
    Messages_en.WRONG_LOGIN_SUBSCRIBER = function () {
        return "Wrong Login Subscriber";
    };
    Messages_en.GetAllIds_TotalItemsMsg = function (iTotalItems) {
        return "This feature can not be performed because the records to be selected/de-selected are too many (" + iTotalItems + ").<br>Restrict retrieved records from search filters so that total records are less than 10,000.";
    };
    Messages_en.clfr_fite_id_fk = function () {
        return "The record cannot be deleted because it is being used.";
    };
    Messages_en.pcla_clas_id_fk = function () {
        return "Cannot be deleted because it is being used.";
    };
    Messages_en.dafi_clas_id_fk = function () {
        return "Cannot be deleted because it is being used.";
    };
    Messages_en.pade_dema_id_fk = function () {
        return "Cannot be deleted because it is being used.";
    };
    Messages_en.sucdsuca_id_cult_id__ui = function () {
        return "Crop Already Exists.";
    };
    Messages_en.cult_name_un = function () {
        return "Crop Name Already Exists.";
    };
    Messages_en.cult_code_un = function () {
        return "Crop Code Already Exists.";
    };
    Messages_en.file_dir_path_un = function () {
        return "Only One File Directory is Allowed.";
    };
    Messages_en.super_class_code_un = function () {
        return "Code Already Exists.";
    };
    Messages_en.super_class_name_un = function () {
        return "Name Already Exists.";
    };
    Messages_en.prco_fite_un = function () {
        return "System Column Already Exists.";
    };
    Messages_en.fite_clna_un = function () {
        return "Import Column Already Exists.";
    };
    Messages_en.file_template_name_un = function () {
        return "Data Import Template Name Already Exists.";
    };
    Messages_en.activity_code_un = function () {
        return "Code Already Exists.";
    };
    Messages_en.activity_name_un = function () {
        return "Name Already Exists.";
    };
    Messages_en.cover_type_code_un = function () {
        return "Land Cover Code Already Exists.";
    };
    Messages_en.cover_type_name_un = function () {
        return "Land Cover Name Already Exists.";
    };
    Messages_en.strings = {};
    return Messages_en;
})();
var Messages = (function () {
    function Messages() {
    }
    Messages.setActiveLangByGlobalLang = function ($scope) {
        var self = this;
        switch ($scope.globals.globalLang) {
            case 'el':
                self.activeLang = Language.el;
                break;
            case 'en':
                self.activeLang = Language.en;
                break;
            default: self.activeLang = Language.en;
        }
    };
    Messages.PersistenceErrorOccured = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.PersistenceErrorOccured();
            case Language.en:
                return Messages_en.PersistenceErrorOccured();
            default:
                return Messages_en.PersistenceErrorOccured();
        }
    };
    Messages.Previous = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.Previous();
            case Language.en:
                return Messages_en.Previous();
            default:
                return Messages_en.Previous();
        }
    };
    Messages.Next = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.Next();
            case Language.en:
                return Messages_en.Next();
            default:
                return Messages_en.Next();
        }
    };
    Messages.SaveButtonLabel = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.SaveButtonLabel();
            case Language.en:
                return Messages_en.SaveButtonLabel();
            default:
                return Messages_en.SaveButtonLabel();
        }
    };
    Messages.ReturnButtonLabel = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ReturnButtonLabel();
            case Language.en:
                return Messages_en.ReturnButtonLabel();
            default:
                return Messages_en.ReturnButtonLabel();
        }
    };
    Messages.CancelButtonLabel = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.CancelButtonLabel();
            case Language.en:
                return Messages_en.CancelButtonLabel();
            default:
                return Messages_en.CancelButtonLabel();
        }
    };
    Messages.DeleteButtonLabel = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.DeleteButtonLabel();
            case Language.en:
                return Messages_en.DeleteButtonLabel();
            default:
                return Messages_en.DeleteButtonLabel();
        }
    };
    Messages.NewButtonLabel = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.NewButtonLabel();
            case Language.en:
                return Messages_en.NewButtonLabel();
            default:
                return Messages_en.NewButtonLabel();
        }
    };
    Messages.MenuApplicationsTitle = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.MenuApplicationsTitle();
            case Language.en:
                return Messages_en.MenuApplicationsTitle();
            default:
                return Messages_en.MenuApplicationsTitle();
        }
    };
    Messages.MenuSettingsTitle = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.MenuSettingsTitle();
            case Language.en:
                return Messages_en.MenuSettingsTitle();
            default:
                return Messages_en.MenuSettingsTitle();
        }
    };
    Messages.MenuAppSettingsTitle = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.MenuAppSettingsTitle();
            case Language.en:
                return Messages_en.MenuAppSettingsTitle();
            default:
                return Messages_en.MenuAppSettingsTitle();
        }
    };
    Messages.AppSettingsTitle = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.AppSettingsTitle();
            case Language.en:
                return Messages_en.AppSettingsTitle();
            default:
                return Messages_en.AppSettingsTitle();
        }
    };
    Messages.ThemeChoiceTabTitle = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ThemeChoiceTabTitle();
            case Language.en:
                return Messages_en.ThemeChoiceTabTitle();
            default:
                return Messages_en.ThemeChoiceTabTitle();
        }
    };
    Messages.MenuCustomerTitle = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.MenuCustomerTitle();
            case Language.en:
                return Messages_en.MenuCustomerTitle();
            default:
                return Messages_en.MenuCustomerTitle();
        }
    };
    Messages.CEVCustomerTitle = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.CEVCustomerTitle();
            case Language.en:
                return Messages_en.CEVCustomerTitle();
            default:
                return Messages_en.CEVCustomerTitle();
        }
    };
    Messages.LoggedInUserLabel = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.LoggedInUserLabel();
            case Language.en:
                return Messages_en.LoggedInUserLabel();
            default:
                return Messages_en.LoggedInUserLabel();
        }
    };
    Messages.LogoutLinkTitle = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.LogoutLinkTitle();
            case Language.en:
                return Messages_en.LogoutLinkTitle();
            default:
                return Messages_en.LogoutLinkTitle();
        }
    };
    Messages.DatatableEmptyMessage = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.DatatableEmptyMessage();
            case Language.en:
                return Messages_en.DatatableEmptyMessage();
            default:
                return Messages_en.DatatableEmptyMessage();
        }
    };
    Messages.DatatableEntries = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.DatatableEntries();
            case Language.en:
                return Messages_en.DatatableEntries();
            default:
                return Messages_en.DatatableEntries();
        }
    };
    Messages.ViewExpirationDialogBody = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ViewExpirationDialogBody();
            case Language.en:
                return Messages_en.ViewExpirationDialogBody();
            default:
                return Messages_en.ViewExpirationDialogBody();
        }
    };
    Messages.ViewExpirationDialogTitle = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ViewExpirationDialogTitle();
            case Language.en:
                return Messages_en.ViewExpirationDialogTitle();
            default:
                return Messages_en.ViewExpirationDialogTitle();
        }
    };
    Messages.DeleteDialogMsg = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.DeleteDialogMsg();
            case Language.en:
                return Messages_en.DeleteDialogMsg();
            default:
                return Messages_en.DeleteDialogMsg();
        }
    };
    Messages.WarningMsgHeader = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.WarningMsgHeader();
            case Language.en:
                return Messages_en.WarningMsgHeader();
            default:
                return Messages_en.WarningMsgHeader();
        }
    };
    Messages.UncommittedChangesMsg = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.UncommittedChangesMsg();
            case Language.en:
                return Messages_en.UncommittedChangesMsg();
            default:
                return Messages_en.UncommittedChangesMsg();
        }
    };
    Messages.EntityCreationMsg = function (p0) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityCreationMsg(p0);
            case Language.en:
                return Messages_en.EntityCreationMsg(p0);
            default:
                return Messages_en.EntityCreationMsg(p0);
        }
    };
    Messages.EntityRemovalMsg = function (p0) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityRemovalMsg(p0);
            case Language.en:
                return Messages_en.EntityRemovalMsg(p0);
            default:
                return Messages_en.EntityRemovalMsg(p0);
        }
    };
    Messages.EntityUpdateMsg = function (p0) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityUpdateMsg(p0);
            case Language.en:
                return Messages_en.EntityUpdateMsg(p0);
            default:
                return Messages_en.EntityUpdateMsg(p0);
        }
    };
    Messages.EntityCreationError = function (p0) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityCreationError(p0);
            case Language.en:
                return Messages_en.EntityCreationError(p0);
            default:
                return Messages_en.EntityCreationError(p0);
        }
    };
    Messages.EntityRemovalError = function (p0) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityRemovalError(p0);
            case Language.en:
                return Messages_en.EntityRemovalError(p0);
            default:
                return Messages_en.EntityRemovalError(p0);
        }
    };
    Messages.EntityUpdateError = function (p0) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityUpdateError(p0);
            case Language.en:
                return Messages_en.EntityUpdateError(p0);
            default:
                return Messages_en.EntityUpdateError(p0);
        }
    };
    Messages.EntitySuccessSave = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntitySuccessSave();
            case Language.en:
                return Messages_en.EntitySuccessSave();
            default:
                return Messages_en.EntitySuccessSave();
        }
    };
    Messages.EntitySuccessDeletion = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntitySuccessDeletion();
            case Language.en:
                return Messages_en.EntitySuccessDeletion();
            default:
                return Messages_en.EntitySuccessDeletion();
        }
    };
    Messages.EntitySuccessDeletion2 = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntitySuccessDeletion2();
            case Language.en:
                return Messages_en.EntitySuccessDeletion2();
            default:
                return Messages_en.EntitySuccessDeletion2();
        }
    };
    Messages.EntityDeletionWarningMsg = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityDeletionWarningMsg();
            case Language.en:
                return Messages_en.EntityDeletionWarningMsg();
            default:
                return Messages_en.EntityDeletionWarningMsg();
        }
    };
    Messages.ExceptionInsideValidationErrorMessage = function (p0, p1) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ExceptionInsideValidationErrorMessage(p0, p1);
            case Language.en:
                return Messages_en.ExceptionInsideValidationErrorMessage(p0, p1);
            default:
                return Messages_en.ExceptionInsideValidationErrorMessage(p0, p1);
        }
    };
    Messages.NoPendingChanges = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.NoPendingChanges();
            case Language.en:
                return Messages_en.NoPendingChanges();
            default:
                return Messages_en.NoPendingChanges();
        }
    };
    Messages.SearchNoResults = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.SearchNoResults();
            case Language.en:
                return Messages_en.SearchNoResults();
            default:
                return Messages_en.SearchNoResults();
        }
    };
    Messages.optimisticLockException = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.optimisticLockException();
            case Language.en:
                return Messages_en.optimisticLockException();
            default:
                return Messages_en.optimisticLockException();
        }
    };
    Messages.insufficientPrivilegesException = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.insufficientPrivilegesException();
            case Language.en:
                return Messages_en.insufficientPrivilegesException();
            default:
                return Messages_en.insufficientPrivilegesException();
        }
    };
    Messages.INVALID_VAT = function (sVat) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.INVALID_VAT(sVat);
            case Language.en:
                return Messages_en.INVALID_VAT(sVat);
            default:
                return Messages_en.INVALID_VAT(sVat);
        }
    };
    Messages.USER_NOT_REGISTERED = function (sVat) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.USER_NOT_REGISTERED(sVat);
            case Language.en:
                return Messages_en.USER_NOT_REGISTERED(sVat);
            default:
                return Messages_en.USER_NOT_REGISTERED(sVat);
        }
    };
    Messages.USER_NOT_REGISTERED_IN_APP = function (sAppName, sVat) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.USER_NOT_REGISTERED_IN_APP(sAppName, sVat);
            case Language.en:
                return Messages_en.USER_NOT_REGISTERED_IN_APP(sAppName, sVat);
            default:
                return Messages_en.USER_NOT_REGISTERED_IN_APP(sAppName, sVat);
        }
    };
    Messages.USER_NOT_REGISTERED_IN_APP_BY_SUBSCRIBER = function (sAppName, sVat) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.USER_NOT_REGISTERED_IN_APP_BY_SUBSCRIBER(sAppName, sVat);
            case Language.en:
                return Messages_en.USER_NOT_REGISTERED_IN_APP_BY_SUBSCRIBER(sAppName, sVat);
            default:
                return Messages_en.USER_NOT_REGISTERED_IN_APP_BY_SUBSCRIBER(sAppName, sVat);
        }
    };
    Messages.TOO_MANY_RESULT_ENTITIES_FOR_EXCEL = function (p0) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.TOO_MANY_RESULT_ENTITIES_FOR_EXCEL(p0);
            case Language.en:
                return Messages_en.TOO_MANY_RESULT_ENTITIES_FOR_EXCEL(p0);
            default:
                return Messages_en.TOO_MANY_RESULT_ENTITIES_FOR_EXCEL(p0);
        }
    };
    Messages.NEW_PASSWORD_SET = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.NEW_PASSWORD_SET();
            case Language.en:
                return Messages_en.NEW_PASSWORD_SET();
            default:
                return Messages_en.NEW_PASSWORD_SET();
        }
    };
    Messages.maxCheckedIdsToInsert_in_MultipleInsertBtn_exceeded = function (p0) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.maxCheckedIdsToInsert_in_MultipleInsertBtn_exceeded(p0);
            case Language.en:
                return Messages_en.maxCheckedIdsToInsert_in_MultipleInsertBtn_exceeded(p0);
            default:
                return Messages_en.maxCheckedIdsToInsert_in_MultipleInsertBtn_exceeded(p0);
        }
    };
    Messages.HttpRequestTimeOutMessage = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.HttpRequestTimeOutMessage();
            case Language.en:
                return Messages_en.HttpRequestTimeOutMessage();
            default:
                return Messages_en.HttpRequestTimeOutMessage();
        }
    };
    Messages.ServerCommunicationError = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ServerCommunicationError();
            case Language.en:
                return Messages_en.ServerCommunicationError();
            default:
                return Messages_en.ServerCommunicationError();
        }
    };
    Messages.SessionExpired = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.SessionExpired();
            case Language.en:
                return Messages_en.SessionExpired();
            default:
                return Messages_en.SessionExpired();
        }
    };
    Messages.OnbeforeunloadMsg = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.OnbeforeunloadMsg();
            case Language.en:
                return Messages_en.OnbeforeunloadMsg();
            default:
                return Messages_en.OnbeforeunloadMsg();
        }
    };
    Messages.EntityById_NotFoundMsg = function (pk) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityById_NotFoundMsg(pk);
            case Language.en:
                return Messages_en.EntityById_NotFoundMsg(pk);
            default:
                return Messages_en.EntityById_NotFoundMsg(pk);
        }
    };
    Messages.EntityByCode_NotFoundMsg = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityByCode_NotFoundMsg();
            case Language.en:
                return Messages_en.EntityByCode_NotFoundMsg();
            default:
                return Messages_en.EntityByCode_NotFoundMsg();
        }
    };
    Messages.EntityByCode_TooManyRowsForCodeMsg = function (codeValue) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityByCode_TooManyRowsForCodeMsg(codeValue);
            case Language.en:
                return Messages_en.EntityByCode_TooManyRowsForCodeMsg(codeValue);
            default:
                return Messages_en.EntityByCode_TooManyRowsForCodeMsg(codeValue);
        }
    };
    Messages.EntityByCode_NoRecordForCodeMsg = function (codeValue) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityByCode_NoRecordForCodeMsg(codeValue);
            case Language.en:
                return Messages_en.EntityByCode_NoRecordForCodeMsg(codeValue);
            default:
                return Messages_en.EntityByCode_NoRecordForCodeMsg(codeValue);
        }
    };
    Messages.SaveChangesWarningMsg = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.SaveChangesWarningMsg();
            case Language.en:
                return Messages_en.SaveChangesWarningMsg();
            default:
                return Messages_en.SaveChangesWarningMsg();
        }
    };
    Messages.NoChangesToSaveMsg = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.NoChangesToSaveMsg();
            case Language.en:
                return Messages_en.NoChangesToSaveMsg();
            default:
                return Messages_en.NoChangesToSaveMsg();
        }
    };
    Messages.OnFailuredSaveMsg = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.OnFailuredSaveMsg();
            case Language.en:
                return Messages_en.OnFailuredSaveMsg();
            default:
                return Messages_en.OnFailuredSaveMsg();
        }
    };
    Messages.FieldValidationErrorMsg = function (fieldName) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidationErrorMsg(fieldName);
            case Language.en:
                return Messages_en.FieldValidationErrorMsg(fieldName);
            default:
                return Messages_en.FieldValidationErrorMsg(fieldName);
        }
    };
    Messages.FieldValidation_RequiredMsg1 = function (fieldName) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_RequiredMsg1(fieldName);
            case Language.en:
                return Messages_en.FieldValidation_RequiredMsg1(fieldName);
            default:
                return Messages_en.FieldValidation_RequiredMsg1(fieldName);
        }
    };
    Messages.FieldValidation_RequiredMsg2 = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_RequiredMsg2();
            case Language.en:
                return Messages_en.FieldValidation_RequiredMsg2();
            default:
                return Messages_en.FieldValidation_RequiredMsg2();
        }
    };
    Messages.FieldValidation_MaxLengthMsg = function (fieldName, fieldValue, iMaxLength) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_MaxLengthMsg(fieldName, fieldValue, iMaxLength);
            case Language.en:
                return Messages_en.FieldValidation_MaxLengthMsg(fieldName, fieldValue, iMaxLength);
            default:
                return Messages_en.FieldValidation_MaxLengthMsg(fieldName, fieldValue, iMaxLength);
        }
    };
    Messages.FieldValidation_MinMaxValueMsg = function (fieldName, fieldValue, maxVal, minVal) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_MinMaxValueMsg(fieldName, fieldValue, maxVal, minVal);
            case Language.en:
                return Messages_en.FieldValidation_MinMaxValueMsg(fieldName, fieldValue, maxVal, minVal);
            default:
                return Messages_en.FieldValidation_MinMaxValueMsg(fieldName, fieldValue, maxVal, minVal);
        }
    };
    Messages.FieldValidation_MinValueMsg = function (fieldName, fieldValue, minVal) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_MinValueMsg(fieldName, fieldValue, minVal);
            case Language.en:
                return Messages_en.FieldValidation_MinValueMsg(fieldName, fieldValue, minVal);
            default:
                return Messages_en.FieldValidation_MinValueMsg(fieldName, fieldValue, minVal);
        }
    };
    Messages.FieldValidation_MaxValueMsg = function (fieldName, fieldValue, maxVal) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_MaxValueMsg(fieldName, fieldValue, maxVal);
            case Language.en:
                return Messages_en.FieldValidation_MaxValueMsg(fieldName, fieldValue, maxVal);
            default:
                return Messages_en.FieldValidation_MaxValueMsg(fieldName, fieldValue, maxVal);
        }
    };
    Messages.FieldValidation_MaxDecimalsMsg = function (fieldName, fieldValue, maxDecimals) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_MaxDecimalsMsg(fieldName, fieldValue, maxDecimals);
            case Language.en:
                return Messages_en.FieldValidation_MaxDecimalsMsg(fieldName, fieldValue, maxDecimals);
            default:
                return Messages_en.FieldValidation_MaxDecimalsMsg(fieldName, fieldValue, maxDecimals);
        }
    };
    Messages.FieldValidation_NumbersOnlyMsg = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_NumbersOnlyMsg();
            case Language.en:
                return Messages_en.FieldValidation_NumbersOnlyMsg();
            default:
                return Messages_en.FieldValidation_NumbersOnlyMsg();
        }
    };
    Messages.FieldValidation_IntegersOnlyMsg = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_IntegersOnlyMsg();
            case Language.en:
                return Messages_en.FieldValidation_IntegersOnlyMsg();
            default:
                return Messages_en.FieldValidation_IntegersOnlyMsg();
        }
    };
    Messages.FieldValidation_DecimalsMsg = function (iDecimals) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_DecimalsMsg(iDecimals);
            case Language.en:
                return Messages_en.FieldValidation_DecimalsMsg(iDecimals);
            default:
                return Messages_en.FieldValidation_DecimalsMsg(iDecimals);
        }
    };
    Messages.FieldValidation_Numbers_Allowed_MaxMsg = function (val) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_Numbers_Allowed_MaxMsg(val);
            case Language.en:
                return Messages_en.FieldValidation_Numbers_Allowed_MaxMsg(val);
            default:
                return Messages_en.FieldValidation_Numbers_Allowed_MaxMsg(val);
        }
    };
    Messages.FieldValidation_Numbers_Allowed_MinMsg = function (val) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_Numbers_Allowed_MinMsg(val);
            case Language.en:
                return Messages_en.FieldValidation_Numbers_Allowed_MinMsg(val);
            default:
                return Messages_en.FieldValidation_Numbers_Allowed_MinMsg(val);
        }
    };
    Messages.FieldValidation_Numbers_Allowed_MinMaxMsg = function (val1, val2) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_Numbers_Allowed_MinMaxMsg(val1, val2);
            case Language.en:
                return Messages_en.FieldValidation_Numbers_Allowed_MinMaxMsg(val1, val2);
            default:
                return Messages_en.FieldValidation_Numbers_Allowed_MinMaxMsg(val1, val2);
        }
    };
    Messages.FieldValidation_DateFormatMsg = function (validFormat) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_DateFormatMsg(validFormat);
            case Language.en:
                return Messages_en.FieldValidation_DateFormatMsg(validFormat);
            default:
                return Messages_en.FieldValidation_DateFormatMsg(validFormat);
        }
    };
    Messages.FieldValidation_DateFromMsg = function (val) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_DateFromMsg(val);
            case Language.en:
                return Messages_en.FieldValidation_DateFromMsg(val);
            default:
                return Messages_en.FieldValidation_DateFromMsg(val);
        }
    };
    Messages.FieldValidation_DateToMsg = function (val) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_DateToMsg(val);
            case Language.en:
                return Messages_en.FieldValidation_DateToMsg(val);
            default:
                return Messages_en.FieldValidation_DateToMsg(val);
        }
    };
    Messages.FieldValidation_DateFromToMsg = function (val1, val2) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_DateFromToMsg(val1, val2);
            case Language.en:
                return Messages_en.FieldValidation_DateFromToMsg(val1, val2);
            default:
                return Messages_en.FieldValidation_DateFromToMsg(val1, val2);
        }
    };
    Messages.FieldValidation_Blob_FileTypeMsg = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_Blob_FileTypeMsg();
            case Language.en:
                return Messages_en.FieldValidation_Blob_FileTypeMsg();
            default:
                return Messages_en.FieldValidation_Blob_FileTypeMsg();
        }
    };
    Messages.FieldValidation_Blob_FileSizeMsg = function (maxSize) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_Blob_FileSizeMsg(maxSize);
            case Language.en:
                return Messages_en.FieldValidation_Blob_FileSizeMsg(maxSize);
            default:
                return Messages_en.FieldValidation_Blob_FileSizeMsg(maxSize);
        }
    };
    Messages.BlobUploadErrorMsg = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.BlobUploadErrorMsg();
            case Language.en:
                return Messages_en.BlobUploadErrorMsg();
            default:
                return Messages_en.BlobUploadErrorMsg();
        }
    };
    Messages.BlobReadFileErrorMsg = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.BlobReadFileErrorMsg();
            case Language.en:
                return Messages_en.BlobReadFileErrorMsg();
            default:
                return Messages_en.BlobReadFileErrorMsg();
        }
    };
    Messages.BlobReadFileCanceledMsg = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.BlobReadFileCanceledMsg();
            case Language.en:
                return Messages_en.BlobReadFileCanceledMsg();
            default:
                return Messages_en.BlobReadFileCanceledMsg();
        }
    };
    Messages.BlobUploadFailedMsg = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.BlobUploadFailedMsg();
            case Language.en:
                return Messages_en.BlobUploadFailedMsg();
            default:
                return Messages_en.BlobUploadFailedMsg();
        }
    };
    Messages.BlobUploadCanceledMsg = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.BlobUploadCanceledMsg();
            case Language.en:
                return Messages_en.BlobUploadCanceledMsg();
            default:
                return Messages_en.BlobUploadCanceledMsg();
        }
    };
    Messages.PasswordExpirarionAlertInBannerMsg = function (iDays) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.PasswordExpirarionAlertInBannerMsg(iDays);
            case Language.en:
                return Messages_en.PasswordExpirarionAlertInBannerMsg(iDays);
            default:
                return Messages_en.PasswordExpirarionAlertInBannerMsg(iDays);
        }
    };
    Messages.PasswordExpirarionAlertInBannerMsg_OneDay = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.PasswordExpirarionAlertInBannerMsg_OneDay();
            case Language.en:
                return Messages_en.PasswordExpirarionAlertInBannerMsg_OneDay();
            default:
                return Messages_en.PasswordExpirarionAlertInBannerMsg_OneDay();
        }
    };
    Messages.ChangePasswdDialog_Null_PasswordsMsg = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ChangePasswdDialog_Null_PasswordsMsg();
            case Language.en:
                return Messages_en.ChangePasswdDialog_Null_PasswordsMsg();
            default:
                return Messages_en.ChangePasswdDialog_Null_PasswordsMsg();
        }
    };
    Messages.ChangePasswdDialog_Null_UserMsg = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ChangePasswdDialog_Null_UserMsg();
            case Language.en:
                return Messages_en.ChangePasswdDialog_Null_UserMsg();
            default:
                return Messages_en.ChangePasswdDialog_Null_UserMsg();
        }
    };
    Messages.ChangePasswdDialog_Null_NewPasswordsDifMsg = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ChangePasswdDialog_Null_NewPasswordsDifMsg();
            case Language.en:
                return Messages_en.ChangePasswdDialog_Null_NewPasswordsDifMsg();
            default:
                return Messages_en.ChangePasswdDialog_Null_NewPasswordsDifMsg();
        }
    };
    Messages.ChangePasswdDialog_SuccessMsg = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ChangePasswdDialog_SuccessMsg();
            case Language.en:
                return Messages_en.ChangePasswdDialog_SuccessMsg();
            default:
                return Messages_en.ChangePasswdDialog_SuccessMsg();
        }
    };
    Messages.ImportExcel_MessageBox_Success_Message = function (fileName, totalRows) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ImportExcel_MessageBox_Success_Message(fileName, totalRows);
            case Language.en:
                return Messages_en.ImportExcel_MessageBox_Success_Message(fileName, totalRows);
            default:
                return Messages_en.ImportExcel_MessageBox_Success_Message(fileName, totalRows);
        }
    };
    Messages.ImportExcel_MessageBox_GenericError_Message = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ImportExcel_MessageBox_GenericError_Message();
            case Language.en:
                return Messages_en.ImportExcel_MessageBox_GenericError_Message();
            default:
                return Messages_en.ImportExcel_MessageBox_GenericError_Message();
        }
    };
    Messages.ImportExcel_MessageBox_NoRecordsError_Message = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ImportExcel_MessageBox_NoRecordsError_Message();
            case Language.en:
                return Messages_en.ImportExcel_MessageBox_NoRecordsError_Message();
            default:
                return Messages_en.ImportExcel_MessageBox_NoRecordsError_Message();
        }
    };
    Messages.USER_STATUS_NOT_ACTIVE = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.USER_STATUS_NOT_ACTIVE();
            case Language.en:
                return Messages_en.USER_STATUS_NOT_ACTIVE();
            default:
                return Messages_en.USER_STATUS_NOT_ACTIVE();
        }
    };
    Messages.SUBSCRIBER_NOT_ASSOC_WITH_APP = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.SUBSCRIBER_NOT_ASSOC_WITH_APP();
            case Language.en:
                return Messages_en.SUBSCRIBER_NOT_ASSOC_WITH_APP();
            default:
                return Messages_en.SUBSCRIBER_NOT_ASSOC_WITH_APP();
        }
    };
    Messages.USER_NOT_ASSOC_WITH_APP_ALTHOUGH_SUBSCRPTN_OK = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.USER_NOT_ASSOC_WITH_APP_ALTHOUGH_SUBSCRPTN_OK();
            case Language.en:
                return Messages_en.USER_NOT_ASSOC_WITH_APP_ALTHOUGH_SUBSCRPTN_OK();
            default:
                return Messages_en.USER_NOT_ASSOC_WITH_APP_ALTHOUGH_SUBSCRPTN_OK();
        }
    };
    Messages.USER_NOT_ASSOC_WITH_APP = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.USER_NOT_ASSOC_WITH_APP();
            case Language.en:
                return Messages_en.USER_NOT_ASSOC_WITH_APP();
            default:
                return Messages_en.USER_NOT_ASSOC_WITH_APP();
        }
    };
    Messages.SUBSCRIBER_STATUS_NOT_ACTIVE = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.SUBSCRIBER_STATUS_NOT_ACTIVE();
            case Language.en:
                return Messages_en.SUBSCRIBER_STATUS_NOT_ACTIVE();
            default:
                return Messages_en.SUBSCRIBER_STATUS_NOT_ACTIVE();
        }
    };
    Messages.SUBSCRIBER_MODULEGROUP_NOT_ACTIVE = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.SUBSCRIBER_MODULEGROUP_NOT_ACTIVE();
            case Language.en:
                return Messages_en.SUBSCRIBER_MODULEGROUP_NOT_ACTIVE();
            default:
                return Messages_en.SUBSCRIBER_MODULEGROUP_NOT_ACTIVE();
        }
    };
    Messages.SUBSCRIBER_MODULEGROUP_TEMPRL_MISMATCH = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.SUBSCRIBER_MODULEGROUP_TEMPRL_MISMATCH();
            case Language.en:
                return Messages_en.SUBSCRIBER_MODULEGROUP_TEMPRL_MISMATCH();
            default:
                return Messages_en.SUBSCRIBER_MODULEGROUP_TEMPRL_MISMATCH();
        }
    };
    Messages.UNKNOWN_APP = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.UNKNOWN_APP();
            case Language.en:
                return Messages_en.UNKNOWN_APP();
            default:
                return Messages_en.UNKNOWN_APP();
        }
    };
    Messages.AUTH_ERROR = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.AUTH_ERROR();
            case Language.en:
                return Messages_en.AUTH_ERROR();
            default:
                return Messages_en.AUTH_ERROR();
        }
    };
    Messages.WRONG_PASSWORD = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.WRONG_PASSWORD();
            case Language.en:
                return Messages_en.WRONG_PASSWORD();
            default:
                return Messages_en.WRONG_PASSWORD();
        }
    };
    Messages.ACCOUNT_DOESNOT_EXIST = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ACCOUNT_DOESNOT_EXIST();
            case Language.en:
                return Messages_en.ACCOUNT_DOESNOT_EXIST();
            default:
                return Messages_en.ACCOUNT_DOESNOT_EXIST();
        }
    };
    Messages.USER_LOCKED_BY_ATTEMPTS_LIMIT = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.USER_LOCKED_BY_ATTEMPTS_LIMIT();
            case Language.en:
                return Messages_en.USER_LOCKED_BY_ATTEMPTS_LIMIT();
            default:
                return Messages_en.USER_LOCKED_BY_ATTEMPTS_LIMIT();
        }
    };
    Messages.USER_PASSWORD_HAS_EXPIRED = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.USER_PASSWORD_HAS_EXPIRED();
            case Language.en:
                return Messages_en.USER_PASSWORD_HAS_EXPIRED();
            default:
                return Messages_en.USER_PASSWORD_HAS_EXPIRED();
        }
    };
    Messages.INVALID_SUBSCRIBER = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.INVALID_SUBSCRIBER();
            case Language.en:
                return Messages_en.INVALID_SUBSCRIBER();
            default:
                return Messages_en.INVALID_SUBSCRIBER();
        }
    };
    Messages.WRONG_LOGIN_SUBSCRIBER = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.WRONG_LOGIN_SUBSCRIBER();
            case Language.en:
                return Messages_en.WRONG_LOGIN_SUBSCRIBER();
            default:
                return Messages_en.WRONG_LOGIN_SUBSCRIBER();
        }
    };
    Messages.GetAllIds_TotalItemsMsg = function (iTotalItems) {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.GetAllIds_TotalItemsMsg(iTotalItems);
            case Language.en:
                return Messages_en.GetAllIds_TotalItemsMsg(iTotalItems);
            default:
                return Messages_en.GetAllIds_TotalItemsMsg(iTotalItems);
        }
    };
    Messages.clfr_fite_id_fk = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.clfr_fite_id_fk();
            case Language.en:
                return Messages_en.clfr_fite_id_fk();
            default:
                return Messages_en.clfr_fite_id_fk();
        }
    };
    Messages.pcla_clas_id_fk = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.pcla_clas_id_fk();
            case Language.en:
                return Messages_en.pcla_clas_id_fk();
            default:
                return Messages_en.pcla_clas_id_fk();
        }
    };
    Messages.dafi_clas_id_fk = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.dafi_clas_id_fk();
            case Language.en:
                return Messages_en.dafi_clas_id_fk();
            default:
                return Messages_en.dafi_clas_id_fk();
        }
    };
    Messages.pade_dema_id_fk = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.pade_dema_id_fk();
            case Language.en:
                return Messages_en.pade_dema_id_fk();
            default:
                return Messages_en.pade_dema_id_fk();
        }
    };
    Messages.sucdsuca_id_cult_id__ui = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.sucdsuca_id_cult_id__ui();
            case Language.en:
                return Messages_en.sucdsuca_id_cult_id__ui();
            default:
                return Messages_en.sucdsuca_id_cult_id__ui();
        }
    };
    Messages.cult_name_un = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.cult_name_un();
            case Language.en:
                return Messages_en.cult_name_un();
            default:
                return Messages_en.cult_name_un();
        }
    };
    Messages.cult_code_un = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.cult_code_un();
            case Language.en:
                return Messages_en.cult_code_un();
            default:
                return Messages_en.cult_code_un();
        }
    };
    Messages.file_dir_path_un = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.file_dir_path_un();
            case Language.en:
                return Messages_en.file_dir_path_un();
            default:
                return Messages_en.file_dir_path_un();
        }
    };
    Messages.super_class_code_un = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.super_class_code_un();
            case Language.en:
                return Messages_en.super_class_code_un();
            default:
                return Messages_en.super_class_code_un();
        }
    };
    Messages.super_class_name_un = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.super_class_name_un();
            case Language.en:
                return Messages_en.super_class_name_un();
            default:
                return Messages_en.super_class_name_un();
        }
    };
    Messages.prco_fite_un = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.prco_fite_un();
            case Language.en:
                return Messages_en.prco_fite_un();
            default:
                return Messages_en.prco_fite_un();
        }
    };
    Messages.fite_clna_un = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.fite_clna_un();
            case Language.en:
                return Messages_en.fite_clna_un();
            default:
                return Messages_en.fite_clna_un();
        }
    };
    Messages.file_template_name_un = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.file_template_name_un();
            case Language.en:
                return Messages_en.file_template_name_un();
            default:
                return Messages_en.file_template_name_un();
        }
    };
    Messages.activity_code_un = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.activity_code_un();
            case Language.en:
                return Messages_en.activity_code_un();
            default:
                return Messages_en.activity_code_un();
        }
    };
    Messages.activity_name_un = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.activity_name_un();
            case Language.en:
                return Messages_en.activity_name_un();
            default:
                return Messages_en.activity_name_un();
        }
    };
    Messages.cover_type_code_un = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.cover_type_code_un();
            case Language.en:
                return Messages_en.cover_type_code_un();
            default:
                return Messages_en.cover_type_code_un();
        }
    };
    Messages.cover_type_name_un = function () {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.cover_type_name_un();
            case Language.en:
                return Messages_en.cover_type_name_un();
            default:
                return Messages_en.cover_type_name_un();
        }
    };
    Messages.dynamicMessage = function (msgKey) {
        var items = msgKey.split('%%');
        var results = _.map(items, function (s) {
            try {
                var methodName = "";
                var _paramsStartIndex = s.indexOf("(");
                var _paramsEndIndex = s.indexOf(")");
                if (_paramsStartIndex > 0 && _paramsEndIndex > _paramsStartIndex) {
                    methodName =
                        s.substring(0, _paramsStartIndex).replace(/\./g, "_") + s.substring(_paramsStartIndex);
                }
                else {
                    methodName = s.replace(/\./g, "_");
                }
                var m = eval("Messages." + methodName);
                if (typeof m === "function") {
                    return eval("Messages." + methodName + "()");
                }
                if (isVoid(m)) {
                    return s;
                }
                return m;
            }
            catch (ex) {
                return s;
            }
        });
        return results.join('<br>');
    };
    Messages.getActiveLangString = function (key, asHtml) {
        if (asHtml === void 0) { asHtml = false; }
        if (isVoid(key))
            return key;
        var ret;
        switch (Messages.activeLang) {
            case Language.el:
                ret = Messages_el.strings[key];
                break;
            case Language.en:
                ret = Messages_en.strings[key];
                break;
            default:
                ret = Messages_en.strings[key];
        }
        if (isVoid(ret))
            ret = key;
        return asHtml ? ret.replace(new RegExp("\n", 'g'), "<br/>").replace(new RegExp(" ", 'g'), "&#160;") : ret;
    };
    return Messages;
})();
Messages_el.strings = {};
Messages_en.strings = {
    "AppVersionLabel": "Application's Last Update",
    "VersionLabel": "Last Update",
    "Login": "Login",
    "User": "User",
    "Password": "Password",
    "Change Password": "Change Password",
    "Logout": "Logout",
    "Application Menu": "Application Menu",
    "Logged-in User": "Logged-in User",
    "Login Error": "Login Error",
    "Save": "Save",
    "Delete": "Delete",
    "New Record": "New Record",
    "Records": "Records",
    "Back": "Back",
    "Show Filters": "Show Filters",
    "Multiple Insert": "Multiple Insert",
    "Page Info": "Page Info",
    "Records:": "Records:",
    "BreadCrumb_Home_Label": "Home",
    "ExitAppQuestion": "Are you sure you want to Exit?",
    "ExitAppDialogTitle": "Exit",
    "Search": "Search",
    "Clear": "Clear",
    "Search Criteria": "Search Criteria",
    "Audit Info of Record": "Audit Info of Record",
    "Insert User": "Insert User",
    "Insert Timestamp": "Insert Timestamp",
    "Last Update User": "Last Update User",
    "Last Update Timestamp": "Last Update Timestamp",
    "Row\nNum": "Row\nNum",
    "Error\nMessage": "Error\nMessage",
    "The Geometries": "The Geometries",
    "WaitDialog_Title": "Loading",
    "WaitDialog_Message": "Please Wait...",
    "LovButtons_Select": "Select (F12)",
    "LovButtons_Clear": "Clear",
    "LovButtons_New": "New Record",
    "ImportExcel_MessageBox_Info_Title": "Import Excel File Info",
    "ImportExcel_MessageBox_Info_Message": "Click 'Download' to get the excel file with the 'Template' and the 'Specifications', which the file must meet in order to complete the import successfully.",
    "ImportExcel_MessageBox_Info_ButtonLabel": "Download",
    "ImportExcel_MessageBox_Error_Message": "The import of the file failed because some records had errors.",
    "ImportExcel_MessageBox_Error_ButtonLabel": "Show Errors",
    "ImportExvel_ResultsDlg_Title": "Excel File Import Results",
    "ImportExvel_ResultsDlg_File": "File",
    "ImportExvel_ResultsDlg_File_NumRecords": "Number of\nFile lines",
    "ImportExvel_ResultsDlg_File_NumErrorRecords": "Number of lines\nwith errors",
    "ImportExvel_ResultsDlg_GridTitle": "Errors",
    "MessageBox_Success_Title": "Success",
    "MessageBox_Attention_Title": "Attention",
    "MessageBox_Error_Title": "Error",
    "MessageBox_MultipleInsertError_Title": "Multiple Insertion Error",
    "ChangePasswdDialog_Title": "Password Change",
    "ChangePasswdDialog_user_label": "User",
    "ChangePasswdDialog_oldpass_label": "Old password",
    "ChangePasswdDialog_newpass1_label": "New password",
    "ChangePasswdDialog_newpass2_label": "New password (repeat)",
    "ChangePasswdDialog_set_button": "Set new password",
    "ChangePasswdDialog_back_button": "Back",
    "ChangePasswdDialog_ErrorLabel": "Error",
    "HelpDialog_Title": "Help",
    "HelpDialog_Heading": "Handling",
    "HelpDialog_F1": "Show Help",
    "HelpDialog_Ctrl_s": "Save",
    "HelpDialog_F9": "Create New Record",
    "HelpDialog_F10": "Focus on next table",
    "HelpDialog_Shift_F10": "Focus on previous table",
    "HelpDialog_F12": "Open/close list of values",
    "HelpDialog_Enter_Tab": "Focus on next field",
    "HelpDialog_Shift_tab": "Focus on previous field",
    "HelpDialog_Esc": "Back/Close dialog",
    "HelpDialog_Double_click": "Open calendar on date fields",
    "HelpDialog_back_button": "Back",
    "MessageBox_Button_Yes": "Yes",
    "MessageBox_Button_No": "No",
    "MessageBox_Button_Cancel": "Cancel",
    "MessageBox_Blob_FileType_Title": "Unacceptable file type!",
    "MessageBox_Blob_FileSize_Title": "Unacceptable file size!",
    "DropDownList_ChooseOne": "Choose one",
    "ExportToExcelButtonHint": "Export to Excel file"
};
//# sourceMappingURL=messages.js.map