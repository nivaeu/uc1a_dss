//6AFB471B601577C8185F37D811814787
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/AgrisnapUsersProducersBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var AgrisnapUsersProducers;
    (function (AgrisnapUsersProducers) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(AgrisnapUsersProducers.PageModelBase);
        AgrisnapUsersProducers.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(AgrisnapUsersProducers.PageControllerBase);
        AgrisnapUsersProducers.PageController = PageController;
        g_controllers['AgrisnapUsersProducers.PageController'] = Controllers.AgrisnapUsersProducers.PageController;
        var ModelGrpAgrisnapUsersProducers = (function (_super) {
            __extends(ModelGrpAgrisnapUsersProducers, _super);
            function ModelGrpAgrisnapUsersProducers() {
                _super.apply(this, arguments);
            }
            return ModelGrpAgrisnapUsersProducers;
        })(AgrisnapUsersProducers.ModelGrpAgrisnapUsersProducersBase);
        AgrisnapUsersProducers.ModelGrpAgrisnapUsersProducers = ModelGrpAgrisnapUsersProducers;
        var ControllerGrpAgrisnapUsersProducers = (function (_super) {
            __extends(ControllerGrpAgrisnapUsersProducers, _super);
            function ControllerGrpAgrisnapUsersProducers($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpAgrisnapUsersProducers($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpAgrisnapUsersProducers;
        })(AgrisnapUsersProducers.ControllerGrpAgrisnapUsersProducersBase);
        AgrisnapUsersProducers.ControllerGrpAgrisnapUsersProducers = ControllerGrpAgrisnapUsersProducers;
        g_controllers['AgrisnapUsersProducers.ControllerGrpAgrisnapUsersProducers'] = Controllers.AgrisnapUsersProducers.ControllerGrpAgrisnapUsersProducers;
    })(AgrisnapUsersProducers = Controllers.AgrisnapUsersProducers || (Controllers.AgrisnapUsersProducers = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=AgrisnapUsersProducers.js.map