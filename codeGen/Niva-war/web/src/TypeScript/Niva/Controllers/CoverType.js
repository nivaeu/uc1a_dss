//055EEC7E6C19214787645A24A71C73EF
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/CoverTypeBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var CoverType;
    (function (CoverType) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(CoverType.PageModelBase);
        CoverType.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            PageController.prototype.successUpdate = function (fileName, jsonResponse) {
                var self = this;
                self.model.modelGrpCoverType.controller.updateUI();
            };
            return PageController;
        })(CoverType.PageControllerBase);
        CoverType.PageController = PageController;
        g_controllers['CoverType.PageController'] = Controllers.CoverType.PageController;
        var ModelGrpCoverType = (function (_super) {
            __extends(ModelGrpCoverType, _super);
            function ModelGrpCoverType() {
                _super.apply(this, arguments);
            }
            return ModelGrpCoverType;
        })(CoverType.ModelGrpCoverTypeBase);
        CoverType.ModelGrpCoverType = ModelGrpCoverType;
        var ControllerGrpCoverType = (function (_super) {
            __extends(ControllerGrpCoverType, _super);
            function ControllerGrpCoverType($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpCoverType($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpCoverType;
        })(CoverType.ControllerGrpCoverTypeBase);
        CoverType.ControllerGrpCoverType = ControllerGrpCoverType;
        g_controllers['CoverType.ControllerGrpCoverType'] = Controllers.CoverType.ControllerGrpCoverType;
    })(CoverType = Controllers.CoverType || (Controllers.CoverType = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=CoverType.js.map