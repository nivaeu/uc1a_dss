//194AB1DB5B87D7A7447D037C9A97C397
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelGPBase.ts" />
/// <reference path="../Services/MainService.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ParcelGP;
    (function (ParcelGP) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(ParcelGP.PageModelBase);
        ParcelGP.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            PageController.prototype.getPageChanges = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var pageChanges = [];
                if (bForDelete) {
                }
                else {
                    pageChanges = pageChanges.concat(self.model.modelGrpGpDecision.controller.getChangesToCommit());
                }
                var hasParcelClas = pageChanges.some(function (change) { return change.entityName === "ParcelClas"; });
                return pageChanges;
            };
            PageController.prototype.onSuccesfullSaveFromButton = function (response) {
                var self = this;
                _super.prototype.onSuccesfullSaveFromButton.call(this, response);
                NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
                //call web service
                var paramData = new Services.MainService_updateIntegratedDecisionsNIssues_req();
                paramData.pclaId = self.$scope.pageModel.modelGrpParcelClas.pclaId;
                console.log("Calling Update Integrated Decisions Service...." + self.$scope.pageModel.modelGrpParcelClas.pclaId);
                Services.MainService.updateIntegratedDecisionsNIssues(self.$scope.pageModel.controller, paramData, function (respDataList) {
                    //self.updateUI();
                    NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
                    NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, "Update was Completed Succesfully.");
                    respDataList.forEach(function (item) { return NpTypes.AlertMessage.addWarning(self.$scope.pageModel, item); });
                });
            };
            return PageController;
        })(ParcelGP.PageControllerBase);
        ParcelGP.PageController = PageController;
        g_controllers['ParcelGP.PageController'] = Controllers.ParcelGP.PageController;
        var ModelGrpParcelClas = (function (_super) {
            __extends(ModelGrpParcelClas, _super);
            function ModelGrpParcelClas() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcelClas;
        })(ParcelGP.ModelGrpParcelClasBase);
        ParcelGP.ModelGrpParcelClas = ModelGrpParcelClas;
        var ControllerGrpParcelClas = (function (_super) {
            __extends(ControllerGrpParcelClas, _super);
            function ControllerGrpParcelClas($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcelClas($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpParcelClas;
        })(ParcelGP.ControllerGrpParcelClasBase);
        ParcelGP.ControllerGrpParcelClas = ControllerGrpParcelClas;
        g_controllers['ParcelGP.ControllerGrpParcelClas'] = Controllers.ParcelGP.ControllerGrpParcelClas;
        var ModelGrpParcelsIssues = (function (_super) {
            __extends(ModelGrpParcelsIssues, _super);
            function ModelGrpParcelsIssues() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcelsIssues;
        })(ParcelGP.ModelGrpParcelsIssuesBase);
        ParcelGP.ModelGrpParcelsIssues = ModelGrpParcelsIssues;
        var ControllerGrpParcelsIssues = (function (_super) {
            __extends(ControllerGrpParcelsIssues, _super);
            function ControllerGrpParcelsIssues($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcelsIssues($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpParcelsIssues;
        })(ParcelGP.ControllerGrpParcelsIssuesBase);
        ParcelGP.ControllerGrpParcelsIssues = ControllerGrpParcelsIssues;
        g_controllers['ParcelGP.ControllerGrpParcelsIssues'] = Controllers.ParcelGP.ControllerGrpParcelsIssues;
        var ModelGrpGpDecision = (function (_super) {
            __extends(ModelGrpGpDecision, _super);
            function ModelGrpGpDecision() {
                _super.apply(this, arguments);
            }
            return ModelGrpGpDecision;
        })(ParcelGP.ModelGrpGpDecisionBase);
        ParcelGP.ModelGrpGpDecision = ModelGrpGpDecision;
        var ControllerGrpGpDecision = (function (_super) {
            __extends(ControllerGrpGpDecision, _super);
            function ControllerGrpGpDecision($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpGpDecision($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            ControllerGrpGpDecision.prototype.isCheckMade = function (gpDecision) {
                var self = this;
                return !gpDecision.isNew();
            };
            return ControllerGrpGpDecision;
        })(ParcelGP.ControllerGrpGpDecisionBase);
        ParcelGP.ControllerGrpGpDecision = ControllerGrpGpDecision;
        g_controllers['ParcelGP.ControllerGrpGpDecision'] = Controllers.ParcelGP.ControllerGrpGpDecision;
        var ModelGrpGpRequestsContexts = (function (_super) {
            __extends(ModelGrpGpRequestsContexts, _super);
            function ModelGrpGpRequestsContexts() {
                _super.apply(this, arguments);
            }
            return ModelGrpGpRequestsContexts;
        })(ParcelGP.ModelGrpGpRequestsContextsBase);
        ParcelGP.ModelGrpGpRequestsContexts = ModelGrpGpRequestsContexts;
        var ControllerGrpGpRequestsContexts = (function (_super) {
            __extends(ControllerGrpGpRequestsContexts, _super);
            function ControllerGrpGpRequestsContexts($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpGpRequestsContexts($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpGpRequestsContexts;
        })(ParcelGP.ControllerGrpGpRequestsContextsBase);
        ParcelGP.ControllerGrpGpRequestsContexts = ControllerGrpGpRequestsContexts;
        g_controllers['ParcelGP.ControllerGrpGpRequestsContexts'] = Controllers.ParcelGP.ControllerGrpGpRequestsContexts;
        var ModelGrpGpUpload = (function (_super) {
            __extends(ModelGrpGpUpload, _super);
            function ModelGrpGpUpload() {
                _super.apply(this, arguments);
            }
            return ModelGrpGpUpload;
        })(ParcelGP.ModelGrpGpUploadBase);
        ParcelGP.ModelGrpGpUpload = ModelGrpGpUpload;
        var ControllerGrpGpUpload = (function (_super) {
            __extends(ControllerGrpGpUpload, _super);
            function ControllerGrpGpUpload($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpGpUpload($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            ControllerGrpGpUpload.prototype.viewGeotaggegPhoto = function (gpUpload) {
                var self = this;
                var srcUrlEncoded = "/Niva/rest/GpUpload/getImage?id=" + encodeURIComponent(gpUpload.gpUploadsId);
                document.getElementById('vgp').innerHTML = "<img id=\"vgpgp\" src=\"" + srcUrlEncoded + "\" />";
            };
            return ControllerGrpGpUpload;
        })(ParcelGP.ControllerGrpGpUploadBase);
        ParcelGP.ControllerGrpGpUpload = ControllerGrpGpUpload;
        g_controllers['ParcelGP.ControllerGrpGpUpload'] = Controllers.ParcelGP.ControllerGrpGpUpload;
    })(ParcelGP = Controllers.ParcelGP || (Controllers.ParcelGP = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ParcelGP.js.map