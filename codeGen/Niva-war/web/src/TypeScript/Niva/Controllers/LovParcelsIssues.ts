//D671BB7F1359250D9349E9ACD5BA1455
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovParcelsIssuesBase.ts" />

module Controllers {

    export class ModelLovParcelsIssues extends ModelLovParcelsIssuesBase {
    }

    export interface IScopeLovParcelsIssues extends IScopeLovParcelsIssuesBase {
    }

    export class ControllerLovParcelsIssues extends ControllerLovParcelsIssuesBase {
        
        constructor(
            public $scope: IScopeLovParcelsIssues,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelLovParcelsIssues($scope));
        }
    }

    g_controllers['ControllerLovParcelsIssues'] = Controllers.ControllerLovParcelsIssues;


    
}
