//2955DD99667ED098043A73C8E3BD5F32
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/EcGroupSearchBase.ts" />

module Controllers {

    export class ModelEcGroupSearch extends ModelEcGroupSearchBase {
    }

    export interface IScopeEcGroupSearch extends IScopeEcGroupSearchBase {
    }

    export class ControllerEcGroupSearch extends ControllerEcGroupSearchBase {
        
        constructor(
            public $scope: IScopeEcGroupSearch,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelEcGroupSearch($scope));
        }
    }

    g_controllers['ControllerEcGroupSearch'] = Controllers.ControllerEcGroupSearch;


    
}
