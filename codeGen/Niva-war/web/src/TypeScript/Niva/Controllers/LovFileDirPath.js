//C70C11849F0FFA31096E62AF6EA1D05D
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovFileDirPathBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovFileDirPath = (function (_super) {
        __extends(ModelLovFileDirPath, _super);
        function ModelLovFileDirPath() {
            _super.apply(this, arguments);
        }
        return ModelLovFileDirPath;
    })(Controllers.ModelLovFileDirPathBase);
    Controllers.ModelLovFileDirPath = ModelLovFileDirPath;
    var ControllerLovFileDirPath = (function (_super) {
        __extends(ControllerLovFileDirPath, _super);
        function ControllerLovFileDirPath($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovFileDirPath($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovFileDirPath;
    })(Controllers.ControllerLovFileDirPathBase);
    Controllers.ControllerLovFileDirPath = ControllerLovFileDirPath;
    g_controllers['ControllerLovFileDirPath'] = Controllers.ControllerLovFileDirPath;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovFileDirPath.js.map