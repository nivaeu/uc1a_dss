//1A235F740A2B9556FD17BC2575B74485
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelDecisionBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ParcelDecision;
    (function (ParcelDecision) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(ParcelDecision.PageModelBase);
        ParcelDecision.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(ParcelDecision.PageControllerBase);
        ParcelDecision.PageController = PageController;
        g_controllers['ParcelDecision.PageController'] = Controllers.ParcelDecision.PageController;
        var ModelGrpParcelDecision = (function (_super) {
            __extends(ModelGrpParcelDecision, _super);
            function ModelGrpParcelDecision() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcelDecision;
        })(ParcelDecision.ModelGrpParcelDecisionBase);
        ParcelDecision.ModelGrpParcelDecision = ModelGrpParcelDecision;
        var ControllerGrpParcelDecision = (function (_super) {
            __extends(ControllerGrpParcelDecision, _super);
            function ControllerGrpParcelDecision($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcelDecision($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpParcelDecision;
        })(ParcelDecision.ControllerGrpParcelDecisionBase);
        ParcelDecision.ControllerGrpParcelDecision = ControllerGrpParcelDecision;
        g_controllers['ParcelDecision.ControllerGrpParcelDecision'] = Controllers.ParcelDecision.ControllerGrpParcelDecision;
    })(ParcelDecision = Controllers.ParcelDecision || (Controllers.ParcelDecision = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ParcelDecision.js.map