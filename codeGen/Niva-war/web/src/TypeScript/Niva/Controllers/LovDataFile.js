//317740BAAC386A5693AC543234C21651
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovDataFileBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovDataFile = (function (_super) {
        __extends(ModelLovDataFile, _super);
        function ModelLovDataFile() {
            _super.apply(this, arguments);
        }
        return ModelLovDataFile;
    })(Controllers.ModelLovDataFileBase);
    Controllers.ModelLovDataFile = ModelLovDataFile;
    var ControllerLovDataFile = (function (_super) {
        __extends(ControllerLovDataFile, _super);
        function ControllerLovDataFile($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovDataFile($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovDataFile;
    })(Controllers.ControllerLovDataFileBase);
    Controllers.ControllerLovDataFile = ControllerLovDataFile;
    g_controllers['ControllerLovDataFile'] = Controllers.ControllerLovDataFile;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovDataFile.js.map