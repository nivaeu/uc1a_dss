//9E281A60648BD200853B0B40F977A1CB
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovClassificationBase.ts" />

module Controllers {

    export class ModelLovClassification extends ModelLovClassificationBase {
    }

    export interface IScopeLovClassification extends IScopeLovClassificationBase {
    }

    export class ControllerLovClassification extends ControllerLovClassificationBase {
        
        constructor(
            public $scope: IScopeLovClassification,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelLovClassification($scope));
        }
    }

    g_controllers['ControllerLovClassification'] = Controllers.ControllerLovClassification;


    
}
