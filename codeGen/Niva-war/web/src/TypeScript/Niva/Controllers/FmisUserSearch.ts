//CF9C8EC0C9B22DB3740A329D8C5D37E2
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/FmisUserSearchBase.ts" />

module Controllers {

    export class ModelFmisUserSearch extends ModelFmisUserSearchBase {
    }

    export interface IScopeFmisUserSearch extends IScopeFmisUserSearchBase {
    }

    export class ControllerFmisUserSearch extends ControllerFmisUserSearchBase {
        
        constructor(
            public $scope: IScopeFmisUserSearch,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelFmisUserSearch($scope));
        }
    }

    g_controllers['ControllerFmisUserSearch'] = Controllers.ControllerFmisUserSearch;


    
}
