//6CE481DC93040BC7B0A5AF89DB097DE6
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ProducersSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelProducersSearch = (function (_super) {
        __extends(ModelProducersSearch, _super);
        function ModelProducersSearch() {
            _super.apply(this, arguments);
        }
        return ModelProducersSearch;
    })(Controllers.ModelProducersSearchBase);
    Controllers.ModelProducersSearch = ModelProducersSearch;
    var ControllerProducersSearch = (function (_super) {
        __extends(ControllerProducersSearch, _super);
        function ControllerProducersSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelProducersSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerProducersSearch;
    })(Controllers.ControllerProducersSearchBase);
    Controllers.ControllerProducersSearch = ControllerProducersSearch;
    g_controllers['ControllerProducersSearch'] = Controllers.ControllerProducersSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ProducersSearch.js.map