//A69668F14683EA59BE5264D36FF27952
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/AgrisnapUsersSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelAgrisnapUsersSearch = (function (_super) {
        __extends(ModelAgrisnapUsersSearch, _super);
        function ModelAgrisnapUsersSearch() {
            _super.apply(this, arguments);
        }
        return ModelAgrisnapUsersSearch;
    })(Controllers.ModelAgrisnapUsersSearchBase);
    Controllers.ModelAgrisnapUsersSearch = ModelAgrisnapUsersSearch;
    var ControllerAgrisnapUsersSearch = (function (_super) {
        __extends(ControllerAgrisnapUsersSearch, _super);
        function ControllerAgrisnapUsersSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelAgrisnapUsersSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerAgrisnapUsersSearch;
    })(Controllers.ControllerAgrisnapUsersSearchBase);
    Controllers.ControllerAgrisnapUsersSearch = ControllerAgrisnapUsersSearch;
    g_controllers['ControllerAgrisnapUsersSearch'] = Controllers.ControllerAgrisnapUsersSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=AgrisnapUsersSearch.js.map