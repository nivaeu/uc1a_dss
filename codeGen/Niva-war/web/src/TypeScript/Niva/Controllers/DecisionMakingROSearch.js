//42F102C048CB4C4A9DCA7337A0EAD9AD
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/DecisionMakingROSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelDecisionMakingROSearch = (function (_super) {
        __extends(ModelDecisionMakingROSearch, _super);
        function ModelDecisionMakingROSearch() {
            _super.apply(this, arguments);
        }
        return ModelDecisionMakingROSearch;
    })(Controllers.ModelDecisionMakingROSearchBase);
    Controllers.ModelDecisionMakingROSearch = ModelDecisionMakingROSearch;
    var ControllerDecisionMakingROSearch = (function (_super) {
        __extends(ControllerDecisionMakingROSearch, _super);
        function ControllerDecisionMakingROSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelDecisionMakingROSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerDecisionMakingROSearch;
    })(Controllers.ControllerDecisionMakingROSearchBase);
    Controllers.ControllerDecisionMakingROSearch = ControllerDecisionMakingROSearch;
    g_controllers['ControllerDecisionMakingROSearch'] = Controllers.ControllerDecisionMakingROSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=DecisionMakingROSearch.js.map