//78DDFE2B9900FB37659CC313596AB963
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ClassifierBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var Classifier;
    (function (Classifier) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(Classifier.PageModelBase);
        Classifier.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(Classifier.PageControllerBase);
        Classifier.PageController = PageController;
        g_controllers['Classifier.PageController'] = Controllers.Classifier.PageController;
        var ModelGrpClassifier = (function (_super) {
            __extends(ModelGrpClassifier, _super);
            function ModelGrpClassifier() {
                _super.apply(this, arguments);
            }
            return ModelGrpClassifier;
        })(Classifier.ModelGrpClassifierBase);
        Classifier.ModelGrpClassifier = ModelGrpClassifier;
        var ControllerGrpClassifier = (function (_super) {
            __extends(ControllerGrpClassifier, _super);
            function ControllerGrpClassifier($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpClassifier($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpClassifier;
        })(Classifier.ControllerGrpClassifierBase);
        Classifier.ControllerGrpClassifier = ControllerGrpClassifier;
        g_controllers['Classifier.ControllerGrpClassifier'] = Controllers.Classifier.ControllerGrpClassifier;
    })(Classifier = Controllers.Classifier || (Controllers.Classifier = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=Classifier.js.map