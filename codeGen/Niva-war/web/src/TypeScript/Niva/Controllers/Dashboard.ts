//356FC6613BDE308955193C0094DFC5AE
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/DashboardBase.ts" />

module Controllers.Dashboard {

    export class PageModel extends PageModelBase {
    }

    export interface IPageScope extends IPageScopeBase {
    }

    export class PageController extends PageControllerBase {
        
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new PageModel($scope));
        }
    }

    g_controllers['Dashboard.PageController'] = Controllers.Dashboard.PageController;



    export class ModelGrpParcelClas extends ModelGrpParcelClasBase {
    }


    export interface IScopeGrpParcelClas extends IScopeGrpParcelClasBase {
    }

    export class ControllerGrpParcelClas extends ControllerGrpParcelClasBase {
        constructor(
            public $scope: IScopeGrpParcelClas,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpParcelClas($scope) );
        }

        public createGeom4326Map(defaultOptions: NpGeo.NpMapConstructionOptions) {
           
            var self: ControllerGrpParcelClasBase = this;
            // self.$scope.modelGrpParcelDecision = self.model;
            defaultOptions.layers = [new NpGeoLayers.OpenStreetTileLayer(true)];
            defaultOptions.zoomOnEntityChange = true;
            defaultOptions.showCoordinates = true;
            defaultOptions.showRuler = true;
            defaultOptions.centerPoint = [49.843056, 9.901944];
            defaultOptions.zoomLevel = 10;
            defaultOptions.fillColor = "rgba(50, 0,255, 0.1)";
            defaultOptions.borderColor = "blue";
            //  defaultOptions.labelColor?: string;
            defaultOptions.penWidth = 4;
            defaultOptions.opacity = 0.5;
            defaultOptions.layerSelectorWidthInPx = 170;
            defaultOptions.coordinateSystem = "EPSG:4326";
            // mapId?: string;
            defaultOptions.showJumpButton = true;
            defaultOptions.showEditButtons = false;
            defaultOptions.showMeasureButton = false;
            defaultOptions.showLegendButton = false;




            // defaultOptions.getEntityLabel?: (currentEntity: any) => string;

            // defaultOptions.getFeatureLabel?: (feature: ol.Feature, lightweight: boolean) => string; // it should be property of vectory layer
            //  defaultOptions.topologyType?: number; //0|undfined = none, 1=client topology, 2:serverside topology
            //topologyFunc?: (feature: ol.geom.Geometry, resp : ) => void


            defaultOptions.fullScreenOnlyMap = false;//true or false. If true only mapdiv shall be used for full screen control. If false or ommited use map+datagrid

           
            this.geom4326Map = new NpGeo.NpMap(defaultOptions);
        }


    }
    g_controllers['Dashboard.ControllerGrpParcelClas'] = Controllers.Dashboard.ControllerGrpParcelClas;

    export class ModelGrpParcelDecision extends ModelGrpParcelDecisionBase {
    }


    export interface IScopeGrpParcelDecision extends IScopeGrpParcelDecisionBase {
    }

    export class ControllerGrpParcelDecision extends ControllerGrpParcelDecisionBase {
        constructor(
            public $scope: IScopeGrpParcelDecision,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpParcelDecision($scope) );
        }
    }
    g_controllers['Dashboard.ControllerGrpParcelDecision'] = Controllers.Dashboard.ControllerGrpParcelDecision;

    export class ModelGrpParcelsIssues extends ModelGrpParcelsIssuesBase {
    }


    export interface IScopeGrpParcelsIssues extends IScopeGrpParcelsIssuesBase {
    }

    export class ControllerGrpParcelsIssues extends ControllerGrpParcelsIssuesBase {
        constructor(
            public $scope: IScopeGrpParcelsIssues,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpParcelsIssues($scope) );
        }
    }
    g_controllers['Dashboard.ControllerGrpParcelsIssues'] = Controllers.Dashboard.ControllerGrpParcelsIssues;

    export class ModelGrpGpDecision extends ModelGrpGpDecisionBase {
    }


    export interface IScopeGrpGpDecision extends IScopeGrpGpDecisionBase {
    }

    export class ControllerGrpGpDecision extends ControllerGrpGpDecisionBase {
        constructor(
            public $scope: IScopeGrpGpDecision,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpGpDecision($scope) );
        }
    }
    g_controllers['Dashboard.ControllerGrpGpDecision'] = Controllers.Dashboard.ControllerGrpGpDecision;

    export class ModelGrpFmisDecision extends ModelGrpFmisDecisionBase {
    }


    export interface IScopeGrpFmisDecision extends IScopeGrpFmisDecisionBase {
    }

    export class ControllerGrpFmisDecision extends ControllerGrpFmisDecisionBase {
        constructor(
            public $scope: IScopeGrpFmisDecision,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpFmisDecision($scope) );
        }
    }
    g_controllers['Dashboard.ControllerGrpFmisDecision'] = Controllers.Dashboard.ControllerGrpFmisDecision;

    export class ModelGrpIntegrateddecision extends ModelGrpIntegrateddecisionBase {
    }


    export interface IScopeGrpIntegrateddecision extends IScopeGrpIntegrateddecisionBase {
    }

    export class ControllerGrpIntegrateddecision extends ControllerGrpIntegrateddecisionBase {
        constructor(
            public $scope: IScopeGrpIntegrateddecision,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpIntegrateddecision($scope) );
        }
    }
    g_controllers['Dashboard.ControllerGrpIntegrateddecision'] = Controllers.Dashboard.ControllerGrpIntegrateddecision;
    
}
