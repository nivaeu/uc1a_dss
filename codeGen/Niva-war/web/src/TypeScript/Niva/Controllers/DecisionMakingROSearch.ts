//42F102C048CB4C4A9DCA7337A0EAD9AD
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/DecisionMakingROSearchBase.ts" />

module Controllers {

    export class ModelDecisionMakingROSearch extends ModelDecisionMakingROSearchBase {
    }

    export interface IScopeDecisionMakingROSearch extends IScopeDecisionMakingROSearchBase {
    }

    export class ControllerDecisionMakingROSearch extends ControllerDecisionMakingROSearchBase {
        
        constructor(
            public $scope: IScopeDecisionMakingROSearch,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelDecisionMakingROSearch($scope));
        }
    }

    g_controllers['ControllerDecisionMakingROSearch'] = Controllers.ControllerDecisionMakingROSearch;


    
}
