//85A7A104974C01CD5EA26826A63D9F99
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelGPSearchBase.ts" />

module Controllers {

    export class ModelParcelGPSearch extends ModelParcelGPSearchBase {
    }

    export interface IScopeParcelGPSearch extends IScopeParcelGPSearchBase {
    }

    export class ControllerParcelGPSearch extends ControllerParcelGPSearchBase {
        
        constructor(
            public $scope: IScopeParcelGPSearch,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelParcelGPSearch($scope));
        }
    }

    g_controllers['ControllerParcelGPSearch'] = Controllers.ControllerParcelGPSearch;


    
}
