//1C8AEC07CD74F688B6D3E67382D8224D
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/PredefColBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var PredefCol;
    (function (PredefCol) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(PredefCol.PageModelBase);
        PredefCol.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(PredefCol.PageControllerBase);
        PredefCol.PageController = PageController;
        g_controllers['PredefCol.PageController'] = Controllers.PredefCol.PageController;
        var ModelGrpPredefCol = (function (_super) {
            __extends(ModelGrpPredefCol, _super);
            function ModelGrpPredefCol() {
                _super.apply(this, arguments);
            }
            return ModelGrpPredefCol;
        })(PredefCol.ModelGrpPredefColBase);
        PredefCol.ModelGrpPredefCol = ModelGrpPredefCol;
        var ControllerGrpPredefCol = (function (_super) {
            __extends(ControllerGrpPredefCol, _super);
            function ControllerGrpPredefCol($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpPredefCol($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpPredefCol;
        })(PredefCol.ControllerGrpPredefColBase);
        PredefCol.ControllerGrpPredefCol = ControllerGrpPredefCol;
        g_controllers['PredefCol.ControllerGrpPredefCol'] = Controllers.PredefCol.ControllerGrpPredefCol;
    })(PredefCol = Controllers.PredefCol || (Controllers.PredefCol = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=PredefCol.js.map