//9FF3987C0D6B69A3402563B74BD68DCA
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovClassifierBase.ts" />

module Controllers {

    export class ModelLovClassifier extends ModelLovClassifierBase {
    }

    export interface IScopeLovClassifier extends IScopeLovClassifierBase {
    }

    export class ControllerLovClassifier extends ControllerLovClassifierBase {
        
        constructor(
            public $scope: IScopeLovClassifier,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelLovClassifier($scope));
        }
    }

    g_controllers['ControllerLovClassifier'] = Controllers.ControllerLovClassifier;


    
}
