//D6B4F596CA78BF9F4A3907FC92D4C74C
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelFMISBase.ts" />
/// <reference path="../Services/MainService.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ParcelFMIS;
    (function (ParcelFMIS) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(ParcelFMIS.PageModelBase);
        ParcelFMIS.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            PageController.prototype.getPageChanges = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var pageChanges = [];
                if (bForDelete) {
                }
                else {
                    pageChanges = pageChanges.concat(self.model.modelGrpFmisDecision.controller.getChangesToCommit());
                }
                var hasParcelClas = pageChanges.some(function (change) { return change.entityName === "ParcelClas"; });
                return pageChanges;
            };
            PageController.prototype.onSuccesfullSaveFromButton = function (response) {
                var self = this;
                _super.prototype.onSuccesfullSaveFromButton.call(this, response);
                NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
                //call web service
                var paramData = new Services.MainService_updateIntegratedDecisionsNIssues_req();
                paramData.pclaId = self.$scope.pageModel.modelGrpParcelClas.pclaId;
                console.log("Calling Update Integrated Decisions Service...." + self.$scope.pageModel.modelGrpParcelClas.pclaId);
                Services.MainService.updateIntegratedDecisionsNIssues(self.$scope.pageModel.controller, paramData, function (respDataList) {
                    //self.updateUI();
                    NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
                    NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, "Update was Completed Succesfully.");
                    respDataList.forEach(function (item) { return NpTypes.AlertMessage.addWarning(self.$scope.pageModel, item); });
                });
            };
            return PageController;
        })(ParcelFMIS.PageControllerBase);
        ParcelFMIS.PageController = PageController;
        g_controllers['ParcelFMIS.PageController'] = Controllers.ParcelFMIS.PageController;
        var ModelGrpParcelClas = (function (_super) {
            __extends(ModelGrpParcelClas, _super);
            function ModelGrpParcelClas() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcelClas;
        })(ParcelFMIS.ModelGrpParcelClasBase);
        ParcelFMIS.ModelGrpParcelClas = ModelGrpParcelClas;
        var ControllerGrpParcelClas = (function (_super) {
            __extends(ControllerGrpParcelClas, _super);
            function ControllerGrpParcelClas($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcelClas($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpParcelClas;
        })(ParcelFMIS.ControllerGrpParcelClasBase);
        ParcelFMIS.ControllerGrpParcelClas = ControllerGrpParcelClas;
        g_controllers['ParcelFMIS.ControllerGrpParcelClas'] = Controllers.ParcelFMIS.ControllerGrpParcelClas;
        var ModelGrpParcelsIssues = (function (_super) {
            __extends(ModelGrpParcelsIssues, _super);
            function ModelGrpParcelsIssues() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcelsIssues;
        })(ParcelFMIS.ModelGrpParcelsIssuesBase);
        ParcelFMIS.ModelGrpParcelsIssues = ModelGrpParcelsIssues;
        var ControllerGrpParcelsIssues = (function (_super) {
            __extends(ControllerGrpParcelsIssues, _super);
            function ControllerGrpParcelsIssues($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcelsIssues($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpParcelsIssues;
        })(ParcelFMIS.ControllerGrpParcelsIssuesBase);
        ParcelFMIS.ControllerGrpParcelsIssues = ControllerGrpParcelsIssues;
        g_controllers['ParcelFMIS.ControllerGrpParcelsIssues'] = Controllers.ParcelFMIS.ControllerGrpParcelsIssues;
        var ModelGrpFmisDecision = (function (_super) {
            __extends(ModelGrpFmisDecision, _super);
            function ModelGrpFmisDecision() {
                _super.apply(this, arguments);
            }
            return ModelGrpFmisDecision;
        })(ParcelFMIS.ModelGrpFmisDecisionBase);
        ParcelFMIS.ModelGrpFmisDecision = ModelGrpFmisDecision;
        var ControllerGrpFmisDecision = (function (_super) {
            __extends(ControllerGrpFmisDecision, _super);
            function ControllerGrpFmisDecision($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpFmisDecision($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            ControllerGrpFmisDecision.prototype.isCheckMade = function (fmisDecision) {
                var self = this;
                return !fmisDecision.isNew();
            };
            return ControllerGrpFmisDecision;
        })(ParcelFMIS.ControllerGrpFmisDecisionBase);
        ParcelFMIS.ControllerGrpFmisDecision = ControllerGrpFmisDecision;
        g_controllers['ParcelFMIS.ControllerGrpFmisDecision'] = Controllers.ParcelFMIS.ControllerGrpFmisDecision;
        var ModelGrpFmisUpload = (function (_super) {
            __extends(ModelGrpFmisUpload, _super);
            function ModelGrpFmisUpload() {
                _super.apply(this, arguments);
            }
            return ModelGrpFmisUpload;
        })(ParcelFMIS.ModelGrpFmisUploadBase);
        ParcelFMIS.ModelGrpFmisUpload = ModelGrpFmisUpload;
        var ControllerGrpFmisUpload = (function (_super) {
            __extends(ControllerGrpFmisUpload, _super);
            function ControllerGrpFmisUpload($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpFmisUpload($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpFmisUpload;
        })(ParcelFMIS.ControllerGrpFmisUploadBase);
        ParcelFMIS.ControllerGrpFmisUpload = ControllerGrpFmisUpload;
        g_controllers['ParcelFMIS.ControllerGrpFmisUpload'] = Controllers.ParcelFMIS.ControllerGrpFmisUpload;
    })(ParcelFMIS = Controllers.ParcelFMIS || (Controllers.ParcelFMIS = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ParcelFMIS.js.map