//61482D854C2B18B1E94FAD100EC5F115
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/FileDirPathBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var FileDirPath;
    (function (FileDirPath) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(FileDirPath.PageModelBase);
        FileDirPath.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(FileDirPath.PageControllerBase);
        FileDirPath.PageController = PageController;
        g_controllers['FileDirPath.PageController'] = Controllers.FileDirPath.PageController;
        var ModelGrpFileDirPath = (function (_super) {
            __extends(ModelGrpFileDirPath, _super);
            function ModelGrpFileDirPath() {
                _super.apply(this, arguments);
            }
            return ModelGrpFileDirPath;
        })(FileDirPath.ModelGrpFileDirPathBase);
        FileDirPath.ModelGrpFileDirPath = ModelGrpFileDirPath;
        var ControllerGrpFileDirPath = (function (_super) {
            __extends(ControllerGrpFileDirPath, _super);
            function ControllerGrpFileDirPath($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpFileDirPath($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpFileDirPath;
        })(FileDirPath.ControllerGrpFileDirPathBase);
        FileDirPath.ControllerGrpFileDirPath = ControllerGrpFileDirPath;
        g_controllers['FileDirPath.ControllerGrpFileDirPath'] = Controllers.FileDirPath.ControllerGrpFileDirPath;
    })(FileDirPath = Controllers.FileDirPath || (Controllers.FileDirPath = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=FileDirPath.js.map