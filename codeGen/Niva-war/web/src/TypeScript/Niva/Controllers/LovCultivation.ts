//B1CA32A3DA7DC612DECF2FA90DF2453D
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovCultivationBase.ts" />

module Controllers {

    export class ModelLovCultivation extends ModelLovCultivationBase {
    }

    export interface IScopeLovCultivation extends IScopeLovCultivationBase {
    }

    export class ControllerLovCultivation extends ControllerLovCultivationBase {
        
        constructor(
            public $scope: IScopeLovCultivation,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelLovCultivation($scope));
        }
    }

    g_controllers['ControllerLovCultivation'] = Controllers.ControllerLovCultivation;


    
}
