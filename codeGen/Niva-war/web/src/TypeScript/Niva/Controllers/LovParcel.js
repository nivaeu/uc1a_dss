//3525FDAD710EE1C59AEB7D3AFEB9C87B
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovParcelBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovParcel = (function (_super) {
        __extends(ModelLovParcel, _super);
        function ModelLovParcel() {
            _super.apply(this, arguments);
        }
        return ModelLovParcel;
    })(Controllers.ModelLovParcelBase);
    Controllers.ModelLovParcel = ModelLovParcel;
    var ControllerLovParcel = (function (_super) {
        __extends(ControllerLovParcel, _super);
        function ControllerLovParcel($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovParcel($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovParcel;
    })(Controllers.ControllerLovParcelBase);
    Controllers.ControllerLovParcel = ControllerLovParcel;
    g_controllers['ControllerLovParcel'] = Controllers.ControllerLovParcel;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovParcel.js.map