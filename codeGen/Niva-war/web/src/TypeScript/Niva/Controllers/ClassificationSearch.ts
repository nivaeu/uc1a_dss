//D880EB4702867E19E0CBFC98D5FF39CA
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ClassificationSearchBase.ts" />

module Controllers {

    export class ModelClassificationSearch extends ModelClassificationSearchBase {
    }

    export interface IScopeClassificationSearch extends IScopeClassificationSearchBase {
    }

    export class ControllerClassificationSearch extends ControllerClassificationSearchBase {
        
        constructor(
            public $scope: IScopeClassificationSearch,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelClassificationSearch($scope));
        }

        public getLabelById(itmId: string): string {
            switch (itmId) {
                case 'saveBtn_id':
                    return 'Save'
                case 'searchBtn_id':
                    return 'Search';
                case 'clearBtn_id':
                    return 'Clear';
                case 'newBtn_id':
                    return 'Add New Data Import';
                case 'newPageBtn_id':
                    return 'Add New Data Import';
                case 'cancelBtn_id':
                    return 'Back';
                case 'deleteBtn_id':
                    return 'Delete';
                case 'auditBtn_id':
                    return '?';
            }
            return itmId;
        }

    }

    g_controllers['ControllerClassificationSearch'] = Controllers.ControllerClassificationSearch;


    
}
