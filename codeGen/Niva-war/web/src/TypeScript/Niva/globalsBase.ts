/// <reference path="../RTL/npTypes.ts" />
/// <reference path="messages.ts" />
/// <reference path="EntitiesBase/DecisionMakingBase.ts" />

class GlobalsBase extends NpTypes.RTLGlobals {
    _globalDema:NpTypes.UIModel<Entities.DecisionMaking> = new NpTypes.UIModel<Entities.DecisionMaking>(undefined);
    public get globalDema():Entities.DecisionMaking {
        return this._globalDema.value;
    }

    public set globalDema(vl:Entities.DecisionMaking) {
        this._globalDema.value = vl;
    }
    constructor() {
        super();
        this.appName = 'Niva';
        this.appTitle = 'Use Case 1a: Decision Support System';
        this.globalLang = 'en';
        this.requestAuthorizationOptions.getFormTitle = (url:string) => {return 'Αίτημα Εξουσιοδότησης';};
        this.requestAuthorizationOptions.getUserNameLabel = (url:string) => {return 'Χρήστης';};
        this.requestAuthorizationOptions.getPasswordLabel = (url:string) => {return 'Κωδικός';};
        this.requestAuthorizationOptions.getDisplayedErrMsg = (url: string, errMsg: string) => {return errMsg;};
        this.getDynamicMessage = (msgKey: string) => { return Messages.dynamicMessage(msgKey) };
    }

    public refresh(ctrl : NpTypes.IAbstractController):void {
        if (!isVoid(this.globalDema) && !this.globalDema.isNew()) {
                    this.globalDema.refresh(ctrl);
                } 
    }
    
    
    public initializeServerSideGlobals(response: any) {
    }




};

