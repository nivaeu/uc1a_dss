/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/ParcelsIssues.ts" />
/// <reference path="../Entities/ParcelsIssuesActivities.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var ParcelsIssuesActivitiesBase = (function (_super) {
        __extends(ParcelsIssuesActivitiesBase, _super);
        function ParcelsIssuesActivitiesBase(parcelsIssuesActivitiesId, rowVersion, type, dteTimestamp, typeExtrainfo, parcelsIssuesId) {
            _super.call(this);
            var self = this;
            this._parcelsIssuesActivitiesId = new NpTypes.UIStringModel(parcelsIssuesActivitiesId, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._type = new NpTypes.UINumberModel(type, this);
            this._dteTimestamp = new NpTypes.UIDateModel(dteTimestamp, this);
            this._typeExtrainfo = new NpTypes.UIStringModel(typeExtrainfo, this);
            this._parcelsIssuesId = new NpTypes.UIManyToOneModel(parcelsIssuesId, this);
            self.postConstruct();
        }
        ParcelsIssuesActivitiesBase.prototype.getKey = function () {
            return this.parcelsIssuesActivitiesId;
        };
        ParcelsIssuesActivitiesBase.prototype.getKeyName = function () {
            return "parcelsIssuesActivitiesId";
        };
        ParcelsIssuesActivitiesBase.prototype.getEntityName = function () {
            return 'ParcelsIssuesActivities';
        };
        ParcelsIssuesActivitiesBase.prototype.fromJSON = function (data) {
            return Entities.ParcelsIssuesActivities.fromJSONComplete(data);
        };
        Object.defineProperty(ParcelsIssuesActivitiesBase.prototype, "parcelsIssuesActivitiesId", {
            get: function () {
                return this._parcelsIssuesActivitiesId.value;
            },
            set: function (parcelsIssuesActivitiesId) {
                var self = this;
                self._parcelsIssuesActivitiesId.value = parcelsIssuesActivitiesId;
                //    self.parcelsIssuesActivitiesId_listeners.forEach(cb => { cb(<ParcelsIssuesActivities>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelsIssuesActivitiesBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<ParcelsIssuesActivities>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelsIssuesActivitiesBase.prototype, "type", {
            get: function () {
                return this._type.value;
            },
            set: function (type) {
                var self = this;
                self._type.value = type;
                //    self.type_listeners.forEach(cb => { cb(<ParcelsIssuesActivities>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelsIssuesActivitiesBase.prototype, "dteTimestamp", {
            get: function () {
                return this._dteTimestamp.value;
            },
            set: function (dteTimestamp) {
                var self = this;
                self._dteTimestamp.value = dteTimestamp;
                //    self.dteTimestamp_listeners.forEach(cb => { cb(<ParcelsIssuesActivities>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelsIssuesActivitiesBase.prototype, "typeExtrainfo", {
            get: function () {
                return this._typeExtrainfo.value;
            },
            set: function (typeExtrainfo) {
                var self = this;
                self._typeExtrainfo.value = typeExtrainfo;
                //    self.typeExtrainfo_listeners.forEach(cb => { cb(<ParcelsIssuesActivities>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelsIssuesActivitiesBase.prototype, "parcelsIssuesId", {
            get: function () {
                return this._parcelsIssuesId.value;
            },
            set: function (parcelsIssuesId) {
                var self = this;
                self._parcelsIssuesId.value = parcelsIssuesId;
                //    self.parcelsIssuesId_listeners.forEach(cb => { cb(<ParcelsIssuesActivities>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public parcelsIssuesId_listeners: Array<(c:ParcelsIssuesActivities) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.parcelsIssuesActivitiesId_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.type_listeners.splice(0);
            self.dteTimestamp_listeners.splice(0);
            self.typeExtrainfo_listeners.splice(0);
            self.parcelsIssuesId_listeners.splice(0);
        }
        */
        ParcelsIssuesActivitiesBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "ParcelsIssuesActivities:" + x.parcelsIssuesActivitiesId;
            var ret = new Entities.ParcelsIssuesActivities(x.parcelsIssuesActivitiesId, x.rowVersion, x.type, isVoid(x.dteTimestamp) ? null : new Date(x.dteTimestamp), x.typeExtrainfo, x.parcelsIssuesId);
            deserializedEntities[key] = ret;
            ret.parcelsIssuesId = (x.parcelsIssuesId !== undefined && x.parcelsIssuesId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.parcelsIssuesId.$entityName].fromJSONPartial(x.parcelsIssuesId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        ParcelsIssuesActivitiesBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        ParcelsIssuesActivitiesBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "ParcelsIssuesActivities:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "ParcelsIssuesActivitiesBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "ParcelsIssuesActivities:"+x.parcelsIssuesActivitiesId;
                    var cachedCopy = <ParcelsIssuesActivities>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = ParcelsIssuesActivities.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = ParcelsIssuesActivities.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.ParcelsIssuesActivities.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        ParcelsIssuesActivitiesBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.ParcelsIssuesActivities.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        ParcelsIssuesActivitiesBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/ParcelsIssuesActivities/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.ParcelsIssuesActivities.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        ParcelsIssuesActivitiesBase.prototype.toJSON = function () {
            var ret = {};
            ret.parcelsIssuesActivitiesId = this.parcelsIssuesActivitiesId;
            ret.rowVersion = this.rowVersion;
            ret.type = this.type;
            ret.dteTimestamp = this.dteTimestamp;
            ret.typeExtrainfo = this.typeExtrainfo;
            ret.parcelsIssuesId =
                (this.parcelsIssuesId !== undefined && this.parcelsIssuesId !== null) ?
                    { parcelsIssuesId: this.parcelsIssuesId.parcelsIssuesId } :
                    (this.parcelsIssuesId === undefined ? undefined : null);
            return ret;
        };
        ParcelsIssuesActivitiesBase.prototype.updateInstance = function (other) {
            var self = this;
            self.parcelsIssuesActivitiesId = other.parcelsIssuesActivitiesId;
            self.rowVersion = other.rowVersion;
            self.type = other.type;
            self.dteTimestamp = other.dteTimestamp;
            self.typeExtrainfo = other.typeExtrainfo;
            self.parcelsIssuesId = other.parcelsIssuesId;
        };
        ParcelsIssuesActivitiesBase.Create = function () {
            return new Entities.ParcelsIssuesActivities(undefined, undefined, undefined, undefined, undefined, undefined);
        };
        ParcelsIssuesActivitiesBase.CreateById = function (parcelsIssuesActivitiesId) {
            var ret = Entities.ParcelsIssuesActivities.Create();
            ret.parcelsIssuesActivitiesId = parcelsIssuesActivitiesId;
            return ret;
        };
        ParcelsIssuesActivitiesBase.findById_unecrypted = function (parcelsIssuesActivitiesId, $scope, $http, errFunc) {
            if (Entities.ParcelsIssuesActivities.cashedEntities[parcelsIssuesActivitiesId.toString()] !== undefined)
                return Entities.ParcelsIssuesActivities.cashedEntities[parcelsIssuesActivitiesId.toString()];
            var wsPath = "ParcelsIssuesActivities/findByParcelsIssuesActivitiesId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['parcelsIssuesActivitiesId'] = parcelsIssuesActivitiesId;
            var ret = Entities.ParcelsIssuesActivities.Create();
            Entities.ParcelsIssuesActivities.cashedEntities[parcelsIssuesActivitiesId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.ParcelsIssuesActivities.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + parcelsIssuesActivitiesId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        ParcelsIssuesActivitiesBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        ParcelsIssuesActivitiesBase.cashedEntities = {};
        return ParcelsIssuesActivitiesBase;
    })(NpTypes.BaseEntity);
    Entities.ParcelsIssuesActivitiesBase = ParcelsIssuesActivitiesBase;
    NpTypes.BaseEntity.entitiesFactory['ParcelsIssuesActivities'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.ParcelsIssuesActivities.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.ParcelsIssuesActivities.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=ParcelsIssuesActivitiesBase.js.map