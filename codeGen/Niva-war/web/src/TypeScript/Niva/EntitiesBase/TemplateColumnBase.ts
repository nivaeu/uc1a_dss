/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/FileTemplate.ts" />
/// <reference path="../Entities/PredefCol.ts" />
/// <reference path="../Entities/TemplateColumn.ts" />

module Entities {

    export class TemplateColumnBase extends NpTypes.BaseEntity {
        constructor(
            tecoId: string,
            clfierName: string,
            rowVersion: number,
            fiteId: FileTemplate,
            prcoId: PredefCol) 
        {
            super();
            var self = this;
            this._tecoId = new NpTypes.UIStringModel(tecoId, this);
            this._clfierName = new NpTypes.UIStringModel(clfierName, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._fiteId = new NpTypes.UIManyToOneModel<Entities.FileTemplate>(fiteId, this);
            this._prcoId = new NpTypes.UIManyToOneModel<Entities.PredefCol>(prcoId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.tecoId; 
        }
        
        public getKeyName(): string { 
            return "tecoId"; 
        }

        public getEntityName(): string { 
            return 'TemplateColumn'; 
        }
        
        public fromJSON(data: any): TemplateColumn[] { 
            return TemplateColumn.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.tecoId === null || this.tecoId === undefined)
                return true;
            if (this.tecoId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: TemplateColumnBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //tecoId property
        _tecoId: NpTypes.UIStringModel;
        public get tecoId():string {
            return this._tecoId.value;
        }
        public set tecoId(tecoId:string) {
            var self = this;
            self._tecoId.value = tecoId;
        //    self.tecoId_listeners.forEach(cb => { cb(<TemplateColumn>self); });
        }
        //public tecoId_listeners: Array<(c:TemplateColumn) => void> = [];
        //clfierName property
        _clfierName: NpTypes.UIStringModel;
        public get clfierName():string {
            return this._clfierName.value;
        }
        public set clfierName(clfierName:string) {
            var self = this;
            self._clfierName.value = clfierName;
        //    self.clfierName_listeners.forEach(cb => { cb(<TemplateColumn>self); });
        }
        //public clfierName_listeners: Array<(c:TemplateColumn) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<TemplateColumn>self); });
        }
        //public rowVersion_listeners: Array<(c:TemplateColumn) => void> = [];
        //fiteId property
        _fiteId: NpTypes.UIManyToOneModel<Entities.FileTemplate>;
        public get fiteId():FileTemplate {
            return this._fiteId.value;
        }
        public set fiteId(fiteId:FileTemplate) {
            var self = this;
            self._fiteId.value = fiteId;
        //    self.fiteId_listeners.forEach(cb => { cb(<TemplateColumn>self); });
        }
        //public fiteId_listeners: Array<(c:TemplateColumn) => void> = [];
        //prcoId property
        _prcoId: NpTypes.UIManyToOneModel<Entities.PredefCol>;
        public get prcoId():PredefCol {
            return this._prcoId.value;
        }
        public set prcoId(prcoId:PredefCol) {
            var self = this;
            self._prcoId.value = prcoId;
        //    self.prcoId_listeners.forEach(cb => { cb(<TemplateColumn>self); });
        }
        //public prcoId_listeners: Array<(c:TemplateColumn) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.tecoId_listeners.splice(0);
            self.clfierName_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.fiteId_listeners.splice(0);
            self.prcoId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : TemplateColumn {
            var key = "TemplateColumn:"+x.tecoId;
            var ret =  new TemplateColumn(
                x.tecoId,
                x.clfierName,
                x.rowVersion,
                x.fiteId,
                x.prcoId
            );
            deserializedEntities[key] = ret;
            ret.fiteId = (x.fiteId !== undefined && x.fiteId !== null) ? <FileTemplate>NpTypes.BaseEntity.entitiesFactory[x.fiteId.$entityName].fromJSONPartial(x.fiteId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.prcoId = (x.prcoId !== undefined && x.prcoId !== null) ? <PredefCol>NpTypes.BaseEntity.entitiesFactory[x.prcoId.$entityName].fromJSONPartial(x.prcoId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): TemplateColumn {
            
            return <TemplateColumn>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : TemplateColumn {
            var self = this;
            var key="";
            var ret:TemplateColumn = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "TemplateColumn:"+x.$refId;
                ret = <TemplateColumn>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "TemplateColumnBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "TemplateColumn:"+x.tecoId;
                    var cachedCopy = <TemplateColumn>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = TemplateColumn.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = TemplateColumn.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = TemplateColumn.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<TemplateColumn> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<TemplateColumn> = _.map(data, (x:any):TemplateColumn => {
                return TemplateColumn.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:TemplateColumn[])=>void)  {
            var url = "/Niva/rest/TemplateColumn/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = TemplateColumn.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.tecoId = this.tecoId;
                ret.clfierName = this.clfierName;
                ret.rowVersion = this.rowVersion;
                ret.fiteId = 
                    (this.fiteId !== undefined && this.fiteId !== null) ? 
                        { fiteId :  this.fiteId.fiteId} :
                        (this.fiteId === undefined ? undefined : null);
                ret.prcoId = 
                    (this.prcoId !== undefined && this.prcoId !== null) ? 
                        { prcoId :  this.prcoId.prcoId} :
                        (this.prcoId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:TemplateColumn):void {
            var self = this;
            self.tecoId = other.tecoId
            self.clfierName = other.clfierName
            self.rowVersion = other.rowVersion
            self.fiteId = other.fiteId
            self.prcoId = other.prcoId
        }

        public static Create() : TemplateColumn {
            return new TemplateColumn(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(tecoId:string) : TemplateColumn {
            var ret = TemplateColumn.Create();
            ret.tecoId = tecoId;
            return ret;
        }

        public static cashedEntities: { [id: string]: TemplateColumn; } = {};
        public static findById_unecrypted(tecoId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : TemplateColumn {
            if (Entities.TemplateColumn.cashedEntities[tecoId.toString()] !== undefined)
                return Entities.TemplateColumn.cashedEntities[tecoId.toString()];

            var wsPath = "TemplateColumn/findByTecoId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['tecoId'] = tecoId;
            var ret = TemplateColumn.Create();
            Entities.TemplateColumn.cashedEntities[tecoId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.TemplateColumn.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + tecoId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['TemplateColumn'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.TemplateColumn.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.TemplateColumn.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


}