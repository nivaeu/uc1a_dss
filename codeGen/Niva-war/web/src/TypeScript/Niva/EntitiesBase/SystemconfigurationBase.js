/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Systemconfiguration.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var SystemconfigurationBase = (function (_super) {
        __extends(SystemconfigurationBase, _super);
        function SystemconfigurationBase(agencyId, srid, language) {
            _super.call(this);
            var self = this;
            this._agencyId = new NpTypes.UIStringModel(agencyId, this);
            this._srid = new NpTypes.UINumberModel(srid, this);
            this._language = new NpTypes.UIStringModel(language, this);
            self.postConstruct();
        }
        SystemconfigurationBase.prototype.getKey = function () {
            return this.agencyId;
        };
        SystemconfigurationBase.prototype.getKeyName = function () {
            return "agencyId";
        };
        SystemconfigurationBase.prototype.getEntityName = function () {
            return 'Systemconfiguration';
        };
        SystemconfigurationBase.prototype.fromJSON = function (data) {
            return Entities.Systemconfiguration.fromJSONComplete(data);
        };
        Object.defineProperty(SystemconfigurationBase.prototype, "agencyId", {
            get: function () {
                return this._agencyId.value;
            },
            set: function (agencyId) {
                var self = this;
                self._agencyId.value = agencyId;
                //    self.agencyId_listeners.forEach(cb => { cb(<Systemconfiguration>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SystemconfigurationBase.prototype, "srid", {
            get: function () {
                return this._srid.value;
            },
            set: function (srid) {
                var self = this;
                self._srid.value = srid;
                //    self.srid_listeners.forEach(cb => { cb(<Systemconfiguration>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SystemconfigurationBase.prototype, "language", {
            get: function () {
                return this._language.value;
            },
            set: function (language) {
                var self = this;
                self._language.value = language;
                //    self.language_listeners.forEach(cb => { cb(<Systemconfiguration>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public language_listeners: Array<(c:Systemconfiguration) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.agencyId_listeners.splice(0);
            self.srid_listeners.splice(0);
            self.language_listeners.splice(0);
        }
        */
        SystemconfigurationBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "Systemconfiguration:" + x.agencyId;
            var ret = new Entities.Systemconfiguration(x.agencyId, x.srid, x.language);
            deserializedEntities[key] = ret;
            return ret;
        };
        SystemconfigurationBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        SystemconfigurationBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "Systemconfiguration:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "SystemconfigurationBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "Systemconfiguration:"+x.agencyId;
                    var cachedCopy = <Systemconfiguration>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = Systemconfiguration.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = Systemconfiguration.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.Systemconfiguration.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        SystemconfigurationBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.Systemconfiguration.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        SystemconfigurationBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/Systemconfiguration/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.Systemconfiguration.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        SystemconfigurationBase.prototype.toJSON = function () {
            var ret = {};
            ret.agencyId = this.agencyId;
            ret.srid = this.srid;
            ret.language = this.language;
            return ret;
        };
        SystemconfigurationBase.prototype.updateInstance = function (other) {
            var self = this;
            self.agencyId = other.agencyId;
            self.srid = other.srid;
            self.language = other.language;
        };
        SystemconfigurationBase.Create = function () {
            return new Entities.Systemconfiguration(undefined, undefined, undefined);
        };
        SystemconfigurationBase.CreateById = function (agencyId) {
            var ret = Entities.Systemconfiguration.Create();
            ret.agencyId = agencyId;
            return ret;
        };
        SystemconfigurationBase.findById_unecrypted = function (agencyId, $scope, $http, errFunc) {
            if (Entities.Systemconfiguration.cashedEntities[agencyId.toString()] !== undefined)
                return Entities.Systemconfiguration.cashedEntities[agencyId.toString()];
            var wsPath = "Systemconfiguration/findByAgencyId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['agencyId'] = agencyId;
            var ret = Entities.Systemconfiguration.Create();
            Entities.Systemconfiguration.cashedEntities[agencyId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.Systemconfiguration.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + agencyId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        SystemconfigurationBase.prototype.getRowVersion = function () {
            return 0;
        };
        SystemconfigurationBase.cashedEntities = {};
        return SystemconfigurationBase;
    })(NpTypes.BaseEntity);
    Entities.SystemconfigurationBase = SystemconfigurationBase;
    NpTypes.BaseEntity.entitiesFactory['Systemconfiguration'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.Systemconfiguration.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.Systemconfiguration.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=SystemconfigurationBase.js.map