/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Classification.ts" />
/// <reference path="../Entities/Statistic.ts" />

module Entities {

    export class StatisticBase extends NpTypes.BaseEntity {
        constructor(
            statId: string,
            name: string,
            description: string,
            value: number,
            rowVersion: number,
            clasId: Classification) 
        {
            super();
            var self = this;
            this._statId = new NpTypes.UIStringModel(statId, this);
            this._name = new NpTypes.UIStringModel(name, this);
            this._description = new NpTypes.UIStringModel(description, this);
            this._value = new NpTypes.UINumberModel(value, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._clasId = new NpTypes.UIManyToOneModel<Entities.Classification>(clasId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.statId; 
        }
        
        public getKeyName(): string { 
            return "statId"; 
        }

        public getEntityName(): string { 
            return 'Statistic'; 
        }
        
        public fromJSON(data: any): Statistic[] { 
            return Statistic.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.statId === null || this.statId === undefined)
                return true;
            if (this.statId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: StatisticBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //statId property
        _statId: NpTypes.UIStringModel;
        public get statId():string {
            return this._statId.value;
        }
        public set statId(statId:string) {
            var self = this;
            self._statId.value = statId;
        //    self.statId_listeners.forEach(cb => { cb(<Statistic>self); });
        }
        //public statId_listeners: Array<(c:Statistic) => void> = [];
        //name property
        _name: NpTypes.UIStringModel;
        public get name():string {
            return this._name.value;
        }
        public set name(name:string) {
            var self = this;
            self._name.value = name;
        //    self.name_listeners.forEach(cb => { cb(<Statistic>self); });
        }
        //public name_listeners: Array<(c:Statistic) => void> = [];
        //description property
        _description: NpTypes.UIStringModel;
        public get description():string {
            return this._description.value;
        }
        public set description(description:string) {
            var self = this;
            self._description.value = description;
        //    self.description_listeners.forEach(cb => { cb(<Statistic>self); });
        }
        //public description_listeners: Array<(c:Statistic) => void> = [];
        //value property
        _value: NpTypes.UINumberModel;
        public get value():number {
            return this._value.value;
        }
        public set value(value:number) {
            var self = this;
            self._value.value = value;
        //    self.value_listeners.forEach(cb => { cb(<Statistic>self); });
        }
        //public value_listeners: Array<(c:Statistic) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<Statistic>self); });
        }
        //public rowVersion_listeners: Array<(c:Statistic) => void> = [];
        //clasId property
        _clasId: NpTypes.UIManyToOneModel<Entities.Classification>;
        public get clasId():Classification {
            return this._clasId.value;
        }
        public set clasId(clasId:Classification) {
            var self = this;
            self._clasId.value = clasId;
        //    self.clasId_listeners.forEach(cb => { cb(<Statistic>self); });
        }
        //public clasId_listeners: Array<(c:Statistic) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.statId_listeners.splice(0);
            self.name_listeners.splice(0);
            self.description_listeners.splice(0);
            self.value_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.clasId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : Statistic {
            var key = "Statistic:"+x.statId;
            var ret =  new Statistic(
                x.statId,
                x.name,
                x.description,
                x.value,
                x.rowVersion,
                x.clasId
            );
            deserializedEntities[key] = ret;
            ret.clasId = (x.clasId !== undefined && x.clasId !== null) ? <Classification>NpTypes.BaseEntity.entitiesFactory[x.clasId.$entityName].fromJSONPartial(x.clasId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): Statistic {
            
            return <Statistic>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : Statistic {
            var self = this;
            var key="";
            var ret:Statistic = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "Statistic:"+x.$refId;
                ret = <Statistic>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "StatisticBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "Statistic:"+x.statId;
                    var cachedCopy = <Statistic>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = Statistic.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = Statistic.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Statistic.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Statistic> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<Statistic> = _.map(data, (x:any):Statistic => {
                return Statistic.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:Statistic[])=>void)  {
            var url = "/Niva/rest/Statistic/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = Statistic.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.statId = this.statId;
                ret.name = this.name;
                ret.description = this.description;
                ret.value = this.value;
                ret.rowVersion = this.rowVersion;
                ret.clasId = 
                    (this.clasId !== undefined && this.clasId !== null) ? 
                        { clasId :  this.clasId.clasId} :
                        (this.clasId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:Statistic):void {
            var self = this;
            self.statId = other.statId
            self.name = other.name
            self.description = other.description
            self.value = other.value
            self.rowVersion = other.rowVersion
            self.clasId = other.clasId
        }

        public static Create() : Statistic {
            return new Statistic(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(statId:string) : Statistic {
            var ret = Statistic.Create();
            ret.statId = statId;
            return ret;
        }

        public static cashedEntities: { [id: string]: Statistic; } = {};
        public static findById_unecrypted(statId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : Statistic {
            if (Entities.Statistic.cashedEntities[statId.toString()] !== undefined)
                return Entities.Statistic.cashedEntities[statId.toString()];

            var wsPath = "Statistic/findByStatId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['statId'] = statId;
            var ret = Statistic.Create();
            Entities.Statistic.cashedEntities[statId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.Statistic.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + statId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['Statistic'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.Statistic.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.Statistic.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


}