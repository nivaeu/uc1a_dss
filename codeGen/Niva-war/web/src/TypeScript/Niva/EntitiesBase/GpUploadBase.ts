/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/GpRequestsContexts.ts" />
/// <reference path="../Entities/GpUpload.ts" />

module Entities {

    export class GpUploadBase extends NpTypes.BaseEntity {
        constructor(
            gpUploadsId: string,
            data: string,
            environment: string,
            dteUpload: Date,
            hash: string,
            image: string,
            gpRequestsContextsId: GpRequestsContexts) 
        {
            super();
            var self = this;
            this._gpUploadsId = new NpTypes.UIStringModel(gpUploadsId, this);
            this._data = new NpTypes.UIStringModel(data, this);
            this._environment = new NpTypes.UIStringModel(environment, this);
            this._dteUpload = new NpTypes.UIDateModel(dteUpload, this);
            this._hash = new NpTypes.UIStringModel(hash, this);
            this._image = new NpTypes.UIBlobModel(
                image, 
                ()  => 'Download',
                (fileName:string)   => {},
                this);
            this._gpRequestsContextsId = new NpTypes.UIManyToOneModel<Entities.GpRequestsContexts>(gpRequestsContextsId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.gpUploadsId; 
        }
        
        public getKeyName(): string { 
            return "gpUploadsId"; 
        }

        public getEntityName(): string { 
            return 'GpUpload'; 
        }
        
        public fromJSON(data: any): GpUpload[] { 
            return GpUpload.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.gpUploadsId === null || this.gpUploadsId === undefined)
                return true;
            if (this.gpUploadsId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: GpUploadBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //gpUploadsId property
        _gpUploadsId: NpTypes.UIStringModel;
        public get gpUploadsId():string {
            return this._gpUploadsId.value;
        }
        public set gpUploadsId(gpUploadsId:string) {
            var self = this;
            self._gpUploadsId.value = gpUploadsId;
        //    self.gpUploadsId_listeners.forEach(cb => { cb(<GpUpload>self); });
        }
        //public gpUploadsId_listeners: Array<(c:GpUpload) => void> = [];
        //data property
        _data: NpTypes.UIStringModel;
        public get data():string {
            return this._data.value;
        }
        public set data(data:string) {
            var self = this;
            self._data.value = data;
        //    self.data_listeners.forEach(cb => { cb(<GpUpload>self); });
        }
        //public data_listeners: Array<(c:GpUpload) => void> = [];
        //environment property
        _environment: NpTypes.UIStringModel;
        public get environment():string {
            return this._environment.value;
        }
        public set environment(environment:string) {
            var self = this;
            self._environment.value = environment;
        //    self.environment_listeners.forEach(cb => { cb(<GpUpload>self); });
        }
        //public environment_listeners: Array<(c:GpUpload) => void> = [];
        //dteUpload property
        _dteUpload: NpTypes.UIDateModel;
        public get dteUpload():Date {
            return this._dteUpload.value;
        }
        public set dteUpload(dteUpload:Date) {
            var self = this;
            self._dteUpload.value = dteUpload;
        //    self.dteUpload_listeners.forEach(cb => { cb(<GpUpload>self); });
        }
        //public dteUpload_listeners: Array<(c:GpUpload) => void> = [];
        //hash property
        _hash: NpTypes.UIStringModel;
        public get hash():string {
            return this._hash.value;
        }
        public set hash(hash:string) {
            var self = this;
            self._hash.value = hash;
        //    self.hash_listeners.forEach(cb => { cb(<GpUpload>self); });
        }
        //public hash_listeners: Array<(c:GpUpload) => void> = [];
        //image property
        _image: NpTypes.UIBlobModel;
        public get image():string {
            return this._image.value;
        }
        public set image(image:string) {
            var self = this;
            self._image.value = image;
        //    self.image_listeners.forEach(cb => { cb(<GpUpload>self); });
        }
        //public image_listeners: Array<(c:GpUpload) => void> = [];
        //gpRequestsContextsId property
        _gpRequestsContextsId: NpTypes.UIManyToOneModel<Entities.GpRequestsContexts>;
        public get gpRequestsContextsId():GpRequestsContexts {
            return this._gpRequestsContextsId.value;
        }
        public set gpRequestsContextsId(gpRequestsContextsId:GpRequestsContexts) {
            var self = this;
            self._gpRequestsContextsId.value = gpRequestsContextsId;
        //    self.gpRequestsContextsId_listeners.forEach(cb => { cb(<GpUpload>self); });
        }
        //public gpRequestsContextsId_listeners: Array<(c:GpUpload) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.gpUploadsId_listeners.splice(0);
            self.data_listeners.splice(0);
            self.environment_listeners.splice(0);
            self.dteUpload_listeners.splice(0);
            self.hash_listeners.splice(0);
            self.image_listeners.splice(0);
            self.gpRequestsContextsId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : GpUpload {
            var key = "GpUpload:"+x.gpUploadsId;
            var ret =  new GpUpload(
                x.gpUploadsId,
                x.data,
                x.environment,
                isVoid(x.dteUpload) ? null : new Date(x.dteUpload),
                x.hash,
                x.image,
                x.gpRequestsContextsId
            );
            deserializedEntities[key] = ret;
            ret.gpRequestsContextsId = (x.gpRequestsContextsId !== undefined && x.gpRequestsContextsId !== null) ? <GpRequestsContexts>NpTypes.BaseEntity.entitiesFactory[x.gpRequestsContextsId.$entityName].fromJSONPartial(x.gpRequestsContextsId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): GpUpload {
            
            return <GpUpload>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : GpUpload {
            var self = this;
            var key="";
            var ret:GpUpload = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "GpUpload:"+x.$refId;
                ret = <GpUpload>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "GpUploadBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "GpUpload:"+x.gpUploadsId;
                    var cachedCopy = <GpUpload>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = GpUpload.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = GpUpload.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = GpUpload.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<GpUpload> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<GpUpload> = _.map(data, (x:any):GpUpload => {
                return GpUpload.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:GpUpload[])=>void)  {
            var url = "/Niva/rest/GpUpload/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = GpUpload.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.gpUploadsId = this.gpUploadsId;
                ret.data = this.data;
                ret.environment = this.environment;
                ret.dteUpload = this.dteUpload;
                ret.hash = this.hash;
                ret.image = this.image;
                ret.gpRequestsContextsId = 
                    (this.gpRequestsContextsId !== undefined && this.gpRequestsContextsId !== null) ? 
                        { gpRequestsContextsId :  this.gpRequestsContextsId.gpRequestsContextsId} :
                        (this.gpRequestsContextsId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:GpUpload):void {
            var self = this;
            self.gpUploadsId = other.gpUploadsId
            self.data = other.data
            self.environment = other.environment
            self.dteUpload = other.dteUpload
            self.hash = other.hash
            self.image = other.image
            self.gpRequestsContextsId = other.gpRequestsContextsId
        }

        public static Create() : GpUpload {
            return new GpUpload(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(gpUploadsId:string) : GpUpload {
            var ret = GpUpload.Create();
            ret.gpUploadsId = gpUploadsId;
            return ret;
        }

        public static cashedEntities: { [id: string]: GpUpload; } = {};
        public static findById_unecrypted(gpUploadsId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : GpUpload {
            if (Entities.GpUpload.cashedEntities[gpUploadsId.toString()] !== undefined)
                return Entities.GpUpload.cashedEntities[gpUploadsId.toString()];

            var wsPath = "GpUpload/findByGpUploadsId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['gpUploadsId'] = gpUploadsId;
            var ret = GpUpload.Create();
            Entities.GpUpload.cashedEntities[gpUploadsId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.GpUpload.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + gpUploadsId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return 0;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['GpUpload'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.GpUpload.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.GpUpload.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


}