/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/AgrisnapUsersProducers.ts" />
/// <reference path="../Entities/GpRequests.ts" />
/// <reference path="../Entities/AgrisnapUsers.ts" />

module Entities {

    export class AgrisnapUsersBase extends NpTypes.BaseEntity {
        constructor(
            agrisnapName: string,
            rowVersion: number,
            agrisnapUsersId: string,
            agrisnapUid: string) 
        {
            super();
            var self = this;
            this._agrisnapName = new NpTypes.UIStringModel(agrisnapName, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._agrisnapUsersId = new NpTypes.UIStringModel(agrisnapUsersId, this);
            this._agrisnapUid = new NpTypes.UIStringModel(agrisnapUid, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.agrisnapUsersId; 
        }
        
        public getKeyName(): string { 
            return "agrisnapUsersId"; 
        }

        public getEntityName(): string { 
            return 'AgrisnapUsers'; 
        }
        
        public fromJSON(data: any): AgrisnapUsers[] { 
            return AgrisnapUsers.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.agrisnapUsersId === null || this.agrisnapUsersId === undefined)
                return true;
            if (this.agrisnapUsersId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: AgrisnapUsersBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //agrisnapName property
        _agrisnapName: NpTypes.UIStringModel;
        public get agrisnapName():string {
            return this._agrisnapName.value;
        }
        public set agrisnapName(agrisnapName:string) {
            var self = this;
            self._agrisnapName.value = agrisnapName;
        //    self.agrisnapName_listeners.forEach(cb => { cb(<AgrisnapUsers>self); });
        }
        //public agrisnapName_listeners: Array<(c:AgrisnapUsers) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<AgrisnapUsers>self); });
        }
        //public rowVersion_listeners: Array<(c:AgrisnapUsers) => void> = [];
        //agrisnapUsersId property
        _agrisnapUsersId: NpTypes.UIStringModel;
        public get agrisnapUsersId():string {
            return this._agrisnapUsersId.value;
        }
        public set agrisnapUsersId(agrisnapUsersId:string) {
            var self = this;
            self._agrisnapUsersId.value = agrisnapUsersId;
        //    self.agrisnapUsersId_listeners.forEach(cb => { cb(<AgrisnapUsers>self); });
        }
        //public agrisnapUsersId_listeners: Array<(c:AgrisnapUsers) => void> = [];
        //agrisnapUid property
        _agrisnapUid: NpTypes.UIStringModel;
        public get agrisnapUid():string {
            return this._agrisnapUid.value;
        }
        public set agrisnapUid(agrisnapUid:string) {
            var self = this;
            self._agrisnapUid.value = agrisnapUid;
        //    self.agrisnapUid_listeners.forEach(cb => { cb(<AgrisnapUsers>self); });
        }
        //public agrisnapUid_listeners: Array<(c:AgrisnapUsers) => void> = [];
        public static agrisnapUsersProducersCollection: (self:Entities.AgrisnapUsers, func: (agrisnapUsersProducersList: Entities.AgrisnapUsersProducers[]) => void) => void = null; //(agrisnapUsers, list) => { };
        public agrisnapUsersProducersCollection(func: (agrisnapUsersProducersList: Entities.AgrisnapUsersProducers[]) => void) {
            var self: Entities.AgrisnapUsersBase = this;
        	if (!isVoid(AgrisnapUsers.agrisnapUsersProducersCollection)) {
        		if (self instanceof Entities.AgrisnapUsers) {
        			AgrisnapUsers.agrisnapUsersProducersCollection(<Entities.AgrisnapUsers>self, func);
                }
            }
        }
        public static gpRequestsCollection: (self:Entities.AgrisnapUsers, func: (gpRequestsList: Entities.GpRequests[]) => void) => void = null; //(agrisnapUsers, list) => { };
        public gpRequestsCollection(func: (gpRequestsList: Entities.GpRequests[]) => void) {
            var self: Entities.AgrisnapUsersBase = this;
        	if (!isVoid(AgrisnapUsers.gpRequestsCollection)) {
        		if (self instanceof Entities.AgrisnapUsers) {
        			AgrisnapUsers.gpRequestsCollection(<Entities.AgrisnapUsers>self, func);
                }
            }
        }
        /*
        public removeAllListeners() {
            var self = this;
            self.agrisnapName_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.agrisnapUsersId_listeners.splice(0);
            self.agrisnapUid_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : AgrisnapUsers {
            var key = "AgrisnapUsers:"+x.agrisnapUsersId;
            var ret =  new AgrisnapUsers(
                x.agrisnapName,
                x.rowVersion,
                x.agrisnapUsersId,
                x.agrisnapUid
            );
            deserializedEntities[key] = ret;
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): AgrisnapUsers {
            
            return <AgrisnapUsers>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : AgrisnapUsers {
            var self = this;
            var key="";
            var ret:AgrisnapUsers = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "AgrisnapUsers:"+x.$refId;
                ret = <AgrisnapUsers>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "AgrisnapUsersBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "AgrisnapUsers:"+x.agrisnapUsersId;
                    var cachedCopy = <AgrisnapUsers>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = AgrisnapUsers.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = AgrisnapUsers.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = AgrisnapUsers.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<AgrisnapUsers> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<AgrisnapUsers> = _.map(data, (x:any):AgrisnapUsers => {
                return AgrisnapUsers.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:AgrisnapUsers[])=>void)  {
            var url = "/Niva/rest/AgrisnapUsers/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = AgrisnapUsers.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.agrisnapName = this.agrisnapName;
                ret.rowVersion = this.rowVersion;
                ret.agrisnapUsersId = this.agrisnapUsersId;
                ret.agrisnapUid = this.agrisnapUid;
            return ret;
        }

        public updateInstance(other:AgrisnapUsers):void {
            var self = this;
            self.agrisnapName = other.agrisnapName
            self.rowVersion = other.rowVersion
            self.agrisnapUsersId = other.agrisnapUsersId
            self.agrisnapUid = other.agrisnapUid
        }

        public static Create() : AgrisnapUsers {
            return new AgrisnapUsers(
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(agrisnapUsersId:string) : AgrisnapUsers {
            var ret = AgrisnapUsers.Create();
            ret.agrisnapUsersId = agrisnapUsersId;
            return ret;
        }

        public static cashedEntities: { [id: string]: AgrisnapUsers; } = {};
        public static findById_unecrypted(agrisnapUsersId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : AgrisnapUsers {
            if (Entities.AgrisnapUsers.cashedEntities[agrisnapUsersId.toString()] !== undefined)
                return Entities.AgrisnapUsers.cashedEntities[agrisnapUsersId.toString()];

            var wsPath = "AgrisnapUsers/findByAgrisnapUsersId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['agrisnapUsersId'] = agrisnapUsersId;
            var ret = AgrisnapUsers.Create();
            Entities.AgrisnapUsers.cashedEntities[agrisnapUsersId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.AgrisnapUsers.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + agrisnapUsersId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['AgrisnapUsers'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.AgrisnapUsers.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.AgrisnapUsers.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


}