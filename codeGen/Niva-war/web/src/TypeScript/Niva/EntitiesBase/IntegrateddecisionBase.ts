/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/ParcelClas.ts" />
/// <reference path="../Entities/DecisionMaking.ts" />
/// <reference path="../Entities/Integrateddecision.ts" />

module Entities {

    export class IntegrateddecisionBase extends NpTypes.BaseEntity {
        constructor(
            integrateddecisionsId: string,
            decisionCode: number,
            dteUpdate: Date,
            usrUpdate: string,
            pclaId: ParcelClas,
            demaId: DecisionMaking) 
        {
            super();
            var self = this;
            this._integrateddecisionsId = new NpTypes.UIStringModel(integrateddecisionsId, this);
            this._decisionCode = new NpTypes.UINumberModel(decisionCode, this);
            this._dteUpdate = new NpTypes.UIDateModel(dteUpdate, this);
            this._usrUpdate = new NpTypes.UIStringModel(usrUpdate, this);
            this._pclaId = new NpTypes.UIManyToOneModel<Entities.ParcelClas>(pclaId, this);
            this._demaId = new NpTypes.UIManyToOneModel<Entities.DecisionMaking>(demaId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.integrateddecisionsId; 
        }
        
        public getKeyName(): string { 
            return "integrateddecisionsId"; 
        }

        public getEntityName(): string { 
            return 'Integrateddecision'; 
        }
        
        public fromJSON(data: any): Integrateddecision[] { 
            return Integrateddecision.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.integrateddecisionsId === null || this.integrateddecisionsId === undefined)
                return true;
            if (this.integrateddecisionsId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: IntegrateddecisionBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //integrateddecisionsId property
        _integrateddecisionsId: NpTypes.UIStringModel;
        public get integrateddecisionsId():string {
            return this._integrateddecisionsId.value;
        }
        public set integrateddecisionsId(integrateddecisionsId:string) {
            var self = this;
            self._integrateddecisionsId.value = integrateddecisionsId;
        //    self.integrateddecisionsId_listeners.forEach(cb => { cb(<Integrateddecision>self); });
        }
        //public integrateddecisionsId_listeners: Array<(c:Integrateddecision) => void> = [];
        //decisionCode property
        _decisionCode: NpTypes.UINumberModel;
        public get decisionCode():number {
            return this._decisionCode.value;
        }
        public set decisionCode(decisionCode:number) {
            var self = this;
            self._decisionCode.value = decisionCode;
        //    self.decisionCode_listeners.forEach(cb => { cb(<Integrateddecision>self); });
        }
        //public decisionCode_listeners: Array<(c:Integrateddecision) => void> = [];
        //dteUpdate property
        _dteUpdate: NpTypes.UIDateModel;
        public get dteUpdate():Date {
            return this._dteUpdate.value;
        }
        public set dteUpdate(dteUpdate:Date) {
            var self = this;
            self._dteUpdate.value = dteUpdate;
        //    self.dteUpdate_listeners.forEach(cb => { cb(<Integrateddecision>self); });
        }
        //public dteUpdate_listeners: Array<(c:Integrateddecision) => void> = [];
        //usrUpdate property
        _usrUpdate: NpTypes.UIStringModel;
        public get usrUpdate():string {
            return this._usrUpdate.value;
        }
        public set usrUpdate(usrUpdate:string) {
            var self = this;
            self._usrUpdate.value = usrUpdate;
        //    self.usrUpdate_listeners.forEach(cb => { cb(<Integrateddecision>self); });
        }
        //public usrUpdate_listeners: Array<(c:Integrateddecision) => void> = [];
        //pclaId property
        _pclaId: NpTypes.UIManyToOneModel<Entities.ParcelClas>;
        public get pclaId():ParcelClas {
            return this._pclaId.value;
        }
        public set pclaId(pclaId:ParcelClas) {
            var self = this;
            self._pclaId.value = pclaId;
        //    self.pclaId_listeners.forEach(cb => { cb(<Integrateddecision>self); });
        }
        //public pclaId_listeners: Array<(c:Integrateddecision) => void> = [];
        //demaId property
        _demaId: NpTypes.UIManyToOneModel<Entities.DecisionMaking>;
        public get demaId():DecisionMaking {
            return this._demaId.value;
        }
        public set demaId(demaId:DecisionMaking) {
            var self = this;
            self._demaId.value = demaId;
        //    self.demaId_listeners.forEach(cb => { cb(<Integrateddecision>self); });
        }
        //public demaId_listeners: Array<(c:Integrateddecision) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.integrateddecisionsId_listeners.splice(0);
            self.decisionCode_listeners.splice(0);
            self.dteUpdate_listeners.splice(0);
            self.usrUpdate_listeners.splice(0);
            self.pclaId_listeners.splice(0);
            self.demaId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : Integrateddecision {
            var key = "Integrateddecision:"+x.integrateddecisionsId;
            var ret =  new Integrateddecision(
                x.integrateddecisionsId,
                x.decisionCode,
                isVoid(x.dteUpdate) ? null : new Date(x.dteUpdate),
                x.usrUpdate,
                x.pclaId,
                x.demaId
            );
            deserializedEntities[key] = ret;
            ret.pclaId = (x.pclaId !== undefined && x.pclaId !== null) ? <ParcelClas>NpTypes.BaseEntity.entitiesFactory[x.pclaId.$entityName].fromJSONPartial(x.pclaId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.demaId = (x.demaId !== undefined && x.demaId !== null) ? <DecisionMaking>NpTypes.BaseEntity.entitiesFactory[x.demaId.$entityName].fromJSONPartial(x.demaId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): Integrateddecision {
            
            return <Integrateddecision>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : Integrateddecision {
            var self = this;
            var key="";
            var ret:Integrateddecision = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "Integrateddecision:"+x.$refId;
                ret = <Integrateddecision>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "IntegrateddecisionBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "Integrateddecision:"+x.integrateddecisionsId;
                    var cachedCopy = <Integrateddecision>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = Integrateddecision.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = Integrateddecision.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Integrateddecision.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Integrateddecision> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<Integrateddecision> = _.map(data, (x:any):Integrateddecision => {
                return Integrateddecision.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:Integrateddecision[])=>void)  {
            var url = "/Niva/rest/Integrateddecision/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = Integrateddecision.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.integrateddecisionsId = this.integrateddecisionsId;
                ret.decisionCode = this.decisionCode;
                ret.dteUpdate = this.dteUpdate;
                ret.usrUpdate = this.usrUpdate;
                ret.pclaId = 
                    (this.pclaId !== undefined && this.pclaId !== null) ? 
                        { pclaId :  this.pclaId.pclaId} :
                        (this.pclaId === undefined ? undefined : null);
                ret.demaId = 
                    (this.demaId !== undefined && this.demaId !== null) ? 
                        { demaId :  this.demaId.demaId} :
                        (this.demaId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:Integrateddecision):void {
            var self = this;
            self.integrateddecisionsId = other.integrateddecisionsId
            self.decisionCode = other.decisionCode
            self.dteUpdate = other.dteUpdate
            self.usrUpdate = other.usrUpdate
            self.pclaId = other.pclaId
            self.demaId = other.demaId
        }

        public static Create() : Integrateddecision {
            return new Integrateddecision(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(integrateddecisionsId:string) : Integrateddecision {
            var ret = Integrateddecision.Create();
            ret.integrateddecisionsId = integrateddecisionsId;
            return ret;
        }

        public static cashedEntities: { [id: string]: Integrateddecision; } = {};
        public static findById_unecrypted(integrateddecisionsId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : Integrateddecision {
            if (Entities.Integrateddecision.cashedEntities[integrateddecisionsId.toString()] !== undefined)
                return Entities.Integrateddecision.cashedEntities[integrateddecisionsId.toString()];

            var wsPath = "Integrateddecision/findByIntegrateddecisionsId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['integrateddecisionsId'] = integrateddecisionsId;
            var ret = Integrateddecision.Create();
            Entities.Integrateddecision.cashedEntities[integrateddecisionsId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.Integrateddecision.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + integrateddecisionsId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return 0;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['Integrateddecision'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.Integrateddecision.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.Integrateddecision.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


}