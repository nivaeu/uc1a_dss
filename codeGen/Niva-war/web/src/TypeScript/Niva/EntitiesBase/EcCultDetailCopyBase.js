/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/EcCultCopy.ts" />
/// <reference path="../Entities/EcCultDetailCopy.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var EcCultDetailCopyBase = (function (_super) {
        __extends(EcCultDetailCopyBase, _super);
        function EcCultDetailCopyBase(ecdcId, orderingNumber, agreesDeclar, comparisonOper, probabThres, decisionLight, rowVersion, agreesDeclar2, comparisonOper2, probabThres2, probabThresSum, comparisonOper3, ecccId) {
            _super.call(this);
            var self = this;
            this._ecdcId = new NpTypes.UIStringModel(ecdcId, this);
            this._orderingNumber = new NpTypes.UINumberModel(orderingNumber, this);
            this._agreesDeclar = new NpTypes.UINumberModel(agreesDeclar, this);
            this._comparisonOper = new NpTypes.UINumberModel(comparisonOper, this);
            this._probabThres = new NpTypes.UINumberModel(probabThres, this);
            this._decisionLight = new NpTypes.UINumberModel(decisionLight, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._agreesDeclar2 = new NpTypes.UINumberModel(agreesDeclar2, this);
            this._comparisonOper2 = new NpTypes.UINumberModel(comparisonOper2, this);
            this._probabThres2 = new NpTypes.UINumberModel(probabThres2, this);
            this._probabThresSum = new NpTypes.UINumberModel(probabThresSum, this);
            this._comparisonOper3 = new NpTypes.UINumberModel(comparisonOper3, this);
            this._ecccId = new NpTypes.UIManyToOneModel(ecccId, this);
            self.postConstruct();
        }
        EcCultDetailCopyBase.prototype.getKey = function () {
            return this.ecdcId;
        };
        EcCultDetailCopyBase.prototype.getKeyName = function () {
            return "ecdcId";
        };
        EcCultDetailCopyBase.prototype.getEntityName = function () {
            return 'EcCultDetailCopy';
        };
        EcCultDetailCopyBase.prototype.fromJSON = function (data) {
            return Entities.EcCultDetailCopy.fromJSONComplete(data);
        };
        Object.defineProperty(EcCultDetailCopyBase.prototype, "ecdcId", {
            get: function () {
                return this._ecdcId.value;
            },
            set: function (ecdcId) {
                var self = this;
                self._ecdcId.value = ecdcId;
                //    self.ecdcId_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailCopyBase.prototype, "orderingNumber", {
            get: function () {
                return this._orderingNumber.value;
            },
            set: function (orderingNumber) {
                var self = this;
                self._orderingNumber.value = orderingNumber;
                //    self.orderingNumber_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailCopyBase.prototype, "agreesDeclar", {
            get: function () {
                return this._agreesDeclar.value;
            },
            set: function (agreesDeclar) {
                var self = this;
                self._agreesDeclar.value = agreesDeclar;
                //    self.agreesDeclar_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailCopyBase.prototype, "comparisonOper", {
            get: function () {
                return this._comparisonOper.value;
            },
            set: function (comparisonOper) {
                var self = this;
                self._comparisonOper.value = comparisonOper;
                //    self.comparisonOper_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailCopyBase.prototype, "probabThres", {
            get: function () {
                return this._probabThres.value;
            },
            set: function (probabThres) {
                var self = this;
                self._probabThres.value = probabThres;
                //    self.probabThres_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailCopyBase.prototype, "decisionLight", {
            get: function () {
                return this._decisionLight.value;
            },
            set: function (decisionLight) {
                var self = this;
                self._decisionLight.value = decisionLight;
                //    self.decisionLight_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailCopyBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailCopyBase.prototype, "agreesDeclar2", {
            get: function () {
                return this._agreesDeclar2.value;
            },
            set: function (agreesDeclar2) {
                var self = this;
                self._agreesDeclar2.value = agreesDeclar2;
                //    self.agreesDeclar2_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailCopyBase.prototype, "comparisonOper2", {
            get: function () {
                return this._comparisonOper2.value;
            },
            set: function (comparisonOper2) {
                var self = this;
                self._comparisonOper2.value = comparisonOper2;
                //    self.comparisonOper2_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailCopyBase.prototype, "probabThres2", {
            get: function () {
                return this._probabThres2.value;
            },
            set: function (probabThres2) {
                var self = this;
                self._probabThres2.value = probabThres2;
                //    self.probabThres2_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailCopyBase.prototype, "probabThresSum", {
            get: function () {
                return this._probabThresSum.value;
            },
            set: function (probabThresSum) {
                var self = this;
                self._probabThresSum.value = probabThresSum;
                //    self.probabThresSum_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailCopyBase.prototype, "comparisonOper3", {
            get: function () {
                return this._comparisonOper3.value;
            },
            set: function (comparisonOper3) {
                var self = this;
                self._comparisonOper3.value = comparisonOper3;
                //    self.comparisonOper3_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultDetailCopyBase.prototype, "ecccId", {
            get: function () {
                return this._ecccId.value;
            },
            set: function (ecccId) {
                var self = this;
                self._ecccId.value = ecccId;
                //    self.ecccId_listeners.forEach(cb => { cb(<EcCultDetailCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public ecccId_listeners: Array<(c:EcCultDetailCopy) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.ecdcId_listeners.splice(0);
            self.orderingNumber_listeners.splice(0);
            self.agreesDeclar_listeners.splice(0);
            self.comparisonOper_listeners.splice(0);
            self.probabThres_listeners.splice(0);
            self.decisionLight_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.agreesDeclar2_listeners.splice(0);
            self.comparisonOper2_listeners.splice(0);
            self.probabThres2_listeners.splice(0);
            self.probabThresSum_listeners.splice(0);
            self.comparisonOper3_listeners.splice(0);
            self.ecccId_listeners.splice(0);
        }
        */
        EcCultDetailCopyBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "EcCultDetailCopy:" + x.ecdcId;
            var ret = new Entities.EcCultDetailCopy(x.ecdcId, x.orderingNumber, x.agreesDeclar, x.comparisonOper, x.probabThres, x.decisionLight, x.rowVersion, x.agreesDeclar2, x.comparisonOper2, x.probabThres2, x.probabThresSum, x.comparisonOper3, x.ecccId);
            deserializedEntities[key] = ret;
            ret.ecccId = (x.ecccId !== undefined && x.ecccId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.ecccId.$entityName].fromJSONPartial(x.ecccId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        EcCultDetailCopyBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        EcCultDetailCopyBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "EcCultDetailCopy:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "EcCultDetailCopyBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "EcCultDetailCopy:"+x.ecdcId;
                    var cachedCopy = <EcCultDetailCopy>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = EcCultDetailCopy.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = EcCultDetailCopy.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.EcCultDetailCopy.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        EcCultDetailCopyBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.EcCultDetailCopy.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        EcCultDetailCopyBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/EcCultDetailCopy/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.EcCultDetailCopy.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        EcCultDetailCopyBase.prototype.toJSON = function () {
            var ret = {};
            ret.ecdcId = this.ecdcId;
            ret.orderingNumber = this.orderingNumber;
            ret.agreesDeclar = this.agreesDeclar;
            ret.comparisonOper = this.comparisonOper;
            ret.probabThres = this.probabThres;
            ret.decisionLight = this.decisionLight;
            ret.rowVersion = this.rowVersion;
            ret.agreesDeclar2 = this.agreesDeclar2;
            ret.comparisonOper2 = this.comparisonOper2;
            ret.probabThres2 = this.probabThres2;
            ret.probabThresSum = this.probabThresSum;
            ret.comparisonOper3 = this.comparisonOper3;
            ret.ecccId =
                (this.ecccId !== undefined && this.ecccId !== null) ?
                    { ecccId: this.ecccId.ecccId } :
                    (this.ecccId === undefined ? undefined : null);
            return ret;
        };
        EcCultDetailCopyBase.prototype.updateInstance = function (other) {
            var self = this;
            self.ecdcId = other.ecdcId;
            self.orderingNumber = other.orderingNumber;
            self.agreesDeclar = other.agreesDeclar;
            self.comparisonOper = other.comparisonOper;
            self.probabThres = other.probabThres;
            self.decisionLight = other.decisionLight;
            self.rowVersion = other.rowVersion;
            self.agreesDeclar2 = other.agreesDeclar2;
            self.comparisonOper2 = other.comparisonOper2;
            self.probabThres2 = other.probabThres2;
            self.probabThresSum = other.probabThresSum;
            self.comparisonOper3 = other.comparisonOper3;
            self.ecccId = other.ecccId;
        };
        EcCultDetailCopyBase.Create = function () {
            return new Entities.EcCultDetailCopy(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined);
        };
        EcCultDetailCopyBase.CreateById = function (ecdcId) {
            var ret = Entities.EcCultDetailCopy.Create();
            ret.ecdcId = ecdcId;
            return ret;
        };
        EcCultDetailCopyBase.findById_unecrypted = function (ecdcId, $scope, $http, errFunc) {
            if (Entities.EcCultDetailCopy.cashedEntities[ecdcId.toString()] !== undefined)
                return Entities.EcCultDetailCopy.cashedEntities[ecdcId.toString()];
            var wsPath = "EcCultDetailCopy/findByEcdcId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['ecdcId'] = ecdcId;
            var ret = Entities.EcCultDetailCopy.Create();
            Entities.EcCultDetailCopy.cashedEntities[ecdcId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.EcCultDetailCopy.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + ecdcId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        EcCultDetailCopyBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        EcCultDetailCopyBase.cashedEntities = {};
        return EcCultDetailCopyBase;
    })(NpTypes.BaseEntity);
    Entities.EcCultDetailCopyBase = EcCultDetailCopyBase;
    NpTypes.BaseEntity.entitiesFactory['EcCultDetailCopy'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.EcCultDetailCopy.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.EcCultDetailCopy.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=EcCultDetailCopyBase.js.map