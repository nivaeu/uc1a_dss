/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Integrateddecision.ts" />
/// <reference path="../Entities/ParcelDecision.ts" />
/// <reference path="../Entities/ParcelsIssues.ts" />
/// <reference path="../Entities/Classification.ts" />
/// <reference path="../Entities/Cultivation.ts" />
/// <reference path="../Entities/Cultivation.ts" />
/// <reference path="../Entities/CoverType.ts" />
/// <reference path="../Entities/CoverType.ts" />
/// <reference path="../Entities/Cultivation.ts" />
/// <reference path="../Entities/ParcelClas.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var ParcelClasBase = (function (_super) {
        __extends(ParcelClasBase, _super);
        function ParcelClasBase(pclaId, probPred, probPred2, prodCode, parcIdentifier, parcCode, geom4326, clasId, cultIdDecl, cultIdPred, cotyIdDecl, cotyIdPred, cultIdPred2) {
            _super.call(this);
            var self = this;
            this._pclaId = new NpTypes.UIStringModel(pclaId, this);
            this._probPred = new NpTypes.UINumberModel(probPred, this);
            this._probPred2 = new NpTypes.UINumberModel(probPred2, this);
            this._prodCode = new NpTypes.UINumberModel(prodCode, this);
            this._parcIdentifier = new NpTypes.UIStringModel(parcIdentifier, this);
            this._parcCode = new NpTypes.UIStringModel(parcCode, this);
            this._geom4326 = new NpTypes.UIGeoModel(geom4326, this);
            this._clasId = new NpTypes.UIManyToOneModel(clasId, this);
            this._cultIdDecl = new NpTypes.UIManyToOneModel(cultIdDecl, this);
            this._cultIdPred = new NpTypes.UIManyToOneModel(cultIdPred, this);
            this._cotyIdDecl = new NpTypes.UIManyToOneModel(cotyIdDecl, this);
            this._cotyIdPred = new NpTypes.UIManyToOneModel(cotyIdPred, this);
            this._cultIdPred2 = new NpTypes.UIManyToOneModel(cultIdPred2, this);
            self.postConstruct();
        }
        ParcelClasBase.prototype.getKey = function () {
            return this.pclaId;
        };
        ParcelClasBase.prototype.getKeyName = function () {
            return "pclaId";
        };
        ParcelClasBase.prototype.getEntityName = function () {
            return 'ParcelClas';
        };
        ParcelClasBase.prototype.fromJSON = function (data) {
            return Entities.ParcelClas.fromJSONComplete(data);
        };
        Object.defineProperty(ParcelClasBase.prototype, "pclaId", {
            get: function () {
                return this._pclaId.value;
            },
            set: function (pclaId) {
                var self = this;
                self._pclaId.value = pclaId;
                //    self.pclaId_listeners.forEach(cb => { cb(<ParcelClas>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelClasBase.prototype, "probPred", {
            get: function () {
                return this._probPred.value;
            },
            set: function (probPred) {
                var self = this;
                self._probPred.value = probPred;
                //    self.probPred_listeners.forEach(cb => { cb(<ParcelClas>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelClasBase.prototype, "probPred2", {
            get: function () {
                return this._probPred2.value;
            },
            set: function (probPred2) {
                var self = this;
                self._probPred2.value = probPred2;
                //    self.probPred2_listeners.forEach(cb => { cb(<ParcelClas>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelClasBase.prototype, "prodCode", {
            get: function () {
                return this._prodCode.value;
            },
            set: function (prodCode) {
                var self = this;
                self._prodCode.value = prodCode;
                //    self.prodCode_listeners.forEach(cb => { cb(<ParcelClas>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelClasBase.prototype, "parcIdentifier", {
            get: function () {
                return this._parcIdentifier.value;
            },
            set: function (parcIdentifier) {
                var self = this;
                self._parcIdentifier.value = parcIdentifier;
                //    self.parcIdentifier_listeners.forEach(cb => { cb(<ParcelClas>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelClasBase.prototype, "parcCode", {
            get: function () {
                return this._parcCode.value;
            },
            set: function (parcCode) {
                var self = this;
                self._parcCode.value = parcCode;
                //    self.parcCode_listeners.forEach(cb => { cb(<ParcelClas>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelClasBase.prototype, "geom4326", {
            get: function () {
                return this._geom4326.value;
            },
            set: function (geom4326) {
                var self = this;
                self._geom4326.value = geom4326;
                //    self.geom4326_listeners.forEach(cb => { cb(<ParcelClas>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelClasBase.prototype, "clasId", {
            get: function () {
                return this._clasId.value;
            },
            set: function (clasId) {
                var self = this;
                self._clasId.value = clasId;
                //    self.clasId_listeners.forEach(cb => { cb(<ParcelClas>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelClasBase.prototype, "cultIdDecl", {
            get: function () {
                return this._cultIdDecl.value;
            },
            set: function (cultIdDecl) {
                var self = this;
                self._cultIdDecl.value = cultIdDecl;
                //    self.cultIdDecl_listeners.forEach(cb => { cb(<ParcelClas>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelClasBase.prototype, "cultIdPred", {
            get: function () {
                return this._cultIdPred.value;
            },
            set: function (cultIdPred) {
                var self = this;
                self._cultIdPred.value = cultIdPred;
                //    self.cultIdPred_listeners.forEach(cb => { cb(<ParcelClas>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelClasBase.prototype, "cotyIdDecl", {
            get: function () {
                return this._cotyIdDecl.value;
            },
            set: function (cotyIdDecl) {
                var self = this;
                self._cotyIdDecl.value = cotyIdDecl;
                //    self.cotyIdDecl_listeners.forEach(cb => { cb(<ParcelClas>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelClasBase.prototype, "cotyIdPred", {
            get: function () {
                return this._cotyIdPred.value;
            },
            set: function (cotyIdPred) {
                var self = this;
                self._cotyIdPred.value = cotyIdPred;
                //    self.cotyIdPred_listeners.forEach(cb => { cb(<ParcelClas>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelClasBase.prototype, "cultIdPred2", {
            get: function () {
                return this._cultIdPred2.value;
            },
            set: function (cultIdPred2) {
                var self = this;
                self._cultIdPred2.value = cultIdPred2;
                //    self.cultIdPred2_listeners.forEach(cb => { cb(<ParcelClas>self); });
            },
            enumerable: true,
            configurable: true
        });
        ParcelClasBase.prototype.integrateddecisionCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.ParcelClas.integrateddecisionCollection)) {
                if (self instanceof Entities.ParcelClas) {
                    Entities.ParcelClas.integrateddecisionCollection(self, func);
                }
            }
        };
        ParcelClasBase.prototype.parcelDecisionCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.ParcelClas.parcelDecisionCollection)) {
                if (self instanceof Entities.ParcelClas) {
                    Entities.ParcelClas.parcelDecisionCollection(self, func);
                }
            }
        };
        ParcelClasBase.prototype.parcelsIssuesCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.ParcelClas.parcelsIssuesCollection)) {
                if (self instanceof Entities.ParcelClas) {
                    Entities.ParcelClas.parcelsIssuesCollection(self, func);
                }
            }
        };
        /*
        public removeAllListeners() {
            var self = this;
            self.pclaId_listeners.splice(0);
            self.probPred_listeners.splice(0);
            self.probPred2_listeners.splice(0);
            self.prodCode_listeners.splice(0);
            self.parcIdentifier_listeners.splice(0);
            self.parcCode_listeners.splice(0);
            self.geom4326_listeners.splice(0);
            self.clasId_listeners.splice(0);
            self.cultIdDecl_listeners.splice(0);
            self.cultIdPred_listeners.splice(0);
            self.cotyIdDecl_listeners.splice(0);
            self.cotyIdPred_listeners.splice(0);
            self.cultIdPred2_listeners.splice(0);
        }
        */
        ParcelClasBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "ParcelClas:" + x.pclaId;
            var ret = new Entities.ParcelClas(x.pclaId, x.probPred, x.probPred2, x.prodCode, x.parcIdentifier, x.parcCode, x.geom4326, x.clasId, x.cultIdDecl, x.cultIdPred, x.cotyIdDecl, x.cotyIdPred, x.cultIdPred2);
            deserializedEntities[key] = ret;
            ret.clasId = (x.clasId !== undefined && x.clasId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.clasId.$entityName].fromJSONPartial(x.clasId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.cultIdDecl = (x.cultIdDecl !== undefined && x.cultIdDecl !== null) ? NpTypes.BaseEntity.entitiesFactory[x.cultIdDecl.$entityName].fromJSONPartial(x.cultIdDecl, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.cultIdPred = (x.cultIdPred !== undefined && x.cultIdPred !== null) ? NpTypes.BaseEntity.entitiesFactory[x.cultIdPred.$entityName].fromJSONPartial(x.cultIdPred, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.cotyIdDecl = (x.cotyIdDecl !== undefined && x.cotyIdDecl !== null) ? NpTypes.BaseEntity.entitiesFactory[x.cotyIdDecl.$entityName].fromJSONPartial(x.cotyIdDecl, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.cotyIdPred = (x.cotyIdPred !== undefined && x.cotyIdPred !== null) ? NpTypes.BaseEntity.entitiesFactory[x.cotyIdPred.$entityName].fromJSONPartial(x.cotyIdPred, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.cultIdPred2 = (x.cultIdPred2 !== undefined && x.cultIdPred2 !== null) ? NpTypes.BaseEntity.entitiesFactory[x.cultIdPred2.$entityName].fromJSONPartial(x.cultIdPred2, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        ParcelClasBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        ParcelClasBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "ParcelClas:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "ParcelClasBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "ParcelClas:"+x.pclaId;
                    var cachedCopy = <ParcelClas>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = ParcelClas.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = ParcelClas.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.ParcelClas.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        ParcelClasBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.ParcelClas.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        ParcelClasBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/ParcelClas/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.ParcelClas.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        ParcelClasBase.prototype.toJSON = function () {
            var ret = {};
            ret.pclaId = this.pclaId;
            ret.probPred = this.probPred;
            ret.probPred2 = this.probPred2;
            ret.prodCode = this.prodCode;
            ret.parcIdentifier = this.parcIdentifier;
            ret.parcCode = this.parcCode;
            ret.geom4326 = this.geom4326;
            ret.clasId =
                (this.clasId !== undefined && this.clasId !== null) ?
                    { clasId: this.clasId.clasId } :
                    (this.clasId === undefined ? undefined : null);
            ret.cultIdDecl =
                (this.cultIdDecl !== undefined && this.cultIdDecl !== null) ?
                    { cultId: this.cultIdDecl.cultId } :
                    (this.cultIdDecl === undefined ? undefined : null);
            ret.cultIdPred =
                (this.cultIdPred !== undefined && this.cultIdPred !== null) ?
                    { cultId: this.cultIdPred.cultId } :
                    (this.cultIdPred === undefined ? undefined : null);
            ret.cotyIdDecl =
                (this.cotyIdDecl !== undefined && this.cotyIdDecl !== null) ?
                    { cotyId: this.cotyIdDecl.cotyId } :
                    (this.cotyIdDecl === undefined ? undefined : null);
            ret.cotyIdPred =
                (this.cotyIdPred !== undefined && this.cotyIdPred !== null) ?
                    { cotyId: this.cotyIdPred.cotyId } :
                    (this.cotyIdPred === undefined ? undefined : null);
            ret.cultIdPred2 =
                (this.cultIdPred2 !== undefined && this.cultIdPred2 !== null) ?
                    { cultId: this.cultIdPred2.cultId } :
                    (this.cultIdPred2 === undefined ? undefined : null);
            return ret;
        };
        ParcelClasBase.prototype.updateInstance = function (other) {
            var self = this;
            self.pclaId = other.pclaId;
            self.probPred = other.probPred;
            self.probPred2 = other.probPred2;
            self.prodCode = other.prodCode;
            self.parcIdentifier = other.parcIdentifier;
            self.parcCode = other.parcCode;
            self.geom4326 = other.geom4326;
            self.clasId = other.clasId;
            self.cultIdDecl = other.cultIdDecl;
            self.cultIdPred = other.cultIdPred;
            self.cotyIdDecl = other.cotyIdDecl;
            self.cotyIdPred = other.cotyIdPred;
            self.cultIdPred2 = other.cultIdPred2;
        };
        ParcelClasBase.Create = function () {
            return new Entities.ParcelClas(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined);
        };
        ParcelClasBase.CreateById = function (pclaId) {
            var ret = Entities.ParcelClas.Create();
            ret.pclaId = pclaId;
            return ret;
        };
        ParcelClasBase.findById_unecrypted = function (pclaId, $scope, $http, errFunc) {
            if (Entities.ParcelClas.cashedEntities[pclaId.toString()] !== undefined)
                return Entities.ParcelClas.cashedEntities[pclaId.toString()];
            var wsPath = "ParcelClas/findByPclaId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['pclaId'] = pclaId;
            var ret = Entities.ParcelClas.Create();
            Entities.ParcelClas.cashedEntities[pclaId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.ParcelClas.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + pclaId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        ParcelClasBase.prototype.getRowVersion = function () {
            return 0;
        };
        //public cultIdPred2_listeners: Array<(c:ParcelClas) => void> = [];
        ParcelClasBase.integrateddecisionCollection = null; //(parcelClas, list) => { };
        ParcelClasBase.parcelDecisionCollection = null; //(parcelClas, list) => { };
        ParcelClasBase.parcelsIssuesCollection = null; //(parcelClas, list) => { };
        ParcelClasBase.cashedEntities = {};
        return ParcelClasBase;
    })(NpTypes.BaseEntity);
    Entities.ParcelClasBase = ParcelClasBase;
    NpTypes.BaseEntity.entitiesFactory['ParcelClas'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.ParcelClas.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.ParcelClas.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=ParcelClasBase.js.map