/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Statistic.ts" />
/// <reference path="../Entities/ParcelClas.ts" />
/// <reference path="../Entities/DecisionMaking.ts" />
/// <reference path="../Entities/Classifier.ts" />
/// <reference path="../Entities/FileTemplate.ts" />
/// <reference path="../Entities/Classification.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var ClassificationBase = (function (_super) {
        __extends(ClassificationBase, _super);
        function ClassificationBase(clasId, name, description, dateTime, rowVersion, recordtype, filePath, attachedFile, year, isImported, clfrId, fiteId) {
            _super.call(this);
            var self = this;
            this._clasId = new NpTypes.UIStringModel(clasId, this);
            this._name = new NpTypes.UIStringModel(name, this);
            this._description = new NpTypes.UIStringModel(description, this);
            this._dateTime = new NpTypes.UIDateModel(dateTime, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._recordtype = new NpTypes.UINumberModel(recordtype, this);
            this._filePath = new NpTypes.UIStringModel(filePath, this);
            this._attachedFile = new NpTypes.UIBlobModel(attachedFile, function () { return self.filePath; }, function (fileName) { self.filePath = fileName; }, this);
            this._year = new NpTypes.UINumberModel(year, this);
            this._isImported = new NpTypes.UIBoolModel(isImported, this);
            this._clfrId = new NpTypes.UIManyToOneModel(clfrId, this);
            this._fiteId = new NpTypes.UIManyToOneModel(fiteId, this);
            self.postConstruct();
        }
        ClassificationBase.prototype.getKey = function () {
            return this.clasId;
        };
        ClassificationBase.prototype.getKeyName = function () {
            return "clasId";
        };
        ClassificationBase.prototype.getEntityName = function () {
            return 'Classification';
        };
        ClassificationBase.prototype.fromJSON = function (data) {
            return Entities.Classification.fromJSONComplete(data);
        };
        Object.defineProperty(ClassificationBase.prototype, "clasId", {
            get: function () {
                return this._clasId.value;
            },
            set: function (clasId) {
                var self = this;
                self._clasId.value = clasId;
                //    self.clasId_listeners.forEach(cb => { cb(<Classification>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ClassificationBase.prototype, "name", {
            get: function () {
                return this._name.value;
            },
            set: function (name) {
                var self = this;
                self._name.value = name;
                //    self.name_listeners.forEach(cb => { cb(<Classification>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ClassificationBase.prototype, "description", {
            get: function () {
                return this._description.value;
            },
            set: function (description) {
                var self = this;
                self._description.value = description;
                //    self.description_listeners.forEach(cb => { cb(<Classification>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ClassificationBase.prototype, "dateTime", {
            get: function () {
                return this._dateTime.value;
            },
            set: function (dateTime) {
                var self = this;
                self._dateTime.value = dateTime;
                //    self.dateTime_listeners.forEach(cb => { cb(<Classification>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ClassificationBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<Classification>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ClassificationBase.prototype, "recordtype", {
            get: function () {
                return this._recordtype.value;
            },
            set: function (recordtype) {
                var self = this;
                self._recordtype.value = recordtype;
                //    self.recordtype_listeners.forEach(cb => { cb(<Classification>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ClassificationBase.prototype, "filePath", {
            get: function () {
                return this._filePath.value;
            },
            set: function (filePath) {
                var self = this;
                self._filePath.value = filePath;
                //    self.filePath_listeners.forEach(cb => { cb(<Classification>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ClassificationBase.prototype, "attachedFile", {
            get: function () {
                return this._attachedFile.value;
            },
            set: function (attachedFile) {
                var self = this;
                self._attachedFile.value = attachedFile;
                //    self.attachedFile_listeners.forEach(cb => { cb(<Classification>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ClassificationBase.prototype, "year", {
            get: function () {
                return this._year.value;
            },
            set: function (year) {
                var self = this;
                self._year.value = year;
                //    self.year_listeners.forEach(cb => { cb(<Classification>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ClassificationBase.prototype, "isImported", {
            get: function () {
                return this._isImported.value;
            },
            set: function (isImported) {
                var self = this;
                self._isImported.value = isImported;
                //    self.isImported_listeners.forEach(cb => { cb(<Classification>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ClassificationBase.prototype, "clfrId", {
            get: function () {
                return this._clfrId.value;
            },
            set: function (clfrId) {
                var self = this;
                self._clfrId.value = clfrId;
                //    self.clfrId_listeners.forEach(cb => { cb(<Classification>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ClassificationBase.prototype, "fiteId", {
            get: function () {
                return this._fiteId.value;
            },
            set: function (fiteId) {
                var self = this;
                self._fiteId.value = fiteId;
                //    self.fiteId_listeners.forEach(cb => { cb(<Classification>self); });
            },
            enumerable: true,
            configurable: true
        });
        ClassificationBase.prototype.statisticCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.Classification.statisticCollection)) {
                if (self instanceof Entities.Classification) {
                    Entities.Classification.statisticCollection(self, func);
                }
            }
        };
        ClassificationBase.prototype.parcelClasCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.Classification.parcelClasCollection)) {
                if (self instanceof Entities.Classification) {
                    Entities.Classification.parcelClasCollection(self, func);
                }
            }
        };
        ClassificationBase.prototype.decisionMakingCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.Classification.decisionMakingCollection)) {
                if (self instanceof Entities.Classification) {
                    Entities.Classification.decisionMakingCollection(self, func);
                }
            }
        };
        /*
        public removeAllListeners() {
            var self = this;
            self.clasId_listeners.splice(0);
            self.name_listeners.splice(0);
            self.description_listeners.splice(0);
            self.dateTime_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.recordtype_listeners.splice(0);
            self.filePath_listeners.splice(0);
            self.attachedFile_listeners.splice(0);
            self.year_listeners.splice(0);
            self.isImported_listeners.splice(0);
            self.clfrId_listeners.splice(0);
            self.fiteId_listeners.splice(0);
        }
        */
        ClassificationBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "Classification:" + x.clasId;
            var ret = new Entities.Classification(x.clasId, x.name, x.description, isVoid(x.dateTime) ? null : new Date(x.dateTime), x.rowVersion, x.recordtype, x.filePath, x.attachedFile, x.year, x.isImported, x.clfrId, x.fiteId);
            deserializedEntities[key] = ret;
            ret.clfrId = (x.clfrId !== undefined && x.clfrId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.clfrId.$entityName].fromJSONPartial(x.clfrId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.fiteId = (x.fiteId !== undefined && x.fiteId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.fiteId.$entityName].fromJSONPartial(x.fiteId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        ClassificationBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        ClassificationBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "Classification:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "ClassificationBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "Classification:"+x.clasId;
                    var cachedCopy = <Classification>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = Classification.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = Classification.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.Classification.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        ClassificationBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.Classification.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        ClassificationBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/Classification/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.Classification.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        ClassificationBase.prototype.toJSON = function () {
            var ret = {};
            ret.clasId = this.clasId;
            ret.name = this.name;
            ret.description = this.description;
            ret.dateTime = this.dateTime;
            ret.rowVersion = this.rowVersion;
            ret.recordtype = this.recordtype;
            ret.filePath = this.filePath;
            ret.attachedFile = this.attachedFile;
            ret.year = this.year;
            ret.isImported = this.isImported;
            ret.clfrId =
                (this.clfrId !== undefined && this.clfrId !== null) ?
                    { clfrId: this.clfrId.clfrId } :
                    (this.clfrId === undefined ? undefined : null);
            ret.fiteId =
                (this.fiteId !== undefined && this.fiteId !== null) ?
                    { fiteId: this.fiteId.fiteId } :
                    (this.fiteId === undefined ? undefined : null);
            return ret;
        };
        ClassificationBase.prototype.updateInstance = function (other) {
            var self = this;
            self.clasId = other.clasId;
            self.name = other.name;
            self.description = other.description;
            self.dateTime = other.dateTime;
            self.rowVersion = other.rowVersion;
            self.recordtype = other.recordtype;
            self.filePath = other.filePath;
            self.attachedFile = other.attachedFile;
            self.year = other.year;
            self.isImported = other.isImported;
            self.clfrId = other.clfrId;
            self.fiteId = other.fiteId;
        };
        ClassificationBase.Create = function () {
            return new Entities.Classification(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined);
        };
        ClassificationBase.CreateById = function (clasId) {
            var ret = Entities.Classification.Create();
            ret.clasId = clasId;
            return ret;
        };
        ClassificationBase.findById_unecrypted = function (clasId, $scope, $http, errFunc) {
            if (Entities.Classification.cashedEntities[clasId.toString()] !== undefined)
                return Entities.Classification.cashedEntities[clasId.toString()];
            var wsPath = "Classification/findByClasId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['clasId'] = clasId;
            var ret = Entities.Classification.Create();
            Entities.Classification.cashedEntities[clasId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.Classification.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + clasId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        ClassificationBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        //public fiteId_listeners: Array<(c:Classification) => void> = [];
        ClassificationBase.statisticCollection = null; //(classification, list) => { };
        ClassificationBase.parcelClasCollection = null; //(classification, list) => { };
        ClassificationBase.decisionMakingCollection = null; //(classification, list) => { };
        ClassificationBase.cashedEntities = {};
        return ClassificationBase;
    })(NpTypes.BaseEntity);
    Entities.ClassificationBase = ClassificationBase;
    NpTypes.BaseEntity.entitiesFactory['Classification'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.Classification.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.Classification.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=ClassificationBase.js.map