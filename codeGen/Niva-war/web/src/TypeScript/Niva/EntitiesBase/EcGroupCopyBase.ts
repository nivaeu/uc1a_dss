/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/EcCultCopy.ts" />
/// <reference path="../Entities/DecisionMaking.ts" />
/// <reference path="../Entities/EcGroupCopy.ts" />

module Entities {

    export class EcGroupCopyBase extends NpTypes.BaseEntity {
        constructor(
            ecgcId: string,
            description: string,
            name: string,
            rowVersion: number,
            recordtype: number,
            cropLevel: number,
            demaId: DecisionMaking) 
        {
            super();
            var self = this;
            this._ecgcId = new NpTypes.UIStringModel(ecgcId, this);
            this._description = new NpTypes.UIStringModel(description, this);
            this._name = new NpTypes.UIStringModel(name, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._recordtype = new NpTypes.UINumberModel(recordtype, this);
            this._cropLevel = new NpTypes.UINumberModel(cropLevel, this);
            this._demaId = new NpTypes.UIManyToOneModel<Entities.DecisionMaking>(demaId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.ecgcId; 
        }
        
        public getKeyName(): string { 
            return "ecgcId"; 
        }

        public getEntityName(): string { 
            return 'EcGroupCopy'; 
        }
        
        public fromJSON(data: any): EcGroupCopy[] { 
            return EcGroupCopy.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.ecgcId === null || this.ecgcId === undefined)
                return true;
            if (this.ecgcId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: EcGroupCopyBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //ecgcId property
        _ecgcId: NpTypes.UIStringModel;
        public get ecgcId():string {
            return this._ecgcId.value;
        }
        public set ecgcId(ecgcId:string) {
            var self = this;
            self._ecgcId.value = ecgcId;
        //    self.ecgcId_listeners.forEach(cb => { cb(<EcGroupCopy>self); });
        }
        //public ecgcId_listeners: Array<(c:EcGroupCopy) => void> = [];
        //description property
        _description: NpTypes.UIStringModel;
        public get description():string {
            return this._description.value;
        }
        public set description(description:string) {
            var self = this;
            self._description.value = description;
        //    self.description_listeners.forEach(cb => { cb(<EcGroupCopy>self); });
        }
        //public description_listeners: Array<(c:EcGroupCopy) => void> = [];
        //name property
        _name: NpTypes.UIStringModel;
        public get name():string {
            return this._name.value;
        }
        public set name(name:string) {
            var self = this;
            self._name.value = name;
        //    self.name_listeners.forEach(cb => { cb(<EcGroupCopy>self); });
        }
        //public name_listeners: Array<(c:EcGroupCopy) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<EcGroupCopy>self); });
        }
        //public rowVersion_listeners: Array<(c:EcGroupCopy) => void> = [];
        //recordtype property
        _recordtype: NpTypes.UINumberModel;
        public get recordtype():number {
            return this._recordtype.value;
        }
        public set recordtype(recordtype:number) {
            var self = this;
            self._recordtype.value = recordtype;
        //    self.recordtype_listeners.forEach(cb => { cb(<EcGroupCopy>self); });
        }
        //public recordtype_listeners: Array<(c:EcGroupCopy) => void> = [];
        //cropLevel property
        _cropLevel: NpTypes.UINumberModel;
        public get cropLevel():number {
            return this._cropLevel.value;
        }
        public set cropLevel(cropLevel:number) {
            var self = this;
            self._cropLevel.value = cropLevel;
        //    self.cropLevel_listeners.forEach(cb => { cb(<EcGroupCopy>self); });
        }
        //public cropLevel_listeners: Array<(c:EcGroupCopy) => void> = [];
        //demaId property
        _demaId: NpTypes.UIManyToOneModel<Entities.DecisionMaking>;
        public get demaId():DecisionMaking {
            return this._demaId.value;
        }
        public set demaId(demaId:DecisionMaking) {
            var self = this;
            self._demaId.value = demaId;
        //    self.demaId_listeners.forEach(cb => { cb(<EcGroupCopy>self); });
        }
        //public demaId_listeners: Array<(c:EcGroupCopy) => void> = [];
        public static ecCultCopyCollection: (self:Entities.EcGroupCopy, func: (ecCultCopyList: Entities.EcCultCopy[]) => void) => void = null; //(ecGroupCopy, list) => { };
        public ecCultCopyCollection(func: (ecCultCopyList: Entities.EcCultCopy[]) => void) {
            var self: Entities.EcGroupCopyBase = this;
        	if (!isVoid(EcGroupCopy.ecCultCopyCollection)) {
        		if (self instanceof Entities.EcGroupCopy) {
        			EcGroupCopy.ecCultCopyCollection(<Entities.EcGroupCopy>self, func);
                }
            }
        }
        /*
        public removeAllListeners() {
            var self = this;
            self.ecgcId_listeners.splice(0);
            self.description_listeners.splice(0);
            self.name_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.recordtype_listeners.splice(0);
            self.cropLevel_listeners.splice(0);
            self.demaId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : EcGroupCopy {
            var key = "EcGroupCopy:"+x.ecgcId;
            var ret =  new EcGroupCopy(
                x.ecgcId,
                x.description,
                x.name,
                x.rowVersion,
                x.recordtype,
                x.cropLevel,
                x.demaId
            );
            deserializedEntities[key] = ret;
            ret.demaId = (x.demaId !== undefined && x.demaId !== null) ? <DecisionMaking>NpTypes.BaseEntity.entitiesFactory[x.demaId.$entityName].fromJSONPartial(x.demaId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): EcGroupCopy {
            
            return <EcGroupCopy>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : EcGroupCopy {
            var self = this;
            var key="";
            var ret:EcGroupCopy = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "EcGroupCopy:"+x.$refId;
                ret = <EcGroupCopy>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "EcGroupCopyBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "EcGroupCopy:"+x.ecgcId;
                    var cachedCopy = <EcGroupCopy>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = EcGroupCopy.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = EcGroupCopy.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = EcGroupCopy.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<EcGroupCopy> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<EcGroupCopy> = _.map(data, (x:any):EcGroupCopy => {
                return EcGroupCopy.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:EcGroupCopy[])=>void)  {
            var url = "/Niva/rest/EcGroupCopy/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = EcGroupCopy.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.ecgcId = this.ecgcId;
                ret.description = this.description;
                ret.name = this.name;
                ret.rowVersion = this.rowVersion;
                ret.recordtype = this.recordtype;
                ret.cropLevel = this.cropLevel;
                ret.demaId = 
                    (this.demaId !== undefined && this.demaId !== null) ? 
                        { demaId :  this.demaId.demaId} :
                        (this.demaId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:EcGroupCopy):void {
            var self = this;
            self.ecgcId = other.ecgcId
            self.description = other.description
            self.name = other.name
            self.rowVersion = other.rowVersion
            self.recordtype = other.recordtype
            self.cropLevel = other.cropLevel
            self.demaId = other.demaId
        }

        public static Create() : EcGroupCopy {
            return new EcGroupCopy(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(ecgcId:string) : EcGroupCopy {
            var ret = EcGroupCopy.Create();
            ret.ecgcId = ecgcId;
            return ret;
        }

        public static cashedEntities: { [id: string]: EcGroupCopy; } = {};
        public static findById_unecrypted(ecgcId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : EcGroupCopy {
            if (Entities.EcGroupCopy.cashedEntities[ecgcId.toString()] !== undefined)
                return Entities.EcGroupCopy.cashedEntities[ecgcId.toString()];

            var wsPath = "EcGroupCopy/findByEcgcId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['ecgcId'] = ecgcId;
            var ret = EcGroupCopy.Create();
            Entities.EcGroupCopy.cashedEntities[ecgcId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.EcGroupCopy.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + ecgcId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['EcGroupCopy'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.EcGroupCopy.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.EcGroupCopy.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


}