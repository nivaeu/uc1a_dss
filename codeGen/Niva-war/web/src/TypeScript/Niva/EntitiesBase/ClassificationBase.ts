/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Statistic.ts" />
/// <reference path="../Entities/ParcelClas.ts" />
/// <reference path="../Entities/DecisionMaking.ts" />
/// <reference path="../Entities/Classifier.ts" />
/// <reference path="../Entities/FileTemplate.ts" />
/// <reference path="../Entities/Classification.ts" />

module Entities {

    export class ClassificationBase extends NpTypes.BaseEntity {
        constructor(
            clasId: string,
            name: string,
            description: string,
            dateTime: Date,
            rowVersion: number,
            recordtype: number,
            filePath: string,
            attachedFile: string,
            year: number,
            isImported: boolean,
            clfrId: Classifier,
            fiteId: FileTemplate) 
        {
            super();
            var self = this;
            this._clasId = new NpTypes.UIStringModel(clasId, this);
            this._name = new NpTypes.UIStringModel(name, this);
            this._description = new NpTypes.UIStringModel(description, this);
            this._dateTime = new NpTypes.UIDateModel(dateTime, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._recordtype = new NpTypes.UINumberModel(recordtype, this);
            this._filePath = new NpTypes.UIStringModel(filePath, this);
            this._attachedFile = new NpTypes.UIBlobModel(
                attachedFile, 
                ()  => self.filePath,
                (fileName:string)   => {self.filePath = fileName},
                this);
            this._year = new NpTypes.UINumberModel(year, this);
            this._isImported = new NpTypes.UIBoolModel(isImported, this);
            this._clfrId = new NpTypes.UIManyToOneModel<Entities.Classifier>(clfrId, this);
            this._fiteId = new NpTypes.UIManyToOneModel<Entities.FileTemplate>(fiteId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.clasId; 
        }
        
        public getKeyName(): string { 
            return "clasId"; 
        }

        public getEntityName(): string { 
            return 'Classification'; 
        }
        
        public fromJSON(data: any): Classification[] { 
            return Classification.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.clasId === null || this.clasId === undefined)
                return true;
            if (this.clasId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: ClassificationBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //clasId property
        _clasId: NpTypes.UIStringModel;
        public get clasId():string {
            return this._clasId.value;
        }
        public set clasId(clasId:string) {
            var self = this;
            self._clasId.value = clasId;
        //    self.clasId_listeners.forEach(cb => { cb(<Classification>self); });
        }
        //public clasId_listeners: Array<(c:Classification) => void> = [];
        //name property
        _name: NpTypes.UIStringModel;
        public get name():string {
            return this._name.value;
        }
        public set name(name:string) {
            var self = this;
            self._name.value = name;
        //    self.name_listeners.forEach(cb => { cb(<Classification>self); });
        }
        //public name_listeners: Array<(c:Classification) => void> = [];
        //description property
        _description: NpTypes.UIStringModel;
        public get description():string {
            return this._description.value;
        }
        public set description(description:string) {
            var self = this;
            self._description.value = description;
        //    self.description_listeners.forEach(cb => { cb(<Classification>self); });
        }
        //public description_listeners: Array<(c:Classification) => void> = [];
        //dateTime property
        _dateTime: NpTypes.UIDateModel;
        public get dateTime():Date {
            return this._dateTime.value;
        }
        public set dateTime(dateTime:Date) {
            var self = this;
            self._dateTime.value = dateTime;
        //    self.dateTime_listeners.forEach(cb => { cb(<Classification>self); });
        }
        //public dateTime_listeners: Array<(c:Classification) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<Classification>self); });
        }
        //public rowVersion_listeners: Array<(c:Classification) => void> = [];
        //recordtype property
        _recordtype: NpTypes.UINumberModel;
        public get recordtype():number {
            return this._recordtype.value;
        }
        public set recordtype(recordtype:number) {
            var self = this;
            self._recordtype.value = recordtype;
        //    self.recordtype_listeners.forEach(cb => { cb(<Classification>self); });
        }
        //public recordtype_listeners: Array<(c:Classification) => void> = [];
        //filePath property
        _filePath: NpTypes.UIStringModel;
        public get filePath():string {
            return this._filePath.value;
        }
        public set filePath(filePath:string) {
            var self = this;
            self._filePath.value = filePath;
        //    self.filePath_listeners.forEach(cb => { cb(<Classification>self); });
        }
        //public filePath_listeners: Array<(c:Classification) => void> = [];
        //attachedFile property
        _attachedFile: NpTypes.UIBlobModel;
        public get attachedFile():string {
            return this._attachedFile.value;
        }
        public set attachedFile(attachedFile:string) {
            var self = this;
            self._attachedFile.value = attachedFile;
        //    self.attachedFile_listeners.forEach(cb => { cb(<Classification>self); });
        }
        //public attachedFile_listeners: Array<(c:Classification) => void> = [];
        //year property
        _year: NpTypes.UINumberModel;
        public get year():number {
            return this._year.value;
        }
        public set year(year:number) {
            var self = this;
            self._year.value = year;
        //    self.year_listeners.forEach(cb => { cb(<Classification>self); });
        }
        //public year_listeners: Array<(c:Classification) => void> = [];
        //isImported property
        _isImported: NpTypes.UIBoolModel;
        public get isImported():boolean {
            return this._isImported.value;
        }
        public set isImported(isImported:boolean) {
            var self = this;
            self._isImported.value = isImported;
        //    self.isImported_listeners.forEach(cb => { cb(<Classification>self); });
        }
        //public isImported_listeners: Array<(c:Classification) => void> = [];
        //clfrId property
        _clfrId: NpTypes.UIManyToOneModel<Entities.Classifier>;
        public get clfrId():Classifier {
            return this._clfrId.value;
        }
        public set clfrId(clfrId:Classifier) {
            var self = this;
            self._clfrId.value = clfrId;
        //    self.clfrId_listeners.forEach(cb => { cb(<Classification>self); });
        }
        //public clfrId_listeners: Array<(c:Classification) => void> = [];
        //fiteId property
        _fiteId: NpTypes.UIManyToOneModel<Entities.FileTemplate>;
        public get fiteId():FileTemplate {
            return this._fiteId.value;
        }
        public set fiteId(fiteId:FileTemplate) {
            var self = this;
            self._fiteId.value = fiteId;
        //    self.fiteId_listeners.forEach(cb => { cb(<Classification>self); });
        }
        //public fiteId_listeners: Array<(c:Classification) => void> = [];
        public static statisticCollection: (self:Entities.Classification, func: (statisticList: Entities.Statistic[]) => void) => void = null; //(classification, list) => { };
        public statisticCollection(func: (statisticList: Entities.Statistic[]) => void) {
            var self: Entities.ClassificationBase = this;
        	if (!isVoid(Classification.statisticCollection)) {
        		if (self instanceof Entities.Classification) {
        			Classification.statisticCollection(<Entities.Classification>self, func);
                }
            }
        }
        public static parcelClasCollection: (self:Entities.Classification, func: (parcelClasList: Entities.ParcelClas[]) => void) => void = null; //(classification, list) => { };
        public parcelClasCollection(func: (parcelClasList: Entities.ParcelClas[]) => void) {
            var self: Entities.ClassificationBase = this;
        	if (!isVoid(Classification.parcelClasCollection)) {
        		if (self instanceof Entities.Classification) {
        			Classification.parcelClasCollection(<Entities.Classification>self, func);
                }
            }
        }
        public static decisionMakingCollection: (self:Entities.Classification, func: (decisionMakingList: Entities.DecisionMaking[]) => void) => void = null; //(classification, list) => { };
        public decisionMakingCollection(func: (decisionMakingList: Entities.DecisionMaking[]) => void) {
            var self: Entities.ClassificationBase = this;
        	if (!isVoid(Classification.decisionMakingCollection)) {
        		if (self instanceof Entities.Classification) {
        			Classification.decisionMakingCollection(<Entities.Classification>self, func);
                }
            }
        }
        /*
        public removeAllListeners() {
            var self = this;
            self.clasId_listeners.splice(0);
            self.name_listeners.splice(0);
            self.description_listeners.splice(0);
            self.dateTime_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.recordtype_listeners.splice(0);
            self.filePath_listeners.splice(0);
            self.attachedFile_listeners.splice(0);
            self.year_listeners.splice(0);
            self.isImported_listeners.splice(0);
            self.clfrId_listeners.splice(0);
            self.fiteId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : Classification {
            var key = "Classification:"+x.clasId;
            var ret =  new Classification(
                x.clasId,
                x.name,
                x.description,
                isVoid(x.dateTime) ? null : new Date(x.dateTime),
                x.rowVersion,
                x.recordtype,
                x.filePath,
                x.attachedFile,
                x.year,
                x.isImported,
                x.clfrId,
                x.fiteId
            );
            deserializedEntities[key] = ret;
            ret.clfrId = (x.clfrId !== undefined && x.clfrId !== null) ? <Classifier>NpTypes.BaseEntity.entitiesFactory[x.clfrId.$entityName].fromJSONPartial(x.clfrId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.fiteId = (x.fiteId !== undefined && x.fiteId !== null) ? <FileTemplate>NpTypes.BaseEntity.entitiesFactory[x.fiteId.$entityName].fromJSONPartial(x.fiteId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): Classification {
            
            return <Classification>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : Classification {
            var self = this;
            var key="";
            var ret:Classification = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "Classification:"+x.$refId;
                ret = <Classification>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "ClassificationBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "Classification:"+x.clasId;
                    var cachedCopy = <Classification>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = Classification.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = Classification.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Classification.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Classification> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<Classification> = _.map(data, (x:any):Classification => {
                return Classification.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:Classification[])=>void)  {
            var url = "/Niva/rest/Classification/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = Classification.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.clasId = this.clasId;
                ret.name = this.name;
                ret.description = this.description;
                ret.dateTime = this.dateTime;
                ret.rowVersion = this.rowVersion;
                ret.recordtype = this.recordtype;
                ret.filePath = this.filePath;
                ret.attachedFile = this.attachedFile;
                ret.year = this.year;
                ret.isImported = this.isImported;
                ret.clfrId = 
                    (this.clfrId !== undefined && this.clfrId !== null) ? 
                        { clfrId :  this.clfrId.clfrId} :
                        (this.clfrId === undefined ? undefined : null);
                ret.fiteId = 
                    (this.fiteId !== undefined && this.fiteId !== null) ? 
                        { fiteId :  this.fiteId.fiteId} :
                        (this.fiteId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:Classification):void {
            var self = this;
            self.clasId = other.clasId
            self.name = other.name
            self.description = other.description
            self.dateTime = other.dateTime
            self.rowVersion = other.rowVersion
            self.recordtype = other.recordtype
            self.filePath = other.filePath
            self.attachedFile = other.attachedFile
            self.year = other.year
            self.isImported = other.isImported
            self.clfrId = other.clfrId
            self.fiteId = other.fiteId
        }

        public static Create() : Classification {
            return new Classification(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(clasId:string) : Classification {
            var ret = Classification.Create();
            ret.clasId = clasId;
            return ret;
        }

        public static cashedEntities: { [id: string]: Classification; } = {};
        public static findById_unecrypted(clasId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : Classification {
            if (Entities.Classification.cashedEntities[clasId.toString()] !== undefined)
                return Entities.Classification.cashedEntities[clasId.toString()];

            var wsPath = "Classification/findByClasId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['clasId'] = clasId;
            var ret = Classification.Create();
            Entities.Classification.cashedEntities[clasId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.Classification.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + clasId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['Classification'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.Classification.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.Classification.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


}