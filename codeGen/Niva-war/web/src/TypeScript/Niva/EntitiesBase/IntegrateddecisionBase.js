/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/ParcelClas.ts" />
/// <reference path="../Entities/DecisionMaking.ts" />
/// <reference path="../Entities/Integrateddecision.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var IntegrateddecisionBase = (function (_super) {
        __extends(IntegrateddecisionBase, _super);
        function IntegrateddecisionBase(integrateddecisionsId, decisionCode, dteUpdate, usrUpdate, pclaId, demaId) {
            _super.call(this);
            var self = this;
            this._integrateddecisionsId = new NpTypes.UIStringModel(integrateddecisionsId, this);
            this._decisionCode = new NpTypes.UINumberModel(decisionCode, this);
            this._dteUpdate = new NpTypes.UIDateModel(dteUpdate, this);
            this._usrUpdate = new NpTypes.UIStringModel(usrUpdate, this);
            this._pclaId = new NpTypes.UIManyToOneModel(pclaId, this);
            this._demaId = new NpTypes.UIManyToOneModel(demaId, this);
            self.postConstruct();
        }
        IntegrateddecisionBase.prototype.getKey = function () {
            return this.integrateddecisionsId;
        };
        IntegrateddecisionBase.prototype.getKeyName = function () {
            return "integrateddecisionsId";
        };
        IntegrateddecisionBase.prototype.getEntityName = function () {
            return 'Integrateddecision';
        };
        IntegrateddecisionBase.prototype.fromJSON = function (data) {
            return Entities.Integrateddecision.fromJSONComplete(data);
        };
        Object.defineProperty(IntegrateddecisionBase.prototype, "integrateddecisionsId", {
            get: function () {
                return this._integrateddecisionsId.value;
            },
            set: function (integrateddecisionsId) {
                var self = this;
                self._integrateddecisionsId.value = integrateddecisionsId;
                //    self.integrateddecisionsId_listeners.forEach(cb => { cb(<Integrateddecision>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(IntegrateddecisionBase.prototype, "decisionCode", {
            get: function () {
                return this._decisionCode.value;
            },
            set: function (decisionCode) {
                var self = this;
                self._decisionCode.value = decisionCode;
                //    self.decisionCode_listeners.forEach(cb => { cb(<Integrateddecision>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(IntegrateddecisionBase.prototype, "dteUpdate", {
            get: function () {
                return this._dteUpdate.value;
            },
            set: function (dteUpdate) {
                var self = this;
                self._dteUpdate.value = dteUpdate;
                //    self.dteUpdate_listeners.forEach(cb => { cb(<Integrateddecision>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(IntegrateddecisionBase.prototype, "usrUpdate", {
            get: function () {
                return this._usrUpdate.value;
            },
            set: function (usrUpdate) {
                var self = this;
                self._usrUpdate.value = usrUpdate;
                //    self.usrUpdate_listeners.forEach(cb => { cb(<Integrateddecision>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(IntegrateddecisionBase.prototype, "pclaId", {
            get: function () {
                return this._pclaId.value;
            },
            set: function (pclaId) {
                var self = this;
                self._pclaId.value = pclaId;
                //    self.pclaId_listeners.forEach(cb => { cb(<Integrateddecision>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(IntegrateddecisionBase.prototype, "demaId", {
            get: function () {
                return this._demaId.value;
            },
            set: function (demaId) {
                var self = this;
                self._demaId.value = demaId;
                //    self.demaId_listeners.forEach(cb => { cb(<Integrateddecision>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public demaId_listeners: Array<(c:Integrateddecision) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.integrateddecisionsId_listeners.splice(0);
            self.decisionCode_listeners.splice(0);
            self.dteUpdate_listeners.splice(0);
            self.usrUpdate_listeners.splice(0);
            self.pclaId_listeners.splice(0);
            self.demaId_listeners.splice(0);
        }
        */
        IntegrateddecisionBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "Integrateddecision:" + x.integrateddecisionsId;
            var ret = new Entities.Integrateddecision(x.integrateddecisionsId, x.decisionCode, isVoid(x.dteUpdate) ? null : new Date(x.dteUpdate), x.usrUpdate, x.pclaId, x.demaId);
            deserializedEntities[key] = ret;
            ret.pclaId = (x.pclaId !== undefined && x.pclaId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.pclaId.$entityName].fromJSONPartial(x.pclaId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.demaId = (x.demaId !== undefined && x.demaId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.demaId.$entityName].fromJSONPartial(x.demaId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        IntegrateddecisionBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        IntegrateddecisionBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "Integrateddecision:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "IntegrateddecisionBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "Integrateddecision:"+x.integrateddecisionsId;
                    var cachedCopy = <Integrateddecision>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = Integrateddecision.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = Integrateddecision.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.Integrateddecision.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        IntegrateddecisionBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.Integrateddecision.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        IntegrateddecisionBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/Integrateddecision/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.Integrateddecision.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        IntegrateddecisionBase.prototype.toJSON = function () {
            var ret = {};
            ret.integrateddecisionsId = this.integrateddecisionsId;
            ret.decisionCode = this.decisionCode;
            ret.dteUpdate = this.dteUpdate;
            ret.usrUpdate = this.usrUpdate;
            ret.pclaId =
                (this.pclaId !== undefined && this.pclaId !== null) ?
                    { pclaId: this.pclaId.pclaId } :
                    (this.pclaId === undefined ? undefined : null);
            ret.demaId =
                (this.demaId !== undefined && this.demaId !== null) ?
                    { demaId: this.demaId.demaId } :
                    (this.demaId === undefined ? undefined : null);
            return ret;
        };
        IntegrateddecisionBase.prototype.updateInstance = function (other) {
            var self = this;
            self.integrateddecisionsId = other.integrateddecisionsId;
            self.decisionCode = other.decisionCode;
            self.dteUpdate = other.dteUpdate;
            self.usrUpdate = other.usrUpdate;
            self.pclaId = other.pclaId;
            self.demaId = other.demaId;
        };
        IntegrateddecisionBase.Create = function () {
            return new Entities.Integrateddecision(undefined, undefined, undefined, undefined, undefined, undefined);
        };
        IntegrateddecisionBase.CreateById = function (integrateddecisionsId) {
            var ret = Entities.Integrateddecision.Create();
            ret.integrateddecisionsId = integrateddecisionsId;
            return ret;
        };
        IntegrateddecisionBase.findById_unecrypted = function (integrateddecisionsId, $scope, $http, errFunc) {
            if (Entities.Integrateddecision.cashedEntities[integrateddecisionsId.toString()] !== undefined)
                return Entities.Integrateddecision.cashedEntities[integrateddecisionsId.toString()];
            var wsPath = "Integrateddecision/findByIntegrateddecisionsId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['integrateddecisionsId'] = integrateddecisionsId;
            var ret = Entities.Integrateddecision.Create();
            Entities.Integrateddecision.cashedEntities[integrateddecisionsId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.Integrateddecision.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + integrateddecisionsId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        IntegrateddecisionBase.prototype.getRowVersion = function () {
            return 0;
        };
        IntegrateddecisionBase.cashedEntities = {};
        return IntegrateddecisionBase;
    })(NpTypes.BaseEntity);
    Entities.IntegrateddecisionBase = IntegrateddecisionBase;
    NpTypes.BaseEntity.entitiesFactory['Integrateddecision'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.Integrateddecision.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.Integrateddecision.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=IntegrateddecisionBase.js.map