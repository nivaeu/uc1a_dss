/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Dbmanagementlog.ts" />

module Entities {

    export class DbmanagementlogBase extends NpTypes.BaseEntity {
        constructor(
            dblid: string,
            dteDone: Date,
            version: string,
            notes: string,
            type: number) 
        {
            super();
            var self = this;
            this._dblid = new NpTypes.UIStringModel(dblid, this);
            this._dteDone = new NpTypes.UIDateModel(dteDone, this);
            this._version = new NpTypes.UIStringModel(version, this);
            this._notes = new NpTypes.UIStringModel(notes, this);
            this._type = new NpTypes.UINumberModel(type, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.dblid; 
        }
        
        public getKeyName(): string { 
            return "dblid"; 
        }

        public getEntityName(): string { 
            return 'Dbmanagementlog'; 
        }
        
        public fromJSON(data: any): Dbmanagementlog[] { 
            return Dbmanagementlog.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.dblid === null || this.dblid === undefined)
                return true;
            if (this.dblid.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: DbmanagementlogBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //dblid property
        _dblid: NpTypes.UIStringModel;
        public get dblid():string {
            return this._dblid.value;
        }
        public set dblid(dblid:string) {
            var self = this;
            self._dblid.value = dblid;
        //    self.dblid_listeners.forEach(cb => { cb(<Dbmanagementlog>self); });
        }
        //public dblid_listeners: Array<(c:Dbmanagementlog) => void> = [];
        //dteDone property
        _dteDone: NpTypes.UIDateModel;
        public get dteDone():Date {
            return this._dteDone.value;
        }
        public set dteDone(dteDone:Date) {
            var self = this;
            self._dteDone.value = dteDone;
        //    self.dteDone_listeners.forEach(cb => { cb(<Dbmanagementlog>self); });
        }
        //public dteDone_listeners: Array<(c:Dbmanagementlog) => void> = [];
        //version property
        _version: NpTypes.UIStringModel;
        public get version():string {
            return this._version.value;
        }
        public set version(version:string) {
            var self = this;
            self._version.value = version;
        //    self.version_listeners.forEach(cb => { cb(<Dbmanagementlog>self); });
        }
        //public version_listeners: Array<(c:Dbmanagementlog) => void> = [];
        //notes property
        _notes: NpTypes.UIStringModel;
        public get notes():string {
            return this._notes.value;
        }
        public set notes(notes:string) {
            var self = this;
            self._notes.value = notes;
        //    self.notes_listeners.forEach(cb => { cb(<Dbmanagementlog>self); });
        }
        //public notes_listeners: Array<(c:Dbmanagementlog) => void> = [];
        //type property
        _type: NpTypes.UINumberModel;
        public get type():number {
            return this._type.value;
        }
        public set type(type:number) {
            var self = this;
            self._type.value = type;
        //    self.type_listeners.forEach(cb => { cb(<Dbmanagementlog>self); });
        }
        //public type_listeners: Array<(c:Dbmanagementlog) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.dblid_listeners.splice(0);
            self.dteDone_listeners.splice(0);
            self.version_listeners.splice(0);
            self.notes_listeners.splice(0);
            self.type_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : Dbmanagementlog {
            var key = "Dbmanagementlog:"+x.dblid;
            var ret =  new Dbmanagementlog(
                x.dblid,
                isVoid(x.dteDone) ? null : new Date(x.dteDone),
                x.version,
                x.notes,
                x.type
            );
            deserializedEntities[key] = ret;
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): Dbmanagementlog {
            
            return <Dbmanagementlog>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : Dbmanagementlog {
            var self = this;
            var key="";
            var ret:Dbmanagementlog = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "Dbmanagementlog:"+x.$refId;
                ret = <Dbmanagementlog>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "DbmanagementlogBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "Dbmanagementlog:"+x.dblid;
                    var cachedCopy = <Dbmanagementlog>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = Dbmanagementlog.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = Dbmanagementlog.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Dbmanagementlog.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Dbmanagementlog> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<Dbmanagementlog> = _.map(data, (x:any):Dbmanagementlog => {
                return Dbmanagementlog.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:Dbmanagementlog[])=>void)  {
            var url = "/Niva/rest/Dbmanagementlog/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = Dbmanagementlog.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.dblid = this.dblid;
                ret.dteDone = this.dteDone;
                ret.version = this.version;
                ret.notes = this.notes;
                ret.type = this.type;
            return ret;
        }

        public updateInstance(other:Dbmanagementlog):void {
            var self = this;
            self.dblid = other.dblid
            self.dteDone = other.dteDone
            self.version = other.version
            self.notes = other.notes
            self.type = other.type
        }

        public static Create() : Dbmanagementlog {
            return new Dbmanagementlog(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(dblid:string) : Dbmanagementlog {
            var ret = Dbmanagementlog.Create();
            ret.dblid = dblid;
            return ret;
        }

        public static cashedEntities: { [id: string]: Dbmanagementlog; } = {};
        public static findById_unecrypted(dblid:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : Dbmanagementlog {
            if (Entities.Dbmanagementlog.cashedEntities[dblid.toString()] !== undefined)
                return Entities.Dbmanagementlog.cashedEntities[dblid.toString()];

            var wsPath = "Dbmanagementlog/findByDblid_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['dblid'] = dblid;
            var ret = Dbmanagementlog.Create();
            Entities.Dbmanagementlog.cashedEntities[dblid.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.Dbmanagementlog.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + dblid + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return 0;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['Dbmanagementlog'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.Dbmanagementlog.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.Dbmanagementlog.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


}