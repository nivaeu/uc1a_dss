/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Classification.ts" />
/// <reference path="../Entities/FileTemplate.ts" />
/// <reference path="../Entities/DataFile.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var DataFileBase = (function (_super) {
        __extends(DataFileBase, _super);
        function DataFileBase(dafiId, filename, rowVersion, clasId, fiteId) {
            _super.call(this);
            var self = this;
            this._dafiId = new NpTypes.UIStringModel(dafiId, this);
            this._filename = new NpTypes.UIStringModel(filename, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._clasId = new NpTypes.UIManyToOneModel(clasId, this);
            this._fiteId = new NpTypes.UIManyToOneModel(fiteId, this);
            self.postConstruct();
        }
        DataFileBase.prototype.getKey = function () {
            return this.dafiId;
        };
        DataFileBase.prototype.getKeyName = function () {
            return "dafiId";
        };
        DataFileBase.prototype.getEntityName = function () {
            return 'DataFile';
        };
        DataFileBase.prototype.fromJSON = function (data) {
            return Entities.DataFile.fromJSONComplete(data);
        };
        Object.defineProperty(DataFileBase.prototype, "dafiId", {
            get: function () {
                return this._dafiId.value;
            },
            set: function (dafiId) {
                var self = this;
                self._dafiId.value = dafiId;
                //    self.dafiId_listeners.forEach(cb => { cb(<DataFile>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DataFileBase.prototype, "filename", {
            get: function () {
                return this._filename.value;
            },
            set: function (filename) {
                var self = this;
                self._filename.value = filename;
                //    self.filename_listeners.forEach(cb => { cb(<DataFile>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DataFileBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<DataFile>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DataFileBase.prototype, "clasId", {
            get: function () {
                return this._clasId.value;
            },
            set: function (clasId) {
                var self = this;
                self._clasId.value = clasId;
                //    self.clasId_listeners.forEach(cb => { cb(<DataFile>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DataFileBase.prototype, "fiteId", {
            get: function () {
                return this._fiteId.value;
            },
            set: function (fiteId) {
                var self = this;
                self._fiteId.value = fiteId;
                //    self.fiteId_listeners.forEach(cb => { cb(<DataFile>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public fiteId_listeners: Array<(c:DataFile) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.dafiId_listeners.splice(0);
            self.filename_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.clasId_listeners.splice(0);
            self.fiteId_listeners.splice(0);
        }
        */
        DataFileBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "DataFile:" + x.dafiId;
            var ret = new Entities.DataFile(x.dafiId, x.filename, x.rowVersion, x.clasId, x.fiteId);
            deserializedEntities[key] = ret;
            ret.clasId = (x.clasId !== undefined && x.clasId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.clasId.$entityName].fromJSONPartial(x.clasId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.fiteId = (x.fiteId !== undefined && x.fiteId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.fiteId.$entityName].fromJSONPartial(x.fiteId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        DataFileBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        DataFileBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "DataFile:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "DataFileBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "DataFile:"+x.dafiId;
                    var cachedCopy = <DataFile>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = DataFile.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = DataFile.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.DataFile.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        DataFileBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.DataFile.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        DataFileBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/DataFile/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.DataFile.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        DataFileBase.prototype.toJSON = function () {
            var ret = {};
            ret.dafiId = this.dafiId;
            ret.filename = this.filename;
            ret.rowVersion = this.rowVersion;
            ret.clasId =
                (this.clasId !== undefined && this.clasId !== null) ?
                    { clasId: this.clasId.clasId } :
                    (this.clasId === undefined ? undefined : null);
            ret.fiteId =
                (this.fiteId !== undefined && this.fiteId !== null) ?
                    { fiteId: this.fiteId.fiteId } :
                    (this.fiteId === undefined ? undefined : null);
            return ret;
        };
        DataFileBase.prototype.updateInstance = function (other) {
            var self = this;
            self.dafiId = other.dafiId;
            self.filename = other.filename;
            self.rowVersion = other.rowVersion;
            self.clasId = other.clasId;
            self.fiteId = other.fiteId;
        };
        DataFileBase.Create = function () {
            return new Entities.DataFile(undefined, undefined, undefined, undefined, undefined);
        };
        DataFileBase.CreateById = function (dafiId) {
            var ret = Entities.DataFile.Create();
            ret.dafiId = dafiId;
            return ret;
        };
        DataFileBase.findById_unecrypted = function (dafiId, $scope, $http, errFunc) {
            if (Entities.DataFile.cashedEntities[dafiId.toString()] !== undefined)
                return Entities.DataFile.cashedEntities[dafiId.toString()];
            var wsPath = "DataFile/findByDafiId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['dafiId'] = dafiId;
            var ret = Entities.DataFile.Create();
            Entities.DataFile.cashedEntities[dafiId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.DataFile.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + dafiId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        DataFileBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        DataFileBase.cashedEntities = {};
        return DataFileBase;
    })(NpTypes.BaseEntity);
    Entities.DataFileBase = DataFileBase;
    NpTypes.BaseEntity.entitiesFactory['DataFile'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.DataFile.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.DataFile.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=DataFileBase.js.map