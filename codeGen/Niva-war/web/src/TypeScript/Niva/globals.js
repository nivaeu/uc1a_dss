//A445BEB15E29D9A35382F80A0A7128CC
/// <reference path="globalsBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Globals = (function (_super) {
    __extends(Globals, _super);
    function Globals() {
        _super.apply(this, arguments);
    }
    return Globals;
})(GlobalsBase);
//# sourceMappingURL=globals.js.map