/// <reference path="../../RTL/npTypes.ts" />

module Services {

    export class MainServiceBase {

        public static finalizationEcGroup(ctrl: NpTypes.IAbstractController, dataRequest: MainService_finalizationEcGroup_req, func: (respDataList: string[]) => void, errFunc?: (response: any) => void) {
            var url = "/Niva/rest/MainService/finalizationEcGroup?";
            ctrl.httpPost(url, dataRequest, response => {
                var respList = response.data;
                func(respList);
            }, errFunc);
        }

        public static runningDecisionMaking(ctrl: NpTypes.IAbstractController, dataRequest: MainService_runningDecisionMaking_req, func: (respDataList: string[]) => void, errFunc?: (response: any) => void) {
            var url = "/Niva/rest/MainService/runningDecisionMaking?";
            ctrl.httpPost(url, dataRequest, response => {
                var respList = response.data;
                func(respList);
            }, errFunc, {timeout: 300000});
        }

        public static importingClassification(ctrl: NpTypes.IAbstractController, dataRequest: MainService_importingClassification_req, func: (respDataList: string[]) => void, errFunc?: (response: any) => void) {
            var url = "/Niva/rest/MainService/importingClassification?";
            ctrl.httpPost(url, dataRequest, response => {
                var respList = response.data;
                func(respList);
            }, errFunc, {timeout: 300000});
        }

        public static getPhotoRequests(ctrl: NpTypes.IAbstractController, dataRequest: MainService_getPhotoRequests_req, func: (respData:string) => void, errFunc?: (response: any) => void) {
            var url = "/Niva/rest/MainService/getPhotoRequests?";
            ctrl.httpPost(url, dataRequest, response => {
                func(response.data);
            }, errFunc);
        }

        public static uploadPhoto(ctrl: NpTypes.IAbstractController, dataRequest: MainService_uploadPhoto_req, func: (respData:string) => void, errFunc?: (response: any) => void) {
            var url = "/Niva/rest/MainService/uploadPhoto?";
            ctrl.httpPost(url, dataRequest, response => {
                func(response.data);
            }, errFunc, {timeout: 180000});
        }

        public static getBRERunsResultsList(ctrl: NpTypes.IAbstractController, dataRequest: MainService_getBRERunsResultsList_req, func: (respData:string) => void, errFunc?: (response: any) => void) {
            var url = "/Niva/rest/MainService/getBRERunsResultsList?";
            ctrl.httpPost(url, dataRequest, response => {
                func(response.data);
            }, errFunc);
        }

        public static getBRERunResults(ctrl: NpTypes.IAbstractController, dataRequest: MainService_getBRERunResults_req, func: (respData:string) => void, errFunc?: (response: any) => void) {
            var url = "/Niva/rest/MainService/getBRERunResults?";
            ctrl.httpPost(url, dataRequest, response => {
                func(response.data);
            }, errFunc, {timeout: 300000});
        }

        public static getFMISRequests(ctrl: NpTypes.IAbstractController, dataRequest: MainService_getFMISRequests_req, func: (respData:string) => void, errFunc?: (response: any) => void) {
            var url = "/Niva/rest/MainService/getFMISRequests?";
            ctrl.httpPost(url, dataRequest, response => {
                func(response.data);
            }, errFunc);
        }

        public static uploadFMIS(ctrl: NpTypes.IAbstractController, dataRequest: MainService_uploadFMIS_req, func: (respData:string) => void, errFunc?: (response: any) => void) {
            var url = "/Niva/rest/MainService/uploadFMIS?";
            ctrl.httpPost(url, dataRequest, response => {
                func(response.data);
            }, errFunc, {timeout: 120000});
        }

        public static updateIntegratedDecisionsNIssues(ctrl: NpTypes.IAbstractController, dataRequest: MainService_updateIntegratedDecisionsNIssues_req, func: (respDataList: string[]) => void, errFunc?: (response: any) => void) {
            var url = "/Niva/rest/MainService/updateIntegratedDecisionsNIssues?";
            ctrl.httpPost(url, dataRequest, response => {
                var respList = response.data;
                func(respList);
            }, errFunc);
        }

        public static updateIntegratedDecisionsNIssuesFromDema(ctrl: NpTypes.IAbstractController, dataRequest: MainService_updateIntegratedDecisionsNIssuesFromDema_req, func: (respDataList: string[]) => void, errFunc?: (response: any) => void) {
            var url = "/Niva/rest/MainService/updateIntegratedDecisionsNIssuesFromDema?";
            ctrl.httpPost(url, dataRequest, response => {
                var respList = response.data;
                func(respList);
            }, errFunc);
        }

        public static actionPushToCommonsAPI(ctrl: NpTypes.IAbstractController, dataRequest: MainService_actionPushToCommonsAPI_req, func: (respDataList: string[]) => void, errFunc?: (response: any) => void) {
            var url = "/Niva/rest/MainService/actionPushToCommonsAPI?";
            ctrl.httpPost(url, dataRequest, response => {
                var respList = response.data;
                func(respList);
            }, errFunc, {timeout: 180000});
        }

    }

    export class MainService_finalizationEcGroup_req {
        ecgrId: string;
    }

    export class MainService_runningDecisionMaking_req {
        demaId: string;
    }

    export class MainService_importingClassification_req {
        classId: string;
    }

    export class MainService_getPhotoRequests_req {
        agrisnapUid: string;
    }

    export class MainService_uploadPhoto_req {
        hash: string;
        attachment: string;
        data: string;
        environment: string;
        id: number;
    }

    export class MainService_getBRERunsResultsList_req {
        info: boolean;
    }

    export class MainService_getBRERunResults_req {
        info: boolean;
        id: number;
    }

    export class MainService_getFMISRequests_req {
        fmisUid: string;
    }

    export class MainService_uploadFMIS_req {
        fmisUid: string;
        id: number;
        attachment: string;
        metadata: string;
    }

    export class MainService_updateIntegratedDecisionsNIssues_req {
        pclaId: string;
    }

    export class MainService_updateIntegratedDecisionsNIssuesFromDema_req {
        demaId: string;
    }

    export class MainService_actionPushToCommonsAPI_req {
        demaId: string;
    }


}

