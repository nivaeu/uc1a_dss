/// <reference path="../RTL/utils.ts" />

enum Language {el, en}

class Formatter {
    public static dateToStrig(inDate: Date, activeLang: Language): string {
        switch (activeLang) {
            case Language.el:
                return Utils.convertDateToString(inDate, "dd/MM/yyyy");
            case Language.en:
                return Utils.convertDateToString(inDate, "MM/dd/yyyy");
            default:
                return Utils.convertDateToString(inDate, "dd/MM/yyyy");
        }

    }
    public static _d(date: string): Date {
        return Utils.convertStringToDate(date, "dd-MM-yyyy")
    }
}

class Messages_el {
    public static strings: { [key: string]: string } = {}

    public static PersistenceErrorOccured(): string {
        return "Σφάλμα Καταχώρησης.";
    }
    public static Previous(): string {
        return "Προηγούμενο";
    }
    public static Next(): string {
        return "Επόμενο";
    }
    public static SaveButtonLabel(): string {
        return "Καταχώρηση";
    }
    public static ReturnButtonLabel(): string {
        return "Επιστροφή";
    }
    public static CancelButtonLabel(): string {
        return "Ακύρωση";
    }
    public static DeleteButtonLabel(): string {
        return "Διαγραφή";
    }
    public static NewButtonLabel(): string {
        return "Νέo";
    }
    public static MenuApplicationsTitle(): string {
        return "Εφαρμογές";
    }
    public static MenuSettingsTitle(): string {
        return "Ρυθμίσεις";
    }
    public static MenuAppSettingsTitle(): string {
        return "Ρυθμίσεις Εφαρμογής";
    }
    public static AppSettingsTitle(): string {
        return "Ρυθμίσεις Εφαρμογής";
    }
    public static ThemeChoiceTabTitle(): string {
        return "Επιλογή Theme";
    }
    public static MenuCustomerTitle(): string {
        return "Πελάτες";
    }
    public static CEVCustomerTitle(): string {
        return "Πελάτης";
    }
    public static LoggedInUserLabel(): string {
        return "Σύνδεση ως";
    }
    public static LogoutLinkTitle(): string {
        return "Αποσύνδεση";
    }
    public static DatatableEmptyMessage(): string {
        return "Δεν υπάρχουν εγγραφές με τα συγκεκριμένα κριτήρια";
    }
    public static DatatableEntries(): string {
        return "Εγγραφές";
    }
    public static ViewExpirationDialogBody(): string {
        return "Λόγω αδράνειας στη σελίδα η συναλλαγή του χρήστη έχει λήξει. Πληκτρολογήστε ΟΚ για να πλοηγηθείτε στην αρχική σελίδα.";
    }
    public static ViewExpirationDialogTitle(): string {
        return "Η σελίδα έχει λήξει ...";
    }
    public static DeleteDialogMsg(): string {
        return "Η εγγραφή που επιλέξατε θα διαγραφεί οριστικά! Θέλετε να συνεχίσετε;";
    }
    public static WarningMsgHeader(): string {
        return "Προειδοποιητικό Μήνυμα!";
    }
    public static UncommittedChangesMsg(): string {
        return "Δεν υπάρχουν αλλαγές προς αποθήκευση";
    }
    public static EntityCreationMsg(p0: string): string {
        return "Εισαγωγή Εγγραφής: "+ p0 + "";
    }
    public static EntityRemovalMsg(p0: string): string {
        return "Διαγραφή Εγγραφής: "+ p0 + "";
    }
    public static EntityUpdateMsg(p0: string): string {
        return "Ενημέρωση Εγγραφής: "+ p0 + "";
    }
    public static EntityCreationError(p0: string): string {
        return "Σφάλμα Εισαγωγής Εγγραφής: "+ p0 + "";
    }
    public static EntityRemovalError(p0: string): string {
        return "Σφάλμα Διαγραφής Εγγραφής: "+ p0 + "";
    }
    public static EntityUpdateError(p0: string): string {
        return "Σφάλμα Ενημέρωσης Εγγραφής: "+ p0 + "";
    }
    public static EntitySuccessSave(): string {
        return "Η αποθήκευση ολοκληρώθηκε με επιτυχία.";
    }
    public static EntitySuccessDeletion(): string {
        return "Η διαγραφή ολοκληρώθηκε με επιτυχία.";
    }
    public static EntitySuccessDeletion2(): string {
        return "Η εγγραφή διαγράφτηκε με επιτυχία.";
    }
    public static EntityDeletionWarningMsg(): string {
        return "Οριστική Διαγραφή Εγγραφής. Θέλετε να συνεχίσετε;";
    }
    public static ExceptionInsideValidationErrorMessage(p0: string, p1: string): string {
        return "Σημειώθηκε σφάλμα κατά την εκτέλεση της μεθόδου επικύρωσης ("+ p0 + "), μήνυμα σφάλματος: "+ p1 + ".";
    }
    public static NoPendingChanges(): string {
        return "Δεν υπάρχουν αλλαγές προς αποθήκευση";
    }
    public static SearchNoResults(): string {
        return "Δεν βρέθηκαν εγγραφές για τα κριτήρια αναζήτησης που δώσατε.";
    }
    public static optimisticLockException(): string {
        return "Η εγγραφή έχει μεταβληθεί από άλλον χρήστη.";
    }
    public static insufficientPrivilegesException(): string {
        return "Ανεπαρκή Δικαιώματα.";
    }
    public static INVALID_VAT(sVat: string): string {
        return "Το ΑΦΜ <b>"+ sVat + "</b> δεν είναι έγκυρο.";
    }
    public static USER_NOT_REGISTERED(sVat: string): string {
        return "Ο παραγωγός με το ΑΦΜ <b>"+ sVat + "</b> δεν είναι μέλος του GAIA Επιχειρείν.";
    }
    public static USER_NOT_REGISTERED_IN_APP(sAppName: string, sVat: string): string {
        return "Ο παραγωγός με το ΑΦΜ <b>"+ sVat + "</b> δεν έχει πρόσβαση στην υπηρεσία "+ sAppName + " του GAIA Επιχειρείν.";
    }
    public static USER_NOT_REGISTERED_IN_APP_BY_SUBSCRIBER(sAppName: string, sVat: string): string {
        return "Η εγγραφή στην υπηρεσία "+ sAppName + " για τον παραγωγό με ΑΦΜ <b>"+ sVat + "</b> έχει γίνει από άλλο φορέα.";
    }
    public static TOO_MANY_RESULT_ENTITIES_FOR_EXCEL(p0: string): string {
        return "Οι εγγραφές ξεπερνούν το όριο των "+ p0 + ". Χρησιμοποιήστε φίλτρο για να περιορίσετε το πλήθος εγγραφών.";
    }
    public static NEW_PASSWORD_SET(): string {
        return "Επιτυχής αλλαγή του κωδικού πρόσβασης.";
    }
    public static maxCheckedIdsToInsert_in_MultipleInsertBtn_exceeded(p0: string): string {
        return "Ξεπεράστηκε το όριο ("+ p0 + ") νέων εγγραφών για εισαγωγή.";
    }
    public static HttpRequestTimeOutMessage(): string {
        return "Ξεπεράστηκε το χρονικό όριο αναμονής απάντησης από τον διακομιστή. Τα δεδομένα της οθόνης δεν είναι ενημερωμένα.";
    }
    public static ServerCommunicationError(): string {
        return "Σφάλμα στην επικοινωνία με τον server.";
    }
    public static SessionExpired(): string {
        return "Η συνεδρία έχει λήξει.";
    }
    public static OnbeforeunloadMsg(): string {
        return "Αν συνεχίσετε, όλες οι αλλαγές που δεν έχετε σώσει θα χαθούν!\n\n";
    }
    public static EntityById_NotFoundMsg(pk: string): string {
        return "Η εγγραφή με PK:"+ pk + " δεν βρέθηκε στη βάση δεδομένων";
    }
    public static EntityByCode_NotFoundMsg(): string {
        return "Δεν βρέθηκε εγγραφή στη βάση δεδομένων";
    }
    public static EntityByCode_TooManyRowsForCodeMsg(codeValue: string): string {
        return "Βρέθηκαν περισσότερες από μία εγγραφές για το '"+ codeValue + "'";
    }
    public static EntityByCode_NoRecordForCodeMsg(codeValue: string): string {
        return "Δεν βρέθηκε εγγραφή για το '"+ codeValue + "'";
    }
    public static SaveChangesWarningMsg(): string {
        return "Θέλετε να σωθούν οι αλλαγές;";
    }
    public static NoChangesToSaveMsg(): string {
        return "Δεν υπάρχουν αλλαγές προς αποθήκευση";
    }
    public static OnFailuredSaveMsg(): string {
        return "Εντοπίστηκαν τα παρακάτω σφάλματα";
    }
    public static FieldValidationErrorMsg(fieldName: string): string {
        return "Λάθος τιμή στο πεδίο <b>"+ fieldName + "</b>";
    }
    public static FieldValidation_RequiredMsg1(fieldName: string): string {
        return "Το πεδίο <b>"+ fieldName + "</b> είναι υποχρεωτικό";
    }
    public static FieldValidation_RequiredMsg2(): string {
        return "Υποχρεωτικό πεδίο";
    }
    public static FieldValidation_MaxLengthMsg(fieldName: string, fieldValue: string, iMaxLength: number): string {
        return "Το πεδίο <b>"+ fieldName + "</b> ("+ fieldValue + ") πρέπει να είναι μέχρι "+ iMaxLength + " χαρακτήρες";
    }
    public static FieldValidation_MinMaxValueMsg(fieldName: string, fieldValue: string, maxVal: string, minVal: string): string {
        return "Το πεδίο <b>"+ fieldName + "</b> ("+ fieldValue + ") πρέπει να είναι μεταξύ "+ minVal + " και "+ maxVal + "";
    }
    public static FieldValidation_MinValueMsg(fieldName: string, fieldValue: string, minVal: string): string {
        return "Το πεδίο <b>"+ fieldName + "</b> ("+ fieldValue + ") πρέπει να είναι μεγαλύτερο από "+ minVal + "";
    }
    public static FieldValidation_MaxValueMsg(fieldName: string, fieldValue: string, maxVal: string): string {
        return "Το πεδίο <b>"+ fieldName + "</b> ("+ fieldValue + ") πρέπει να είναι μικρότερο από "+ maxVal + "";
    }
    public static FieldValidation_MaxDecimalsMsg(fieldName: string, fieldValue: string, maxDecimals: string): string {
        return "Το πεδίο <b>"+ fieldName + "</b> ("+ fieldValue + ") πρέπει να έχει μέχρι "+ maxDecimals + " δεκαδικά ψηφία";
    }
    public static FieldValidation_NumbersOnlyMsg(): string {
        return "Μόνο αριθμοί";
    }
    public static FieldValidation_IntegersOnlyMsg(): string {
        return "Μόνο ακέραιοι αριθμοί";
    }
    public static FieldValidation_DecimalsMsg(iDecimals: number): string {
        return "Μόνο αριθμοί, "+ iDecimals + " δεκαδικά ψηφία και μία υποδιαστολή";
    }
    public static FieldValidation_Numbers_Allowed_MaxMsg(val: string): string {
        return "Επιτρεπτές τιμές: Έως "+ val + "";
    }
    public static FieldValidation_Numbers_Allowed_MinMsg(val: string): string {
        return "Επιτρεπτές τιμές: Από "+ val + "";
    }
    public static FieldValidation_Numbers_Allowed_MinMaxMsg(val1: string, val2: string): string {
        return "Επιτρεπτές τιμές: Από "+ val1 + " έως "+ val2 + "";
    }
    public static FieldValidation_DateFormatMsg(validFormat: string): string {
        return "Σε μορφή: "+ validFormat + "";
    }
    public static FieldValidation_DateFromMsg(val: string): string {
        return "Από: "+ val + "";
    }
    public static FieldValidation_DateToMsg(val: string): string {
        return "έως: "+ val + "";
    }
    public static FieldValidation_DateFromToMsg(val1: string, val2: string): string {
        return "Από: "+ val1 + " έως: "+ val2 + "";
    }
    public static FieldValidation_Blob_FileTypeMsg(): string {
        return "Επιτρέπονται μόνο αρχεία τύπου:";
    }
    public static FieldValidation_Blob_FileSizeMsg(maxSize: string): string {
        return "Επιτρέπονται αρχεία έως: "+ maxSize + " KB";
    }
    public static BlobUploadErrorMsg(): string {
        return "Δεν ήταν δυνατή η μεταφόρτωση του αρχείου";
    }
    public static BlobReadFileErrorMsg(): string {
        return "Σφάλμα κατά την διάρκεια ανάγνωσης του αρχείου...";
    }
    public static BlobReadFileCanceledMsg(): string {
        return "Η διαδικασία ανάγνωσης του αρχείου ακυρώθηκε...";
    }
    public static BlobUploadFailedMsg(): string {
        return "Σφάλμα κατά την διάρκεια μεταφόρτωσης του αρχείου...";
    }
    public static BlobUploadCanceledMsg(): string {
        return "Η διαδικασία μεταφόρτωσης του αρχείου ακυρώθηκε...";
    }
    public static PasswordExpirarionAlertInBannerMsg(iDays: number): string {
        return "Ο κωδικός σας θα λήξει σε "+ iDays + " μέρες. Παρακαλώ φροντίστε να τον αλλάξετε εγκαίρως.";
    }
    public static PasswordExpirarionAlertInBannerMsg_OneDay(): string {
        return "Ο κωδικός σας θα λήξει σε μία μέρα! Παρακαλώ φροντίστε να τον αλλάξετε εγκαίρως.";
    }
    public static ChangePasswdDialog_Null_PasswordsMsg(): string {
        return "Τα πεδία με τους κωδικούς δεν μπορούν να είναι άδεια!";
    }
    public static ChangePasswdDialog_Null_UserMsg(): string {
        return "Το πεδίο 'Χρήστης' δεν μπορεί να είναι άδειο!";
    }
    public static ChangePasswdDialog_Null_NewPasswordsDifMsg(): string {
        return "Τα δύο πεδία με τον νέο κωδικό πρέπει να έχουν την ίδια τιμή!";
    }
    public static ChangePasswdDialog_SuccessMsg(): string {
        return "Ο ορισμός νέου κωδικού ήταν επιτυχής.";
    }
    public static ImportExcel_MessageBox_Success_Message(fileName: string, totalRows: string): string {
        return "Η εισαγωγή του αρχείου '"+ fileName + "' ολοκληρώθηκε με επιτυχία.<br>Πλήθος γραμμών : "+ totalRows + "";
    }
    public static ImportExcel_MessageBox_GenericError_Message(): string {
        return "Παρουσιάστηκε σφάλμα κατά την επεξεργασία του αρχείου.";
    }
    public static ImportExcel_MessageBox_NoRecordsError_Message(): string {
        return "Το αρχείο δεν περιλαμβάνει εγγραφές";
    }
    public static USER_STATUS_NOT_ACTIVE(): string {
        return "Ο λογαριασμός σας δεν είναι ενεργός";
    }
    public static SUBSCRIBER_NOT_ASSOC_WITH_APP(): string {
        return "Ο φορέας δεν έχει άδεια χρήσης της εφαρμογής";
    }
    public static USER_NOT_ASSOC_WITH_APP_ALTHOUGH_SUBSCRPTN_OK(): string {
        return "Ο λογαριασμός σας δεν έχει άδεια χρήσης της εφαρμογής";
    }
    public static USER_NOT_ASSOC_WITH_APP(): string {
        return "Ο λογαριασμός σας δεν έχει άδεια χρήσης της εφαρμογής";
    }
    public static SUBSCRIBER_STATUS_NOT_ACTIVE(): string {
        return "Ο λογαριασμός φορέα δεν είναι ενεργός";
    }
    public static SUBSCRIBER_MODULEGROUP_NOT_ACTIVE(): string {
        return "Το πακέτο εφαρμογών δεν έχει ενεργοποιηθεί";
    }
    public static SUBSCRIBER_MODULEGROUP_TEMPRL_MISMATCH(): string {
        return "Η περίοδος λειτουργίας του πακέτου εφαρμογών δεν είναι έγκυρη";
    }
    public static UNKNOWN_APP(): string {
        return "Άγνωστη εφαρμογή";
    }
    public static AUTH_ERROR(): string {
        return "Σφάλμα κατά την ταυτοποίηση";
    }
    public static WRONG_PASSWORD(): string {
        return "Ο λογαριασμός ή ο κωδικός πρόσβασης που δώσατε δεν είναι έγκυρος";
    }
    public static ACCOUNT_DOESNOT_EXIST(): string {
        return "Ο λογαριασμός ή ο κωδικός πρόσβασης που δώσατε δεν είναι έγκυρος";
    }
    public static USER_LOCKED_BY_ATTEMPTS_LIMIT(): string {
        return "Ξεπεράσατε το όριο ανεπιτυχών προσπαθειών σύνδεσης. Ο λογαριασμός σας θα παραμείνει κλειδωμένος για τα επόμενα 30 λεπτά. Δοκιμάστε αργότερα.";
    }
    public static USER_PASSWORD_HAS_EXPIRED(): string {
        return "Ο κωδικός σας έχει λήξει. Παρακαλώ αλλάξτε τον κωδικό σας.";
    }
    public static INVALID_SUBSCRIBER(): string {
        return "Μη έγκυρος Φορέας";
    }
    public static WRONG_LOGIN_SUBSCRIBER(): string {
        return "Λανθασμένος Φορέας Εισόδου";
    }
    public static GetAllIds_TotalItemsMsg(iTotalItems: number): string {
        return "Η συγκεκριμένη λειτουργία δεν μπορεί να πραγματοποιηθεί γιατί οι προς επιλογή/αποεπιλογή εγγραφές είναι πάρα πολλές ("+ iTotalItems + ").<br>Περιορίστε τις επιστρεφόμενες εγγραφές από τα φίλτρα αναζήτησης έτσι ώστε οι συνολικές εγγραφές να είναι λιγότερες από 10.000.";
    }
    public static clfr_fite_id_fk(): string {
        return "clfr_fite_id_fk";
    }
    public static pcla_clas_id_fk(): string {
        return "pcla_clas_id_fk";
    }
    public static dafi_clas_id_fk(): string {
        return "dafi_clas_id_fk";
    }
    public static pade_dema_id_fk(): string {
        return "pade_dema_id_fk";
    }
    public static sucdsuca_id_cult_id__ui(): string {
        return "sucdsuca_id_cult_id__ui";
    }
    public static cult_name_un(): string {
        return "cult_name_un";
    }
    public static cult_code_un(): string {
        return "cult_code_un";
    }
    public static file_dir_path_un(): string {
        return "file_dir_path_un";
    }
    public static super_class_code_un(): string {
        return "super_class_code_un";
    }
    public static super_class_name_un(): string {
        return "super_class_name_un";
    }
    public static prco_fite_un(): string {
        return "prco_fite_un";
    }
    public static fite_clna_un(): string {
        return "fite_clna_un";
    }
    public static file_template_name_un(): string {
        return "file_template_name_un";
    }
    public static activity_code_un(): string {
        return "activity_code_un";
    }
    public static activity_name_un(): string {
        return "activity_name_un";
    }
    public static cover_type_code_un(): string {
        return "cover_type_code_un";
    }
    public static cover_type_name_un(): string {
        return "cover_type_name_un";
    }
}

class Messages_en {
    public static strings: { [key: string]: string } = {}

    public static PersistenceErrorOccured(): string {
        return "Persistence Error.";
    }
    public static Previous(): string {
        return "Previous";
    }
    public static Next(): string {
        return "Next";
    }
    public static SaveButtonLabel(): string {
        return "SaveButtonLabel";
    }
    public static ReturnButtonLabel(): string {
        return "ReturnButtonLabel";
    }
    public static CancelButtonLabel(): string {
        return "CancelButtonLabel";
    }
    public static DeleteButtonLabel(): string {
        return "DeleteButtonLabel";
    }
    public static NewButtonLabel(): string {
        return "NewButtonLabel";
    }
    public static MenuApplicationsTitle(): string {
        return "MenuApplicationsTitle";
    }
    public static MenuSettingsTitle(): string {
        return "MenuSettingsTitle";
    }
    public static MenuAppSettingsTitle(): string {
        return "MenuAppSettingsTitle";
    }
    public static AppSettingsTitle(): string {
        return "AppSettingsTitle";
    }
    public static ThemeChoiceTabTitle(): string {
        return "ThemeChoiceTabTitle";
    }
    public static MenuCustomerTitle(): string {
        return "MenuCustomerTitle";
    }
    public static CEVCustomerTitle(): string {
        return "CEVCustomerTitle";
    }
    public static LoggedInUserLabel(): string {
        return "LoggedInUserLabel";
    }
    public static LogoutLinkTitle(): string {
        return "LogoutLinkTitle";
    }
    public static DatatableEmptyMessage(): string {
        return "There are no records with these criteria";
    }
    public static DatatableEntries(): string {
        return "DatatableEntries";
    }
    public static ViewExpirationDialogBody(): string {
        return "Due to inactivity on the page, the user's transaction has expired. Type OK to navigate to the home page.";
    }
    public static ViewExpirationDialogTitle(): string {
        return "The page has expired ...";
    }
    public static DeleteDialogMsg(): string {
        return "The record you have selected will be permanently deleted! Would you like to proceed;";
    }
    public static WarningMsgHeader(): string {
        return "Warning Message!";
    }
    public static UncommittedChangesMsg(): string {
        return "No changes to save";
    }
    public static EntityCreationMsg(p0: string): string {
        return "Insert Record: "+ p0 + "";
    }
    public static EntityRemovalMsg(p0: string): string {
        return "Delete Record: "+ p0 + "";
    }
    public static EntityUpdateMsg(p0: string): string {
        return "Update Record: "+ p0 + "";
    }
    public static EntityCreationError(p0: string): string {
        return "Insert Record Error: "+ p0 + "";
    }
    public static EntityRemovalError(p0: string): string {
        return "Delete Record Error: "+ p0 + "";
    }
    public static EntityUpdateError(p0: string): string {
        return "Update Record Error: "+ p0 + "";
    }
    public static EntitySuccessSave(): string {
        return "Save successfully completed.";
    }
    public static EntitySuccessDeletion(): string {
        return "Deletion successfully completed.";
    }
    public static EntitySuccessDeletion2(): string {
        return "The record deleted successfully.";
    }
    public static EntityDeletionWarningMsg(): string {
        return "Permanent record deletion. Would you like to proceed?";
    }
    public static ExceptionInsideValidationErrorMessage(p0: string, p1: string): string {
        return "An error occurred while performing the validation method ("+ p0 + "), error message: "+ p1 + ".";
    }
    public static NoPendingChanges(): string {
        return "There are no changes to save";
    }
    public static SearchNoResults(): string {
        return "No records were found for your search criteria.";
    }
    public static optimisticLockException(): string {
        return "The record has been changed by another user.";
    }
    public static insufficientPrivilegesException(): string {
        return "Insufficient Privileges";
    }
    public static INVALID_VAT(sVat: string): string {
        return "INVALID_VAT";
    }
    public static USER_NOT_REGISTERED(sVat: string): string {
        return "USER_NOT_REGISTERED";
    }
    public static USER_NOT_REGISTERED_IN_APP(sAppName: string, sVat: string): string {
        return "USER_NOT_REGISTERED_IN_APP";
    }
    public static USER_NOT_REGISTERED_IN_APP_BY_SUBSCRIBER(sAppName: string, sVat: string): string {
        return "USER_NOT_REGISTERED_IN_APP_BY_SUBSCRIBER";
    }
    public static TOO_MANY_RESULT_ENTITIES_FOR_EXCEL(p0: string): string {
        return "The records exceed the limit of "+ p0 + ". Use filter to limit the number of records.";
    }
    public static NEW_PASSWORD_SET(): string {
        return "Successful password change.";
    }
    public static maxCheckedIdsToInsert_in_MultipleInsertBtn_exceeded(p0: string): string {
        return "New Record insertion limit ("+ p0 + ") exceeded.";
    }
    public static HttpRequestTimeOutMessage(): string {
        return "The server waiting time limit was exceeded. Form data are not updated.";
    }
    public static ServerCommunicationError(): string {
        return "Error communicating with the server.";
    }
    public static SessionExpired(): string {
        return "The session has been expired.";
    }
    public static OnbeforeunloadMsg(): string {
        return "If you continue, any changes you have not saved will be lost!\n\n";
    }
    public static EntityById_NotFoundMsg(pk: string): string {
        return "The record with PK:"+ pk + " was not found in the database";
    }
    public static EntityByCode_NotFoundMsg(): string {
        return "No record was not found in the database";
    }
    public static EntityByCode_TooManyRowsForCodeMsg(codeValue: string): string {
        return "More than one records found for '"+ codeValue + "'";
    }
    public static EntityByCode_NoRecordForCodeMsg(codeValue: string): string {
        return "No record found for '"+ codeValue + "'";
    }
    public static SaveChangesWarningMsg(): string {
        return "Do you want to save the changes?";
    }
    public static NoChangesToSaveMsg(): string {
        return "No changes to save";
    }
    public static OnFailuredSaveMsg(): string {
        return "The following errors were identified";
    }
    public static FieldValidationErrorMsg(fieldName: string): string {
        return "Invalid value in field <b>"+ fieldName + "</b>";
    }
    public static FieldValidation_RequiredMsg1(fieldName: string): string {
        return "The <b>"+ fieldName + "</b> field is required";
    }
    public static FieldValidation_RequiredMsg2(): string {
        return "Required field";
    }
    public static FieldValidation_MaxLengthMsg(fieldName: string, fieldValue: string, iMaxLength: number): string {
        return "The <b>"+ fieldName + "</b> field ("+ fieldValue + ") must be up to "+ iMaxLength + " characters";
    }
    public static FieldValidation_MinMaxValueMsg(fieldName: string, fieldValue: string, maxVal: string, minVal: string): string {
        return "The <b>"+ fieldName + "</b> field ("+ fieldValue + ") must be between "+ minVal + " and "+ maxVal + "";
    }
    public static FieldValidation_MinValueMsg(fieldName: string, fieldValue: string, minVal: string): string {
        return "The <b>"+ fieldName + "</b> field ("+ fieldValue + ") must be greater than "+ minVal + "";
    }
    public static FieldValidation_MaxValueMsg(fieldName: string, fieldValue: string, maxVal: string): string {
        return "The <b>"+ fieldName + "</b> field ("+ fieldValue + ") must be less than "+ maxVal + "";
    }
    public static FieldValidation_MaxDecimalsMsg(fieldName: string, fieldValue: string, maxDecimals: string): string {
        return "The <b>"+ fieldName + "</b> field ("+ fieldValue + ") must have up to "+ maxDecimals + " decimals";
    }
    public static FieldValidation_NumbersOnlyMsg(): string {
        return "Numbers only";
    }
    public static FieldValidation_IntegersOnlyMsg(): string {
        return "Integer numbers only";
    }
    public static FieldValidation_DecimalsMsg(iDecimals: number): string {
        return "Numbers only, "+ iDecimals + " decimals and one decimal point";
    }
    public static FieldValidation_Numbers_Allowed_MaxMsg(val: string): string {
        return "Allowed values: To "+ val + "";
    }
    public static FieldValidation_Numbers_Allowed_MinMsg(val: string): string {
        return "Allowed values: From "+ val + "";
    }
    public static FieldValidation_Numbers_Allowed_MinMaxMsg(val1: string, val2: string): string {
        return "Allowed values: From "+ val1 + " to "+ val2 + "";
    }
    public static FieldValidation_DateFormatMsg(validFormat: string): string {
        return "Valid format: "+ validFormat + "";
    }
    public static FieldValidation_DateFromMsg(val: string): string {
        return "From: "+ val + "";
    }
    public static FieldValidation_DateToMsg(val: string): string {
        return "to: "+ val + "";
    }
    public static FieldValidation_DateFromToMsg(val1: string, val2: string): string {
        return "From: "+ val1 + " to: "+ val2 + "";
    }
    public static FieldValidation_Blob_FileTypeMsg(): string {
        return "Allowed:";
    }
    public static FieldValidation_Blob_FileSizeMsg(maxSize: string): string {
        return "Files allowed up to: "+ maxSize + " KB";
    }
    public static BlobUploadErrorMsg(): string {
        return "Unable to upload file";
    }
    public static BlobReadFileErrorMsg(): string {
        return "Error while reading the file...";
    }
    public static BlobReadFileCanceledMsg(): string {
        return "The process of reading the file was canceled...";
    }
    public static BlobUploadFailedMsg(): string {
        return "Error while uploading the file...";
    }
    public static BlobUploadCanceledMsg(): string {
        return "The process of uploading the file was canceled...";
    }
    public static PasswordExpirarionAlertInBannerMsg(iDays: number): string {
        return "Your password will expire in "+ iDays + " days. Please be sure to change it in time.";
    }
    public static PasswordExpirarionAlertInBannerMsg_OneDay(): string {
        return "Your password will expire in one day! Please be sure to change it in time.";
    }
    public static ChangePasswdDialog_Null_PasswordsMsg(): string {
        return "Password fields cannot be null!";
    }
    public static ChangePasswdDialog_Null_UserMsg(): string {
        return "The field 'User' cannot be null!";
    }
    public static ChangePasswdDialog_Null_NewPasswordsDifMsg(): string {
        return "The two new password fields must have the same value!";
    }
    public static ChangePasswdDialog_SuccessMsg(): string {
        return "New password was set successfully.";
    }
    public static ImportExcel_MessageBox_Success_Message(fileName: string, totalRows: string): string {
        return "File '"+ fileName + "' import completed successfully.<br>Total rows imported: "+ totalRows + "";
    }
    public static ImportExcel_MessageBox_GenericError_Message(): string {
        return "An error occurred while processing the file.";
    }
    public static ImportExcel_MessageBox_NoRecordsError_Message(): string {
        return "The file does not contain records";
    }
    public static USER_STATUS_NOT_ACTIVE(): string {
        return "Your account is not active";
    }
    public static SUBSCRIBER_NOT_ASSOC_WITH_APP(): string {
        return "The subscriber has no usage license of this application";
    }
    public static USER_NOT_ASSOC_WITH_APP_ALTHOUGH_SUBSCRPTN_OK(): string {
        return "Your account has no usage license of this application";
    }
    public static USER_NOT_ASSOC_WITH_APP(): string {
        return "Your account has no usage license of this application";
    }
    public static SUBSCRIBER_STATUS_NOT_ACTIVE(): string {
        return "The subscriber's account is not active";
    }
    public static SUBSCRIBER_MODULEGROUP_NOT_ACTIVE(): string {
        return "The application package is not active";
    }
    public static SUBSCRIBER_MODULEGROUP_TEMPRL_MISMATCH(): string {
        return "Application package usage period is not valid";
    }
    public static UNKNOWN_APP(): string {
        return "Unknown application";
    }
    public static AUTH_ERROR(): string {
        return "Authentication error";
    }
    public static WRONG_PASSWORD(): string {
        return "The account or password you entered is invalid";
    }
    public static ACCOUNT_DOESNOT_EXIST(): string {
        return "The account or password you entered is invalid";
    }
    public static USER_LOCKED_BY_ATTEMPTS_LIMIT(): string {
        return "You have exceeded the limit of unsuccessful login attempts. Your account will remain locked for the next 30 minutes. Try it later.";
    }
    public static USER_PASSWORD_HAS_EXPIRED(): string {
        return "Your password has expired. Please change your password.";
    }
    public static INVALID_SUBSCRIBER(): string {
        return "Invalid Subscriber";
    }
    public static WRONG_LOGIN_SUBSCRIBER(): string {
        return "Wrong Login Subscriber";
    }
    public static GetAllIds_TotalItemsMsg(iTotalItems: number): string {
        return "This feature can not be performed because the records to be selected/de-selected are too many ("+ iTotalItems + ").<br>Restrict retrieved records from search filters so that total records are less than 10,000.";
    }
    public static clfr_fite_id_fk(): string {
        return "The record cannot be deleted because it is being used.";
    }
    public static pcla_clas_id_fk(): string {
        return "Cannot be deleted because it is being used.";
    }
    public static dafi_clas_id_fk(): string {
        return "Cannot be deleted because it is being used.";
    }
    public static pade_dema_id_fk(): string {
        return "Cannot be deleted because it is being used.";
    }
    public static sucdsuca_id_cult_id__ui(): string {
        return "Crop Already Exists.";
    }
    public static cult_name_un(): string {
        return "Crop Name Already Exists.";
    }
    public static cult_code_un(): string {
        return "Crop Code Already Exists.";
    }
    public static file_dir_path_un(): string {
        return "Only One File Directory is Allowed.";
    }
    public static super_class_code_un(): string {
        return "Code Already Exists.";
    }
    public static super_class_name_un(): string {
        return "Name Already Exists.";
    }
    public static prco_fite_un(): string {
        return "System Column Already Exists.";
    }
    public static fite_clna_un(): string {
        return "Import Column Already Exists.";
    }
    public static file_template_name_un(): string {
        return "Data Import Template Name Already Exists.";
    }
    public static activity_code_un(): string {
        return "Code Already Exists.";
    }
    public static activity_name_un(): string {
        return "Name Already Exists.";
    }
    public static cover_type_code_un(): string {
        return "Land Cover Code Already Exists.";
    }
    public static cover_type_name_un(): string {
        return "Land Cover Name Already Exists.";
    }
}


class Messages {
    private static activeLang: Language;

    public static setActiveLangByGlobalLang($scope: NpTypes.IApplicationScope) {
        var self = this;
        switch ($scope.globals.globalLang) {
            case 'el':
                self.activeLang = Language.el;
                break;
            case 'en':
                self.activeLang = Language.en;
                break;
            default: self.activeLang = Language.en;
        }
    }

    public static PersistenceErrorOccured(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.PersistenceErrorOccured();
            case Language.en:
                return Messages_en.PersistenceErrorOccured();
            default:
                return Messages_en.PersistenceErrorOccured();
        }
    }

    public static Previous(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.Previous();
            case Language.en:
                return Messages_en.Previous();
            default:
                return Messages_en.Previous();
        }
    }

    public static Next(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.Next();
            case Language.en:
                return Messages_en.Next();
            default:
                return Messages_en.Next();
        }
    }

    public static SaveButtonLabel(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.SaveButtonLabel();
            case Language.en:
                return Messages_en.SaveButtonLabel();
            default:
                return Messages_en.SaveButtonLabel();
        }
    }

    public static ReturnButtonLabel(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ReturnButtonLabel();
            case Language.en:
                return Messages_en.ReturnButtonLabel();
            default:
                return Messages_en.ReturnButtonLabel();
        }
    }

    public static CancelButtonLabel(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.CancelButtonLabel();
            case Language.en:
                return Messages_en.CancelButtonLabel();
            default:
                return Messages_en.CancelButtonLabel();
        }
    }

    public static DeleteButtonLabel(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.DeleteButtonLabel();
            case Language.en:
                return Messages_en.DeleteButtonLabel();
            default:
                return Messages_en.DeleteButtonLabel();
        }
    }

    public static NewButtonLabel(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.NewButtonLabel();
            case Language.en:
                return Messages_en.NewButtonLabel();
            default:
                return Messages_en.NewButtonLabel();
        }
    }

    public static MenuApplicationsTitle(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.MenuApplicationsTitle();
            case Language.en:
                return Messages_en.MenuApplicationsTitle();
            default:
                return Messages_en.MenuApplicationsTitle();
        }
    }

    public static MenuSettingsTitle(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.MenuSettingsTitle();
            case Language.en:
                return Messages_en.MenuSettingsTitle();
            default:
                return Messages_en.MenuSettingsTitle();
        }
    }

    public static MenuAppSettingsTitle(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.MenuAppSettingsTitle();
            case Language.en:
                return Messages_en.MenuAppSettingsTitle();
            default:
                return Messages_en.MenuAppSettingsTitle();
        }
    }

    public static AppSettingsTitle(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.AppSettingsTitle();
            case Language.en:
                return Messages_en.AppSettingsTitle();
            default:
                return Messages_en.AppSettingsTitle();
        }
    }

    public static ThemeChoiceTabTitle(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ThemeChoiceTabTitle();
            case Language.en:
                return Messages_en.ThemeChoiceTabTitle();
            default:
                return Messages_en.ThemeChoiceTabTitle();
        }
    }

    public static MenuCustomerTitle(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.MenuCustomerTitle();
            case Language.en:
                return Messages_en.MenuCustomerTitle();
            default:
                return Messages_en.MenuCustomerTitle();
        }
    }

    public static CEVCustomerTitle(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.CEVCustomerTitle();
            case Language.en:
                return Messages_en.CEVCustomerTitle();
            default:
                return Messages_en.CEVCustomerTitle();
        }
    }

    public static LoggedInUserLabel(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.LoggedInUserLabel();
            case Language.en:
                return Messages_en.LoggedInUserLabel();
            default:
                return Messages_en.LoggedInUserLabel();
        }
    }

    public static LogoutLinkTitle(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.LogoutLinkTitle();
            case Language.en:
                return Messages_en.LogoutLinkTitle();
            default:
                return Messages_en.LogoutLinkTitle();
        }
    }

    public static DatatableEmptyMessage(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.DatatableEmptyMessage();
            case Language.en:
                return Messages_en.DatatableEmptyMessage();
            default:
                return Messages_en.DatatableEmptyMessage();
        }
    }

    public static DatatableEntries(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.DatatableEntries();
            case Language.en:
                return Messages_en.DatatableEntries();
            default:
                return Messages_en.DatatableEntries();
        }
    }

    public static ViewExpirationDialogBody(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ViewExpirationDialogBody();
            case Language.en:
                return Messages_en.ViewExpirationDialogBody();
            default:
                return Messages_en.ViewExpirationDialogBody();
        }
    }

    public static ViewExpirationDialogTitle(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ViewExpirationDialogTitle();
            case Language.en:
                return Messages_en.ViewExpirationDialogTitle();
            default:
                return Messages_en.ViewExpirationDialogTitle();
        }
    }

    public static DeleteDialogMsg(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.DeleteDialogMsg();
            case Language.en:
                return Messages_en.DeleteDialogMsg();
            default:
                return Messages_en.DeleteDialogMsg();
        }
    }

    public static WarningMsgHeader(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.WarningMsgHeader();
            case Language.en:
                return Messages_en.WarningMsgHeader();
            default:
                return Messages_en.WarningMsgHeader();
        }
    }

    public static UncommittedChangesMsg(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.UncommittedChangesMsg();
            case Language.en:
                return Messages_en.UncommittedChangesMsg();
            default:
                return Messages_en.UncommittedChangesMsg();
        }
    }

    public static EntityCreationMsg(p0: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityCreationMsg(p0);
            case Language.en:
                return Messages_en.EntityCreationMsg(p0);
            default:
                return Messages_en.EntityCreationMsg(p0);
        }
    }

    public static EntityRemovalMsg(p0: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityRemovalMsg(p0);
            case Language.en:
                return Messages_en.EntityRemovalMsg(p0);
            default:
                return Messages_en.EntityRemovalMsg(p0);
        }
    }

    public static EntityUpdateMsg(p0: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityUpdateMsg(p0);
            case Language.en:
                return Messages_en.EntityUpdateMsg(p0);
            default:
                return Messages_en.EntityUpdateMsg(p0);
        }
    }

    public static EntityCreationError(p0: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityCreationError(p0);
            case Language.en:
                return Messages_en.EntityCreationError(p0);
            default:
                return Messages_en.EntityCreationError(p0);
        }
    }

    public static EntityRemovalError(p0: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityRemovalError(p0);
            case Language.en:
                return Messages_en.EntityRemovalError(p0);
            default:
                return Messages_en.EntityRemovalError(p0);
        }
    }

    public static EntityUpdateError(p0: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityUpdateError(p0);
            case Language.en:
                return Messages_en.EntityUpdateError(p0);
            default:
                return Messages_en.EntityUpdateError(p0);
        }
    }

    public static EntitySuccessSave(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntitySuccessSave();
            case Language.en:
                return Messages_en.EntitySuccessSave();
            default:
                return Messages_en.EntitySuccessSave();
        }
    }

    public static EntitySuccessDeletion(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntitySuccessDeletion();
            case Language.en:
                return Messages_en.EntitySuccessDeletion();
            default:
                return Messages_en.EntitySuccessDeletion();
        }
    }

    public static EntitySuccessDeletion2(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntitySuccessDeletion2();
            case Language.en:
                return Messages_en.EntitySuccessDeletion2();
            default:
                return Messages_en.EntitySuccessDeletion2();
        }
    }

    public static EntityDeletionWarningMsg(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityDeletionWarningMsg();
            case Language.en:
                return Messages_en.EntityDeletionWarningMsg();
            default:
                return Messages_en.EntityDeletionWarningMsg();
        }
    }

    public static ExceptionInsideValidationErrorMessage(p0: string, p1: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ExceptionInsideValidationErrorMessage(p0, p1);
            case Language.en:
                return Messages_en.ExceptionInsideValidationErrorMessage(p0, p1);
            default:
                return Messages_en.ExceptionInsideValidationErrorMessage(p0, p1);
        }
    }

    public static NoPendingChanges(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.NoPendingChanges();
            case Language.en:
                return Messages_en.NoPendingChanges();
            default:
                return Messages_en.NoPendingChanges();
        }
    }

    public static SearchNoResults(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.SearchNoResults();
            case Language.en:
                return Messages_en.SearchNoResults();
            default:
                return Messages_en.SearchNoResults();
        }
    }

    public static optimisticLockException(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.optimisticLockException();
            case Language.en:
                return Messages_en.optimisticLockException();
            default:
                return Messages_en.optimisticLockException();
        }
    }

    public static insufficientPrivilegesException(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.insufficientPrivilegesException();
            case Language.en:
                return Messages_en.insufficientPrivilegesException();
            default:
                return Messages_en.insufficientPrivilegesException();
        }
    }

    public static INVALID_VAT(sVat: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.INVALID_VAT(sVat);
            case Language.en:
                return Messages_en.INVALID_VAT(sVat);
            default:
                return Messages_en.INVALID_VAT(sVat);
        }
    }

    public static USER_NOT_REGISTERED(sVat: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.USER_NOT_REGISTERED(sVat);
            case Language.en:
                return Messages_en.USER_NOT_REGISTERED(sVat);
            default:
                return Messages_en.USER_NOT_REGISTERED(sVat);
        }
    }

    public static USER_NOT_REGISTERED_IN_APP(sAppName: string, sVat: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.USER_NOT_REGISTERED_IN_APP(sAppName, sVat);
            case Language.en:
                return Messages_en.USER_NOT_REGISTERED_IN_APP(sAppName, sVat);
            default:
                return Messages_en.USER_NOT_REGISTERED_IN_APP(sAppName, sVat);
        }
    }

    public static USER_NOT_REGISTERED_IN_APP_BY_SUBSCRIBER(sAppName: string, sVat: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.USER_NOT_REGISTERED_IN_APP_BY_SUBSCRIBER(sAppName, sVat);
            case Language.en:
                return Messages_en.USER_NOT_REGISTERED_IN_APP_BY_SUBSCRIBER(sAppName, sVat);
            default:
                return Messages_en.USER_NOT_REGISTERED_IN_APP_BY_SUBSCRIBER(sAppName, sVat);
        }
    }

    public static TOO_MANY_RESULT_ENTITIES_FOR_EXCEL(p0: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.TOO_MANY_RESULT_ENTITIES_FOR_EXCEL(p0);
            case Language.en:
                return Messages_en.TOO_MANY_RESULT_ENTITIES_FOR_EXCEL(p0);
            default:
                return Messages_en.TOO_MANY_RESULT_ENTITIES_FOR_EXCEL(p0);
        }
    }

    public static NEW_PASSWORD_SET(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.NEW_PASSWORD_SET();
            case Language.en:
                return Messages_en.NEW_PASSWORD_SET();
            default:
                return Messages_en.NEW_PASSWORD_SET();
        }
    }

    public static maxCheckedIdsToInsert_in_MultipleInsertBtn_exceeded(p0: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.maxCheckedIdsToInsert_in_MultipleInsertBtn_exceeded(p0);
            case Language.en:
                return Messages_en.maxCheckedIdsToInsert_in_MultipleInsertBtn_exceeded(p0);
            default:
                return Messages_en.maxCheckedIdsToInsert_in_MultipleInsertBtn_exceeded(p0);
        }
    }

    public static HttpRequestTimeOutMessage(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.HttpRequestTimeOutMessage();
            case Language.en:
                return Messages_en.HttpRequestTimeOutMessage();
            default:
                return Messages_en.HttpRequestTimeOutMessage();
        }
    }

    public static ServerCommunicationError(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ServerCommunicationError();
            case Language.en:
                return Messages_en.ServerCommunicationError();
            default:
                return Messages_en.ServerCommunicationError();
        }
    }

    public static SessionExpired(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.SessionExpired();
            case Language.en:
                return Messages_en.SessionExpired();
            default:
                return Messages_en.SessionExpired();
        }
    }

    public static OnbeforeunloadMsg(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.OnbeforeunloadMsg();
            case Language.en:
                return Messages_en.OnbeforeunloadMsg();
            default:
                return Messages_en.OnbeforeunloadMsg();
        }
    }

    public static EntityById_NotFoundMsg(pk: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityById_NotFoundMsg(pk);
            case Language.en:
                return Messages_en.EntityById_NotFoundMsg(pk);
            default:
                return Messages_en.EntityById_NotFoundMsg(pk);
        }
    }

    public static EntityByCode_NotFoundMsg(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityByCode_NotFoundMsg();
            case Language.en:
                return Messages_en.EntityByCode_NotFoundMsg();
            default:
                return Messages_en.EntityByCode_NotFoundMsg();
        }
    }

    public static EntityByCode_TooManyRowsForCodeMsg(codeValue: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityByCode_TooManyRowsForCodeMsg(codeValue);
            case Language.en:
                return Messages_en.EntityByCode_TooManyRowsForCodeMsg(codeValue);
            default:
                return Messages_en.EntityByCode_TooManyRowsForCodeMsg(codeValue);
        }
    }

    public static EntityByCode_NoRecordForCodeMsg(codeValue: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.EntityByCode_NoRecordForCodeMsg(codeValue);
            case Language.en:
                return Messages_en.EntityByCode_NoRecordForCodeMsg(codeValue);
            default:
                return Messages_en.EntityByCode_NoRecordForCodeMsg(codeValue);
        }
    }

    public static SaveChangesWarningMsg(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.SaveChangesWarningMsg();
            case Language.en:
                return Messages_en.SaveChangesWarningMsg();
            default:
                return Messages_en.SaveChangesWarningMsg();
        }
    }

    public static NoChangesToSaveMsg(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.NoChangesToSaveMsg();
            case Language.en:
                return Messages_en.NoChangesToSaveMsg();
            default:
                return Messages_en.NoChangesToSaveMsg();
        }
    }

    public static OnFailuredSaveMsg(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.OnFailuredSaveMsg();
            case Language.en:
                return Messages_en.OnFailuredSaveMsg();
            default:
                return Messages_en.OnFailuredSaveMsg();
        }
    }

    public static FieldValidationErrorMsg(fieldName: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidationErrorMsg(fieldName);
            case Language.en:
                return Messages_en.FieldValidationErrorMsg(fieldName);
            default:
                return Messages_en.FieldValidationErrorMsg(fieldName);
        }
    }

    public static FieldValidation_RequiredMsg1(fieldName: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_RequiredMsg1(fieldName);
            case Language.en:
                return Messages_en.FieldValidation_RequiredMsg1(fieldName);
            default:
                return Messages_en.FieldValidation_RequiredMsg1(fieldName);
        }
    }

    public static FieldValidation_RequiredMsg2(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_RequiredMsg2();
            case Language.en:
                return Messages_en.FieldValidation_RequiredMsg2();
            default:
                return Messages_en.FieldValidation_RequiredMsg2();
        }
    }

    public static FieldValidation_MaxLengthMsg(fieldName: string, fieldValue: string, iMaxLength: number): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_MaxLengthMsg(fieldName, fieldValue, iMaxLength);
            case Language.en:
                return Messages_en.FieldValidation_MaxLengthMsg(fieldName, fieldValue, iMaxLength);
            default:
                return Messages_en.FieldValidation_MaxLengthMsg(fieldName, fieldValue, iMaxLength);
        }
    }

    public static FieldValidation_MinMaxValueMsg(fieldName: string, fieldValue: string, maxVal: string, minVal: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_MinMaxValueMsg(fieldName, fieldValue, maxVal, minVal);
            case Language.en:
                return Messages_en.FieldValidation_MinMaxValueMsg(fieldName, fieldValue, maxVal, minVal);
            default:
                return Messages_en.FieldValidation_MinMaxValueMsg(fieldName, fieldValue, maxVal, minVal);
        }
    }

    public static FieldValidation_MinValueMsg(fieldName: string, fieldValue: string, minVal: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_MinValueMsg(fieldName, fieldValue, minVal);
            case Language.en:
                return Messages_en.FieldValidation_MinValueMsg(fieldName, fieldValue, minVal);
            default:
                return Messages_en.FieldValidation_MinValueMsg(fieldName, fieldValue, minVal);
        }
    }

    public static FieldValidation_MaxValueMsg(fieldName: string, fieldValue: string, maxVal: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_MaxValueMsg(fieldName, fieldValue, maxVal);
            case Language.en:
                return Messages_en.FieldValidation_MaxValueMsg(fieldName, fieldValue, maxVal);
            default:
                return Messages_en.FieldValidation_MaxValueMsg(fieldName, fieldValue, maxVal);
        }
    }

    public static FieldValidation_MaxDecimalsMsg(fieldName: string, fieldValue: string, maxDecimals: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_MaxDecimalsMsg(fieldName, fieldValue, maxDecimals);
            case Language.en:
                return Messages_en.FieldValidation_MaxDecimalsMsg(fieldName, fieldValue, maxDecimals);
            default:
                return Messages_en.FieldValidation_MaxDecimalsMsg(fieldName, fieldValue, maxDecimals);
        }
    }

    public static FieldValidation_NumbersOnlyMsg(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_NumbersOnlyMsg();
            case Language.en:
                return Messages_en.FieldValidation_NumbersOnlyMsg();
            default:
                return Messages_en.FieldValidation_NumbersOnlyMsg();
        }
    }

    public static FieldValidation_IntegersOnlyMsg(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_IntegersOnlyMsg();
            case Language.en:
                return Messages_en.FieldValidation_IntegersOnlyMsg();
            default:
                return Messages_en.FieldValidation_IntegersOnlyMsg();
        }
    }

    public static FieldValidation_DecimalsMsg(iDecimals: number): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_DecimalsMsg(iDecimals);
            case Language.en:
                return Messages_en.FieldValidation_DecimalsMsg(iDecimals);
            default:
                return Messages_en.FieldValidation_DecimalsMsg(iDecimals);
        }
    }

    public static FieldValidation_Numbers_Allowed_MaxMsg(val: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_Numbers_Allowed_MaxMsg(val);
            case Language.en:
                return Messages_en.FieldValidation_Numbers_Allowed_MaxMsg(val);
            default:
                return Messages_en.FieldValidation_Numbers_Allowed_MaxMsg(val);
        }
    }

    public static FieldValidation_Numbers_Allowed_MinMsg(val: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_Numbers_Allowed_MinMsg(val);
            case Language.en:
                return Messages_en.FieldValidation_Numbers_Allowed_MinMsg(val);
            default:
                return Messages_en.FieldValidation_Numbers_Allowed_MinMsg(val);
        }
    }

    public static FieldValidation_Numbers_Allowed_MinMaxMsg(val1: string, val2: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_Numbers_Allowed_MinMaxMsg(val1, val2);
            case Language.en:
                return Messages_en.FieldValidation_Numbers_Allowed_MinMaxMsg(val1, val2);
            default:
                return Messages_en.FieldValidation_Numbers_Allowed_MinMaxMsg(val1, val2);
        }
    }

    public static FieldValidation_DateFormatMsg(validFormat: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_DateFormatMsg(validFormat);
            case Language.en:
                return Messages_en.FieldValidation_DateFormatMsg(validFormat);
            default:
                return Messages_en.FieldValidation_DateFormatMsg(validFormat);
        }
    }

    public static FieldValidation_DateFromMsg(val: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_DateFromMsg(val);
            case Language.en:
                return Messages_en.FieldValidation_DateFromMsg(val);
            default:
                return Messages_en.FieldValidation_DateFromMsg(val);
        }
    }

    public static FieldValidation_DateToMsg(val: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_DateToMsg(val);
            case Language.en:
                return Messages_en.FieldValidation_DateToMsg(val);
            default:
                return Messages_en.FieldValidation_DateToMsg(val);
        }
    }

    public static FieldValidation_DateFromToMsg(val1: string, val2: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_DateFromToMsg(val1, val2);
            case Language.en:
                return Messages_en.FieldValidation_DateFromToMsg(val1, val2);
            default:
                return Messages_en.FieldValidation_DateFromToMsg(val1, val2);
        }
    }

    public static FieldValidation_Blob_FileTypeMsg(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_Blob_FileTypeMsg();
            case Language.en:
                return Messages_en.FieldValidation_Blob_FileTypeMsg();
            default:
                return Messages_en.FieldValidation_Blob_FileTypeMsg();
        }
    }

    public static FieldValidation_Blob_FileSizeMsg(maxSize: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.FieldValidation_Blob_FileSizeMsg(maxSize);
            case Language.en:
                return Messages_en.FieldValidation_Blob_FileSizeMsg(maxSize);
            default:
                return Messages_en.FieldValidation_Blob_FileSizeMsg(maxSize);
        }
    }

    public static BlobUploadErrorMsg(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.BlobUploadErrorMsg();
            case Language.en:
                return Messages_en.BlobUploadErrorMsg();
            default:
                return Messages_en.BlobUploadErrorMsg();
        }
    }

    public static BlobReadFileErrorMsg(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.BlobReadFileErrorMsg();
            case Language.en:
                return Messages_en.BlobReadFileErrorMsg();
            default:
                return Messages_en.BlobReadFileErrorMsg();
        }
    }

    public static BlobReadFileCanceledMsg(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.BlobReadFileCanceledMsg();
            case Language.en:
                return Messages_en.BlobReadFileCanceledMsg();
            default:
                return Messages_en.BlobReadFileCanceledMsg();
        }
    }

    public static BlobUploadFailedMsg(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.BlobUploadFailedMsg();
            case Language.en:
                return Messages_en.BlobUploadFailedMsg();
            default:
                return Messages_en.BlobUploadFailedMsg();
        }
    }

    public static BlobUploadCanceledMsg(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.BlobUploadCanceledMsg();
            case Language.en:
                return Messages_en.BlobUploadCanceledMsg();
            default:
                return Messages_en.BlobUploadCanceledMsg();
        }
    }

    public static PasswordExpirarionAlertInBannerMsg(iDays: number): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.PasswordExpirarionAlertInBannerMsg(iDays);
            case Language.en:
                return Messages_en.PasswordExpirarionAlertInBannerMsg(iDays);
            default:
                return Messages_en.PasswordExpirarionAlertInBannerMsg(iDays);
        }
    }

    public static PasswordExpirarionAlertInBannerMsg_OneDay(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.PasswordExpirarionAlertInBannerMsg_OneDay();
            case Language.en:
                return Messages_en.PasswordExpirarionAlertInBannerMsg_OneDay();
            default:
                return Messages_en.PasswordExpirarionAlertInBannerMsg_OneDay();
        }
    }

    public static ChangePasswdDialog_Null_PasswordsMsg(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ChangePasswdDialog_Null_PasswordsMsg();
            case Language.en:
                return Messages_en.ChangePasswdDialog_Null_PasswordsMsg();
            default:
                return Messages_en.ChangePasswdDialog_Null_PasswordsMsg();
        }
    }

    public static ChangePasswdDialog_Null_UserMsg(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ChangePasswdDialog_Null_UserMsg();
            case Language.en:
                return Messages_en.ChangePasswdDialog_Null_UserMsg();
            default:
                return Messages_en.ChangePasswdDialog_Null_UserMsg();
        }
    }

    public static ChangePasswdDialog_Null_NewPasswordsDifMsg(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ChangePasswdDialog_Null_NewPasswordsDifMsg();
            case Language.en:
                return Messages_en.ChangePasswdDialog_Null_NewPasswordsDifMsg();
            default:
                return Messages_en.ChangePasswdDialog_Null_NewPasswordsDifMsg();
        }
    }

    public static ChangePasswdDialog_SuccessMsg(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ChangePasswdDialog_SuccessMsg();
            case Language.en:
                return Messages_en.ChangePasswdDialog_SuccessMsg();
            default:
                return Messages_en.ChangePasswdDialog_SuccessMsg();
        }
    }

    public static ImportExcel_MessageBox_Success_Message(fileName: string, totalRows: string): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ImportExcel_MessageBox_Success_Message(fileName, totalRows);
            case Language.en:
                return Messages_en.ImportExcel_MessageBox_Success_Message(fileName, totalRows);
            default:
                return Messages_en.ImportExcel_MessageBox_Success_Message(fileName, totalRows);
        }
    }

    public static ImportExcel_MessageBox_GenericError_Message(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ImportExcel_MessageBox_GenericError_Message();
            case Language.en:
                return Messages_en.ImportExcel_MessageBox_GenericError_Message();
            default:
                return Messages_en.ImportExcel_MessageBox_GenericError_Message();
        }
    }

    public static ImportExcel_MessageBox_NoRecordsError_Message(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ImportExcel_MessageBox_NoRecordsError_Message();
            case Language.en:
                return Messages_en.ImportExcel_MessageBox_NoRecordsError_Message();
            default:
                return Messages_en.ImportExcel_MessageBox_NoRecordsError_Message();
        }
    }

    public static USER_STATUS_NOT_ACTIVE(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.USER_STATUS_NOT_ACTIVE();
            case Language.en:
                return Messages_en.USER_STATUS_NOT_ACTIVE();
            default:
                return Messages_en.USER_STATUS_NOT_ACTIVE();
        }
    }

    public static SUBSCRIBER_NOT_ASSOC_WITH_APP(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.SUBSCRIBER_NOT_ASSOC_WITH_APP();
            case Language.en:
                return Messages_en.SUBSCRIBER_NOT_ASSOC_WITH_APP();
            default:
                return Messages_en.SUBSCRIBER_NOT_ASSOC_WITH_APP();
        }
    }

    public static USER_NOT_ASSOC_WITH_APP_ALTHOUGH_SUBSCRPTN_OK(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.USER_NOT_ASSOC_WITH_APP_ALTHOUGH_SUBSCRPTN_OK();
            case Language.en:
                return Messages_en.USER_NOT_ASSOC_WITH_APP_ALTHOUGH_SUBSCRPTN_OK();
            default:
                return Messages_en.USER_NOT_ASSOC_WITH_APP_ALTHOUGH_SUBSCRPTN_OK();
        }
    }

    public static USER_NOT_ASSOC_WITH_APP(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.USER_NOT_ASSOC_WITH_APP();
            case Language.en:
                return Messages_en.USER_NOT_ASSOC_WITH_APP();
            default:
                return Messages_en.USER_NOT_ASSOC_WITH_APP();
        }
    }

    public static SUBSCRIBER_STATUS_NOT_ACTIVE(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.SUBSCRIBER_STATUS_NOT_ACTIVE();
            case Language.en:
                return Messages_en.SUBSCRIBER_STATUS_NOT_ACTIVE();
            default:
                return Messages_en.SUBSCRIBER_STATUS_NOT_ACTIVE();
        }
    }

    public static SUBSCRIBER_MODULEGROUP_NOT_ACTIVE(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.SUBSCRIBER_MODULEGROUP_NOT_ACTIVE();
            case Language.en:
                return Messages_en.SUBSCRIBER_MODULEGROUP_NOT_ACTIVE();
            default:
                return Messages_en.SUBSCRIBER_MODULEGROUP_NOT_ACTIVE();
        }
    }

    public static SUBSCRIBER_MODULEGROUP_TEMPRL_MISMATCH(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.SUBSCRIBER_MODULEGROUP_TEMPRL_MISMATCH();
            case Language.en:
                return Messages_en.SUBSCRIBER_MODULEGROUP_TEMPRL_MISMATCH();
            default:
                return Messages_en.SUBSCRIBER_MODULEGROUP_TEMPRL_MISMATCH();
        }
    }

    public static UNKNOWN_APP(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.UNKNOWN_APP();
            case Language.en:
                return Messages_en.UNKNOWN_APP();
            default:
                return Messages_en.UNKNOWN_APP();
        }
    }

    public static AUTH_ERROR(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.AUTH_ERROR();
            case Language.en:
                return Messages_en.AUTH_ERROR();
            default:
                return Messages_en.AUTH_ERROR();
        }
    }

    public static WRONG_PASSWORD(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.WRONG_PASSWORD();
            case Language.en:
                return Messages_en.WRONG_PASSWORD();
            default:
                return Messages_en.WRONG_PASSWORD();
        }
    }

    public static ACCOUNT_DOESNOT_EXIST(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.ACCOUNT_DOESNOT_EXIST();
            case Language.en:
                return Messages_en.ACCOUNT_DOESNOT_EXIST();
            default:
                return Messages_en.ACCOUNT_DOESNOT_EXIST();
        }
    }

    public static USER_LOCKED_BY_ATTEMPTS_LIMIT(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.USER_LOCKED_BY_ATTEMPTS_LIMIT();
            case Language.en:
                return Messages_en.USER_LOCKED_BY_ATTEMPTS_LIMIT();
            default:
                return Messages_en.USER_LOCKED_BY_ATTEMPTS_LIMIT();
        }
    }

    public static USER_PASSWORD_HAS_EXPIRED(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.USER_PASSWORD_HAS_EXPIRED();
            case Language.en:
                return Messages_en.USER_PASSWORD_HAS_EXPIRED();
            default:
                return Messages_en.USER_PASSWORD_HAS_EXPIRED();
        }
    }

    public static INVALID_SUBSCRIBER(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.INVALID_SUBSCRIBER();
            case Language.en:
                return Messages_en.INVALID_SUBSCRIBER();
            default:
                return Messages_en.INVALID_SUBSCRIBER();
        }
    }

    public static WRONG_LOGIN_SUBSCRIBER(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.WRONG_LOGIN_SUBSCRIBER();
            case Language.en:
                return Messages_en.WRONG_LOGIN_SUBSCRIBER();
            default:
                return Messages_en.WRONG_LOGIN_SUBSCRIBER();
        }
    }

    public static GetAllIds_TotalItemsMsg(iTotalItems: number): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.GetAllIds_TotalItemsMsg(iTotalItems);
            case Language.en:
                return Messages_en.GetAllIds_TotalItemsMsg(iTotalItems);
            default:
                return Messages_en.GetAllIds_TotalItemsMsg(iTotalItems);
        }
    }

    public static clfr_fite_id_fk(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.clfr_fite_id_fk();
            case Language.en:
                return Messages_en.clfr_fite_id_fk();
            default:
                return Messages_en.clfr_fite_id_fk();
        }
    }

    public static pcla_clas_id_fk(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.pcla_clas_id_fk();
            case Language.en:
                return Messages_en.pcla_clas_id_fk();
            default:
                return Messages_en.pcla_clas_id_fk();
        }
    }

    public static dafi_clas_id_fk(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.dafi_clas_id_fk();
            case Language.en:
                return Messages_en.dafi_clas_id_fk();
            default:
                return Messages_en.dafi_clas_id_fk();
        }
    }

    public static pade_dema_id_fk(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.pade_dema_id_fk();
            case Language.en:
                return Messages_en.pade_dema_id_fk();
            default:
                return Messages_en.pade_dema_id_fk();
        }
    }

    public static sucdsuca_id_cult_id__ui(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.sucdsuca_id_cult_id__ui();
            case Language.en:
                return Messages_en.sucdsuca_id_cult_id__ui();
            default:
                return Messages_en.sucdsuca_id_cult_id__ui();
        }
    }

    public static cult_name_un(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.cult_name_un();
            case Language.en:
                return Messages_en.cult_name_un();
            default:
                return Messages_en.cult_name_un();
        }
    }

    public static cult_code_un(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.cult_code_un();
            case Language.en:
                return Messages_en.cult_code_un();
            default:
                return Messages_en.cult_code_un();
        }
    }

    public static file_dir_path_un(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.file_dir_path_un();
            case Language.en:
                return Messages_en.file_dir_path_un();
            default:
                return Messages_en.file_dir_path_un();
        }
    }

    public static super_class_code_un(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.super_class_code_un();
            case Language.en:
                return Messages_en.super_class_code_un();
            default:
                return Messages_en.super_class_code_un();
        }
    }

    public static super_class_name_un(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.super_class_name_un();
            case Language.en:
                return Messages_en.super_class_name_un();
            default:
                return Messages_en.super_class_name_un();
        }
    }

    public static prco_fite_un(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.prco_fite_un();
            case Language.en:
                return Messages_en.prco_fite_un();
            default:
                return Messages_en.prco_fite_un();
        }
    }

    public static fite_clna_un(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.fite_clna_un();
            case Language.en:
                return Messages_en.fite_clna_un();
            default:
                return Messages_en.fite_clna_un();
        }
    }

    public static file_template_name_un(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.file_template_name_un();
            case Language.en:
                return Messages_en.file_template_name_un();
            default:
                return Messages_en.file_template_name_un();
        }
    }

    public static activity_code_un(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.activity_code_un();
            case Language.en:
                return Messages_en.activity_code_un();
            default:
                return Messages_en.activity_code_un();
        }
    }

    public static activity_name_un(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.activity_name_un();
            case Language.en:
                return Messages_en.activity_name_un();
            default:
                return Messages_en.activity_name_un();
        }
    }

    public static cover_type_code_un(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.cover_type_code_un();
            case Language.en:
                return Messages_en.cover_type_code_un();
            default:
                return Messages_en.cover_type_code_un();
        }
    }

    public static cover_type_name_un(): string {
        switch (Messages.activeLang) {
            case Language.el:
                return Messages_el.cover_type_name_un();
            case Language.en:
                return Messages_en.cover_type_name_un();
            default:
                return Messages_en.cover_type_name_un();
        }
    }


    public static dynamicMessage(msgKey: string) {
        var items = msgKey.split('%%');
        var results = _.map(items, s => {
            try {
                var methodName: string = "";
                var _paramsStartIndex: number = s.indexOf("(");
                var _paramsEndIndex: number = s.indexOf(")");
                if (_paramsStartIndex > 0 && _paramsEndIndex > _paramsStartIndex) {
                    methodName =
                        s.substring(0, _paramsStartIndex).replace(/\./g, "_") + s.substring(_paramsStartIndex);
                } else {
                    methodName = s.replace(/\./g, "_");
                }
                var m = eval("Messages." + methodName);
                if (typeof m === "function") {
                    return eval("Messages." + methodName + "()");
                }
                if (isVoid(m)) { return s;}
                return m;
            }
            catch(ex) { return s;}   
        })
        return results.join('<br>');
    }

    public static getActiveLangString(key: string, asHtml: boolean = false) {
        if (isVoid(key))
            return key;
        var ret: string;
        switch (Messages.activeLang) {
            case Language.el:
                ret = Messages_el.strings[key];
                break;
            case Language.en:
                ret = Messages_en.strings[key];
                break;
            default:
                ret = Messages_en.strings[key];
        }
        if (isVoid(ret))
            ret = key;
        return asHtml ? ret.replace(new RegExp("\n", 'g'), "<br/>").replace(new RegExp(" ", 'g'), "&#160;") : ret;
    }
}

Messages_el.strings = {
}
Messages_en.strings = {
    "AppVersionLabel": "Application's Last Update",
    "VersionLabel": "Last Update",
    "Login": "Login",
    "User": "User",
    "Password": "Password",
    "Change Password": "Change Password",
    "Logout": "Logout",
    "Application Menu": "Application Menu",
    "Logged-in User": "Logged-in User",
    "Login Error": "Login Error",
    "Save": "Save",
    "Delete": "Delete",
    "New Record": "New Record",
    "Records": "Records",
    "Back": "Back",
    "Show Filters": "Show Filters",
    "Multiple Insert": "Multiple Insert",
    "Page Info": "Page Info",
    "Records:": "Records:",
    "BreadCrumb_Home_Label": "Home",
    "ExitAppQuestion": "Are you sure you want to Exit?",
    "ExitAppDialogTitle": "Exit",
    "Search": "Search",
    "Clear": "Clear",
    "Search Criteria": "Search Criteria",
    "Audit Info of Record": "Audit Info of Record",
    "Insert User": "Insert User",
    "Insert Timestamp": "Insert Timestamp",
    "Last Update User": "Last Update User",
    "Last Update Timestamp": "Last Update Timestamp",
    "Row\nNum": "Row\nNum",
    "Error\nMessage": "Error\nMessage",
    "The Geometries": "The Geometries",
    "WaitDialog_Title": "Loading",
    "WaitDialog_Message": "Please Wait...",
    "LovButtons_Select": "Select (F12)",
    "LovButtons_Clear": "Clear",
    "LovButtons_New": "New Record",
    "ImportExcel_MessageBox_Info_Title": "Import Excel File Info",
    "ImportExcel_MessageBox_Info_Message": "Click 'Download' to get the excel file with the 'Template' and the 'Specifications', which the file must meet in order to complete the import successfully.",
    "ImportExcel_MessageBox_Info_ButtonLabel": "Download",
    "ImportExcel_MessageBox_Error_Message": "The import of the file failed because some records had errors.",
    "ImportExcel_MessageBox_Error_ButtonLabel": "Show Errors",
    "ImportExvel_ResultsDlg_Title": "Excel File Import Results",
    "ImportExvel_ResultsDlg_File": "File",
    "ImportExvel_ResultsDlg_File_NumRecords": "Number of\nFile lines",
    "ImportExvel_ResultsDlg_File_NumErrorRecords": "Number of lines\nwith errors",
    "ImportExvel_ResultsDlg_GridTitle": "Errors",
    "MessageBox_Success_Title": "Success",
    "MessageBox_Attention_Title": "Attention",
    "MessageBox_Error_Title": "Error",
    "MessageBox_MultipleInsertError_Title": "Multiple Insertion Error",
    "ChangePasswdDialog_Title": "Password Change",
    "ChangePasswdDialog_user_label": "User",
    "ChangePasswdDialog_oldpass_label": "Old password",
    "ChangePasswdDialog_newpass1_label": "New password",
    "ChangePasswdDialog_newpass2_label": "New password (repeat)",
    "ChangePasswdDialog_set_button": "Set new password",
    "ChangePasswdDialog_back_button": "Back",
    "ChangePasswdDialog_ErrorLabel": "Error",
    "HelpDialog_Title": "Help",
    "HelpDialog_Heading": "Handling",
    "HelpDialog_F1": "Show Help",
    "HelpDialog_Ctrl_s": "Save",
    "HelpDialog_F9": "Create New Record",
    "HelpDialog_F10": "Focus on next table",
    "HelpDialog_Shift_F10": "Focus on previous table",
    "HelpDialog_F12": "Open/close list of values",
    "HelpDialog_Enter_Tab": "Focus on next field",
    "HelpDialog_Shift_tab": "Focus on previous field",
    "HelpDialog_Esc": "Back/Close dialog",
    "HelpDialog_Double_click": "Open calendar on date fields",
    "HelpDialog_back_button": "Back",
    "MessageBox_Button_Yes": "Yes",
    "MessageBox_Button_No": "No",
    "MessageBox_Button_Cancel": "Cancel",
    "MessageBox_Blob_FileType_Title": "Unacceptable file type!",
    "MessageBox_Blob_FileSize_Title": "Unacceptable file size!",
    "DropDownList_ChooseOne": "Choose one",
    "ExportToExcelButtonHint": "Export to Excel file"
}
