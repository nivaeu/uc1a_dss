//0AC14049FFE5AC045D0F6C91E1C65ED4
//Εxtended classes
/// <reference path="../EntitiesBase/FmisUserBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var FmisUser = (function (_super) {
        __extends(FmisUser, _super);
        function FmisUser() {
            _super.apply(this, arguments);
        }
        FmisUser.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.FmisUserBase.fromJSONComplete(data, deserializedEntities);
        };
        return FmisUser;
    })(Entities.FmisUserBase);
    Entities.FmisUser = FmisUser;
})(Entities || (Entities = {}));
//# sourceMappingURL=FmisUser.js.map