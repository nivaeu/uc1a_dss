//83BC4A74FD68D3C57FF3A0EEEE7B63AD
//Εxtended classes
/// <reference path="../EntitiesBase/EcCultCopyBase.ts" />

module Entities {

    export class EcCultCopy extends Entities.EcCultCopyBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<EcCultCopy> {
            return Entities.EcCultCopyBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
