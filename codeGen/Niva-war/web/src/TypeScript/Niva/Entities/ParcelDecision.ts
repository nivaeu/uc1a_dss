//52D2E6DC036609E5B2009F7E47994BAA
//Εxtended classes
/// <reference path="../EntitiesBase/ParcelDecisionBase.ts" />

module Entities {

    export class ParcelDecision extends Entities.ParcelDecisionBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<ParcelDecision> {
            return Entities.ParcelDecisionBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
