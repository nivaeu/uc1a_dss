//1A4BC602E30DBD9970EEBC870E5F88A8
//Εxtended classes
/// <reference path="../EntitiesBase/FileTemplateBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var FileTemplate = (function (_super) {
        __extends(FileTemplate, _super);
        function FileTemplate() {
            _super.apply(this, arguments);
        }
        FileTemplate.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.FileTemplateBase.fromJSONComplete(data, deserializedEntities);
        };
        return FileTemplate;
    })(Entities.FileTemplateBase);
    Entities.FileTemplate = FileTemplate;
})(Entities || (Entities = {}));
//# sourceMappingURL=FileTemplate.js.map