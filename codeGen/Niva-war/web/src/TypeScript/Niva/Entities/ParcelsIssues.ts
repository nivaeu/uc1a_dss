//996CBA9563095D58D1256540E7DC5808
//Εxtended classes
/// <reference path="../EntitiesBase/ParcelsIssuesBase.ts" />

module Entities {

    export class ParcelsIssues extends Entities.ParcelsIssuesBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<ParcelsIssues> {
            return Entities.ParcelsIssuesBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
