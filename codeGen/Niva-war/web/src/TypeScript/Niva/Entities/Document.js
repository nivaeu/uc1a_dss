//1109DF6A0AE8BF72A5FFB15706754C3E
//Εxtended classes
/// <reference path="../EntitiesBase/DocumentBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var Document = (function (_super) {
        __extends(Document, _super);
        function Document() {
            _super.apply(this, arguments);
        }
        Document.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.DocumentBase.fromJSONComplete(data, deserializedEntities);
        };
        return Document;
    })(Entities.DocumentBase);
    Entities.Document = Document;
})(Entities || (Entities = {}));
//# sourceMappingURL=Document.js.map