//1D330186A0A22CF942771EB1B15BC664
//Εxtended classes
/// <reference path="../EntitiesBase/CultivationBase.ts" />

module Entities {

    export class Cultivation extends Entities.CultivationBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Cultivation> {
            return Entities.CultivationBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
