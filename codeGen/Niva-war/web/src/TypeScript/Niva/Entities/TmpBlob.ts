//63041AD5E549498F89CC1BA6F0E2F280
//Εxtended classes
/// <reference path="../EntitiesBase/TmpBlobBase.ts" />

module Entities {

    export class TmpBlob extends Entities.TmpBlobBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<TmpBlob> {
            return Entities.TmpBlobBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
