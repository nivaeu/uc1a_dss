//0FFD49D2400DDDD038E9EB98547369D2
//Εxtended classes
/// <reference path="../EntitiesBase/StatisticBase.ts" />

module Entities {

    export class Statistic extends Entities.StatisticBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Statistic> {
            return Entities.StatisticBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
