//8D57FFB7AE38A742A550B99E8DC72680
//Εxtended classes
/// <reference path="../EntitiesBase/GpUploadBase.ts" />

module Entities {

    export class GpUpload extends Entities.GpUploadBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<GpUpload> {
            return Entities.GpUploadBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
