//AB9101FC56873AB5E2CF4A361D44FD73
//Εxtended classes
/// <reference path="../EntitiesBase/IntegrateddecisionBase.ts" />

module Entities {

    export class Integrateddecision extends Entities.IntegrateddecisionBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Integrateddecision> {
            return Entities.IntegrateddecisionBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
