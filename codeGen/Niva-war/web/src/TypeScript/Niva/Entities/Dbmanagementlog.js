//8981DD36E2C64B40F2C71D4514612C92
//Εxtended classes
/// <reference path="../EntitiesBase/DbmanagementlogBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var Dbmanagementlog = (function (_super) {
        __extends(Dbmanagementlog, _super);
        function Dbmanagementlog() {
            _super.apply(this, arguments);
        }
        Dbmanagementlog.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.DbmanagementlogBase.fromJSONComplete(data, deserializedEntities);
        };
        return Dbmanagementlog;
    })(Entities.DbmanagementlogBase);
    Entities.Dbmanagementlog = Dbmanagementlog;
})(Entities || (Entities = {}));
//# sourceMappingURL=Dbmanagementlog.js.map