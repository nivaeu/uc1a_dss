//878BEBA3CA4ECA36AE58A14BA4F2AF83
//Εxtended classes
/// <reference path="../EntitiesBase/DecisionMakingBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var DecisionMaking = (function (_super) {
        __extends(DecisionMaking, _super);
        function DecisionMaking() {
            _super.apply(this, arguments);
        }
        DecisionMaking.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.DecisionMakingBase.fromJSONComplete(data, deserializedEntities);
        };
        return DecisionMaking;
    })(Entities.DecisionMakingBase);
    Entities.DecisionMaking = DecisionMaking;
})(Entities || (Entities = {}));
//# sourceMappingURL=DecisionMaking.js.map