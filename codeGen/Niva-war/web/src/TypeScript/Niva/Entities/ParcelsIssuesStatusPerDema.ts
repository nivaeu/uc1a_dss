//D91DACA9547C6E73E350E892D310674F
//Εxtended classes
/// <reference path="../EntitiesBase/ParcelsIssuesStatusPerDemaBase.ts" />

module Entities {

    export class ParcelsIssuesStatusPerDema extends Entities.ParcelsIssuesStatusPerDemaBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<ParcelsIssuesStatusPerDema> {
            return Entities.ParcelsIssuesStatusPerDemaBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
