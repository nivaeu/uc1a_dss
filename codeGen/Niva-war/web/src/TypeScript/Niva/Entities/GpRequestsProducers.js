//6815A9E961B863277EDDD671329E27EE
//Εxtended classes
/// <reference path="../EntitiesBase/GpRequestsProducersBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var GpRequestsProducers = (function (_super) {
        __extends(GpRequestsProducers, _super);
        function GpRequestsProducers() {
            _super.apply(this, arguments);
        }
        GpRequestsProducers.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.GpRequestsProducersBase.fromJSONComplete(data, deserializedEntities);
        };
        return GpRequestsProducers;
    })(Entities.GpRequestsProducersBase);
    Entities.GpRequestsProducers = GpRequestsProducers;
})(Entities || (Entities = {}));
//# sourceMappingURL=GpRequestsProducers.js.map