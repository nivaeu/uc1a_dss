//06448CD982E32B903599BB46B97B1281
//Εxtended classes
/// <reference path="../EntitiesBase/AgrisnapUsersProducersBase.ts" />

module Entities {

    export class AgrisnapUsersProducers extends Entities.AgrisnapUsersProducersBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<AgrisnapUsersProducers> {
            return Entities.AgrisnapUsersProducersBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
