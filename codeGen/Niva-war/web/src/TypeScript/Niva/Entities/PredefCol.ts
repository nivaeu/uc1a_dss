//84E9096A7EF038441E4BE92AFB07481E
//Εxtended classes
/// <reference path="../EntitiesBase/PredefColBase.ts" />

module Entities {

    export class PredefCol extends Entities.PredefColBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<PredefCol> {
            return Entities.PredefColBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
