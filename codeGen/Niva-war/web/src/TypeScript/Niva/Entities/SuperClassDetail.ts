//AB55F02D962DE26DCC82D5E48C7D1CC4
//Εxtended classes
/// <reference path="../EntitiesBase/SuperClassDetailBase.ts" />

module Entities {

    export class SuperClassDetail extends Entities.SuperClassDetailBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<SuperClassDetail> {
            return Entities.SuperClassDetailBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
