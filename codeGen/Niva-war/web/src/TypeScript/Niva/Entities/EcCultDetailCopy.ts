//7F6E780EF7A934B868DE19CDE0B9A795
//Εxtended classes
/// <reference path="../EntitiesBase/EcCultDetailCopyBase.ts" />

module Entities {

    export class EcCultDetailCopy extends Entities.EcCultDetailCopyBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<EcCultDetailCopy> {
            return Entities.EcCultDetailCopyBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
