//DE0927D8627BCB8144CD9F45A87C9685
//Εxtended classes
/// <reference path="../EntitiesBase/CoverTypeBase.ts" />

module Entities {

    export class CoverType extends Entities.CoverTypeBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<CoverType> {
            return Entities.CoverTypeBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
