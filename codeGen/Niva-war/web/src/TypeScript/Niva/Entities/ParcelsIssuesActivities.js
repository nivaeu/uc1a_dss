//63FD56D735E84F419E755E23BF0BB296
//Εxtended classes
/// <reference path="../EntitiesBase/ParcelsIssuesActivitiesBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var ParcelsIssuesActivities = (function (_super) {
        __extends(ParcelsIssuesActivities, _super);
        function ParcelsIssuesActivities() {
            _super.apply(this, arguments);
        }
        ParcelsIssuesActivities.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.ParcelsIssuesActivitiesBase.fromJSONComplete(data, deserializedEntities);
        };
        return ParcelsIssuesActivities;
    })(Entities.ParcelsIssuesActivitiesBase);
    Entities.ParcelsIssuesActivities = ParcelsIssuesActivities;
})(Entities || (Entities = {}));
//# sourceMappingURL=ParcelsIssuesActivities.js.map