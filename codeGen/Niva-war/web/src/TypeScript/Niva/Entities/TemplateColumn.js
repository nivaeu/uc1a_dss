//8BC5DADB19580B5822BD906B89FC71B9
//Εxtended classes
/// <reference path="../EntitiesBase/TemplateColumnBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var TemplateColumn = (function (_super) {
        __extends(TemplateColumn, _super);
        function TemplateColumn() {
            _super.apply(this, arguments);
        }
        TemplateColumn.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.TemplateColumnBase.fromJSONComplete(data, deserializedEntities);
        };
        return TemplateColumn;
    })(Entities.TemplateColumnBase);
    Entities.TemplateColumn = TemplateColumn;
})(Entities || (Entities = {}));
//# sourceMappingURL=TemplateColumn.js.map