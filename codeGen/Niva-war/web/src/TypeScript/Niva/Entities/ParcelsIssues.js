//996CBA9563095D58D1256540E7DC5808
//Εxtended classes
/// <reference path="../EntitiesBase/ParcelsIssuesBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var ParcelsIssues = (function (_super) {
        __extends(ParcelsIssues, _super);
        function ParcelsIssues() {
            _super.apply(this, arguments);
        }
        ParcelsIssues.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.ParcelsIssuesBase.fromJSONComplete(data, deserializedEntities);
        };
        return ParcelsIssues;
    })(Entities.ParcelsIssuesBase);
    Entities.ParcelsIssues = ParcelsIssues;
})(Entities || (Entities = {}));
//# sourceMappingURL=ParcelsIssues.js.map