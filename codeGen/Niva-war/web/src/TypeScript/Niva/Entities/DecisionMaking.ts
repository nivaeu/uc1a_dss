//878BEBA3CA4ECA36AE58A14BA4F2AF83
//Εxtended classes
/// <reference path="../EntitiesBase/DecisionMakingBase.ts" />

module Entities {

    export class DecisionMaking extends Entities.DecisionMakingBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<DecisionMaking> {
            return Entities.DecisionMakingBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
