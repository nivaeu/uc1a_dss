//0AC14049FFE5AC045D0F6C91E1C65ED4
//Εxtended classes
/// <reference path="../EntitiesBase/FmisUserBase.ts" />

module Entities {

    export class FmisUser extends Entities.FmisUserBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<FmisUser> {
            return Entities.FmisUserBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
