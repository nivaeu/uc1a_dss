//AB55F02D962DE26DCC82D5E48C7D1CC4
//Εxtended classes
/// <reference path="../EntitiesBase/SuperClassDetailBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var SuperClassDetail = (function (_super) {
        __extends(SuperClassDetail, _super);
        function SuperClassDetail() {
            _super.apply(this, arguments);
        }
        SuperClassDetail.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.SuperClassDetailBase.fromJSONComplete(data, deserializedEntities);
        };
        return SuperClassDetail;
    })(Entities.SuperClassDetailBase);
    Entities.SuperClassDetail = SuperClassDetail;
})(Entities || (Entities = {}));
//# sourceMappingURL=SuperClassDetail.js.map