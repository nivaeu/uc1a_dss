//6815A9E961B863277EDDD671329E27EE
//Εxtended classes
/// <reference path="../EntitiesBase/GpRequestsProducersBase.ts" />

module Entities {

    export class GpRequestsProducers extends Entities.GpRequestsProducersBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<GpRequestsProducers> {
            return Entities.GpRequestsProducersBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
