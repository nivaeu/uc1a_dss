//925BC14783A547EE944145A81A3CD0A1
//Εxtended classes
/// <reference path="../EntitiesBase/EcGroupBase.ts" />

module Entities {

    export class EcGroup extends Entities.EcGroupBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<EcGroup> {
            return Entities.EcGroupBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
