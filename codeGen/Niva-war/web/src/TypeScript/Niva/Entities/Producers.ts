//67482CCA7D76EDC69BE72E1317DB9B50
//Εxtended classes
/// <reference path="../EntitiesBase/ProducersBase.ts" />

module Entities {

    export class Producers extends Entities.ProducersBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Producers> {
            return Entities.ProducersBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
