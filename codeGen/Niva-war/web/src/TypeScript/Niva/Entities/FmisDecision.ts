//9D34200D5BC4FC4596F1DE9220D4AFCE
//Εxtended classes
/// <reference path="../EntitiesBase/FmisDecisionBase.ts" />

module Entities {

    export class FmisDecision extends Entities.FmisDecisionBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<FmisDecision> {
            return Entities.FmisDecisionBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
