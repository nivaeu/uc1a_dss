//1A4BC602E30DBD9970EEBC870E5F88A8
//Εxtended classes
/// <reference path="../EntitiesBase/FileTemplateBase.ts" />

module Entities {

    export class FileTemplate extends Entities.FileTemplateBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<FileTemplate> {
            return Entities.FileTemplateBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
