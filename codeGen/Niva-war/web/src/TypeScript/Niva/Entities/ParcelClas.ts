//AC42F303F23F1D9A5E0285EB17948378
//Εxtended classes
/// <reference path="../EntitiesBase/ParcelClasBase.ts" />

module Entities {

    export class ParcelClas extends Entities.ParcelClasBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<ParcelClas> {
            return Entities.ParcelClasBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
