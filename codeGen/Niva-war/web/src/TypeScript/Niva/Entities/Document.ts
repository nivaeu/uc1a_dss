//1109DF6A0AE8BF72A5FFB15706754C3E
//Εxtended classes
/// <reference path="../EntitiesBase/DocumentBase.ts" />

module Entities {

    export class Document extends Entities.DocumentBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Document> {
            return Entities.DocumentBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
