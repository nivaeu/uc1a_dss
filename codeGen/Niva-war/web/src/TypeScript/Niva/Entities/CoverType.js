//DE0927D8627BCB8144CD9F45A87C9685
//Εxtended classes
/// <reference path="../EntitiesBase/CoverTypeBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var CoverType = (function (_super) {
        __extends(CoverType, _super);
        function CoverType() {
            _super.apply(this, arguments);
        }
        CoverType.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.CoverTypeBase.fromJSONComplete(data, deserializedEntities);
        };
        return CoverType;
    })(Entities.CoverTypeBase);
    Entities.CoverType = CoverType;
})(Entities || (Entities = {}));
//# sourceMappingURL=CoverType.js.map