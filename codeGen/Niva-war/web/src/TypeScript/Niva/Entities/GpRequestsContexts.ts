//588E0044C92B5C55BADB13B586228E44
//Εxtended classes
/// <reference path="../EntitiesBase/GpRequestsContextsBase.ts" />

module Entities {

    export class GpRequestsContexts extends Entities.GpRequestsContextsBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<GpRequestsContexts> {
            return Entities.GpRequestsContextsBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
