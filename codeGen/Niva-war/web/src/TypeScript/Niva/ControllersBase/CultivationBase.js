var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/CultivationBase.ts" />
/// <reference path="../EntitiesBase/CoverTypeBase.ts" />
/// <reference path="../Controllers/Cultivation.ts" />
var Controllers;
(function (Controllers) {
    var Cultivation;
    (function (Cultivation) {
        var PageModelBase = (function (_super) {
            __extends(PageModelBase, _super);
            function PageModelBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(PageModelBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return PageModelBase;
        })(Controllers.AbstractPageModel);
        Cultivation.PageModelBase = PageModelBase;
        var PageControllerBase = (function (_super) {
            __extends(PageControllerBase, _super);
            function PageControllerBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelGroupControllers = undefined;
                this._allGroupControllers = undefined;
                var self = this;
                model.controller = self;
                $scope.pageModel = self.model;
                $scope.HeaderSection_0_disabled =
                    function () {
                        return self.HeaderSection_0_disabled();
                    };
                $scope.HeaderSection_0_invisible =
                    function () {
                        return self.HeaderSection_0_invisible();
                    };
                $scope.createFileUploadComponent_Cultivation_impCults_id =
                    function () {
                        return self.createFileUploadComponent_Cultivation_impCults_id();
                    };
                $scope.HeaderSection_0_1_disabled =
                    function () {
                        return self.HeaderSection_0_1_disabled();
                    };
                $scope.deleteSelectedCrops =
                    function () {
                        self.deleteSelectedCrops();
                    };
                $scope._saveIsDisabled =
                    function () {
                        return self._saveIsDisabled();
                    };
                $scope._cancelIsDisabled =
                    function () {
                        return self._cancelIsDisabled();
                    };
                $scope.onSaveBtnAction = function () {
                    self.onSaveBtnAction(function (response) { self.onSuccesfullSaveFromButton(response); });
                };
                $timeout(function () {
                    $('#Cultivation').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                    $('#NpMainContent').scrollTop(0);
                }, 0);
            }
            Object.defineProperty(PageControllerBase.prototype, "ControllerClassName", {
                get: function () {
                    return "PageController";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "HtmlDivId", {
                get: function () {
                    return "Cultivation";
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._getPageTitle = function () {
                return "Crops List";
            };
            Object.defineProperty(PageControllerBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.HeaderSection_0_disabled = function () {
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            PageControllerBase.prototype.HeaderSection_0_invisible = function () {
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            PageControllerBase.prototype.impCults_id_disabled = function () {
                var self = this;
                if (!self.$scope.globals.hasPrivilege("Niva_Cultivation_W"))
                    return true; // 
                var isContainerControlDisabled = self.HeaderSection_0_disabled();
                var disabledByProgrammerMethod = self.isExcelDisabled();
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            PageControllerBase.prototype.isExcelDisabled = function () {
                console.warn("Unimplemented function isExcelDisabled()");
                return false;
            };
            PageControllerBase.prototype.createFileUploadComponent_Cultivation_impCults_id = function () {
                var self = this;
                var ret = new NpTypes.NpFileUpload(
                /*formDataAppendCallback:*/
                /*formDataAppendCallback:*/
                function (fd) {
                }, 
                /*fileUpLoadedCallback:*/
                /*fileUpLoadedCallback:*/
                function (fileName, uploadResponseText) {
                    var jsonResponse = JSON.parse(uploadResponseText);
                    if (jsonResponse.totalErrorRows === 0) {
                        messageBox(self.$scope, self.Plato, "MessageBox_Success_Title", Messages.ImportExcel_MessageBox_Success_Message(fileName, jsonResponse.totalRows), IconKind.INFO, [new Tuple2("OK", function () { }),], 0, 0, '30em');
                        self.successUpdate(fileName, jsonResponse);
                    }
                    else {
                        messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", self.$scope.getALString("ImportExcel_MessageBox_Error_Message"), IconKind.ERROR, [new Tuple2("ImportExcel_MessageBox_Error_ButtonLabel", function () {
                                Utils.showExcelFileImportResultsDlg(self.$scope, self.Plato, jsonResponse.excelFileId, "ExcelFile", "ExcelError");
                            }),], 0, 0, '30em');
                    }
                }, 
                /*getLabel:*/ 
                /*getLabel:*/ function () { return "Import From Excel File"; }, 
                /*isDisabled:*/ 
                /*isDisabled:*/ function () { return self.impCults_id_disabled(); }, 
                /*isInvisible:*/ 
                /*isInvisible:*/ function () { return false; }, 
                /*postURL:*/ "/Niva/rest/MainService/importExcel_Cultivation_impCults_id", 
                /*helpButtonAction*/
                /*helpButtonAction*/
                function () {
                    messageBox(self.$scope, self.Plato, "ImportExcel_MessageBox_Info_Title", self.$scope.getALString("ImportExcel_MessageBox_Info_Message"), IconKind.INFO, [new Tuple2("ImportExcel_MessageBox_Info_ButtonLabel", function () {
                            self.getImportExcel_Cultivation_impCults_id_SpecsAndSample();
                        }),], 0, 0, '30em');
                }, 
                /*extensions:*/ "xls|xlsx", 
                /*maximumSizeInKB:*/ 10240);
                return ret;
            };
            PageControllerBase.prototype.successUpdate = function (fileName, jsonResponse) {
                console.warn("Unimplemented function successUpdate()");
            };
            PageControllerBase.prototype.getImportExcel_Cultivation_impCults_id_SpecsAndSample = function () {
                var self = this;
                var wsPath = "MainService/importExcel_Cultivation_impCults_id_SpecsAndSample";
                var url = "/Niva/rest/" + wsPath;
                var paramData = {};
                paramData['globalLang'] = self.$scope.globals.globalLang;
                var filename = "Niva_Cultivation_SpecsAndSample_" + Utils.convertDateToString(new Date(), "yyyy_MM_dd_HHmm") + ".xls";
                NpTypes.AlertMessage.clearAlerts(self.PageModel);
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    var data = base64DecToArr(response.data);
                    var blob = new Blob([data], { type: "application/octet-stream" });
                    saveAs(blob, filename);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                });
            };
            PageControllerBase.prototype.HeaderSection_0_1_disabled = function () {
                var self = this;
                var isContainerControlDisabled = self.HeaderSection_0_disabled();
                var disabledByProgrammerMethod = self.isDeleteSelectedCropsDisabled();
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            PageControllerBase.prototype.isDeleteSelectedCropsDisabled = function () {
                console.warn("Unimplemented function isDeleteSelectedCropsDisabled()");
                return false;
            };
            PageControllerBase.prototype.deleteSelectedCrops = function () {
                var self = this;
                console.log("Method: deleteSelectedCrops() called");
            };
            PageControllerBase.prototype._saveIsDisabled = function () {
                var self = this;
                if (!self.$scope.globals.hasPrivilege("Niva_Cultivation_W"))
                    return true; // 
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            PageControllerBase.prototype._cancelIsDisabled = function () {
                var self = this;
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            Object.defineProperty(PageControllerBase.prototype, "pageModel", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.update = function () {
                var self = this;
                self.model.modelGrpCultivation.controller.updateUI();
            };
            PageControllerBase.prototype.refreshVisibleEntities = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpCultivation.controller.refreshVisibleEntities(newEntitiesIds);
            };
            PageControllerBase.prototype.refreshGroups = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpCultivation.controller.refreshVisibleEntities(newEntitiesIds);
            };
            Object.defineProperty(PageControllerBase.prototype, "FirstLevelGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._firstLevelGroupControllers === undefined) {
                        self._firstLevelGroupControllers = [
                            self.model.modelGrpCultivation.controller
                        ];
                    }
                    return this._firstLevelGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "AllGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._allGroupControllers === undefined) {
                        self._allGroupControllers = [
                            self.model.modelGrpCultivation.controller
                        ];
                    }
                    return this._allGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.getPageChanges = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var pageChanges = [];
                pageChanges = pageChanges.concat(self.model.modelGrpCultivation.controller.getChangesToCommit());
                return pageChanges;
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbUrl = function () {
                return "/Niva/rest/MainService/synchronizeChangesWithDb_Cultivation";
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbData = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var paramData = {};
                paramData.data = self.getPageChanges(bForDelete);
                return paramData;
            };
            PageControllerBase.prototype.onSuccesfullSaveFromButton = function (response) {
                var self = this;
                _super.prototype.onSuccesfullSaveFromButton.call(this, response);
                if (!isVoid(response.warningMessages)) {
                    NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
                }
                if (!isVoid(response.infoMessages)) {
                    NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
                }
                self.model.modelGrpCultivation.controller.cleanUpAfterSave();
                self.$scope.globals.isCurrentTransactionDirty = false;
                self.refreshGroups(response.newEntitiesIds);
            };
            PageControllerBase.prototype.onFailuredSave = function (data, status) {
                var self = this;
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                var errors = Messages.dynamicMessage(data);
                NpTypes.AlertMessage.addDanger(self.model, errors);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
            };
            PageControllerBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            PageControllerBase.prototype.onSaveBtnAction = function (onSuccess) {
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                var errors = self.validatePage();
                if (errors.length > 0) {
                    var errMessage = errors.join('<br>');
                    NpTypes.AlertMessage.addDanger(self.model, errMessage);
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
                    return;
                }
                if (self.$scope.globals.isCurrentTransactionDirty === false) {
                    var jqSaveBtn = self.SaveBtnJQueryHandler;
                    self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                    return;
                }
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData();
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status);
                });
            };
            PageControllerBase.prototype.onPageUnload = function (actualNavigation) {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                actualNavigation(self.$scope);
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    actualNavigation(self.$scope);
                }
            };
            return PageControllerBase;
        })(Controllers.AbstractPageController);
        Cultivation.PageControllerBase = PageControllerBase;
        // GROUP GrpCultivation
        var ModelGrpCultivationBase = (function (_super) {
            __extends(ModelGrpCultivationBase, _super);
            function ModelGrpCultivationBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
                this._fsch_name = new NpTypes.UIStringModel(undefined);
                this._fsch_code = new NpTypes.UINumberModel(undefined);
                this._fsch_cotyId = new NpTypes.UIManyToOneModel(undefined);
            }
            Object.defineProperty(ModelGrpCultivationBase.prototype, "cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cultId;
                },
                set: function (cultId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cultId = cultId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "_cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cultId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "name", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].name;
                },
                set: function (name_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].name = name_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "_name", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._name;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "code", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].code;
                },
                set: function (code_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].code = code_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "_code", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._code;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cotyId;
                },
                set: function (cotyId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cotyId = cotyId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "_cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cotyId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "exfiId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].exfiId;
                },
                set: function (exfiId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].exfiId = exfiId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "_exfiId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._exfiId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "fsch_name", {
                get: function () {
                    return this._fsch_name.value;
                },
                set: function (vl) {
                    this._fsch_name.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "fsch_code", {
                get: function () {
                    return this._fsch_code.value;
                },
                set: function (vl) {
                    this._fsch_code.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "fsch_cotyId", {
                get: function () {
                    return this._fsch_cotyId.value;
                },
                set: function (vl) {
                    this._fsch_cotyId.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpCultivationBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpCultivationBase;
        })(Controllers.AbstractGroupTableModel);
        Cultivation.ModelGrpCultivationBase = ModelGrpCultivationBase;
        var ControllerGrpCultivationBase = (function (_super) {
            __extends(ControllerGrpCultivationBase, _super);
            function ControllerGrpCultivationBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 20, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = undefined;
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                this.GrpCultivation_srchr__fsch_cotyIdCache = new ExpiredArrayResource();
                this.GrpCultivation_itm__cotyIdCache = new ExpiredArrayResource();
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpCultivation = self.model;
                $scope._disabled =
                    function () {
                        return self._disabled();
                    };
                $scope._invisible =
                    function () {
                        return self._invisible();
                    };
                $scope.GrpCultivation_srchr__fsch_name_disabled =
                    function (cultivation) {
                        return self.GrpCultivation_srchr__fsch_name_disabled(cultivation);
                    };
                $scope.GrpCultivation_srchr__fsch_code_disabled =
                    function (cultivation) {
                        return self.GrpCultivation_srchr__fsch_code_disabled(cultivation);
                    };
                $scope.GrpCultivation_srchr__fsch_cotyId_disabled =
                    function (cultivation) {
                        return self.GrpCultivation_srchr__fsch_cotyId_disabled(cultivation);
                    };
                $scope.getDropDownItems_GrpCultivation_srchr__fsch_cotyId =
                    function () {
                        return self.getDropDownItems_GrpCultivation_srchr__fsch_cotyId();
                    };
                $scope.GrpCultivation_itm__name_disabled =
                    function (cultivation) {
                        return self.GrpCultivation_itm__name_disabled(cultivation);
                    };
                $scope.GrpCultivation_itm__code_disabled =
                    function (cultivation) {
                        return self.GrpCultivation_itm__code_disabled(cultivation);
                    };
                $scope.GrpCultivation_itm__cotyId_disabled =
                    function (cultivation) {
                        return self.GrpCultivation_itm__cotyId_disabled(cultivation);
                    };
                $scope.getDropDownItems_GrpCultivation_itm__cotyId =
                    function (cultivation) {
                        return self.getDropDownItems_GrpCultivation_itm__cotyId(cultivation);
                    };
                $scope.pageModel.modelGrpCultivation = $scope.modelGrpCultivation;
                $scope.clearBtnAction = function () {
                    self.modelGrpCultivation.fsch_name = undefined;
                    self.modelGrpCultivation.fsch_code = undefined;
                    self.modelGrpCultivation.fsch_cotyId = undefined;
                    self.updateGrid(0, false, true);
                };
                self.$timeout(function () {
                    self.updateUI();
                }, 0);
                /*
                        $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                            $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.Cultivation[] , oldVisible:Entities.Cultivation[]) => {
                                console.log("visible entities changed",newVisible);
                                self.onVisibleItemsChange(newVisible, oldVisible);
                            } )  ));
                */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            ControllerGrpCultivationBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpCultivationBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpCultivation";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpCultivationBase.prototype, "HtmlDivId", {
                get: function () {
                    return "Cultivation_ControllerGrpCultivation";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpCultivationBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.TextItem(function (ent) { return 'Name'; }, true, function (ent) { return self.model.fsch_name; }, function (ent) { return self.model._fsch_name; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.NumberItem(function (ent) { return 'Code'; }, true, function (ent) { return self.model.fsch_code; }, function (ent) { return self.model._fsch_code; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 0),
                            new Controllers.LovItem(function (ent) { return 'Cover Type'; }, true, function (ent) { return self.model.fsch_cotyId; }, function (ent) { return self.model._fsch_cotyId; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.TextItem(function (ent) { return 'Crop Name'; }, false, function (ent) { return ent.name; }, function (ent) { return ent._name; }, function (ent) { return true; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.NumberItem(function (ent) { return 'Code'; }, false, function (ent) { return ent.code; }, function (ent) { return ent._code; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 0),
                            new Controllers.LovItem(function (ent) { return 'Land Cover Name'; }, false, function (ent) { return ent.cotyId; }, function (ent) { return ent._cotyId; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); })
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpCultivationBase.prototype.gridTitle = function () {
                return "Crops List";
            };
            ControllerGrpCultivationBase.prototype.isMultiSelect = function () {
                return true;
            };
            ControllerGrpCultivationBase.prototype.getCheckedEntities = function (func) {
                var ids = this.getCheckedEntitiesIds();
                if (ids.length > 0) {
                    Entities.Cultivation.getEntitiesFromIdsList(this, ids, function (entList) {
                        func(entList);
                    });
                }
                else {
                    func([]);
                }
            };
            ControllerGrpCultivationBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpCultivationBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { width: '22', cellTemplate: "<div class=\"GridSpecialCheckBox\" ><input class=\"npGridSelectCheckBox\" type=\"checkbox\" data-ng-click=\"setSelectedRow(row.rowIndex);selectEntityCheckBoxClicked(row.entity)\" data-ng-checked=\"isEntitySelected(row.entity)\" > </input></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'name', displayName: 'getALString("Crop Name", true)', requiredAsterisk: self.$scope.globals.hasPrivilege("Niva_Cultivation_W"), resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpCultivation_itm__name' data-ng-model='row.entity.name' data-np-ui-model='row.entity._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;name&quot;)' data-np-required='true' data-ng-readonly='GrpCultivation_itm__name_disabled(row.entity)' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'code', displayName: 'getALString("Code", true)', requiredAsterisk: self.$scope.globals.hasPrivilege("Niva_Cultivation_W"), resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpCultivation_itm__code' data-ng-model='row.entity.code' data-np-ui-model='row.entity._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;code&quot;)' data-np-required='true' data-ng-readonly='GrpCultivation_itm__code_disabled(row.entity)' data-np-number='dummy' data-np-decimals='0' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cotyId', displayName: 'getALString("Land Cover Name", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='GrpCultivation_itm__cotyId' data-ng-model='row.entity.cotyId' data-np-ui-model='row.entity._cotyId' data-ng-change='markEntityAsUpdated(row.entity,&quot;cotyId&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in getDropDownItems_GrpCultivation_itm__cotyId(row.entity) track by x.cotyId' data-ng-disabled='GrpCultivation_itm__cotyId_disabled(row.entity)' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpCultivationBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpCultivationBase.prototype, "modelGrpCultivation", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpCultivationBase.prototype.getEntityName = function () {
                return "Cultivation";
            };
            ControllerGrpCultivationBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = undefined;
            };
            ControllerGrpCultivationBase.prototype.getEntitiesFromJSON = function (webResponse) {
                var _this = this;
                var entlist = Entities.Cultivation.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpCultivationBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpCultivation.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpCultivationBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return false;
            };
            ControllerGrpCultivationBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_name)) {
                    paramData['fsch_name'] = self.modelGrpCultivation.fsch_name;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_code)) {
                    paramData['fsch_code'] = self.modelGrpCultivation.fsch_code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_cotyId) && !isVoid(self.modelGrpCultivation.fsch_cotyId.cotyId)) {
                    paramData['fsch_cotyId_cotyId'] = self.modelGrpCultivation.fsch_cotyId.cotyId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpCultivationBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "Cultivation/findAllByCriteriaRange_CultivationGrpCultivation_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_name)) {
                    paramData['fsch_name'] = self.modelGrpCultivation.fsch_name;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_code)) {
                    paramData['fsch_code'] = self.modelGrpCultivation.fsch_code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_cotyId) && !isVoid(self.modelGrpCultivation.fsch_cotyId.cotyId)) {
                    paramData['fsch_cotyId_cotyId'] = self.modelGrpCultivation.fsch_cotyId.cotyId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpCultivationBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "Cultivation/findAllByCriteriaRange_CultivationGrpCultivation";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_name)) {
                    paramData['fsch_name'] = self.modelGrpCultivation.fsch_name;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_code)) {
                    paramData['fsch_code'] = self.modelGrpCultivation.fsch_code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_cotyId) && !isVoid(self.modelGrpCultivation.fsch_cotyId.cotyId)) {
                    paramData['fsch_cotyId_cotyId'] = self.modelGrpCultivation.fsch_cotyId.cotyId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpCultivationBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "Cultivation/findAllByCriteriaRange_CultivationGrpCultivation_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_name)) {
                    paramData['fsch_name'] = self.modelGrpCultivation.fsch_name;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_code)) {
                    paramData['fsch_code'] = self.modelGrpCultivation.fsch_code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_cotyId) && !isVoid(self.modelGrpCultivation.fsch_cotyId.cotyId)) {
                    paramData['fsch_cotyId_cotyId'] = self.modelGrpCultivation.fsch_cotyId.cotyId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpCultivationBase.prototype.getMergedItems = function (func, bContinuousCaller, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache === undefined;
                if (bMakeWebRequest) {
                    var pendingRequest = self.getMergedItems_cache !== undefined &&
                        self.getMergedItems_cache.a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache = new Tuple2(true, undefined);
                    var wsPath = "Cultivation/findAllByCriteriaRange_CultivationGrpCultivation";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                        success(function (response, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                        if (self.$scope.globals.nInFlightRequests > 0)
                            self.$scope.globals.nInFlightRequests--;
                        var dbEntities = self.getEntitiesFromJSON(response);
                        self.getMergedItems_cache.a = false;
                        self.getMergedItems_cache.b = dbEntities;
                        processDBItems(dbEntities);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                        if (self.$scope.globals.nInFlightRequests > 0)
                            self.$scope.globals.nInFlightRequests--;
                        self.getMergedItems_cache.a = false;
                        self.getMergedItems_cache.b = [];
                        NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                        console.error("Error received in getMergedItems:" + data);
                        console.dir(status);
                        console.dir(header);
                        console.dir(config);
                    });
                }
                else {
                    var bPendingRequest = self.getMergedItems_cache.a;
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        }
                        else {
                            processDBItems([]);
                        }
                    }
                    else {
                        var dbEntities = self.getMergedItems_cache.b;
                        processDBItems(dbEntities);
                    }
                }
            };
            ControllerGrpCultivationBase.prototype._expToExcelIsDisabled = function () {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    return true;
                }
                return false;
            };
            ControllerGrpCultivationBase.prototype.expToExcelBtnAction_fileName = function () {
                var self = this;
                return "Crops List" + "_" + Utils.convertDateToString(new Date(), "yyyy_MM_dd_HHmm") + ".xls";
            };
            ControllerGrpCultivationBase.prototype.exportToExcelBtnAction = function () {
                var self = this;
                var wsPath = "Cultivation/findAllByCriteriaRange_CultivationGrpCultivation_toExcel";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['__fields'] = self.getExcelFieldDefinitions();
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_name)) {
                    paramData['fsch_name'] = self.modelGrpCultivation.fsch_name;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_code)) {
                    paramData['fsch_code'] = self.modelGrpCultivation.fsch_code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_cotyId) && !isVoid(self.modelGrpCultivation.fsch_cotyId.cotyId)) {
                    paramData['fsch_cotyId_cotyId'] = self.modelGrpCultivation.fsch_cotyId.cotyId;
                }
                NpTypes.AlertMessage.clearAlerts(self.PageModel);
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    var data = base64DecToArr(response.data);
                    var blob = new Blob([data], { type: "application/octet-stream" });
                    saveAs(blob, self.expToExcelBtnAction_fileName());
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                });
            };
            ControllerGrpCultivationBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.Cultivation(
                /*cultId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*name:string*/ null, 
                /*code:number*/ null, 
                /*rowVersion:number*/ null, 
                /*cotyId:Entities.CoverType*/ null, 
                /*exfiId:Entities.ExcelFile*/ null);
                return ret;
            };
            ControllerGrpCultivationBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpCultivationBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpCultivationBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.Cultivation.Create();
                ret.updateInstance(src);
                ret.cultId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            Object.defineProperty(ControllerGrpCultivationBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpCultivationBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpCultivationBase.prototype._disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpCultivationBase.prototype._invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpCultivationBase.prototype.GrpCultivation_srchr__fsch_name_disabled = function (cultivation) {
                var self = this;
                return false;
            };
            ControllerGrpCultivationBase.prototype.GrpCultivation_srchr__fsch_code_disabled = function (cultivation) {
                var self = this;
                return false;
            };
            ControllerGrpCultivationBase.prototype.GrpCultivation_srchr__fsch_cotyId_disabled = function (cultivation) {
                var self = this;
                return false;
            };
            ControllerGrpCultivationBase.prototype.getDropDownItems_GrpCultivation_srchr__fsch_cotyId = function () {
                var self = this;
                var key = '#';
                return self.GrpCultivation_srchr__fsch_cotyIdCache.getArrayResource(key, function (newArray) {
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['toRowIndex'] = 100;
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                        success(function (result, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                        if (self.$scope.globals.nInFlightRequests > 0)
                            self.$scope.globals.nInFlightRequests--;
                        var nullObject = new SelectItem(null, self.$scope.getALString("DropDownList_ChooseOne"));
                        nullObject.cotyId = null;
                        newArray.push(nullObject);
                        var entities = Entities.CoverType.fromJSONComplete(result.data);
                        _.each(entities, function (coverType) {
                            var newItem = new SelectItem(coverType, coverType.name);
                            newItem.cotyId = coverType.cotyId;
                            newArray.push(newItem);
                        });
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                        if (self.$scope.globals.nInFlightRequests > 0)
                            self.$scope.globals.nInFlightRequests--;
                        NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                    });
                });
            };
            ControllerGrpCultivationBase.prototype.GrpCultivation_itm__name_disabled = function (cultivation) {
                var self = this;
                if (cultivation === undefined || cultivation === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Cultivation_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(cultivation, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpCultivationBase.prototype.GrpCultivation_itm__code_disabled = function (cultivation) {
                var self = this;
                if (cultivation === undefined || cultivation === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Cultivation_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(cultivation, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpCultivationBase.prototype.GrpCultivation_itm__cotyId_disabled = function (cultivation) {
                var self = this;
                if (cultivation === undefined || cultivation === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Cultivation_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(cultivation, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpCultivationBase.prototype.getDropDownItems_GrpCultivation_itm__cotyId = function (cultivation) {
                var self = this;
                var key = '#';
                return self.GrpCultivation_itm__cotyIdCache.getArrayResource(key, function (newArray) {
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['toRowIndex'] = 100;
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                        success(function (result, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                        if (self.$scope.globals.nInFlightRequests > 0)
                            self.$scope.globals.nInFlightRequests--;
                        var nullObject = new SelectItem(null, self.$scope.getALString("DropDownList_ChooseOne"));
                        nullObject.cotyId = null;
                        newArray.push(nullObject);
                        var entities = Entities.CoverType.fromJSONComplete(result.data);
                        _.each(entities, function (coverType) {
                            var newItem = new SelectItem(coverType, coverType.name);
                            newItem.cotyId = coverType.cotyId;
                            newArray.push(newItem);
                        });
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                        if (self.$scope.globals.nInFlightRequests > 0)
                            self.$scope.globals.nInFlightRequests--;
                        NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                    });
                });
            };
            ControllerGrpCultivationBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpCultivationBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpCultivationBase.prototype._newIsDisabled = function () {
                var self = this;
                if (!self.$scope.globals.hasPrivilege("Niva_Cultivation_W"))
                    return true; // no write privilege
                var parEntityIsLocked = false;
                if (parEntityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpCultivationBase.prototype._deleteIsDisabled = function (cultivation) {
                var self = this;
                if (cultivation === null || cultivation === undefined || cultivation.getEntityName() !== "Cultivation")
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Cultivation_D"))
                    return true; // no delete privilege;
                var entityIsLocked = self.isEntityLocked(cultivation, Controllers.LockKind.DeleteLock);
                if (entityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            return ControllerGrpCultivationBase;
        })(Controllers.AbstractGroupTableController);
        Cultivation.ControllerGrpCultivationBase = ControllerGrpCultivationBase;
    })(Cultivation = Controllers.Cultivation || (Controllers.Cultivation = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=CultivationBase.js.map