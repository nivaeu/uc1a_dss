/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/DecisionMakingBase.ts" />
/// <reference path="../EntitiesBase/ParcelDecisionBase.ts" />
/// <reference path="../EntitiesBase/EcGroupCopyBase.ts" />
/// <reference path="../EntitiesBase/EcCultCopyBase.ts" />
/// <reference path="../EntitiesBase/EcCultDetailCopyBase.ts" />
/// <reference path="../EntitiesBase/ClassificationBase.ts" />
/// <reference path="LovClassificationBase.ts" />
/// <reference path="../EntitiesBase/EcGroupBase.ts" />
/// <reference path="LovEcGroupBase.ts" />
/// <reference path="../EntitiesBase/CultivationBase.ts" />
/// <reference path="LovCultivationBase.ts" />
/// <reference path="../EntitiesBase/CoverTypeBase.ts" />
/// <reference path="LovCoverTypeBase.ts" />
/// <reference path="../Controllers/DecisionMaking.ts" />
module Controllers.DecisionMaking {
    export class PageModelBase extends AbstractPageModel {
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        modelGrpDema:ModelGrpDema;
        modelGrpParcelDecision:ModelGrpParcelDecision;
        modelGrpEcGroupCopy:ModelGrpEcGroupCopy;
        modelGrpEcCultCopy:ModelGrpEcCultCopy;
        modelGrpEcCultDetailCopy:ModelGrpEcCultDetailCopy;
        modelEcCultCopyCover:ModelEcCultCopyCover;
        modelEcCultDetailCopyCover:ModelEcCultDetailCopyCover;
        modelEcCultCopySuper:ModelEcCultCopySuper;
        modelEcCultDetailCopySuper:ModelEcCultDetailCopySuper;
        controller: PageController;
        constructor(public $scope: IPageScope) { super($scope); }
    }


    export interface IPageScopeBase extends IAbstractPageScope {
        globals: Globals;
        _saveIsDisabled():boolean; 
        _cancelIsDisabled():boolean; 
        pageModel : PageModel;
        onSaveBtnAction(): void;
        onNewBtnAction(): void;
        onDeleteBtnAction():void;

    }

    export class PageControllerBase extends AbstractPageController {
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model:PageModel)
        {
            super($scope, $http, $timeout, Plato, model);
            var self: PageControllerBase = this;
            model.controller = <PageController>self;
            $scope.pageModel = self.model;

            $scope._saveIsDisabled = 
                () => {
                    return self._saveIsDisabled();
                };
            $scope._cancelIsDisabled = 
                () => {
                    return self._cancelIsDisabled();
                };

            $scope.onSaveBtnAction = () => { 
                self.onSaveBtnAction((response:NpTypes.SaveResponce) => {self.onSuccesfullSaveFromButton(response);});
            };
            

            $scope.onNewBtnAction = () => { 
                self.onNewBtnAction();
            };
            
            $scope.onDeleteBtnAction = () => { 
                self.onDeleteBtnAction();
            };


            $timeout(function() {
                $('#DecisionMaking').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                $('#NpMainContent').scrollTop(0);
            }, 0);

        }
        public get ControllerClassName(): string {
            return "PageController";
        }
        public get HtmlDivId(): string {
            return "DecisionMaking";
        }
    
        public _getPageTitle(): string {
            return "Decision Making";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                ];
            }
            return this._items;
        }

        public _saveIsDisabled():boolean {
            var self = this;

            if (!self.$scope.globals.hasPrivilege("Niva_DecisionMaking_W"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _cancelIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }

        public get pageModel():PageModel {
            var self = this;
            return self.model;
        }

        
        public update():void {
            var self = this;
            self.model.modelGrpDema.controller.updateUI();
        }

        public refreshVisibleEntities(newEntitiesIds: NpTypes.NewEntityId[]):void {
            var self = this;
            self.model.modelGrpDema.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpParcelDecision.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpEcGroupCopy.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpEcCultCopy.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpEcCultDetailCopy.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelEcCultCopyCover.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelEcCultDetailCopyCover.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelEcCultCopySuper.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelEcCultDetailCopySuper.controller.refreshVisibleEntities(newEntitiesIds);
        }

        public refreshGroups(newEntitiesIds: NpTypes.NewEntityId[]): void {
            var self = this;
            self.model.modelGrpDema.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpParcelDecision.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpEcGroupCopy.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpEcCultCopy.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpEcCultDetailCopy.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelEcCultCopyCover.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelEcCultDetailCopyCover.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelEcCultCopySuper.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelEcCultDetailCopySuper.controller.refreshVisibleEntities(newEntitiesIds);
        }

        _firstLevelGroupControllers: Array<AbstractGroupController>=undefined;
        public get FirstLevelGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelGroupControllers === undefined) {
                self._firstLevelGroupControllers = [
                    self.model.modelGrpDema.controller
                ];
            }
            return this._firstLevelGroupControllers;
        }

        _allGroupControllers: Array<AbstractGroupController>=undefined;
        public get AllGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._allGroupControllers === undefined) {
                self._allGroupControllers = [
                    self.model.modelGrpDema.controller,
                    self.model.modelGrpParcelDecision.controller,
                    self.model.modelGrpEcGroupCopy.controller,
                    self.model.modelGrpEcCultCopy.controller,
                    self.model.modelGrpEcCultDetailCopy.controller,
                    self.model.modelEcCultCopyCover.controller,
                    self.model.modelEcCultDetailCopyCover.controller,
                    self.model.modelEcCultCopySuper.controller,
                    self.model.modelEcCultDetailCopySuper.controller
                ];
            }
            return this._allGroupControllers;
        }

        public getPageChanges(bForDelete:boolean = false):Array<ChangeToCommit> {
            var self = this;
            var pageChanges: Array<ChangeToCommit> = [];
            if (bForDelete) {
                pageChanges = pageChanges.concat(self.model.modelGrpDema.controller.getChangesToCommitForDeletion());
                pageChanges = pageChanges.concat(self.model.modelGrpDema.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpParcelDecision.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpEcGroupCopy.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpEcCultCopy.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpEcCultDetailCopy.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelEcCultCopyCover.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelEcCultDetailCopyCover.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelEcCultCopySuper.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelEcCultDetailCopySuper.controller.getDeleteChangesToCommit());
            } else {

                pageChanges = pageChanges.concat(self.model.modelGrpDema.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpParcelDecision.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpEcGroupCopy.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpEcCultCopy.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpEcCultDetailCopy.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelEcCultCopyCover.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelEcCultDetailCopyCover.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelEcCultCopySuper.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelEcCultDetailCopySuper.controller.getChangesToCommit());
            }
            
            var hasDecisionMaking = pageChanges.some(change => change.entityName === "DecisionMaking")
            if (!hasDecisionMaking) {
                var validateEntity = new ChangeToCommit(ChangeStatus.UPDATE, 0, "DecisionMaking", self.model.modelGrpDema.controller.Current);
                pageChanges = pageChanges.concat(validateEntity);
            }
            return pageChanges;
        }

        private getSynchronizeChangesWithDbUrl():string { 
            return "/Niva/rest/MainService/synchronizeChangesWithDb_DecisionMaking"; 
        }
        
        public getSynchronizeChangesWithDbData(bForDelete:boolean = false):any {
            var self = this;
            var paramData:any = {}
            paramData.data = self.getPageChanges(bForDelete);
            return paramData;
        }


        public onSuccesfullSaveFromButton(response:NpTypes.SaveResponce) {
            var self = this;
            super.onSuccesfullSaveFromButton(response);
            if (!isVoid(response.warningMessages)) {
                NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
            }
            if (!isVoid(response.infoMessages)) {
                NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
            }

            self.model.modelGrpDema.controller.cleanUpAfterSave();
            self.model.modelGrpParcelDecision.controller.cleanUpAfterSave();
            self.model.modelGrpEcGroupCopy.controller.cleanUpAfterSave();
            self.model.modelGrpEcCultCopy.controller.cleanUpAfterSave();
            self.model.modelGrpEcCultDetailCopy.controller.cleanUpAfterSave();
            self.model.modelEcCultCopyCover.controller.cleanUpAfterSave();
            self.model.modelEcCultDetailCopyCover.controller.cleanUpAfterSave();
            self.model.modelEcCultCopySuper.controller.cleanUpAfterSave();
            self.model.modelEcCultDetailCopySuper.controller.cleanUpAfterSave();
            self.$scope.globals.isCurrentTransactionDirty = false;
            self.refreshGroups(response.newEntitiesIds);
        }

        public onFailuredSave(data:any, status:number) {
            var self = this;
            if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
            var errors = Messages.dynamicMessage(data);
            NpTypes.AlertMessage.addDanger(self.model, errors);
            messageBox( self.$scope, self.Plato,"MessageBox_Attention_Title",
                "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>",
                IconKind.ERROR, [new Tuple2("OK", () => {}),], 0, 0,'50em');
        }
        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }
        
        public onSaveBtnAction(onSuccess:(response:NpTypes.SaveResponce)=>void): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            var errors = self.validatePage();
            if (errors.length > 0) {
                var errMessage = errors.join('<br>');
                NpTypes.AlertMessage.addDanger(self.model, errMessage);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title",
                    "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>",
                    IconKind.ERROR,[new Tuple2("OK", () => {}),], 0, 0,'50em');
                return;
            }

            if (self.$scope.globals.isCurrentTransactionDirty === false) {
                var jqSaveBtn = self.SaveBtnJQueryHandler;
                self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                return;
            }

            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData();
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success((response:NpTypes.SaveResponce, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;

                    if (!self.shownAsDialog()) {
                        var breadcrumbStepModel = <IFormPageBreadcrumbStepModel<Entities.DecisionMaking>>self.$scope.getCurrentPageModel();
                        var newDecisionMakingId = response.newEntitiesIds.firstOrNull(x => x.entityName === 'DecisionMaking');
                        if (!isVoid(newDecisionMakingId)) {
                            if (!isVoid(breadcrumbStepModel)) {
                                breadcrumbStepModel.selectedEntity = Entities.DecisionMaking.CreateById(newDecisionMakingId.databaseId);
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        } else {
                            if (!isVoid(breadcrumbStepModel) && !(isVoid(breadcrumbStepModel.selectedEntity))) {
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        self.$scope.setCurrentPageModel(breadcrumbStepModel);
                    }

                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status)
            });
        }

        public getInitialEntity(): Entities.DecisionMaking {
            if (this.shownAsDialog()) {
                return <Entities.DecisionMaking>this.dialogSelectedEntity();
            } else {
                var breadCrumbStepModel = <IFormPageBreadcrumbStepModel<Entities.DecisionMaking>>this.$scope.getPageModelByURL('/DecisionMaking');
                if (isVoid(breadCrumbStepModel)) {
                    return null;
                } else {
                    return isVoid(breadCrumbStepModel.selectedEntity) ? null : breadCrumbStepModel.selectedEntity;
                }
            }
        }



        public onPageUnload(actualNavigation: (pageScopeYouAreLeavingFrom?: NpTypes.IApplicationScope) => void) {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => {
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                actualNavigation(self.$scope);
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                actualNavigation(self.$scope);
            }
        }


        public onNewBtnAction(): void {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => { 
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                self.addNewRecord();
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {self.addNewRecord();}),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                self.addNewRecord();
            }
        }

        public addNewRecord(): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            self.model.modelGrpDema.controller.cleanUpAfterSave();
            self.model.modelGrpParcelDecision.controller.cleanUpAfterSave();
            self.model.modelGrpEcGroupCopy.controller.cleanUpAfterSave();
            self.model.modelGrpEcCultCopy.controller.cleanUpAfterSave();
            self.model.modelGrpEcCultDetailCopy.controller.cleanUpAfterSave();
            self.model.modelEcCultCopyCover.controller.cleanUpAfterSave();
            self.model.modelEcCultDetailCopyCover.controller.cleanUpAfterSave();
            self.model.modelEcCultCopySuper.controller.cleanUpAfterSave();
            self.model.modelEcCultDetailCopySuper.controller.cleanUpAfterSave();
            self.model.modelGrpDema.controller.createNewEntityAndAddToUI();
        }

        public onDeleteBtnAction():void {
            var self = this;
            messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("EntityDeletionWarningMsg"), IconKind.QUESTION, 
                [
                    new Tuple2("MessageBox_Button_Yes", () => {self.deleteRecord();}),
                    new Tuple2("MessageBox_Button_No", () => {})
                ], 1,1);
        }

        public deleteRecord(): void {
            var self = this;
            if (self.Mode === EditMode.NEW) {
                console.log("Delete pressed in a form while being in new mode!");
                return;
            }
            NpTypes.AlertMessage.clearAlerts(self.model);
            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData(true);
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.$scope.navigateBackward();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.model, Messages.dynamicMessage(data));
            });
        }
        
        public get Mode(): EditMode {
            var self = this;
            if (isVoid(self.model.modelGrpDema) || isVoid(self.model.modelGrpDema.controller))
                return EditMode.NEW;
            return self.model.modelGrpDema.controller.Mode;
        }

        public _newIsDisabled(): boolean {
            var self = this;
            if (isVoid(self.model.modelGrpDema) || isVoid(self.model.modelGrpDema.controller))
                return true;
            return self.model.modelGrpDema.controller._newIsDisabled();
        }
        public _deleteIsDisabled(): boolean {
            var self = this;
            if (self.Mode === EditMode.NEW)
                return true;
            if (isVoid(self.model.modelGrpDema) || isVoid(self.model.modelGrpDema.controller))
                return true;
            return self.model.modelGrpDema.controller._deleteIsDisabled(self.model.modelGrpDema.controller.Current);
        }


    }


    

    // GROUP GrpDema

    export class ModelGrpDemaBase extends Controllers.AbstractGroupFormModel {
        modelGrpParcelDecision:ModelGrpParcelDecision;
        modelGrpEcGroupCopy:ModelGrpEcGroupCopy;
        controller: ControllerGrpDema;
        public get demaId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.DecisionMaking>this.selectedEntities[0]).demaId;
        }

        public set demaId(demaId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.DecisionMaking>this.selectedEntities[0]).demaId = demaId_newVal;
        }

        public get _demaId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._demaId;
        }

        public get description():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.DecisionMaking>this.selectedEntities[0]).description;
        }

        public set description(description_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.DecisionMaking>this.selectedEntities[0]).description = description_newVal;
        }

        public get _description():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._description;
        }

        public get dateTime():Date {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.DecisionMaking>this.selectedEntities[0]).dateTime;
        }

        public set dateTime(dateTime_newVal:Date) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.DecisionMaking>this.selectedEntities[0]).dateTime = dateTime_newVal;
        }

        public get _dateTime():NpTypes.UIDateModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._dateTime;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.DecisionMaking>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.DecisionMaking>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get recordtype():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.DecisionMaking>this.selectedEntities[0]).recordtype;
        }

        public set recordtype(recordtype_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.DecisionMaking>this.selectedEntities[0]).recordtype = recordtype_newVal;
        }

        public get _recordtype():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._recordtype;
        }

        public get hasBeenRun():boolean {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.DecisionMaking>this.selectedEntities[0]).hasBeenRun;
        }

        public set hasBeenRun(hasBeenRun_newVal:boolean) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.DecisionMaking>this.selectedEntities[0]).hasBeenRun = hasBeenRun_newVal;
        }

        public get _hasBeenRun():NpTypes.UIBoolModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._hasBeenRun;
        }

        public get hasPopulatedIntegratedDecisionsAndIssues():boolean {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.DecisionMaking>this.selectedEntities[0]).hasPopulatedIntegratedDecisionsAndIssues;
        }

        public set hasPopulatedIntegratedDecisionsAndIssues(hasPopulatedIntegratedDecisionsAndIssues_newVal:boolean) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.DecisionMaking>this.selectedEntities[0]).hasPopulatedIntegratedDecisionsAndIssues = hasPopulatedIntegratedDecisionsAndIssues_newVal;
        }

        public get _hasPopulatedIntegratedDecisionsAndIssues():NpTypes.UIBoolModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._hasPopulatedIntegratedDecisionsAndIssues;
        }

        public get clasId():Entities.Classification {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.DecisionMaking>this.selectedEntities[0]).clasId;
        }

        public set clasId(clasId_newVal:Entities.Classification) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.DecisionMaking>this.selectedEntities[0]).clasId = clasId_newVal;
        }

        public get _clasId():NpTypes.UIManyToOneModel<Entities.Classification> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._clasId;
        }

        public get ecgrId():Entities.EcGroup {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.DecisionMaking>this.selectedEntities[0]).ecgrId;
        }

        public set ecgrId(ecgrId_newVal:Entities.EcGroup) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.DecisionMaking>this.selectedEntities[0]).ecgrId = ecgrId_newVal;
        }

        public get _ecgrId():NpTypes.UIManyToOneModel<Entities.EcGroup> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._ecgrId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpDema) { super($scope); }
    }



    export interface IScopeGrpDemaBase extends Controllers.IAbstractFormGroupScope, IPageScope{
        globals: Globals;
        modelGrpDema : ModelGrpDema;
        GrpDema_itm__clasId_disabled(decisionMaking:Entities.DecisionMaking):boolean; 
        showLov_GrpDema_itm__clasId(decisionMaking:Entities.DecisionMaking):void; 
        GrpDema_itm__ecgrId_disabled(decisionMaking:Entities.DecisionMaking):boolean; 
        showLov_GrpDema_itm__ecgrId(decisionMaking:Entities.DecisionMaking):void; 
        GrpDema_itm__description_disabled(decisionMaking:Entities.DecisionMaking):boolean; 
        finalizeButtonId_disabled(decisionMaking:Entities.DecisionMaking):boolean; 
        finalizationAction(decisionMaking:Entities.DecisionMaking):void; 
        GrpDema_0_disabled():boolean; 
        GrpDema_0_invisible():boolean; 
        GrpDema_0_0_disabled():boolean; 
        GrpDema_0_0_invisible():boolean; 
        GrpDema_0_0_0_disabled(decisionMaking:Entities.DecisionMaking):boolean; 
        actionUpdateIntegratedDecisionsNIssuesFromDema(decisionMaking:Entities.DecisionMaking):void; 
        child_group_GrpParcelDecision_isInvisible():boolean; 
        child_group_GrpEcGroupCopy_isInvisible():boolean; 

    }



    export class ControllerGrpDemaBase extends Controllers.AbstractGroupFormController {
        constructor(
            public $scope: IScopeGrpDema,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpDema)
        {
            super($scope, $http, $timeout, Plato, model);
            var self:ControllerGrpDemaBase = this;
            model.controller = <ControllerGrpDema>self;
            $scope.modelGrpDema = self.model;

            var selectedEntity: Entities.DecisionMaking = this.$scope.pageModel.controller.getInitialEntity();
            if (isVoid(selectedEntity)) {
                self.createNewEntityAndAddToUI();
            } else {
                var clonedEntity = Entities.DecisionMaking.Create();
                clonedEntity.updateInstance(selectedEntity);
                $scope.modelGrpDema.visibleEntities[0] = clonedEntity;
                $scope.modelGrpDema.selectedEntities[0] = clonedEntity;
            } 
            $scope.GrpDema_itm__clasId_disabled = 
                (decisionMaking:Entities.DecisionMaking) => {
                    return self.GrpDema_itm__clasId_disabled(decisionMaking);
                };
            $scope.showLov_GrpDema_itm__clasId = 
                (decisionMaking:Entities.DecisionMaking) => {
                    $timeout( () => {        
                        self.showLov_GrpDema_itm__clasId(decisionMaking);
                    }, 0);
                };
            $scope.GrpDema_itm__ecgrId_disabled = 
                (decisionMaking:Entities.DecisionMaking) => {
                    return self.GrpDema_itm__ecgrId_disabled(decisionMaking);
                };
            $scope.showLov_GrpDema_itm__ecgrId = 
                (decisionMaking:Entities.DecisionMaking) => {
                    $timeout( () => {        
                        self.showLov_GrpDema_itm__ecgrId(decisionMaking);
                    }, 0);
                };
            $scope.GrpDema_itm__description_disabled = 
                (decisionMaking:Entities.DecisionMaking) => {
                    return self.GrpDema_itm__description_disabled(decisionMaking);
                };
            $scope.finalizeButtonId_disabled = 
                (decisionMaking:Entities.DecisionMaking) => {
                    return self.finalizeButtonId_disabled(decisionMaking);
                };
            $scope.finalizationAction = 
                (decisionMaking:Entities.DecisionMaking) => {
            self.finalizationAction(decisionMaking);
                };
            $scope.GrpDema_0_disabled = 
                () => {
                    return self.GrpDema_0_disabled();
                };
            $scope.GrpDema_0_invisible = 
                () => {
                    return self.GrpDema_0_invisible();
                };
            $scope.GrpDema_0_0_disabled = 
                () => {
                    return self.GrpDema_0_0_disabled();
                };
            $scope.GrpDema_0_0_invisible = 
                () => {
                    return self.GrpDema_0_0_invisible();
                };
            $scope.GrpDema_0_0_0_disabled = 
                (decisionMaking:Entities.DecisionMaking) => {
                    return self.GrpDema_0_0_0_disabled(decisionMaking);
                };
            $scope.actionUpdateIntegratedDecisionsNIssuesFromDema = 
                (decisionMaking:Entities.DecisionMaking) => {
            self.actionUpdateIntegratedDecisionsNIssuesFromDema(decisionMaking);
                };
            $scope.child_group_GrpParcelDecision_isInvisible = 
                () => {
                    return self.child_group_GrpParcelDecision_isInvisible();
                };
            $scope.child_group_GrpEcGroupCopy_isInvisible = 
                () => {
                    return self.child_group_GrpEcGroupCopy_isInvisible();
                };


            $scope.pageModel.modelGrpDema = $scope.modelGrpDema;


    /*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.DecisionMaking[] , oldVisible:Entities.DecisionMaking[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
            //bind entity child collection with child controller merged items
            Entities.DecisionMaking.parcelDecisionCollection = (decisionMaking, func) => {
                this.model.modelGrpParcelDecision.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, decisionMaking);
            }
            //bind entity child collection with child controller merged items
            Entities.DecisionMaking.ecGroupCopyCollection = (decisionMaking, func) => {
                this.model.modelGrpEcGroupCopy.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, decisionMaking);
            }

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
                //unbind entity child collection with child controller merged items
                Entities.DecisionMaking.parcelDecisionCollection = null;//(decisionMaking, func) => { }
                //unbind entity child collection with child controller merged items
                Entities.DecisionMaking.ecGroupCopyCollection = null;//(decisionMaking, func) => { }
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpDema";
        }
        public get HtmlDivId(): string {
            return "DecisionMaking_ControllerGrpDema";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new LovItem (
                        (ent?: Entities.DecisionMaking) => 'Data Import Name',
                        false ,
                        (ent?: Entities.DecisionMaking) => ent.clasId,  
                        (ent?: Entities.DecisionMaking) => ent._clasId,  
                        (ent?: Entities.DecisionMaking) => true, //isRequired
                        (vl: Entities.Classification, ent?: Entities.DecisionMaking) => new NpTypes.ValidationResult(true, "")),
                    new LovItem (
                        (ent?: Entities.DecisionMaking) => 'BRE Rules Name',
                        false ,
                        (ent?: Entities.DecisionMaking) => ent.ecgrId,  
                        (ent?: Entities.DecisionMaking) => ent._ecgrId,  
                        (ent?: Entities.DecisionMaking) => true, //isRequired
                        (vl: Entities.EcGroup, ent?: Entities.DecisionMaking) => new NpTypes.ValidationResult(true, "")),
                    new TextItem (
                        (ent?: Entities.DecisionMaking) => 'Description',
                        false ,
                        (ent?: Entities.DecisionMaking) => ent.description,  
                        (ent?: Entities.DecisionMaking) => ent._description,  
                        (ent?: Entities.DecisionMaking) => false, 
                        (vl: string, ent?: Entities.DecisionMaking) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new DateItem (
                        (ent?: Entities.DecisionMaking) => 'Date',
                        false ,
                        (ent?: Entities.DecisionMaking) => ent.dateTime,  
                        (ent?: Entities.DecisionMaking) => ent._dateTime,  
                        (ent?: Entities.DecisionMaking) => true, //isRequired
                        (vl: Date, ent?: Entities.DecisionMaking) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined)
                ];
            }
            return this._items;
        }

        

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpDema():ModelGrpDema {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "DecisionMaking";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
        }

        public setCurrentFormPageBreadcrumpStepModel() {
            var self = this;
            if (self.$scope.pageModel.controller.shownAsDialog())
                return;

            var breadCrumbStepModel: IFormPageBreadcrumbStepModel<Entities.DecisionMaking> = { selectedEntity: null };
            if (!isVoid(self.Current)) {
                var selectedEntity: Entities.DecisionMaking = Entities.DecisionMaking.Create();
                selectedEntity.updateInstance(self.Current);
                breadCrumbStepModel.selectedEntity = selectedEntity;
            } else {
                breadCrumbStepModel.selectedEntity = null;
            }
            self.$scope.setCurrentPageModel(breadCrumbStepModel);
        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.DecisionMaking.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.DecisionMaking {
            var self = this;
            return <Entities.DecisionMaking>self.$scope.modelGrpDema.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.DecisionMaking, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  false;

        }







        public constructEntity(): Entities.DecisionMaking {
            var self = this;
            var ret = new Entities.DecisionMaking(
                /*demaId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*description:string*/ null,
                /*dateTime:Date*/ new Date,
                /*rowVersion:number*/ null,
                /*recordtype:number*/ 0,
                /*hasBeenRun:boolean*/ null,
                /*hasPopulatedIntegratedDecisionsAndIssues:boolean*/ null,
                /*clasId:Entities.Classification*/ null,
                /*ecgrId:Entities.EcGroup*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.DecisionMaking = <Entities.DecisionMaking>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.$scope.modelGrpDema.visibleEntities[0] = newEnt;
            self.$scope.modelGrpDema.selectedEntities[0] = newEnt;
            return newEnt;

        }

        public cloneEntity(src: Entities.DecisionMaking): Entities.DecisionMaking {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.DecisionMaking, calledByParent: boolean= false): Entities.DecisionMaking {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.DecisionMaking.Create();
            ret.updateInstance(src);
            ret.demaId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.visibleEntities[0] = ret;
            this.model.selectedEntities[0] = ret;

            this.$timeout( () => {
                this.modelGrpDema.modelGrpParcelDecision.controller.cloneAllEntitiesUnderParent(src, ret);
                this.modelGrpDema.modelGrpEcGroupCopy.controller.cloneAllEntitiesUnderParent(src, ret);
            },1);

            return ret;
        }



        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                    self.modelGrpDema.modelGrpParcelDecision.controller,
                    self.modelGrpDema.modelGrpEcGroupCopy.controller
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.DecisionMaking, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                self.modelGrpDema.modelGrpParcelDecision.controller.deleteAllEntitiesUnderParent(ent, () => {
                    self.modelGrpDema.modelGrpEcGroupCopy.controller.deleteAllEntitiesUnderParent(ent, () => {
                        super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                    });
                });
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    self.modelGrpDema.modelGrpParcelDecision.controller.deleteNewEntitiesUnderParent(ent);
                    self.modelGrpDema.modelGrpEcGroupCopy.controller.deleteNewEntitiesUnderParent(ent);
                    afterDeleteAction();
                });
            }
        }


        public decisionMakingHasBeenRun(decisionMaking:Entities.DecisionMaking):boolean { 
            console.warn("Unimplemented function decisionMakingHasBeenRun()");
            return false; 
        }
        public GrpDema_itm__clasId_disabled(decisionMaking:Entities.DecisionMaking):boolean {
            var self = this;
            if (decisionMaking === undefined || decisionMaking === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_DecisionMaking_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(decisionMaking, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = self.decisionMakingHasBeenRun(decisionMaking);
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_GrpDema_itm__clasId:ModelLovClassificationBase;
        public showLov_GrpDema_itm__clasId(decisionMaking:Entities.DecisionMaking) {
            var self = this;
            if (decisionMaking === undefined)
                return;
            var uimodel:NpTypes.IUIModel = decisionMaking._clasId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Data Import Name';
            dialogOptions.previousModel= self.cachedLovModel_GrpDema_itm__clasId;
            dialogOptions.className = "ControllerLovClassification";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var classification1:Entities.Classification =   <Entities.Classification>selectedEntity;
                uimodel.clearAllErrors();
                if(true && isVoid(classification1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(classification1) && classification1.isEqual(decisionMaking.clasId))
                    return;
                decisionMaking.clasId = classification1;
                self.markEntityAsUpdated(decisionMaking, 'GrpDema_itm__clasId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovClassificationBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpDema_itm__clasId = lovModel;
                    var wsPath = "Classification/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                        paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                        paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                        paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovClassificationBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "Classification/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                        paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                        paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                        paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                        paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                        paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                        paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['description'] = true;
            dialogOptions.shownCols['dateTime'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/Classification.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public GrpDema_itm__ecgrId_disabled(decisionMaking:Entities.DecisionMaking):boolean {
            var self = this;
            if (decisionMaking === undefined || decisionMaking === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_DecisionMaking_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(decisionMaking, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = self.decisionMakingHasBeenRun(decisionMaking);
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_GrpDema_itm__ecgrId:ModelLovEcGroupBase;
        public showLov_GrpDema_itm__ecgrId(decisionMaking:Entities.DecisionMaking) {
            var self = this;
            if (decisionMaking === undefined)
                return;
            var uimodel:NpTypes.IUIModel = decisionMaking._ecgrId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'BRE Rules';
            dialogOptions.previousModel= self.cachedLovModel_GrpDema_itm__ecgrId;
            dialogOptions.className = "ControllerLovEcGroup";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var ecGroup1:Entities.EcGroup =   <Entities.EcGroup>selectedEntity;
                uimodel.clearAllErrors();
                if(true && isVoid(ecGroup1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(ecGroup1) && ecGroup1.isEqual(decisionMaking.ecgrId))
                    return;
                decisionMaking.ecgrId = ecGroup1;
                self.markEntityAsUpdated(decisionMaking, 'GrpDema_itm__ecgrId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovEcGroupBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpDema_itm__ecgrId = lovModel;
                    var wsPath = "EcGroup/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_description)) {
                        paramData['fsch_EcGroupLov_description'] = lovModel.fsch_EcGroupLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_name)) {
                        paramData['fsch_EcGroupLov_name'] = lovModel.fsch_EcGroupLov_name;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovEcGroupBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "EcGroup/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_description)) {
                        paramData['fsch_EcGroupLov_description'] = lovModel.fsch_EcGroupLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_name)) {
                        paramData['fsch_EcGroupLov_name'] = lovModel.fsch_EcGroupLov_name;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_description)) {
                        paramData['fsch_EcGroupLov_description'] = lovModel.fsch_EcGroupLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_name)) {
                        paramData['fsch_EcGroupLov_name'] = lovModel.fsch_EcGroupLov_name;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['description'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/EcGroup.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public isFieldsDisabled(decisionMaking:Entities.DecisionMaking):boolean { 
            console.warn("Unimplemented function isFieldsDisabled()");
            return false; 
        }
        public GrpDema_itm__description_disabled(decisionMaking:Entities.DecisionMaking):boolean {
            var self = this;
            if (decisionMaking === undefined || decisionMaking === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_DecisionMaking_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(decisionMaking, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = self.isFieldsDisabled(decisionMaking);
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public finalizeButtonId_disabled(decisionMaking:Entities.DecisionMaking):boolean {
            var self = this;
            if (decisionMaking === undefined || decisionMaking === null)
                return true;


            

            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = self.decisionMakingHasBeenRun(decisionMaking);
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public finalizationAction(decisionMaking:Entities.DecisionMaking) {
            var self = this;
            console.log("Method: finalizationAction() called")
            console.log(decisionMaking);

        }
        public GrpDema_0_disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpDema_0_invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  this.decisionMakingHasNotBeenRun();
            return programmerVal;
        }
        public decisionMakingHasNotBeenRun():boolean { 
            console.warn("Unimplemented function decisionMakingHasNotBeenRun()");
            return false; 
        }
        public GrpDema_0_0_disabled():boolean { 
            if (false)
                return true;
            var parControl = this.GrpDema_0_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpDema_0_0_invisible():boolean { 
            if (false)
                return true;
            var parControl = this.GrpDema_0_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpDema_0_0_0_disabled(decisionMaking:Entities.DecisionMaking):boolean {
            var self = this;
            if (decisionMaking === undefined || decisionMaking === null)
                return true;


            

            var isContainerControlDisabled   = self.GrpDema_0_0_disabled();
            var disabledByProgrammerMethod = self.parcelsIntegratedDecisionsAndIssuesHaveBeenPopulated(decisionMaking);
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public parcelsIntegratedDecisionsAndIssuesHaveBeenPopulated(decisionMaking:Entities.DecisionMaking):boolean { 
            console.warn("Unimplemented function parcelsIntegratedDecisionsAndIssuesHaveBeenPopulated()");
            return false; 
        }
        public actionUpdateIntegratedDecisionsNIssuesFromDema(decisionMaking:Entities.DecisionMaking) {
            var self = this;
            console.log("Method: actionUpdateIntegratedDecisionsNIssuesFromDema() called")
            console.log(decisionMaking);

        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            var self = this;
            if (!self.$scope.globals.hasPrivilege("Niva_DecisionMaking_W"))
                return true; // no write privilege

            

            var parEntityIsLocked   = false;
            if (parEntityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public _deleteIsDisabled(decisionMaking:Entities.DecisionMaking):boolean  {
            var self = this;
            if (decisionMaking === null || decisionMaking === undefined || decisionMaking.getEntityName() !== "DecisionMaking")
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_DecisionMaking_D"))
                return true; // no delete privilege;




            var entityIsLocked   = self.isEntityLocked(decisionMaking, LockKind.DeleteLock);
            if (entityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return self.isFieldsDisabled(decisionMaking);

        }

        public child_group_GrpParcelDecision_isInvisible():boolean {
            var self = this;
            if ((self.model.modelGrpParcelDecision === undefined) || (self.model.modelGrpParcelDecision.controller === undefined))
                return false;
            return self.model.modelGrpParcelDecision.controller._isInvisible()
        }
        public child_group_GrpEcGroupCopy_isInvisible():boolean {
            var self = this;
            if ((self.model.modelGrpEcGroupCopy === undefined) || (self.model.modelGrpEcGroupCopy.controller === undefined))
                return false;
            return self.model.modelGrpEcGroupCopy.controller._isInvisible()
        }

    }


    // GROUP GrpParcelDecision

    export class ModelGrpParcelDecisionBase extends Controllers.AbstractGroupTableModel {
        controller: ControllerGrpParcelDecision;
        public get padeId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelDecision>this.selectedEntities[0]).padeId;
        }

        public set padeId(padeId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelDecision>this.selectedEntities[0]).padeId = padeId_newVal;
        }

        public get _padeId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._padeId;
        }

        public get decisionLight():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelDecision>this.selectedEntities[0]).decisionLight;
        }

        public set decisionLight(decisionLight_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelDecision>this.selectedEntities[0]).decisionLight = decisionLight_newVal;
        }

        public get _decisionLight():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._decisionLight;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelDecision>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelDecision>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get demaId():Entities.DecisionMaking {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelDecision>this.selectedEntities[0]).demaId;
        }

        public set demaId(demaId_newVal:Entities.DecisionMaking) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelDecision>this.selectedEntities[0]).demaId = demaId_newVal;
        }

        public get _demaId():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._demaId;
        }

        public get pclaId():Entities.ParcelClas {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.ParcelDecision>this.selectedEntities[0]).pclaId;
        }

        public set pclaId(pclaId_newVal:Entities.ParcelClas) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.ParcelDecision>this.selectedEntities[0]).pclaId = pclaId_newVal;
        }

        public get _pclaId():NpTypes.UIManyToOneModel<Entities.ParcelClas> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._pclaId;
        }

        _fsch_decisionLight:NpTypes.UINumberModel = new NpTypes.UINumberModel(undefined);
        public get fsch_decisionLight():number {
            return this._fsch_decisionLight.value;
        }
        public set fsch_decisionLight(vl:number) {
            this._fsch_decisionLight.value = vl;
        }
        _fsch_Parcel_Identifier:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get fsch_Parcel_Identifier():string {
            return this._fsch_Parcel_Identifier.value;
        }
        public set fsch_Parcel_Identifier(vl:string) {
            this._fsch_Parcel_Identifier.value = vl;
        }
        _fsch_Code:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get fsch_Code():string {
            return this._fsch_Code.value;
        }
        public set fsch_Code(vl:string) {
            this._fsch_Code.value = vl;
        }
        _fsch_prodCode:NpTypes.UINumberModel = new NpTypes.UINumberModel(undefined);
        public get fsch_prodCode():number {
            return this._fsch_prodCode.value;
        }
        public set fsch_prodCode(vl:number) {
            this._fsch_prodCode.value = vl;
        }
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpParcelDecision) { super($scope); }
    }



    export interface IScopeGrpParcelDecisionBase extends Controllers.IAbstractTableGroupScope, IScopeGrpDemaBase{
        globals: Globals;
        modelGrpParcelDecision : ModelGrpParcelDecision;
        _disabled():boolean; 
        _invisible():boolean; 
        GrpParcelDecision_srchr__fsch_decisionLight_disabled(parcelDecision:Entities.ParcelDecision):boolean; 
        GrpParcelDecision_srchr__fsch_Parcel_Identifier_disabled(parcelDecision:Entities.ParcelDecision):boolean; 
        GrpParcelDecision_srchr__fsch_Code_disabled(parcelDecision:Entities.ParcelDecision):boolean; 
        GrpParcelDecision_srchr__fsch_prodCode_disabled(parcelDecision:Entities.ParcelDecision):boolean; 
        GrpParcelDecision_0_disabled():boolean; 
        GrpParcelDecision_0_invisible():boolean; 
        GrpParcelDecision_0_0_disabled(parcelDecision:Entities.ParcelDecision):boolean; 
        exportParcelDecisionsToCSV(parcelDecision:Entities.ParcelDecision):void; 

    }



    export class ControllerGrpParcelDecisionBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpParcelDecision,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpParcelDecision)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 3});
            var self:ControllerGrpParcelDecisionBase = this;
            model.controller = <ControllerGrpParcelDecision>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpParcelDecision = self.model;

            $scope._disabled = 
                () => {
                    return self._disabled();
                };
            $scope._invisible = 
                () => {
                    return self._invisible();
                };
            $scope.GrpParcelDecision_srchr__fsch_decisionLight_disabled = 
                (parcelDecision:Entities.ParcelDecision) => {
                    return self.GrpParcelDecision_srchr__fsch_decisionLight_disabled(parcelDecision);
                };
            $scope.GrpParcelDecision_srchr__fsch_Parcel_Identifier_disabled = 
                (parcelDecision:Entities.ParcelDecision) => {
                    return self.GrpParcelDecision_srchr__fsch_Parcel_Identifier_disabled(parcelDecision);
                };
            $scope.GrpParcelDecision_srchr__fsch_Code_disabled = 
                (parcelDecision:Entities.ParcelDecision) => {
                    return self.GrpParcelDecision_srchr__fsch_Code_disabled(parcelDecision);
                };
            $scope.GrpParcelDecision_srchr__fsch_prodCode_disabled = 
                (parcelDecision:Entities.ParcelDecision) => {
                    return self.GrpParcelDecision_srchr__fsch_prodCode_disabled(parcelDecision);
                };
            $scope.GrpParcelDecision_0_disabled = 
                () => {
                    return self.GrpParcelDecision_0_disabled();
                };
            $scope.GrpParcelDecision_0_invisible = 
                () => {
                    return self.GrpParcelDecision_0_invisible();
                };
            $scope.GrpParcelDecision_0_0_disabled = 
                (parcelDecision:Entities.ParcelDecision) => {
                    return self.GrpParcelDecision_0_0_disabled(parcelDecision);
                };
            $scope.exportParcelDecisionsToCSV = 
                (parcelDecision:Entities.ParcelDecision) => {
            self.exportParcelDecisionsToCSV(parcelDecision);
                };


            $scope.pageModel.modelGrpParcelDecision = $scope.modelGrpParcelDecision;


            $scope.clearBtnAction = () => { 
                self.modelGrpParcelDecision.fsch_decisionLight = undefined;
                self.modelGrpParcelDecision.fsch_Parcel_Identifier = undefined;
                self.modelGrpParcelDecision.fsch_Code = undefined;
                self.modelGrpParcelDecision.fsch_prodCode = undefined;
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpDema.modelGrpParcelDecision = $scope.modelGrpParcelDecision;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpDema.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.ParcelDecision[] , oldVisible:Entities.ParcelDecision[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpParcelDecision";
        }
        public get HtmlDivId(): string {
            return "DecisionMaking_ControllerGrpParcelDecision";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new StaticListItem<number> (
                        (ent?: NpTypes.IBaseEntity) => 'Decision Light',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_decisionLight,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_decisionLight,  
                        (ent?: NpTypes.IBaseEntity) => false, //isRequired
                        (vl: number, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, "")),
                    new TextItem (
                        (ent?: NpTypes.IBaseEntity) => 'Identifier',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_Parcel_Identifier,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_Parcel_Identifier,  
                        (ent?: NpTypes.IBaseEntity) => false, 
                        (vl: string, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new TextItem (
                        (ent?: NpTypes.IBaseEntity) => 'Parcel Code',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_Code,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_Code,  
                        (ent?: NpTypes.IBaseEntity) => false, 
                        (vl: string, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new NumberItem (
                        (ent?: NpTypes.IBaseEntity) => 'Farmer Code',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_prodCode,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_prodCode,  
                        (ent?: NpTypes.IBaseEntity) => false, //isRequired
                        (vl: number, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined,
                        0)
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Parcel Decision";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'decisionLight', displayName:'getALString("Decision Light", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='GrpParcelDecision_itm__decisionLight' data-ng-model='row.entity.decisionLight' data-np-ui-model='row.entity._decisionLight' data-ng-change='markEntityAsUpdated(row.entity,&quot;decisionLight&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Green\"), value:1}, {label:getALString(\"Yellow\"), value:2}, {label:getALString(\"Red\"), value:3}, {label:getALString(\"Undefined\"), value:4}]' data-ng-disabled='true' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.parcIdentifier', displayName:'getALString("Identifier", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_parcIdentifier' data-ng-model='row.entity.pclaId.parcIdentifier' data-np-ui-model='row.entity.pclaId._parcIdentifier' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.parcIdentifier&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.parcCode', displayName:'getALString("Parcel \n Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_parcCode' data-ng-model='row.entity.pclaId.parcCode' data-np-ui-model='row.entity.pclaId._parcCode' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.parcCode&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.prodCode', displayName:'getALString("Farmer \n Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'6%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_prodCode' data-ng-model='row.entity.pclaId.prodCode' data-np-ui-model='row.entity.pclaId._prodCode' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.prodCode&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.cultIdDecl.code', displayName:'getALString("Cultivation \n Declared Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'7%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_cultIdDecl_code' data-ng-model='row.entity.pclaId.cultIdDecl.code' data-np-ui-model='row.entity.pclaId.cultIdDecl._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdDecl.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.cultIdDecl.name', displayName:'getALString("Cultivation \n Declared Name", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'9%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_cultIdDecl_name' data-ng-model='row.entity.pclaId.cultIdDecl.name' data-np-ui-model='row.entity.pclaId.cultIdDecl._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdDecl.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.cultIdPred.code', displayName:'getALString("Cultivation \n 1st Prediction \n Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'7%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_cultIdPred_code' data-ng-model='row.entity.pclaId.cultIdPred.code' data-np-ui-model='row.entity.pclaId.cultIdPred._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdPred.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.cultIdPred.name', displayName:'getALString("Cultivation \n 1st Prediction \n Name", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'10%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_cultIdPred_name' data-ng-model='row.entity.pclaId.cultIdPred.name' data-np-ui-model='row.entity.pclaId.cultIdPred._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdPred.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.probPred', displayName:'getALString("Probability \n 1st Prediction", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_probPred' data-ng-model='row.entity.pclaId.probPred' data-np-ui-model='row.entity.pclaId._probPred' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.probPred&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.cultIdPred2.code', displayName:'getALString("Cultivation \n 2nd Prediction  \nCode", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'7%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_cultIdPred2_code' data-ng-model='row.entity.pclaId.cultIdPred2.code' data-np-ui-model='row.entity.pclaId.cultIdPred2._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdPred2.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.cultIdPred2.name', displayName:'getALString("Cultivation \n 2nd Prediction \n Name", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'10%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_cultIdPred2_name' data-ng-model='row.entity.pclaId.cultIdPred2.name' data-np-ui-model='row.entity.pclaId.cultIdPred2._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdPred2.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.probPred2', displayName:'getALString("Probability \n 2nd Prediction", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_probPred2' data-ng-model='row.entity.pclaId.probPred2' data-np-ui-model='row.entity.pclaId._probPred2' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.probPred2&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpParcelDecision():ModelGrpParcelDecision {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "ParcelDecision";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.DecisionMaking): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.ParcelDecision.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.demaId = this.Parent;
                } else {
                    x.demaId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.ParcelDecision {
            var self = this;
            return <Entities.ParcelDecision>self.$scope.modelGrpParcelDecision.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.ParcelDecision, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpDema.controller.isEntityLocked(<Entities.DecisionMaking>cur.demaId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_decisionLight)) {
                    paramData['fsch_decisionLight'] = self.modelGrpParcelDecision.fsch_decisionLight;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpParcelDecision.fsch_prodCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_Code)) {
                    paramData['fsch_Code'] = self.modelGrpParcelDecision.fsch_Code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_Parcel_Identifier)) {
                    paramData['fsch_Parcel_Identifier'] = self.modelGrpParcelDecision.fsch_Parcel_Identifier;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "ParcelDecision/findAllByCriteriaRange_DecisionMakingGrpParcelDecision_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_decisionLight)) {
                    paramData['fsch_decisionLight'] = self.modelGrpParcelDecision.fsch_decisionLight;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpParcelDecision.fsch_prodCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_Code)) {
                    paramData['fsch_Code'] = self.modelGrpParcelDecision.fsch_Code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_Parcel_Identifier)) {
                    paramData['fsch_Parcel_Identifier'] = self.modelGrpParcelDecision.fsch_Parcel_Identifier;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "ParcelDecision/findAllByCriteriaRange_DecisionMakingGrpParcelDecision";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_decisionLight)) {
                    paramData['fsch_decisionLight'] = self.modelGrpParcelDecision.fsch_decisionLight;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpParcelDecision.fsch_prodCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_Code)) {
                    paramData['fsch_Code'] = self.modelGrpParcelDecision.fsch_Code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_Parcel_Identifier)) {
                    paramData['fsch_Parcel_Identifier'] = self.modelGrpParcelDecision.fsch_Parcel_Identifier;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "ParcelDecision/findAllByCriteriaRange_DecisionMakingGrpParcelDecision_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_decisionLight)) {
                    paramData['fsch_decisionLight'] = self.modelGrpParcelDecision.fsch_decisionLight;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpParcelDecision.fsch_prodCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_Code)) {
                    paramData['fsch_Code'] = self.modelGrpParcelDecision.fsch_Code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_Parcel_Identifier)) {
                    paramData['fsch_Parcel_Identifier'] = self.modelGrpParcelDecision.fsch_Parcel_Identifier;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.ParcelDecision[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.ParcelDecision[]) => void> = [];


        public getMergedItems(func: (items: Entities.ParcelDecision[]) => void, bContinuousCaller:boolean=true, parent?: Entities.DecisionMaking, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.ParcelDecision[]):void {
                var mergedEntities = <Entities.ParcelDecision[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.ParcelDecision>e.a).demaId)).
                        map(e => <Entities.ParcelDecision>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "ParcelDecision/findAllByCriteriaRange_DecisionMakingGrpParcelDecision";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['demaId_demaId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.ParcelDecision[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.ParcelDecision):boolean {
            if (this.Parent === undefined || x.demaId === undefined)
                return false;
            return x.demaId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.DecisionMaking {
            var self = this;
            return self.demaId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpDema.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get demaId():Entities.DecisionMaking {
            var self = this;
            return <Entities.DecisionMaking>self.$scope.modelGrpDema.selectedEntities[0];
        }









        public constructEntity(): Entities.ParcelDecision {
            var self = this;
            var ret = new Entities.ParcelDecision(
                /*padeId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*decisionLight:number*/ null,
                /*rowVersion:number*/ null,
                /*demaId:Entities.DecisionMaking*/ null,
                /*pclaId:Entities.ParcelClas*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.ParcelDecision = <Entities.ParcelDecision>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.demaId = self.demaId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.ParcelDecision): Entities.ParcelDecision {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.ParcelDecision, calledByParent: boolean= false): Entities.ParcelDecision {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.ParcelDecision.Create();
            ret.updateInstance(src);
            ret.padeId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.DecisionMaking, newParent:Entities.DecisionMaking) {
        
            this.model.totalItems = 0;

            this.getMergedItems(parcelDecisionList => {
                parcelDecisionList.forEach(parcelDecision => {
                    var parcelDecisionCloned = this.cloneEntity_internal(parcelDecision, true);
                    parcelDecisionCloned.demaId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(decisionMaking: Entities.DecisionMaking) {
        

            var self = this;
            var toBeDeleted:Array<Entities.ParcelDecision> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var parcelDecision = <Entities.ParcelDecision>change.a
                if (decisionMaking.getKey() === parcelDecision.demaId.getKey()) {
                    toBeDeleted.push(parcelDecision);
                }
            }

            _.each(toBeDeleted, (x:Entities.ParcelDecision) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
            });

        }

        public deleteAllEntitiesUnderParent(decisionMaking: Entities.DecisionMaking, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteParcelDecisionList(parcelDecisionList: Entities.ParcelDecision[]) {
                if (parcelDecisionList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = parcelDecisionList[0];
                    var tail = parcelDecisionList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteParcelDecisionList(tail);
                    });
                }
            }


            this.getMergedItems(parcelDecisionList => {
                deleteParcelDecisionList(parcelDecisionList);
            }, false, decisionMaking);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.ParcelDecision, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        public _disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpParcelDecision_srchr__fsch_decisionLight_disabled(parcelDecision:Entities.ParcelDecision):boolean {
            var self = this;


            return false;
        }
        public GrpParcelDecision_srchr__fsch_Parcel_Identifier_disabled(parcelDecision:Entities.ParcelDecision):boolean {
            var self = this;


            return false;
        }
        public GrpParcelDecision_srchr__fsch_Code_disabled(parcelDecision:Entities.ParcelDecision):boolean {
            var self = this;


            return false;
        }
        public GrpParcelDecision_srchr__fsch_prodCode_disabled(parcelDecision:Entities.ParcelDecision):boolean {
            var self = this;


            return false;
        }
        public GrpParcelDecision_0_disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpParcelDecision_0_invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpParcelDecision_0_0_disabled(parcelDecision:Entities.ParcelDecision):boolean {
            var self = this;
            if (parcelDecision === undefined || parcelDecision === null)
                return true;


            

            var isContainerControlDisabled   = self.GrpParcelDecision_0_disabled();
            var disabledByProgrammerMethod = self.isExportToCSVDisabled(parcelDecision);
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public isExportToCSVDisabled(parcelDecision:Entities.ParcelDecision):boolean { 
            console.warn("Unimplemented function isExportToCSVDisabled()");
            return false; 
        }
        public exportParcelDecisionsToCSV(parcelDecision:Entities.ParcelDecision) {
            var self = this;
            console.log("Method: exportParcelDecisionsToCSV() called")
            console.log(parcelDecision);

        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController.GrpDema_0_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController.GrpDema_0_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            return true;
        }
        public _deleteIsDisabled(parcelDecision:Entities.ParcelDecision):boolean  {
            return true;
        }

    }


    // GROUP GrpEcGroupCopy

    export class ModelGrpEcGroupCopyBase extends Controllers.AbstractGroupTableModel {
        modelGrpEcCultCopy:ModelGrpEcCultCopy;
        modelEcCultCopyCover:ModelEcCultCopyCover;
        modelEcCultCopySuper:ModelEcCultCopySuper;
        controller: ControllerGrpEcGroupCopy;
        public get ecgcId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcGroupCopy>this.selectedEntities[0]).ecgcId;
        }

        public set ecgcId(ecgcId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcGroupCopy>this.selectedEntities[0]).ecgcId = ecgcId_newVal;
        }

        public get _ecgcId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._ecgcId;
        }

        public get description():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcGroupCopy>this.selectedEntities[0]).description;
        }

        public set description(description_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcGroupCopy>this.selectedEntities[0]).description = description_newVal;
        }

        public get _description():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._description;
        }

        public get name():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcGroupCopy>this.selectedEntities[0]).name;
        }

        public set name(name_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcGroupCopy>this.selectedEntities[0]).name = name_newVal;
        }

        public get _name():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._name;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcGroupCopy>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcGroupCopy>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get recordtype():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcGroupCopy>this.selectedEntities[0]).recordtype;
        }

        public set recordtype(recordtype_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcGroupCopy>this.selectedEntities[0]).recordtype = recordtype_newVal;
        }

        public get _recordtype():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._recordtype;
        }

        public get cropLevel():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcGroupCopy>this.selectedEntities[0]).cropLevel;
        }

        public set cropLevel(cropLevel_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcGroupCopy>this.selectedEntities[0]).cropLevel = cropLevel_newVal;
        }

        public get _cropLevel():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cropLevel;
        }

        public get demaId():Entities.DecisionMaking {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcGroupCopy>this.selectedEntities[0]).demaId;
        }

        public set demaId(demaId_newVal:Entities.DecisionMaking) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcGroupCopy>this.selectedEntities[0]).demaId = demaId_newVal;
        }

        public get _demaId():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._demaId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpEcGroupCopy) { super($scope); }
    }



    export interface IScopeGrpEcGroupCopyBase extends Controllers.IAbstractTableGroupScope, IScopeGrpDemaBase{
        globals: Globals;
        modelGrpEcGroupCopy : ModelGrpEcGroupCopy;
        GrpEcGroupCopy_0_disabled():boolean; 
        GrpEcGroupCopy_0_invisible():boolean; 
        GrpEcGroupCopy_1_disabled():boolean; 
        GrpEcGroupCopy_1_invisible():boolean; 
        GrpEcGroupCopy_2_disabled():boolean; 
        GrpEcGroupCopy_2_invisible():boolean; 
        child_group_GrpEcCultCopy_isInvisible():boolean; 
        child_group_EcCultCopyCover_isInvisible():boolean; 
        child_group_EcCultCopySuper_isInvisible():boolean; 

    }



    export class ControllerGrpEcGroupCopyBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpEcGroupCopy,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpEcGroupCopy)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 1});
            var self:ControllerGrpEcGroupCopyBase = this;
            model.controller = <ControllerGrpEcGroupCopy>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpEcGroupCopy = self.model;

            $scope.GrpEcGroupCopy_0_disabled = 
                () => {
                    return self.GrpEcGroupCopy_0_disabled();
                };
            $scope.GrpEcGroupCopy_0_invisible = 
                () => {
                    return self.GrpEcGroupCopy_0_invisible();
                };
            $scope.GrpEcGroupCopy_1_disabled = 
                () => {
                    return self.GrpEcGroupCopy_1_disabled();
                };
            $scope.GrpEcGroupCopy_1_invisible = 
                () => {
                    return self.GrpEcGroupCopy_1_invisible();
                };
            $scope.GrpEcGroupCopy_2_disabled = 
                () => {
                    return self.GrpEcGroupCopy_2_disabled();
                };
            $scope.GrpEcGroupCopy_2_invisible = 
                () => {
                    return self.GrpEcGroupCopy_2_invisible();
                };
            $scope.child_group_GrpEcCultCopy_isInvisible = 
                () => {
                    return self.child_group_GrpEcCultCopy_isInvisible();
                };
            $scope.child_group_EcCultCopyCover_isInvisible = 
                () => {
                    return self.child_group_EcCultCopyCover_isInvisible();
                };
            $scope.child_group_EcCultCopySuper_isInvisible = 
                () => {
                    return self.child_group_EcCultCopySuper_isInvisible();
                };


            $scope.pageModel.modelGrpEcGroupCopy = $scope.modelGrpEcGroupCopy;


            $scope.clearBtnAction = () => { 
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpDema.modelGrpEcGroupCopy = $scope.modelGrpEcGroupCopy;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpDema.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcGroupCopy[] , oldVisible:Entities.EcGroupCopy[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
            //bind entity child collection with child controller merged items
            Entities.EcGroupCopy.ecCultCopyCollection = (ecGroupCopy, func) => {
                this.model.modelGrpEcCultCopy.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, ecGroupCopy);
            }
            //bind entity child collection with child controller merged items
            Entities.EcGroupCopy.ecCultCopyCollection = (ecGroupCopy, func) => {
                this.model.modelEcCultCopyCover.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, ecGroupCopy);
            }
            //bind entity child collection with child controller merged items
            Entities.EcGroupCopy.ecCultCopyCollection = (ecGroupCopy, func) => {
                this.model.modelEcCultCopySuper.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, ecGroupCopy);
            }

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
                //unbind entity child collection with child controller merged items
                Entities.EcGroupCopy.ecCultCopyCollection = null;//(ecGroupCopy, func) => { }
                //unbind entity child collection with child controller merged items
                Entities.EcGroupCopy.ecCultCopyCollection = null;//(ecGroupCopy, func) => { }
                //unbind entity child collection with child controller merged items
                Entities.EcGroupCopy.ecCultCopyCollection = null;//(ecGroupCopy, func) => { }
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpEcGroupCopy";
        }
        public get HtmlDivId(): string {
            return "DecisionMaking_ControllerGrpEcGroupCopy";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "BRE";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'name', displayName:'getALString("Name", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcGroupCopy_itm__name' data-ng-model='row.entity.name' data-np-ui-model='row.entity._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;name&quot;)' data-np-required='true' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'description', displayName:'getALString("Description", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcGroupCopy_itm__description' data-ng-model='row.entity.description' data-np-ui-model='row.entity._description' data-ng-change='markEntityAsUpdated(row.entity,&quot;description&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'cropLevel', displayName:'getALString("Crop Level", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcGroupCopy_itm__cropLevel' data-ng-model='row.entity.cropLevel' data-np-ui-model='row.entity._cropLevel' data-ng-change='markEntityAsUpdated(row.entity,&quot;cropLevel&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Crop\"), value:0}, {label:getALString(\"Land Cover\"), value:1}, {label:getALString(\"Crop to Land Cover\"), value:2}]' data-ng-disabled='true' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpEcGroupCopy():ModelGrpEcGroupCopy {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "EcGroupCopy";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.DecisionMaking): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.EcGroupCopy.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.demaId = this.Parent;
                } else {
                    x.demaId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.EcGroupCopy {
            var self = this;
            return <Entities.EcGroupCopy>self.$scope.modelGrpEcGroupCopy.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.EcGroupCopy, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpDema.controller.isEntityLocked(<Entities.DecisionMaking>cur.demaId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcGroupCopy/findAllByCriteriaRange_DecisionMakingGrpEcGroupCopy_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcGroupCopy/findAllByCriteriaRange_DecisionMakingGrpEcGroupCopy";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcGroupCopy/findAllByCriteriaRange_DecisionMakingGrpEcGroupCopy_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.EcGroupCopy[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.EcGroupCopy[]) => void> = [];


        public getMergedItems(func: (items: Entities.EcGroupCopy[]) => void, bContinuousCaller:boolean=true, parent?: Entities.DecisionMaking, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.EcGroupCopy[]):void {
                var mergedEntities = <Entities.EcGroupCopy[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.EcGroupCopy>e.a).demaId)).
                        map(e => <Entities.EcGroupCopy>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "EcGroupCopy/findAllByCriteriaRange_DecisionMakingGrpEcGroupCopy";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['demaId_demaId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.EcGroupCopy[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.EcGroupCopy):boolean {
            if (this.Parent === undefined || x.demaId === undefined)
                return false;
            return x.demaId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.DecisionMaking {
            var self = this;
            return self.demaId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpDema.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get demaId():Entities.DecisionMaking {
            var self = this;
            return <Entities.DecisionMaking>self.$scope.modelGrpDema.selectedEntities[0];
        }









        public constructEntity(): Entities.EcGroupCopy {
            var self = this;
            var ret = new Entities.EcGroupCopy(
                /*ecgcId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*description:string*/ null,
                /*name:string*/ null,
                /*rowVersion:number*/ null,
                /*recordtype:number*/ null,
                /*cropLevel:number*/ null,
                /*demaId:Entities.DecisionMaking*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.EcGroupCopy = <Entities.EcGroupCopy>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.demaId = self.demaId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.EcGroupCopy): Entities.EcGroupCopy {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.EcGroupCopy, calledByParent: boolean= false): Entities.EcGroupCopy {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.EcGroupCopy.Create();
            ret.updateInstance(src);
            ret.ecgcId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
                this.modelGrpEcGroupCopy.modelGrpEcCultCopy.controller.cloneAllEntitiesUnderParent(src, ret);
                this.modelGrpEcGroupCopy.modelEcCultCopyCover.controller.cloneAllEntitiesUnderParent(src, ret);
                this.modelGrpEcGroupCopy.modelEcCultCopySuper.controller.cloneAllEntitiesUnderParent(src, ret);
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.DecisionMaking, newParent:Entities.DecisionMaking) {
        
            this.model.totalItems = 0;

            this.getMergedItems(ecGroupCopyList => {
                ecGroupCopyList.forEach(ecGroupCopy => {
                    var ecGroupCopyCloned = this.cloneEntity_internal(ecGroupCopy, true);
                    ecGroupCopyCloned.demaId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(decisionMaking: Entities.DecisionMaking) {
        

            var self = this;
            var toBeDeleted:Array<Entities.EcGroupCopy> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var ecGroupCopy = <Entities.EcGroupCopy>change.a
                if (decisionMaking.getKey() === ecGroupCopy.demaId.getKey()) {
                    toBeDeleted.push(ecGroupCopy);
                }
            }

            _.each(toBeDeleted, (x:Entities.EcGroupCopy) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
                self.modelGrpEcGroupCopy.modelGrpEcCultCopy.controller.deleteNewEntitiesUnderParent(x);
                self.modelGrpEcGroupCopy.modelEcCultCopyCover.controller.deleteNewEntitiesUnderParent(x);
                self.modelGrpEcGroupCopy.modelEcCultCopySuper.controller.deleteNewEntitiesUnderParent(x);
            });

        }

        public deleteAllEntitiesUnderParent(decisionMaking: Entities.DecisionMaking, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteEcGroupCopyList(ecGroupCopyList: Entities.EcGroupCopy[]) {
                if (ecGroupCopyList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = ecGroupCopyList[0];
                    var tail = ecGroupCopyList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteEcGroupCopyList(tail);
                    });
                }
            }


            this.getMergedItems(ecGroupCopyList => {
                deleteEcGroupCopyList(ecGroupCopyList);
            }, false, decisionMaking);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                    self.modelGrpEcGroupCopy.modelGrpEcCultCopy.controller,
                    self.modelGrpEcGroupCopy.modelEcCultCopyCover.controller,
                    self.modelGrpEcGroupCopy.modelEcCultCopySuper.controller
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.EcGroupCopy, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                self.modelGrpEcGroupCopy.modelGrpEcCultCopy.controller.deleteAllEntitiesUnderParent(ent, () => {
                    self.modelGrpEcGroupCopy.modelEcCultCopyCover.controller.deleteAllEntitiesUnderParent(ent, () => {
                        self.modelGrpEcGroupCopy.modelEcCultCopySuper.controller.deleteAllEntitiesUnderParent(ent, () => {
                            super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                        });
                    });
                });
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    self.modelGrpEcGroupCopy.modelGrpEcCultCopy.controller.deleteNewEntitiesUnderParent(ent);
                    self.modelGrpEcGroupCopy.modelEcCultCopyCover.controller.deleteNewEntitiesUnderParent(ent);
                    self.modelGrpEcGroupCopy.modelEcCultCopySuper.controller.deleteNewEntitiesUnderParent(ent);
                    afterDeleteAction();
                });
            }
        }


        public GrpEcGroupCopy_0_disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpEcGroupCopy_0_invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  this.isEcGroupChildInv();
            return programmerVal;
        }
        public isEcGroupChildInv():boolean { 
            console.warn("Unimplemented function isEcGroupChildInv()");
            return false; 
        }
        public GrpEcGroupCopy_1_disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpEcGroupCopy_1_invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  this.isEcGroupCoverChildInv();
            return programmerVal;
        }
        public isEcGroupCoverChildInv():boolean { 
            console.warn("Unimplemented function isEcGroupCoverChildInv()");
            return false; 
        }
        public GrpEcGroupCopy_2_disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpEcGroupCopy_2_invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  this.isEcGroupSuperChildInv();
            return programmerVal;
        }
        public isEcGroupSuperChildInv():boolean { 
            console.warn("Unimplemented function isEcGroupSuperChildInv()");
            return false; 
        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController.GrpDema_0_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (true)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            return true;
        }
        public _deleteIsDisabled(ecGroupCopy:Entities.EcGroupCopy):boolean  {
            return true;
        }
        public child_group_GrpEcCultCopy_isInvisible():boolean {
            var self = this;
            if ((self.model.modelGrpEcCultCopy === undefined) || (self.model.modelGrpEcCultCopy.controller === undefined))
                return false;
            return self.model.modelGrpEcCultCopy.controller._isInvisible()
        }
        public child_group_EcCultCopyCover_isInvisible():boolean {
            var self = this;
            if ((self.model.modelEcCultCopyCover === undefined) || (self.model.modelEcCultCopyCover.controller === undefined))
                return false;
            return self.model.modelEcCultCopyCover.controller._isInvisible()
        }
        public child_group_EcCultCopySuper_isInvisible():boolean {
            var self = this;
            if ((self.model.modelEcCultCopySuper === undefined) || (self.model.modelEcCultCopySuper.controller === undefined))
                return false;
            return self.model.modelEcCultCopySuper.controller._isInvisible()
        }

    }


    // GROUP GrpEcCultCopy

    export class ModelGrpEcCultCopyBase extends Controllers.AbstractGroupTableModel {
        modelGrpEcCultDetailCopy:ModelGrpEcCultDetailCopy;
        controller: ControllerGrpEcCultCopy;
        public get ecccId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultCopy>this.selectedEntities[0]).ecccId;
        }

        public set ecccId(ecccId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultCopy>this.selectedEntities[0]).ecccId = ecccId_newVal;
        }

        public get _ecccId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._ecccId;
        }

        public get noneMatchDecision():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultCopy>this.selectedEntities[0]).noneMatchDecision;
        }

        public set noneMatchDecision(noneMatchDecision_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultCopy>this.selectedEntities[0]).noneMatchDecision = noneMatchDecision_newVal;
        }

        public get _noneMatchDecision():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._noneMatchDecision;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultCopy>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultCopy>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get cultId():Entities.Cultivation {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultCopy>this.selectedEntities[0]).cultId;
        }

        public set cultId(cultId_newVal:Entities.Cultivation) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultCopy>this.selectedEntities[0]).cultId = cultId_newVal;
        }

        public get _cultId():NpTypes.UIManyToOneModel<Entities.Cultivation> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cultId;
        }

        public get ecgcId():Entities.EcGroupCopy {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultCopy>this.selectedEntities[0]).ecgcId;
        }

        public set ecgcId(ecgcId_newVal:Entities.EcGroupCopy) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultCopy>this.selectedEntities[0]).ecgcId = ecgcId_newVal;
        }

        public get _ecgcId():NpTypes.UIManyToOneModel<Entities.EcGroupCopy> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._ecgcId;
        }

        public get cotyId():Entities.CoverType {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultCopy>this.selectedEntities[0]).cotyId;
        }

        public set cotyId(cotyId_newVal:Entities.CoverType) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultCopy>this.selectedEntities[0]).cotyId = cotyId_newVal;
        }

        public get _cotyId():NpTypes.UIManyToOneModel<Entities.CoverType> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cotyId;
        }

        public get sucaId():Entities.SuperClas {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultCopy>this.selectedEntities[0]).sucaId;
        }

        public set sucaId(sucaId_newVal:Entities.SuperClas) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultCopy>this.selectedEntities[0]).sucaId = sucaId_newVal;
        }

        public get _sucaId():NpTypes.UIManyToOneModel<Entities.SuperClas> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._sucaId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpEcCultCopy) { super($scope); }
    }



    export interface IScopeGrpEcCultCopyBase extends Controllers.IAbstractTableGroupScope, IScopeGrpEcGroupCopyBase{
        globals: Globals;
        modelGrpEcCultCopy : ModelGrpEcCultCopy;
        showLov_GrpEcCultCopy_itm__cultId(ecCultCopy:Entities.EcCultCopy):void; 
        child_group_GrpEcCultDetailCopy_isInvisible():boolean; 

    }



    export class ControllerGrpEcCultCopyBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpEcCultCopy,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpEcCultCopy)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 1});
            var self:ControllerGrpEcCultCopyBase = this;
            model.controller = <ControllerGrpEcCultCopy>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpEcCultCopy = self.model;

            $scope.showLov_GrpEcCultCopy_itm__cultId = 
                (ecCultCopy:Entities.EcCultCopy) => {
                    $timeout( () => {        
                        self.showLov_GrpEcCultCopy_itm__cultId(ecCultCopy);
                    }, 0);
                };
            $scope.child_group_GrpEcCultDetailCopy_isInvisible = 
                () => {
                    return self.child_group_GrpEcCultDetailCopy_isInvisible();
                };


            $scope.pageModel.modelGrpEcCultCopy = $scope.modelGrpEcCultCopy;


            $scope.clearBtnAction = () => { 
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpEcGroupCopy.modelGrpEcCultCopy = $scope.modelGrpEcCultCopy;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpEcGroupCopy.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCultCopy[] , oldVisible:Entities.EcCultCopy[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
            //bind entity child collection with child controller merged items
            Entities.EcCultCopy.ecCultDetailCopyCollection = (ecCultCopy, func) => {
                this.model.modelGrpEcCultDetailCopy.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, ecCultCopy);
            }

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
                //unbind entity child collection with child controller merged items
                Entities.EcCultCopy.ecCultDetailCopyCollection = null;//(ecCultCopy, func) => { }
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpEcCultCopy";
        }
        public get HtmlDivId(): string {
            return "DecisionMaking_ControllerGrpEcCultCopy";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Cultivation Criteria";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'cultId.name', displayName:'getALString("Crop", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'9%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <div  > \
                    <input data-np-source='row.entity.cultId.name' data-np-ui-model='row.entity._cultId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='GrpEcCultCopy_itm__cultId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_GrpEcCultCopy_itm__cultId(row.entity)'   data-ng-disabled=\"true\"   />  \
                    </div> \
                </div>"},
                { cellClass:'cellToolTip', field:'cultId.code', displayName:'getALString("Crop Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'6%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCultCopy_itm__cultId_code' data-ng-model='row.entity.cultId.code' data-np-ui-model='row.entity.cultId._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultId.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'cultId.cotyId.name', displayName:'getALString("Land Cover", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'9%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCultCopy_itm__cultId_cotyId_name' data-ng-model='row.entity.cultId.cotyId.name' data-np-ui-model='row.entity.cultId.cotyId._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultId.cotyId.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpEcCultCopy():ModelGrpEcCultCopy {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "EcCultCopy";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.EcGroupCopy): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.EcCultCopy.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.ecgcId = this.Parent;
                } else {
                    x.ecgcId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.EcCultCopy {
            var self = this;
            return <Entities.EcCultCopy>self.$scope.modelGrpEcCultCopy.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.EcCultCopy, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpEcGroupCopy.controller.isEntityLocked(<Entities.EcGroupCopy>cur.ecgcId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingGrpEcCultCopy_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingGrpEcCultCopy";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingGrpEcCultCopy_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.EcCultCopy[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.EcCultCopy[]) => void> = [];


        public getMergedItems(func: (items: Entities.EcCultCopy[]) => void, bContinuousCaller:boolean=true, parent?: Entities.EcGroupCopy, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.EcCultCopy[]):void {
                var mergedEntities = <Entities.EcCultCopy[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.EcCultCopy>e.a).ecgcId)).
                        map(e => <Entities.EcCultCopy>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingGrpEcCultCopy";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['ecgcId_ecgcId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.EcCultCopy[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.EcCultCopy):boolean {
            if (this.Parent === undefined || x.ecgcId === undefined)
                return false;
            return x.ecgcId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.EcGroupCopy {
            var self = this;
            return self.ecgcId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpEcGroupCopy.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get ecgcId():Entities.EcGroupCopy {
            var self = this;
            return <Entities.EcGroupCopy>self.$scope.modelGrpEcGroupCopy.selectedEntities[0];
        }









        public constructEntity(): Entities.EcCultCopy {
            var self = this;
            var ret = new Entities.EcCultCopy(
                /*ecccId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*noneMatchDecision:number*/ null,
                /*rowVersion:number*/ null,
                /*cultId:Entities.Cultivation*/ null,
                /*ecgcId:Entities.EcGroupCopy*/ null,
                /*cotyId:Entities.CoverType*/ null,
                /*sucaId:Entities.SuperClas*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.EcCultCopy = <Entities.EcCultCopy>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.ecgcId = self.ecgcId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.EcCultCopy): Entities.EcCultCopy {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.EcCultCopy, calledByParent: boolean= false): Entities.EcCultCopy {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.EcCultCopy.Create();
            ret.updateInstance(src);
            ret.ecccId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
                this.modelGrpEcCultCopy.modelGrpEcCultDetailCopy.controller.cloneAllEntitiesUnderParent(src, ret);
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.EcGroupCopy, newParent:Entities.EcGroupCopy) {
        
            this.model.totalItems = 0;

            this.getMergedItems(ecCultCopyList => {
                ecCultCopyList.forEach(ecCultCopy => {
                    var ecCultCopyCloned = this.cloneEntity_internal(ecCultCopy, true);
                    ecCultCopyCloned.ecgcId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(ecGroupCopy: Entities.EcGroupCopy) {
        

            var self = this;
            var toBeDeleted:Array<Entities.EcCultCopy> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var ecCultCopy = <Entities.EcCultCopy>change.a
                if (ecGroupCopy.getKey() === ecCultCopy.ecgcId.getKey()) {
                    toBeDeleted.push(ecCultCopy);
                }
            }

            _.each(toBeDeleted, (x:Entities.EcCultCopy) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
                self.modelGrpEcCultCopy.modelGrpEcCultDetailCopy.controller.deleteNewEntitiesUnderParent(x);
            });

        }

        public deleteAllEntitiesUnderParent(ecGroupCopy: Entities.EcGroupCopy, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteEcCultCopyList(ecCultCopyList: Entities.EcCultCopy[]) {
                if (ecCultCopyList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = ecCultCopyList[0];
                    var tail = ecCultCopyList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteEcCultCopyList(tail);
                    });
                }
            }


            this.getMergedItems(ecCultCopyList => {
                deleteEcCultCopyList(ecCultCopyList);
            }, false, ecGroupCopy);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                    self.modelGrpEcCultCopy.modelGrpEcCultDetailCopy.controller
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.EcCultCopy, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                self.modelGrpEcCultCopy.modelGrpEcCultDetailCopy.controller.deleteAllEntitiesUnderParent(ent, () => {
                    super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                });
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    self.modelGrpEcCultCopy.modelGrpEcCultDetailCopy.controller.deleteNewEntitiesUnderParent(ent);
                    afterDeleteAction();
                });
            }
        }


        cachedLovModel_GrpEcCultCopy_itm__cultId:ModelLovCultivationBase;
        public showLov_GrpEcCultCopy_itm__cultId(ecCultCopy:Entities.EcCultCopy) {
            var self = this;
            if (ecCultCopy === undefined)
                return;
            var uimodel:NpTypes.IUIModel = ecCultCopy._cultId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Cultivation';
            dialogOptions.previousModel= self.cachedLovModel_GrpEcCultCopy_itm__cultId;
            dialogOptions.className = "ControllerLovCultivation";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var cultivation1:Entities.Cultivation =   <Entities.Cultivation>selectedEntity;
                uimodel.clearAllErrors();
                if(false && isVoid(cultivation1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(cultivation1) && cultivation1.isEqual(ecCultCopy.cultId))
                    return;
                ecCultCopy.cultId = cultivation1;
                self.markEntityAsUpdated(ecCultCopy, 'GrpEcCultCopy_itm__cultId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovCultivationBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpEcCultCopy_itm__cultId = lovModel;
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovCultivationBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['code'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/Cultivation.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController.GrpEcGroupCopy_0_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController.GrpEcGroupCopy_0_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            return true;
        }
        public _deleteIsDisabled(ecCultCopy:Entities.EcCultCopy):boolean  {
            return true;
        }
        public child_group_GrpEcCultDetailCopy_isInvisible():boolean {
            var self = this;
            if ((self.model.modelGrpEcCultDetailCopy === undefined) || (self.model.modelGrpEcCultDetailCopy.controller === undefined))
                return false;
            return self.model.modelGrpEcCultDetailCopy.controller._isInvisible()
        }

    }


    // GROUP GrpEcCultDetailCopy

    export class ModelGrpEcCultDetailCopyBase extends Controllers.AbstractGroupTableModel {
        controller: ControllerGrpEcCultDetailCopy;
        public get ecdcId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).ecdcId;
        }

        public set ecdcId(ecdcId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).ecdcId = ecdcId_newVal;
        }

        public get _ecdcId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._ecdcId;
        }

        public get orderingNumber():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).orderingNumber;
        }

        public set orderingNumber(orderingNumber_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).orderingNumber = orderingNumber_newVal;
        }

        public get _orderingNumber():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._orderingNumber;
        }

        public get agreesDeclar():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).agreesDeclar;
        }

        public set agreesDeclar(agreesDeclar_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).agreesDeclar = agreesDeclar_newVal;
        }

        public get _agreesDeclar():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._agreesDeclar;
        }

        public get comparisonOper():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).comparisonOper;
        }

        public set comparisonOper(comparisonOper_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).comparisonOper = comparisonOper_newVal;
        }

        public get _comparisonOper():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._comparisonOper;
        }

        public get probabThres():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).probabThres;
        }

        public set probabThres(probabThres_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).probabThres = probabThres_newVal;
        }

        public get _probabThres():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probabThres;
        }

        public get decisionLight():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).decisionLight;
        }

        public set decisionLight(decisionLight_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).decisionLight = decisionLight_newVal;
        }

        public get _decisionLight():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._decisionLight;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get agreesDeclar2():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).agreesDeclar2;
        }

        public set agreesDeclar2(agreesDeclar2_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).agreesDeclar2 = agreesDeclar2_newVal;
        }

        public get _agreesDeclar2():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._agreesDeclar2;
        }

        public get comparisonOper2():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).comparisonOper2;
        }

        public set comparisonOper2(comparisonOper2_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).comparisonOper2 = comparisonOper2_newVal;
        }

        public get _comparisonOper2():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._comparisonOper2;
        }

        public get probabThres2():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).probabThres2;
        }

        public set probabThres2(probabThres2_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).probabThres2 = probabThres2_newVal;
        }

        public get _probabThres2():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probabThres2;
        }

        public get probabThresSum():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).probabThresSum;
        }

        public set probabThresSum(probabThresSum_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).probabThresSum = probabThresSum_newVal;
        }

        public get _probabThresSum():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probabThresSum;
        }

        public get comparisonOper3():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).comparisonOper3;
        }

        public set comparisonOper3(comparisonOper3_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).comparisonOper3 = comparisonOper3_newVal;
        }

        public get _comparisonOper3():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._comparisonOper3;
        }

        public get ecccId():Entities.EcCultCopy {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).ecccId;
        }

        public set ecccId(ecccId_newVal:Entities.EcCultCopy) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).ecccId = ecccId_newVal;
        }

        public get _ecccId():NpTypes.UIManyToOneModel<Entities.EcCultCopy> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._ecccId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpEcCultDetailCopy) { super($scope); }
    }



    export interface IScopeGrpEcCultDetailCopyBase extends Controllers.IAbstractTableGroupScope, IScopeGrpEcCultCopyBase{
        globals: Globals;
        modelGrpEcCultDetailCopy : ModelGrpEcCultDetailCopy;

    }



    export class ControllerGrpEcCultDetailCopyBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpEcCultDetailCopy,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpEcCultDetailCopy)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 3});
            var self:ControllerGrpEcCultDetailCopyBase = this;
            model.controller = <ControllerGrpEcCultDetailCopy>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpEcCultDetailCopy = self.model;




            $scope.pageModel.modelGrpEcCultDetailCopy = $scope.modelGrpEcCultDetailCopy;


            $scope.clearBtnAction = () => { 
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpEcCultCopy.modelGrpEcCultDetailCopy = $scope.modelGrpEcCultDetailCopy;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpEcCultCopy.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCultDetailCopy[] , oldVisible:Entities.EcCultDetailCopy[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpEcCultDetailCopy";
        }
        public get HtmlDivId(): string {
            return "DecisionMaking_ControllerGrpEcCultDetailCopy";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Criteria";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'orderingNumber', displayName:'getALString("Ordering \n No", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'5%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCultDetailCopy_itm__orderingNumber' data-ng-model='row.entity.orderingNumber' data-np-ui-model='row.entity._orderingNumber' data-ng-change='markEntityAsUpdated(row.entity,&quot;orderingNumber&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='0' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'agreesDeclar', displayName:'getALString("1st \ Prediction ", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetailCopy_itm__agreesDeclar' data-ng-model='row.entity.agreesDeclar' data-np-ui-model='row.entity._agreesDeclar' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='true' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'comparisonOper', displayName:'getALString("Comparison \n Operator 1st", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'11%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetailCopy_itm__comparisonOper' data-ng-model='row.entity.comparisonOper' data-np-ui-model='row.entity._comparisonOper' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='true' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'probabThres', displayName:'getALString("Confidence \n Pred. 1st (%)", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCultDetailCopy_itm__probabThres' data-ng-model='row.entity.probabThres' data-np-ui-model='row.entity._probabThres' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'agreesDeclar2', displayName:'getALString("2nd \ Prediction ", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetailCopy_itm__agreesDeclar2' data-ng-model='row.entity.agreesDeclar2' data-np-ui-model='row.entity._agreesDeclar2' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='true' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'comparisonOper2', displayName:'getALString("Comparison \n Operator 2nd", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'11%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetailCopy_itm__comparisonOper2' data-ng-model='row.entity.comparisonOper2' data-np-ui-model='row.entity._comparisonOper2' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='true' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'probabThres2', displayName:'getALString("Confidence \n Pred. 2nd (%)", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCultDetailCopy_itm__probabThres2' data-ng-model='row.entity.probabThres2' data-np-ui-model='row.entity._probabThres2' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres2&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'comparisonOper3', displayName:'getALString("Comparison \n Operator 1st + 2nd", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'11%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetailCopy_itm__comparisonOper3' data-ng-model='row.entity.comparisonOper3' data-np-ui-model='row.entity._comparisonOper3' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper3&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='true' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'probabThresSum', displayName:'getALString("Confidence \n Prediction \n 1st + 2nd (%)", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCultDetailCopy_itm__probabThresSum' data-ng-model='row.entity.probabThresSum' data-np-ui-model='row.entity._probabThresSum' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThresSum&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'decisionLight', displayName:'getALString("Traffic \n Light Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetailCopy_itm__decisionLight' data-ng-model='row.entity.decisionLight' data-np-ui-model='row.entity._decisionLight' data-ng-change='markEntityAsUpdated(row.entity,&quot;decisionLight&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Green\"), value:1}, {label:getALString(\"Yellow\"), value:2}, {label:getALString(\"Red\"), value:3}, {label:getALString(\"Undefined\"), value:4}]' data-ng-disabled='true' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpEcCultDetailCopy():ModelGrpEcCultDetailCopy {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "EcCultDetailCopy";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.EcCultCopy): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.EcCultDetailCopy.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.ecccId = this.Parent;
                } else {
                    x.ecccId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.EcCultDetailCopy {
            var self = this;
            return <Entities.EcCultDetailCopy>self.$scope.modelGrpEcCultDetailCopy.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.EcCultDetailCopy, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpEcCultCopy.controller.isEntityLocked(<Entities.EcCultCopy>cur.ecccId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingGrpEcCultDetailCopy_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingGrpEcCultDetailCopy";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingGrpEcCultDetailCopy_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.EcCultDetailCopy[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.EcCultDetailCopy[]) => void> = [];


        public getMergedItems(func: (items: Entities.EcCultDetailCopy[]) => void, bContinuousCaller:boolean=true, parent?: Entities.EcCultCopy, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.EcCultDetailCopy[]):void {
                var mergedEntities = <Entities.EcCultDetailCopy[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.EcCultDetailCopy>e.a).ecccId)).
                        map(e => <Entities.EcCultDetailCopy>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingGrpEcCultDetailCopy";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['ecccId_ecccId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.EcCultDetailCopy[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.EcCultDetailCopy):boolean {
            if (this.Parent === undefined || x.ecccId === undefined)
                return false;
            return x.ecccId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.EcCultCopy {
            var self = this;
            return self.ecccId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpEcCultCopy.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get ecccId():Entities.EcCultCopy {
            var self = this;
            return <Entities.EcCultCopy>self.$scope.modelGrpEcCultCopy.selectedEntities[0];
        }









        public constructEntity(): Entities.EcCultDetailCopy {
            var self = this;
            var ret = new Entities.EcCultDetailCopy(
                /*ecdcId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*orderingNumber:number*/ null,
                /*agreesDeclar:number*/ null,
                /*comparisonOper:number*/ null,
                /*probabThres:number*/ null,
                /*decisionLight:number*/ null,
                /*rowVersion:number*/ null,
                /*agreesDeclar2:number*/ null,
                /*comparisonOper2:number*/ null,
                /*probabThres2:number*/ null,
                /*probabThresSum:number*/ null,
                /*comparisonOper3:number*/ null,
                /*ecccId:Entities.EcCultCopy*/ null);
            this.getMergedItems(items => {
                ret.orderingNumber = items.maxBy(i => i.orderingNumber, 0) + 1;
            }, false);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.EcCultDetailCopy = <Entities.EcCultDetailCopy>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.ecccId = self.ecccId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.EcCultDetailCopy): Entities.EcCultDetailCopy {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.EcCultDetailCopy, calledByParent: boolean= false): Entities.EcCultDetailCopy {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.EcCultDetailCopy.Create();
            ret.updateInstance(src);
            ret.ecdcId = tmpId;
            if (!calledByParent) {
                this.getMergedItems(items => {
                    ret.orderingNumber = items.maxBy(i => i.orderingNumber, 0) + 1;
                }, false);
            }
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.EcCultCopy, newParent:Entities.EcCultCopy) {
        
            this.model.totalItems = 0;

            this.getMergedItems(ecCultDetailCopyList => {
                ecCultDetailCopyList.forEach(ecCultDetailCopy => {
                    var ecCultDetailCopyCloned = this.cloneEntity_internal(ecCultDetailCopy, true);
                    ecCultDetailCopyCloned.ecccId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(ecCultCopy: Entities.EcCultCopy) {
        

            var self = this;
            var toBeDeleted:Array<Entities.EcCultDetailCopy> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var ecCultDetailCopy = <Entities.EcCultDetailCopy>change.a
                if (ecCultCopy.getKey() === ecCultDetailCopy.ecccId.getKey()) {
                    toBeDeleted.push(ecCultDetailCopy);
                }
            }

            _.each(toBeDeleted, (x:Entities.EcCultDetailCopy) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
            });

        }

        public deleteAllEntitiesUnderParent(ecCultCopy: Entities.EcCultCopy, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteEcCultDetailCopyList(ecCultDetailCopyList: Entities.EcCultDetailCopy[]) {
                if (ecCultDetailCopyList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = ecCultDetailCopyList[0];
                    var tail = ecCultDetailCopyList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteEcCultDetailCopyList(tail);
                    });
                }
            }


            this.getMergedItems(ecCultDetailCopyList => {
                deleteEcCultDetailCopyList(ecCultDetailCopyList);
            }, false, ecCultCopy);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.EcCultDetailCopy, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            return true;
        }
        public _deleteIsDisabled(ecCultDetailCopy:Entities.EcCultDetailCopy):boolean  {
            return true;
        }

    }


    // GROUP EcCultCopyCover

    export class ModelEcCultCopyCoverBase extends Controllers.AbstractGroupTableModel {
        modelEcCultDetailCopyCover:ModelEcCultDetailCopyCover;
        controller: ControllerEcCultCopyCover;
        public get ecccId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultCopy>this.selectedEntities[0]).ecccId;
        }

        public set ecccId(ecccId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultCopy>this.selectedEntities[0]).ecccId = ecccId_newVal;
        }

        public get _ecccId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._ecccId;
        }

        public get noneMatchDecision():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultCopy>this.selectedEntities[0]).noneMatchDecision;
        }

        public set noneMatchDecision(noneMatchDecision_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultCopy>this.selectedEntities[0]).noneMatchDecision = noneMatchDecision_newVal;
        }

        public get _noneMatchDecision():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._noneMatchDecision;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultCopy>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultCopy>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get cultId():Entities.Cultivation {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultCopy>this.selectedEntities[0]).cultId;
        }

        public set cultId(cultId_newVal:Entities.Cultivation) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultCopy>this.selectedEntities[0]).cultId = cultId_newVal;
        }

        public get _cultId():NpTypes.UIManyToOneModel<Entities.Cultivation> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cultId;
        }

        public get ecgcId():Entities.EcGroupCopy {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultCopy>this.selectedEntities[0]).ecgcId;
        }

        public set ecgcId(ecgcId_newVal:Entities.EcGroupCopy) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultCopy>this.selectedEntities[0]).ecgcId = ecgcId_newVal;
        }

        public get _ecgcId():NpTypes.UIManyToOneModel<Entities.EcGroupCopy> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._ecgcId;
        }

        public get cotyId():Entities.CoverType {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultCopy>this.selectedEntities[0]).cotyId;
        }

        public set cotyId(cotyId_newVal:Entities.CoverType) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultCopy>this.selectedEntities[0]).cotyId = cotyId_newVal;
        }

        public get _cotyId():NpTypes.UIManyToOneModel<Entities.CoverType> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cotyId;
        }

        public get sucaId():Entities.SuperClas {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultCopy>this.selectedEntities[0]).sucaId;
        }

        public set sucaId(sucaId_newVal:Entities.SuperClas) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultCopy>this.selectedEntities[0]).sucaId = sucaId_newVal;
        }

        public get _sucaId():NpTypes.UIManyToOneModel<Entities.SuperClas> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._sucaId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeEcCultCopyCover) { super($scope); }
    }



    export interface IScopeEcCultCopyCoverBase extends Controllers.IAbstractTableGroupScope, IScopeGrpEcGroupCopyBase{
        globals: Globals;
        modelEcCultCopyCover : ModelEcCultCopyCover;
        showLov_EcCultCopyCover_itm__cotyId(ecCultCopy:Entities.EcCultCopy):void; 
        child_group_EcCultDetailCopyCover_isInvisible():boolean; 

    }



    export class ControllerEcCultCopyCoverBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeEcCultCopyCover,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelEcCultCopyCover)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 1});
            var self:ControllerEcCultCopyCoverBase = this;
            model.controller = <ControllerEcCultCopyCover>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelEcCultCopyCover = self.model;

            $scope.showLov_EcCultCopyCover_itm__cotyId = 
                (ecCultCopy:Entities.EcCultCopy) => {
                    $timeout( () => {        
                        self.showLov_EcCultCopyCover_itm__cotyId(ecCultCopy);
                    }, 0);
                };
            $scope.child_group_EcCultDetailCopyCover_isInvisible = 
                () => {
                    return self.child_group_EcCultDetailCopyCover_isInvisible();
                };


            $scope.pageModel.modelEcCultCopyCover = $scope.modelEcCultCopyCover;


            $scope.clearBtnAction = () => { 
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpEcGroupCopy.modelEcCultCopyCover = $scope.modelEcCultCopyCover;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpEcGroupCopy.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCultCopy[] , oldVisible:Entities.EcCultCopy[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
            //bind entity child collection with child controller merged items
            Entities.EcCultCopy.ecCultDetailCopyCollection = (ecCultCopy, func) => {
                this.model.modelEcCultDetailCopyCover.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, ecCultCopy);
            }

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
                //unbind entity child collection with child controller merged items
                Entities.EcCultCopy.ecCultDetailCopyCollection = null;//(ecCultCopy, func) => { }
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerEcCultCopyCover";
        }
        public get HtmlDivId(): string {
            return "DecisionMaking_ControllerEcCultCopyCover";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Land Cover Criteria";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'cotyId.name', displayName:'getALString("Land Cover", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'9%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <div  > \
                    <input data-np-source='row.entity.cotyId.name' data-np-ui-model='row.entity._cotyId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='EcCultCopyCover_itm__cotyId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_EcCultCopyCover_itm__cotyId(row.entity)'   data-ng-disabled=\"true\"   />  \
                    </div> \
                </div>"},
                { cellClass:'cellToolTip', field:'cotyId.code', displayName:'getALString("Land Cover Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'7%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultCopyCover_itm__cotyId_code' data-ng-model='row.entity.cotyId.code' data-np-ui-model='row.entity.cotyId._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cotyId.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelEcCultCopyCover():ModelEcCultCopyCover {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "EcCultCopy";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.EcGroupCopy): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.EcCultCopy.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.ecgcId = this.Parent;
                } else {
                    x.ecgcId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.EcCultCopy {
            var self = this;
            return <Entities.EcCultCopy>self.$scope.modelEcCultCopyCover.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.EcCultCopy, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpEcGroupCopy.controller.isEntityLocked(<Entities.EcGroupCopy>cur.ecgcId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingEcCultCopyCover_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingEcCultCopyCover";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingEcCultCopyCover_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.EcCultCopy[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.EcCultCopy[]) => void> = [];


        public getMergedItems(func: (items: Entities.EcCultCopy[]) => void, bContinuousCaller:boolean=true, parent?: Entities.EcGroupCopy, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.EcCultCopy[]):void {
                var mergedEntities = <Entities.EcCultCopy[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.EcCultCopy>e.a).ecgcId)).
                        map(e => <Entities.EcCultCopy>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingEcCultCopyCover";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['ecgcId_ecgcId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.EcCultCopy[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.EcCultCopy):boolean {
            if (this.Parent === undefined || x.ecgcId === undefined)
                return false;
            return x.ecgcId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.EcGroupCopy {
            var self = this;
            return self.ecgcId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpEcGroupCopy.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get ecgcId():Entities.EcGroupCopy {
            var self = this;
            return <Entities.EcGroupCopy>self.$scope.modelGrpEcGroupCopy.selectedEntities[0];
        }









        public constructEntity(): Entities.EcCultCopy {
            var self = this;
            var ret = new Entities.EcCultCopy(
                /*ecccId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*noneMatchDecision:number*/ null,
                /*rowVersion:number*/ null,
                /*cultId:Entities.Cultivation*/ null,
                /*ecgcId:Entities.EcGroupCopy*/ null,
                /*cotyId:Entities.CoverType*/ null,
                /*sucaId:Entities.SuperClas*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.EcCultCopy = <Entities.EcCultCopy>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.ecgcId = self.ecgcId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.EcCultCopy): Entities.EcCultCopy {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.EcCultCopy, calledByParent: boolean= false): Entities.EcCultCopy {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.EcCultCopy.Create();
            ret.updateInstance(src);
            ret.ecccId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
                this.modelEcCultCopyCover.modelEcCultDetailCopyCover.controller.cloneAllEntitiesUnderParent(src, ret);
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.EcGroupCopy, newParent:Entities.EcGroupCopy) {
        
            this.model.totalItems = 0;

            this.getMergedItems(ecCultCopyList => {
                ecCultCopyList.forEach(ecCultCopy => {
                    var ecCultCopyCloned = this.cloneEntity_internal(ecCultCopy, true);
                    ecCultCopyCloned.ecgcId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(ecGroupCopy: Entities.EcGroupCopy) {
        

            var self = this;
            var toBeDeleted:Array<Entities.EcCultCopy> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var ecCultCopy = <Entities.EcCultCopy>change.a
                if (ecGroupCopy.getKey() === ecCultCopy.ecgcId.getKey()) {
                    toBeDeleted.push(ecCultCopy);
                }
            }

            _.each(toBeDeleted, (x:Entities.EcCultCopy) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
                self.modelEcCultCopyCover.modelEcCultDetailCopyCover.controller.deleteNewEntitiesUnderParent(x);
            });

        }

        public deleteAllEntitiesUnderParent(ecGroupCopy: Entities.EcGroupCopy, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteEcCultCopyList(ecCultCopyList: Entities.EcCultCopy[]) {
                if (ecCultCopyList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = ecCultCopyList[0];
                    var tail = ecCultCopyList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteEcCultCopyList(tail);
                    });
                }
            }


            this.getMergedItems(ecCultCopyList => {
                deleteEcCultCopyList(ecCultCopyList);
            }, false, ecGroupCopy);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                    self.modelEcCultCopyCover.modelEcCultDetailCopyCover.controller
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.EcCultCopy, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                self.modelEcCultCopyCover.modelEcCultDetailCopyCover.controller.deleteAllEntitiesUnderParent(ent, () => {
                    super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                });
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    self.modelEcCultCopyCover.modelEcCultDetailCopyCover.controller.deleteNewEntitiesUnderParent(ent);
                    afterDeleteAction();
                });
            }
        }


        cachedLovModel_EcCultCopyCover_itm__cotyId:ModelLovCoverTypeBase;
        public showLov_EcCultCopyCover_itm__cotyId(ecCultCopy:Entities.EcCultCopy) {
            var self = this;
            if (ecCultCopy === undefined)
                return;
            var uimodel:NpTypes.IUIModel = ecCultCopy._cotyId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Land Cover';
            dialogOptions.previousModel= self.cachedLovModel_EcCultCopyCover_itm__cotyId;
            dialogOptions.className = "ControllerLovCoverType";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var coverType1:Entities.CoverType =   <Entities.CoverType>selectedEntity;
                uimodel.clearAllErrors();
                if(false && isVoid(coverType1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(coverType1) && coverType1.isEqual(ecCultCopy.cotyId))
                    return;
                ecCultCopy.cotyId = coverType1;
                self.markEntityAsUpdated(ecCultCopy, 'EcCultCopyCover_itm__cotyId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovCoverTypeBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_EcCultCopyCover_itm__cotyId = lovModel;
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovCoverTypeBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['code'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/CoverType.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController.GrpEcGroupCopy_1_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController.GrpEcGroupCopy_1_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            return true;
        }
        public _deleteIsDisabled(ecCultCopy:Entities.EcCultCopy):boolean  {
            return true;
        }
        public child_group_EcCultDetailCopyCover_isInvisible():boolean {
            var self = this;
            if ((self.model.modelEcCultDetailCopyCover === undefined) || (self.model.modelEcCultDetailCopyCover.controller === undefined))
                return false;
            return self.model.modelEcCultDetailCopyCover.controller._isInvisible()
        }

    }


    // GROUP EcCultDetailCopyCover

    export class ModelEcCultDetailCopyCoverBase extends Controllers.AbstractGroupTableModel {
        controller: ControllerEcCultDetailCopyCover;
        public get ecdcId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).ecdcId;
        }

        public set ecdcId(ecdcId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).ecdcId = ecdcId_newVal;
        }

        public get _ecdcId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._ecdcId;
        }

        public get orderingNumber():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).orderingNumber;
        }

        public set orderingNumber(orderingNumber_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).orderingNumber = orderingNumber_newVal;
        }

        public get _orderingNumber():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._orderingNumber;
        }

        public get agreesDeclar():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).agreesDeclar;
        }

        public set agreesDeclar(agreesDeclar_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).agreesDeclar = agreesDeclar_newVal;
        }

        public get _agreesDeclar():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._agreesDeclar;
        }

        public get comparisonOper():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).comparisonOper;
        }

        public set comparisonOper(comparisonOper_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).comparisonOper = comparisonOper_newVal;
        }

        public get _comparisonOper():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._comparisonOper;
        }

        public get probabThres():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).probabThres;
        }

        public set probabThres(probabThres_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).probabThres = probabThres_newVal;
        }

        public get _probabThres():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probabThres;
        }

        public get decisionLight():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).decisionLight;
        }

        public set decisionLight(decisionLight_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).decisionLight = decisionLight_newVal;
        }

        public get _decisionLight():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._decisionLight;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get agreesDeclar2():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).agreesDeclar2;
        }

        public set agreesDeclar2(agreesDeclar2_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).agreesDeclar2 = agreesDeclar2_newVal;
        }

        public get _agreesDeclar2():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._agreesDeclar2;
        }

        public get comparisonOper2():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).comparisonOper2;
        }

        public set comparisonOper2(comparisonOper2_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).comparisonOper2 = comparisonOper2_newVal;
        }

        public get _comparisonOper2():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._comparisonOper2;
        }

        public get probabThres2():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).probabThres2;
        }

        public set probabThres2(probabThres2_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).probabThres2 = probabThres2_newVal;
        }

        public get _probabThres2():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probabThres2;
        }

        public get probabThresSum():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).probabThresSum;
        }

        public set probabThresSum(probabThresSum_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).probabThresSum = probabThresSum_newVal;
        }

        public get _probabThresSum():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probabThresSum;
        }

        public get comparisonOper3():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).comparisonOper3;
        }

        public set comparisonOper3(comparisonOper3_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).comparisonOper3 = comparisonOper3_newVal;
        }

        public get _comparisonOper3():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._comparisonOper3;
        }

        public get ecccId():Entities.EcCultCopy {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).ecccId;
        }

        public set ecccId(ecccId_newVal:Entities.EcCultCopy) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).ecccId = ecccId_newVal;
        }

        public get _ecccId():NpTypes.UIManyToOneModel<Entities.EcCultCopy> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._ecccId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeEcCultDetailCopyCover) { super($scope); }
    }



    export interface IScopeEcCultDetailCopyCoverBase extends Controllers.IAbstractTableGroupScope, IScopeEcCultCopyCoverBase{
        globals: Globals;
        modelEcCultDetailCopyCover : ModelEcCultDetailCopyCover;

    }



    export class ControllerEcCultDetailCopyCoverBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeEcCultDetailCopyCover,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelEcCultDetailCopyCover)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 3});
            var self:ControllerEcCultDetailCopyCoverBase = this;
            model.controller = <ControllerEcCultDetailCopyCover>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelEcCultDetailCopyCover = self.model;




            $scope.pageModel.modelEcCultDetailCopyCover = $scope.modelEcCultDetailCopyCover;


            $scope.clearBtnAction = () => { 
                self.updateGrid(0, false, true);
            };



            $scope.modelEcCultCopyCover.modelEcCultDetailCopyCover = $scope.modelEcCultDetailCopyCover;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelEcCultCopyCover.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCultDetailCopy[] , oldVisible:Entities.EcCultDetailCopy[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerEcCultDetailCopyCover";
        }
        public get HtmlDivId(): string {
            return "DecisionMaking_ControllerEcCultDetailCopyCover";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Criteria";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'orderingNumber', displayName:'getALString("Ordering \n No", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'5%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCopyCover_itm__orderingNumber' data-ng-model='row.entity.orderingNumber' data-np-ui-model='row.entity._orderingNumber' data-ng-change='markEntityAsUpdated(row.entity,&quot;orderingNumber&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='0' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'agreesDeclar', displayName:'getALString("1st \ Prediction ", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopyCover_itm__agreesDeclar' data-ng-model='row.entity.agreesDeclar' data-np-ui-model='row.entity._agreesDeclar' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='true' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'comparisonOper', displayName:'getALString("Comparison \n Operator 1st", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'11%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopyCover_itm__comparisonOper' data-ng-model='row.entity.comparisonOper' data-np-ui-model='row.entity._comparisonOper' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='true' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'probabThres', displayName:'getALString("Confidence \n Pred. 1st (%)", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCopyCover_itm__probabThres' data-ng-model='row.entity.probabThres' data-np-ui-model='row.entity._probabThres' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'agreesDeclar2', displayName:'getALString("2nd \ Prediction ", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopyCover_itm__agreesDeclar2' data-ng-model='row.entity.agreesDeclar2' data-np-ui-model='row.entity._agreesDeclar2' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='true' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'comparisonOper2', displayName:'getALString("Comparison \n Operator 2nd", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'11%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopyCover_itm__comparisonOper2' data-ng-model='row.entity.comparisonOper2' data-np-ui-model='row.entity._comparisonOper2' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='true' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'probabThres2', displayName:'getALString("Confidence \n Pred. 2nd (%)", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCopyCover_itm__probabThres2' data-ng-model='row.entity.probabThres2' data-np-ui-model='row.entity._probabThres2' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres2&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'comparisonOper3', displayName:'getALString("Comparison \n Operator 1st + 2nd", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'11%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopyCover_itm__comparisonOper3' data-ng-model='row.entity.comparisonOper3' data-np-ui-model='row.entity._comparisonOper3' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper3&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='true' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'probabThresSum', displayName:'getALString("Confidence \n Prediction \n 1st + 2nd (%)", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCopyCover_itm__probabThresSum' data-ng-model='row.entity.probabThresSum' data-np-ui-model='row.entity._probabThresSum' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThresSum&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'decisionLight', displayName:'getALString("Traffic \n Light Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopyCover_itm__decisionLight' data-ng-model='row.entity.decisionLight' data-np-ui-model='row.entity._decisionLight' data-ng-change='markEntityAsUpdated(row.entity,&quot;decisionLight&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Green\"), value:1}, {label:getALString(\"Yellow\"), value:2}, {label:getALString(\"Red\"), value:3}, {label:getALString(\"Undefined\"), value:4}]' data-ng-disabled='true' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelEcCultDetailCopyCover():ModelEcCultDetailCopyCover {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "EcCultDetailCopy";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.EcCultCopy): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.EcCultDetailCopy.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.ecccId = this.Parent;
                } else {
                    x.ecccId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.EcCultDetailCopy {
            var self = this;
            return <Entities.EcCultDetailCopy>self.$scope.modelEcCultDetailCopyCover.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.EcCultDetailCopy, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelEcCultCopyCover.controller.isEntityLocked(<Entities.EcCultCopy>cur.ecccId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingEcCultDetailCopyCover_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingEcCultDetailCopyCover";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingEcCultDetailCopyCover_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.EcCultDetailCopy[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.EcCultDetailCopy[]) => void> = [];


        public getMergedItems(func: (items: Entities.EcCultDetailCopy[]) => void, bContinuousCaller:boolean=true, parent?: Entities.EcCultCopy, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.EcCultDetailCopy[]):void {
                var mergedEntities = <Entities.EcCultDetailCopy[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.EcCultDetailCopy>e.a).ecccId)).
                        map(e => <Entities.EcCultDetailCopy>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingEcCultDetailCopyCover";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['ecccId_ecccId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.EcCultDetailCopy[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.EcCultDetailCopy):boolean {
            if (this.Parent === undefined || x.ecccId === undefined)
                return false;
            return x.ecccId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.EcCultCopy {
            var self = this;
            return self.ecccId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelEcCultCopyCover.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get ecccId():Entities.EcCultCopy {
            var self = this;
            return <Entities.EcCultCopy>self.$scope.modelEcCultCopyCover.selectedEntities[0];
        }









        public constructEntity(): Entities.EcCultDetailCopy {
            var self = this;
            var ret = new Entities.EcCultDetailCopy(
                /*ecdcId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*orderingNumber:number*/ null,
                /*agreesDeclar:number*/ null,
                /*comparisonOper:number*/ null,
                /*probabThres:number*/ null,
                /*decisionLight:number*/ null,
                /*rowVersion:number*/ null,
                /*agreesDeclar2:number*/ null,
                /*comparisonOper2:number*/ null,
                /*probabThres2:number*/ null,
                /*probabThresSum:number*/ null,
                /*comparisonOper3:number*/ null,
                /*ecccId:Entities.EcCultCopy*/ null);
            this.getMergedItems(items => {
                ret.orderingNumber = items.maxBy(i => i.orderingNumber, 0) + 1;
            }, false);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.EcCultDetailCopy = <Entities.EcCultDetailCopy>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.ecccId = self.ecccId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.EcCultDetailCopy): Entities.EcCultDetailCopy {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.EcCultDetailCopy, calledByParent: boolean= false): Entities.EcCultDetailCopy {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.EcCultDetailCopy.Create();
            ret.updateInstance(src);
            ret.ecdcId = tmpId;
            if (!calledByParent) {
                this.getMergedItems(items => {
                    ret.orderingNumber = items.maxBy(i => i.orderingNumber, 0) + 1;
                }, false);
            }
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.EcCultCopy, newParent:Entities.EcCultCopy) {
        
            this.model.totalItems = 0;

            this.getMergedItems(ecCultDetailCopyList => {
                ecCultDetailCopyList.forEach(ecCultDetailCopy => {
                    var ecCultDetailCopyCloned = this.cloneEntity_internal(ecCultDetailCopy, true);
                    ecCultDetailCopyCloned.ecccId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(ecCultCopy: Entities.EcCultCopy) {
        

            var self = this;
            var toBeDeleted:Array<Entities.EcCultDetailCopy> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var ecCultDetailCopy = <Entities.EcCultDetailCopy>change.a
                if (ecCultCopy.getKey() === ecCultDetailCopy.ecccId.getKey()) {
                    toBeDeleted.push(ecCultDetailCopy);
                }
            }

            _.each(toBeDeleted, (x:Entities.EcCultDetailCopy) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
            });

        }

        public deleteAllEntitiesUnderParent(ecCultCopy: Entities.EcCultCopy, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteEcCultDetailCopyList(ecCultDetailCopyList: Entities.EcCultDetailCopy[]) {
                if (ecCultDetailCopyList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = ecCultDetailCopyList[0];
                    var tail = ecCultDetailCopyList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteEcCultDetailCopyList(tail);
                    });
                }
            }


            this.getMergedItems(ecCultDetailCopyList => {
                deleteEcCultDetailCopyList(ecCultDetailCopyList);
            }, false, ecCultCopy);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.EcCultDetailCopy, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            return true;
        }
        public _deleteIsDisabled(ecCultDetailCopy:Entities.EcCultDetailCopy):boolean  {
            return true;
        }

    }


    // GROUP EcCultCopySuper

    export class ModelEcCultCopySuperBase extends Controllers.AbstractGroupTableModel {
        modelEcCultDetailCopySuper:ModelEcCultDetailCopySuper;
        controller: ControllerEcCultCopySuper;
        public get ecccId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultCopy>this.selectedEntities[0]).ecccId;
        }

        public set ecccId(ecccId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultCopy>this.selectedEntities[0]).ecccId = ecccId_newVal;
        }

        public get _ecccId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._ecccId;
        }

        public get noneMatchDecision():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultCopy>this.selectedEntities[0]).noneMatchDecision;
        }

        public set noneMatchDecision(noneMatchDecision_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultCopy>this.selectedEntities[0]).noneMatchDecision = noneMatchDecision_newVal;
        }

        public get _noneMatchDecision():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._noneMatchDecision;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultCopy>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultCopy>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get cultId():Entities.Cultivation {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultCopy>this.selectedEntities[0]).cultId;
        }

        public set cultId(cultId_newVal:Entities.Cultivation) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultCopy>this.selectedEntities[0]).cultId = cultId_newVal;
        }

        public get _cultId():NpTypes.UIManyToOneModel<Entities.Cultivation> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cultId;
        }

        public get ecgcId():Entities.EcGroupCopy {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultCopy>this.selectedEntities[0]).ecgcId;
        }

        public set ecgcId(ecgcId_newVal:Entities.EcGroupCopy) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultCopy>this.selectedEntities[0]).ecgcId = ecgcId_newVal;
        }

        public get _ecgcId():NpTypes.UIManyToOneModel<Entities.EcGroupCopy> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._ecgcId;
        }

        public get cotyId():Entities.CoverType {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultCopy>this.selectedEntities[0]).cotyId;
        }

        public set cotyId(cotyId_newVal:Entities.CoverType) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultCopy>this.selectedEntities[0]).cotyId = cotyId_newVal;
        }

        public get _cotyId():NpTypes.UIManyToOneModel<Entities.CoverType> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cotyId;
        }

        public get sucaId():Entities.SuperClas {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultCopy>this.selectedEntities[0]).sucaId;
        }

        public set sucaId(sucaId_newVal:Entities.SuperClas) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultCopy>this.selectedEntities[0]).sucaId = sucaId_newVal;
        }

        public get _sucaId():NpTypes.UIManyToOneModel<Entities.SuperClas> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._sucaId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeEcCultCopySuper) { super($scope); }
    }



    export interface IScopeEcCultCopySuperBase extends Controllers.IAbstractTableGroupScope, IScopeGrpEcGroupCopyBase{
        globals: Globals;
        modelEcCultCopySuper : ModelEcCultCopySuper;
        showLov_EcCultCopySuper_itm__cultId(ecCultCopy:Entities.EcCultCopy):void; 
        child_group_EcCultDetailCopySuper_isInvisible():boolean; 

    }



    export class ControllerEcCultCopySuperBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeEcCultCopySuper,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelEcCultCopySuper)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 1});
            var self:ControllerEcCultCopySuperBase = this;
            model.controller = <ControllerEcCultCopySuper>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelEcCultCopySuper = self.model;

            $scope.showLov_EcCultCopySuper_itm__cultId = 
                (ecCultCopy:Entities.EcCultCopy) => {
                    $timeout( () => {        
                        self.showLov_EcCultCopySuper_itm__cultId(ecCultCopy);
                    }, 0);
                };
            $scope.child_group_EcCultDetailCopySuper_isInvisible = 
                () => {
                    return self.child_group_EcCultDetailCopySuper_isInvisible();
                };


            $scope.pageModel.modelEcCultCopySuper = $scope.modelEcCultCopySuper;


            $scope.clearBtnAction = () => { 
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpEcGroupCopy.modelEcCultCopySuper = $scope.modelEcCultCopySuper;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpEcGroupCopy.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCultCopy[] , oldVisible:Entities.EcCultCopy[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
            //bind entity child collection with child controller merged items
            Entities.EcCultCopy.ecCultDetailCopyCollection = (ecCultCopy, func) => {
                this.model.modelEcCultDetailCopySuper.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, ecCultCopy);
            }

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
                //unbind entity child collection with child controller merged items
                Entities.EcCultCopy.ecCultDetailCopyCollection = null;//(ecCultCopy, func) => { }
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerEcCultCopySuper";
        }
        public get HtmlDivId(): string {
            return "DecisionMaking_ControllerEcCultCopySuper";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Crop to Land Cover Criteria";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'cultId.name', displayName:'getALString("Crop", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'9%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <div  > \
                    <input data-np-source='row.entity.cultId.name' data-np-ui-model='row.entity._cultId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='EcCultCopySuper_itm__cultId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_EcCultCopySuper_itm__cultId(row.entity)'   data-ng-disabled=\"true\"   />  \
                    </div> \
                </div>"},
                { cellClass:'cellToolTip', field:'cultId.code', displayName:'getALString("Crop Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultCopySuper_itm__cultId_code' data-ng-model='row.entity.cultId.code' data-np-ui-model='row.entity.cultId._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultId.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'cultId.cotyId.name', displayName:'getALString("Cover Type", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'9%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultCopySuper_itm__cultId_cotyId_name' data-ng-model='row.entity.cultId.cotyId.name' data-np-ui-model='row.entity.cultId.cotyId._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultId.cotyId.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'cotyId.code', displayName:'getALString("Land Cover Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultCopySuper_itm__cotyId_code' data-ng-model='row.entity.cotyId.code' data-np-ui-model='row.entity.cotyId._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cotyId.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelEcCultCopySuper():ModelEcCultCopySuper {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "EcCultCopy";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.EcGroupCopy): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.EcCultCopy.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.ecgcId = this.Parent;
                } else {
                    x.ecgcId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.EcCultCopy {
            var self = this;
            return <Entities.EcCultCopy>self.$scope.modelEcCultCopySuper.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.EcCultCopy, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpEcGroupCopy.controller.isEntityLocked(<Entities.EcGroupCopy>cur.ecgcId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingEcCultCopySuper_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingEcCultCopySuper";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingEcCultCopySuper_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.EcCultCopy[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.EcCultCopy[]) => void> = [];


        public getMergedItems(func: (items: Entities.EcCultCopy[]) => void, bContinuousCaller:boolean=true, parent?: Entities.EcGroupCopy, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.EcCultCopy[]):void {
                var mergedEntities = <Entities.EcCultCopy[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.EcCultCopy>e.a).ecgcId)).
                        map(e => <Entities.EcCultCopy>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingEcCultCopySuper";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['ecgcId_ecgcId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.EcCultCopy[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.EcCultCopy):boolean {
            if (this.Parent === undefined || x.ecgcId === undefined)
                return false;
            return x.ecgcId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.EcGroupCopy {
            var self = this;
            return self.ecgcId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpEcGroupCopy.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get ecgcId():Entities.EcGroupCopy {
            var self = this;
            return <Entities.EcGroupCopy>self.$scope.modelGrpEcGroupCopy.selectedEntities[0];
        }









        public constructEntity(): Entities.EcCultCopy {
            var self = this;
            var ret = new Entities.EcCultCopy(
                /*ecccId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*noneMatchDecision:number*/ null,
                /*rowVersion:number*/ null,
                /*cultId:Entities.Cultivation*/ null,
                /*ecgcId:Entities.EcGroupCopy*/ null,
                /*cotyId:Entities.CoverType*/ null,
                /*sucaId:Entities.SuperClas*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.EcCultCopy = <Entities.EcCultCopy>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.ecgcId = self.ecgcId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.EcCultCopy): Entities.EcCultCopy {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.EcCultCopy, calledByParent: boolean= false): Entities.EcCultCopy {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.EcCultCopy.Create();
            ret.updateInstance(src);
            ret.ecccId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
                this.modelEcCultCopySuper.modelEcCultDetailCopySuper.controller.cloneAllEntitiesUnderParent(src, ret);
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.EcGroupCopy, newParent:Entities.EcGroupCopy) {
        
            this.model.totalItems = 0;

            this.getMergedItems(ecCultCopyList => {
                ecCultCopyList.forEach(ecCultCopy => {
                    var ecCultCopyCloned = this.cloneEntity_internal(ecCultCopy, true);
                    ecCultCopyCloned.ecgcId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(ecGroupCopy: Entities.EcGroupCopy) {
        

            var self = this;
            var toBeDeleted:Array<Entities.EcCultCopy> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var ecCultCopy = <Entities.EcCultCopy>change.a
                if (ecGroupCopy.getKey() === ecCultCopy.ecgcId.getKey()) {
                    toBeDeleted.push(ecCultCopy);
                }
            }

            _.each(toBeDeleted, (x:Entities.EcCultCopy) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
                self.modelEcCultCopySuper.modelEcCultDetailCopySuper.controller.deleteNewEntitiesUnderParent(x);
            });

        }

        public deleteAllEntitiesUnderParent(ecGroupCopy: Entities.EcGroupCopy, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteEcCultCopyList(ecCultCopyList: Entities.EcCultCopy[]) {
                if (ecCultCopyList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = ecCultCopyList[0];
                    var tail = ecCultCopyList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteEcCultCopyList(tail);
                    });
                }
            }


            this.getMergedItems(ecCultCopyList => {
                deleteEcCultCopyList(ecCultCopyList);
            }, false, ecGroupCopy);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                    self.modelEcCultCopySuper.modelEcCultDetailCopySuper.controller
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.EcCultCopy, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                self.modelEcCultCopySuper.modelEcCultDetailCopySuper.controller.deleteAllEntitiesUnderParent(ent, () => {
                    super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                });
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    self.modelEcCultCopySuper.modelEcCultDetailCopySuper.controller.deleteNewEntitiesUnderParent(ent);
                    afterDeleteAction();
                });
            }
        }


        cachedLovModel_EcCultCopySuper_itm__cultId:ModelLovCultivationBase;
        public showLov_EcCultCopySuper_itm__cultId(ecCultCopy:Entities.EcCultCopy) {
            var self = this;
            if (ecCultCopy === undefined)
                return;
            var uimodel:NpTypes.IUIModel = ecCultCopy._cultId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Cultivation';
            dialogOptions.previousModel= self.cachedLovModel_EcCultCopySuper_itm__cultId;
            dialogOptions.className = "ControllerLovCultivation";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var cultivation1:Entities.Cultivation =   <Entities.Cultivation>selectedEntity;
                uimodel.clearAllErrors();
                if(false && isVoid(cultivation1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(cultivation1) && cultivation1.isEqual(ecCultCopy.cultId))
                    return;
                ecCultCopy.cultId = cultivation1;
                self.markEntityAsUpdated(ecCultCopy, 'EcCultCopySuper_itm__cultId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovCultivationBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_EcCultCopySuper_itm__cultId = lovModel;
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovCultivationBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['code'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/Cultivation.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController.GrpEcGroupCopy_2_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController.GrpEcGroupCopy_2_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            return true;
        }
        public _deleteIsDisabled(ecCultCopy:Entities.EcCultCopy):boolean  {
            return true;
        }
        public child_group_EcCultDetailCopySuper_isInvisible():boolean {
            var self = this;
            if ((self.model.modelEcCultDetailCopySuper === undefined) || (self.model.modelEcCultDetailCopySuper.controller === undefined))
                return false;
            return self.model.modelEcCultDetailCopySuper.controller._isInvisible()
        }

    }


    // GROUP EcCultDetailCopySuper

    export class ModelEcCultDetailCopySuperBase extends Controllers.AbstractGroupTableModel {
        controller: ControllerEcCultDetailCopySuper;
        public get ecdcId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).ecdcId;
        }

        public set ecdcId(ecdcId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).ecdcId = ecdcId_newVal;
        }

        public get _ecdcId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._ecdcId;
        }

        public get orderingNumber():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).orderingNumber;
        }

        public set orderingNumber(orderingNumber_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).orderingNumber = orderingNumber_newVal;
        }

        public get _orderingNumber():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._orderingNumber;
        }

        public get agreesDeclar():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).agreesDeclar;
        }

        public set agreesDeclar(agreesDeclar_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).agreesDeclar = agreesDeclar_newVal;
        }

        public get _agreesDeclar():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._agreesDeclar;
        }

        public get comparisonOper():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).comparisonOper;
        }

        public set comparisonOper(comparisonOper_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).comparisonOper = comparisonOper_newVal;
        }

        public get _comparisonOper():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._comparisonOper;
        }

        public get probabThres():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).probabThres;
        }

        public set probabThres(probabThres_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).probabThres = probabThres_newVal;
        }

        public get _probabThres():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probabThres;
        }

        public get decisionLight():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).decisionLight;
        }

        public set decisionLight(decisionLight_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).decisionLight = decisionLight_newVal;
        }

        public get _decisionLight():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._decisionLight;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get agreesDeclar2():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).agreesDeclar2;
        }

        public set agreesDeclar2(agreesDeclar2_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).agreesDeclar2 = agreesDeclar2_newVal;
        }

        public get _agreesDeclar2():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._agreesDeclar2;
        }

        public get comparisonOper2():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).comparisonOper2;
        }

        public set comparisonOper2(comparisonOper2_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).comparisonOper2 = comparisonOper2_newVal;
        }

        public get _comparisonOper2():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._comparisonOper2;
        }

        public get probabThres2():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).probabThres2;
        }

        public set probabThres2(probabThres2_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).probabThres2 = probabThres2_newVal;
        }

        public get _probabThres2():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probabThres2;
        }

        public get probabThresSum():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).probabThresSum;
        }

        public set probabThresSum(probabThresSum_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).probabThresSum = probabThresSum_newVal;
        }

        public get _probabThresSum():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probabThresSum;
        }

        public get comparisonOper3():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).comparisonOper3;
        }

        public set comparisonOper3(comparisonOper3_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).comparisonOper3 = comparisonOper3_newVal;
        }

        public get _comparisonOper3():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._comparisonOper3;
        }

        public get ecccId():Entities.EcCultCopy {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetailCopy>this.selectedEntities[0]).ecccId;
        }

        public set ecccId(ecccId_newVal:Entities.EcCultCopy) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetailCopy>this.selectedEntities[0]).ecccId = ecccId_newVal;
        }

        public get _ecccId():NpTypes.UIManyToOneModel<Entities.EcCultCopy> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._ecccId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeEcCultDetailCopySuper) { super($scope); }
    }



    export interface IScopeEcCultDetailCopySuperBase extends Controllers.IAbstractTableGroupScope, IScopeEcCultCopySuperBase{
        globals: Globals;
        modelEcCultDetailCopySuper : ModelEcCultDetailCopySuper;

    }



    export class ControllerEcCultDetailCopySuperBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeEcCultDetailCopySuper,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelEcCultDetailCopySuper)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 3});
            var self:ControllerEcCultDetailCopySuperBase = this;
            model.controller = <ControllerEcCultDetailCopySuper>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelEcCultDetailCopySuper = self.model;




            $scope.pageModel.modelEcCultDetailCopySuper = $scope.modelEcCultDetailCopySuper;


            $scope.clearBtnAction = () => { 
                self.updateGrid(0, false, true);
            };



            $scope.modelEcCultCopySuper.modelEcCultDetailCopySuper = $scope.modelEcCultDetailCopySuper;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelEcCultCopySuper.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCultDetailCopy[] , oldVisible:Entities.EcCultDetailCopy[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerEcCultDetailCopySuper";
        }
        public get HtmlDivId(): string {
            return "DecisionMaking_ControllerEcCultDetailCopySuper";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Criteria";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'orderingNumber', displayName:'getALString("Ordering \n No", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'5%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCopySuper_itm__orderingNumber' data-ng-model='row.entity.orderingNumber' data-np-ui-model='row.entity._orderingNumber' data-ng-change='markEntityAsUpdated(row.entity,&quot;orderingNumber&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='0' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'agreesDeclar', displayName:'getALString("1st \ Prediction ", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopySuper_itm__agreesDeclar' data-ng-model='row.entity.agreesDeclar' data-np-ui-model='row.entity._agreesDeclar' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='true' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'comparisonOper', displayName:'getALString("Comparison \n Operator 1st", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'11%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopySuper_itm__comparisonOper' data-ng-model='row.entity.comparisonOper' data-np-ui-model='row.entity._comparisonOper' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='true' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'probabThres', displayName:'getALString("Confidence \n Pred. 1st (%)", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCopySuper_itm__probabThres' data-ng-model='row.entity.probabThres' data-np-ui-model='row.entity._probabThres' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'agreesDeclar2', displayName:'getALString("2nd \ Prediction ", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopySuper_itm__agreesDeclar2' data-ng-model='row.entity.agreesDeclar2' data-np-ui-model='row.entity._agreesDeclar2' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='true' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'comparisonOper2', displayName:'getALString("Comparison \n Operator 2nd", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'11%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopySuper_itm__comparisonOper2' data-ng-model='row.entity.comparisonOper2' data-np-ui-model='row.entity._comparisonOper2' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='true' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'probabThres2', displayName:'getALString("Confidence \n Pred. 2nd (%)", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCopySuper_itm__probabThres2' data-ng-model='row.entity.probabThres2' data-np-ui-model='row.entity._probabThres2' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres2&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'comparisonOper3', displayName:'getALString("Comparison \n Operator 1st + 2nd", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'11%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopySuper_itm__comparisonOper3' data-ng-model='row.entity.comparisonOper3' data-np-ui-model='row.entity._comparisonOper3' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper3&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='true' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'probabThresSum', displayName:'getALString("Confidence \n Prediction \n 1st + 2nd (%)", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCopySuper_itm__probabThresSum' data-ng-model='row.entity.probabThresSum' data-np-ui-model='row.entity._probabThresSum' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThresSum&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'decisionLight', displayName:'getALString("Traffic \n Light Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopySuper_itm__decisionLight' data-ng-model='row.entity.decisionLight' data-np-ui-model='row.entity._decisionLight' data-ng-change='markEntityAsUpdated(row.entity,&quot;decisionLight&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Green\"), value:1}, {label:getALString(\"Yellow\"), value:2}, {label:getALString(\"Red\"), value:3}, {label:getALString(\"Undefined\"), value:4}]' data-ng-disabled='true' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelEcCultDetailCopySuper():ModelEcCultDetailCopySuper {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "EcCultDetailCopy";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.EcCultCopy): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.EcCultDetailCopy.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.ecccId = this.Parent;
                } else {
                    x.ecccId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.EcCultDetailCopy {
            var self = this;
            return <Entities.EcCultDetailCopy>self.$scope.modelEcCultDetailCopySuper.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.EcCultDetailCopy, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelEcCultCopySuper.controller.isEntityLocked(<Entities.EcCultCopy>cur.ecccId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingEcCultDetailCopySuper_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingEcCultDetailCopySuper";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingEcCultDetailCopySuper_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.EcCultDetailCopy[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.EcCultDetailCopy[]) => void> = [];


        public getMergedItems(func: (items: Entities.EcCultDetailCopy[]) => void, bContinuousCaller:boolean=true, parent?: Entities.EcCultCopy, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.EcCultDetailCopy[]):void {
                var mergedEntities = <Entities.EcCultDetailCopy[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.EcCultDetailCopy>e.a).ecccId)).
                        map(e => <Entities.EcCultDetailCopy>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingEcCultDetailCopySuper";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['ecccId_ecccId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.EcCultDetailCopy[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.EcCultDetailCopy):boolean {
            if (this.Parent === undefined || x.ecccId === undefined)
                return false;
            return x.ecccId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.EcCultCopy {
            var self = this;
            return self.ecccId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelEcCultCopySuper.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get ecccId():Entities.EcCultCopy {
            var self = this;
            return <Entities.EcCultCopy>self.$scope.modelEcCultCopySuper.selectedEntities[0];
        }









        public constructEntity(): Entities.EcCultDetailCopy {
            var self = this;
            var ret = new Entities.EcCultDetailCopy(
                /*ecdcId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*orderingNumber:number*/ null,
                /*agreesDeclar:number*/ null,
                /*comparisonOper:number*/ null,
                /*probabThres:number*/ null,
                /*decisionLight:number*/ null,
                /*rowVersion:number*/ null,
                /*agreesDeclar2:number*/ null,
                /*comparisonOper2:number*/ null,
                /*probabThres2:number*/ null,
                /*probabThresSum:number*/ null,
                /*comparisonOper3:number*/ null,
                /*ecccId:Entities.EcCultCopy*/ null);
            this.getMergedItems(items => {
                ret.orderingNumber = items.maxBy(i => i.orderingNumber, 0) + 1;
            }, false);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.EcCultDetailCopy = <Entities.EcCultDetailCopy>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.ecccId = self.ecccId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.EcCultDetailCopy): Entities.EcCultDetailCopy {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.EcCultDetailCopy, calledByParent: boolean= false): Entities.EcCultDetailCopy {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.EcCultDetailCopy.Create();
            ret.updateInstance(src);
            ret.ecdcId = tmpId;
            if (!calledByParent) {
                this.getMergedItems(items => {
                    ret.orderingNumber = items.maxBy(i => i.orderingNumber, 0) + 1;
                }, false);
            }
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.EcCultCopy, newParent:Entities.EcCultCopy) {
        
            this.model.totalItems = 0;

            this.getMergedItems(ecCultDetailCopyList => {
                ecCultDetailCopyList.forEach(ecCultDetailCopy => {
                    var ecCultDetailCopyCloned = this.cloneEntity_internal(ecCultDetailCopy, true);
                    ecCultDetailCopyCloned.ecccId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(ecCultCopy: Entities.EcCultCopy) {
        

            var self = this;
            var toBeDeleted:Array<Entities.EcCultDetailCopy> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var ecCultDetailCopy = <Entities.EcCultDetailCopy>change.a
                if (ecCultCopy.getKey() === ecCultDetailCopy.ecccId.getKey()) {
                    toBeDeleted.push(ecCultDetailCopy);
                }
            }

            _.each(toBeDeleted, (x:Entities.EcCultDetailCopy) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
            });

        }

        public deleteAllEntitiesUnderParent(ecCultCopy: Entities.EcCultCopy, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteEcCultDetailCopyList(ecCultDetailCopyList: Entities.EcCultDetailCopy[]) {
                if (ecCultDetailCopyList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = ecCultDetailCopyList[0];
                    var tail = ecCultDetailCopyList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteEcCultDetailCopyList(tail);
                    });
                }
            }


            this.getMergedItems(ecCultDetailCopyList => {
                deleteEcCultDetailCopyList(ecCultDetailCopyList);
            }, false, ecCultCopy);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.EcCultDetailCopy, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            return true;
        }
        public _deleteIsDisabled(ecCultDetailCopy:Entities.EcCultDetailCopy):boolean  {
            return true;
        }

    }

}
