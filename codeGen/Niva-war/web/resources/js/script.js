

function openNewTab(appName, page) {
    var origin = window.location.origin;
    var newUrl = origin + '/' + appName + '/' + page;
        window.open('','_new').location.href=newUrl;
}

function initActions () 
{
    $(document).keydown (navigateWithArrows);
    $(document).focusin(globalFocusIn);
    $(document).click(globalClick);
    $(document).dblclick(globalDoubleClick);
    
}

function navigateAfterReacId(curElement, navigateToGross, navigateToNet){
    if(navigateToGross) {
        //$(document).find('[id$=\'ReTranHeaderReTranHeader_0_0_ReTranHeader_ReTranDetail_0_grossValue\']').filter('span:first').children('input:first').focus();
        $(document.getElementById(curElement.source)).closest('tr.ui-widget-content').find('[id$=\'grossValue_input\']').focus();
    }
    if(navigateToNet){
        $(document.getElementById(curElement.source)).closest('tr.ui-widget-content').find('[id$=\'netValue_input\']').focus();
        //$(document).find('[id$=\'ReTranHeaderReTranHeader_0_0_ReTranHeader_ReTranDetail_0_netValue\']').filter('span:first').children('input:first').focus();
    }
}


function focusCursor() {}


globalClick = function(event) {
    var element = event.target || event.srcElement; // srcElement in Internet Explorer, target in other browsers
    
    $(element).parents('.ui-chkbox').filter('.withinTable').find('input').focus();
    if ($(element).hasClass('ui-commandlink'))
        $(element).focus();
    if ($(element).hasClass('OutputTextOuterDivWithinTable'))
        $(element).children().click();
/*      
    var focusedElmentId = $(element).attr('id');
    if (typeof focusedElmentId == "undefined")
        focusedElmentId="??";
    var singleId = focusedElmentId.split(/[:]+/).slice(-1)[0];
    var tagName = $(element).prop("tagName");
    console.log('click in: '+singleId + ' '+tagName);
    */
};

globalDoubleClick = function(event) {
    var element = event.target || event.srcElement; // srcElement in Internet Explorer, target in other browsers
    if ((typeof $(element).parents('.ui-dialog').attr('id')) != "undefined") {
        // we are within a LOV
        $(element).parents('.ui-dialog').find('[id$=selectBtn]').click();
        return;
    }
    
};

String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};


onFocus = function() {
        //$('div.ui-dt-c').css('background', 'none');
        //$(this).closest('div.ui-dt-c').css('background', 'rgb(87, 235, 255)');

        if (!this.id.endsWith(':filter')) { // if not filter
            var row = $(this).closest('tr.ui-widget-content'); // find row
            if (!$(row).hasClass('ui-state-highlight')) {  // if not selected
                //$(this).parents('td').first().addClass('selected-table-cell');
                                var allIds = this.id.split(/[:]+/);
                                var tableWidgetVar;
                for (var i=0; i<allIds.length; i++) {
                    if (allIds[i].endsWith('_tableId')){
                        tableWidgetVar = window[allIds[i]+'WdgtVar'];
                        tableWidgetVar.unselectAllRows();
                        tableWidgetVar.selectRow(row);
                        break;
                    }
                }
            }
        }
};

tdFocusIn = function() {
//      console.log('focus in:'+$(this).find('[id]').attr('id'));
        $(this).parents('.KeyboardNavigableTable').find('.selected-table-cell').removeClass('selected-table-cell');
        $(this).addClass('selected-table-cell');
};

/*  
tdFocusOut = function() {
        $(this).removeClass('selected-table-cell');
    }
*/
    
function initActionsAjaxPartial() {
    $('.KeyboardNavigableTable').find('input').off('focus', onFocus); //removed old handlers
    $('.KeyboardNavigableTable').find('input').focus(onFocus);        // add new one
    $('.KeyboardNavigableTable').find('select').off('focus', onFocus);
    $('.KeyboardNavigableTable').find('select').focus(onFocus);
    $('.KeyboardNavigableTable').find('a').off('focus', onFocus);
    $('.KeyboardNavigableTable').find('a').focus(onFocus);
    
    $('.KeyboardNavigableTable').find('.ui-dt-c').parent().off('focusin', tdFocusIn);
    $('.KeyboardNavigableTable').find('.ui-dt-c').parent().on('focusin', tdFocusIn);
    restoreFocusedElement();
}


function toggleFilters(dataTableID) {
    $("[id$='" + dataTableID + "']").find("input.ui-column-filter").each(
        function (idx) {
            $(this).toggle();
        }
    );
}


/* 
Keyboard navigation functions
*/
var ARROWLEFT_KEY_CODE =  37;
var ARROWUP_KEY_CODE   =  38;
var ARROWRIGHT_KEY_CODE = 39;
var ARROWDOWN_KEY_CODE =  40;
var ENTER_KEY_CODE = 13;
var TAB_KEY_CODE    = 9;
var ESC_KEY_CODE = 27;
var SPACE_KEY_CODE = 32;
var PGUP_KEY_CODE = 33;
var PGDN_KEY_CODE = 34;
var S_KEY_CODE  = 83;
var F1_KEY_CODE = 112;
var F2_KEY_CODE = 113;
var F08_KEY_CODE = 119;
var F09_KEY_CODE = 120;
var F10_KEY_CODE = 121;
var F12_KEY_CODE = 123;
var BACKSPACE_KEY_CODE = 8;
var ARROWKEY_CODES = [SPACE_KEY_CODE, ARROWLEFT_KEY_CODE, ARROWRIGHT_KEY_CODE, ARROWUP_KEY_CODE, ARROWDOWN_KEY_CODE, ENTER_KEY_CODE, ESC_KEY_CODE, PGUP_KEY_CODE, PGDN_KEY_CODE, S_KEY_CODE, TAB_KEY_CODE, F1_KEY_CODE, F2_KEY_CODE, F08_KEY_CODE, F12_KEY_CODE, BACKSPACE_KEY_CODE, F10_KEY_CODE, F09_KEY_CODE];


$.fn.getCursorPosition = function() {
    var el = $(this).get(0);
    var pos = 0;
    if('selectionStart' in el) {
        pos = el.selectionStart;
    } else if('selection' in document) {
        el.focus();
        var Sel = document.selection.createRange();
        var SelLength = document.selection.createRange().text.length;
        Sel.moveStart('character', -el.value.length);
        pos = Sel.text.length - SelLength;
    }
    return pos;
};


$.fn.selectRange = function(start, end) {
    return this.each(function() {
        if (this.setSelectionRange) {
            this.focus();
            this.setSelectionRange(start, end);
        } else if (this.createTextRange) {
            var range = this.createTextRange();
            range.collapse(true);
            range.moveEnd('character', end);
            range.moveStart('character', start);
            range.select();
        }
    });
};

$.fn.exists = function () {
    return this.length !== 0;
};


setFocus = function(tableCell) {
    var input = tableCell.find('.ui-inputfield');
    var endSelection = 0;
    var text = $(input).attr('value');
    if (text && !tableCell.find('.closedLovItem').exists())
        endSelection = text.length;
    $(input).selectRange(0,endSelection);
    var inputOrButton = tableCell.find('input, button, .closedLovItem, select').last();
    $(inputOrButton).focus();
    
    tableCell.find('.ui-selectonemenu-trigger').trigger('click');
    tableCell.parents('.KeyboardNavigableTable').find('.selected-table-cell').removeClass('selected-table-cell');
    if (tableCell.parents('.KeyboardNavigableTable').exists())
        tableCell.addClass('selected-table-cell');
};



function focusNewRowTable(tableId) {
    var cellToMove = $('[id$='+tableId+']').find('tr.ui-widget-content').filter('.ui-state-highlight').children().first();
    while(cellToMove.exists() &&  (cellToMove.find('span.ui-icon-closethick').exists() || cellToMove.find('.ReadOnlyControl').exists())) {
        cellToMove = cellToMove.next('td');
    }
    if (cellToMove.exists()) {
        setFocus(cellToMove);
    } 
}

function navigateWithArrowsLOV(event) {
    var element = event.target || event.srcElement; // srcElement in Internet Explorer, target in other browsers
    var lovId = $(element).parents('.ui-dialog').attr('id');
    var widgetVar = window[lovId.substring(0,lovId.length-6)+"wgVar"];
    
    var selectedRow = $(element).parents('.ui-dialog').find('tr.ui-widget-content').filter('.ui-state-highlight');
    var nextRow;
    if (event.keyCode==ARROWDOWN_KEY_CODE) {
        nextRow = selectedRow.next('tr');
        if (nextRow.exists()) {
            widgetVar.unselectAllRows();
            widgetVar.selectRow(nextRow);
        } else {
            $(element).parents('.ui-dialog').find('.ui-paginator-next').trigger('click');
        }
    } else if (event.keyCode==ARROWUP_KEY_CODE) {
        nextRow = selectedRow.prev('tr');
        if (nextRow.exists()) {
            widgetVar.unselectAllRows();
            widgetVar.selectRow(nextRow);
        } else {
            $(element).parents('.ui-dialog').find('.ui-paginator-prev').trigger('click');
        }
    } else if (event.keyCode==PGDN_KEY_CODE) {
        $(element).parents('.ui-dialog').find('.ui-paginator-next').trigger('click');
    } else if (event.keyCode==PGUP_KEY_CODE) {
        $(element).parents('.ui-dialog').find('.ui-paginator-prev').trigger('click');
    }
    
    if (event.keyCode==F12_KEY_CODE) {
        //console.log('Triggering close click on LOV');
        $(element).parents('.ui-dialog').find('[id$=selectBtn]').click();
        return false;
    }
    return true;
}


function onDialogOpen(dlgWgVar) {
    focusedElements.push(""); //add new item for this level
    openDialogs.push(dlgWgVar);
}
var focusedElements=[""];
var openDialogs=[];

function onDialogClose() {
    var dlgClosed = openDialogs.pop();
    var elementId = focusedElements.pop();  //remove this level item
    restoreFocusedElement();
}

function getTopDialog() {
    return openDialogs.slice(-1)[0];
}

function restoreFocusedElement() {

    var elemId = focusedElements.slice(-1)[0];
    if(elemId !== "") {
        var element = $(document).find("[id$='"+elemId+"']");
        //console.log('restoreFocusedElement: '+elemId+' tag is: ' + element.prop("tagName"));
        element.focus();
        element.filter('input').not(':checkbox').not(':file').selectRange(0,0);
    }
    else
        console.log('restoreFocusedElement() called but last element id was empty');
}

function restoreFocusAfterReacIdLovClose(navigateToGross, navigateToNet){

    var dlgClosed = openDialogs.pop();
    var elementId = focusedElements.pop();  //remove this level item

    var elemId = focusedElements.slice(-1)[0];
    if(elemId !== "") {
        var element = $(document).find("[id$='"+elemId+"']");

        if(navigateToGross) {
            element.closest('tr.ui-widget-content').find('[id$=\'grossValue_input\']').focus();
        }
        if(navigateToNet){
            element.closest('tr.ui-widget-content').find('[id$=\'netValue_input\']').focus();
        }

        //console.log('restoreFocusedElement: '+elemId+' tag is: ' + element.prop("tagName"));
        //element.focus();
        //element.filter('input').not(':checkbox').not(':file').selectRange(0,0);
    }
    else
        console.log('restoreFocusedElement() called but last element id was empty');
}



globalFocusIn = function(event) {
    var element = event.target || event.srcElement; // srcElement in Internet Explorer, target in other browsers
    var focusedElmentId = $(element).attr('id');
    if (typeof focusedElmentId != "undefined") {
        //replace existing item, element is not the save button
        //console.log('last element focused: ' + focusedElmentId + ' visible' + $(element).is(":visible"));
        if ($(element).is(":visible") && !focusedElmentId.endsWith("commitBtn") && !$(element).is('a.openLovItem')) {
            focusedElements.pop();
            focusedElements.push(focusedElmentId);
        }
    } else {
        var tagName = $(element).prop("tagName");
        console.log('warning focused element with no id tag is: '+tagName);
    }
};



function onF10(event) {
    var element = event.target || event.srcElement; // srcElement in Internet Explorer, target in other browsers
    var formElements = $(element).parents('form').find('*');
    if (event.shiftKey)
        formElements = $(formElements.get().reverse());
    var curIndexWithinForm = formElements.index($(element));
    var nextTables = formElements.filter('*:gt('+curIndexWithinForm+')').filter('div.ui-datatable-scrollable-body:visible');
    var nextTable = nextTables.first();
    if (event.shiftKey)
        nextTable = nextTables.slice(1).first();
    if (nextTable.exists()) {
        var addBtn = nextTable.parents('.KeyboardNavigableTable').find('[id$=add_id]');
        var tableRows = nextTable.children('table').children('tbody').children('tr').not('.ui-datatable-empty-message');
        var firstTableRow = tableRows.filter('.ui-state-highlight').first();
        if (!firstTableRow.exists()) 
            firstTableRow = tableRows.first();
        if (!firstTableRow.exists()) {
            addBtn.focus();
            addBtn.trigger('click');
        } else {
            var cellToMove = firstTableRow.children().slice(1).first();
            while(cellToMove.exists() &&  cellIsReadOnlyOrDisabled(cellToMove)) {
                cellToMove = cellToMove.next('td');
            }
            if (cellToMove.exists() && cellToMove.find('*').not('div.ui-dt-c').exists()) {
                if ($(element).parents().first().hasClass('dateBoxWithinTable')) {
                    // close callendar div
                    $("div.ui-datepicker").hide();
                }
                setFocus(cellToMove);
            } 
        }
    } else {
        if (event.shiftKey) {
            focusFirstElement($(element).parents('form').attr('id'));
        }
    }
    
}

/*
onF7 (next tab)
$(element).parents('div.ui-tabs').first().find('li.ui-state-active').first().next()
*/


function navigateWithinTable(event) {
    var nextRow;
    var element = event.target || event.srcElement; // srcElement in Internet Explorer, target in other browsers


    var singleEnterPressed = (event.keyCode==ENTER_KEY_CODE || event.keyCode==TAB_KEY_CODE) && !event.shiftKey && !event.ctrlKey;
    var shiftEnterPressed = (event.keyCode==ENTER_KEY_CODE || event.keyCode==TAB_KEY_CODE) && event.shiftKey && !event.ctrlKey;
    var ctrlEnterPressed = event.keyCode==ENTER_KEY_CODE && !event.shiftKey && event.ctrlKey;
    var ctrlShiftEnterPressed = event.keyCode==ENTER_KEY_CODE && event.shiftKey && event.ctrlKey;
    var singleF10Pressed = (event.keyCode==F10_KEY_CODE ) && !event.shiftKey && !event.ctrlKey;
    var shiftF10Pressed = (event.keyCode==F10_KEY_CODE ) && event.shiftKey && !event.ctrlKey;

    
    var addButton = $(element).parents('.KeyboardNavigableTable').find('[id$=add_id]');
    var cloneButton = $(element).parents('.KeyboardNavigableTable').find('tr.ui-widget-content').filter('.ui-state-highlight').find('[id$=clone_id]');
    /*
    if (event.keyCode==F10_KEY_CODE) {
        var addButtons = $(element).parents('form').find('[id$=add_id]');
        var curIndex = addButtons.index(addButton);
        var nextIndex = singleF10Pressed ? curIndex+1 : curIndex-1;
        var nextAddButton = addButtons.slice(nextIndex,nextIndex+1);
        nextAddButton.focus();
        nextAddButton.trigger('click');
        return false;
    }*/
    
    if (singleF10Pressed || shiftF10Pressed) {
        onF10(event);
        return false;
    }
    
    
    //check if element is within a table, if not return

    //if it is filter do nothing
    if ($(element).attr('id').endsWith(':filter'))
        return true;
    

    // On Ctrl+Enter create new table row
    if (ctrlEnterPressed || (event.keyCode == F09_KEY_CODE)) {
        addButton.focus();
        addButton.trigger('click');
        return false;
    }
    // On Shift + Ctrl+Enter clone row
    if (ctrlShiftEnterPressed) {
        cloneButton.focus();
        cloneButton.trigger('click');
        return false;
    }
    
    var currentCell = $(element).closest('td');
    if ($(element).hasClass('lovItem') ) 
         currentCell =  $(element).closest('tbody').closest('td');//$($(element).closest('tbody')[0]).closest('td');
    var curCellIndex = currentCell.index();
    var currentRow = currentCell.closest('tr');
    
    var hasCursor = ($(element).hasClass('textBoxWithinTable') || $(element).hasClass('dateBoxWithinTable') || $(element).parents().first().hasClass('dateBoxWithinTable') || $(element).parent().hasClass('numberBoxWithinTable'));
    var cellToMove=$([]);
    

    if (event.keyCode==ARROWRIGHT_KEY_CODE || singleEnterPressed) {
        if ( hasCursor && (event.keyCode==ARROWRIGHT_KEY_CODE) &&              
             ($(element).getCursorPosition() != element.value.length)) 
            return true; //caret  is not at the end so let the default behavior to occure.
        
        cellToMove = currentCell.next('td');
        while(cellToMove.exists() &&  cellIsReadOnlyOrDisabled(cellToMove)) {
            cellToMove = cellToMove.next('td');
        }
    } else if (event.keyCode==ARROWLEFT_KEY_CODE || shiftEnterPressed) {
        if ( hasCursor && (event.keyCode==ARROWLEFT_KEY_CODE) && ($(element).getCursorPosition() !== 0)) 
            return true; //caret  is not at the start so let the default behavior to occure.

        cellToMove = currentCell.prev('td');
        while(cellToMove.exists() &&  cellIsReadOnlyOrDisabled(cellToMove)) {
            cellToMove = cellToMove.prev('td');
        }
        //cellToMove is the column contaning the delete and duplicate button
        if (cellToMove.find('span.ui-icon-closethick').exists())
            return false;
        
        
    } else if (event.keyCode==ARROWDOWN_KEY_CODE) {
        nextRow = currentRow.next('tr');
        cellToMove = nextRow.children('td').slice(curCellIndex,curCellIndex+1);
    } else if (event.keyCode==ARROWUP_KEY_CODE) {
        nextRow = currentRow.prev('tr');
        cellToMove = nextRow.children('td').slice(curCellIndex,curCellIndex+1);
    }
    if (cellToMove.exists() && cellToMove.find('*').not('div.ui-dt-c').exists()) {
        if ($(element).parents().first().hasClass('dateBoxWithinTable')) {
            // close callendar div
            $("div.ui-datepicker").hide();
        }
        setFocus(cellToMove);
    } else {
        if (event.keyCode==ARROWRIGHT_KEY_CODE || singleEnterPressed) {
            var groupId = $(element).parents('div.KeyboardNavigableTable').first().attr('id').split(':').slice(-1)[0].replace('_tableId','');
            var itemRegion = $(element).parents('form').find('.childof'+groupId);
            if (itemRegion.exists()) {
                focusFirstChild(itemRegion);
            }
        }
    }
    //
    
    return false;

}

function focusFirstChild(jqObject) {
    var elementToFocus = jqObject.find('input.ui-inputfield:not([readonly]), select').first();
    elementToFocus.focus();
    var txtElement = elementToFocus.filter('input:not([readonly])').not(':checkbox');
    var text = $(txtElement).attr('value');
    var endSelection = 0;
    if (text)
        endSelection = text.length;
    $(txtElement).selectRange(0,endSelection);
}

function focusFirstElement(frmId) {
    var form = $('form#'+frmId);
    focusFirstChild(form);
}

function navigateWithinForm(event) {
    var singleEnterPressed = (event.keyCode==ENTER_KEY_CODE || event.keyCode==TAB_KEY_CODE) && !event.shiftKey && !event.ctrlKey;
    var shiftEnterPressed = (event.keyCode==ENTER_KEY_CODE || event.keyCode==TAB_KEY_CODE) && event.shiftKey && !event.ctrlKey;
    var singleF10Pressed = (event.keyCode==F10_KEY_CODE ) && !event.shiftKey && !event.ctrlKey;
    var shiftF10Pressed = (event.keyCode==F10_KEY_CODE ) && event.shiftKey && !event.ctrlKey;

	
    var element = event.target || event.srcElement; // srcElement in Internet Explorer, target in other browsers
	
	
    if (event.keyCode == F09_KEY_CODE) {
		var newButton = $(element).parents('form').find('[id$=newBtnFrm]');
        newButton.focus();
        newButton.trigger('click');
        return false;
    }
	
	
    if (singleF10Pressed || shiftF10Pressed) {
        onF10(event);
        return false;
    }
    
    if (singleEnterPressed || shiftEnterPressed) {

        //      var formElements = $(element).closest('.FormRegion').find('input.ui-inputfield:not([readonly]), :checkbox, select, button');
        // primefaces check box can not get focus
        
        // see also http://stackoverflow.com/questions/4328381/jquery-set-focus-on-the-first-enabled-input-or-select-or-textarea-on-the-page
        //  $(element).closest('form').find(':input:enabled:visible:first').focus();
        var formElements = $(element).closest('form').find('input.ui-inputfield:enabled:not([readonly]), select, button');
        
        var curIndex = formElements.index(element);
        var nextIndex = singleEnterPressed ? curIndex+1 : curIndex-1;
        var nextElement = formElements.slice(nextIndex,nextIndex+1);
        if (nextElement.parents('.FormRegion').exists()) {
            nextElement.focus();
            var txtElement = nextElement.filter('input:not([readonly])').not(':checkbox');
            var text = $(txtElement).attr('value');
            var endSelection = 0;
            if (text)
                endSelection = text.length;
            $(txtElement).selectRange(0,endSelection);
        } else {
            if (shiftEnterPressed) {
                var frmRegion = $(element).parents('.FormRegion');
                var classes = $(element).parents('.FormRegion').first().attr('class').split(/\s+/);
                for (var i=0; i<classes.length; i++) {
                    if (classes[i].substring(0,7) === 'childof'){
                        var tableId = classes[i].replace('childof','')+'_tableId';
                        var table = $(element).parents('form').find("[id$='"+tableId+"']").find('div.ui-datatable-scrollable-body:visible');
    
                        var tableRows = table.children('table').children('tbody').children('tr').not('.ui-datatable-empty-message');
                        var firstTableRow = tableRows.filter('.ui-state-highlight').first();
                        if (firstTableRow.exists()) {
                            var cellToMove = firstTableRow.children().slice(-2).first();
                            while(cellToMove.exists() &&  cellIsReadOnlyOrDisabled(cellToMove)) {
                                cellToMove = cellToMove.prev('td');
                            }
                            if (cellToMove.exists() && cellToMove.find('*').not('div.ui-dt-c').exists()) {
                                if ($(element).parents().first().hasClass('dateBoxWithinTable')) {
                                    // close callendar div
                                    $("div.ui-datepicker").hide();
                                }
                                setFocus(cellToMove);
                            } 
                        }
                        break;
                    }
                }
            }
        }
        return false;
    }
    return true;
}


function navigateWithArrows(event) {
    var lovItemText;
    if (ARROWKEY_CODES.indexOf(event.keyCode) <0 )
        return true;
		
    //element is the Html element that received the key down event
    var element = event.target || event.srcElement; // srcElement in Internet Explorer, target in other browsers

	if ($(element).hasClass('ui-inputtextarea'))
		return true;
	
    if (event.keyCode == F1_KEY_CODE) {
		helpPageDlg.show();
		$(document).find('[id$=HelpPageFrmIdOkId]').focus();
		return false;
	}
    
    if (event.keyCode == BACKSPACE_KEY_CODE) 
        return $(element).is('input');
    
    if (event.keyCode==SPACE_KEY_CODE) {
        if ($(element).hasClass('closedLovItem')) {
            $(element).trigger('click');
            return false;
        }
        lovItemText = $(element).closest('input.closedLovItem').closest('tbody').find('a.closedLovItem');
        if (lovItemText.exists()) {
            lovItemText.click();
            return false;
        }
        return true;
    }
    
    if (event.keyCode==F12_KEY_CODE) {
        if ($(element).hasClass('ui-commandlink')) {
            $(element).trigger('click');
            return false;
        }
        lovItemText = $(element).closest('input.lovItem').closest('tbody').find('a.lovItem');
        if (lovItemText.exists()) {
            lovItemText.click();
            return false;
        }
    }
    if (event.keyCode==ESC_KEY_CODE) {
        var topMostDialog = $(document).
						   find('.ui-dialog').
						   not('.ui-overlay-hidden').
						   sort(function(a,b){ return parseInt($(a).css('z-index')) - parseInt($(b).css('z-index'))}).
						   slice(-1);
        if (topMostDialog.exists()) 
            cancelButton = topMostDialog.find('.ui-dialog-titlebar-close');
        else 
            cancelButton = $(document).find("[id$='cancelBtn']").first();
        cancelButton.focus();
        cancelButton.click();
        return true;
    }
    
    if (event.keyCode==S_KEY_CODE) {
        if (event.ctrlKey) {
            var saveButton = $(element).parents('form').last().find("[id$='commitBtn']").first();
            saveButton.focus();
            saveButton.trigger('click');
            return false;
        } else {
            return true;
        }
    }
	
	if (event.keyCode == F08_KEY_CODE) {
		var saveButton = $(element).parents('form').last().find("[id$='commitBtn']").first();
		saveButton.focus();
		saveButton.trigger('click');
	}
    
    if ($(element).is('select')) {
        if ((event.keyCode==ARROWDOWN_KEY_CODE) ||
            (event.keyCode==ARROWUP_KEY_CODE)   ||
            (event.keyCode==ARROWLEFT_KEY_CODE)   ||
            (event.keyCode==ARROWRIGHT_KEY_CODE))
            return true;
    }
    
    
    if ($(element).closest('.withinTable').exists())
        return navigateWithinTable(event);
        
    if ($(element).closest('.withinForm').exists())
        return navigateWithinForm(event);

    if ((typeof $(element).parents('.ui-dialog').attr('id')) != "undefined")
        return navigateWithArrowsLOV(event);

    if (event.keyCode==F12_KEY_CODE) {
		return false;
	}
        
    return true;
}


function cellIsReadOnlyOrDisabled(cell) {
    return cell.find('.ReadOnlyControl').exists() || cell.find('input:disabled').exists() || cell.find('select:disabled').exists() || cell.find('button:disabled').exists();
}


function onUploadDialog() {
	var input = $('[id$=uploadDialog]').find('label.ui-button');
	//input.focus();
	input.trigger('click');
}


$(document).ready(function () {
    $("input.ui-column-filter").each(
        function (idx) {
            $(this).toggle();
        }
    );
});
