var validNavigation = false;

function setValidNavigation(booleanValue) {

    validNavigation = booleanValue;
}

function wireUpEvents() {

  function goodbye(e) {
    if (!validNavigation) {
        destroySession();
    }
  }
  window.onbeforeunload=goodbye;
  //window.onunload = destroySession;

  // Attach the event keypress to exclude the F5 refresh
  $('document').bind('keypress', function(e) {
    if (e.keyCode == 116){
      validNavigation = true;
    }
  });

  // Attach the event click for all links in the page
  $("a").bind("click", function() {
    validNavigation = true;
  });

  // Attach the event submit for all forms in the page
  $("form").bind("submit", function() {
    validNavigation = true;
  });

  // Attach the event click for all inputs in the page
  $("input[type=submit]").bind("click", function() {
    validNavigation = true;
  });

}

// Wire up the events as soon as the DOM tree is ready
$(document).ready(function() {
  wireUpEvents();
});