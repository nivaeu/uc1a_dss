/**
 * PrimeFaces DataTable Widget (*** Extended Version ***)
 * PF version: 3.4.2
 * 
 * Fix: An ajax row click event does not fire (in other words it fires silently) if row clicked is already selected
 * override by g_dimitriadis
 */
PrimeFaces.widget.DataTable = PrimeFaces.widget.DataTable.extend({
    
    onRowClick: function(event, rowElement, silent) {
        //Check if rowclick triggered this event not a clickable element in row content
        if($(event.target).is('.ui-dt-c,td,span')) {
            var row = $(rowElement),
            selected = row.hasClass('ui-state-highlight'),
            metaKey = event.metaKey||event.ctrlKey;

            //unselect a selected row if metakey is on
            if(selected && metaKey) {
                this.unselectRow(row, silent);
            }
            else {
                //unselect previous selection if this is single selection or multiple one with no keys
                if(this.isSingleSelection() || (this.isMultipleSelection() && event && !metaKey && !event.shiftKey)) {
                    this.unselectAllRows();
                }
                
                //range selection with shift key
                if(this.isMultipleSelection() && event && event.shiftKey) {                    
                    this.selectRowsInRange(row);
                }
                //select current row
                else {
                    this.originRowIndex = row.index();
                    this.cursorIndex = null;

		    //select row silently (does not fire rowSelect event) if row is already selected
                    if (!selected) { 
	               this.selectRow(row, silent);
		    }
		    else {
		       this.selectRow(row, true);
		    }
                }
            } 

            PrimeFaces.clearSelection();
        }
    },
    bindRowHover: function() {
        $(document).off('mouseover.datatable mouseout.datatable', this.rowSelector)
                    .on('mouseover.datatable', this.rowSelector, null, function() {
                        var element = $(this);

                    })
                    .on('mouseout.datatable', this.rowSelector, null, function() {
                        var element = $(this);

                    });
    }
    
});


/**
 * PrimeFaces PanelMenu Widget
 */
PrimeFaces.widget.PanelMenu = PrimeFaces.widget.PanelMenu.extend({

    saveState: function() {
        var expandedNodeIds = this.expandedNodes.join(',');
        
        //PrimeFaces.setCookie(this.stateKey, expandedNodeIds);
		$.cookie(this.stateKey, expandedNodeIds, {path: '/'})
    },
    
    
    clearState: function() {
//        PrimeFaces.setCookie(this.stateKey, null);
		$.cookie(this.stateKey, null, {path: '/'})
    }

});
