var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/ProducersBase.ts" />
/// <reference path="../Controllers/Producers.ts" />
var Controllers;
(function (Controllers) {
    var Producers;
    (function (Producers) {
        var PageModelBase = (function (_super) {
            __extends(PageModelBase, _super);
            function PageModelBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(PageModelBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return PageModelBase;
        })(Controllers.AbstractPageModel);
        Producers.PageModelBase = PageModelBase;
        var PageControllerBase = (function (_super) {
            __extends(PageControllerBase, _super);
            function PageControllerBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelGroupControllers = undefined;
                this._allGroupControllers = undefined;
                var self = this;
                model.controller = self;
                $scope.pageModel = self.model;
                $scope._saveIsDisabled =
                    function () {
                        return self._saveIsDisabled();
                    };
                $scope._cancelIsDisabled =
                    function () {
                        return self._cancelIsDisabled();
                    };
                $scope.onSaveBtnAction = function () {
                    self.onSaveBtnAction(function (response) { self.onSuccesfullSaveFromButton(response); });
                };
                $scope.onNewBtnAction = function () {
                    self.onNewBtnAction();
                };
                $scope.onDeleteBtnAction = function () {
                    self.onDeleteBtnAction();
                };
                $timeout(function () {
                    $('#Producers').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                    $('#NpMainContent').scrollTop(0);
                }, 0);
            }
            Object.defineProperty(PageControllerBase.prototype, "ControllerClassName", {
                get: function () {
                    return "PageController";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "HtmlDivId", {
                get: function () {
                    return "Producers";
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._getPageTitle = function () {
                return "Producers";
            };
            Object.defineProperty(PageControllerBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._saveIsDisabled = function () {
                var self = this;
                if (!self.$scope.globals.hasPrivilege("Niva_Producers_W"))
                    return true; // 
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            PageControllerBase.prototype._cancelIsDisabled = function () {
                var self = this;
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            Object.defineProperty(PageControllerBase.prototype, "pageModel", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.update = function () {
                var self = this;
                self.model.modelGrpProducers.controller.updateUI();
            };
            PageControllerBase.prototype.refreshVisibleEntities = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpProducers.controller.refreshVisibleEntities(newEntitiesIds);
            };
            PageControllerBase.prototype.refreshGroups = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpProducers.controller.refreshVisibleEntities(newEntitiesIds);
            };
            Object.defineProperty(PageControllerBase.prototype, "FirstLevelGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._firstLevelGroupControllers === undefined) {
                        self._firstLevelGroupControllers = [
                            self.model.modelGrpProducers.controller
                        ];
                    }
                    return this._firstLevelGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "AllGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._allGroupControllers === undefined) {
                        self._allGroupControllers = [
                            self.model.modelGrpProducers.controller
                        ];
                    }
                    return this._allGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.getPageChanges = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var pageChanges = [];
                if (bForDelete) {
                    pageChanges = pageChanges.concat(self.model.modelGrpProducers.controller.getChangesToCommitForDeletion());
                    pageChanges = pageChanges.concat(self.model.modelGrpProducers.controller.getDeleteChangesToCommit());
                }
                else {
                    pageChanges = pageChanges.concat(self.model.modelGrpProducers.controller.getChangesToCommit());
                }
                var hasProducers = pageChanges.some(function (change) { return change.entityName === "Producers"; });
                if (!hasProducers) {
                    var validateEntity = new Controllers.ChangeToCommit(Controllers.ChangeStatus.UPDATE, 0, "Producers", self.model.modelGrpProducers.controller.Current);
                    pageChanges = pageChanges.concat(validateEntity);
                }
                return pageChanges;
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbUrl = function () {
                return "/Niva/rest/MainService/synchronizeChangesWithDb_Producers";
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbData = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var paramData = {};
                paramData.data = self.getPageChanges(bForDelete);
                return paramData;
            };
            PageControllerBase.prototype.onSuccesfullSaveFromButton = function (response) {
                var self = this;
                _super.prototype.onSuccesfullSaveFromButton.call(this, response);
                if (!isVoid(response.warningMessages)) {
                    NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
                }
                if (!isVoid(response.infoMessages)) {
                    NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
                }
                self.model.modelGrpProducers.controller.cleanUpAfterSave();
                self.$scope.globals.isCurrentTransactionDirty = false;
                self.refreshGroups(response.newEntitiesIds);
            };
            PageControllerBase.prototype.onFailuredSave = function (data, status) {
                var self = this;
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                var errors = Messages.dynamicMessage(data);
                NpTypes.AlertMessage.addDanger(self.model, errors);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
            };
            PageControllerBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            PageControllerBase.prototype.onSaveBtnAction = function (onSuccess) {
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                var errors = self.validatePage();
                if (errors.length > 0) {
                    var errMessage = errors.join('<br>');
                    NpTypes.AlertMessage.addDanger(self.model, errMessage);
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
                    return;
                }
                if (self.$scope.globals.isCurrentTransactionDirty === false) {
                    var jqSaveBtn = self.SaveBtnJQueryHandler;
                    self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                    return;
                }
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData();
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    if (!self.shownAsDialog()) {
                        var breadcrumbStepModel = self.$scope.getCurrentPageModel();
                        var newProducersId = response.newEntitiesIds.firstOrNull(function (x) { return x.entityName === 'Producers'; });
                        if (!isVoid(newProducersId)) {
                            if (!isVoid(breadcrumbStepModel)) {
                                breadcrumbStepModel.selectedEntity = Entities.Producers.CreateById(newProducersId.databaseId);
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        else {
                            if (!isVoid(breadcrumbStepModel) && !(isVoid(breadcrumbStepModel.selectedEntity))) {
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        self.$scope.setCurrentPageModel(breadcrumbStepModel);
                    }
                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status);
                });
            };
            PageControllerBase.prototype.getInitialEntity = function () {
                if (this.shownAsDialog()) {
                    return this.dialogSelectedEntity();
                }
                else {
                    var breadCrumbStepModel = this.$scope.getPageModelByURL('/Producers');
                    if (isVoid(breadCrumbStepModel)) {
                        return null;
                    }
                    else {
                        return isVoid(breadCrumbStepModel.selectedEntity) ? null : breadCrumbStepModel.selectedEntity;
                    }
                }
            };
            PageControllerBase.prototype.onPageUnload = function (actualNavigation) {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                actualNavigation(self.$scope);
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    actualNavigation(self.$scope);
                }
            };
            PageControllerBase.prototype.onNewBtnAction = function () {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                self.addNewRecord();
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () { self.addNewRecord(); }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    self.addNewRecord();
                }
            };
            PageControllerBase.prototype.addNewRecord = function () {
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                self.model.modelGrpProducers.controller.cleanUpAfterSave();
                self.model.modelGrpProducers.controller.createNewEntityAndAddToUI();
            };
            PageControllerBase.prototype.onDeleteBtnAction = function () {
                var self = this;
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("EntityDeletionWarningMsg"), IconKind.QUESTION, [
                    new Tuple2("MessageBox_Button_Yes", function () { self.deleteRecord(); }),
                    new Tuple2("MessageBox_Button_No", function () { })
                ], 1, 1);
            };
            PageControllerBase.prototype.deleteRecord = function () {
                var self = this;
                if (self.Mode === Controllers.EditMode.NEW) {
                    console.log("Delete pressed in a form while being in new mode!");
                    return;
                }
                NpTypes.AlertMessage.clearAlerts(self.model);
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData(true);
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.$scope.navigateBackward();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.model, Messages.dynamicMessage(data));
                });
            };
            Object.defineProperty(PageControllerBase.prototype, "Mode", {
                get: function () {
                    var self = this;
                    if (isVoid(self.model.modelGrpProducers) || isVoid(self.model.modelGrpProducers.controller))
                        return Controllers.EditMode.NEW;
                    return self.model.modelGrpProducers.controller.Mode;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._newIsDisabled = function () {
                var self = this;
                if (isVoid(self.model.modelGrpProducers) || isVoid(self.model.modelGrpProducers.controller))
                    return true;
                return self.model.modelGrpProducers.controller._newIsDisabled();
            };
            PageControllerBase.prototype._deleteIsDisabled = function () {
                var self = this;
                if (self.Mode === Controllers.EditMode.NEW)
                    return true;
                if (isVoid(self.model.modelGrpProducers) || isVoid(self.model.modelGrpProducers.controller))
                    return true;
                return self.model.modelGrpProducers.controller._deleteIsDisabled(self.model.modelGrpProducers.controller.Current);
            };
            return PageControllerBase;
        })(Controllers.AbstractPageController);
        Producers.PageControllerBase = PageControllerBase;
        // GROUP GrpProducers
        var ModelGrpProducersBase = (function (_super) {
            __extends(ModelGrpProducersBase, _super);
            function ModelGrpProducersBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpProducersBase.prototype, "prodName", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].prodName;
                },
                set: function (prodName_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].prodName = prodName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "_prodName", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._prodName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "prodCode", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].prodCode;
                },
                set: function (prodCode_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].prodCode = prodCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "_prodCode", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._prodCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "producersId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].producersId;
                },
                set: function (producersId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].producersId = producersId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "_producersId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._producersId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpProducersBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpProducersBase;
        })(Controllers.AbstractGroupFormModel);
        Producers.ModelGrpProducersBase = ModelGrpProducersBase;
        var ControllerGrpProducersBase = (function (_super) {
            __extends(ControllerGrpProducersBase, _super);
            function ControllerGrpProducersBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                $scope.modelGrpProducers = self.model;
                var selectedEntity = this.$scope.pageModel.controller.getInitialEntity();
                if (isVoid(selectedEntity)) {
                    self.createNewEntityAndAddToUI();
                }
                else {
                    var clonedEntity = Entities.Producers.Create();
                    clonedEntity.updateInstance(selectedEntity);
                    $scope.modelGrpProducers.visibleEntities[0] = clonedEntity;
                    $scope.modelGrpProducers.selectedEntities[0] = clonedEntity;
                }
                $scope.GrpProducers_itm__prodName_disabled =
                    function (producers) {
                        return self.GrpProducers_itm__prodName_disabled(producers);
                    };
                $scope.GrpProducers_itm__prodCode_disabled =
                    function (producers) {
                        return self.GrpProducers_itm__prodCode_disabled(producers);
                    };
                $scope.pageModel.modelGrpProducers = $scope.modelGrpProducers;
                /*
                        $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                            $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.Producers[] , oldVisible:Entities.Producers[]) => {
                                console.log("visible entities changed",newVisible);
                                self.onVisibleItemsChange(newVisible, oldVisible);
                            } )  ));
                */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            ControllerGrpProducersBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpProducersBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpProducers";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpProducersBase.prototype, "HtmlDivId", {
                get: function () {
                    return "Producers_ControllerGrpProducers";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpProducersBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.TextItem(function (ent) { return 'Full Name'; }, false, function (ent) { return ent.prodName; }, function (ent) { return ent._prodName; }, function (ent) { return true; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.NumberItem(function (ent) { return 'Code'; }, false, function (ent) { return ent.prodCode; }, function (ent) { return ent._prodCode; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 0)
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpProducersBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpProducersBase.prototype, "modelGrpProducers", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpProducersBase.prototype.getEntityName = function () {
                return "Producers";
            };
            ControllerGrpProducersBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
            };
            ControllerGrpProducersBase.prototype.setCurrentFormPageBreadcrumpStepModel = function () {
                var self = this;
                if (self.$scope.pageModel.controller.shownAsDialog())
                    return;
                var breadCrumbStepModel = { selectedEntity: null };
                if (!isVoid(self.Current)) {
                    var selectedEntity = Entities.Producers.Create();
                    selectedEntity.updateInstance(self.Current);
                    breadCrumbStepModel.selectedEntity = selectedEntity;
                }
                else {
                    breadCrumbStepModel.selectedEntity = null;
                }
                self.$scope.setCurrentPageModel(breadCrumbStepModel);
            };
            ControllerGrpProducersBase.prototype.getEntitiesFromJSON = function (webResponse) {
                var _this = this;
                var entlist = Entities.Producers.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpProducersBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpProducers.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpProducersBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return false;
            };
            ControllerGrpProducersBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.Producers(
                /*prodName:string*/ null, 
                /*rowVersion:number*/ null, 
                /*prodCode:number*/ null, 
                /*producersId:string*/ self.$scope.globals.getNextSequenceValue());
                return ret;
            };
            ControllerGrpProducersBase.prototype.createNewEntityAndAddToUI = function () {
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.$scope.modelGrpProducers.visibleEntities[0] = newEnt;
                self.$scope.modelGrpProducers.selectedEntities[0] = newEnt;
                return newEnt;
            };
            ControllerGrpProducersBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpProducersBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.Producers.Create();
                ret.updateInstance(src);
                ret.producersId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.visibleEntities[0] = ret;
                this.model.selectedEntities[0] = ret;
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            Object.defineProperty(ControllerGrpProducersBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpProducersBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpProducersBase.prototype.GrpProducers_itm__prodName_disabled = function (producers) {
                var self = this;
                if (producers === undefined || producers === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Producers_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(producers, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpProducersBase.prototype.GrpProducers_itm__prodCode_disabled = function (producers) {
                var self = this;
                if (producers === undefined || producers === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Producers_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(producers, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpProducersBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpProducersBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpProducersBase.prototype._newIsDisabled = function () {
                var self = this;
                if (!self.$scope.globals.hasPrivilege("Niva_Producers_W"))
                    return true; // no write privilege
                var parEntityIsLocked = false;
                if (parEntityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpProducersBase.prototype._deleteIsDisabled = function (producers) {
                var self = this;
                if (producers === null || producers === undefined || producers.getEntityName() !== "Producers")
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Producers_D"))
                    return true; // no delete privilege;
                var entityIsLocked = self.isEntityLocked(producers, Controllers.LockKind.DeleteLock);
                if (entityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            return ControllerGrpProducersBase;
        })(Controllers.AbstractGroupFormController);
        Producers.ControllerGrpProducersBase = ControllerGrpProducersBase;
    })(Producers = Controllers.Producers || (Controllers.Producers = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ProducersBase.js.map