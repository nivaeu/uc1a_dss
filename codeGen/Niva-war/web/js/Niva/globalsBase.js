/// <reference path="../RTL/npTypes.ts" />
/// <reference path="messages.ts" />
/// <reference path="EntitiesBase/DecisionMakingBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var GlobalsBase = (function (_super) {
    __extends(GlobalsBase, _super);
    function GlobalsBase() {
        _super.call(this);
        this._globalDema = new NpTypes.UIModel(undefined);
        this.appName = 'Niva';
        this.appTitle = 'Use Case 1a: Decision Support System';
        this.globalLang = 'en';
        this.requestAuthorizationOptions.getFormTitle = function (url) { return 'Αίτημα Εξουσιοδότησης'; };
        this.requestAuthorizationOptions.getUserNameLabel = function (url) { return 'Χρήστης'; };
        this.requestAuthorizationOptions.getPasswordLabel = function (url) { return 'Κωδικός'; };
        this.requestAuthorizationOptions.getDisplayedErrMsg = function (url, errMsg) { return errMsg; };
        this.getDynamicMessage = function (msgKey) { return Messages.dynamicMessage(msgKey); };
    }
    Object.defineProperty(GlobalsBase.prototype, "globalDema", {
        get: function () {
            return this._globalDema.value;
        },
        set: function (vl) {
            this._globalDema.value = vl;
        },
        enumerable: true,
        configurable: true
    });
    GlobalsBase.prototype.refresh = function (ctrl) {
        if (!isVoid(this.globalDema) && !this.globalDema.isNew()) {
            this.globalDema.refresh(ctrl);
        }
    };
    GlobalsBase.prototype.initializeServerSideGlobals = function (response) {
    };
    return GlobalsBase;
})(NpTypes.RTLGlobals);
;
//# sourceMappingURL=globalsBase.js.map