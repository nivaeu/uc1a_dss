//1054BBA6AD328D3D6A9F3B08FB6192B3
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/GpRequestsSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelGpRequestsSearch = (function (_super) {
        __extends(ModelGpRequestsSearch, _super);
        function ModelGpRequestsSearch() {
            _super.apply(this, arguments);
        }
        return ModelGpRequestsSearch;
    })(Controllers.ModelGpRequestsSearchBase);
    Controllers.ModelGpRequestsSearch = ModelGpRequestsSearch;
    var ControllerGpRequestsSearch = (function (_super) {
        __extends(ControllerGpRequestsSearch, _super);
        function ControllerGpRequestsSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelGpRequestsSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerGpRequestsSearch;
    })(Controllers.ControllerGpRequestsSearchBase);
    Controllers.ControllerGpRequestsSearch = ControllerGpRequestsSearch;
    g_controllers['ControllerGpRequestsSearch'] = Controllers.ControllerGpRequestsSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=GpRequestsSearch.js.map