//3CB46D4F4770D8B104EE5DC952B58F02
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelParcelSearch = (function (_super) {
        __extends(ModelParcelSearch, _super);
        function ModelParcelSearch() {
            _super.apply(this, arguments);
        }
        return ModelParcelSearch;
    })(Controllers.ModelParcelSearchBase);
    Controllers.ModelParcelSearch = ModelParcelSearch;
    var ControllerParcelSearch = (function (_super) {
        __extends(ControllerParcelSearch, _super);
        function ControllerParcelSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelParcelSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerParcelSearch;
    })(Controllers.ControllerParcelSearchBase);
    Controllers.ControllerParcelSearch = ControllerParcelSearch;
    g_controllers['ControllerParcelSearch'] = Controllers.ControllerParcelSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ParcelSearch.js.map