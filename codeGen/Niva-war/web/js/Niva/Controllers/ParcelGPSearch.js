//85A7A104974C01CD5EA26826A63D9F99
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelGPSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelParcelGPSearch = (function (_super) {
        __extends(ModelParcelGPSearch, _super);
        function ModelParcelGPSearch() {
            _super.apply(this, arguments);
        }
        return ModelParcelGPSearch;
    })(Controllers.ModelParcelGPSearchBase);
    Controllers.ModelParcelGPSearch = ModelParcelGPSearch;
    var ControllerParcelGPSearch = (function (_super) {
        __extends(ControllerParcelGPSearch, _super);
        function ControllerParcelGPSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelParcelGPSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerParcelGPSearch;
    })(Controllers.ControllerParcelGPSearchBase);
    Controllers.ControllerParcelGPSearch = ControllerParcelGPSearch;
    g_controllers['ControllerParcelGPSearch'] = Controllers.ControllerParcelGPSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ParcelGPSearch.js.map