//448CE064B01E30DE9950DA873118068B
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelClasSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelParcelClasSearch = (function (_super) {
        __extends(ModelParcelClasSearch, _super);
        function ModelParcelClasSearch() {
            _super.apply(this, arguments);
        }
        return ModelParcelClasSearch;
    })(Controllers.ModelParcelClasSearchBase);
    Controllers.ModelParcelClasSearch = ModelParcelClasSearch;
    var ControllerParcelClasSearch = (function (_super) {
        __extends(ControllerParcelClasSearch, _super);
        function ControllerParcelClasSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelParcelClasSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerParcelClasSearch;
    })(Controllers.ControllerParcelClasSearchBase);
    Controllers.ControllerParcelClasSearch = ControllerParcelClasSearch;
    g_controllers['ControllerParcelClasSearch'] = Controllers.ControllerParcelClasSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ParcelClasSearch.js.map