/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/EcCult.ts" />
/// <reference path="../Entities/SuperClassDetail.ts" />
/// <reference path="../Entities/EcCultCopy.ts" />
/// <reference path="../Entities/SuperClas.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var SuperClasBase = (function (_super) {
        __extends(SuperClasBase, _super);
        function SuperClasBase(sucaId, code, name, rowVersion) {
            _super.call(this);
            var self = this;
            this._sucaId = new NpTypes.UIStringModel(sucaId, this);
            this._code = new NpTypes.UIStringModel(code, this);
            this._name = new NpTypes.UIStringModel(name, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            self.postConstruct();
        }
        SuperClasBase.prototype.getKey = function () {
            return this.sucaId;
        };
        SuperClasBase.prototype.getKeyName = function () {
            return "sucaId";
        };
        SuperClasBase.prototype.getEntityName = function () {
            return 'SuperClas';
        };
        SuperClasBase.prototype.fromJSON = function (data) {
            return Entities.SuperClas.fromJSONComplete(data);
        };
        Object.defineProperty(SuperClasBase.prototype, "sucaId", {
            get: function () {
                return this._sucaId.value;
            },
            set: function (sucaId) {
                var self = this;
                self._sucaId.value = sucaId;
                //    self.sucaId_listeners.forEach(cb => { cb(<SuperClas>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SuperClasBase.prototype, "code", {
            get: function () {
                return this._code.value;
            },
            set: function (code) {
                var self = this;
                self._code.value = code;
                //    self.code_listeners.forEach(cb => { cb(<SuperClas>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SuperClasBase.prototype, "name", {
            get: function () {
                return this._name.value;
            },
            set: function (name) {
                var self = this;
                self._name.value = name;
                //    self.name_listeners.forEach(cb => { cb(<SuperClas>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SuperClasBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<SuperClas>self); });
            },
            enumerable: true,
            configurable: true
        });
        SuperClasBase.prototype.ecCultCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.SuperClas.ecCultCollection)) {
                if (self instanceof Entities.SuperClas) {
                    Entities.SuperClas.ecCultCollection(self, func);
                }
            }
        };
        SuperClasBase.prototype.superClassDetailCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.SuperClas.superClassDetailCollection)) {
                if (self instanceof Entities.SuperClas) {
                    Entities.SuperClas.superClassDetailCollection(self, func);
                }
            }
        };
        SuperClasBase.prototype.ecCultCopyCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.SuperClas.ecCultCopyCollection)) {
                if (self instanceof Entities.SuperClas) {
                    Entities.SuperClas.ecCultCopyCollection(self, func);
                }
            }
        };
        /*
        public removeAllListeners() {
            var self = this;
            self.sucaId_listeners.splice(0);
            self.code_listeners.splice(0);
            self.name_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
        }
        */
        SuperClasBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "SuperClas:" + x.sucaId;
            var ret = new Entities.SuperClas(x.sucaId, x.code, x.name, x.rowVersion);
            deserializedEntities[key] = ret;
            return ret;
        };
        SuperClasBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        SuperClasBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "SuperClas:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "SuperClasBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "SuperClas:"+x.sucaId;
                    var cachedCopy = <SuperClas>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = SuperClas.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = SuperClas.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.SuperClas.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        SuperClasBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.SuperClas.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        SuperClasBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/SuperClas/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.SuperClas.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        SuperClasBase.prototype.toJSON = function () {
            var ret = {};
            ret.sucaId = this.sucaId;
            ret.code = this.code;
            ret.name = this.name;
            ret.rowVersion = this.rowVersion;
            return ret;
        };
        SuperClasBase.prototype.updateInstance = function (other) {
            var self = this;
            self.sucaId = other.sucaId;
            self.code = other.code;
            self.name = other.name;
            self.rowVersion = other.rowVersion;
        };
        SuperClasBase.Create = function () {
            return new Entities.SuperClas(undefined, undefined, undefined, undefined);
        };
        SuperClasBase.CreateById = function (sucaId) {
            var ret = Entities.SuperClas.Create();
            ret.sucaId = sucaId;
            return ret;
        };
        SuperClasBase.findById_unecrypted = function (sucaId, $scope, $http, errFunc) {
            if (Entities.SuperClas.cashedEntities[sucaId.toString()] !== undefined)
                return Entities.SuperClas.cashedEntities[sucaId.toString()];
            var wsPath = "SuperClas/findBySucaId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['sucaId'] = sucaId;
            var ret = Entities.SuperClas.Create();
            Entities.SuperClas.cashedEntities[sucaId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.SuperClas.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + sucaId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        SuperClasBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        //public rowVersion_listeners: Array<(c:SuperClas) => void> = [];
        SuperClasBase.ecCultCollection = null; //(superClas, list) => { };
        SuperClasBase.superClassDetailCollection = null; //(superClas, list) => { };
        SuperClasBase.ecCultCopyCollection = null; //(superClas, list) => { };
        SuperClasBase.cashedEntities = {};
        return SuperClasBase;
    })(NpTypes.BaseEntity);
    Entities.SuperClasBase = SuperClasBase;
    NpTypes.BaseEntity.entitiesFactory['SuperClas'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.SuperClas.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.SuperClas.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=SuperClasBase.js.map