/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/FmisUser.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var FmisUserBase = (function (_super) {
        __extends(FmisUserBase, _super);
        function FmisUserBase(fmisName, rowVersion, fmisUsersId, fmisUid) {
            _super.call(this);
            var self = this;
            this._fmisName = new NpTypes.UIStringModel(fmisName, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._fmisUsersId = new NpTypes.UIStringModel(fmisUsersId, this);
            this._fmisUid = new NpTypes.UIStringModel(fmisUid, this);
            self.postConstruct();
        }
        FmisUserBase.prototype.getKey = function () {
            return this.fmisUsersId;
        };
        FmisUserBase.prototype.getKeyName = function () {
            return "fmisUsersId";
        };
        FmisUserBase.prototype.getEntityName = function () {
            return 'FmisUser';
        };
        FmisUserBase.prototype.fromJSON = function (data) {
            return Entities.FmisUser.fromJSONComplete(data);
        };
        Object.defineProperty(FmisUserBase.prototype, "fmisName", {
            get: function () {
                return this._fmisName.value;
            },
            set: function (fmisName) {
                var self = this;
                self._fmisName.value = fmisName;
                //    self.fmisName_listeners.forEach(cb => { cb(<FmisUser>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FmisUserBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<FmisUser>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FmisUserBase.prototype, "fmisUsersId", {
            get: function () {
                return this._fmisUsersId.value;
            },
            set: function (fmisUsersId) {
                var self = this;
                self._fmisUsersId.value = fmisUsersId;
                //    self.fmisUsersId_listeners.forEach(cb => { cb(<FmisUser>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(FmisUserBase.prototype, "fmisUid", {
            get: function () {
                return this._fmisUid.value;
            },
            set: function (fmisUid) {
                var self = this;
                self._fmisUid.value = fmisUid;
                //    self.fmisUid_listeners.forEach(cb => { cb(<FmisUser>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public fmisUid_listeners: Array<(c:FmisUser) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.fmisName_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.fmisUsersId_listeners.splice(0);
            self.fmisUid_listeners.splice(0);
        }
        */
        FmisUserBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "FmisUser:" + x.fmisUsersId;
            var ret = new Entities.FmisUser(x.fmisName, x.rowVersion, x.fmisUsersId, x.fmisUid);
            deserializedEntities[key] = ret;
            return ret;
        };
        FmisUserBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        FmisUserBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "FmisUser:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "FmisUserBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "FmisUser:"+x.fmisUsersId;
                    var cachedCopy = <FmisUser>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = FmisUser.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = FmisUser.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.FmisUser.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        FmisUserBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.FmisUser.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        FmisUserBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/FmisUser/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.FmisUser.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        FmisUserBase.prototype.toJSON = function () {
            var ret = {};
            ret.fmisName = this.fmisName;
            ret.rowVersion = this.rowVersion;
            ret.fmisUsersId = this.fmisUsersId;
            ret.fmisUid = this.fmisUid;
            return ret;
        };
        FmisUserBase.prototype.updateInstance = function (other) {
            var self = this;
            self.fmisName = other.fmisName;
            self.rowVersion = other.rowVersion;
            self.fmisUsersId = other.fmisUsersId;
            self.fmisUid = other.fmisUid;
        };
        FmisUserBase.Create = function () {
            return new Entities.FmisUser(undefined, undefined, undefined, undefined);
        };
        FmisUserBase.CreateById = function (fmisUsersId) {
            var ret = Entities.FmisUser.Create();
            ret.fmisUsersId = fmisUsersId;
            return ret;
        };
        FmisUserBase.findById_unecrypted = function (fmisUsersId, $scope, $http, errFunc) {
            if (Entities.FmisUser.cashedEntities[fmisUsersId.toString()] !== undefined)
                return Entities.FmisUser.cashedEntities[fmisUsersId.toString()];
            var wsPath = "FmisUser/findByFmisUsersId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['fmisUsersId'] = fmisUsersId;
            var ret = Entities.FmisUser.Create();
            Entities.FmisUser.cashedEntities[fmisUsersId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.FmisUser.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + fmisUsersId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        FmisUserBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        FmisUserBase.cashedEntities = {};
        return FmisUserBase;
    })(NpTypes.BaseEntity);
    Entities.FmisUserBase = FmisUserBase;
    NpTypes.BaseEntity.entitiesFactory['FmisUser'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.FmisUser.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.FmisUser.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=FmisUserBase.js.map