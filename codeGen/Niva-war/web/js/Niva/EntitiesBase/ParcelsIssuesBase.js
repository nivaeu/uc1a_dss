/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/FmisDecision.ts" />
/// <reference path="../Entities/GpDecision.ts" />
/// <reference path="../Entities/GpRequestsContexts.ts" />
/// <reference path="../Entities/FmisUpload.ts" />
/// <reference path="../Entities/ParcelsIssuesStatusPerDema.ts" />
/// <reference path="../Entities/ParcelsIssuesActivities.ts" />
/// <reference path="../Entities/ParcelClas.ts" />
/// <reference path="../Entities/ParcelsIssues.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var ParcelsIssuesBase = (function (_super) {
        __extends(ParcelsIssuesBase, _super);
        function ParcelsIssuesBase(parcelsIssuesId, dteCreated, status, dteStatusUpdate, rowVersion, typeOfIssue, active, pclaId) {
            _super.call(this);
            var self = this;
            this._parcelsIssuesId = new NpTypes.UIStringModel(parcelsIssuesId, this);
            this._dteCreated = new NpTypes.UIDateModel(dteCreated, this);
            this._status = new NpTypes.UINumberModel(status, this);
            this._dteStatusUpdate = new NpTypes.UIDateModel(dteStatusUpdate, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._typeOfIssue = new NpTypes.UINumberModel(typeOfIssue, this);
            this._active = new NpTypes.UIBoolModel(active, this);
            this._pclaId = new NpTypes.UIManyToOneModel(pclaId, this);
            self.postConstruct();
        }
        ParcelsIssuesBase.prototype.getKey = function () {
            return this.parcelsIssuesId;
        };
        ParcelsIssuesBase.prototype.getKeyName = function () {
            return "parcelsIssuesId";
        };
        ParcelsIssuesBase.prototype.getEntityName = function () {
            return 'ParcelsIssues';
        };
        ParcelsIssuesBase.prototype.fromJSON = function (data) {
            return Entities.ParcelsIssues.fromJSONComplete(data);
        };
        Object.defineProperty(ParcelsIssuesBase.prototype, "parcelsIssuesId", {
            get: function () {
                return this._parcelsIssuesId.value;
            },
            set: function (parcelsIssuesId) {
                var self = this;
                self._parcelsIssuesId.value = parcelsIssuesId;
                //    self.parcelsIssuesId_listeners.forEach(cb => { cb(<ParcelsIssues>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelsIssuesBase.prototype, "dteCreated", {
            get: function () {
                return this._dteCreated.value;
            },
            set: function (dteCreated) {
                var self = this;
                self._dteCreated.value = dteCreated;
                //    self.dteCreated_listeners.forEach(cb => { cb(<ParcelsIssues>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelsIssuesBase.prototype, "status", {
            get: function () {
                return this._status.value;
            },
            set: function (status) {
                var self = this;
                self._status.value = status;
                //    self.status_listeners.forEach(cb => { cb(<ParcelsIssues>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelsIssuesBase.prototype, "dteStatusUpdate", {
            get: function () {
                return this._dteStatusUpdate.value;
            },
            set: function (dteStatusUpdate) {
                var self = this;
                self._dteStatusUpdate.value = dteStatusUpdate;
                //    self.dteStatusUpdate_listeners.forEach(cb => { cb(<ParcelsIssues>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelsIssuesBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<ParcelsIssues>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelsIssuesBase.prototype, "typeOfIssue", {
            get: function () {
                return this._typeOfIssue.value;
            },
            set: function (typeOfIssue) {
                var self = this;
                self._typeOfIssue.value = typeOfIssue;
                //    self.typeOfIssue_listeners.forEach(cb => { cb(<ParcelsIssues>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelsIssuesBase.prototype, "active", {
            get: function () {
                return this._active.value;
            },
            set: function (active) {
                var self = this;
                self._active.value = active;
                //    self.active_listeners.forEach(cb => { cb(<ParcelsIssues>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelsIssuesBase.prototype, "pclaId", {
            get: function () {
                return this._pclaId.value;
            },
            set: function (pclaId) {
                var self = this;
                self._pclaId.value = pclaId;
                //    self.pclaId_listeners.forEach(cb => { cb(<ParcelsIssues>self); });
            },
            enumerable: true,
            configurable: true
        });
        ParcelsIssuesBase.prototype.fmisDecisionCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.ParcelsIssues.fmisDecisionCollection)) {
                if (self instanceof Entities.ParcelsIssues) {
                    Entities.ParcelsIssues.fmisDecisionCollection(self, func);
                }
            }
        };
        ParcelsIssuesBase.prototype.gpDecisionCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.ParcelsIssues.gpDecisionCollection)) {
                if (self instanceof Entities.ParcelsIssues) {
                    Entities.ParcelsIssues.gpDecisionCollection(self, func);
                }
            }
        };
        ParcelsIssuesBase.prototype.gpRequestsContextsCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.ParcelsIssues.gpRequestsContextsCollection)) {
                if (self instanceof Entities.ParcelsIssues) {
                    Entities.ParcelsIssues.gpRequestsContextsCollection(self, func);
                }
            }
        };
        ParcelsIssuesBase.prototype.fmisUploadCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.ParcelsIssues.fmisUploadCollection)) {
                if (self instanceof Entities.ParcelsIssues) {
                    Entities.ParcelsIssues.fmisUploadCollection(self, func);
                }
            }
        };
        ParcelsIssuesBase.prototype.parcelsIssuesStatusPerDemaCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.ParcelsIssues.parcelsIssuesStatusPerDemaCollection)) {
                if (self instanceof Entities.ParcelsIssues) {
                    Entities.ParcelsIssues.parcelsIssuesStatusPerDemaCollection(self, func);
                }
            }
        };
        ParcelsIssuesBase.prototype.parcelsIssuesActivitiesCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.ParcelsIssues.parcelsIssuesActivitiesCollection)) {
                if (self instanceof Entities.ParcelsIssues) {
                    Entities.ParcelsIssues.parcelsIssuesActivitiesCollection(self, func);
                }
            }
        };
        /*
        public removeAllListeners() {
            var self = this;
            self.parcelsIssuesId_listeners.splice(0);
            self.dteCreated_listeners.splice(0);
            self.status_listeners.splice(0);
            self.dteStatusUpdate_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.typeOfIssue_listeners.splice(0);
            self.active_listeners.splice(0);
            self.pclaId_listeners.splice(0);
        }
        */
        ParcelsIssuesBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "ParcelsIssues:" + x.parcelsIssuesId;
            var ret = new Entities.ParcelsIssues(x.parcelsIssuesId, isVoid(x.dteCreated) ? null : new Date(x.dteCreated), x.status, isVoid(x.dteStatusUpdate) ? null : new Date(x.dteStatusUpdate), x.rowVersion, x.typeOfIssue, x.active, x.pclaId);
            deserializedEntities[key] = ret;
            ret.pclaId = (x.pclaId !== undefined && x.pclaId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.pclaId.$entityName].fromJSONPartial(x.pclaId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        ParcelsIssuesBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        ParcelsIssuesBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "ParcelsIssues:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "ParcelsIssuesBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "ParcelsIssues:"+x.parcelsIssuesId;
                    var cachedCopy = <ParcelsIssues>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = ParcelsIssues.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = ParcelsIssues.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.ParcelsIssues.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        ParcelsIssuesBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.ParcelsIssues.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        ParcelsIssuesBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/ParcelsIssues/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.ParcelsIssues.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        ParcelsIssuesBase.prototype.toJSON = function () {
            var ret = {};
            ret.parcelsIssuesId = this.parcelsIssuesId;
            ret.dteCreated = this.dteCreated;
            ret.status = this.status;
            ret.dteStatusUpdate = this.dteStatusUpdate;
            ret.rowVersion = this.rowVersion;
            ret.typeOfIssue = this.typeOfIssue;
            ret.active = this.active;
            ret.pclaId =
                (this.pclaId !== undefined && this.pclaId !== null) ?
                    { pclaId: this.pclaId.pclaId } :
                    (this.pclaId === undefined ? undefined : null);
            return ret;
        };
        ParcelsIssuesBase.prototype.updateInstance = function (other) {
            var self = this;
            self.parcelsIssuesId = other.parcelsIssuesId;
            self.dteCreated = other.dteCreated;
            self.status = other.status;
            self.dteStatusUpdate = other.dteStatusUpdate;
            self.rowVersion = other.rowVersion;
            self.typeOfIssue = other.typeOfIssue;
            self.active = other.active;
            self.pclaId = other.pclaId;
        };
        ParcelsIssuesBase.Create = function () {
            return new Entities.ParcelsIssues(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined);
        };
        ParcelsIssuesBase.CreateById = function (parcelsIssuesId) {
            var ret = Entities.ParcelsIssues.Create();
            ret.parcelsIssuesId = parcelsIssuesId;
            return ret;
        };
        ParcelsIssuesBase.findById_unecrypted = function (parcelsIssuesId, $scope, $http, errFunc) {
            if (Entities.ParcelsIssues.cashedEntities[parcelsIssuesId.toString()] !== undefined)
                return Entities.ParcelsIssues.cashedEntities[parcelsIssuesId.toString()];
            var wsPath = "ParcelsIssues/findByParcelsIssuesId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['parcelsIssuesId'] = parcelsIssuesId;
            var ret = Entities.ParcelsIssues.Create();
            Entities.ParcelsIssues.cashedEntities[parcelsIssuesId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.ParcelsIssues.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + parcelsIssuesId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        ParcelsIssuesBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        //public pclaId_listeners: Array<(c:ParcelsIssues) => void> = [];
        ParcelsIssuesBase.fmisDecisionCollection = null; //(parcelsIssues, list) => { };
        ParcelsIssuesBase.gpDecisionCollection = null; //(parcelsIssues, list) => { };
        ParcelsIssuesBase.gpRequestsContextsCollection = null; //(parcelsIssues, list) => { };
        ParcelsIssuesBase.fmisUploadCollection = null; //(parcelsIssues, list) => { };
        ParcelsIssuesBase.parcelsIssuesStatusPerDemaCollection = null; //(parcelsIssues, list) => { };
        ParcelsIssuesBase.parcelsIssuesActivitiesCollection = null; //(parcelsIssues, list) => { };
        ParcelsIssuesBase.cashedEntities = {};
        return ParcelsIssuesBase;
    })(NpTypes.BaseEntity);
    Entities.ParcelsIssuesBase = ParcelsIssuesBase;
    NpTypes.BaseEntity.entitiesFactory['ParcelsIssues'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.ParcelsIssues.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.ParcelsIssues.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=ParcelsIssuesBase.js.map