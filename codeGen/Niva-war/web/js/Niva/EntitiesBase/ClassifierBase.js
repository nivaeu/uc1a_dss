/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Classification.ts" />
/// <reference path="../Entities/Classifier.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var ClassifierBase = (function (_super) {
        __extends(ClassifierBase, _super);
        function ClassifierBase(clfrId, name, description, rowVersion) {
            _super.call(this);
            var self = this;
            this._clfrId = new NpTypes.UIStringModel(clfrId, this);
            this._name = new NpTypes.UIStringModel(name, this);
            this._description = new NpTypes.UIStringModel(description, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            self.postConstruct();
        }
        ClassifierBase.prototype.getKey = function () {
            return this.clfrId;
        };
        ClassifierBase.prototype.getKeyName = function () {
            return "clfrId";
        };
        ClassifierBase.prototype.getEntityName = function () {
            return 'Classifier';
        };
        ClassifierBase.prototype.fromJSON = function (data) {
            return Entities.Classifier.fromJSONComplete(data);
        };
        Object.defineProperty(ClassifierBase.prototype, "clfrId", {
            get: function () {
                return this._clfrId.value;
            },
            set: function (clfrId) {
                var self = this;
                self._clfrId.value = clfrId;
                //    self.clfrId_listeners.forEach(cb => { cb(<Classifier>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ClassifierBase.prototype, "name", {
            get: function () {
                return this._name.value;
            },
            set: function (name) {
                var self = this;
                self._name.value = name;
                //    self.name_listeners.forEach(cb => { cb(<Classifier>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ClassifierBase.prototype, "description", {
            get: function () {
                return this._description.value;
            },
            set: function (description) {
                var self = this;
                self._description.value = description;
                //    self.description_listeners.forEach(cb => { cb(<Classifier>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ClassifierBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<Classifier>self); });
            },
            enumerable: true,
            configurable: true
        });
        ClassifierBase.prototype.classificationCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.Classifier.classificationCollection)) {
                if (self instanceof Entities.Classifier) {
                    Entities.Classifier.classificationCollection(self, func);
                }
            }
        };
        /*
        public removeAllListeners() {
            var self = this;
            self.clfrId_listeners.splice(0);
            self.name_listeners.splice(0);
            self.description_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
        }
        */
        ClassifierBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "Classifier:" + x.clfrId;
            var ret = new Entities.Classifier(x.clfrId, x.name, x.description, x.rowVersion);
            deserializedEntities[key] = ret;
            return ret;
        };
        ClassifierBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        ClassifierBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "Classifier:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "ClassifierBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "Classifier:"+x.clfrId;
                    var cachedCopy = <Classifier>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = Classifier.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = Classifier.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.Classifier.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        ClassifierBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.Classifier.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        ClassifierBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/Classifier/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.Classifier.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        ClassifierBase.prototype.toJSON = function () {
            var ret = {};
            ret.clfrId = this.clfrId;
            ret.name = this.name;
            ret.description = this.description;
            ret.rowVersion = this.rowVersion;
            return ret;
        };
        ClassifierBase.prototype.updateInstance = function (other) {
            var self = this;
            self.clfrId = other.clfrId;
            self.name = other.name;
            self.description = other.description;
            self.rowVersion = other.rowVersion;
        };
        ClassifierBase.Create = function () {
            return new Entities.Classifier(undefined, undefined, undefined, undefined);
        };
        ClassifierBase.CreateById = function (clfrId) {
            var ret = Entities.Classifier.Create();
            ret.clfrId = clfrId;
            return ret;
        };
        ClassifierBase.findById_unecrypted = function (clfrId, $scope, $http, errFunc) {
            if (Entities.Classifier.cashedEntities[clfrId.toString()] !== undefined)
                return Entities.Classifier.cashedEntities[clfrId.toString()];
            var wsPath = "Classifier/findByClfrId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['clfrId'] = clfrId;
            var ret = Entities.Classifier.Create();
            Entities.Classifier.cashedEntities[clfrId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.Classifier.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + clfrId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        ClassifierBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        //public rowVersion_listeners: Array<(c:Classifier) => void> = [];
        ClassifierBase.classificationCollection = null; //(classifier, list) => { };
        ClassifierBase.cashedEntities = {};
        return ClassifierBase;
    })(NpTypes.BaseEntity);
    Entities.ClassifierBase = ClassifierBase;
    NpTypes.BaseEntity.entitiesFactory['Classifier'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.Classifier.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.Classifier.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=ClassifierBase.js.map