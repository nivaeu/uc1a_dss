/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/AgrisnapUsers.ts" />
/// <reference path="../Entities/Producers.ts" />
/// <reference path="../Entities/AgrisnapUsersProducers.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var AgrisnapUsersProducersBase = (function (_super) {
        __extends(AgrisnapUsersProducersBase, _super);
        function AgrisnapUsersProducersBase(dteRelCreated, dteRelCanceled, agrisnapUsersProducersId, rowVersion, agrisnapUsersId, producersId) {
            _super.call(this);
            var self = this;
            this._dteRelCreated = new NpTypes.UIDateModel(dteRelCreated, this);
            this._dteRelCanceled = new NpTypes.UIDateModel(dteRelCanceled, this);
            this._agrisnapUsersProducersId = new NpTypes.UIStringModel(agrisnapUsersProducersId, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._agrisnapUsersId = new NpTypes.UIManyToOneModel(agrisnapUsersId, this);
            this._producersId = new NpTypes.UIManyToOneModel(producersId, this);
            self.postConstruct();
        }
        AgrisnapUsersProducersBase.prototype.getKey = function () {
            return this.agrisnapUsersProducersId;
        };
        AgrisnapUsersProducersBase.prototype.getKeyName = function () {
            return "agrisnapUsersProducersId";
        };
        AgrisnapUsersProducersBase.prototype.getEntityName = function () {
            return 'AgrisnapUsersProducers';
        };
        AgrisnapUsersProducersBase.prototype.fromJSON = function (data) {
            return Entities.AgrisnapUsersProducers.fromJSONComplete(data);
        };
        Object.defineProperty(AgrisnapUsersProducersBase.prototype, "dteRelCreated", {
            get: function () {
                return this._dteRelCreated.value;
            },
            set: function (dteRelCreated) {
                var self = this;
                self._dteRelCreated.value = dteRelCreated;
                //    self.dteRelCreated_listeners.forEach(cb => { cb(<AgrisnapUsersProducers>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AgrisnapUsersProducersBase.prototype, "dteRelCanceled", {
            get: function () {
                return this._dteRelCanceled.value;
            },
            set: function (dteRelCanceled) {
                var self = this;
                self._dteRelCanceled.value = dteRelCanceled;
                //    self.dteRelCanceled_listeners.forEach(cb => { cb(<AgrisnapUsersProducers>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AgrisnapUsersProducersBase.prototype, "agrisnapUsersProducersId", {
            get: function () {
                return this._agrisnapUsersProducersId.value;
            },
            set: function (agrisnapUsersProducersId) {
                var self = this;
                self._agrisnapUsersProducersId.value = agrisnapUsersProducersId;
                //    self.agrisnapUsersProducersId_listeners.forEach(cb => { cb(<AgrisnapUsersProducers>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AgrisnapUsersProducersBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<AgrisnapUsersProducers>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AgrisnapUsersProducersBase.prototype, "agrisnapUsersId", {
            get: function () {
                return this._agrisnapUsersId.value;
            },
            set: function (agrisnapUsersId) {
                var self = this;
                self._agrisnapUsersId.value = agrisnapUsersId;
                //    self.agrisnapUsersId_listeners.forEach(cb => { cb(<AgrisnapUsersProducers>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(AgrisnapUsersProducersBase.prototype, "producersId", {
            get: function () {
                return this._producersId.value;
            },
            set: function (producersId) {
                var self = this;
                self._producersId.value = producersId;
                //    self.producersId_listeners.forEach(cb => { cb(<AgrisnapUsersProducers>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public producersId_listeners: Array<(c:AgrisnapUsersProducers) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.dteRelCreated_listeners.splice(0);
            self.dteRelCanceled_listeners.splice(0);
            self.agrisnapUsersProducersId_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.agrisnapUsersId_listeners.splice(0);
            self.producersId_listeners.splice(0);
        }
        */
        AgrisnapUsersProducersBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "AgrisnapUsersProducers:" + x.agrisnapUsersProducersId;
            var ret = new Entities.AgrisnapUsersProducers(isVoid(x.dteRelCreated) ? null : new Date(x.dteRelCreated), isVoid(x.dteRelCanceled) ? null : new Date(x.dteRelCanceled), x.agrisnapUsersProducersId, x.rowVersion, x.agrisnapUsersId, x.producersId);
            deserializedEntities[key] = ret;
            ret.agrisnapUsersId = (x.agrisnapUsersId !== undefined && x.agrisnapUsersId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.agrisnapUsersId.$entityName].fromJSONPartial(x.agrisnapUsersId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.producersId = (x.producersId !== undefined && x.producersId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.producersId.$entityName].fromJSONPartial(x.producersId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        AgrisnapUsersProducersBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        AgrisnapUsersProducersBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "AgrisnapUsersProducers:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "AgrisnapUsersProducersBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "AgrisnapUsersProducers:"+x.agrisnapUsersProducersId;
                    var cachedCopy = <AgrisnapUsersProducers>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = AgrisnapUsersProducers.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = AgrisnapUsersProducers.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.AgrisnapUsersProducers.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        AgrisnapUsersProducersBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.AgrisnapUsersProducers.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        AgrisnapUsersProducersBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/AgrisnapUsersProducers/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.AgrisnapUsersProducers.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        AgrisnapUsersProducersBase.prototype.toJSON = function () {
            var ret = {};
            ret.dteRelCreated = this.dteRelCreated;
            ret.dteRelCanceled = this.dteRelCanceled;
            ret.agrisnapUsersProducersId = this.agrisnapUsersProducersId;
            ret.rowVersion = this.rowVersion;
            ret.agrisnapUsersId =
                (this.agrisnapUsersId !== undefined && this.agrisnapUsersId !== null) ?
                    { agrisnapUsersId: this.agrisnapUsersId.agrisnapUsersId } :
                    (this.agrisnapUsersId === undefined ? undefined : null);
            ret.producersId =
                (this.producersId !== undefined && this.producersId !== null) ?
                    { producersId: this.producersId.producersId } :
                    (this.producersId === undefined ? undefined : null);
            return ret;
        };
        AgrisnapUsersProducersBase.prototype.updateInstance = function (other) {
            var self = this;
            self.dteRelCreated = other.dteRelCreated;
            self.dteRelCanceled = other.dteRelCanceled;
            self.agrisnapUsersProducersId = other.agrisnapUsersProducersId;
            self.rowVersion = other.rowVersion;
            self.agrisnapUsersId = other.agrisnapUsersId;
            self.producersId = other.producersId;
        };
        AgrisnapUsersProducersBase.Create = function () {
            return new Entities.AgrisnapUsersProducers(undefined, undefined, undefined, undefined, undefined, undefined);
        };
        AgrisnapUsersProducersBase.CreateById = function (agrisnapUsersProducersId) {
            var ret = Entities.AgrisnapUsersProducers.Create();
            ret.agrisnapUsersProducersId = agrisnapUsersProducersId;
            return ret;
        };
        AgrisnapUsersProducersBase.findById_unecrypted = function (agrisnapUsersProducersId, $scope, $http, errFunc) {
            if (Entities.AgrisnapUsersProducers.cashedEntities[agrisnapUsersProducersId.toString()] !== undefined)
                return Entities.AgrisnapUsersProducers.cashedEntities[agrisnapUsersProducersId.toString()];
            var wsPath = "AgrisnapUsersProducers/findByAgrisnapUsersProducersId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['agrisnapUsersProducersId'] = agrisnapUsersProducersId;
            var ret = Entities.AgrisnapUsersProducers.Create();
            Entities.AgrisnapUsersProducers.cashedEntities[agrisnapUsersProducersId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.AgrisnapUsersProducers.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + agrisnapUsersProducersId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        AgrisnapUsersProducersBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        AgrisnapUsersProducersBase.cashedEntities = {};
        return AgrisnapUsersProducersBase;
    })(NpTypes.BaseEntity);
    Entities.AgrisnapUsersProducersBase = AgrisnapUsersProducersBase;
    NpTypes.BaseEntity.entitiesFactory['AgrisnapUsersProducers'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.AgrisnapUsersProducers.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.AgrisnapUsersProducers.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=AgrisnapUsersProducersBase.js.map