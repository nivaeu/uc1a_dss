/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/TmpBlob.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var TmpBlobBase = (function (_super) {
        __extends(TmpBlobBase, _super);
        function TmpBlobBase(id, data, rowVersion) {
            _super.call(this);
            var self = this;
            this._id = new NpTypes.UIStringModel(id, this);
            this._data = new NpTypes.UIBlobModel(data, function () { return '...'; }, function (fileName) { }, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            self.postConstruct();
        }
        TmpBlobBase.prototype.getKey = function () {
            return this.id;
        };
        TmpBlobBase.prototype.getKeyName = function () {
            return "id";
        };
        TmpBlobBase.prototype.getEntityName = function () {
            return 'TmpBlob';
        };
        TmpBlobBase.prototype.fromJSON = function (data) {
            return Entities.TmpBlob.fromJSONComplete(data);
        };
        Object.defineProperty(TmpBlobBase.prototype, "id", {
            get: function () {
                return this._id.value;
            },
            set: function (id) {
                var self = this;
                self._id.value = id;
                //    self.id_listeners.forEach(cb => { cb(<TmpBlob>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TmpBlobBase.prototype, "data", {
            get: function () {
                return this._data.value;
            },
            set: function (data) {
                var self = this;
                self._data.value = data;
                //    self.data_listeners.forEach(cb => { cb(<TmpBlob>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TmpBlobBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<TmpBlob>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public rowVersion_listeners: Array<(c:TmpBlob) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.id_listeners.splice(0);
            self.data_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
        }
        */
        TmpBlobBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "TmpBlob:" + x.id;
            var ret = new Entities.TmpBlob(x.id, x.data, x.rowVersion);
            deserializedEntities[key] = ret;
            return ret;
        };
        TmpBlobBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        TmpBlobBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "TmpBlob:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "TmpBlobBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "TmpBlob:"+x.id;
                    var cachedCopy = <TmpBlob>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = TmpBlob.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = TmpBlob.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.TmpBlob.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        TmpBlobBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.TmpBlob.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        TmpBlobBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/TmpBlob/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.TmpBlob.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        TmpBlobBase.prototype.toJSON = function () {
            var ret = {};
            ret.id = this.id;
            ret.data = this.data;
            ret.rowVersion = this.rowVersion;
            return ret;
        };
        TmpBlobBase.prototype.updateInstance = function (other) {
            var self = this;
            self.id = other.id;
            self.data = other.data;
            self.rowVersion = other.rowVersion;
        };
        TmpBlobBase.Create = function () {
            return new Entities.TmpBlob(undefined, undefined, undefined);
        };
        TmpBlobBase.CreateById = function (id) {
            var ret = Entities.TmpBlob.Create();
            ret.id = id;
            return ret;
        };
        TmpBlobBase.findById_unecrypted = function (id, $scope, $http, errFunc) {
            if (Entities.TmpBlob.cashedEntities[id.toString()] !== undefined)
                return Entities.TmpBlob.cashedEntities[id.toString()];
            var wsPath = "TmpBlob/findById_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['id'] = id;
            var ret = Entities.TmpBlob.Create();
            Entities.TmpBlob.cashedEntities[id.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.TmpBlob.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + id + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        TmpBlobBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        TmpBlobBase.cashedEntities = {};
        return TmpBlobBase;
    })(NpTypes.BaseEntity);
    Entities.TmpBlobBase = TmpBlobBase;
    NpTypes.BaseEntity.entitiesFactory['TmpBlob'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.TmpBlob.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.TmpBlob.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=TmpBlobBase.js.map