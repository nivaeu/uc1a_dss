/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/EcCult.ts" />
/// <reference path="../Entities/FmisDecision.ts" />
/// <reference path="../Entities/SuperClassDetail.ts" />
/// <reference path="../Entities/GpDecision.ts" />
/// <reference path="../Entities/EcCultCopy.ts" />
/// <reference path="../Entities/ParcelClas.ts" />
/// <reference path="../Entities/ParcelClas.ts" />
/// <reference path="../Entities/ParcelClas.ts" />
/// <reference path="../Entities/CoverType.ts" />
/// <reference path="../Entities/ExcelFile.ts" />
/// <reference path="../Entities/Cultivation.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var CultivationBase = (function (_super) {
        __extends(CultivationBase, _super);
        function CultivationBase(cultId, name, code, rowVersion, cotyId, exfiId) {
            _super.call(this);
            var self = this;
            this._cultId = new NpTypes.UIStringModel(cultId, this);
            this._name = new NpTypes.UIStringModel(name, this);
            this._code = new NpTypes.UINumberModel(code, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._cotyId = new NpTypes.UIManyToOneModel(cotyId, this);
            this._exfiId = new NpTypes.UIManyToOneModel(exfiId, this);
            self.postConstruct();
        }
        CultivationBase.prototype.getKey = function () {
            return this.cultId;
        };
        CultivationBase.prototype.getKeyName = function () {
            return "cultId";
        };
        CultivationBase.prototype.getEntityName = function () {
            return 'Cultivation';
        };
        CultivationBase.prototype.fromJSON = function (data) {
            return Entities.Cultivation.fromJSONComplete(data);
        };
        Object.defineProperty(CultivationBase.prototype, "cultId", {
            get: function () {
                return this._cultId.value;
            },
            set: function (cultId) {
                var self = this;
                self._cultId.value = cultId;
                //    self.cultId_listeners.forEach(cb => { cb(<Cultivation>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CultivationBase.prototype, "name", {
            get: function () {
                return this._name.value;
            },
            set: function (name) {
                var self = this;
                self._name.value = name;
                //    self.name_listeners.forEach(cb => { cb(<Cultivation>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CultivationBase.prototype, "code", {
            get: function () {
                return this._code.value;
            },
            set: function (code) {
                var self = this;
                self._code.value = code;
                //    self.code_listeners.forEach(cb => { cb(<Cultivation>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CultivationBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<Cultivation>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CultivationBase.prototype, "cotyId", {
            get: function () {
                return this._cotyId.value;
            },
            set: function (cotyId) {
                var self = this;
                self._cotyId.value = cotyId;
                //    self.cotyId_listeners.forEach(cb => { cb(<Cultivation>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(CultivationBase.prototype, "exfiId", {
            get: function () {
                return this._exfiId.value;
            },
            set: function (exfiId) {
                var self = this;
                self._exfiId.value = exfiId;
                //    self.exfiId_listeners.forEach(cb => { cb(<Cultivation>self); });
            },
            enumerable: true,
            configurable: true
        });
        CultivationBase.prototype.ecCultCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.Cultivation.ecCultCollection)) {
                if (self instanceof Entities.Cultivation) {
                    Entities.Cultivation.ecCultCollection(self, func);
                }
            }
        };
        CultivationBase.prototype.fmisDecisionCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.Cultivation.fmisDecisionCollection)) {
                if (self instanceof Entities.Cultivation) {
                    Entities.Cultivation.fmisDecisionCollection(self, func);
                }
            }
        };
        CultivationBase.prototype.superClassDetailCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.Cultivation.superClassDetailCollection)) {
                if (self instanceof Entities.Cultivation) {
                    Entities.Cultivation.superClassDetailCollection(self, func);
                }
            }
        };
        CultivationBase.prototype.gpDecisionCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.Cultivation.gpDecisionCollection)) {
                if (self instanceof Entities.Cultivation) {
                    Entities.Cultivation.gpDecisionCollection(self, func);
                }
            }
        };
        CultivationBase.prototype.ecCultCopyCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.Cultivation.ecCultCopyCollection)) {
                if (self instanceof Entities.Cultivation) {
                    Entities.Cultivation.ecCultCopyCollection(self, func);
                }
            }
        };
        CultivationBase.prototype.parcelClascult_id_declCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.Cultivation.parcelClascult_id_declCollection)) {
                if (self instanceof Entities.Cultivation) {
                    Entities.Cultivation.parcelClascult_id_declCollection(self, func);
                }
            }
        };
        CultivationBase.prototype.parcelClascult_id_predCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.Cultivation.parcelClascult_id_predCollection)) {
                if (self instanceof Entities.Cultivation) {
                    Entities.Cultivation.parcelClascult_id_predCollection(self, func);
                }
            }
        };
        CultivationBase.prototype.parcelClascult_id_pred2Collection = function (func) {
            var self = this;
            if (!isVoid(Entities.Cultivation.parcelClascult_id_pred2Collection)) {
                if (self instanceof Entities.Cultivation) {
                    Entities.Cultivation.parcelClascult_id_pred2Collection(self, func);
                }
            }
        };
        /*
        public removeAllListeners() {
            var self = this;
            self.cultId_listeners.splice(0);
            self.name_listeners.splice(0);
            self.code_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.cotyId_listeners.splice(0);
            self.exfiId_listeners.splice(0);
        }
        */
        CultivationBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "Cultivation:" + x.cultId;
            var ret = new Entities.Cultivation(x.cultId, x.name, x.code, x.rowVersion, x.cotyId, x.exfiId);
            deserializedEntities[key] = ret;
            ret.cotyId = (x.cotyId !== undefined && x.cotyId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.cotyId.$entityName].fromJSONPartial(x.cotyId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.exfiId = (x.exfiId !== undefined && x.exfiId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.exfiId.$entityName].fromJSONPartial(x.exfiId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        CultivationBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        CultivationBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "Cultivation:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "CultivationBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "Cultivation:"+x.cultId;
                    var cachedCopy = <Cultivation>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = Cultivation.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = Cultivation.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.Cultivation.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        CultivationBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.Cultivation.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        CultivationBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/Cultivation/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.Cultivation.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        CultivationBase.prototype.toJSON = function () {
            var ret = {};
            ret.cultId = this.cultId;
            ret.name = this.name;
            ret.code = this.code;
            ret.rowVersion = this.rowVersion;
            ret.cotyId =
                (this.cotyId !== undefined && this.cotyId !== null) ?
                    { cotyId: this.cotyId.cotyId } :
                    (this.cotyId === undefined ? undefined : null);
            ret.exfiId =
                (this.exfiId !== undefined && this.exfiId !== null) ?
                    { id: this.exfiId.id } :
                    (this.exfiId === undefined ? undefined : null);
            return ret;
        };
        CultivationBase.prototype.updateInstance = function (other) {
            var self = this;
            self.cultId = other.cultId;
            self.name = other.name;
            self.code = other.code;
            self.rowVersion = other.rowVersion;
            self.cotyId = other.cotyId;
            self.exfiId = other.exfiId;
        };
        CultivationBase.Create = function () {
            return new Entities.Cultivation(undefined, undefined, undefined, undefined, undefined, undefined);
        };
        CultivationBase.CreateById = function (cultId) {
            var ret = Entities.Cultivation.Create();
            ret.cultId = cultId;
            return ret;
        };
        CultivationBase.findById_unecrypted = function (cultId, $scope, $http, errFunc) {
            if (Entities.Cultivation.cashedEntities[cultId.toString()] !== undefined)
                return Entities.Cultivation.cashedEntities[cultId.toString()];
            var wsPath = "Cultivation/findByCultId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['cultId'] = cultId;
            var ret = Entities.Cultivation.Create();
            Entities.Cultivation.cashedEntities[cultId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.Cultivation.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + cultId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        CultivationBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        //public exfiId_listeners: Array<(c:Cultivation) => void> = [];
        CultivationBase.ecCultCollection = null; //(cultivation, list) => { };
        CultivationBase.fmisDecisionCollection = null; //(cultivation, list) => { };
        CultivationBase.superClassDetailCollection = null; //(cultivation, list) => { };
        CultivationBase.gpDecisionCollection = null; //(cultivation, list) => { };
        CultivationBase.ecCultCopyCollection = null; //(cultivation, list) => { };
        CultivationBase.parcelClascult_id_declCollection = null; //(cultivation, list) => { };
        CultivationBase.parcelClascult_id_predCollection = null; //(cultivation, list) => { };
        CultivationBase.parcelClascult_id_pred2Collection = null; //(cultivation, list) => { };
        CultivationBase.cashedEntities = {};
        return CultivationBase;
    })(NpTypes.BaseEntity);
    Entities.CultivationBase = CultivationBase;
    NpTypes.BaseEntity.entitiesFactory['Cultivation'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.Cultivation.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.Cultivation.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=CultivationBase.js.map