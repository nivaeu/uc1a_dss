//CBB799A9D06D643A0287B914444081F6
//Εxtended classes
/// <reference path="../EntitiesBase/SuperClasBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var SuperClas = (function (_super) {
        __extends(SuperClas, _super);
        function SuperClas() {
            _super.apply(this, arguments);
        }
        SuperClas.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.SuperClasBase.fromJSONComplete(data, deserializedEntities);
        };
        return SuperClas;
    })(Entities.SuperClasBase);
    Entities.SuperClas = SuperClas;
})(Entities || (Entities = {}));
//# sourceMappingURL=SuperClas.js.map