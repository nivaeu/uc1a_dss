//AC42F303F23F1D9A5E0285EB17948378
//Εxtended classes
/// <reference path="../EntitiesBase/ParcelClasBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var ParcelClas = (function (_super) {
        __extends(ParcelClas, _super);
        function ParcelClas() {
            _super.apply(this, arguments);
        }
        ParcelClas.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.ParcelClasBase.fromJSONComplete(data, deserializedEntities);
        };
        return ParcelClas;
    })(Entities.ParcelClasBase);
    Entities.ParcelClas = ParcelClas;
})(Entities || (Entities = {}));
//# sourceMappingURL=ParcelClas.js.map