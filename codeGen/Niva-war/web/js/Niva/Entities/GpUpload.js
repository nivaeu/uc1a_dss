//8D57FFB7AE38A742A550B99E8DC72680
//Εxtended classes
/// <reference path="../EntitiesBase/GpUploadBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var GpUpload = (function (_super) {
        __extends(GpUpload, _super);
        function GpUpload() {
            _super.apply(this, arguments);
        }
        GpUpload.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.GpUploadBase.fromJSONComplete(data, deserializedEntities);
        };
        return GpUpload;
    })(Entities.GpUploadBase);
    Entities.GpUpload = GpUpload;
})(Entities || (Entities = {}));
//# sourceMappingURL=GpUpload.js.map