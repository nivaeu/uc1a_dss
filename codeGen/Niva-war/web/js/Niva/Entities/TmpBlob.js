//63041AD5E549498F89CC1BA6F0E2F280
//Εxtended classes
/// <reference path="../EntitiesBase/TmpBlobBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var TmpBlob = (function (_super) {
        __extends(TmpBlob, _super);
        function TmpBlob() {
            _super.apply(this, arguments);
        }
        TmpBlob.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.TmpBlobBase.fromJSONComplete(data, deserializedEntities);
        };
        return TmpBlob;
    })(Entities.TmpBlobBase);
    Entities.TmpBlob = TmpBlob;
})(Entities || (Entities = {}));
//# sourceMappingURL=TmpBlob.js.map