/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../npTypes.ts" />
/// <reference path="../services.ts" />
var Controllers;
(function (Controllers) {
    var controllerMenu = (function () {
        function controllerMenu($scope, $timeout, Plato, NavigationService) {
            this.$scope = $scope;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.NavigationService = NavigationService;
            var self = this;
            $scope.getSelectedClass = function (current) {
                return self.getSelectedClass(current);
            };
            $scope.getMenuParentClass = function (label) {
                return self.getMenuParentClass(label);
            };
            $scope.onMenuClick = function (currentNumber, url, label, menuId) {
                self.onMenuClick(currentNumber, url, label, menuId);
            };
            // We want to resize the menu and main area after the element has been displayed or not
            $scope.$watch(function () { return angular.element('.resizable1').is(':visible'); }, function (newValue, oldValue) {
                if (newValue !== oldValue) {
                    Utils.UpdateMainWindowWidthAndMainButtonPlacement($scope);
                }
            });
        }
        controllerMenu.prototype.getSelectedClass = function (current) {
            var self = this;
            if (current === self.$scope.modelNavigation.current)
                return 'selected';
            else
                return '';
        };
        controllerMenu.prototype.getMenuParentClass = function (label) {
            return this.$scope.modelNavigation.btnState[label] ? "open NpMenuButton" : "NpMenuButton";
        };
        controllerMenu.prototype.onMenuClick = function (currentNumber, url, label, menuId) {
            var self = this;
            var doActualNavigation = function (pageScopeYouAreLeavingFrom) {
                self.$scope.modelNavigation.current = currentNumber;
                self.$scope.modelNavigation.steps.splice(1);
                self.$scope.modelNavigation.steps.push(new NpTypes.BreadcrumbStep(url, menuId, label, null));
                self.NavigationService.go(url, menuId);
            };
            var doNavigation = !self.$scope.modelNavigation.steps.some(function (step) { return step.menuId === menuId; });
            if (doNavigation) {
                if (self.$scope.modelNavigation.onNavigation === undefined)
                    doActualNavigation();
                else
                    self.$scope.modelNavigation.onNavigation(doActualNavigation);
            }
        };
        return controllerMenu;
    })();
    Controllers.controllerMenu = controllerMenu;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerMenu.js.map