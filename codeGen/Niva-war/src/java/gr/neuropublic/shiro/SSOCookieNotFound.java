package gr.neuropublic.shiro;

public class SSOCookieNotFound extends Exception {

    private String cookieKey;
    public SSOCookieNotFound(String cookieKey) {
        this.cookieKey = cookieKey;
    }
    public String toString() {
        return String.format("%s(%s)", this.getClass().getName(), this.cookieKey);
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
