package gr.neuropublic.shiro;
import org.apache.shiro.session.SessionListener;
import org.apache.shiro.session.Session;
import org.apache.shiro.SecurityUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ShiroSessionListener implements SessionListener {

    private static final Logger l = LoggerFactory.getLogger(ShiroSessionListener.class);

    @Override
    public void onStart(Session session) {
        // nothing
    }

    @Override
    public void onStop(Session session) {
        // l.info("**************** ON STOP &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& ");
        // nothing
        /*
        long millis = System.currentTimeMillis()- session.getStartTimestamp().getTime();
        LOG.info("User with id {} logged out. Session duration was {} minutes and {} seconds.",
                 session.getAttribute(USER_ID_KEY),
                 millis / MINUTES,
                 millis % MINUTES / MILLIS);
                 clearSessionAttributes(session); */
    }

    @Override
    public void onExpiration(Session session) {
        l.info("**************** EXPIRATION ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        /*
        long millis = System.currentTimeMillis()- session.getStartTimestamp().getTime();
        LOG.info("Session expired for user with id {}. Session duration was {} minutes and {} seconds. ",
                 session.getAttribute(USER_ID_KEY),
                 millis / MINUTES,
                 millis % MINUTES / MILLIS);
                 clearSessionAttributes(session);*/
        SecurityUtils.getSubject().logout();
    }

    /*    private void clearSessionAttributes(Session session) {
        Collection sessionAttributeKeys = session.getAttributeKeys();
        for (Iterator iterator = sessionAttributeKeys.iterator(); iterator.hasNext(); ) {
            Object attrKey =  iterator.next();
            session.removeAttribute(attrKey);
            }*/
    }
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
