package gr.neuropublic.shiro;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.servlet.http.HttpSessionListener;
import javax.servlet.http.HttpSessionEvent;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedProperty;
import javax.annotation.PostConstruct; 
import javax.faces.context.FacesContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.shiro.SecurityUtils;
import org.apache.commons.dbutils.DbUtils;

import gr.neuropublic.mutil.base.Util;
import gr.neuropublic.mutil.base.ExceptionAdapter;

import gr.neuropublic.jsf.util.JsfUtil;
import gr.neuropublic.jsf.util.FacesUtil;
import gr.neuropublic.gaia.usermngmt.util.LogoutCode;

public class ShiroHTTPSessionListener implements HttpSessionListener {

    private static final Logger l = LoggerFactory.getLogger(ShiroHTTPSessionListener.class);

    public ShiroHTTPSessionListener() {
        ensureDataSource();
    }

    /*    String appName;
    public void ensureConfigured() throws NamingException {
        try {
            InitialContext ic = new InitialContext();
            Context env = (Context) ic.lookup("java:comp/env");
            appName = (String) env.lookup("appName");
            String dataSourceURI = (String) env.lookup("userMngmtDataSourceURI");
            dataSource = (DataSource) ic.lookup(dataSourceURI);
        } catch (Exception e) {
            l.info("ensureConfigured threw exception: ", e);
            throw e;
        }
        }*/
    DataSource dataSource;
    private void ensureDataSource() {
        if (dataSource == null) {
            String dataSourceURI = JsfUtil.getFromEnv(LoginController.USER_MNGMNT_URI_ENV_KEY); // userMngmtDataSource.getDataSourceURI();
            l.debug("looking for the '{}' datasource", dataSourceURI);
            try {
                InitialContext ic = new InitialContext();
                dataSource = (DataSource) ic.lookup(dataSourceURI);
            } catch (NamingException e) { throw new ExceptionAdapter(e); }
            l.debug("data source is: "+dataSource);
        }
    }

    public void sessionCreated(HttpSessionEvent se) {
        l.info("**** sessionCreated {}", se.getSession().getId());
    }




    public void sessionDestroyed(HttpSessionEvent se) {
        l.info("**** sessionDestroyed : {}", se.getSession().getId());

        String appName = null;
        {
            appName = (String) se.getSession().getAttribute("appName");
            l.info("appName obtained as: {}", appName);
        }


        Connection        conn = null;
        PreparedStatement pstm = null; 
        try {
            conn = dataSource.getConnection();
            String stmtStr = "UPDATE subscription.ss_user_sessions SET usrs_logoutsse = ?, uslc_id="+LogoutCode.EXPIRY.getCode()+" WHERE usrs_session=? AND usrs_app=? AND usrs_logoutsse IS NULL";
            pstm  = conn.prepareStatement(stmtStr);
            pstm.setInt        (1, Util.secondsSinceTheEpoch());
            pstm.setString     (2, se.getSession().getId());
            pstm.setString     (3, appName);
            pstm.executeUpdate();
        }
        catch (SQLException    e) { l.info("sessionDestroyed: ", e); throw new ExceptionAdapter(e); }
        finally {
            DbUtils.closeQuietly(conn, pstm, null);
        }        
    }
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
