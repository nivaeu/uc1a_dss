package gr.neuropublic.shiro;


import org.apache.shiro.authc.SimpleAuthenticationInfo;

public class GaiaAuthenticationInfo extends SimpleAuthenticationInfo {

    public boolean sessionExistsInSSOStore;

    public GaiaAuthenticationInfo(Object principal, Object credentials, String realmName, boolean sessionExistsInSSOStore) {
        super(principal, credentials, realmName);
        this.sessionExistsInSSOStore = sessionExistsInSSOStore;
    }


    public static GaiaAuthenticationInfo ssoSuccessfulInfo(Object principal, String realmName) {
        return new GaiaAuthenticationInfo(principal, "foo", realmName, true);
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
