package gr.neuropublic.shiro;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;

//@ManagedBean(eager=true)
//@ApplicationScoped
public class UserMngmtDataSource implements Serializable {

    private String dataSourceURI;
    public String getDataSourceURI()                   { return dataSourceURI; }
    public void setDataSourceURI(String dataSourceURI) { this.dataSourceURI = dataSourceURI; }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
