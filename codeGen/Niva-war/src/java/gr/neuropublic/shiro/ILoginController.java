package gr.neuropublic.shiro;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.application.NavigationHandler;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.faces.bean.ManagedProperty;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import javax.faces.event.ActionEvent;
import javax.annotation.PostConstruct; 
import javax.naming.InitialContext;
import java.util.Map;
import java.util.HashMap;
import javax.faces.event.ComponentSystemEvent;
import java.util.Collection;
import java.util.Date;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpSession;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.naming.InitialContext;
import javax.naming.Context;

import org.apache.shiro.realm.jdbc.JdbcRealm;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.ConcurrentAccessException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

import gr.neuropublic.mutil.jdbc.JdbcUtils;
import gr.neuropublic.mutil.base.Util;
import gr.neuropublic.mutil.base.Pair;
import gr.neuropublic.mutil.base.Triad;
import gr.neuropublic.mutil.base.ExceptionAdapter;

import gr.neuropublic.jsf.util.JsfUtil;

import gr.neuropublic.gaia.usermngmt.util.*;

public interface ILoginController {

    public String login(String username, String password) throws UnknownAccountException, IncorrectCredentialsException, ConcurrentAccessException,
                                                                                 UserStatusNotActive,
                                                                                 SubscriberNotAssociatedWithApp,
                                                                                 UserNotAssociatedWithApp,
                                                                                 SubscriberStatusNotActive,
                                                                                 UserNotAssociatedWithAppAlthoughSubscriptionOK,
                                                                                 SubscriberModuleGroupNotActive,
                                                                                 SubscriberModuleGroupTemporalMismatch,
                                                                                 UnknownApp;
    public boolean isAuthenticated();
    public void logout();
    public boolean isPermitted(String permission);
    public void checkPermission(String permission);
    public String getNickname() ;
    public int getSubsId()      ;
    public Map<String, Object> getSubsInfo();
    public Pair<String, Integer> getEmail_Id();
    public Triad<String, Integer, String> getEmail_Id_VAT();
    public void redirectForLoggedInAndOpportunisticSSOLogin(); /*throws IncorrectCredentialsException, NamingException, SQLException,
                                                                     UnknownAccountException, ConcurrentAccessException,
                                                                                 UserStatusNotActive,
                                                                                 SubscriberNotAssociatedWithApp,
                                                                                 UserNotAssociatedWithApp,
                                                                                 SubscriberStatusNotActive,
                                                                                 UserNotAssociatedWithAppAlthoughSubscriptionOK,
                                                                                 SubscriberModuleGroupNotActive,
                                                                                 SubscriberModuleGroupTemporalMismatch,
                                                                                 UnknownApp;*/
    public void redirectIfIsAuthenticated();

    public boolean vatIsRegisteredInGaia(String vat) throws SQLException;
    public boolean vatIsRegisteredInModule(String vat, String appName) throws SQLException;
    public boolean isUserRegistedInModuleBySpecificSubscriber(String userVat, String moduleName, Integer fromSubscriber) throws SQLException;
    
    public Integer getFromSubsIdForModule(String moduleName) throws SQLException;

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
