package gr.neuropublic.shiro;


import org.apache.shiro.authc.UsernamePasswordToken;

public class GaiaAuthenticationToken extends UsernamePasswordToken {

    public String sessionId;

    public static enum Type { OPPORTUNISTIC_SSO, TRADITIONAL };

    public GaiaAuthenticationToken(String username, String password, String sessionId) {
        super(username, password);
        assertWellStructured(username, password, sessionId);
        this.sessionId = sessionId;
    }

    public void assertWellStructured(String username, String password, String sessionId) {
        if ((sessionId == null) && (username==null || password==null)) throw new RuntimeException();
        if ((sessionId != null) && (username!=null || password!=null)) throw new RuntimeException();
    }

    public Type type() {
        if   (this.sessionId == null) return Type.TRADITIONAL;
        else                          return Type.OPPORTUNISTIC_SSO;
    }

    public boolean isTypeOpportunisticSSO() {
        return type().equals(Type.OPPORTUNISTIC_SSO);
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
