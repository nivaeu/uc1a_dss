package gr.neuropublic.shiro;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.context.ExternalContext;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.faces.bean.ManagedProperty;
import java.util.List;
import java.util.Arrays;
import java.util.ArrayList;
import javax.faces.event.ActionEvent;
import javax.annotation.PostConstruct; 
import javax.naming.InitialContext;
import java.util.Map;
import java.util.HashMap;
import javax.faces.event.ComponentSystemEvent;
import java.util.Collection;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.faces.application.NavigationHandler;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.naming.InitialContext;
import javax.naming.Context;
import javax.naming.NamingException;

import org.apache.shiro.realm.jdbc.JdbcRealm;
import org.apache.commons.dbutils.DbUtils;
import org.apache.shiro.authz.AuthorizationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.ConcurrentAccessException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;

import gr.neuropublic.mutil.base.Util;

import gr.neuropublic.mutil.base.SessionIdentifierGenerator;
import gr.neuropublic.mutil.base.ExceptionAdapter;
import gr.neuropublic.mutil.base.Pair;
import gr.neuropublic.mutil.base.Triad;

import gr.neuropublic.jsf.util.JsfUtil;
import gr.neuropublic.jsf.util.FacesUtil;

import gr.neuropublic.gaia.usermngmt.util.*;

@ManagedBean
@SessionScoped
public class LoginController implements Serializable, ILoginController {


    private static final Logger l = LoggerFactory.getLogger(LoginController.class);

    private SessionIdentifierGenerator sessIdGen;
    public LoginController() {
        sessIdGen = new SessionIdentifierGenerator(System.currentTimeMillis());
    }


    public static final String USER_MNGMNT_URI_ENV_KEY = "userMngmtDataSourceURI";
    public static final String COOKIE_NAME_ENV_KEY     = "sso-cookie";
    public static final String COOKIE_DOMAIN_ENV_KEY   = "sso-domain";

    private String username;
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }

    private String password;
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }


    private String getAppName() {
        String[] components = FacesUtil.getContextPath().split("/");
        return components[components.length - 1];
    }




    private String ssoLoginCookieName;
    private void ensureSSOLoginCookieName() {
        if (this.ssoLoginCookieName == null) this.ssoLoginCookieName = JsfUtil.getFromEnv(COOKIE_NAME_ENV_KEY);
    }

    private String ssoLoginCookieDomain;
    private void ensureSSOLoginCookieDomain() {
        if (this.ssoLoginCookieDomain == null) this.ssoLoginCookieDomain = JsfUtil.getFromEnv(COOKIE_DOMAIN_ENV_KEY);
    }
    private void ensureAssertSSOLoginCookieDomain() {
        ensureSSOLoginCookieDomain();
        if (ssoLoginCookieDomain == null) throw new RuntimeException(String.format("Cookie domain (%s) not configured in web.xml", COOKIE_DOMAIN_ENV_KEY));
    }

    private String landingPage;
    private void ensureLandingPage() {
        if (this.landingPage == null) landingPage = JsfUtil.getFromEnv("landing-page");
    }
    private void ensureAssertLandingPage() {
        ensureLandingPage();
        if (this.landingPage == null) throw new RuntimeException("landingPage not configured");
    }


    private void navigateToLandingPage() {
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationHandler navigationHandler = context.getApplication().getNavigationHandler();
        navigationHandler.handleNavigation(context, null, redirectProvision("?", this.landingPage));
    }

    private void navigateToLoginPage(String ssoMsg) {
        l.info(String.format("navigateToLoginPage(%s)", ssoMsg));
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationHandler navigationHandler = context.getApplication().getNavigationHandler();
        navigationHandler.handleNavigation(context, null, redirectProvision("&", String.format("login.xhtml?ssoMsg=%s", ssoMsg)));
    }


    private String redirectProvision(String connector, String page) {
        return String.format("%s%sfaces-redirect=true", page, connector);
    }


    @Override
    public void redirectForLoggedInAndOpportunisticSSOLogin() {
        redirectIfIsAuthenticated();
        try {
            ssoLogin();
            navigateToLandingPage();
        } catch (Exception e) {
            navigateToLoginPage(e.toString());
        }
    }

    @Override
    public void redirectIfIsAuthenticated() {
        if (isAuthenticated())
            try { navigateToLandingPage(); } catch (java.lang.IllegalStateException ignoreMe) {} // innocuous redirection: this is to implement the logic that if an authenticated subject revisits the login.xhtml view, he should be directed immediately to the landing page.
    }



    public String getLoginUserName() {
        return username;
    }

    private String nickname;
    @Override
    public String getNickname() { return nickname; }

    private Integer subsId;
    @Override
    public int getSubsId() { return subsId; }

    private Map<String, Object> subsInfo;
    @Override
    public Map<String, Object> getSubsInfo() { return subsInfo; }

    private int user_id;
    private String user_vat;
    @Override
    public Pair<String, Integer> getEmail_Id() {
        return Pair.create(username, user_id);
    }
    @Override
    public Triad<String, Integer, String> getEmail_Id_VAT() {
        return Triad.create(username, user_id, user_vat);
    }

    private Pair<Integer, String> _getUser_Id_VAT(String email) throws SQLException {
        Connection conn = dataSource.getConnection();
        try {
            Map<String, Object> userProps = UserMngmtUtil.getUserInfo(conn, (String) null, email, (Integer) null);
            return Pair.create((Integer) userProps.get("user_id"), (String) userProps.get("user_vat"));
        } finally {
            DbUtils.closeQuietly(conn, null, null);
        }
    }

    DataSource dataSource;
    private void ensureDataSource() {
        if (dataSource == null) {
            String dataSourceURI = JsfUtil.getFromEnv(USER_MNGMNT_URI_ENV_KEY); // userMngmtDataSource.getDataSourceURI();
            l.debug("looking for the '{}' datasource", dataSourceURI);
            try {
                InitialContext ic = new InitialContext();
                dataSource = (DataSource) ic.lookup(dataSourceURI);
            } catch (NamingException e) { throw new ExceptionAdapter(e); }
            l.debug("data source is: "+dataSource);
        }
    }

    @PostConstruct
    public void ensureConfigured() {
        ensureSSOLoginCookieName();
        ensureDataSource();
        if (this.ssoLoginCookieName   == null) throw new RuntimeException("SSO login cookie not configured");
        if (this.dataSource           == null) throw new RuntimeException("User Mngmnt datasource not configured");
        ensureAssertLandingPage(); // replace all the above with ensureAssert
        ensureAssertSSOLoginCookieDomain();
    }


    public void ensureNicknameAndSubsId() throws NamingException, SQLException {
        if (username==null) throw new RuntimeException();
        // ensureDataSource();
        Connection conn = dataSource.getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement("SELECT a.user_nickname, a.subs_id FROM subscription.ss_users a WHERE a.user_email=?");
            ps.setString(1, username);
            rs = ps.executeQuery();
            boolean haveBeenHereBefore = false;
            while (rs.next()) {
                if (haveBeenHereBefore) throw new RuntimeException(username);
                nickname = rs.getString(1);
                subsId   = rs.getInt(2);
                haveBeenHereBefore = true;
            }
            subsInfo = UserMngmtUtil.getSubsInfo(conn, subsId);
        } finally {
            DbUtils.closeQuietly(conn, ps, rs);
        }
    }


    private void addAppNameToSession(String appName) {
        HttpServletRequest  request  = (HttpServletRequest)  FacesContext.getCurrentInstance().getExternalContext().getRequest();  
        request.getSession().setAttribute("appName", appName);
        l.info("appName configured as: {}", appName);
    }

    private void makeNoteOfSuccessfulLogin(String userName, String userIp, String userSessionId, String ssoSessionId, String appName) throws SQLException, NamingException {
        Connection conn = null;
        try {
            ensureDataSource();
            conn = dataSource.getConnection();
            UserMngmtUtil.makeNoteOfSuccessfulLogin(conn, userName, userIp, userSessionId, ssoSessionId, appName);    
        } finally {
            DbUtils.closeQuietly(conn);
        }
        addAppNameToSession(appName);
    }


    private String ssoSessionId;
    public void ensureSSOSessionId() {
        if (ssoSessionId != null) throw new RuntimeException();
        ssoSessionId = sessIdGen.nextSessionId();
    }


   @Override
   public String login(String username, String password) throws UnknownAccountException, IncorrectCredentialsException, ConcurrentAccessException,
                                                                                 UserStatusNotActive,
                                                                                 SubscriberNotAssociatedWithApp,
                                                                                 UserNotAssociatedWithAppAlthoughSubscriptionOK,
                                                                                 UserNotAssociatedWithApp,
                                                                                 SubscriberStatusNotActive,
                                                                                 SubscriberModuleGroupNotActive,
                                                                                 SubscriberModuleGroupTemporalMismatch,
                                                                                 UnknownApp {
        try {
            l.info(String.format("login(%s, %s): app-name is: %s", username, password, getAppName()));
            {
                Connection conn = null;
                try {
                    conn = dataSource.getConnection();
                    if (!UserMngmtUtil.userExists(conn, username))
                        throw new UnknownAccountException(username);
                    l.info(String.format("login(%s, %s): about to call assertSubscriptionOK(%s, %s)", username, password, username, getAppName()));
                    UserMngmtUtil.assertSubscriptionOK(conn, username, getAppName());
                    l.info(String.format("login(%s, %s): after call to assertSubscriptionOK", username, password));

                } finally {
                    DbUtils.closeQuietly(conn);
                }
            }
            PreparedStatement pstm = null;


            loginInternal(username, password);
            l.info("about to set cookie");
            setSSOCookie(ssoSessionId);
            return redirectProvision("?", this.landingPage);
        } catch (SQLException    e) { throw new ExceptionAdapter(e); }
          catch (NamingException e) { throw new ExceptionAdapter(e); }
    }

    private void loginInternal(String username, String password) throws UnknownAccountException, IncorrectCredentialsException, NamingException, SQLException {
        //l.info(String.format("loginInternal: subject class is %s", SecurityUtils.getSubject().getClass().getName()));
        //l.info(String.format("loginInternal: session id is %s", SecurityUtils.getSubject().getSession().getId()));
        GaiaAuthenticationToken token = new GaiaAuthenticationToken(username, password, null);

        Subject subject = SecurityUtils.getSubject();
        l.info(String.format("loginInternal: **** Subject is: %s with session: %s", subject, subject.getSession().getId()));
        if (!subject.isAuthenticated()) {
            l.info("loginInternal: subject is not authenticated, logging in");

            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            String clientIP = request.getHeader("X-Forwarded-For");

            if(clientIP != null && !clientIP.isEmpty()) {
                l.info("X-Forwarded-For = " + clientIP);
                clientIP = clientIP.split("\\s*,\\s*", 2)[0]; // It's a comma separated string: client,proxy1,proxy2,...
                l.info("ClientIP (from X-Forwarded-For) = " + clientIP);
            }
            else {
                clientIP = request.getRemoteAddr();
                l.info("ClientIP (from RemoteAddr) = " + clientIP);
            }

            subject.login(token);
            setUsername(username);
            setPassword(password);
            token.clear();
            ensureNicknameAndSubsId();
            ensureSSOSessionId();
            makeNoteOfSuccessfulLogin(username,
                                      clientIP,
                                      subject.getSession().getId().toString(), ssoSessionId , getAppName());
            Pair<Integer, String> _foo =  _getUser_Id_VAT(username);
            this.user_id  = _foo.a;
            this.user_vat = _foo.b;
        }
        else
            l.info("loginInternal: subject is authenticated, not logging in again");
    }

    @Override
    public boolean isAuthenticated() {
        Subject subject = SecurityUtils.getSubject();
        l.info(String.format("isAuthenticated: **** Subject is: %s with session: %s", subject, subject.getSession().getId()));
        return subject.isAuthenticated();
    }

    private void setSSOCookie(String ssoSessionId) {
         HttpServletResponse httpServletResponse = 
             (HttpServletResponse)FacesContext.getCurrentInstance().getExternalContext().getResponse();
         Cookie cookie = new Cookie(ssoLoginCookieName, ssoSessionId);
         cookie.setMaxAge(-1); // cookie not stored persistently
         cookie.setDomain(ssoLoginCookieDomain);
         cookie.setPath("/");
         httpServletResponse.addCookie(cookie);  

    }

    private void clearSSOCookie() {
        HttpServletRequest  request  = (HttpServletRequest)  FacesContext.getCurrentInstance().getExternalContext().getRequest();  
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        Cookie[] cookies  = request.getCookies();
        for (Cookie c : cookies) {
            if (c.getName().equals(ssoLoginCookieName)) {
                l.info("found the cookie: "+c.getName()+" domain:"+c.getDomain()+" exp:"+c.getMaxAge()+ "value: "+c.getValue());
                c.setMaxAge(0);  
                c.setValue(""); // it is more elegant to clear the value but not necessary  
                response.addCookie(c);  
                // NOTE: the following line may be necessary
                try { response.sendRedirect(request.getContextPath()); } catch (Exception e) {throw new ExceptionAdapter(e);}
                break;
            }
        }        
    }


    private void ssoLogin() throws IncorrectCredentialsException, UnknownAccountException,
                                                                                 UserStatusNotActive,
                                                                                 SubscriberNotAssociatedWithApp,
                                                                                 UserNotAssociatedWithApp,
                                                                                 SubscriberStatusNotActive,
                                                                                 UserNotAssociatedWithAppAlthoughSubscriptionOK,
                                                                                 SubscriberModuleGroupNotActive,
                                                                                 SubscriberModuleGroupTemporalMismatch,
                                                                                  UnknownApp, SSOCookieNotFound, SSOCookieObsolete
    {
        try {
            l.info("ssoLogin()");
            String ssoSessionId = getSSOSessionId();
            l.info("################ {} = {}", ssoLoginCookieName, ssoSessionId);
            if (ssoSessionId!=null) {
                loginSSOInternal(ssoSessionId);
            } else throw new SSOCookieNotFound( ssoLoginCookieName );
        } catch (SQLException e) {
            throw new ExceptionAdapter(e);
        } catch (NamingException e) {
            throw new ExceptionAdapter(e);
        }
    }

    private String getSSOSessionId() {
        // get cookies
        HttpServletRequest httpServletRequest = 
          (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();   
        Cookie[] cookies = httpServletRequest.getCookies();
        if (cookies != null) {
            for(int i=0; i<cookies.length; i++) {
                if (cookies[i].getName().equalsIgnoreCase(ssoLoginCookieName)) return cookies[i].getValue(); 
            }
        }
        return null;
    }

    private void loginSSOInternal(String ssoSessionId) throws UnknownAccountException, IncorrectCredentialsException, NamingException, SQLException,
                                                                             UserStatusNotActive,
                                                                             SubscriberNotAssociatedWithApp,
                                                                             UserNotAssociatedWithAppAlthoughSubscriptionOK,
                                                                             UserNotAssociatedWithApp,
                                                                             SubscriberStatusNotActive,
                                                                             SubscriberModuleGroupNotActive,
                                                                             SubscriberModuleGroupTemporalMismatch,
                                                                             UnknownApp, SSOCookieObsolete {

        l.debug("loginSSOInternal({})", ssoSessionId);
        ensureDataSource();
        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            String email = UserMngmtUtil.getUsernameOfLoggedInSession(conn, ssoSessionId);
            l.info("**************** EMAIL and APPNAME obtained as: {}, {}", email, getAppName());
            if (email == null) {
                l.info("sso session: '{}' is now obsolete", ssoSessionId);
                throw new SSOCookieObsolete(ssoSessionId);
            }
            UserMngmtUtil.assertSubscriptionOK(conn, email, getAppName());
        } finally {
            DbUtils.closeQuietly(conn);
        }
        

	GaiaAuthenticationToken token = new GaiaAuthenticationToken(null, null, ssoSessionId);
        Subject subject = SecurityUtils.getSubject();
        l.info(String.format("**** Subject is: %s with session: %s and SSO session: %s", subject, subject.getSession().getId(), ssoSessionId));
        if (!subject.isAuthenticated()) {
            l.info("subject is not authenticated, logging in");
            subject.login(token);
            token.clear();
            this.username = (String) subject.getPrincipal();
            ensureNicknameAndSubsId();
            makeNoteOfSuccessfulLogin(username, subject.getSession().getHost(), subject.getSession().getId().toString(), ssoSessionId, getAppName());
            Pair<Integer, String> _foo = _getUser_Id_VAT(username);
            this.user_id =  _foo.a;
            this.user_vat = _foo.b;
        }
        else
            l.info("subject is authenticated, not logging in again"); // TODO: I believe this code never executes
    }


    @Override
    public void logout() { // TODO throw exception NotLoggedInToBeginWith
	Subject subject = SecurityUtils.getSubject();
        if (subject != null) {
            subject.logout();
            final boolean CLEAR_SSO_COOKIE = false;
            if (CLEAR_SSO_COOKIE)
                clearSSOCookie();
        }
        final boolean ENABLE_PATCH = true; // strange patch (subject::logout should invalidate the session in container-managed session and therefore this patch should have been unecessary as we are using container-managed sessions)
        if (ENABLE_PATCH) {
            ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
            HttpSession session = (HttpSession)ectx.getSession(false);
            if (session != null)
                session.invalidate();
        }
    }

    private Set<String> scPermissions = null;

    private boolean securityClassPermitted(String username, String appName, String permission) {
        if (scPermissions!=null)
            return scPermissions.contains(permission);
        else {
            Connection conn = null;
            try {
                conn = dataSource.getConnection();
                // return UserMngmtUtil.securityClassPermitted(conn, username, appName, permission);
                scPermissions = new HashSet<String>(UserMngmtUtil.securityClassPermissions(conn, username, appName));
                return securityClassPermitted(username, appName, permission);
            } catch (SQLException e) {
                throw new ExceptionAdapter(e);
            } finally {
                DbUtils.closeQuietly(conn, null, null);
            }
        }
    }

    @Override
    public boolean isPermitted(String permission) {
        Subject subject = SecurityUtils.getSubject();
        boolean shiroPermitted = subject.isPermitted(permission);
        boolean scPermitted = securityClassPermitted(getUsername(), getAppName(), permission);
        return shiroPermitted & scPermitted;
    }

    @Override
    public void checkPermission(String permission) {
        Subject subject = SecurityUtils.getSubject();
        subject.checkPermission(permission);
        if (!securityClassPermitted(getUsername(), getAppName(), permission))
            throw new AuthorizationException(String.format("user: %s doesn't have the permission %s in app % because of the security class", getUsername(), permission, getAppName()));
    }

    // cor-id:234sdfkhj START
    // code for the below two functions contributed by g_mamais @ 30.IV.2013 via email
    // contract not very clear to me
    @Override
    public boolean vatIsRegisteredInGaia(String vat) throws SQLException {
        Connection conn = dataSource.getConnection();
        try {
            Map<String, Object> userProps = UserMngmtUtil.getUserInfo(conn, vat, null, null);
            return userProps!=null && !userProps.isEmpty();
        } finally {
            DbUtils.closeQuietly(conn, null, null);
        }
    }
    
    @Override
    public boolean vatIsRegisteredInModule(String vat, String appName)
        throws SQLException {
        Connection conn = dataSource.getConnection();
        try {
            Map<String, Object> userProps = UserMngmtUtil.getUserInfo(conn, vat, null, null);
            
            if (userProps==null || userProps.isEmpty() ||
                !userProps.containsKey("user_email"))
                return false;
            String user_mail = (String)userProps.get("user_email");
            Pair<String, List<Triad<String, String, String>>> userApps =
                UserMngmtUtil.getAppsForEmail(conn, user_mail);
            if (userApps == null || userApps.b == null ||
                userApps.b.isEmpty())
                return false;
            for(Triad<String, String, String> app:userApps.b) {
                if (app.a.equals(appName))
                    return true;
            }
            return false;
        } finally {
            DbUtils.closeQuietly(conn, null, null);
        }
    }
    
    
    @Override
    public boolean isUserRegistedInModuleBySpecificSubscriber(String userVat, String moduleName, Integer fromSubscriber)
        throws SQLException {
        Connection conn = dataSource.getConnection();
        try {
            return UserMngmtUtil.isUserRegistedInModuleBySpecificSubscriber(conn, userVat, moduleName, fromSubscriber);
        } finally {
            DbUtils.closeQuietly(conn, null, null);
        }
    }
    
    @Override
    public Integer getFromSubsIdForModule(String moduleName) throws SQLException
    {
        Connection conn = dataSource.getConnection();
        try {
            return UserMngmtUtil.getFromSubsIdForModule(conn, user_id, moduleName);
        } finally {
            DbUtils.closeQuietly(conn, null, null);
        }
        
    }
    
    
    
    
    // cor-id:234sdfkhj END
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
