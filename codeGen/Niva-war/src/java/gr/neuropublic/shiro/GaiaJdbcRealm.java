package gr.neuropublic.shiro;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import javax.faces.context.FacesContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.config.ConfigurationException;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.apache.shiro.util.JdbcUtils;
import org.apache.shiro.SecurityUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.Context;
import javax.naming.NamingException;


import gr.neuropublic.mutil.base.Util;
import gr.neuropublic.mutil.base.ExceptionAdapter;


public class GaiaJdbcRealm extends JdbcRealm {

    private static Logger l = LoggerFactory.getLogger(GaiaJdbcRealm.class);

        
    private String appName ;
    private void ensureAppName() throws NamingException {
        if (this.appName == null) {
            InitialContext ic = new InitialContext();
            Context env = (Context) ic.lookup("java:comp/env");
            appName = (String) env.lookup("appName");
        }
    }

    // we're using a salted realm so we can no longer be using the default ( DEFAULT_AUTHENTICATION_QUERY )

    protected static final String OVERRIDEN_SALTED_AUTHENTICATION_QUERY =
        "SELECT a.user_password, a.user_password_salt FROM subscription.ss_users a WHERE a.user_email = ? AND a.enst_id = 0"; 

    protected static final String OVERRIDEN_USER_ROLES_QUERY = 
        "SELECT a.syro_name FROM subscription.ssv_user_system_roles  a WHERE a.user_email = ? AND a.role_enst_id = 0";


    protected static final String OVERRIDEN_PERMISSIONS_QUERY =
        "SELECT a.sypr_name FROM subscription.ssv_system_roles_privileges a WHERE a.syro_name = ?";


    private void ensureDataSource() {
        if (this.dataSource ==null) {
            UserMngmtDataSource userMngmtDataSource = (UserMngmtDataSource) FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().get("userMngmtDataSource");
            try {
                InitialContext ic = new InitialContext();
                String dataSourceURI = userMngmtDataSource.getDataSourceURI();
                l.debug("looking for the '{}' datasource", dataSourceURI);
                DataSource dataSource = (DataSource) ic.lookup(dataSourceURI); // was: ic.lookup("jdbc/auth");
                l.debug("data source is: "+dataSource);
                this.setDataSource(dataSource);
                this.setSaltStyle(JdbcRealm.SaltStyle.COLUMN);
            } catch (NamingException e) {
                e.printStackTrace();
                throw new RuntimeException(e.getMessage()); // if we can't find the source, we should panic
            }
        }
    }


    public GaiaJdbcRealm() {
        super();
        setAuthenticationQuery(OVERRIDEN_SALTED_AUTHENTICATION_QUERY);
        setUserRolesQuery     (OVERRIDEN_USER_ROLES_QUERY           );
        setPermissionsQuery   (OVERRIDEN_PERMISSIONS_QUERY          );
        //        ensureDataSource(userMngmtDataSource.getDataSourceURI()); // we can't set the data source here as we aren't in FacesContext, hence the need to 
                                                                            // redefine the JdbcRealm methods.
    }

  private void makeNoteOfLogout(String userName, String userIp, String userSessionId) throws SQLException, NamingException {
        Connection conn = null;
        try {
            ensureDataSource();
            ensureAppName();
            conn = dataSource.getConnection();
            String stmtStr = "UPDATE subscription.ss_user_sessions SET usrs_logoutsse=? "+
                             "WHERE usrs_email=? AND usrs_ip=? AND usrs_session=? AND usrs_app=?";

            PreparedStatement pstm  = conn.prepareStatement(stmtStr);

            pstm.setInt        (1, Util.secondsSinceTheEpoch());
            pstm.setString     (2, userName);
            pstm.setString     (3, userIp);
            pstm.setString     (4, userSessionId);
            pstm.setString     (5, appName);
            pstm.executeUpdate();
        } finally {
            conn.close();
        }        
    }

    /****************************************************************/

    public void onLogout(PrincipalCollection principals) {
        try {
            {
                l.info("onLogout called");
                List principalsL = principals.asList();
                for (Object principal : principalsL)
                l.info("**************** principal : {}", principal.toString());
                l.info("**************** host: {}", SecurityUtils.getSubject().getSession().getHost());
                l.info("**************** session: {}", SecurityUtils.getSubject().getSession().getId().toString());
            }
            super.onLogout(principals);
            makeNoteOfLogout((String) principals.getPrimaryPrincipal(),
                                      SecurityUtils.getSubject().getSession().getHost(),
                                      SecurityUtils.getSubject().getSession().getId().toString());
                         

        } catch (Exception e) {
            throw new ExceptionAdapter(e);
        }
    }


    /****************************************************************/


    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        l.info("GaiaJdbcRealm doGetAuthenticationInfo");
        int i = 0; if (i==0) throw new RuntimeException();
        ensureDataSource();

        UsernamePasswordToken upToken = (UsernamePasswordToken) token;
        String username = upToken.getUsername();

        // Null username is invalid
        if (username == null) {
            throw new AccountException("Null usernames are not allowed by this realm.");
        }

        Connection conn = null;
        SimpleAuthenticationInfo info = null;
        try {
            conn = dataSource.getConnection();

            String password = null;
            String salt = null;
            switch (saltStyle) {
            case NO_SALT:
                password = getPasswordForUser(conn, username)[0];
                break;
            case CRYPT:
                // TODO: separate password and hash from getPasswordForUser[0]
                throw new ConfigurationException("Not implemented yet");
                //break;
            case COLUMN:
                String[] queryResults = getPasswordForUser(conn, username);
                password = queryResults[0];
                salt = queryResults[1];
                break;
            case EXTERNAL:
                password = getPasswordForUser(conn, username)[0];
                salt = getSaltForUser(username);
            }

            if (password == null) {
                throw new UnknownAccountException("No account found for user [" + username + "]");
            }

            info = new SimpleAuthenticationInfo(username, password.toCharArray(), getName());
            
            if (salt != null) {
                info.setCredentialsSalt(ByteSource.Util.bytes(salt));
            }

        } catch (SQLException e) {
            final String message = "There was a SQL error while authenticating user [" + username + "]";
            if (l.isErrorEnabled()) {
                l.error(message, e);
            }

            // Rethrow any SQL errors as an authentication exception
            throw new AuthenticationException(message, e);
        } finally {
            JdbcUtils.closeConnection(conn);
        }

        return info;
    }

    private String[] getPasswordForUser(Connection conn, String username) throws SQLException {
        ensureDataSource();
        String[] result;
        boolean returningSeparatedSalt = false;
        switch (saltStyle) {
        case NO_SALT:
        case CRYPT:
        case EXTERNAL:
            result = new String[1];
            break;
        default:
            result = new String[2];
            returningSeparatedSalt = true;
        }
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(authenticationQuery);
            ps.setString(1, username);

            // Execute query
            rs = ps.executeQuery();

            // Loop over results - although we are only expecting one result, since usernames should be unique
            boolean foundResult = false;
            while (rs.next()) {

                // Check to ensure only one row is processed
                if (foundResult) {
                    throw new AuthenticationException("More than one user row found for user [" + username + "]. Usernames must be unique.");
                }

                result[0] = rs.getString(1);
                if (returningSeparatedSalt) {
                    result[1] = rs.getString(2);
                }

                foundResult = true;
            }
        } finally {
            JdbcUtils.closeResultSet(rs);
            JdbcUtils.closeStatement(ps);
        }

        return result;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        ensureDataSource();
        //null usernames are invalid
        if (principals == null) {
            throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
        }

        String username = (String) getAvailablePrincipal(principals);

        Connection conn = null;
        Set<String> roleNames = null;
        Set<String> permissions = null;
        try {
            conn = dataSource.getConnection();

            // Retrieve roles and permissions from database
            roleNames = getRoleNamesForUser(conn, username);
            if (permissionsLookupEnabled) {
                permissions = getPermissions(conn, username, roleNames);
            }

        } catch (SQLException e) {
            final String message = "There was a SQL error while authorizing user [" + username + "]";
            if (l.isErrorEnabled()) {
                l.error(message, e);
            }

            // Rethrow any SQL errors as an authorization exception
            throw new AuthorizationException(message, e);
        } finally {
            JdbcUtils.closeConnection(conn);
        }

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo(roleNames);
        info.setStringPermissions(permissions);
        return info;

    }

    protected Set<String> getRoleNamesForUser(Connection conn, String username) throws SQLException {
        ensureDataSource();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Set<String> roleNames = new LinkedHashSet<String>();
        try {
            ps = conn.prepareStatement(userRolesQuery);
            ps.setString(1, username);

            // Execute query
            rs = ps.executeQuery();

            // Loop over results and add each returned role to a set
            while (rs.next()) {

                String roleName = rs.getString(1);

                // Add the role to the list of names if it isn't null
                if (roleName != null) {
                    roleNames.add(roleName);
                } else {
                    if (l.isWarnEnabled()) {
                        l.warn("Null role name found while retrieving role names for user [" + username + "]");
                    }
                }
            }
        } finally {
            JdbcUtils.closeResultSet(rs);
            JdbcUtils.closeStatement(ps);
        }
        return roleNames;
    }

    protected Set<String> getPermissions(Connection conn, String username, Collection<String> roleNames) throws SQLException {
        ensureDataSource();
        PreparedStatement ps = null;
        Set<String> permissions = new LinkedHashSet<String>();
        try {
            ps = conn.prepareStatement(permissionsQuery);
            for (String roleName : roleNames) {

                ps.setString(1, roleName);

                ResultSet rs = null;

                try {
                    // Execute query
                    rs = ps.executeQuery();

                    // Loop over results and add each returned role to a set
                    while (rs.next()) {

                        String permissionString = rs.getString(1);

                        // Add the permission to the set of permissions
                        permissions.add(permissionString);
                    }
                } finally {
                    JdbcUtils.closeResultSet(rs);
                }

            }
        } finally {
            JdbcUtils.closeStatement(ps);
        }

        return permissions;
    }

}

 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
