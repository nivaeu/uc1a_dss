package gr.neuropublic.Niva.wsRestFacadesBase;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import gr.neuropublic.shiro.ILoginController;
import java.util.List;
import java.util.ArrayList;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Context;
import javax.servlet.ServletContext;
import gr.neuropublic.exceptions.GenericApplicationException;
import gr.neuropublic.wsrestutils.AbstractWebServiceHandler;
import gr.neuropublic.functional.Func1;
import gr.neuropublic.Niva.services.IUserManagementService;
import gr.neuropublic.mutil.base.Pair;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.core.MediaType;
import gr.neuropublic.Niva.facades.IParcelClasFacade;
import gr.neuropublic.Niva.entities.ParcelClas;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import gr.neuropublic.Niva.facades.ITmpBlobFacade;
import gr.neuropublic.Niva.entities.TmpBlob;
import gr.neuropublic.wsrestutils.LazyFieldExclusionStrategy;
import gr.neuropublic.base.DateFormat;
import javax.ws.rs.CookieParam;
import javax.ws.rs.core.NewCookie;
import gr.neuropublic.Niva.services.ISessionsCacheService;
import gr.neuropublic.base.JsonBlob;
import gr.neuropublic.base.JsonSingleValue;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.jboss.resteasy.annotations.GZIP;
import javax.ws.rs.core.MultivaluedMap;
import java.io.InputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Set;
import org.apache.commons.io.IOUtils;
import org.slf4j.LoggerFactory;

import gr.neuropublic.base.GridColDefinition;

public  class ParcelClasWsFacadeBase  extends AbstractWebServiceHandler {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(gr.neuropublic.base.IAbstractService.class);

    @EJB
    private IParcelClasFacade.ILocal parcelClasFacade;
    public IParcelClasFacade.ILocal getParcelClasFacade() {
        return parcelClasFacade;
    }
    public void setParcelClasFacade(IParcelClasFacade.ILocal val) {
        parcelClasFacade = val;
    }

    @EJB
    private IUserManagementService.ILocal usrMngSrv;
    @Override
    public IUserManagementService.ILocal getUserManagementService() {
        return usrMngSrv;
    }
    public void setUserManagementService(IUserManagementService.ILocal val) {
        usrMngSrv = val;
    }

    @EJB
    private ISessionsCacheService.ILocal sessionsCache;
    @Override
    public ISessionsCacheService.ILocal getSessionsCache() {
        return sessionsCache;
    }
    public void setSessionsCache(ISessionsCacheService.ILocal sessionsCache) {
        this.sessionsCache = sessionsCache;
    }


    @EJB
    private ITmpBlobFacade.ILocal tmpBlobFacade;
    public ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }

    @EJB
    private gr.neuropublic.Niva.services.IMainService.ILocal service;

    public gr.neuropublic.Niva.services.IMainService.ILocal getService()
    {
        return service;
    }
    public void setService(gr.neuropublic.Niva.services.IMainService.ILocal service)
    {
        this.service = service;
    }

    @Override
    public void initializeDatabaseSession(gr.neuropublic.base.UserSession usrSession) {
    }




    public static class FindAllByIds_request {
        public String[] ids; 
    }

    @POST
    @Path("/findAllByIds")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByIds(
        final FindAllByIds_request r, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            gr.neuropublic.wsrestutils.LazyData ret = findAllByIds_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    public gr.neuropublic.wsrestutils.LazyData findAllByIds_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindAllByIds_request r) {
            
        ArrayList<Integer> ids = new ArrayList<Integer>();
        for(String enc_id: r.ids) {
            Integer id = (enc_id != null ? usrSession.DecryptInteger(enc_id) : null);
            if (id != null)
                ids.add(id);
        } 
        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        List<ParcelClas> entities = getParcelClasFacade().findAllByIds(ids);
        ret.data = entities;
        ret.count = entities.size();
        return ret;
    }


    public static class FindByPclaId_request {
        public String pclaId; 
    }

    @POST
    @Path("/findByPclaId")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findByPclaId(
        final FindByPclaId_request r, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            gr.neuropublic.wsrestutils.LazyData ret = findByPclaId_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    public gr.neuropublic.wsrestutils.LazyData findByPclaId_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindByPclaId_request r) {
            
        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        java.util.ArrayList<ParcelClas> list = new java.util.ArrayList<ParcelClas>();
        ParcelClas c = getParcelClasFacade().findByPclaId((r.pclaId != null ? usrSession.DecryptInteger(r.pclaId) : null));
        if (c!=null)
            list.add(c); 
        ret.data = list;
        ret.count = list.size();
        return ret;
    }


    public static class FindLazyParcelGP_request {
        public Integer prodCode;
        public String parcIdentifier;
        public String parcCode;
        public String clasId_clasId;
        public Integer fromRowIndex;
        public Integer toRowIndex;
        public String sortField;
        public Boolean sortOrder;
        public List<String> exc_Id;
        public List<GridColDefinition> __fields;
        public gr.neuropublic.wsrestutils.LazyData.requestType __dataReqType;
    }

    @POST
    @Path("/findLazyParcelGP")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findLazyParcelGP(
        final FindLazyParcelGP_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.DATA;
            gr.neuropublic.wsrestutils.LazyData ret = findLazyParcelGP_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    @POST
    @Path("/findLazyParcelGP_count")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findLazyParcelGP_count(
        final FindLazyParcelGP_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.COUNT;
            gr.neuropublic.wsrestutils.LazyData ret = findLazyParcelGP_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }
    public gr.neuropublic.wsrestutils.LazyData findLazyParcelGP_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindLazyParcelGP_request r) {
        if (!usrSession.privileges.contains("Niva_ParcelGP_R"))
            throw new GenericApplicationException("NO_READ_ACCESS");
        //Thread.sleep(1500); // debugging waiting dialogs
        int[] range = new int[] {r.fromRowIndex != null ? r.fromRowIndex : 0, r.toRowIndex != null ? r.toRowIndex : 10};
        int[] recordCount = new int [] {0};
        List<String> excludedIds = new ArrayList<String>();
        if (r != null && r.exc_Id != null && !r.exc_Id.isEmpty()) {
            for(String encrypted_id:r.exc_Id) {
                if (encrypted_id == null)
                    continue;
                String descryptedId = usrSession.DecryptInteger(encrypted_id).toString();
                excludedIds.add(descryptedId);
            }
        }

        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        if (r.__dataReqType == null || r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.DATA) {
            List<ParcelClas> retEntities= getParcelClasFacade().findLazyParcelGP(r.prodCode, r.parcIdentifier, r.parcCode, (r.clasId_clasId != null && !r.clasId_clasId.startsWith("TEMP_ID_") ? (r.clasId_clasId != null ? usrSession.DecryptInteger(r.clasId_clasId) : null) : null), range, recordCount, r.sortField, r.sortOrder != null ? r.sortOrder : false, excludedIds);
            ret.data = retEntities;
            ret.count = recordCount[0];
        } else if (r.__dataReqType != null && r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.COUNT) {
            ret.data = new ArrayList<>();
            ret.count = getParcelClasFacade().findLazyParcelGP_count(r.prodCode, r.parcIdentifier, r.parcCode, (r.clasId_clasId != null && !r.clasId_clasId.startsWith("TEMP_ID_") ? (r.clasId_clasId != null ? usrSession.DecryptInteger(r.clasId_clasId) : null) : null), excludedIds);
        } else {
            throw new GenericApplicationException("Unknown_DataRequestType");
        }
        return ret;
    }


    public static class FindLazyDashboard_request {
        public Integer prodCode;
        public String parcIdentifier;
        public String parcCode;
        public String clasId_clasId;
        public Integer fromRowIndex;
        public Integer toRowIndex;
        public String sortField;
        public Boolean sortOrder;
        public List<String> exc_Id;
        public List<GridColDefinition> __fields;
        public gr.neuropublic.wsrestutils.LazyData.requestType __dataReqType;
    }

    @POST
    @Path("/findLazyDashboard")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findLazyDashboard(
        final FindLazyDashboard_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.DATA;
            gr.neuropublic.wsrestutils.LazyData ret = findLazyDashboard_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    @POST
    @Path("/findLazyDashboard_count")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findLazyDashboard_count(
        final FindLazyDashboard_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.COUNT;
            gr.neuropublic.wsrestutils.LazyData ret = findLazyDashboard_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }
    public gr.neuropublic.wsrestutils.LazyData findLazyDashboard_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindLazyDashboard_request r) {
        if (!usrSession.privileges.contains("Niva_Dashboard_R"))
            throw new GenericApplicationException("NO_READ_ACCESS");
        //Thread.sleep(1500); // debugging waiting dialogs
        int[] range = new int[] {r.fromRowIndex != null ? r.fromRowIndex : 0, r.toRowIndex != null ? r.toRowIndex : 10};
        int[] recordCount = new int [] {0};
        List<String> excludedIds = new ArrayList<String>();
        if (r != null && r.exc_Id != null && !r.exc_Id.isEmpty()) {
            for(String encrypted_id:r.exc_Id) {
                if (encrypted_id == null)
                    continue;
                String descryptedId = usrSession.DecryptInteger(encrypted_id).toString();
                excludedIds.add(descryptedId);
            }
        }

        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        if (r.__dataReqType == null || r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.DATA) {
            List<ParcelClas> retEntities= getParcelClasFacade().findLazyDashboard(r.prodCode, r.parcIdentifier, r.parcCode, (r.clasId_clasId != null && !r.clasId_clasId.startsWith("TEMP_ID_") ? (r.clasId_clasId != null ? usrSession.DecryptInteger(r.clasId_clasId) : null) : null), range, recordCount, r.sortField, r.sortOrder != null ? r.sortOrder : false, excludedIds);
            ret.data = retEntities;
            ret.count = recordCount[0];
        } else if (r.__dataReqType != null && r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.COUNT) {
            ret.data = new ArrayList<>();
            ret.count = getParcelClasFacade().findLazyDashboard_count(r.prodCode, r.parcIdentifier, r.parcCode, (r.clasId_clasId != null && !r.clasId_clasId.startsWith("TEMP_ID_") ? (r.clasId_clasId != null ? usrSession.DecryptInteger(r.clasId_clasId) : null) : null), excludedIds);
        } else {
            throw new GenericApplicationException("Unknown_DataRequestType");
        }
        return ret;
    }


    public static class FindLazyParcelFMIS_request {
        public Integer prodCode;
        public String parcIdentifier;
        public String parcCode;
        public String clasId_clasId;
        public Integer fromRowIndex;
        public Integer toRowIndex;
        public String sortField;
        public Boolean sortOrder;
        public List<String> exc_Id;
        public List<GridColDefinition> __fields;
        public gr.neuropublic.wsrestutils.LazyData.requestType __dataReqType;
    }

    @POST
    @Path("/findLazyParcelFMIS")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findLazyParcelFMIS(
        final FindLazyParcelFMIS_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.DATA;
            gr.neuropublic.wsrestutils.LazyData ret = findLazyParcelFMIS_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    @POST
    @Path("/findLazyParcelFMIS_count")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findLazyParcelFMIS_count(
        final FindLazyParcelFMIS_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.COUNT;
            gr.neuropublic.wsrestutils.LazyData ret = findLazyParcelFMIS_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }
    public gr.neuropublic.wsrestutils.LazyData findLazyParcelFMIS_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindLazyParcelFMIS_request r) {
        if (!usrSession.privileges.contains("Niva_ParcelFMIS_R"))
            throw new GenericApplicationException("NO_READ_ACCESS");
        //Thread.sleep(1500); // debugging waiting dialogs
        int[] range = new int[] {r.fromRowIndex != null ? r.fromRowIndex : 0, r.toRowIndex != null ? r.toRowIndex : 10};
        int[] recordCount = new int [] {0};
        List<String> excludedIds = new ArrayList<String>();
        if (r != null && r.exc_Id != null && !r.exc_Id.isEmpty()) {
            for(String encrypted_id:r.exc_Id) {
                if (encrypted_id == null)
                    continue;
                String descryptedId = usrSession.DecryptInteger(encrypted_id).toString();
                excludedIds.add(descryptedId);
            }
        }

        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        if (r.__dataReqType == null || r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.DATA) {
            List<ParcelClas> retEntities= getParcelClasFacade().findLazyParcelFMIS(r.prodCode, r.parcIdentifier, r.parcCode, (r.clasId_clasId != null && !r.clasId_clasId.startsWith("TEMP_ID_") ? (r.clasId_clasId != null ? usrSession.DecryptInteger(r.clasId_clasId) : null) : null), range, recordCount, r.sortField, r.sortOrder != null ? r.sortOrder : false, excludedIds);
            ret.data = retEntities;
            ret.count = recordCount[0];
        } else if (r.__dataReqType != null && r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.COUNT) {
            ret.data = new ArrayList<>();
            ret.count = getParcelClasFacade().findLazyParcelFMIS_count(r.prodCode, r.parcIdentifier, r.parcCode, (r.clasId_clasId != null && !r.clasId_clasId.startsWith("TEMP_ID_") ? (r.clasId_clasId != null ? usrSession.DecryptInteger(r.clasId_clasId) : null) : null), excludedIds);
        } else {
            throw new GenericApplicationException("Unknown_DataRequestType");
        }
        return ret;
    }


    public static class FindAllByCriteriaRange_ClassificationGrpParcelClas_request {
        public String clasId_clasId;
        public String fsch_parcIdentifier;
        public String fsch_parcCode;
        public Integer fsch_prodCode;
        public Integer fromRowIndex;
        public Integer toRowIndex;
        public String sortField;
        public Boolean sortOrder;
        public List<String> exc_Id;
        public List<GridColDefinition> __fields;
        public gr.neuropublic.wsrestutils.LazyData.requestType __dataReqType;
    }

    @POST
    @Path("/findAllByCriteriaRange_ClassificationGrpParcelClas")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_ClassificationGrpParcelClas(
        final FindAllByCriteriaRange_ClassificationGrpParcelClas_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.DATA;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_ClassificationGrpParcelClas_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    @POST
    @Path("/findAllByCriteriaRange_ClassificationGrpParcelClas_count")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_ClassificationGrpParcelClas_count(
        final FindAllByCriteriaRange_ClassificationGrpParcelClas_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.COUNT;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_ClassificationGrpParcelClas_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }
    public gr.neuropublic.wsrestutils.LazyData findAllByCriteriaRange_ClassificationGrpParcelClas_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindAllByCriteriaRange_ClassificationGrpParcelClas_request r) {
        if (!usrSession.privileges.contains("Niva_Classification_R"))
            throw new GenericApplicationException("NO_READ_ACCESS");
        //Thread.sleep(1500); // debugging waiting dialogs
        int[] range = new int[] {r.fromRowIndex != null ? r.fromRowIndex : 0, r.toRowIndex != null ? r.toRowIndex : 10};
        int[] recordCount = new int [] {0};
        List<String> excludedIds = new ArrayList<String>();
        if (r != null && r.exc_Id != null && !r.exc_Id.isEmpty()) {
            for(String encrypted_id:r.exc_Id) {
                if (encrypted_id == null)
                    continue;
                String descryptedId = usrSession.DecryptInteger(encrypted_id).toString();
                excludedIds.add(descryptedId);
            }
        }

        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        if (r.__dataReqType == null || r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.DATA) {
            List<ParcelClas> retEntities= getParcelClasFacade().findAllByCriteriaRange_ClassificationGrpParcelClas((r.clasId_clasId != null && !r.clasId_clasId.startsWith("TEMP_ID_") ? (r.clasId_clasId != null ? usrSession.DecryptInteger(r.clasId_clasId) : null) : null), r.fsch_parcIdentifier, r.fsch_parcCode, r.fsch_prodCode, range, recordCount, r.sortField, r.sortOrder != null ? r.sortOrder : false, excludedIds);
            ret.data = retEntities;
            ret.count = recordCount[0];
        } else if (r.__dataReqType != null && r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.COUNT) {
            ret.data = new ArrayList<>();
            ret.count = getParcelClasFacade().findAllByCriteriaRange_ClassificationGrpParcelClas_count((r.clasId_clasId != null && !r.clasId_clasId.startsWith("TEMP_ID_") ? (r.clasId_clasId != null ? usrSession.DecryptInteger(r.clasId_clasId) : null) : null), r.fsch_parcIdentifier, r.fsch_parcCode, r.fsch_prodCode, excludedIds);
        } else {
            throw new GenericApplicationException("Unknown_DataRequestType");
        }
        return ret;
    }


    public static class FindAllByCriteriaRange_forLov_request {
        public Double fsch_ParcelClasLov_probPred;
        public Double fsch_ParcelClasLov_probPred2;
        public Integer fsch_ParcelClasLov_prodCode;
        public String fsch_ParcelClasLov_parcIdentifier;
        public Integer fromRowIndex;
        public Integer toRowIndex;
        public String sortField;
        public Boolean sortOrder;
        public List<String> exc_Id;
        public List<GridColDefinition> __fields;
        public gr.neuropublic.wsrestutils.LazyData.requestType __dataReqType;
    }

    @POST
    @Path("/findAllByCriteriaRange_forLov")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_forLov(
        final FindAllByCriteriaRange_forLov_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.DATA;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_forLov_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    @POST
    @Path("/findAllByCriteriaRange_forLov_count")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_forLov_count(
        final FindAllByCriteriaRange_forLov_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.COUNT;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_forLov_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }
    public gr.neuropublic.wsrestutils.LazyData findAllByCriteriaRange_forLov_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindAllByCriteriaRange_forLov_request r) {
        //Thread.sleep(1500); // debugging waiting dialogs
        int[] range = new int[] {r.fromRowIndex != null ? r.fromRowIndex : 0, r.toRowIndex != null ? r.toRowIndex : 10};
        int[] recordCount = new int [] {0};
        List<String> excludedIds = new ArrayList<String>();
        if (r != null && r.exc_Id != null && !r.exc_Id.isEmpty()) {
            for(String encrypted_id:r.exc_Id) {
                if (encrypted_id == null)
                    continue;
                String descryptedId = usrSession.DecryptInteger(encrypted_id).toString();
                excludedIds.add(descryptedId);
            }
        }

        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        if (r.__dataReqType == null || r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.DATA) {
            List<ParcelClas> retEntities= getParcelClasFacade().findAllByCriteriaRange_forLov(r.fsch_ParcelClasLov_probPred, r.fsch_ParcelClasLov_probPred2, r.fsch_ParcelClasLov_prodCode, r.fsch_ParcelClasLov_parcIdentifier, range, recordCount, r.sortField, r.sortOrder != null ? r.sortOrder : false, excludedIds);
            ret.data = retEntities;
            ret.count = recordCount[0];
        } else if (r.__dataReqType != null && r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.COUNT) {
            ret.data = new ArrayList<>();
            ret.count = getParcelClasFacade().findAllByCriteriaRange_forLov_count(r.fsch_ParcelClasLov_probPred, r.fsch_ParcelClasLov_probPred2, r.fsch_ParcelClasLov_prodCode, r.fsch_ParcelClasLov_parcIdentifier, excludedIds);
        } else {
            throw new GenericApplicationException("Unknown_DataRequestType");
        }
        return ret;
    }


    @DELETE
    @Path("/delByPclaId")
    public Response delByPclaId(
        @QueryParam("pclaId") final String pclaId, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            ParcelClas parcelClas = getParcelClasFacade().findByPclaId((pclaId != null ? usrSession.DecryptInteger(pclaId) : null));
            getService().removeEntity(usrSession.usrEmail, parcelClas);
        //    Integer c = getParcelClasFacade().findByPclaId((pclaId != null ? usrSession.DecryptInteger(pclaId) : null));
            return Response.status(200).type("application/json;charset=UTF-8").entity("").build();
        }});
    }

    public  Response handleWebRequest(String sessionId, final Func1<gr.neuropublic.Niva.services.UserSession, Response> javaClosure) {
        return this.handleWebRequest(sessionId, null, new Pair<String,String>(null, null), javaClosure);
    }
    public  Response handleWebRequest(String sessionId, String ssoSessionId, Pair<String,String> subscriberCookie, final Func1<gr.neuropublic.Niva.services.UserSession, Response> javaClosure) {
        Func1<gr.neuropublic.base.UserSession, Response> javaClosureCasted = new Func1<gr.neuropublic.base.UserSession, Response>() {
            @Override
            public Response lambda(gr.neuropublic.base.UserSession t1) {
                return javaClosure.lambda((gr.neuropublic.Niva.services.UserSession)t1);
            }
            
        };
    
        return super.handleWebRequest(sessionId, ssoSessionId, subscriberCookie, "niva-session-id", "/Niva/", javaClosureCasted);
    }
}