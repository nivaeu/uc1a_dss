package gr.neuropublic.Niva.wsRestFacadesBase;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import gr.neuropublic.shiro.ILoginController;
import java.util.List;
import java.util.ArrayList;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Context;
import javax.servlet.ServletContext;
import gr.neuropublic.exceptions.GenericApplicationException;
import gr.neuropublic.wsrestutils.AbstractWebServiceHandler;
import gr.neuropublic.functional.Func1;
import gr.neuropublic.Niva.services.IUserManagementService;
import gr.neuropublic.mutil.base.Pair;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.core.MediaType;
import gr.neuropublic.Niva.facades.IExcelErrorFacade;
import gr.neuropublic.Niva.entities.ExcelError;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import gr.neuropublic.Niva.facades.ITmpBlobFacade;
import gr.neuropublic.Niva.entities.TmpBlob;
import gr.neuropublic.wsrestutils.LazyFieldExclusionStrategy;
import gr.neuropublic.base.DateFormat;
import javax.ws.rs.CookieParam;
import javax.ws.rs.core.NewCookie;
import gr.neuropublic.Niva.services.ISessionsCacheService;
import gr.neuropublic.base.JsonBlob;
import gr.neuropublic.base.JsonSingleValue;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.jboss.resteasy.annotations.GZIP;
import javax.ws.rs.core.MultivaluedMap;
import java.io.InputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Set;
import org.apache.commons.io.IOUtils;
import org.slf4j.LoggerFactory;

import gr.neuropublic.base.GridColDefinition;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.DataFormat;

public  class ExcelErrorWsFacadeBase  extends AbstractWebServiceHandler {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(gr.neuropublic.base.IAbstractService.class);

    @EJB
    private IExcelErrorFacade.ILocal excelErrorFacade;
    public IExcelErrorFacade.ILocal getExcelErrorFacade() {
        return excelErrorFacade;
    }
    public void setExcelErrorFacade(IExcelErrorFacade.ILocal val) {
        excelErrorFacade = val;
    }

    @EJB
    private IUserManagementService.ILocal usrMngSrv;
    @Override
    public IUserManagementService.ILocal getUserManagementService() {
        return usrMngSrv;
    }
    public void setUserManagementService(IUserManagementService.ILocal val) {
        usrMngSrv = val;
    }

    @EJB
    private ISessionsCacheService.ILocal sessionsCache;
    @Override
    public ISessionsCacheService.ILocal getSessionsCache() {
        return sessionsCache;
    }
    public void setSessionsCache(ISessionsCacheService.ILocal sessionsCache) {
        this.sessionsCache = sessionsCache;
    }


    @EJB
    private ITmpBlobFacade.ILocal tmpBlobFacade;
    public ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }

    @EJB
    private gr.neuropublic.Niva.services.IMainService.ILocal service;

    public gr.neuropublic.Niva.services.IMainService.ILocal getService()
    {
        return service;
    }
    public void setService(gr.neuropublic.Niva.services.IMainService.ILocal service)
    {
        this.service = service;
    }

    @Override
    public void initializeDatabaseSession(gr.neuropublic.base.UserSession usrSession) {
    }




    public static class FindAllByIds_request {
        public String[] ids; 
    }

    @POST
    @Path("/findAllByIds")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByIds(
        final FindAllByIds_request r, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            gr.neuropublic.wsrestutils.LazyData ret = findAllByIds_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    public gr.neuropublic.wsrestutils.LazyData findAllByIds_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindAllByIds_request r) {
            
        ArrayList<Integer> ids = new ArrayList<Integer>();
        for(String enc_id: r.ids) {
            Integer id = (enc_id != null ? usrSession.DecryptInteger(enc_id) : null);
            if (id != null)
                ids.add(id);
        } 
        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        List<ExcelError> entities = getExcelErrorFacade().findAllByIds(ids);
        ret.data = entities;
        ret.count = entities.size();
        return ret;
    }


    public static class FindById_request {
        public String id; 
    }

    @POST
    @Path("/findById")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findById(
        final FindById_request r, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            gr.neuropublic.wsrestutils.LazyData ret = findById_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    public gr.neuropublic.wsrestutils.LazyData findById_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindById_request r) {
            
        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        java.util.ArrayList<ExcelError> list = new java.util.ArrayList<ExcelError>();
        ExcelError c = getExcelErrorFacade().findById((r.id != null ? usrSession.DecryptInteger(r.id) : null));
        if (c!=null)
            list.add(c); 
        ret.data = list;
        ret.count = list.size();
        return ret;
    }

    public static class FindAllByCriteriaRange_ExcelFileImportResultsGrpExcelError_request {
        public String exfiId_id;
        public Integer fsch_row;
        public String fsch_errMessage;
        public Integer fromRowIndex;
        public Integer toRowIndex;
        public String sortField;
        public Boolean sortOrder;
        public List<String> exc_Id;
        public List<String> __fields;
        public gr.neuropublic.wsrestutils.LazyData.requestType __dataReqType;
    }

    @POST
    @Path("/findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError(
        final FindAllByCriteriaRange_ExcelFileImportResultsGrpExcelError_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.DATA;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    @POST
    @Path("/findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError_count")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError_count(
        final FindAllByCriteriaRange_ExcelFileImportResultsGrpExcelError_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.COUNT;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }
    public gr.neuropublic.wsrestutils.LazyData findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindAllByCriteriaRange_ExcelFileImportResultsGrpExcelError_request r) {
        int[] range = new int[] {r.fromRowIndex != null ? r.fromRowIndex : 0, r.toRowIndex != null ? r.toRowIndex : 10};
        int[] recordCount = new int [] {0};
        List<String> excludedIds = new ArrayList<String>();
        if (r != null && r.exc_Id != null && !r.exc_Id.isEmpty()) {
            for(String encrypted_id:r.exc_Id) {
                if (encrypted_id == null)
                    continue;
                String descryptedId = usrSession.DecryptInteger(encrypted_id).toString();
                excludedIds.add(descryptedId);
            }
        }

        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        if (r.__dataReqType == null || r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.DATA) {
            List<ExcelError> retEntities= getExcelErrorFacade().findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError((r.exfiId_id != null && !r.exfiId_id.startsWith("TEMP_ID_") ? (r.exfiId_id != null ? usrSession.DecryptInteger(r.exfiId_id) : null) : null), r.fsch_row, r.fsch_errMessage, range, recordCount, r.sortField, r.sortOrder != null ? r.sortOrder : false, excludedIds);
            ret.data = retEntities;
            ret.count = recordCount[0];
        } else if (r.__dataReqType != null && r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.COUNT) {
            ret.data = new ArrayList<>();
            ret.count = getExcelErrorFacade().findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError_count((r.exfiId_id != null && !r.exfiId_id.startsWith("TEMP_ID_") ? (r.exfiId_id != null ? usrSession.DecryptInteger(r.exfiId_id) : null) : null), r.fsch_row, r.fsch_errMessage, excludedIds);
        } else {
            throw new GenericApplicationException("Unknown_DataRequestType");
        }
        return ret;
    }


    @POST
    @Path("/findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError_toExcel")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError_toExcel(
        final FindAllByCriteriaRange_ExcelFileImportResultsGrpExcelError_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            int maxRetEntities = 100000;
            int[] range = new int[] {0, maxRetEntities};
            int[] recordCount = new int [] {0};
            List<String> excludedIds = new ArrayList<String>();
            if (r != null && r.exc_Id != null && !r.exc_Id.isEmpty()) {
                for(String encrypted_id:r.exc_Id) {
                    if (encrypted_id == null)
                        continue;
                    String descryptedId = usrSession.DecryptInteger(encrypted_id).toString();
                    excludedIds.add(descryptedId);
                }
            }

            //Determine Excel Columns
            String[] availableFields = {"excelRowNum", "errMessage"};
            List<String> fieldsRequested = r.__fields;
                    
            List<String> visibleColumns = new ArrayList<>();
            for (String field : availableFields){
                if ( (fieldsRequested == null || fieldsRequested.contains(field)) )
                    visibleColumns.add(field);
            }
            if (visibleColumns.isEmpty())
                throw new GenericApplicationException("EXCEL HAS NO COLUMNS"); 

            //Get Entities
            List<ExcelError> retEntities= getExcelErrorFacade().findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError(true, (r.exfiId_id != null && !r.exfiId_id.startsWith("TEMP_ID_") ? (r.exfiId_id != null ? usrSession.DecryptInteger(r.exfiId_id) : null) : null), r.fsch_row, r.fsch_errMessage, range, recordCount, r.sortField, r.sortOrder != null ? r.sortOrder : false, excludedIds);

            //create excel
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet worksheet = workbook.createSheet("Sheet1");

            // add headers
            HSSFRow headersRow = worksheet.createRow(0);
            HSSFCellStyle cellStyleHeader = workbook.createCellStyle();
            cellStyleHeader.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            cellStyleHeader.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

            HSSFCell cell;
            int cellIndex = 0;
            if (visibleColumns.contains("excelRowNum")){
                cell = headersRow.createCell(cellIndex);
                cell.setCellValue("excelRowNum");
                cell.setCellStyle(cellStyleHeader);
                worksheet.autoSizeColumn(cellIndex);
                cellIndex++;
            }

            if (visibleColumns.contains("errMessage")){
                cell = headersRow.createCell(cellIndex);
                cell.setCellValue("errMessage");
                cell.setCellStyle(cellStyleHeader);
                worksheet.autoSizeColumn(cellIndex);
                cellIndex++;
            }


            // add entities
            HSSFCellStyle cellStyle = workbook.createCellStyle();
            HSSFCellStyle cellStyleDate = workbook.createCellStyle();
            DataFormat formatForDate = workbook.createDataFormat();
            cellStyleDate.setDataFormat(formatForDate.getFormat("dd/mm/yyyy"));

            int length = retEntities.size();
            if (length>0) {
                for(int i=0;i<length; i++) {
                    HSSFRow entityRow = worksheet.createRow(i+1);

                    cellIndex = 0;
                    if (visibleColumns.contains("excelRowNum")) {
                        cell = entityRow.createCell(cellIndex);
                        if (retEntities.get(i).getExcelRowNum()!=null)
                            cell.setCellValue(retEntities.get(i).getExcelRowNum());
                        cell.setCellStyle(cellStyle);

                        cellIndex++;
                    }

                    if (visibleColumns.contains("errMessage")) {
                        cell = entityRow.createCell(cellIndex);
                        if (retEntities.get(i).getErrMessage()!=null)
                            cell.setCellValue(retEntities.get(i).getErrMessage());
                        cell.setCellStyle(cellStyle);

                        cellIndex++;
                    }
                            
                }
            }

            // ByteArrayOutput
            java.io.ByteArrayOutputStream strm = new java.io.ByteArrayOutputStream();
            try {
                workbook.write(strm);
            } catch (IOException e) {
                logger.error("IOException", e);
                throw new RuntimeException(e);
            }
                            
            JsonBlob rsp = new JsonBlob();
            rsp.data = strm.toByteArray();
                    
            return Response.status(200).type("application/json;charset=UTF-8").entity(rsp).build();
        }});
    }


    @DELETE
    @Path("/delById")
    public Response delById(
        @QueryParam("id") final String id, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            ExcelError excelError = getExcelErrorFacade().findById((id != null ? usrSession.DecryptInteger(id) : null));
            getService().removeEntity(usrSession.usrEmail, excelError);
        //    Integer c = getExcelErrorFacade().findById((id != null ? usrSession.DecryptInteger(id) : null));
            return Response.status(200).type("application/json;charset=UTF-8").entity("").build();
        }});
    }

    public  Response handleWebRequest(String sessionId, final Func1<gr.neuropublic.Niva.services.UserSession, Response> javaClosure) {
        return this.handleWebRequest(sessionId, null, new Pair<String,String>(null, null), javaClosure);
    }
    public  Response handleWebRequest(String sessionId, String ssoSessionId, Pair<String,String> subscriberCookie, final Func1<gr.neuropublic.Niva.services.UserSession, Response> javaClosure) {
        Func1<gr.neuropublic.base.UserSession, Response> javaClosureCasted = new Func1<gr.neuropublic.base.UserSession, Response>() {
            @Override
            public Response lambda(gr.neuropublic.base.UserSession t1) {
                return javaClosure.lambda((gr.neuropublic.Niva.services.UserSession)t1);
            }
            
        };
    
        return super.handleWebRequest(sessionId, ssoSessionId, subscriberCookie, "niva-session-id", "/Niva/", javaClosureCasted);
    }
}