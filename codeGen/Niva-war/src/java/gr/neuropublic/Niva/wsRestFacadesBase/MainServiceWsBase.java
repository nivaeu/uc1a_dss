package gr.neuropublic.Niva.wsRestFacadesBase;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import gr.neuropublic.shiro.ILoginController;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import gr.neuropublic.wsrestutils.AbstractWebServiceHandler;
import javax.ws.rs.CookieParam;
import gr.neuropublic.Niva.services.IUserManagementService;
import gr.neuropublic.Niva.services.ISessionsCacheService;
import org.slf4j.LoggerFactory;

import gr.neuropublic.exceptions.GenericApplicationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.HashMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import gr.neuropublic.wsrestutils.LazyFieldExclusionStrategy;
import gr.neuropublic.mutil.base.Pair;
import gr.neuropublic.base.Base64Utils;
import java.util.ArrayList;
import gr.neuropublic.functional.Func1;
import gr.neuropublic.functional.Func;
import gr.neuropublic.functional.FList;
import gr.neuropublic.base.JsonSingleValue;
import gr.neuropublic.base.SaveResponse;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.jboss.resteasy.annotations.GZIP;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.apache.commons.io.IOUtils;
import gr.neuropublic.wsrestutils.DynamicLayerInfo;
import javax.annotation.PostConstruct;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import gr.neuropublic.base.ExcelFileUtils;
import gr.neuropublic.base.ExcelFileUtils.ExcelWorkBook;
import gr.neuropublic.base.JsonBlob;

import gr.neuropublic.Niva.serviceInputs.*;
import gr.neuropublic.Niva.entities.ExcelFile;

public  class MainServiceWsBase extends AbstractWebServiceHandler{

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(MainServiceWsBase.class);

    @EJB
    private IUserManagementService.ILocal usrMngSrv;
    @Override
    public IUserManagementService.ILocal getUserManagementService() {
        return usrMngSrv;
    }
    public void setUserManagementService(IUserManagementService.ILocal val) {
        usrMngSrv = val;
    }

    @EJB
    private ISessionsCacheService.ILocal sessionsCache;
    @Override
    public ISessionsCacheService.ILocal getSessionsCache() {
        return sessionsCache;
    }
    public void setSessionsCache(ISessionsCacheService.ILocal sessionsCache) {
        this.sessionsCache = sessionsCache;
    }

    @EJB
    private gr.neuropublic.Niva.services.IMainService.ILocal mainService;

    public gr.neuropublic.Niva.services.IMainService.ILocal getService() {
        return mainService;
    }

    @Override
    public void initializeDatabaseSession(gr.neuropublic.base.UserSession usrSession) {
    }



    
    private Map<String, gr.neuropublic.base.IAbstractService> services = new HashMap<String, gr.neuropublic.base.IAbstractService>();
        
    @PostConstruct
    public void initializeEbj() {
        this.services.put("MainService", mainService);
    }


    
    public static class checkProducerStatus_request {
        public String producerVat;
    }
    
    @POST
    @Path("/checkProducerStatus")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response checkProducerStatus(
        final checkProducerStatus_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            getUserManagementService().checkProducerStatus(r.producerVat, "Niva", "Use Case 1a: Decision Support System", usrSession.subsId);
    
            return Response.status(200).type("application/json;charset=UTF-8").entity("").build();
        }});
    }
    
    @POST
    @Path("/getProducerStatus")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getProducerStatus(
        final checkProducerStatus_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            gr.neuropublic.base.ProducerCheckStatus checkStatus = getUserManagementService().getProducerStatus(r.producerVat, "Niva", usrSession.subsId);
            return Response.status(200).type("application/json;charset=UTF-8").entity(checkStatus.toString()).build();
        }});
    }

    
    @POST
    @Path("/getLogoProvider")
    @Consumes("application/json")
    @GZIP
    public Response getLogoProvider(@CookieParam("niva-session-id") final String sessionId) {
                
        return handleWebRequest(sessionId, new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(final gr.neuropublic.Niva.services.UserSession usrSession) {
            JsonSingleValue rsp = new JsonSingleValue(mainService.getLogoProvider(usrSession));
            Gson gson =  new GsonBuilder().setPrettyPrinting().create();
                
            String result = gson.toJson(rsp);
            return Response.status(200).type("application/json;charset=UTF-8").entity(result).build();
        }});
    }    


    public static class DynamicLayer_request {
        public String layerCode;
        public String mode;
        public BigDecimal xmin;
        public BigDecimal ymin;
        public BigDecimal xmax;
        public BigDecimal ymax;
        public BigDecimal fromRowIndex;
        public BigDecimal toRowIndex;

        public String selectedGeometry;
        public String geoFunc;
        public String geoFuncParam;
        public List<String> idsInCache;
    }
        
    public DynamicLayerInfo getLayerInfoByCode(String layerCode)  {
        /*
            DynamicLayerInfo l = new DynamicLayerInfo();
            l.jdbcDriver = "oracle.jdbc.OracleDriver";
            l.jdbcConnectionURL = "jdbc:oracle:thin:@//10.31.3.126:1521/ORCL";
            l.user = "YPOERGO2$APP";
            l.pass = "YPOERGO2$APP";
            l.code = "ILOTS";
            l.SRID = 2100;
            l.nMaxWindowDX = 5000;
            l.sqlQuery = "select ILOT_NO as id, geom, cast(ilot_no as varchar(100)) as shortdesc , cover_id "
                    + "from S_ADMIN2014.GR_ILOTS "
                    + "where (geom is not null)";
            return l;
            */
            return null;
    }

    @POST
    @Path("/dynamicLayer_request")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response dynamicLayer_request(
        final DynamicLayer_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            ArrayList<Pair<String, Object>> params =  new ArrayList<Pair<String, Object>>();
            DynamicLayerInfo linfo = getLayerInfoByCode(r.layerCode);
            String sqlQuery = null;
                if ("MAP".equals(r.mode)) {
                    sqlQuery =
                        "select a.* " +
                        "from { " +
                                linfo.sqlQuery +
                        " } a " +
                        "where " +
                        "  sdo_anyinteract(a.geom, ( " +
                        "     select mdsys.sdo_geometry( " +
                        "       2003, " +
                        "       2100, " +
                        "       null, " +
                        "       mdsys.sdo_elem_info_array(1,1003,3), " +
                        "       SDO_ORDINATE_ARRAY(:xmin, :ymin, :xmax, :ymax)) " +
                        "     FROM DUAL)) = 'TRUE' " +
                        "  and (10=10) " +
                        "  and (:xmax - :xmin <= " + linfo.nMaxWindowDX + ") ";

                        params.add(gr.neuropublic.mutil.base.Pair.create("xmin", (Object)r.xmin));
                        params.add(gr.neuropublic.mutil.base.Pair.create("ymin", (Object)r.ymin));
                        params.add(gr.neuropublic.mutil.base.Pair.create("xmax", (Object)r.xmax));
                        params.add(gr.neuropublic.mutil.base.Pair.create("ymax", (Object)r.ymax));

                } else {
                    sqlQuery =
                        "select *  " +
                        "from { " +
                                linfo.sqlQuery +
                        "}   " +
                        "where  " +
                        "  rownum <= :toRowIndex " +
                        "  and (10=10) ";

                    params.add(gr.neuropublic.mutil.base.Pair.create("toRowIndex", (Object)r.toRowIndex));

                    if (!BigDecimal.ZERO.equals(r.fromRowIndex)) {
                        sqlQuery = 
                            "select *  " +
                            "from {  " +
                            "    select row_.*, rownum as rownum__   " +
                            "    from { " +
                                    linfo.sqlQuery +
                            "    } row_   " +
                            "    where rownum <= :toRowIndex " +
                            "          and (10=10) " +
                            "} where rownum__ > :fromRowIndex ";

                        params.add(gr.neuropublic.mutil.base.Pair.create("fromRowIndex", (Object)r.fromRowIndex));
                    }   
                }
            Set<String> ids = r.idsInCache!=null ?  new HashSet<String>(r.idsInCache) : new HashSet<String>();

          
            if (r.geoFunc != null &&  r.selectedGeometry != null) {
                org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
                try {
                    com.vividsolutions.jts.geom.Geometry geo = gjson.read(r.selectedGeometry);
                    geo.setSRID(2100);
                    String rep;
                    if (r.geoFunc.equals("WITHIN_DISTANCE")) {
                        rep = "SDO_WITHIN_DISTANCE(geom, :selectedGeometry,'DISTANCE=" +(r.geoFuncParam==null?"0":r.geoFuncParam) + " UNIT=KM ' )='TRUE'";
                    } else if (r.geoFunc.equals("CONTAINS")) {
                        rep = "SDO_CONTAINS(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("INSIDE")) {
                        rep = "SDO_INSIDE(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("TOUCH")) {
                        rep = "SDO_TOUCH(geom, :selectedGeometry)='TRUE'";
                    } else {
                        throw new GenericApplicationException("Unsupported spatial operator:" + r.geoFunc == null ? "null" : r.geoFunc);
                    }
            
                    sqlQuery = sqlQuery.replaceAll("10=10", rep);
                    params.add(gr.neuropublic.mutil.base.Pair.create("selectedGeometry", (Object)geo));
            
                } catch (IOException ex) {
                    logger.error("Error in reading geoJSON DATA ",ex);
                    throw new GenericApplicationException(ex.getMessage());
                }
            }
                
                  
            sqlQuery = sqlQuery.replace('{', '(').replace('}', ')');
            List<gr.neuropublic.base.SqlQueryRow> entities = services.get(linfo.service).executeSqlQuery2(sqlQuery, params);
            gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();
                
            for(gr.neuropublic.base.SqlQueryRow r : entities) {
                Object id = r.getId();
                if (ids.contains(id.toString())) {
                    r.clear();
                    r.setId(id);
                }
            }
            ret.data = entities;
            ret.count = entities.size();
            String jsonStr = ret.toJson2();
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    
    @POST
    @Path("/synchronizeChangesWithDb_Agency")
    @Consumes("*/*")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response synchronizeChangesWithDb_Agency(
        final String jsonData, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            SaveResponse response = mainService.synchronizeChangesWithDbJS_Agency(usrSession.usrEmail, jsonData, (gr.neuropublic.Niva.services.UserSession)usrSession); 
            return Response.status(200).type("application/json;charset=UTF-8").entity(response).build();
        }});
    }    
    @POST
    @Path("/synchronizeChangesWithDb_Classification")
    @Consumes("*/*")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response synchronizeChangesWithDb_Classification(
        final String jsonData, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            SaveResponse response = mainService.synchronizeChangesWithDbJS_Classification(usrSession.usrEmail, jsonData, (gr.neuropublic.Niva.services.UserSession)usrSession); 
            return Response.status(200).type("application/json;charset=UTF-8").entity(response).build();
        }});
    }    
    @POST
    @Path("/synchronizeChangesWithDb_Classifier")
    @Consumes("*/*")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response synchronizeChangesWithDb_Classifier(
        final String jsonData, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            SaveResponse response = mainService.synchronizeChangesWithDbJS_Classifier(usrSession.usrEmail, jsonData, (gr.neuropublic.Niva.services.UserSession)usrSession); 
            return Response.status(200).type("application/json;charset=UTF-8").entity(response).build();
        }});
    }    
    @POST
    @Path("/synchronizeChangesWithDb_CoverType")
    @Consumes("*/*")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response synchronizeChangesWithDb_CoverType(
        final String jsonData, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            SaveResponse response = mainService.synchronizeChangesWithDbJS_CoverType(usrSession.usrEmail, jsonData, (gr.neuropublic.Niva.services.UserSession)usrSession); 
            return Response.status(200).type("application/json;charset=UTF-8").entity(response).build();
        }});
    }    
    @POST
    @Path("/synchronizeChangesWithDb_Cultivation")
    @Consumes("*/*")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response synchronizeChangesWithDb_Cultivation(
        final String jsonData, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            SaveResponse response = mainService.synchronizeChangesWithDbJS_Cultivation(usrSession.usrEmail, jsonData, (gr.neuropublic.Niva.services.UserSession)usrSession); 
            return Response.status(200).type("application/json;charset=UTF-8").entity(response).build();
        }});
    }    
    @POST
    @Path("/synchronizeChangesWithDb_DecisionMaking")
    @Consumes("*/*")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response synchronizeChangesWithDb_DecisionMaking(
        final String jsonData, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            SaveResponse response = mainService.synchronizeChangesWithDbJS_DecisionMaking(usrSession.usrEmail, jsonData, (gr.neuropublic.Niva.services.UserSession)usrSession); 
            return Response.status(200).type("application/json;charset=UTF-8").entity(response).build();
        }});
    }    
    @POST
    @Path("/synchronizeChangesWithDb_DecisionMakingRO")
    @Consumes("*/*")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response synchronizeChangesWithDb_DecisionMakingRO(
        final String jsonData, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            SaveResponse response = mainService.synchronizeChangesWithDbJS_DecisionMakingRO(usrSession.usrEmail, jsonData, (gr.neuropublic.Niva.services.UserSession)usrSession); 
            return Response.status(200).type("application/json;charset=UTF-8").entity(response).build();
        }});
    }    
    @POST
    @Path("/synchronizeChangesWithDb_EcGroup")
    @Consumes("*/*")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response synchronizeChangesWithDb_EcGroup(
        final String jsonData, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            SaveResponse response = mainService.synchronizeChangesWithDbJS_EcGroup(usrSession.usrEmail, jsonData, (gr.neuropublic.Niva.services.UserSession)usrSession); 
            return Response.status(200).type("application/json;charset=UTF-8").entity(response).build();
        }});
    }    
    @POST
    @Path("/synchronizeChangesWithDb_FileTemplate")
    @Consumes("*/*")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response synchronizeChangesWithDb_FileTemplate(
        final String jsonData, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            SaveResponse response = mainService.synchronizeChangesWithDbJS_FileTemplate(usrSession.usrEmail, jsonData, (gr.neuropublic.Niva.services.UserSession)usrSession); 
            return Response.status(200).type("application/json;charset=UTF-8").entity(response).build();
        }});
    }    
    @POST
    @Path("/synchronizeChangesWithDb_Document")
    @Consumes("*/*")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response synchronizeChangesWithDb_Document(
        final String jsonData, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            SaveResponse response = mainService.synchronizeChangesWithDbJS_Document(usrSession.usrEmail, jsonData, (gr.neuropublic.Niva.services.UserSession)usrSession); 
            return Response.status(200).type("application/json;charset=UTF-8").entity(response).build();
        }});
    }    
    @POST
    @Path("/synchronizeChangesWithDb_FileDirPath")
    @Consumes("*/*")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response synchronizeChangesWithDb_FileDirPath(
        final String jsonData, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            SaveResponse response = mainService.synchronizeChangesWithDbJS_FileDirPath(usrSession.usrEmail, jsonData, (gr.neuropublic.Niva.services.UserSession)usrSession); 
            return Response.status(200).type("application/json;charset=UTF-8").entity(response).build();
        }});
    }    
    @POST
    @Path("/synchronizeChangesWithDb_Integrateddecision")
    @Consumes("*/*")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response synchronizeChangesWithDb_Integrateddecision(
        final String jsonData, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            SaveResponse response = mainService.synchronizeChangesWithDbJS_Integrateddecision(usrSession.usrEmail, jsonData, (gr.neuropublic.Niva.services.UserSession)usrSession); 
            return Response.status(200).type("application/json;charset=UTF-8").entity(response).build();
        }});
    }    
    @POST
    @Path("/synchronizeChangesWithDb_ParcelGP")
    @Consumes("*/*")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response synchronizeChangesWithDb_ParcelGP(
        final String jsonData, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            SaveResponse response = mainService.synchronizeChangesWithDbJS_ParcelGP(usrSession.usrEmail, jsonData, (gr.neuropublic.Niva.services.UserSession)usrSession); 
            return Response.status(200).type("application/json;charset=UTF-8").entity(response).build();
        }});
    }    
    @POST
    @Path("/synchronizeChangesWithDb_AgrisnapUsers")
    @Consumes("*/*")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response synchronizeChangesWithDb_AgrisnapUsers(
        final String jsonData, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            SaveResponse response = mainService.synchronizeChangesWithDbJS_AgrisnapUsers(usrSession.usrEmail, jsonData, (gr.neuropublic.Niva.services.UserSession)usrSession); 
            return Response.status(200).type("application/json;charset=UTF-8").entity(response).build();
        }});
    }    
    @POST
    @Path("/synchronizeChangesWithDb_ParcelsIssues")
    @Consumes("*/*")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response synchronizeChangesWithDb_ParcelsIssues(
        final String jsonData, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            SaveResponse response = mainService.synchronizeChangesWithDbJS_ParcelsIssues(usrSession.usrEmail, jsonData, (gr.neuropublic.Niva.services.UserSession)usrSession); 
            return Response.status(200).type("application/json;charset=UTF-8").entity(response).build();
        }});
    }    
    @POST
    @Path("/synchronizeChangesWithDb_Producers")
    @Consumes("*/*")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response synchronizeChangesWithDb_Producers(
        final String jsonData, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            SaveResponse response = mainService.synchronizeChangesWithDbJS_Producers(usrSession.usrEmail, jsonData, (gr.neuropublic.Niva.services.UserSession)usrSession); 
            return Response.status(200).type("application/json;charset=UTF-8").entity(response).build();
        }});
    }    
    @POST
    @Path("/synchronizeChangesWithDb_Dashboard")
    @Consumes("*/*")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response synchronizeChangesWithDb_Dashboard(
        final String jsonData, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            SaveResponse response = mainService.synchronizeChangesWithDbJS_Dashboard(usrSession.usrEmail, jsonData, (gr.neuropublic.Niva.services.UserSession)usrSession); 
            return Response.status(200).type("application/json;charset=UTF-8").entity(response).build();
        }});
    }    
    @POST
    @Path("/synchronizeChangesWithDb_ParcelFMIS")
    @Consumes("*/*")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response synchronizeChangesWithDb_ParcelFMIS(
        final String jsonData, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            SaveResponse response = mainService.synchronizeChangesWithDbJS_ParcelFMIS(usrSession.usrEmail, jsonData, (gr.neuropublic.Niva.services.UserSession)usrSession); 
            return Response.status(200).type("application/json;charset=UTF-8").entity(response).build();
        }});
    }    
    @POST
    @Path("/synchronizeChangesWithDb_FmisUser")
    @Consumes("*/*")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response synchronizeChangesWithDb_FmisUser(
        final String jsonData, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            SaveResponse response = mainService.synchronizeChangesWithDbJS_FmisUser(usrSession.usrEmail, jsonData, (gr.neuropublic.Niva.services.UserSession)usrSession); 
            return Response.status(200).type("application/json;charset=UTF-8").entity(response).build();
        }});
    }    

    public  Response handleWebRequest(String sessionId, final Func1<gr.neuropublic.Niva.services.UserSession, Response> javaClosure) {
        return this.handleWebRequest(sessionId, null, new Pair<String,String>(null, null), javaClosure);
    }
    public  Response handleWebRequest(String sessionId, String ssoSessionId, Pair<String,String> subscriberCookie, final Func1<gr.neuropublic.Niva.services.UserSession, Response> javaClosure) {
        Func1<gr.neuropublic.base.UserSession, Response> javaClosureCasted = new Func1<gr.neuropublic.base.UserSession, Response>() {
            @Override
            public Response lambda(gr.neuropublic.base.UserSession t1) {
                return javaClosure.lambda((gr.neuropublic.Niva.services.UserSession)t1);
            }
            
        };
    
        return super.handleWebRequest(sessionId, ssoSessionId, subscriberCookie, "niva-session-id", "/Niva/", javaClosureCasted);
    }

        
    @POST
    @Path("/finalizationEcGroup")
    @Consumes("application/json")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response finalizationEcGroup(final FinalizationEcGroupInput req, @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) {
            
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(final gr.neuropublic.Niva.services.UserSession usrSession) {

            JsonSingleValue rsp = new JsonSingleValue(new ArrayList<String>(mainService.finalizationEcGroup(
                req != null ? (req.ecgrId != null ? usrSession.crypto.DecryptInteger(req.ecgrId) : null) : null,
                usrSession)) );
            Gson gson =  new GsonBuilder().setPrettyPrinting().create();
            
            String result = gson.toJson(rsp);
            return Response.status(200).type("application/json;charset=UTF-8").entity(result).build();    }});
    }    

        
    @POST
    @Path("/runningDecisionMaking")
    @Consumes("application/json")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response runningDecisionMaking(final RunningDecisionMakingInput req, @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) {
            
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(final gr.neuropublic.Niva.services.UserSession usrSession) {

            JsonSingleValue rsp = new JsonSingleValue(new ArrayList<String>(mainService.runningDecisionMaking(
                req != null ? (req.demaId != null ? usrSession.crypto.DecryptInteger(req.demaId) : null) : null,
                usrSession)) );
            Gson gson =  new GsonBuilder().setPrettyPrinting().create();
            
            String result = gson.toJson(rsp);
            return Response.status(200).type("application/json;charset=UTF-8").entity(result).build();    }});
    }    

        
    @POST
    @Path("/importingClassification")
    @Consumes("application/json")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response importingClassification(final ImportingClassificationInput req, @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) {
            
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(final gr.neuropublic.Niva.services.UserSession usrSession) {

            JsonSingleValue rsp = new JsonSingleValue(new ArrayList<String>(mainService.importingClassification(
                req != null ? (req.classId != null ? usrSession.crypto.DecryptInteger(req.classId) : null) : null,
                usrSession)) );
            Gson gson =  new GsonBuilder().setPrettyPrinting().create();
            
            String result = gson.toJson(rsp);
            return Response.status(200).type("application/json;charset=UTF-8").entity(result).build();    }});
    }    

        
    @GET
    @Path("/getPhotoRequests")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getPhotoRequests(
        @QueryParam("agrisnapUid") final String agrisnapUid
    ) {
            
        return handleWebRequest(new Func<Response>() {@Override public Response  lambda() {

            JsonSingleValue rsp = new JsonSingleValue(mainService.getPhotoRequests(
                agrisnapUid) );
            Gson gson =  new GsonBuilder().setPrettyPrinting().create();
            
            String result = gson.toJson(rsp);
            return Response.status(200).type("application/json;charset=UTF-8").entity(result).build();
        }});
    }    

        
    @POST
    @Path("/uploadPhoto")
    @Consumes("application/json")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response uploadPhoto(final UploadPhotoInput req) {
            
        return handleWebRequest(new Func<Response>() {@Override public Response  lambda() {

            JsonSingleValue rsp = new JsonSingleValue(mainService.uploadPhoto(
                req != null ? req.hash : null,
                req != null ? req.attachment : null,
                req != null ? req.data : null,
                req != null ? req.environment : null,
                req != null ? req.id : null) );
            Gson gson =  new GsonBuilder().setPrettyPrinting().create();
            
            String result = gson.toJson(rsp);
            return Response.status(200).type("application/json;charset=UTF-8").entity(result).build();    }});
    }    

        
    @GET
    @Path("/getBRERunsResultsList")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getBRERunsResultsList(
        @QueryParam("info") final Boolean info,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie
    ) {
            
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(final gr.neuropublic.Niva.services.UserSession usrSession) {

            JsonSingleValue rsp = new JsonSingleValue(mainService.getBRERunsResultsList(
                info,
                usrSession) );
            Gson gson =  new GsonBuilder().setPrettyPrinting().create();
            
            String result = gson.toJson(rsp);
            return Response.status(200).type("application/json;charset=UTF-8").entity(result).build();
        }});
    }    

        
    @GET
    @Path("/getBRERunResults")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getBRERunResults(
        @QueryParam("info") final Boolean info,
        @QueryParam("id") final Integer id,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie
    ) {
            
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(final gr.neuropublic.Niva.services.UserSession usrSession) {

            JsonSingleValue rsp = new JsonSingleValue(mainService.getBRERunResults(
                info,
                id,
                usrSession) );
            Gson gson =  new GsonBuilder().setPrettyPrinting().create();
            
            String result = gson.toJson(rsp);
            return Response.status(200).type("application/json;charset=UTF-8").entity(result).build();
        }});
    }    

        
    @GET
    @Path("/getFMISRequests")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getFMISRequests(
        @QueryParam("fmisUid") final String fmisUid
    ) {
            
        return handleWebRequest(new Func<Response>() {@Override public Response  lambda() {

            JsonSingleValue rsp = new JsonSingleValue(mainService.getFMISRequests(
                fmisUid) );
            Gson gson =  new GsonBuilder().setPrettyPrinting().create();
            
            String result = gson.toJson(rsp);
            return Response.status(200).type("application/json;charset=UTF-8").entity(result).build();
        }});
    }    

        
    @POST
    @Path("/uploadFMIS")
    @Consumes("application/json")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response uploadFMIS(final UploadFMISInput req) {
            
        return handleWebRequest(new Func<Response>() {@Override public Response  lambda() {

            JsonSingleValue rsp = new JsonSingleValue(mainService.uploadFMIS(
                req != null ? req.fmisUid : null,
                req != null ? req.id : null,
                req != null ? req.attachment : null,
                req != null ? req.metadata : null) );
            Gson gson =  new GsonBuilder().setPrettyPrinting().create();
            
            String result = gson.toJson(rsp);
            return Response.status(200).type("application/json;charset=UTF-8").entity(result).build();    }});
    }    

        
    @POST
    @Path("/updateIntegratedDecisionsNIssues")
    @Consumes("application/json")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response updateIntegratedDecisionsNIssues(final UpdateIntegratedDecisionsNIssuesInput req, @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) {
            
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(final gr.neuropublic.Niva.services.UserSession usrSession) {

            JsonSingleValue rsp = new JsonSingleValue(new ArrayList<String>(mainService.updateIntegratedDecisionsNIssues(
                req != null ? (req.pclaId != null ? usrSession.crypto.DecryptInteger(req.pclaId) : null) : null,
                usrSession)) );
            Gson gson =  new GsonBuilder().setPrettyPrinting().create();
            
            String result = gson.toJson(rsp);
            return Response.status(200).type("application/json;charset=UTF-8").entity(result).build();    }});
    }    

        
    @POST
    @Path("/updateIntegratedDecisionsNIssuesFromDema")
    @Consumes("application/json")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response updateIntegratedDecisionsNIssuesFromDema(final UpdateIntegratedDecisionsNIssuesFromDemaInput req, @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) {
            
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(final gr.neuropublic.Niva.services.UserSession usrSession) {

            JsonSingleValue rsp = new JsonSingleValue(new ArrayList<String>(mainService.updateIntegratedDecisionsNIssuesFromDema(
                req != null ? (req.demaId != null ? usrSession.crypto.DecryptInteger(req.demaId) : null) : null,
                usrSession)) );
            Gson gson =  new GsonBuilder().setPrettyPrinting().create();
            
            String result = gson.toJson(rsp);
            return Response.status(200).type("application/json;charset=UTF-8").entity(result).build();    }});
    }    

        
    @POST
    @Path("/actionPushToCommonsAPI")
    @Consumes("application/json")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response actionPushToCommonsAPI(final ActionPushToCommonsAPIInput req, @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) {
            
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(final gr.neuropublic.Niva.services.UserSession usrSession) {

            JsonSingleValue rsp = new JsonSingleValue(new ArrayList<String>(mainService.actionPushToCommonsAPI(
                req != null ? (req.demaId != null ? usrSession.crypto.DecryptInteger(req.demaId) : null) : null,
                usrSession)) );
            Gson gson =  new GsonBuilder().setPrettyPrinting().create();
            
            String result = gson.toJson(rsp);
            return Response.status(200).type("application/json;charset=UTF-8").entity(result).build();    }});
    }    


    @POST
    @Path("/importExcel_CoverType_impCotys_id")
    @Consumes("multipart/form-data")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response importExcel_CoverType_impCotys_id(
        final MultipartFormDataInput input, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            if (!usrSession.privileges.contains("Niva_CoverType_W"))
                throw new GenericApplicationException("NO_WRITE_ACCESS");

            byte [] excelData;
            String excelFilename;

            try {
                InputStream inputStream = input.getFormDataPart("uploadedFileData", InputStream.class, null);
                excelData = IOUtils.toByteArray(inputStream);
                excelFilename = new String(Base64Utils.decode(input.getFormDataPart("uploadedFileName", String.class, null)), "UTF-8");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            ExcelFile excelFile = getService().importExcel_CoverType_impCotys_id(
                excelData, 
                excelFilename, 
                (gr.neuropublic.Niva.services.UserSession)usrSession);

            String ret = "{\"excelFileId\":\"" + usrSession.EncryptNumber(excelFile.getId()) + "\","
                    + "\"totalRows\": " + excelFile.getTotalRows() + ","
                    + "\"totalErrorRows\": " + excelFile.getTotalErrorRows() + "}";
            return Response.status(200).type("application/json;charset=UTF-8").entity(ret).build();
        }});
    }        

    public static class ImportExcel_CoverType_impCotys_id_SpecsAndSample_request {
        public String globalLang;
    }

    @POST
    @Path("/importExcel_CoverType_impCotys_id_SpecsAndSample")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response importExcel_CoverType_impCotys_id_SpecsAndSample(
        final ImportExcel_CoverType_impCotys_id_SpecsAndSample_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {

            ExcelWorkBook eWb = new ExcelWorkBook(r.globalLang);
            HSSFWorkbook workbook = eWb.createImportExcelSampleWorkBook(true, new ExcelWorkBook.ImportExcelSampleCell[]{
                new ExcelWorkBook.ImportExcelSampleCellNumeric("A", "Code", "Integer", true, 0, null, null, null, null),
                new ExcelWorkBook.ImportExcelSampleCellString("B", "Name", true, 60, null, null)
            });

            // ByteArrayOutput
            java.io.ByteArrayOutputStream strm = new java.io.ByteArrayOutputStream();
            try {
                workbook.write(strm);
            } catch (IOException e) {
                logger.error("IOException", e);
                throw new RuntimeException(e);
            }
                            
            JsonBlob rsp = new JsonBlob();
            rsp.data = strm.toByteArray();
                    
            return Response.status(200).type("application/json;charset=UTF-8").entity(rsp).build();
        }});
    }
        

    @POST
    @Path("/importExcel_Cultivation_impCults_id")
    @Consumes("multipart/form-data")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response importExcel_Cultivation_impCults_id(
        final MultipartFormDataInput input, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            if (!usrSession.privileges.contains("Niva_Cultivation_W"))
                throw new GenericApplicationException("NO_WRITE_ACCESS");

            byte [] excelData;
            String excelFilename;

            try {
                InputStream inputStream = input.getFormDataPart("uploadedFileData", InputStream.class, null);
                excelData = IOUtils.toByteArray(inputStream);
                excelFilename = new String(Base64Utils.decode(input.getFormDataPart("uploadedFileName", String.class, null)), "UTF-8");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            ExcelFile excelFile = getService().importExcel_Cultivation_impCults_id(
                excelData, 
                excelFilename, 
                (gr.neuropublic.Niva.services.UserSession)usrSession);

            String ret = "{\"excelFileId\":\"" + usrSession.EncryptNumber(excelFile.getId()) + "\","
                    + "\"totalRows\": " + excelFile.getTotalRows() + ","
                    + "\"totalErrorRows\": " + excelFile.getTotalErrorRows() + "}";
            return Response.status(200).type("application/json;charset=UTF-8").entity(ret).build();
        }});
    }        

    public static class ImportExcel_Cultivation_impCults_id_SpecsAndSample_request {
        public String globalLang;
    }

    @POST
    @Path("/importExcel_Cultivation_impCults_id_SpecsAndSample")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response importExcel_Cultivation_impCults_id_SpecsAndSample(
        final ImportExcel_Cultivation_impCults_id_SpecsAndSample_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {

            ExcelWorkBook eWb = new ExcelWorkBook(r.globalLang);
            HSSFWorkbook workbook = eWb.createImportExcelSampleWorkBook(true, new ExcelWorkBook.ImportExcelSampleCell[]{
                new ExcelWorkBook.ImportExcelSampleCellString("A", "Crop Name", true, 200, null, null),
                new ExcelWorkBook.ImportExcelSampleCellNumeric("B", "Crop Code", "Integer", true, 0, null, null, null, null),
                new ExcelWorkBook.ImportExcelSampleCellString("C", "Land Cover Name", false, 60, null, null)
            });

            // ByteArrayOutput
            java.io.ByteArrayOutputStream strm = new java.io.ByteArrayOutputStream();
            try {
                workbook.write(strm);
            } catch (IOException e) {
                logger.error("IOException", e);
                throw new RuntimeException(e);
            }
                            
            JsonBlob rsp = new JsonBlob();
            rsp.data = strm.toByteArray();
                    
            return Response.status(200).type("application/json;charset=UTF-8").entity(rsp).build();
        }});
    }
        

}