package gr.neuropublic.Niva.wsRestFacadesBase;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import gr.neuropublic.shiro.ILoginController;
import java.util.List;
import java.util.ArrayList;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Context;
import javax.servlet.ServletContext;
import gr.neuropublic.exceptions.GenericApplicationException;
import gr.neuropublic.wsrestutils.AbstractWebServiceHandler;
import gr.neuropublic.functional.Func1;
import gr.neuropublic.Niva.services.IUserManagementService;
import gr.neuropublic.mutil.base.Pair;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.core.MediaType;
import gr.neuropublic.Niva.facades.IIntegrateddecisionFacade;
import gr.neuropublic.Niva.entities.Integrateddecision;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import gr.neuropublic.Niva.facades.ITmpBlobFacade;
import gr.neuropublic.Niva.entities.TmpBlob;
import gr.neuropublic.wsrestutils.LazyFieldExclusionStrategy;
import gr.neuropublic.base.DateFormat;
import javax.ws.rs.CookieParam;
import javax.ws.rs.core.NewCookie;
import gr.neuropublic.Niva.services.ISessionsCacheService;
import gr.neuropublic.base.JsonBlob;
import gr.neuropublic.base.JsonSingleValue;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.jboss.resteasy.annotations.GZIP;
import javax.ws.rs.core.MultivaluedMap;
import java.io.InputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Set;
import org.apache.commons.io.IOUtils;
import org.slf4j.LoggerFactory;

import gr.neuropublic.base.GridColDefinition;

public  class IntegrateddecisionWsFacadeBase  extends AbstractWebServiceHandler {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(gr.neuropublic.base.IAbstractService.class);

    @EJB
    private IIntegrateddecisionFacade.ILocal integrateddecisionFacade;
    public IIntegrateddecisionFacade.ILocal getIntegrateddecisionFacade() {
        return integrateddecisionFacade;
    }
    public void setIntegrateddecisionFacade(IIntegrateddecisionFacade.ILocal val) {
        integrateddecisionFacade = val;
    }

    @EJB
    private IUserManagementService.ILocal usrMngSrv;
    @Override
    public IUserManagementService.ILocal getUserManagementService() {
        return usrMngSrv;
    }
    public void setUserManagementService(IUserManagementService.ILocal val) {
        usrMngSrv = val;
    }

    @EJB
    private ISessionsCacheService.ILocal sessionsCache;
    @Override
    public ISessionsCacheService.ILocal getSessionsCache() {
        return sessionsCache;
    }
    public void setSessionsCache(ISessionsCacheService.ILocal sessionsCache) {
        this.sessionsCache = sessionsCache;
    }


    @EJB
    private ITmpBlobFacade.ILocal tmpBlobFacade;
    public ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }

    @EJB
    private gr.neuropublic.Niva.services.IMainService.ILocal service;

    public gr.neuropublic.Niva.services.IMainService.ILocal getService()
    {
        return service;
    }
    public void setService(gr.neuropublic.Niva.services.IMainService.ILocal service)
    {
        this.service = service;
    }

    @Override
    public void initializeDatabaseSession(gr.neuropublic.base.UserSession usrSession) {
    }




    public static class FindAllByIds_request {
        public String[] ids; 
    }

    @POST
    @Path("/findAllByIds")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByIds(
        final FindAllByIds_request r, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            gr.neuropublic.wsrestutils.LazyData ret = findAllByIds_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    public gr.neuropublic.wsrestutils.LazyData findAllByIds_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindAllByIds_request r) {
            
        ArrayList<Integer> ids = new ArrayList<Integer>();
        for(String enc_id: r.ids) {
            Integer id = (enc_id != null ? usrSession.DecryptInteger(enc_id) : null);
            if (id != null)
                ids.add(id);
        } 
        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        List<Integrateddecision> entities = getIntegrateddecisionFacade().findAllByIds(ids);
        ret.data = entities;
        ret.count = entities.size();
        return ret;
    }


    public static class FindByIntegrateddecisionsId_request {
        public String integrateddecisionsId; 
    }

    @POST
    @Path("/findByIntegrateddecisionsId")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findByIntegrateddecisionsId(
        final FindByIntegrateddecisionsId_request r, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            gr.neuropublic.wsrestutils.LazyData ret = findByIntegrateddecisionsId_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    public gr.neuropublic.wsrestutils.LazyData findByIntegrateddecisionsId_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindByIntegrateddecisionsId_request r) {
            
        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        java.util.ArrayList<Integrateddecision> list = new java.util.ArrayList<Integrateddecision>();
        Integrateddecision c = getIntegrateddecisionFacade().findByIntegrateddecisionsId((r.integrateddecisionsId != null ? usrSession.DecryptInteger(r.integrateddecisionsId) : null));
        if (c!=null)
            list.add(c); 
        ret.data = list;
        ret.count = list.size();
        return ret;
    }


    public static class FindLazyIntegrateddecision_request {
        public Short decisionCode;
        public String dteUpdate;
        public String usrUpdate;
        public String pclaId_pclaId;
        public String demaId_demaId;
        public Integer fromRowIndex;
        public Integer toRowIndex;
        public String sortField;
        public Boolean sortOrder;
        public List<String> exc_Id;
        public List<GridColDefinition> __fields;
        public gr.neuropublic.wsrestutils.LazyData.requestType __dataReqType;
    }

    @POST
    @Path("/findLazyIntegrateddecision")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findLazyIntegrateddecision(
        final FindLazyIntegrateddecision_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.DATA;
            gr.neuropublic.wsrestutils.LazyData ret = findLazyIntegrateddecision_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    @POST
    @Path("/findLazyIntegrateddecision_count")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findLazyIntegrateddecision_count(
        final FindLazyIntegrateddecision_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.COUNT;
            gr.neuropublic.wsrestutils.LazyData ret = findLazyIntegrateddecision_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }
    public gr.neuropublic.wsrestutils.LazyData findLazyIntegrateddecision_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindLazyIntegrateddecision_request r) {
        if (!usrSession.privileges.contains("Niva_Integrateddecision_R"))
            throw new GenericApplicationException("NO_READ_ACCESS");
        //Thread.sleep(1500); // debugging waiting dialogs
        int[] range = new int[] {r.fromRowIndex != null ? r.fromRowIndex : 0, r.toRowIndex != null ? r.toRowIndex : 10};
        int[] recordCount = new int [] {0};
        List<String> excludedIds = new ArrayList<String>();
        if (r != null && r.exc_Id != null && !r.exc_Id.isEmpty()) {
            for(String encrypted_id:r.exc_Id) {
                if (encrypted_id == null)
                    continue;
                String descryptedId = usrSession.DecryptInteger(encrypted_id).toString();
                excludedIds.add(descryptedId);
            }
        }

        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        if (r.__dataReqType == null || r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.DATA) {
            List<Integrateddecision> retEntities= getIntegrateddecisionFacade().findLazyIntegrateddecision(r.decisionCode, gr.neuropublic.base.DateFormatter.parseISO8601Date(r.dteUpdate), r.usrUpdate, (r.pclaId_pclaId != null && !r.pclaId_pclaId.startsWith("TEMP_ID_") ? (r.pclaId_pclaId != null ? usrSession.DecryptInteger(r.pclaId_pclaId) : null) : null), (r.demaId_demaId != null && !r.demaId_demaId.startsWith("TEMP_ID_") ? (r.demaId_demaId != null ? usrSession.DecryptInteger(r.demaId_demaId) : null) : null), range, recordCount, r.sortField, r.sortOrder != null ? r.sortOrder : false, excludedIds);
            ret.data = retEntities;
            ret.count = recordCount[0];
        } else if (r.__dataReqType != null && r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.COUNT) {
            ret.data = new ArrayList<>();
            ret.count = getIntegrateddecisionFacade().findLazyIntegrateddecision_count(r.decisionCode, gr.neuropublic.base.DateFormatter.parseISO8601Date(r.dteUpdate), r.usrUpdate, (r.pclaId_pclaId != null && !r.pclaId_pclaId.startsWith("TEMP_ID_") ? (r.pclaId_pclaId != null ? usrSession.DecryptInteger(r.pclaId_pclaId) : null) : null), (r.demaId_demaId != null && !r.demaId_demaId.startsWith("TEMP_ID_") ? (r.demaId_demaId != null ? usrSession.DecryptInteger(r.demaId_demaId) : null) : null), excludedIds);
        } else {
            throw new GenericApplicationException("Unknown_DataRequestType");
        }
        return ret;
    }


    public static class FindAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision_request {
        public String demaId_demaId;
        public Short fsch_decisionCode;
        public Integer fsch_prodCode;
        public String fsch_Code;
        public String fsch_Parcel_Identifier;
        public String fsch_CultDeclCode_cultId;
        public Integer fromRowIndex;
        public Integer toRowIndex;
        public String sortField;
        public Boolean sortOrder;
        public List<String> exc_Id;
        public List<GridColDefinition> __fields;
        public gr.neuropublic.wsrestutils.LazyData.requestType __dataReqType;
    }

    @POST
    @Path("/findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision(
        final FindAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.DATA;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    @POST
    @Path("/findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision_count")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision_count(
        final FindAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.COUNT;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }
    public gr.neuropublic.wsrestutils.LazyData findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision_request r) {
        if (!usrSession.privileges.contains("Niva_DecisionMakingRO_R"))
            throw new GenericApplicationException("NO_READ_ACCESS");
        //Thread.sleep(1500); // debugging waiting dialogs
        int[] range = new int[] {r.fromRowIndex != null ? r.fromRowIndex : 0, r.toRowIndex != null ? r.toRowIndex : 10};
        int[] recordCount = new int [] {0};
        List<String> excludedIds = new ArrayList<String>();
        if (r != null && r.exc_Id != null && !r.exc_Id.isEmpty()) {
            for(String encrypted_id:r.exc_Id) {
                if (encrypted_id == null)
                    continue;
                String descryptedId = usrSession.DecryptInteger(encrypted_id).toString();
                excludedIds.add(descryptedId);
            }
        }

        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        if (r.__dataReqType == null || r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.DATA) {
            List<Integrateddecision> retEntities= getIntegrateddecisionFacade().findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision((r.demaId_demaId != null && !r.demaId_demaId.startsWith("TEMP_ID_") ? (r.demaId_demaId != null ? usrSession.DecryptInteger(r.demaId_demaId) : null) : null), r.fsch_decisionCode, r.fsch_prodCode, r.fsch_Code, r.fsch_Parcel_Identifier, (r.fsch_CultDeclCode_cultId != null && !r.fsch_CultDeclCode_cultId.startsWith("TEMP_ID_") ? (r.fsch_CultDeclCode_cultId != null ? usrSession.DecryptInteger(r.fsch_CultDeclCode_cultId) : null) : null), range, recordCount, r.sortField, r.sortOrder != null ? r.sortOrder : false, excludedIds);
            ret.data = retEntities;
            ret.count = recordCount[0];
        } else if (r.__dataReqType != null && r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.COUNT) {
            ret.data = new ArrayList<>();
            ret.count = getIntegrateddecisionFacade().findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision_count((r.demaId_demaId != null && !r.demaId_demaId.startsWith("TEMP_ID_") ? (r.demaId_demaId != null ? usrSession.DecryptInteger(r.demaId_demaId) : null) : null), r.fsch_decisionCode, r.fsch_prodCode, r.fsch_Code, r.fsch_Parcel_Identifier, (r.fsch_CultDeclCode_cultId != null && !r.fsch_CultDeclCode_cultId.startsWith("TEMP_ID_") ? (r.fsch_CultDeclCode_cultId != null ? usrSession.DecryptInteger(r.fsch_CultDeclCode_cultId) : null) : null), excludedIds);
        } else {
            throw new GenericApplicationException("Unknown_DataRequestType");
        }
        return ret;
    }


    public static class GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_request {
        public String globalDema_demaId;
        public BigDecimal xmin;
        public BigDecimal ymin;
        public BigDecimal xmax;
        public BigDecimal ymax;
        public String selectedGeometry;
        public String geoFunc;
        public String geoFuncParam;
        public List<String> idsInCache;
    }

    public List<gr.neuropublic.base.SqlQueryRow> getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_entities(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_request r,
        final gr.neuropublic.Niva.services.UserSession usrSession)
    {
        ArrayList<Pair<String, Object>> params =  new ArrayList<Pair<String, Object>>();
        String sqlQuery = 
            "select a.* " +
            "from { " +
            "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=1 AND ide.dema_id= :globalDema_demaId   " +
            "} a " +
            "where " +
            "  ST_MakeEnvelope(:xmin, :ymin, :xmax, :ymax, 4326)::geometry && (a.geom)::geometry " +
            "  and (10=10) " +
            "  and (:xmax - :xmin <= 500000) ";
            
        params.add(gr.neuropublic.mutil.base.Pair.create("globalDema_demaId", (Object)(r.globalDema_demaId != null && !r.globalDema_demaId.startsWith("TEMP_ID_") ? (r.globalDema_demaId != null ? usrSession.DecryptInteger(r.globalDema_demaId) : null) : null)));

        params.add(gr.neuropublic.mutil.base.Pair.create("xmin", (Object)r.xmin != null ? (Object)r.xmin : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("ymin", (Object)r.ymin != null ? (Object)r.ymin : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("xmax", (Object)r.xmax != null ? (Object)r.xmax : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("ymax", (Object)r.ymax != null ? (Object)r.ymax : BigDecimal.ZERO) );
      
            
        if (r.geoFunc != null &&  r.selectedGeometry != null) {
            org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
            try {
                com.vividsolutions.jts.geom.Geometry geo = gjson.read(r.selectedGeometry);
                geo.setSRID(4326);
                String rep;
                if (r.geoFunc.equals("WITHIN_DISTANCE")) {
                    rep = "SDO_WITHIN_DISTANCE(geom, :selectedGeometry,'DISTANCE=" +(r.geoFuncParam==null?"0":r.geoFuncParam) + " UNIT=KM ' )='TRUE'";
                } else if (r.geoFunc.equals("CONTAINS")) {
                    rep = "SDO_CONTAINS(geom, :selectedGeometry)='TRUE'";
                } else if (r.geoFunc.equals("INSIDE")) {
                    rep = "SDO_INSIDE(geom, :selectedGeometry)='TRUE'";
                } else if (r.geoFunc.equals("TOUCH")) {
                    rep = "SDO_TOUCH(geom, :selectedGeometry)='TRUE'";
                } else {
                    throw new GenericApplicationException("Unsupported spatial operator:" + r.geoFunc == null ? "null" : r.geoFunc);
                }
        
                sqlQuery = sqlQuery.replaceAll("10=10", rep);
                params.add(gr.neuropublic.mutil.base.Pair.create("selectedGeometry", (Object)geo));
        
            } catch (IOException ex) {
                logger.error("Error in reading geoJSON DATA ",ex);
                throw new GenericApplicationException(ex.getMessage());
            }
        }
              
        sqlQuery = sqlQuery.replace('{', '(').replace('}', ')');
        return getService().executeSqlQuery2(sqlQuery, params);
    }

    @POST
    @Path("/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            Set<String> ids = new HashSet<String>(r.idsInCache);

            List<gr.neuropublic.base.SqlQueryRow> entities = getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_entities(r, usrSession);
            gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();
            
            for(gr.neuropublic.base.SqlQueryRow r : entities) {
                Object id = r.getId();
                if (ids.contains(id.toString())) {
                    r.clear();
                    r.setId(id);
                }
            }

            ret.data = entities;
            ret.count = entities.size();
            String jsonStr = ret.toJson2();

            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }



    public static class GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter_request {
        public String globalDema_demaId;
        public BigDecimal xmin;
        public BigDecimal ymin;
        public BigDecimal xmax;
        public BigDecimal ymax;
        public String selectedGeometry;
        public String geoFunc;
        public String geoFuncParam;
        public List<String> idsInCache;
    }

    public List<gr.neuropublic.base.SqlQueryRow> getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter_entities(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter_request r,
        final gr.neuropublic.Niva.services.UserSession usrSession)
    {
        ArrayList<Pair<String, Object>> params =  new ArrayList<Pair<String, Object>>();
        String sqlQuery = 
            "select a.* " +
            "from { " +
            "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=1 AND ide.dema_id= :globalDema_demaId   " +
            "} a " +
            "where " +
            "  ST_MakeEnvelope(:xmin, :ymin, :xmax, :ymax, 4326)::geometry && (a.geom)::geometry " +
            "  and (10=10) " +
            "  and (:xmax - :xmin <= 500000) ";
            
        params.add(gr.neuropublic.mutil.base.Pair.create("globalDema_demaId", (Object)(r.globalDema_demaId != null && !r.globalDema_demaId.startsWith("TEMP_ID_") ? (r.globalDema_demaId != null ? usrSession.DecryptInteger(r.globalDema_demaId) : null) : null)));

        params.add(gr.neuropublic.mutil.base.Pair.create("xmin", (Object)r.xmin != null ? (Object)r.xmin : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("ymin", (Object)r.ymin != null ? (Object)r.ymin : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("xmax", (Object)r.xmax != null ? (Object)r.xmax : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("ymax", (Object)r.ymax != null ? (Object)r.ymax : BigDecimal.ZERO) );
      
            
        if (r.geoFunc != null &&  r.selectedGeometry != null) {
            org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
            try {
                com.vividsolutions.jts.geom.Geometry geo = gjson.read(r.selectedGeometry);
                geo.setSRID(4326);
                String rep;
                if (r.geoFunc.equals("WITHIN_DISTANCE")) {
                    rep = "SDO_WITHIN_DISTANCE(geom, :selectedGeometry,'DISTANCE=" +(r.geoFuncParam==null?"0":r.geoFuncParam) + " UNIT=KM ' )='TRUE'";
                } else if (r.geoFunc.equals("CONTAINS")) {
                    rep = "SDO_CONTAINS(geom, :selectedGeometry)='TRUE'";
                } else if (r.geoFunc.equals("INSIDE")) {
                    rep = "SDO_INSIDE(geom, :selectedGeometry)='TRUE'";
                } else if (r.geoFunc.equals("TOUCH")) {
                    rep = "SDO_TOUCH(geom, :selectedGeometry)='TRUE'";
                } else {
                    throw new GenericApplicationException("Unsupported spatial operator:" + r.geoFunc == null ? "null" : r.geoFunc);
                }
        
                sqlQuery = sqlQuery.replaceAll("10=10", rep);
                params.add(gr.neuropublic.mutil.base.Pair.create("selectedGeometry", (Object)geo));
        
            } catch (IOException ex) {
                logger.error("Error in reading geoJSON DATA ",ex);
                throw new GenericApplicationException(ex.getMessage());
            }
        }
              
        sqlQuery = sqlQuery.replace('{', '(').replace('}', ')');
        return getService().executeSqlQuery2(sqlQuery, params);
    }

    @POST
    @Path("/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            Set<String> ids = new HashSet<String>(r.idsInCache);

            List<gr.neuropublic.base.SqlQueryRow> entities = getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter_entities(r, usrSession);
            gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();
            
            for(gr.neuropublic.base.SqlQueryRow r : entities) {
                Object id = r.getId();
                if (ids.contains(id.toString())) {
                    r.clear();
                    r.setId(id);
                }
            }

            ret.data = entities;
            ret.count = entities.size();
            String jsonStr = ret.toJson2();

            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }



    public static class GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter_grid_request {
        public String globalDema_demaId;
        public BigDecimal fromRowIndex;
        public BigDecimal toRowIndex;
        public String sortField;
        public String sortOrder;
        public String selectedGeometry;
        public String geoFunc;
        public String geoFuncParam;
    }


    @POST
    @Path("/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter_grid")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter_grid(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter_grid_request r,
        @CookieParam("niva-session-id") final String sessionId) 
    {

        return handleWebRequest(sessionId, new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            ArrayList<Pair<String, Object>> params =  new ArrayList<Pair<String, Object>>();
            String sqlQuery = 
                "select *  " +
                "from { " +
                "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=1 AND ide.dema_id= :globalDema_demaId   " +
                "}   " +
                "where  " +
                "  (10=10) " +
                "limit :toRowIndex ";
            if (!BigDecimal.ZERO.equals(r.fromRowIndex)) {
                sqlQuery = 
                    "select row_.*, rownum as rownum__   " +
                    "from { " +
                    "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=1 AND ide.dema_id= :globalDema_demaId   " +
                    "} row_   " +
                    "where  " +
                    "  (10=10) " +
                    "offset :fromRowIndex limit :toRowIndex ";
            }
            String sqlQueryCnt = 
                "select count(1) as cnt  " +
                "from { " +
                "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=1 AND ide.dema_id= :globalDema_demaId   " +
                "}   " +
                "where (10=10) ";
            
            sqlQueryCnt = sqlQueryCnt.replaceAll("1=1", "rownum<=10000");


            
            params.add(gr.neuropublic.mutil.base.Pair.create("globalDema_demaId", (Object)(r.globalDema_demaId != null && !r.globalDema_demaId.startsWith("TEMP_ID_") ? (r.globalDema_demaId != null ? usrSession.DecryptInteger(r.globalDema_demaId) : null) : null)));
        
            params.add(gr.neuropublic.mutil.base.Pair.create("toRowIndex", (Object)r.toRowIndex));
            if (!BigDecimal.ZERO.equals(r.fromRowIndex)) {
                params.add(gr.neuropublic.mutil.base.Pair.create("fromRowIndex", (Object)r.fromRowIndex));
            }   

            if (r.geoFunc != null &&  r.selectedGeometry != null) {
                org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
                try {
                    com.vividsolutions.jts.geom.Geometry geo = gjson.read(r.selectedGeometry);
                    geo.setSRID(4326);
                    String rep;
                    if (r.geoFunc.equals("WITHIN_DISTANCE")) {
                        rep = "SDO_WITHIN_DISTANCE(geom, :selectedGeometry,'DISTANCE=" +(r.geoFuncParam==null?"0":r.geoFuncParam) + " UNIT=KM ' )='TRUE'";
                    } else if (r.geoFunc.equals("CONTAINS")) {
                        rep = "SDO_CONTAINS(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("INSIDE")) {
                        rep = "SDO_INSIDE(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("TOUCH")) {
                        rep = "SDO_TOUCH(geom, :selectedGeometry)='TRUE'";
                    } else {
                        throw new GenericApplicationException("Unsupported spatial operator:" + r.geoFunc == null ? "null" : r.geoFunc);
                    }
            
                    sqlQuery = sqlQuery.replaceAll("10=10", rep);
                    sqlQueryCnt = sqlQueryCnt.replaceAll("10=10", rep);
            
                    params.add(gr.neuropublic.mutil.base.Pair.create("selectedGeometry", (Object)geo));
            
                } catch (IOException ex) {
                    logger.error("Error in reading geoJSON DATA ",ex);
                    throw new GenericApplicationException(ex.getMessage());
                }
            }


            sqlQuery = sqlQuery.replace('{', '(').replace('}', ')');
            List<gr.neuropublic.base.SqlQueryRow> entities = getService().executeSqlQuery2(sqlQuery, params);

            params.remove(gr.neuropublic.mutil.base.Pair.create("toRowIndex", (Object)r.toRowIndex));
            if (params.contains(gr.neuropublic.mutil.base.Pair.create("fromRowIndex", (Object)r.fromRowIndex))) {
                params.remove(gr.neuropublic.mutil.base.Pair.create("fromRowIndex", (Object)r.fromRowIndex));
            }
            
            //sqlQueryCnt = sqlQueryCnt.replace('{', '(').replace('}', ')');
            //List<gr.neuropublic.base.SqlQueryRow> entitiesCnt = getService().executeSqlQuery2(sqlQueryCnt, params);

            gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

            
            ret.data = entities;
            ret.count = -1; //((BigDecimal)entitiesCnt.get(0).get("cnt")).intValue();
            String jsonStr = ret.toJson2();

            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }



    public static class GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter_geoJsonFile_request {
        public String globalDema_demaId;
        public String sortField;
        public String sortOrder;
        public String selectedGeometry;
        public String geoFunc;
        public String geoFuncParam;
    }


    @POST
    @Path("/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter_geoJsonFile")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter_geoJsonFile(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter_geoJsonFile_request r,
        @CookieParam("niva-session-id") final String sessionId) 
    {

        return handleWebRequest(sessionId, new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            ArrayList<Pair<String, Object>> params =  new ArrayList<Pair<String, Object>>();
            String sqlQuery = 
                "select *  " +
                "from { " +
                "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=1 AND ide.dema_id= :globalDema_demaId   " +
                "}   " +
                "where  " +
                "   (10=10) " +
                " " +
                "limit 10000 " +
                " ";

            
            params.add(gr.neuropublic.mutil.base.Pair.create("globalDema_demaId", (Object)(r.globalDema_demaId != null && !r.globalDema_demaId.startsWith("TEMP_ID_") ? (r.globalDema_demaId != null ? usrSession.DecryptInteger(r.globalDema_demaId) : null) : null)));
        

            if (r.geoFunc != null &&  r.selectedGeometry != null) {
                org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
                try {
                    com.vividsolutions.jts.geom.Geometry geo = gjson.read(r.selectedGeometry);
                    geo.setSRID(4326);
                    String rep;
                    if (r.geoFunc.equals("WITHIN_DISTANCE")) {
                        rep = "SDO_WITHIN_DISTANCE(geom, :selectedGeometry,'DISTANCE=" +(r.geoFuncParam==null?"0":r.geoFuncParam) + " UNIT=KM ' )='TRUE'";
                    } else if (r.geoFunc.equals("CONTAINS")) {
                        rep = "SDO_CONTAINS(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("INSIDE")) {
                        rep = "SDO_INSIDE(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("TOUCH")) {
                        rep = "SDO_TOUCH(geom, :selectedGeometry)='TRUE'";
                    } else {
                        throw new GenericApplicationException("Unsupported spatial operator:" + r.geoFunc == null ? "null" : r.geoFunc);
                    }
            
                    sqlQuery = sqlQuery.replaceAll("10=10", rep);
                    params.add(gr.neuropublic.mutil.base.Pair.create("selectedGeometry", (Object)geo));
            
                } catch (IOException ex) {
                    logger.error("Error in reading geoJSON DATA ",ex);
                    throw new GenericApplicationException(ex.getMessage());
                }
            }


            sqlQuery = sqlQuery.replace('{', '(').replace('}', ')');
            List<gr.neuropublic.base.SqlQueryRow> entities = getService().executeSqlQuery2(sqlQuery, params);

            JsonBlob rsp = new JsonBlob();
            gr.neuropublic.wsrestutils.LazyData lzData = new gr.neuropublic.wsrestutils.LazyData();
            lzData.data = entities;
            lzData.count = entities.size();
            rsp.data = lzData.toGeoJson(4326);


            return Response.status(200).type("application/json;charset=UTF-8").entity(rsp).build();
        }});
    }


    public static class GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter_excelFile_request {
        public String globalDema_demaId;
        public String sortField;
        public String sortOrder;
        public String selectedGeometry;
        public String geoFunc;
        public String geoFuncParam;
    }


    @POST
    @Path("/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter_excelFile")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter_excelFile(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen_filter_excelFile_request r,
        @CookieParam("niva-session-id") final String sessionId) 
    {

        return handleWebRequest(sessionId, new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            ArrayList<Pair<String, Object>> params =  new ArrayList<Pair<String, Object>>();
            String sqlQuery = 
                "select *  " +
                "from { " +
                "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=1 AND ide.dema_id= :globalDema_demaId   " +
                "}   " +
                "where  " +
                "   (10=10) " +
                " " +
                "limit 10000 " +
                " ";

            
            params.add(gr.neuropublic.mutil.base.Pair.create("globalDema_demaId", (Object)(r.globalDema_demaId != null && !r.globalDema_demaId.startsWith("TEMP_ID_") ? (r.globalDema_demaId != null ? usrSession.DecryptInteger(r.globalDema_demaId) : null) : null)));
        

            if (r.geoFunc != null &&  r.selectedGeometry != null) {
                org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
                try {
                    com.vividsolutions.jts.geom.Geometry geo = gjson.read(r.selectedGeometry);
                    geo.setSRID(4326);
                    String rep;
                    if (r.geoFunc.equals("WITHIN_DISTANCE")) {
                        rep = "SDO_WITHIN_DISTANCE(geom, :selectedGeometry,'DISTANCE=" +(r.geoFuncParam==null?"0":r.geoFuncParam) + " UNIT=KM ' )='TRUE'";
                    } else if (r.geoFunc.equals("CONTAINS")) {
                        rep = "SDO_CONTAINS(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("INSIDE")) {
                        rep = "SDO_INSIDE(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("TOUCH")) {
                        rep = "SDO_TOUCH(geom, :selectedGeometry)='TRUE'";
                    } else {
                        throw new GenericApplicationException("Unsupported spatial operator:" + r.geoFunc == null ? "null" : r.geoFunc);
                    }
            
                    sqlQuery = sqlQuery.replaceAll("10=10", rep);
                    params.add(gr.neuropublic.mutil.base.Pair.create("selectedGeometry", (Object)geo));
            
                } catch (IOException ex) {
                    logger.error("Error in reading geoJSON DATA ",ex);
                    throw new GenericApplicationException(ex.getMessage());
                }
            }


            sqlQuery = sqlQuery.replace('{', '(').replace('}', ')');
            List<gr.neuropublic.base.SqlQueryRow> entities = getService().executeSqlQuery2(sqlQuery, params);

            JsonBlob rsp = new JsonBlob();
            ArrayList<Pair<String,String>> colDefs = new ArrayList<Pair<String,String>>();

            rsp.data = gr.neuropublic.base.SqlQueryRow.ExportToExcel(colDefs, entities);


            return Response.status(200).type("application/json;charset=UTF-8").entity(rsp).build();
        }});
    }


    public static class GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_request {
        public String globalDema_demaId;
        public BigDecimal xmin;
        public BigDecimal ymin;
        public BigDecimal xmax;
        public BigDecimal ymax;
        public String selectedGeometry;
        public String geoFunc;
        public String geoFuncParam;
        public List<String> idsInCache;
    }

    public List<gr.neuropublic.base.SqlQueryRow> getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_entities(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_request r,
        final gr.neuropublic.Niva.services.UserSession usrSession)
    {
        ArrayList<Pair<String, Object>> params =  new ArrayList<Pair<String, Object>>();
        String sqlQuery = 
            "select a.* " +
            "from { " +
            "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=2 AND ide.dema_id= :globalDema_demaId   " +
            "} a " +
            "where " +
            "  ST_MakeEnvelope(:xmin, :ymin, :xmax, :ymax, 4326)::geometry && (a.geom)::geometry " +
            "  and (10=10) " +
            "  and (:xmax - :xmin <= 500000) ";
            
        params.add(gr.neuropublic.mutil.base.Pair.create("globalDema_demaId", (Object)(r.globalDema_demaId != null && !r.globalDema_demaId.startsWith("TEMP_ID_") ? (r.globalDema_demaId != null ? usrSession.DecryptInteger(r.globalDema_demaId) : null) : null)));

        params.add(gr.neuropublic.mutil.base.Pair.create("xmin", (Object)r.xmin != null ? (Object)r.xmin : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("ymin", (Object)r.ymin != null ? (Object)r.ymin : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("xmax", (Object)r.xmax != null ? (Object)r.xmax : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("ymax", (Object)r.ymax != null ? (Object)r.ymax : BigDecimal.ZERO) );
      
            
        if (r.geoFunc != null &&  r.selectedGeometry != null) {
            org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
            try {
                com.vividsolutions.jts.geom.Geometry geo = gjson.read(r.selectedGeometry);
                geo.setSRID(4326);
                String rep;
                if (r.geoFunc.equals("WITHIN_DISTANCE")) {
                    rep = "SDO_WITHIN_DISTANCE(geom, :selectedGeometry,'DISTANCE=" +(r.geoFuncParam==null?"0":r.geoFuncParam) + " UNIT=KM ' )='TRUE'";
                } else if (r.geoFunc.equals("CONTAINS")) {
                    rep = "SDO_CONTAINS(geom, :selectedGeometry)='TRUE'";
                } else if (r.geoFunc.equals("INSIDE")) {
                    rep = "SDO_INSIDE(geom, :selectedGeometry)='TRUE'";
                } else if (r.geoFunc.equals("TOUCH")) {
                    rep = "SDO_TOUCH(geom, :selectedGeometry)='TRUE'";
                } else {
                    throw new GenericApplicationException("Unsupported spatial operator:" + r.geoFunc == null ? "null" : r.geoFunc);
                }
        
                sqlQuery = sqlQuery.replaceAll("10=10", rep);
                params.add(gr.neuropublic.mutil.base.Pair.create("selectedGeometry", (Object)geo));
        
            } catch (IOException ex) {
                logger.error("Error in reading geoJSON DATA ",ex);
                throw new GenericApplicationException(ex.getMessage());
            }
        }
              
        sqlQuery = sqlQuery.replace('{', '(').replace('}', ')');
        return getService().executeSqlQuery2(sqlQuery, params);
    }

    @POST
    @Path("/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            Set<String> ids = new HashSet<String>(r.idsInCache);

            List<gr.neuropublic.base.SqlQueryRow> entities = getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_entities(r, usrSession);
            gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();
            
            for(gr.neuropublic.base.SqlQueryRow r : entities) {
                Object id = r.getId();
                if (ids.contains(id.toString())) {
                    r.clear();
                    r.setId(id);
                }
            }

            ret.data = entities;
            ret.count = entities.size();
            String jsonStr = ret.toJson2();

            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }



    public static class GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter_request {
        public String globalDema_demaId;
        public BigDecimal xmin;
        public BigDecimal ymin;
        public BigDecimal xmax;
        public BigDecimal ymax;
        public String selectedGeometry;
        public String geoFunc;
        public String geoFuncParam;
        public List<String> idsInCache;
    }

    public List<gr.neuropublic.base.SqlQueryRow> getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter_entities(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter_request r,
        final gr.neuropublic.Niva.services.UserSession usrSession)
    {
        ArrayList<Pair<String, Object>> params =  new ArrayList<Pair<String, Object>>();
        String sqlQuery = 
            "select a.* " +
            "from { " +
            "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=2 AND ide.dema_id= :globalDema_demaId   " +
            "} a " +
            "where " +
            "  ST_MakeEnvelope(:xmin, :ymin, :xmax, :ymax, 4326)::geometry && (a.geom)::geometry " +
            "  and (10=10) " +
            "  and (:xmax - :xmin <= 500000) ";
            
        params.add(gr.neuropublic.mutil.base.Pair.create("globalDema_demaId", (Object)(r.globalDema_demaId != null && !r.globalDema_demaId.startsWith("TEMP_ID_") ? (r.globalDema_demaId != null ? usrSession.DecryptInteger(r.globalDema_demaId) : null) : null)));

        params.add(gr.neuropublic.mutil.base.Pair.create("xmin", (Object)r.xmin != null ? (Object)r.xmin : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("ymin", (Object)r.ymin != null ? (Object)r.ymin : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("xmax", (Object)r.xmax != null ? (Object)r.xmax : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("ymax", (Object)r.ymax != null ? (Object)r.ymax : BigDecimal.ZERO) );
      
            
        if (r.geoFunc != null &&  r.selectedGeometry != null) {
            org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
            try {
                com.vividsolutions.jts.geom.Geometry geo = gjson.read(r.selectedGeometry);
                geo.setSRID(4326);
                String rep;
                if (r.geoFunc.equals("WITHIN_DISTANCE")) {
                    rep = "SDO_WITHIN_DISTANCE(geom, :selectedGeometry,'DISTANCE=" +(r.geoFuncParam==null?"0":r.geoFuncParam) + " UNIT=KM ' )='TRUE'";
                } else if (r.geoFunc.equals("CONTAINS")) {
                    rep = "SDO_CONTAINS(geom, :selectedGeometry)='TRUE'";
                } else if (r.geoFunc.equals("INSIDE")) {
                    rep = "SDO_INSIDE(geom, :selectedGeometry)='TRUE'";
                } else if (r.geoFunc.equals("TOUCH")) {
                    rep = "SDO_TOUCH(geom, :selectedGeometry)='TRUE'";
                } else {
                    throw new GenericApplicationException("Unsupported spatial operator:" + r.geoFunc == null ? "null" : r.geoFunc);
                }
        
                sqlQuery = sqlQuery.replaceAll("10=10", rep);
                params.add(gr.neuropublic.mutil.base.Pair.create("selectedGeometry", (Object)geo));
        
            } catch (IOException ex) {
                logger.error("Error in reading geoJSON DATA ",ex);
                throw new GenericApplicationException(ex.getMessage());
            }
        }
              
        sqlQuery = sqlQuery.replace('{', '(').replace('}', ')');
        return getService().executeSqlQuery2(sqlQuery, params);
    }

    @POST
    @Path("/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            Set<String> ids = new HashSet<String>(r.idsInCache);

            List<gr.neuropublic.base.SqlQueryRow> entities = getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter_entities(r, usrSession);
            gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();
            
            for(gr.neuropublic.base.SqlQueryRow r : entities) {
                Object id = r.getId();
                if (ids.contains(id.toString())) {
                    r.clear();
                    r.setId(id);
                }
            }

            ret.data = entities;
            ret.count = entities.size();
            String jsonStr = ret.toJson2();

            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }



    public static class GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter_grid_request {
        public String globalDema_demaId;
        public BigDecimal fromRowIndex;
        public BigDecimal toRowIndex;
        public String sortField;
        public String sortOrder;
        public String selectedGeometry;
        public String geoFunc;
        public String geoFuncParam;
    }


    @POST
    @Path("/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter_grid")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter_grid(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter_grid_request r,
        @CookieParam("niva-session-id") final String sessionId) 
    {

        return handleWebRequest(sessionId, new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            ArrayList<Pair<String, Object>> params =  new ArrayList<Pair<String, Object>>();
            String sqlQuery = 
                "select *  " +
                "from { " +
                "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=2 AND ide.dema_id= :globalDema_demaId   " +
                "}   " +
                "where  " +
                "  (10=10) " +
                "limit :toRowIndex ";
            if (!BigDecimal.ZERO.equals(r.fromRowIndex)) {
                sqlQuery = 
                    "select row_.*, rownum as rownum__   " +
                    "from { " +
                    "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=2 AND ide.dema_id= :globalDema_demaId   " +
                    "} row_   " +
                    "where  " +
                    "  (10=10) " +
                    "offset :fromRowIndex limit :toRowIndex ";
            }
            String sqlQueryCnt = 
                "select count(1) as cnt  " +
                "from { " +
                "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=2 AND ide.dema_id= :globalDema_demaId   " +
                "}   " +
                "where (10=10) ";
            
            sqlQueryCnt = sqlQueryCnt.replaceAll("1=1", "rownum<=10000");


            
            params.add(gr.neuropublic.mutil.base.Pair.create("globalDema_demaId", (Object)(r.globalDema_demaId != null && !r.globalDema_demaId.startsWith("TEMP_ID_") ? (r.globalDema_demaId != null ? usrSession.DecryptInteger(r.globalDema_demaId) : null) : null)));
        
            params.add(gr.neuropublic.mutil.base.Pair.create("toRowIndex", (Object)r.toRowIndex));
            if (!BigDecimal.ZERO.equals(r.fromRowIndex)) {
                params.add(gr.neuropublic.mutil.base.Pair.create("fromRowIndex", (Object)r.fromRowIndex));
            }   

            if (r.geoFunc != null &&  r.selectedGeometry != null) {
                org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
                try {
                    com.vividsolutions.jts.geom.Geometry geo = gjson.read(r.selectedGeometry);
                    geo.setSRID(4326);
                    String rep;
                    if (r.geoFunc.equals("WITHIN_DISTANCE")) {
                        rep = "SDO_WITHIN_DISTANCE(geom, :selectedGeometry,'DISTANCE=" +(r.geoFuncParam==null?"0":r.geoFuncParam) + " UNIT=KM ' )='TRUE'";
                    } else if (r.geoFunc.equals("CONTAINS")) {
                        rep = "SDO_CONTAINS(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("INSIDE")) {
                        rep = "SDO_INSIDE(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("TOUCH")) {
                        rep = "SDO_TOUCH(geom, :selectedGeometry)='TRUE'";
                    } else {
                        throw new GenericApplicationException("Unsupported spatial operator:" + r.geoFunc == null ? "null" : r.geoFunc);
                    }
            
                    sqlQuery = sqlQuery.replaceAll("10=10", rep);
                    sqlQueryCnt = sqlQueryCnt.replaceAll("10=10", rep);
            
                    params.add(gr.neuropublic.mutil.base.Pair.create("selectedGeometry", (Object)geo));
            
                } catch (IOException ex) {
                    logger.error("Error in reading geoJSON DATA ",ex);
                    throw new GenericApplicationException(ex.getMessage());
                }
            }


            sqlQuery = sqlQuery.replace('{', '(').replace('}', ')');
            List<gr.neuropublic.base.SqlQueryRow> entities = getService().executeSqlQuery2(sqlQuery, params);

            params.remove(gr.neuropublic.mutil.base.Pair.create("toRowIndex", (Object)r.toRowIndex));
            if (params.contains(gr.neuropublic.mutil.base.Pair.create("fromRowIndex", (Object)r.fromRowIndex))) {
                params.remove(gr.neuropublic.mutil.base.Pair.create("fromRowIndex", (Object)r.fromRowIndex));
            }
            
            //sqlQueryCnt = sqlQueryCnt.replace('{', '(').replace('}', ')');
            //List<gr.neuropublic.base.SqlQueryRow> entitiesCnt = getService().executeSqlQuery2(sqlQueryCnt, params);

            gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

            
            ret.data = entities;
            ret.count = -1; //((BigDecimal)entitiesCnt.get(0).get("cnt")).intValue();
            String jsonStr = ret.toJson2();

            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }



    public static class GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter_geoJsonFile_request {
        public String globalDema_demaId;
        public String sortField;
        public String sortOrder;
        public String selectedGeometry;
        public String geoFunc;
        public String geoFuncParam;
    }


    @POST
    @Path("/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter_geoJsonFile")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter_geoJsonFile(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter_geoJsonFile_request r,
        @CookieParam("niva-session-id") final String sessionId) 
    {

        return handleWebRequest(sessionId, new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            ArrayList<Pair<String, Object>> params =  new ArrayList<Pair<String, Object>>();
            String sqlQuery = 
                "select *  " +
                "from { " +
                "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=2 AND ide.dema_id= :globalDema_demaId   " +
                "}   " +
                "where  " +
                "   (10=10) " +
                " " +
                "limit 10000 " +
                " ";

            
            params.add(gr.neuropublic.mutil.base.Pair.create("globalDema_demaId", (Object)(r.globalDema_demaId != null && !r.globalDema_demaId.startsWith("TEMP_ID_") ? (r.globalDema_demaId != null ? usrSession.DecryptInteger(r.globalDema_demaId) : null) : null)));
        

            if (r.geoFunc != null &&  r.selectedGeometry != null) {
                org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
                try {
                    com.vividsolutions.jts.geom.Geometry geo = gjson.read(r.selectedGeometry);
                    geo.setSRID(4326);
                    String rep;
                    if (r.geoFunc.equals("WITHIN_DISTANCE")) {
                        rep = "SDO_WITHIN_DISTANCE(geom, :selectedGeometry,'DISTANCE=" +(r.geoFuncParam==null?"0":r.geoFuncParam) + " UNIT=KM ' )='TRUE'";
                    } else if (r.geoFunc.equals("CONTAINS")) {
                        rep = "SDO_CONTAINS(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("INSIDE")) {
                        rep = "SDO_INSIDE(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("TOUCH")) {
                        rep = "SDO_TOUCH(geom, :selectedGeometry)='TRUE'";
                    } else {
                        throw new GenericApplicationException("Unsupported spatial operator:" + r.geoFunc == null ? "null" : r.geoFunc);
                    }
            
                    sqlQuery = sqlQuery.replaceAll("10=10", rep);
                    params.add(gr.neuropublic.mutil.base.Pair.create("selectedGeometry", (Object)geo));
            
                } catch (IOException ex) {
                    logger.error("Error in reading geoJSON DATA ",ex);
                    throw new GenericApplicationException(ex.getMessage());
                }
            }


            sqlQuery = sqlQuery.replace('{', '(').replace('}', ')');
            List<gr.neuropublic.base.SqlQueryRow> entities = getService().executeSqlQuery2(sqlQuery, params);

            JsonBlob rsp = new JsonBlob();
            gr.neuropublic.wsrestutils.LazyData lzData = new gr.neuropublic.wsrestutils.LazyData();
            lzData.data = entities;
            lzData.count = entities.size();
            rsp.data = lzData.toGeoJson(4326);


            return Response.status(200).type("application/json;charset=UTF-8").entity(rsp).build();
        }});
    }


    public static class GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter_excelFile_request {
        public String globalDema_demaId;
        public String sortField;
        public String sortOrder;
        public String selectedGeometry;
        public String geoFunc;
        public String geoFuncParam;
    }


    @POST
    @Path("/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter_excelFile")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter_excelFile(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow_filter_excelFile_request r,
        @CookieParam("niva-session-id") final String sessionId) 
    {

        return handleWebRequest(sessionId, new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            ArrayList<Pair<String, Object>> params =  new ArrayList<Pair<String, Object>>();
            String sqlQuery = 
                "select *  " +
                "from { " +
                "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=2 AND ide.dema_id= :globalDema_demaId   " +
                "}   " +
                "where  " +
                "   (10=10) " +
                " " +
                "limit 10000 " +
                " ";

            
            params.add(gr.neuropublic.mutil.base.Pair.create("globalDema_demaId", (Object)(r.globalDema_demaId != null && !r.globalDema_demaId.startsWith("TEMP_ID_") ? (r.globalDema_demaId != null ? usrSession.DecryptInteger(r.globalDema_demaId) : null) : null)));
        

            if (r.geoFunc != null &&  r.selectedGeometry != null) {
                org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
                try {
                    com.vividsolutions.jts.geom.Geometry geo = gjson.read(r.selectedGeometry);
                    geo.setSRID(4326);
                    String rep;
                    if (r.geoFunc.equals("WITHIN_DISTANCE")) {
                        rep = "SDO_WITHIN_DISTANCE(geom, :selectedGeometry,'DISTANCE=" +(r.geoFuncParam==null?"0":r.geoFuncParam) + " UNIT=KM ' )='TRUE'";
                    } else if (r.geoFunc.equals("CONTAINS")) {
                        rep = "SDO_CONTAINS(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("INSIDE")) {
                        rep = "SDO_INSIDE(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("TOUCH")) {
                        rep = "SDO_TOUCH(geom, :selectedGeometry)='TRUE'";
                    } else {
                        throw new GenericApplicationException("Unsupported spatial operator:" + r.geoFunc == null ? "null" : r.geoFunc);
                    }
            
                    sqlQuery = sqlQuery.replaceAll("10=10", rep);
                    params.add(gr.neuropublic.mutil.base.Pair.create("selectedGeometry", (Object)geo));
            
                } catch (IOException ex) {
                    logger.error("Error in reading geoJSON DATA ",ex);
                    throw new GenericApplicationException(ex.getMessage());
                }
            }


            sqlQuery = sqlQuery.replace('{', '(').replace('}', ')');
            List<gr.neuropublic.base.SqlQueryRow> entities = getService().executeSqlQuery2(sqlQuery, params);

            JsonBlob rsp = new JsonBlob();
            ArrayList<Pair<String,String>> colDefs = new ArrayList<Pair<String,String>>();

            rsp.data = gr.neuropublic.base.SqlQueryRow.ExportToExcel(colDefs, entities);


            return Response.status(200).type("application/json;charset=UTF-8").entity(rsp).build();
        }});
    }


    public static class GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_request {
        public String globalDema_demaId;
        public BigDecimal xmin;
        public BigDecimal ymin;
        public BigDecimal xmax;
        public BigDecimal ymax;
        public String selectedGeometry;
        public String geoFunc;
        public String geoFuncParam;
        public List<String> idsInCache;
    }

    public List<gr.neuropublic.base.SqlQueryRow> getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_entities(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_request r,
        final gr.neuropublic.Niva.services.UserSession usrSession)
    {
        ArrayList<Pair<String, Object>> params =  new ArrayList<Pair<String, Object>>();
        String sqlQuery = 
            "select a.* " +
            "from { " +
            "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=3 AND ide.dema_id= :globalDema_demaId   " +
            "} a " +
            "where " +
            "  ST_MakeEnvelope(:xmin, :ymin, :xmax, :ymax, 4326)::geometry && (a.geom)::geometry " +
            "  and (10=10) " +
            "  and (:xmax - :xmin <= 500000) ";
            
        params.add(gr.neuropublic.mutil.base.Pair.create("globalDema_demaId", (Object)(r.globalDema_demaId != null && !r.globalDema_demaId.startsWith("TEMP_ID_") ? (r.globalDema_demaId != null ? usrSession.DecryptInteger(r.globalDema_demaId) : null) : null)));

        params.add(gr.neuropublic.mutil.base.Pair.create("xmin", (Object)r.xmin != null ? (Object)r.xmin : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("ymin", (Object)r.ymin != null ? (Object)r.ymin : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("xmax", (Object)r.xmax != null ? (Object)r.xmax : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("ymax", (Object)r.ymax != null ? (Object)r.ymax : BigDecimal.ZERO) );
      
            
        if (r.geoFunc != null &&  r.selectedGeometry != null) {
            org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
            try {
                com.vividsolutions.jts.geom.Geometry geo = gjson.read(r.selectedGeometry);
                geo.setSRID(4326);
                String rep;
                if (r.geoFunc.equals("WITHIN_DISTANCE")) {
                    rep = "SDO_WITHIN_DISTANCE(geom, :selectedGeometry,'DISTANCE=" +(r.geoFuncParam==null?"0":r.geoFuncParam) + " UNIT=KM ' )='TRUE'";
                } else if (r.geoFunc.equals("CONTAINS")) {
                    rep = "SDO_CONTAINS(geom, :selectedGeometry)='TRUE'";
                } else if (r.geoFunc.equals("INSIDE")) {
                    rep = "SDO_INSIDE(geom, :selectedGeometry)='TRUE'";
                } else if (r.geoFunc.equals("TOUCH")) {
                    rep = "SDO_TOUCH(geom, :selectedGeometry)='TRUE'";
                } else {
                    throw new GenericApplicationException("Unsupported spatial operator:" + r.geoFunc == null ? "null" : r.geoFunc);
                }
        
                sqlQuery = sqlQuery.replaceAll("10=10", rep);
                params.add(gr.neuropublic.mutil.base.Pair.create("selectedGeometry", (Object)geo));
        
            } catch (IOException ex) {
                logger.error("Error in reading geoJSON DATA ",ex);
                throw new GenericApplicationException(ex.getMessage());
            }
        }
              
        sqlQuery = sqlQuery.replace('{', '(').replace('}', ')');
        return getService().executeSqlQuery2(sqlQuery, params);
    }

    @POST
    @Path("/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            Set<String> ids = new HashSet<String>(r.idsInCache);

            List<gr.neuropublic.base.SqlQueryRow> entities = getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_entities(r, usrSession);
            gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();
            
            for(gr.neuropublic.base.SqlQueryRow r : entities) {
                Object id = r.getId();
                if (ids.contains(id.toString())) {
                    r.clear();
                    r.setId(id);
                }
            }

            ret.data = entities;
            ret.count = entities.size();
            String jsonStr = ret.toJson2();

            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }



    public static class GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter_request {
        public String globalDema_demaId;
        public BigDecimal xmin;
        public BigDecimal ymin;
        public BigDecimal xmax;
        public BigDecimal ymax;
        public String selectedGeometry;
        public String geoFunc;
        public String geoFuncParam;
        public List<String> idsInCache;
    }

    public List<gr.neuropublic.base.SqlQueryRow> getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter_entities(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter_request r,
        final gr.neuropublic.Niva.services.UserSession usrSession)
    {
        ArrayList<Pair<String, Object>> params =  new ArrayList<Pair<String, Object>>();
        String sqlQuery = 
            "select a.* " +
            "from { " +
            "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=3 AND ide.dema_id= :globalDema_demaId   " +
            "} a " +
            "where " +
            "  ST_MakeEnvelope(:xmin, :ymin, :xmax, :ymax, 4326)::geometry && (a.geom)::geometry " +
            "  and (10=10) " +
            "  and (:xmax - :xmin <= 500000) ";
            
        params.add(gr.neuropublic.mutil.base.Pair.create("globalDema_demaId", (Object)(r.globalDema_demaId != null && !r.globalDema_demaId.startsWith("TEMP_ID_") ? (r.globalDema_demaId != null ? usrSession.DecryptInteger(r.globalDema_demaId) : null) : null)));

        params.add(gr.neuropublic.mutil.base.Pair.create("xmin", (Object)r.xmin != null ? (Object)r.xmin : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("ymin", (Object)r.ymin != null ? (Object)r.ymin : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("xmax", (Object)r.xmax != null ? (Object)r.xmax : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("ymax", (Object)r.ymax != null ? (Object)r.ymax : BigDecimal.ZERO) );
      
            
        if (r.geoFunc != null &&  r.selectedGeometry != null) {
            org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
            try {
                com.vividsolutions.jts.geom.Geometry geo = gjson.read(r.selectedGeometry);
                geo.setSRID(4326);
                String rep;
                if (r.geoFunc.equals("WITHIN_DISTANCE")) {
                    rep = "SDO_WITHIN_DISTANCE(geom, :selectedGeometry,'DISTANCE=" +(r.geoFuncParam==null?"0":r.geoFuncParam) + " UNIT=KM ' )='TRUE'";
                } else if (r.geoFunc.equals("CONTAINS")) {
                    rep = "SDO_CONTAINS(geom, :selectedGeometry)='TRUE'";
                } else if (r.geoFunc.equals("INSIDE")) {
                    rep = "SDO_INSIDE(geom, :selectedGeometry)='TRUE'";
                } else if (r.geoFunc.equals("TOUCH")) {
                    rep = "SDO_TOUCH(geom, :selectedGeometry)='TRUE'";
                } else {
                    throw new GenericApplicationException("Unsupported spatial operator:" + r.geoFunc == null ? "null" : r.geoFunc);
                }
        
                sqlQuery = sqlQuery.replaceAll("10=10", rep);
                params.add(gr.neuropublic.mutil.base.Pair.create("selectedGeometry", (Object)geo));
        
            } catch (IOException ex) {
                logger.error("Error in reading geoJSON DATA ",ex);
                throw new GenericApplicationException(ex.getMessage());
            }
        }
              
        sqlQuery = sqlQuery.replace('{', '(').replace('}', ')');
        return getService().executeSqlQuery2(sqlQuery, params);
    }

    @POST
    @Path("/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            Set<String> ids = new HashSet<String>(r.idsInCache);

            List<gr.neuropublic.base.SqlQueryRow> entities = getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter_entities(r, usrSession);
            gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();
            
            for(gr.neuropublic.base.SqlQueryRow r : entities) {
                Object id = r.getId();
                if (ids.contains(id.toString())) {
                    r.clear();
                    r.setId(id);
                }
            }

            ret.data = entities;
            ret.count = entities.size();
            String jsonStr = ret.toJson2();

            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }



    public static class GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter_grid_request {
        public String globalDema_demaId;
        public BigDecimal fromRowIndex;
        public BigDecimal toRowIndex;
        public String sortField;
        public String sortOrder;
        public String selectedGeometry;
        public String geoFunc;
        public String geoFuncParam;
    }


    @POST
    @Path("/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter_grid")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter_grid(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter_grid_request r,
        @CookieParam("niva-session-id") final String sessionId) 
    {

        return handleWebRequest(sessionId, new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            ArrayList<Pair<String, Object>> params =  new ArrayList<Pair<String, Object>>();
            String sqlQuery = 
                "select *  " +
                "from { " +
                "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=3 AND ide.dema_id= :globalDema_demaId   " +
                "}   " +
                "where  " +
                "  (10=10) " +
                "limit :toRowIndex ";
            if (!BigDecimal.ZERO.equals(r.fromRowIndex)) {
                sqlQuery = 
                    "select row_.*, rownum as rownum__   " +
                    "from { " +
                    "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=3 AND ide.dema_id= :globalDema_demaId   " +
                    "} row_   " +
                    "where  " +
                    "  (10=10) " +
                    "offset :fromRowIndex limit :toRowIndex ";
            }
            String sqlQueryCnt = 
                "select count(1) as cnt  " +
                "from { " +
                "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=3 AND ide.dema_id= :globalDema_demaId   " +
                "}   " +
                "where (10=10) ";
            
            sqlQueryCnt = sqlQueryCnt.replaceAll("1=1", "rownum<=10000");


            
            params.add(gr.neuropublic.mutil.base.Pair.create("globalDema_demaId", (Object)(r.globalDema_demaId != null && !r.globalDema_demaId.startsWith("TEMP_ID_") ? (r.globalDema_demaId != null ? usrSession.DecryptInteger(r.globalDema_demaId) : null) : null)));
        
            params.add(gr.neuropublic.mutil.base.Pair.create("toRowIndex", (Object)r.toRowIndex));
            if (!BigDecimal.ZERO.equals(r.fromRowIndex)) {
                params.add(gr.neuropublic.mutil.base.Pair.create("fromRowIndex", (Object)r.fromRowIndex));
            }   

            if (r.geoFunc != null &&  r.selectedGeometry != null) {
                org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
                try {
                    com.vividsolutions.jts.geom.Geometry geo = gjson.read(r.selectedGeometry);
                    geo.setSRID(4326);
                    String rep;
                    if (r.geoFunc.equals("WITHIN_DISTANCE")) {
                        rep = "SDO_WITHIN_DISTANCE(geom, :selectedGeometry,'DISTANCE=" +(r.geoFuncParam==null?"0":r.geoFuncParam) + " UNIT=KM ' )='TRUE'";
                    } else if (r.geoFunc.equals("CONTAINS")) {
                        rep = "SDO_CONTAINS(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("INSIDE")) {
                        rep = "SDO_INSIDE(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("TOUCH")) {
                        rep = "SDO_TOUCH(geom, :selectedGeometry)='TRUE'";
                    } else {
                        throw new GenericApplicationException("Unsupported spatial operator:" + r.geoFunc == null ? "null" : r.geoFunc);
                    }
            
                    sqlQuery = sqlQuery.replaceAll("10=10", rep);
                    sqlQueryCnt = sqlQueryCnt.replaceAll("10=10", rep);
            
                    params.add(gr.neuropublic.mutil.base.Pair.create("selectedGeometry", (Object)geo));
            
                } catch (IOException ex) {
                    logger.error("Error in reading geoJSON DATA ",ex);
                    throw new GenericApplicationException(ex.getMessage());
                }
            }


            sqlQuery = sqlQuery.replace('{', '(').replace('}', ')');
            List<gr.neuropublic.base.SqlQueryRow> entities = getService().executeSqlQuery2(sqlQuery, params);

            params.remove(gr.neuropublic.mutil.base.Pair.create("toRowIndex", (Object)r.toRowIndex));
            if (params.contains(gr.neuropublic.mutil.base.Pair.create("fromRowIndex", (Object)r.fromRowIndex))) {
                params.remove(gr.neuropublic.mutil.base.Pair.create("fromRowIndex", (Object)r.fromRowIndex));
            }
            
            //sqlQueryCnt = sqlQueryCnt.replace('{', '(').replace('}', ')');
            //List<gr.neuropublic.base.SqlQueryRow> entitiesCnt = getService().executeSqlQuery2(sqlQueryCnt, params);

            gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

            
            ret.data = entities;
            ret.count = -1; //((BigDecimal)entitiesCnt.get(0).get("cnt")).intValue();
            String jsonStr = ret.toJson2();

            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }



    public static class GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter_geoJsonFile_request {
        public String globalDema_demaId;
        public String sortField;
        public String sortOrder;
        public String selectedGeometry;
        public String geoFunc;
        public String geoFuncParam;
    }


    @POST
    @Path("/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter_geoJsonFile")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter_geoJsonFile(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter_geoJsonFile_request r,
        @CookieParam("niva-session-id") final String sessionId) 
    {

        return handleWebRequest(sessionId, new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            ArrayList<Pair<String, Object>> params =  new ArrayList<Pair<String, Object>>();
            String sqlQuery = 
                "select *  " +
                "from { " +
                "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=3 AND ide.dema_id= :globalDema_demaId   " +
                "}   " +
                "where  " +
                "   (10=10) " +
                " " +
                "limit 10000 " +
                " ";

            
            params.add(gr.neuropublic.mutil.base.Pair.create("globalDema_demaId", (Object)(r.globalDema_demaId != null && !r.globalDema_demaId.startsWith("TEMP_ID_") ? (r.globalDema_demaId != null ? usrSession.DecryptInteger(r.globalDema_demaId) : null) : null)));
        

            if (r.geoFunc != null &&  r.selectedGeometry != null) {
                org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
                try {
                    com.vividsolutions.jts.geom.Geometry geo = gjson.read(r.selectedGeometry);
                    geo.setSRID(4326);
                    String rep;
                    if (r.geoFunc.equals("WITHIN_DISTANCE")) {
                        rep = "SDO_WITHIN_DISTANCE(geom, :selectedGeometry,'DISTANCE=" +(r.geoFuncParam==null?"0":r.geoFuncParam) + " UNIT=KM ' )='TRUE'";
                    } else if (r.geoFunc.equals("CONTAINS")) {
                        rep = "SDO_CONTAINS(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("INSIDE")) {
                        rep = "SDO_INSIDE(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("TOUCH")) {
                        rep = "SDO_TOUCH(geom, :selectedGeometry)='TRUE'";
                    } else {
                        throw new GenericApplicationException("Unsupported spatial operator:" + r.geoFunc == null ? "null" : r.geoFunc);
                    }
            
                    sqlQuery = sqlQuery.replaceAll("10=10", rep);
                    params.add(gr.neuropublic.mutil.base.Pair.create("selectedGeometry", (Object)geo));
            
                } catch (IOException ex) {
                    logger.error("Error in reading geoJSON DATA ",ex);
                    throw new GenericApplicationException(ex.getMessage());
                }
            }


            sqlQuery = sqlQuery.replace('{', '(').replace('}', ')');
            List<gr.neuropublic.base.SqlQueryRow> entities = getService().executeSqlQuery2(sqlQuery, params);

            JsonBlob rsp = new JsonBlob();
            gr.neuropublic.wsrestutils.LazyData lzData = new gr.neuropublic.wsrestutils.LazyData();
            lzData.data = entities;
            lzData.count = entities.size();
            rsp.data = lzData.toGeoJson(4326);


            return Response.status(200).type("application/json;charset=UTF-8").entity(rsp).build();
        }});
    }


    public static class GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter_excelFile_request {
        public String globalDema_demaId;
        public String sortField;
        public String sortOrder;
        public String selectedGeometry;
        public String geoFunc;
        public String geoFuncParam;
    }


    @POST
    @Path("/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter_excelFile")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter_excelFile(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed_filter_excelFile_request r,
        @CookieParam("niva-session-id") final String sessionId) 
    {

        return handleWebRequest(sessionId, new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            ArrayList<Pair<String, Object>> params =  new ArrayList<Pair<String, Object>>();
            String sqlQuery = 
                "select *  " +
                "from { " +
                "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=3 AND ide.dema_id= :globalDema_demaId   " +
                "}   " +
                "where  " +
                "   (10=10) " +
                " " +
                "limit 10000 " +
                " ";

            
            params.add(gr.neuropublic.mutil.base.Pair.create("globalDema_demaId", (Object)(r.globalDema_demaId != null && !r.globalDema_demaId.startsWith("TEMP_ID_") ? (r.globalDema_demaId != null ? usrSession.DecryptInteger(r.globalDema_demaId) : null) : null)));
        

            if (r.geoFunc != null &&  r.selectedGeometry != null) {
                org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
                try {
                    com.vividsolutions.jts.geom.Geometry geo = gjson.read(r.selectedGeometry);
                    geo.setSRID(4326);
                    String rep;
                    if (r.geoFunc.equals("WITHIN_DISTANCE")) {
                        rep = "SDO_WITHIN_DISTANCE(geom, :selectedGeometry,'DISTANCE=" +(r.geoFuncParam==null?"0":r.geoFuncParam) + " UNIT=KM ' )='TRUE'";
                    } else if (r.geoFunc.equals("CONTAINS")) {
                        rep = "SDO_CONTAINS(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("INSIDE")) {
                        rep = "SDO_INSIDE(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("TOUCH")) {
                        rep = "SDO_TOUCH(geom, :selectedGeometry)='TRUE'";
                    } else {
                        throw new GenericApplicationException("Unsupported spatial operator:" + r.geoFunc == null ? "null" : r.geoFunc);
                    }
            
                    sqlQuery = sqlQuery.replaceAll("10=10", rep);
                    params.add(gr.neuropublic.mutil.base.Pair.create("selectedGeometry", (Object)geo));
            
                } catch (IOException ex) {
                    logger.error("Error in reading geoJSON DATA ",ex);
                    throw new GenericApplicationException(ex.getMessage());
                }
            }


            sqlQuery = sqlQuery.replace('{', '(').replace('}', ')');
            List<gr.neuropublic.base.SqlQueryRow> entities = getService().executeSqlQuery2(sqlQuery, params);

            JsonBlob rsp = new JsonBlob();
            ArrayList<Pair<String,String>> colDefs = new ArrayList<Pair<String,String>>();

            rsp.data = gr.neuropublic.base.SqlQueryRow.ExportToExcel(colDefs, entities);


            return Response.status(200).type("application/json;charset=UTF-8").entity(rsp).build();
        }});
    }


    public static class GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_request {
        public String globalDema_demaId;
        public BigDecimal xmin;
        public BigDecimal ymin;
        public BigDecimal xmax;
        public BigDecimal ymax;
        public String selectedGeometry;
        public String geoFunc;
        public String geoFuncParam;
        public List<String> idsInCache;
    }

    public List<gr.neuropublic.base.SqlQueryRow> getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_entities(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_request r,
        final gr.neuropublic.Niva.services.UserSession usrSession)
    {
        ArrayList<Pair<String, Object>> params =  new ArrayList<Pair<String, Object>>();
        String sqlQuery = 
            "select a.* " +
            "from { " +
            "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=4 AND ide.dema_id= :globalDema_demaId   " +
            "} a " +
            "where " +
            "  ST_MakeEnvelope(:xmin, :ymin, :xmax, :ymax, 4326)::geometry && (a.geom)::geometry " +
            "  and (10=10) " +
            "  and (:xmax - :xmin <= 500000) ";
            
        params.add(gr.neuropublic.mutil.base.Pair.create("globalDema_demaId", (Object)(r.globalDema_demaId != null && !r.globalDema_demaId.startsWith("TEMP_ID_") ? (r.globalDema_demaId != null ? usrSession.DecryptInteger(r.globalDema_demaId) : null) : null)));

        params.add(gr.neuropublic.mutil.base.Pair.create("xmin", (Object)r.xmin != null ? (Object)r.xmin : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("ymin", (Object)r.ymin != null ? (Object)r.ymin : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("xmax", (Object)r.xmax != null ? (Object)r.xmax : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("ymax", (Object)r.ymax != null ? (Object)r.ymax : BigDecimal.ZERO) );
      
            
        if (r.geoFunc != null &&  r.selectedGeometry != null) {
            org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
            try {
                com.vividsolutions.jts.geom.Geometry geo = gjson.read(r.selectedGeometry);
                geo.setSRID(4326);
                String rep;
                if (r.geoFunc.equals("WITHIN_DISTANCE")) {
                    rep = "SDO_WITHIN_DISTANCE(geom, :selectedGeometry,'DISTANCE=" +(r.geoFuncParam==null?"0":r.geoFuncParam) + " UNIT=KM ' )='TRUE'";
                } else if (r.geoFunc.equals("CONTAINS")) {
                    rep = "SDO_CONTAINS(geom, :selectedGeometry)='TRUE'";
                } else if (r.geoFunc.equals("INSIDE")) {
                    rep = "SDO_INSIDE(geom, :selectedGeometry)='TRUE'";
                } else if (r.geoFunc.equals("TOUCH")) {
                    rep = "SDO_TOUCH(geom, :selectedGeometry)='TRUE'";
                } else {
                    throw new GenericApplicationException("Unsupported spatial operator:" + r.geoFunc == null ? "null" : r.geoFunc);
                }
        
                sqlQuery = sqlQuery.replaceAll("10=10", rep);
                params.add(gr.neuropublic.mutil.base.Pair.create("selectedGeometry", (Object)geo));
        
            } catch (IOException ex) {
                logger.error("Error in reading geoJSON DATA ",ex);
                throw new GenericApplicationException(ex.getMessage());
            }
        }
              
        sqlQuery = sqlQuery.replace('{', '(').replace('}', ')');
        return getService().executeSqlQuery2(sqlQuery, params);
    }

    @POST
    @Path("/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            Set<String> ids = new HashSet<String>(r.idsInCache);

            List<gr.neuropublic.base.SqlQueryRow> entities = getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_entities(r, usrSession);
            gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();
            
            for(gr.neuropublic.base.SqlQueryRow r : entities) {
                Object id = r.getId();
                if (ids.contains(id.toString())) {
                    r.clear();
                    r.setId(id);
                }
            }

            ret.data = entities;
            ret.count = entities.size();
            String jsonStr = ret.toJson2();

            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }



    public static class GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter_request {
        public String globalDema_demaId;
        public BigDecimal xmin;
        public BigDecimal ymin;
        public BigDecimal xmax;
        public BigDecimal ymax;
        public String selectedGeometry;
        public String geoFunc;
        public String geoFuncParam;
        public List<String> idsInCache;
    }

    public List<gr.neuropublic.base.SqlQueryRow> getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter_entities(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter_request r,
        final gr.neuropublic.Niva.services.UserSession usrSession)
    {
        ArrayList<Pair<String, Object>> params =  new ArrayList<Pair<String, Object>>();
        String sqlQuery = 
            "select a.* " +
            "from { " +
            "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=4 AND ide.dema_id= :globalDema_demaId   " +
            "} a " +
            "where " +
            "  ST_MakeEnvelope(:xmin, :ymin, :xmax, :ymax, 4326)::geometry && (a.geom)::geometry " +
            "  and (10=10) " +
            "  and (:xmax - :xmin <= 500000) ";
            
        params.add(gr.neuropublic.mutil.base.Pair.create("globalDema_demaId", (Object)(r.globalDema_demaId != null && !r.globalDema_demaId.startsWith("TEMP_ID_") ? (r.globalDema_demaId != null ? usrSession.DecryptInteger(r.globalDema_demaId) : null) : null)));

        params.add(gr.neuropublic.mutil.base.Pair.create("xmin", (Object)r.xmin != null ? (Object)r.xmin : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("ymin", (Object)r.ymin != null ? (Object)r.ymin : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("xmax", (Object)r.xmax != null ? (Object)r.xmax : BigDecimal.ZERO) );

        params.add(gr.neuropublic.mutil.base.Pair.create("ymax", (Object)r.ymax != null ? (Object)r.ymax : BigDecimal.ZERO) );
      
            
        if (r.geoFunc != null &&  r.selectedGeometry != null) {
            org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
            try {
                com.vividsolutions.jts.geom.Geometry geo = gjson.read(r.selectedGeometry);
                geo.setSRID(4326);
                String rep;
                if (r.geoFunc.equals("WITHIN_DISTANCE")) {
                    rep = "SDO_WITHIN_DISTANCE(geom, :selectedGeometry,'DISTANCE=" +(r.geoFuncParam==null?"0":r.geoFuncParam) + " UNIT=KM ' )='TRUE'";
                } else if (r.geoFunc.equals("CONTAINS")) {
                    rep = "SDO_CONTAINS(geom, :selectedGeometry)='TRUE'";
                } else if (r.geoFunc.equals("INSIDE")) {
                    rep = "SDO_INSIDE(geom, :selectedGeometry)='TRUE'";
                } else if (r.geoFunc.equals("TOUCH")) {
                    rep = "SDO_TOUCH(geom, :selectedGeometry)='TRUE'";
                } else {
                    throw new GenericApplicationException("Unsupported spatial operator:" + r.geoFunc == null ? "null" : r.geoFunc);
                }
        
                sqlQuery = sqlQuery.replaceAll("10=10", rep);
                params.add(gr.neuropublic.mutil.base.Pair.create("selectedGeometry", (Object)geo));
        
            } catch (IOException ex) {
                logger.error("Error in reading geoJSON DATA ",ex);
                throw new GenericApplicationException(ex.getMessage());
            }
        }
              
        sqlQuery = sqlQuery.replace('{', '(').replace('}', ')');
        return getService().executeSqlQuery2(sqlQuery, params);
    }

    @POST
    @Path("/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            Set<String> ids = new HashSet<String>(r.idsInCache);

            List<gr.neuropublic.base.SqlQueryRow> entities = getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter_entities(r, usrSession);
            gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();
            
            for(gr.neuropublic.base.SqlQueryRow r : entities) {
                Object id = r.getId();
                if (ids.contains(id.toString())) {
                    r.clear();
                    r.setId(id);
                }
            }

            ret.data = entities;
            ret.count = entities.size();
            String jsonStr = ret.toJson2();

            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }



    public static class GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter_grid_request {
        public String globalDema_demaId;
        public BigDecimal fromRowIndex;
        public BigDecimal toRowIndex;
        public String sortField;
        public String sortOrder;
        public String selectedGeometry;
        public String geoFunc;
        public String geoFuncParam;
    }


    @POST
    @Path("/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter_grid")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter_grid(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter_grid_request r,
        @CookieParam("niva-session-id") final String sessionId) 
    {

        return handleWebRequest(sessionId, new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            ArrayList<Pair<String, Object>> params =  new ArrayList<Pair<String, Object>>();
            String sqlQuery = 
                "select *  " +
                "from { " +
                "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=4 AND ide.dema_id= :globalDema_demaId   " +
                "}   " +
                "where  " +
                "  (10=10) " +
                "limit :toRowIndex ";
            if (!BigDecimal.ZERO.equals(r.fromRowIndex)) {
                sqlQuery = 
                    "select row_.*, rownum as rownum__   " +
                    "from { " +
                    "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=4 AND ide.dema_id= :globalDema_demaId   " +
                    "} row_   " +
                    "where  " +
                    "  (10=10) " +
                    "offset :fromRowIndex limit :toRowIndex ";
            }
            String sqlQueryCnt = 
                "select count(1) as cnt  " +
                "from { " +
                "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=4 AND ide.dema_id= :globalDema_demaId   " +
                "}   " +
                "where (10=10) ";
            
            sqlQueryCnt = sqlQueryCnt.replaceAll("1=1", "rownum<=10000");


            
            params.add(gr.neuropublic.mutil.base.Pair.create("globalDema_demaId", (Object)(r.globalDema_demaId != null && !r.globalDema_demaId.startsWith("TEMP_ID_") ? (r.globalDema_demaId != null ? usrSession.DecryptInteger(r.globalDema_demaId) : null) : null)));
        
            params.add(gr.neuropublic.mutil.base.Pair.create("toRowIndex", (Object)r.toRowIndex));
            if (!BigDecimal.ZERO.equals(r.fromRowIndex)) {
                params.add(gr.neuropublic.mutil.base.Pair.create("fromRowIndex", (Object)r.fromRowIndex));
            }   

            if (r.geoFunc != null &&  r.selectedGeometry != null) {
                org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
                try {
                    com.vividsolutions.jts.geom.Geometry geo = gjson.read(r.selectedGeometry);
                    geo.setSRID(4326);
                    String rep;
                    if (r.geoFunc.equals("WITHIN_DISTANCE")) {
                        rep = "SDO_WITHIN_DISTANCE(geom, :selectedGeometry,'DISTANCE=" +(r.geoFuncParam==null?"0":r.geoFuncParam) + " UNIT=KM ' )='TRUE'";
                    } else if (r.geoFunc.equals("CONTAINS")) {
                        rep = "SDO_CONTAINS(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("INSIDE")) {
                        rep = "SDO_INSIDE(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("TOUCH")) {
                        rep = "SDO_TOUCH(geom, :selectedGeometry)='TRUE'";
                    } else {
                        throw new GenericApplicationException("Unsupported spatial operator:" + r.geoFunc == null ? "null" : r.geoFunc);
                    }
            
                    sqlQuery = sqlQuery.replaceAll("10=10", rep);
                    sqlQueryCnt = sqlQueryCnt.replaceAll("10=10", rep);
            
                    params.add(gr.neuropublic.mutil.base.Pair.create("selectedGeometry", (Object)geo));
            
                } catch (IOException ex) {
                    logger.error("Error in reading geoJSON DATA ",ex);
                    throw new GenericApplicationException(ex.getMessage());
                }
            }


            sqlQuery = sqlQuery.replace('{', '(').replace('}', ')');
            List<gr.neuropublic.base.SqlQueryRow> entities = getService().executeSqlQuery2(sqlQuery, params);

            params.remove(gr.neuropublic.mutil.base.Pair.create("toRowIndex", (Object)r.toRowIndex));
            if (params.contains(gr.neuropublic.mutil.base.Pair.create("fromRowIndex", (Object)r.fromRowIndex))) {
                params.remove(gr.neuropublic.mutil.base.Pair.create("fromRowIndex", (Object)r.fromRowIndex));
            }
            
            //sqlQueryCnt = sqlQueryCnt.replace('{', '(').replace('}', ')');
            //List<gr.neuropublic.base.SqlQueryRow> entitiesCnt = getService().executeSqlQuery2(sqlQueryCnt, params);

            gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

            
            ret.data = entities;
            ret.count = -1; //((BigDecimal)entitiesCnt.get(0).get("cnt")).intValue();
            String jsonStr = ret.toJson2();

            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }



    public static class GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter_geoJsonFile_request {
        public String globalDema_demaId;
        public String sortField;
        public String sortOrder;
        public String selectedGeometry;
        public String geoFunc;
        public String geoFuncParam;
    }


    @POST
    @Path("/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter_geoJsonFile")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter_geoJsonFile(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter_geoJsonFile_request r,
        @CookieParam("niva-session-id") final String sessionId) 
    {

        return handleWebRequest(sessionId, new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            ArrayList<Pair<String, Object>> params =  new ArrayList<Pair<String, Object>>();
            String sqlQuery = 
                "select *  " +
                "from { " +
                "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=4 AND ide.dema_id= :globalDema_demaId   " +
                "}   " +
                "where  " +
                "   (10=10) " +
                " " +
                "limit 10000 " +
                " ";

            
            params.add(gr.neuropublic.mutil.base.Pair.create("globalDema_demaId", (Object)(r.globalDema_demaId != null && !r.globalDema_demaId.startsWith("TEMP_ID_") ? (r.globalDema_demaId != null ? usrSession.DecryptInteger(r.globalDema_demaId) : null) : null)));
        

            if (r.geoFunc != null &&  r.selectedGeometry != null) {
                org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
                try {
                    com.vividsolutions.jts.geom.Geometry geo = gjson.read(r.selectedGeometry);
                    geo.setSRID(4326);
                    String rep;
                    if (r.geoFunc.equals("WITHIN_DISTANCE")) {
                        rep = "SDO_WITHIN_DISTANCE(geom, :selectedGeometry,'DISTANCE=" +(r.geoFuncParam==null?"0":r.geoFuncParam) + " UNIT=KM ' )='TRUE'";
                    } else if (r.geoFunc.equals("CONTAINS")) {
                        rep = "SDO_CONTAINS(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("INSIDE")) {
                        rep = "SDO_INSIDE(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("TOUCH")) {
                        rep = "SDO_TOUCH(geom, :selectedGeometry)='TRUE'";
                    } else {
                        throw new GenericApplicationException("Unsupported spatial operator:" + r.geoFunc == null ? "null" : r.geoFunc);
                    }
            
                    sqlQuery = sqlQuery.replaceAll("10=10", rep);
                    params.add(gr.neuropublic.mutil.base.Pair.create("selectedGeometry", (Object)geo));
            
                } catch (IOException ex) {
                    logger.error("Error in reading geoJSON DATA ",ex);
                    throw new GenericApplicationException(ex.getMessage());
                }
            }


            sqlQuery = sqlQuery.replace('{', '(').replace('}', ')');
            List<gr.neuropublic.base.SqlQueryRow> entities = getService().executeSqlQuery2(sqlQuery, params);

            JsonBlob rsp = new JsonBlob();
            gr.neuropublic.wsrestutils.LazyData lzData = new gr.neuropublic.wsrestutils.LazyData();
            lzData.data = entities;
            lzData.count = entities.size();
            rsp.data = lzData.toGeoJson(4326);


            return Response.status(200).type("application/json;charset=UTF-8").entity(rsp).build();
        }});
    }


    public static class GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter_excelFile_request {
        public String globalDema_demaId;
        public String sortField;
        public String sortOrder;
        public String selectedGeometry;
        public String geoFunc;
        public String geoFuncParam;
    }


    @POST
    @Path("/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter_excelFile")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter_excelFile(
        final GetSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined_filter_excelFile_request r,
        @CookieParam("niva-session-id") final String sessionId) 
    {

        return handleWebRequest(sessionId, new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            ArrayList<Pair<String, Object>> params =  new ArrayList<Pair<String, Object>>();
            String sqlQuery = 
                "select *  " +
                "from { " +
                "      SELECT pc.parc_identifier as id, pc.parc_identifier as shortdesc, pc.parc_code as parcels_code, pc.prod_code as farmers_code, pc.geom4326 as geom FROM niva.parcel_class pc JOIN niva.integrateddecisions ide ON pc.pcla_id=ide.pcla_id WHERE ide.decision_code=4 AND ide.dema_id= :globalDema_demaId   " +
                "}   " +
                "where  " +
                "   (10=10) " +
                " " +
                "limit 10000 " +
                " ";

            
            params.add(gr.neuropublic.mutil.base.Pair.create("globalDema_demaId", (Object)(r.globalDema_demaId != null && !r.globalDema_demaId.startsWith("TEMP_ID_") ? (r.globalDema_demaId != null ? usrSession.DecryptInteger(r.globalDema_demaId) : null) : null)));
        

            if (r.geoFunc != null &&  r.selectedGeometry != null) {
                org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
                try {
                    com.vividsolutions.jts.geom.Geometry geo = gjson.read(r.selectedGeometry);
                    geo.setSRID(4326);
                    String rep;
                    if (r.geoFunc.equals("WITHIN_DISTANCE")) {
                        rep = "SDO_WITHIN_DISTANCE(geom, :selectedGeometry,'DISTANCE=" +(r.geoFuncParam==null?"0":r.geoFuncParam) + " UNIT=KM ' )='TRUE'";
                    } else if (r.geoFunc.equals("CONTAINS")) {
                        rep = "SDO_CONTAINS(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("INSIDE")) {
                        rep = "SDO_INSIDE(geom, :selectedGeometry)='TRUE'";
                    } else if (r.geoFunc.equals("TOUCH")) {
                        rep = "SDO_TOUCH(geom, :selectedGeometry)='TRUE'";
                    } else {
                        throw new GenericApplicationException("Unsupported spatial operator:" + r.geoFunc == null ? "null" : r.geoFunc);
                    }
            
                    sqlQuery = sqlQuery.replaceAll("10=10", rep);
                    params.add(gr.neuropublic.mutil.base.Pair.create("selectedGeometry", (Object)geo));
            
                } catch (IOException ex) {
                    logger.error("Error in reading geoJSON DATA ",ex);
                    throw new GenericApplicationException(ex.getMessage());
                }
            }


            sqlQuery = sqlQuery.replace('{', '(').replace('}', ')');
            List<gr.neuropublic.base.SqlQueryRow> entities = getService().executeSqlQuery2(sqlQuery, params);

            JsonBlob rsp = new JsonBlob();
            ArrayList<Pair<String,String>> colDefs = new ArrayList<Pair<String,String>>();

            rsp.data = gr.neuropublic.base.SqlQueryRow.ExportToExcel(colDefs, entities);


            return Response.status(200).type("application/json;charset=UTF-8").entity(rsp).build();
        }});
    }


    public static class FindAllByCriteriaRange_DashboardGrpIntegrateddecision_request {
        public String pclaId_pclaId;
        public Integer fromRowIndex;
        public Integer toRowIndex;
        public String sortField;
        public Boolean sortOrder;
        public List<String> exc_Id;
        public List<GridColDefinition> __fields;
        public gr.neuropublic.wsrestutils.LazyData.requestType __dataReqType;
    }

    @POST
    @Path("/findAllByCriteriaRange_DashboardGrpIntegrateddecision")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_DashboardGrpIntegrateddecision(
        final FindAllByCriteriaRange_DashboardGrpIntegrateddecision_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.DATA;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_DashboardGrpIntegrateddecision_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    @POST
    @Path("/findAllByCriteriaRange_DashboardGrpIntegrateddecision_count")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_DashboardGrpIntegrateddecision_count(
        final FindAllByCriteriaRange_DashboardGrpIntegrateddecision_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.COUNT;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_DashboardGrpIntegrateddecision_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }
    public gr.neuropublic.wsrestutils.LazyData findAllByCriteriaRange_DashboardGrpIntegrateddecision_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindAllByCriteriaRange_DashboardGrpIntegrateddecision_request r) {
        if (!usrSession.privileges.contains("Niva_Dashboard_R"))
            throw new GenericApplicationException("NO_READ_ACCESS");
        //Thread.sleep(1500); // debugging waiting dialogs
        int[] range = new int[] {r.fromRowIndex != null ? r.fromRowIndex : 0, r.toRowIndex != null ? r.toRowIndex : 10};
        int[] recordCount = new int [] {0};
        List<String> excludedIds = new ArrayList<String>();
        if (r != null && r.exc_Id != null && !r.exc_Id.isEmpty()) {
            for(String encrypted_id:r.exc_Id) {
                if (encrypted_id == null)
                    continue;
                String descryptedId = usrSession.DecryptInteger(encrypted_id).toString();
                excludedIds.add(descryptedId);
            }
        }

        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        if (r.__dataReqType == null || r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.DATA) {
            List<Integrateddecision> retEntities= getIntegrateddecisionFacade().findAllByCriteriaRange_DashboardGrpIntegrateddecision((r.pclaId_pclaId != null && !r.pclaId_pclaId.startsWith("TEMP_ID_") ? (r.pclaId_pclaId != null ? usrSession.DecryptInteger(r.pclaId_pclaId) : null) : null), range, recordCount, r.sortField, r.sortOrder != null ? r.sortOrder : false, excludedIds);
            ret.data = retEntities;
            ret.count = recordCount[0];
        } else if (r.__dataReqType != null && r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.COUNT) {
            ret.data = new ArrayList<>();
            ret.count = getIntegrateddecisionFacade().findAllByCriteriaRange_DashboardGrpIntegrateddecision_count((r.pclaId_pclaId != null && !r.pclaId_pclaId.startsWith("TEMP_ID_") ? (r.pclaId_pclaId != null ? usrSession.DecryptInteger(r.pclaId_pclaId) : null) : null), excludedIds);
        } else {
            throw new GenericApplicationException("Unknown_DataRequestType");
        }
        return ret;
    }


    @DELETE
    @Path("/delByIntegrateddecisionsId")
    public Response delByIntegrateddecisionsId(
        @QueryParam("integrateddecisionsId") final String integrateddecisionsId, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            Integrateddecision integrateddecision = getIntegrateddecisionFacade().findByIntegrateddecisionsId((integrateddecisionsId != null ? usrSession.DecryptInteger(integrateddecisionsId) : null));
            getService().removeEntity(usrSession.usrEmail, integrateddecision);
        //    Integer c = getIntegrateddecisionFacade().findByIntegrateddecisionsId((integrateddecisionsId != null ? usrSession.DecryptInteger(integrateddecisionsId) : null));
            return Response.status(200).type("application/json;charset=UTF-8").entity("").build();
        }});
    }

    public  Response handleWebRequest(String sessionId, final Func1<gr.neuropublic.Niva.services.UserSession, Response> javaClosure) {
        return this.handleWebRequest(sessionId, null, new Pair<String,String>(null, null), javaClosure);
    }
    public  Response handleWebRequest(String sessionId, String ssoSessionId, Pair<String,String> subscriberCookie, final Func1<gr.neuropublic.Niva.services.UserSession, Response> javaClosure) {
        Func1<gr.neuropublic.base.UserSession, Response> javaClosureCasted = new Func1<gr.neuropublic.base.UserSession, Response>() {
            @Override
            public Response lambda(gr.neuropublic.base.UserSession t1) {
                return javaClosure.lambda((gr.neuropublic.Niva.services.UserSession)t1);
            }
            
        };
    
        return super.handleWebRequest(sessionId, ssoSessionId, subscriberCookie, "niva-session-id", "/Niva/", javaClosureCasted);
    }
}