package gr.neuropublic.Niva.wsRestFacadesBase;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import gr.neuropublic.shiro.ILoginController;
import java.util.List;
import java.util.ArrayList;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Context;
import javax.servlet.ServletContext;
import gr.neuropublic.exceptions.GenericApplicationException;
import gr.neuropublic.wsrestutils.AbstractWebServiceHandler;
import gr.neuropublic.functional.Func1;
import gr.neuropublic.Niva.services.IUserManagementService;
import gr.neuropublic.mutil.base.Pair;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.core.MediaType;
import gr.neuropublic.Niva.facades.IFmisUploadFacade;
import gr.neuropublic.Niva.entities.FmisUpload;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import gr.neuropublic.Niva.facades.ITmpBlobFacade;
import gr.neuropublic.Niva.entities.TmpBlob;
import gr.neuropublic.wsrestutils.LazyFieldExclusionStrategy;
import gr.neuropublic.base.DateFormat;
import javax.ws.rs.CookieParam;
import javax.ws.rs.core.NewCookie;
import gr.neuropublic.Niva.services.ISessionsCacheService;
import gr.neuropublic.base.JsonBlob;
import gr.neuropublic.base.JsonSingleValue;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.jboss.resteasy.annotations.GZIP;
import javax.ws.rs.core.MultivaluedMap;
import java.io.InputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.HashMap;
import java.util.Set;
import org.apache.commons.io.IOUtils;
import org.slf4j.LoggerFactory;

import gr.neuropublic.base.GridColDefinition;

public  class FmisUploadWsFacadeBase  extends AbstractWebServiceHandler {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(gr.neuropublic.base.IAbstractService.class);

    @EJB
    private IFmisUploadFacade.ILocal fmisUploadFacade;
    public IFmisUploadFacade.ILocal getFmisUploadFacade() {
        return fmisUploadFacade;
    }
    public void setFmisUploadFacade(IFmisUploadFacade.ILocal val) {
        fmisUploadFacade = val;
    }

    @EJB
    private IUserManagementService.ILocal usrMngSrv;
    @Override
    public IUserManagementService.ILocal getUserManagementService() {
        return usrMngSrv;
    }
    public void setUserManagementService(IUserManagementService.ILocal val) {
        usrMngSrv = val;
    }

    @EJB
    private ISessionsCacheService.ILocal sessionsCache;
    @Override
    public ISessionsCacheService.ILocal getSessionsCache() {
        return sessionsCache;
    }
    public void setSessionsCache(ISessionsCacheService.ILocal sessionsCache) {
        this.sessionsCache = sessionsCache;
    }


    @EJB
    private ITmpBlobFacade.ILocal tmpBlobFacade;
    public ITmpBlobFacade.ILocal getTmpBlobFacade() {
        return tmpBlobFacade;
    }
    public void setTmpBlobFacade(ITmpBlobFacade.ILocal val) {
        tmpBlobFacade = val;
    }

    @EJB
    private gr.neuropublic.Niva.services.IMainService.ILocal service;

    public gr.neuropublic.Niva.services.IMainService.ILocal getService()
    {
        return service;
    }
    public void setService(gr.neuropublic.Niva.services.IMainService.ILocal service)
    {
        this.service = service;
    }

    @Override
    public void initializeDatabaseSession(gr.neuropublic.base.UserSession usrSession) {
    }




    public static class FindAllByIds_request {
        public String[] ids; 
    }

    @POST
    @Path("/findAllByIds")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByIds(
        final FindAllByIds_request r, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            gr.neuropublic.wsrestutils.LazyData ret = findAllByIds_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    public gr.neuropublic.wsrestutils.LazyData findAllByIds_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindAllByIds_request r) {
            
        ArrayList<Integer> ids = new ArrayList<Integer>();
        for(String enc_id: r.ids) {
            Integer id = (enc_id != null ? usrSession.DecryptInteger(enc_id) : null);
            if (id != null)
                ids.add(id);
        } 
        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        List<FmisUpload> entities = getFmisUploadFacade().findAllByIds(ids);
        ret.data = entities;
        ret.count = entities.size();
        return ret;
    }


    public static class FindByFmisUploadsId_request {
        public String fmisUploadsId; 
    }

    @POST
    @Path("/findByFmisUploadsId")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findByFmisUploadsId(
        final FindByFmisUploadsId_request r, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) {

        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            gr.neuropublic.wsrestutils.LazyData ret = findByFmisUploadsId_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    public gr.neuropublic.wsrestutils.LazyData findByFmisUploadsId_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindByFmisUploadsId_request r) {
            
        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        java.util.ArrayList<FmisUpload> list = new java.util.ArrayList<FmisUpload>();
        FmisUpload c = getFmisUploadFacade().findByFmisUploadsId((r.fmisUploadsId != null ? usrSession.DecryptInteger(r.fmisUploadsId) : null));
        if (c!=null)
            list.add(c); 
        ret.data = list;
        ret.count = list.size();
        return ret;
    }


    public static class FindAllByCriteriaRange_ParcelFMISGrpFmisUpload_request {
        public String parcelsIssuesId_parcelsIssuesId;
        public String fsch_dteUpload;
        public Integer fromRowIndex;
        public Integer toRowIndex;
        public String sortField;
        public Boolean sortOrder;
        public List<String> exc_Id;
        public List<GridColDefinition> __fields;
        public gr.neuropublic.wsrestutils.LazyData.requestType __dataReqType;
    }

    @POST
    @Path("/findAllByCriteriaRange_ParcelFMISGrpFmisUpload")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_ParcelFMISGrpFmisUpload(
        final FindAllByCriteriaRange_ParcelFMISGrpFmisUpload_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.DATA;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_ParcelFMISGrpFmisUpload_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }

    @POST
    @Path("/findAllByCriteriaRange_ParcelFMISGrpFmisUpload_count")
    @Consumes("application/json")
    @GZIP
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    public Response findAllByCriteriaRange_ParcelFMISGrpFmisUpload_count(
        final FindAllByCriteriaRange_ParcelFMISGrpFmisUpload_request r,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            r.__dataReqType = gr.neuropublic.wsrestutils.LazyData.requestType.COUNT;
            gr.neuropublic.wsrestutils.LazyData ret = findAllByCriteriaRange_ParcelFMISGrpFmisUpload_naked(usrSession, r);
            String jsonStr = ret.toJson(usrSession.crypto);
            return Response.status(200).type("application/json;charset=UTF-8").entity(jsonStr).build();
        }});
    }
    public gr.neuropublic.wsrestutils.LazyData findAllByCriteriaRange_ParcelFMISGrpFmisUpload_naked(final gr.neuropublic.Niva.services.UserSession usrSession, final FindAllByCriteriaRange_ParcelFMISGrpFmisUpload_request r) {
        if (!usrSession.privileges.contains("Niva_ParcelFMIS_R"))
            throw new GenericApplicationException("NO_READ_ACCESS");
        //Thread.sleep(1500); // debugging waiting dialogs
        int[] range = new int[] {r.fromRowIndex != null ? r.fromRowIndex : 0, r.toRowIndex != null ? r.toRowIndex : 10};
        int[] recordCount = new int [] {0};
        List<String> excludedIds = new ArrayList<String>();
        if (r != null && r.exc_Id != null && !r.exc_Id.isEmpty()) {
            for(String encrypted_id:r.exc_Id) {
                if (encrypted_id == null)
                    continue;
                String descryptedId = usrSession.DecryptInteger(encrypted_id).toString();
                excludedIds.add(descryptedId);
            }
        }

        gr.neuropublic.wsrestutils.LazyData ret = new gr.neuropublic.wsrestutils.LazyData();

        if (r.__dataReqType == null || r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.DATA) {
            List<FmisUpload> retEntities= getFmisUploadFacade().findAllByCriteriaRange_ParcelFMISGrpFmisUpload((r.parcelsIssuesId_parcelsIssuesId != null && !r.parcelsIssuesId_parcelsIssuesId.startsWith("TEMP_ID_") ? (r.parcelsIssuesId_parcelsIssuesId != null ? usrSession.DecryptInteger(r.parcelsIssuesId_parcelsIssuesId) : null) : null), gr.neuropublic.base.DateFormatter.parseISO8601Date(r.fsch_dteUpload), range, recordCount, r.sortField, r.sortOrder != null ? r.sortOrder : false, excludedIds);
            ret.data = retEntities;
            ret.count = recordCount[0];
        } else if (r.__dataReqType != null && r.__dataReqType == gr.neuropublic.wsrestutils.LazyData.requestType.COUNT) {
            ret.data = new ArrayList<>();
            ret.count = getFmisUploadFacade().findAllByCriteriaRange_ParcelFMISGrpFmisUpload_count((r.parcelsIssuesId_parcelsIssuesId != null && !r.parcelsIssuesId_parcelsIssuesId.startsWith("TEMP_ID_") ? (r.parcelsIssuesId_parcelsIssuesId != null ? usrSession.DecryptInteger(r.parcelsIssuesId_parcelsIssuesId) : null) : null), gr.neuropublic.base.DateFormatter.parseISO8601Date(r.fsch_dteUpload), excludedIds);
        } else {
            throw new GenericApplicationException("Unknown_DataRequestType");
        }
        return ret;
    }

    //http://localhost:8080/Niva/rest/FmisUpload/getDocfile?id=1&temp_id
    @GET
    @Path("/getDocfile")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)

    public Response getDocfile(
        @QueryParam("id") final String id_encrypted,
        @QueryParam("temp_id") final String blobFieldTempKey_encrypted,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            Integer id = (id_encrypted != null ? usrSession.DecryptInteger(id_encrypted) : null);
            Integer blobFieldTempKey = (blobFieldTempKey_encrypted != null ? usrSession.DecryptInteger(blobFieldTempKey_encrypted) : null);

            JsonBlob rsp = new JsonBlob();
            String fileName = "UnsavedFile";
            if (blobFieldTempKey != null) {
                // the entity has not yet been persited to the dabase, so get it from the temp blob table
                TmpBlob tmpBlob = getTmpBlobFacade().findById(blobFieldTempKey);
                if (tmpBlob!= null) {
                    rsp.data = tmpBlob.getData();
                } else {
                    throw new RuntimeException("TEMP_BLOB_DELETED");
                }
            } else {
                //if new entity and user has not yet upload something return null
                if (id == null)  { 
                    rsp.data = null; 
                } else {
                    FmisUpload fmisUpload = getFmisUploadFacade().findByFmisUploadsId(id);
                    if (fmisUpload == null) {
                        throw new RuntimeException("ENTITY_DELETED");
                    }
                    fileName = "FMIS Document" != null ? "FMIS Document" : "noFile";
                    rsp.data = fmisUpload.getDocfile();
                }
            }
            // for file name encoding see also http://stackoverflow.com/questions/93551/how-to-encode-the-filename-parameter-of-content-disposition-header-in-http
            //return Response.status(200).type("application/octet-stream").header("Content-Disposition", "attachment; filename=\""+fileName+"\";charset=UTF-8").entity(rsp.data).build();
            try {
                return Response.status(200).
                    type("application/octet-stream").
                    header("Content-Disposition", "attachment; filename=\""+fileName+"\"; filename*=UTF-8''" + java.net.URLEncoder.encode(fileName,"utf-8" )).entity(rsp.data).build();
            } catch (java.io.UnsupportedEncodingException ex) {
                throw new RuntimeException(ex);
            }
        }});
    }


    @POST
    @Path("/setDocfile")
    @Consumes("multipart/form-data")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response setDocfile(
        final MultipartFormDataInput input, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
            List<InputPart> inputParts = uploadForm.get("uploadedFile");
            TmpBlob tmpBlob = getTmpBlobFacade().initRow();
            for (InputPart inputPart : inputParts) {
                MultivaluedMap<String, String> header = inputPart.getHeaders();
     
                try {
                    //convert the uploaded file to inputstream
                    InputStream inputStream = inputPart.getBody(InputStream.class,null);

                    byte [] bytes = IOUtils.toByteArray(inputStream);
                    tmpBlob.setData(bytes);
                    break;
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
            getService().saveEntityToDB(usrSession, tmpBlob);
            return Response.status(200).type("application/json;charset=UTF-8").entity(usrSession.EncryptNumber(tmpBlob.getId())).build();
        }});
    }    


    @DELETE
    @Path("/delByFmisUploadsId")
    public Response delByFmisUploadsId(
        @QueryParam("fmisUploadsId") final String fmisUploadsId, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
            FmisUpload fmisUpload = getFmisUploadFacade().findByFmisUploadsId((fmisUploadsId != null ? usrSession.DecryptInteger(fmisUploadsId) : null));
            getService().removeEntity(usrSession.usrEmail, fmisUpload);
        //    Integer c = getFmisUploadFacade().findByFmisUploadsId((fmisUploadsId != null ? usrSession.DecryptInteger(fmisUploadsId) : null));
            return Response.status(200).type("application/json;charset=UTF-8").entity("").build();
        }});
    }

    public  Response handleWebRequest(String sessionId, final Func1<gr.neuropublic.Niva.services.UserSession, Response> javaClosure) {
        return this.handleWebRequest(sessionId, null, new Pair<String,String>(null, null), javaClosure);
    }
    public  Response handleWebRequest(String sessionId, String ssoSessionId, Pair<String,String> subscriberCookie, final Func1<gr.neuropublic.Niva.services.UserSession, Response> javaClosure) {
        Func1<gr.neuropublic.base.UserSession, Response> javaClosureCasted = new Func1<gr.neuropublic.base.UserSession, Response>() {
            @Override
            public Response lambda(gr.neuropublic.base.UserSession t1) {
                return javaClosure.lambda((gr.neuropublic.Niva.services.UserSession)t1);
            }
            
        };
    
        return super.handleWebRequest(sessionId, ssoSessionId, subscriberCookie, "niva-session-id", "/Niva/", javaClosureCasted);
    }
}