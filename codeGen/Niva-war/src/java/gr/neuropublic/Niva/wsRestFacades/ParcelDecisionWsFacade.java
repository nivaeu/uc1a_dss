//94D560588367E565A234B94A7297BE50

// MT - Added Support for CSV EXport of Decision Making Runs

package gr.neuropublic.Niva.wsRestFacades;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javax.ejb.Stateless;
import javax.ejb.EJB;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.CookieParam;
import javax.ws.rs.Consumes;
import org.jboss.resteasy.annotations.GZIP;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import gr.neuropublic.wsrestutils.LazyFieldExclusionStrategy;
import gr.neuropublic.mutil.base.Pair;
import java.util.ArrayList;
import gr.neuropublic.functional.Func1;
import gr.neuropublic.functional.Func;
import gr.neuropublic.base.JsonSingleValue;

import java.util.List;
import java.util.HashSet;
import java.util.HashMap;
import gr.neuropublic.base.GridColDefinition;
import gr.neuropublic.exceptions.GenericApplicationException;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import gr.neuropublic.Niva.entities.ParcelDecision;

import gr.neuropublic.Niva.wsRestFacadesBase.*;

import gr.neuropublic.Niva.entities.ParcelClas;
import java.lang.StringBuilder;

@Path("/ParcelDecision")
@Stateless

public class ParcelDecisionWsFacade extends ParcelDecisionWsFacadeBase {

        @POST
        @Path("/findAllByCriteriaRange_DecisionMakingGrpParcelDecision_toCSV")
        @Consumes("application/json")
        @GZIP
        @TransactionAttribute(TransactionAttributeType.SUPPORTS)
        public Response findAllByCriteriaRange_DecisionMakingGrpParcelDecision_toCSV(
                        final FindAllByCriteriaRange_DecisionMakingGrpParcelDecision_request r,
                        @CookieParam("niva-session-id") final String sessionId,
                        @CookieParam("sso-value") final String ssoSessionId,
                        @CookieParam("niva-subs-code") final String subscriberCookie) {

                return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code", subscriberCookie),
                                new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {
                                        @Override
                                        public Response lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
                                                if (!usrSession.privileges.contains("Niva_DecisionMaking_R")) {
                                                        throw new GenericApplicationException("NO_READ_ACCESS");
                                                }

                                                int maxRetEntities = 1000000;
                                                int[] range = new int[] { 0, maxRetEntities + 1 };
                                                int[] recordCount = new int[] { 0 };

                                                List<String> excludedIds = new ArrayList<String>();
                                                // Not Useful
                                                if (r != null && r.exc_Id != null && !r.exc_Id.isEmpty()) {
                                                        for (String encrypted_id : r.exc_Id) {
                                                                if (encrypted_id == null)
                                                                        continue;
                                                                String descryptedId = usrSession
                                                                                .DecryptInteger(encrypted_id)
                                                                                .toString();
                                                                excludedIds.add(descryptedId);
                                                        }
                                                }

                                                // Get Entities
                                                List<ParcelDecision> retEntities = getParcelDecisionFacade()
                                                                .findAllByCriteriaRange_DecisionMakingGrpParcelDecision(
                                                                                true,
                                                                                (r.demaId_demaId != null
                                                                                                && !r.demaId_demaId
                                                                                                                .startsWith("TEMP_ID_")
                                                                                                                                ? (r.demaId_demaId != null
                                                                                                                                                ? usrSession.DecryptInteger(
                                                                                                                                                                r.demaId_demaId)
                                                                                                                                                : null)
                                                                                                                                : null),
                                                                                r.fsch_decisionLight, r.fsch_prodCode,
                                                                                r.fsch_Code, r.fsch_Parcel_Identifier,
                                                                                range, recordCount, r.sortField,
                                                                                r.sortOrder != null ? r.sortOrder
                                                                                                : false,
                                                                                excludedIds);

                                                if (retEntities.size() > maxRetEntities) {
                                                        throw new GenericApplicationException(
                                                                        "TOO_MANY_RESULT_ENTITIES_FOR_CSV("
                                                                                        + maxRetEntities + ")");
                                                }

                                                // StaticLists
                                                java.util.HashMap<Integer, String> decisLightDomain = new java.util.HashMap<>();
                                                decisLightDomain.put(1, "Green");
                                                decisLightDomain.put(2, "Yellow");
                                                decisLightDomain.put(3, "Red");
                                                decisLightDomain.put(4, "Undefined");

                                                // Create text response

                                                // String textResponse = new String();
                                                StringBuilder textResponse = new StringBuilder(); // Use StringBuilder to boost the concatenation.

                                                // add headers
                                                String textResponseHeader = new String();

                                                textResponseHeader = "\"Decision Light\";\"Farmer Code\";\"Parcel Code\";\"Identifier\";\"Cultivation Declared Code\";\"Cultivation Declared Name\";\"Cultivation 1st Prediction Code\";\"Cultivation 1st Prediction Name\";\"Probability 1st Prediction\";\"Cultivation 2nd Prediction Code\";\"Cultivation 2nd Prediction Name\";\"Probability 2nd Prediction\"";
                                                textResponse.append(textResponseHeader);

                                                String textResponseRow = new String();

                                                ParcelDecision ent=new ParcelDecision();
                                                ParcelClas entPclaId=new ParcelClas();


                                                // add entities
                                                int length = retEntities.size();
                                                if (length > 0) {
                                                        for (int i = 0; i<length; i++) {

                                                               ent = retEntities.get(i);
                                                               
                                                                entPclaId=ent.getPclaId();

                                                                textResponseRow = "\n\""
                                                                                + decisLightDomain.get(ent.getDecisionLight())
                                                                                + "\";\""+ entPclaId.getProdCode()
                                                                                + "\";\""+ entPclaId.getParcCode()
                                                                                + "\";\""+ entPclaId.getParcIdentifier()
                                                                                + "\";\""+ entPclaId.getCultIdDecl().getCode()
                                                                                + "\";\""
                                                                                + entPclaId.getCultIdDecl().getName()
                                                                                + "\";\""
                                                                                + entPclaId.getCultIdPred().getCode()
                                                                                + "\";\""
                                                                                + entPclaId.getCultIdPred().getName()
                                                                                + "\";\""
                                                                                + entPclaId.getProbPred()
                                                                                + "\";\""+entPclaId.getCultIdPred2().getCode()
                                                                                + "\";\""+ entPclaId.getCultIdPred2().getName()
                                                                                + "\";\""+ entPclaId.getProbPred2()
                                                                                + "\"";

                                                                textResponse.append(textResponseRow);

                                                        }
                                                }

                                                return Response.status(200).type("text/csv;charset=UTF-8")
                                                                .entity(textResponse.toString()).build();

                                        }

                                });

        }

}
