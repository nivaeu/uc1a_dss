//760F3AA3EBAC2AF38FDDDAC919CCBF79
package gr.neuropublic.Niva.wsRestFacades;


import gr.neuropublic.Niva.entities.TmpBlob;
import javax.ejb.Stateless;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.CookieParam;
import org.jboss.resteasy.annotations.GZIP;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import gr.neuropublic.mutil.base.Pair;
import gr.neuropublic.functional.Func1;

import gr.neuropublic.Niva.wsRestFacadesBase.*;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ws.rs.Consumes;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

@Path("/Classification")
@Stateless
public  class ClassificationWsFacade extends ClassificationWsFacadeBase{
/*    
    @POST
    @Path("/setAttachedFile")
    @Consumes("multipart/form-data")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @GZIP
    public Response setAttachedFile(
        final MultipartFormDataInput input, 
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
//            Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
//            List<InputPart> inputParts = uploadForm.get("uploadedFile");
//            TmpBlob tmpBlob = getTmpBlobFacade().initRow();
//            for (InputPart inputPart : inputParts) {
//                MultivaluedMap<String, String> header = inputPart.getHeaders();
//     
//                try {
//                    //convert the uploaded file to inputstream
//                    InputStream inputStream = inputPart.getBody(InputStream.class,null);
//
//                    byte [] bytes = null;
//                    tmpBlob.setData(bytes);
//                    break;
//                } catch (IOException e) {
//                    throw new RuntimeException(e);
//                }
//            }
                    TmpBlob tmpBlob = getTmpBlobFacade().initRow();
                    byte [] bytes = null;
                    tmpBlob.setData(bytes);
            getService().saveEntityToDB(usrSession.usrEmail, tmpBlob);
            return Response.status(200).type("application/json;charset=UTF-8").entity(usrSession.EncryptNumber(tmpBlob.getId())).build();
        }});
    }    

    
     //http://localhost:8080/Niva/rest/Classification/getAttachedFile?id=1&temp_id
    @GET
    @Path("/getAttachedFile")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)

    public Response getAttachedFile(
        @QueryParam("id") final String id_encrypted,
        @QueryParam("temp_id") final String blobFieldTempKey_encrypted,
        @CookieParam("niva-session-id") final String sessionId,
        @CookieParam("sso-value") final String ssoSessionId,
        @CookieParam("niva-subs-code") final String subscriberCookie) 
    {   
        
        return null;
//        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code",subscriberCookie), new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {@Override public Response  lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
//            Integer id = (id_encrypted != null ? usrSession.DecryptInteger(id_encrypted) : null);
//            Integer blobFieldTempKey = (blobFieldTempKey_encrypted != null ? usrSession.DecryptInteger(blobFieldTempKey_encrypted) : null);
//
//            JsonBlob rsp = new JsonBlob();
//            String fileName = "UnsavedFile";
//            if (blobFieldTempKey != null) {
//                // the entity has not yet been persited to the dabase, so get it from the temp blob table
//                TmpBlob tmpBlob = getTmpBlobFacade().findById(blobFieldTempKey);
//                if (tmpBlob!= null) {
//                    rsp.data = tmpBlob.getData();
//                } else {
//                    throw new RuntimeException("TEMP_BLOB_DELETED");
//                }
//            } else {
//                //if new entity and user has not yet upload something return null
//                if (id == null)  { 
//                    rsp.data = null; 
//                } else {
//                    Classification classification = getClassificationFacade().findByClasId(id);
//                    if (classification == null) {
//                        throw new RuntimeException("ENTITY_DELETED");
//                    }
//                    fileName = classification.getFilePath() != null ? classification.getFilePath() : "noFile";
//                    rsp.data = classification.getAttachedFile();
//                }
//            }
//            // for file name encoding see also http://stackoverflow.com/questions/93551/how-to-encode-the-filename-parameter-of-content-disposition-header-in-http
//            //return Response.status(200).type("application/octet-stream").header("Content-Disposition", "attachment; filename=\""+fileName+"\";charset=UTF-8").entity(rsp.data).build();
//            try {
//                return Response.status(200).
//                    type("application/octet-stream").
//                    header("Content-Disposition", "attachment; filename=\""+fileName+"\"; filename*=UTF-8''" + java.net.URLEncoder.encode(fileName,"utf-8" )).entity(rsp.data).build();
//            } catch (java.io.UnsupportedEncodingException ex) {
//                throw new RuntimeException(ex);
//            }
//        }});

        
      }

*/
}
