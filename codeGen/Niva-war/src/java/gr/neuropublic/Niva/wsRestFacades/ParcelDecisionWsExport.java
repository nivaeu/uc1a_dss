package gr.neuropublic.Niva.wsRestFacades;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlElement;

/* @author m_mastorakis */

public class ParcelDecisionWsExport {

    public void ParcelDecisionWsExport(){}

    @XmlElement(required=false) String DECISION_LIGHT;
    @XmlElement(required=false) BigInteger FARMER_CODE;
    @XmlElement(required=false) BigInteger PARCEL_CODE;
    @XmlElement(required=false) String IDENTIFIER;
    @XmlElement(required=false) BigInteger CULTIVATION_DECLARED_CODE;
    @XmlElement(required=false) String CULTIVATION_DECLARED_NAME;
    
    @XmlElement(required=false) BigInteger CULTIVATION_1ST_PREDICTION_CODE;
    @XmlElement(required=false) String CULTIVATION_1ST_PREDICTION_NAME;
    @XmlElement(required=false) BigDecimal PROBABILITY_1ST_PREDICTION;
        
    @XmlElement(required=false) BigInteger CULTIVATION_2ND_PREDICTION_CODE;
    @XmlElement(required=false) String CULTIVATION_2ND_PREDICTION_NAME;
    @XmlElement(required=false) BigDecimal PROBABILITY_2ND_PREDICTION;
}
