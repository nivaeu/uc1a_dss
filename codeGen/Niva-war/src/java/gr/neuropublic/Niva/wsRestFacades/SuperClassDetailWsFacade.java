//4C14814EEF9C91BBD2C422A60C51E0F7
package gr.neuropublic.Niva.wsRestFacades;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javax.ejb.Stateless;
import javax.ejb.EJB;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.CookieParam;
import javax.ws.rs.Consumes;
import org.jboss.resteasy.annotations.GZIP;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import gr.neuropublic.wsrestutils.LazyFieldExclusionStrategy;
import gr.neuropublic.mutil.base.Pair;
import java.util.ArrayList;
import gr.neuropublic.functional.Func1;
import gr.neuropublic.functional.Func;
import gr.neuropublic.base.JsonSingleValue;

import gr.neuropublic.Niva.wsRestFacadesBase.*;

@Path("/SuperClassDetail")
@Stateless
public  class SuperClassDetailWsFacade extends SuperClassDetailWsFacadeBase{
}
