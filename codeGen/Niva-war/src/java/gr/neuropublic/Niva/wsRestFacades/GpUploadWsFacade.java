//C7DBCC0B349379FB613C18EDB8EDC9EC
package gr.neuropublic.Niva.wsRestFacades;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javax.ejb.Stateless;
import javax.ejb.EJB;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.CookieParam;
import javax.ws.rs.Consumes;
import org.jboss.resteasy.annotations.GZIP;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import gr.neuropublic.wsrestutils.LazyFieldExclusionStrategy;
import gr.neuropublic.mutil.base.Pair;
import java.util.ArrayList;
import gr.neuropublic.functional.Func1;
import gr.neuropublic.functional.Func;
import gr.neuropublic.base.JsonSingleValue;

import gr.neuropublic.Niva.wsRestFacadesBase.*;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.apache.tika.Tika;
import gr.neuropublic.base.JsonBlob;
import gr.neuropublic.Niva.entities.TmpBlob;
import gr.neuropublic.Niva.entities.GpUpload;

@Path("/GpUpload")
@Stateless
public class GpUploadWsFacade extends GpUploadWsFacadeBase {

    // http://localhost:8080/Niva/rest/GpUpload/getImage?id=1&temp_id
    @GET
    @Path("/getImage")
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)

    public Response getImage(@QueryParam("id") final String id_encrypted,
            @QueryParam("temp_id") final String blobFieldTempKey_encrypted,
            @CookieParam("niva-session-id") final String sessionId, @CookieParam("sso-value") final String ssoSessionId,
            @CookieParam("niva-subs-code") final String subscriberCookie) {
        return handleWebRequest(sessionId, ssoSessionId, new Pair<>("niva-subs-code", subscriberCookie),
                new Func1<gr.neuropublic.Niva.services.UserSession, Response>() {
                    @Override
                    public Response lambda(gr.neuropublic.Niva.services.UserSession usrSession) {
                        Integer id = (id_encrypted != null ? usrSession.DecryptInteger(id_encrypted) : null);
                        Integer blobFieldTempKey = (blobFieldTempKey_encrypted != null
                                ? usrSession.DecryptInteger(blobFieldTempKey_encrypted)
                                : null);

                        JsonBlob rsp = new JsonBlob();
                        String fileName = "UnsavedFile";
                        Tika tika = new Tika();
                        String mimeType = "";
                        if (blobFieldTempKey != null) {
                            // the entity has not yet been persited to the dabase, so get it from the temp
                            // blob table
                            TmpBlob tmpBlob = getTmpBlobFacade().findById(blobFieldTempKey);
                            if (tmpBlob != null) {
                                rsp.data = tmpBlob.getData();
                            } else {
                                throw new RuntimeException("TEMP_BLOB_DELETED");
                            }
                        } else {
                            // if new entity and user has not yet upload something return null
                            if (id == null) {
                                rsp.data = null;
                            } else {
                                GpUpload gpUpload = getGpUploadFacade().findByGpUploadsId(id);
                                if (gpUpload == null) {
                                    throw new RuntimeException("ENTITY_DELETED");
                                }
                                fileName = "NIVA_DSS-Geotagged_Photo" != null ? "NIVA_DSS-Geotagged_Photo" : "noFile";
                                rsp.data = gpUpload.getImage();

                                // Detect content's type
                                mimeType = tika.detect(rsp.data);
                            }
                        }
                        // for file name encoding see also
                        // http://stackoverflow.com/questions/93551/how-to-encode-the-filename-parameter-of-content-disposition-header-in-http
                        // return
                        // Response.status(200).type("application/octet-stream").header("Content-Disposition",
                        // "attachment;
                        // filename=\""+fileName+"\";charset=UTF-8").entity(rsp.data).build();
                        try {
                            return Response.status(200).type(mimeType)
                                    .header("Content-Disposition",
                                            "attachment; filename=\"" + fileName + "\"; filename*=UTF-8''"
                                                    + java.net.URLEncoder.encode(fileName, "utf-8"))
                                    .entity(rsp.data).build();
                        } catch (java.io.UnsupportedEncodingException ex) {
                            throw new RuntimeException(ex);
                        }
                    }
                });
    }

}
