//2C488290C143E71884A8C3925139DAD7
package gr.neuropublic.Niva.wsRestFacades;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javax.ejb.Stateless;
import javax.ejb.EJB;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.CookieParam;
import javax.ws.rs.Consumes;
import org.jboss.resteasy.annotations.GZIP;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import gr.neuropublic.wsrestutils.LazyFieldExclusionStrategy;
import gr.neuropublic.mutil.base.Pair;
import java.util.ArrayList;
import gr.neuropublic.functional.Func1;
import gr.neuropublic.functional.Func;
import gr.neuropublic.base.JsonSingleValue;

import gr.neuropublic.Niva.wsRestFacadesBase.*;

import gr.neuropublic.Niva.serviceInputs.*;

@Path("/MainService")
@Stateless
public  class MainServiceWs extends MainServiceWsBase{
}
