package gr.neuropublic.jsf.base;

import gr.neuropublic.exceptions.DatabaseConstraintViolationException;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.exceptions.ValidationException;
import gr.neuropublic.functional.Action;
import gr.neuropublic.jsf.util.JsfUtil;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import java.util.List;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author gmamais
 */
public class BaseForm {

    private static final Logger logger = LoggerFactory.getLogger(BaseForm.class);
    
    public boolean callServiceFunction(Action javaClosure, String successMessage) {
        RequestContext context = RequestContext.getCurrentInstance();        
        boolean commitError = true; 
        try
        {
            javaClosure.lambda();
            JsfUtil.addSuccessMessage((JsfUtil.getMsgString(successMessage)));
            commitError = false;
            context.addCallbackParam("commitError", commitError);
        } catch (DatabaseConstraintViolationException e) {
            JsfUtil.addErrorMessage(JsfUtil.getMsgString(e.getMessage()));
            context.addCallbackParam("commitError", commitError);
            return false;
        } catch (ValidationException e) {
            String[] messages = e.getMessage().split("%%");
            for (String currentMessage : messages)
            {
                //create EntityValidationError from message 
                EntityValidationError entityValidationError = EntityValidationError.createFromString(currentMessage);

                //get suberrors
                List<EntityValidationSubError> subErrorsList = entityValidationError.getSubErrors();
                for (EntityValidationSubError currentSubError : subErrorsList)
                {
                    String messageKey = currentSubError.getErrorId();
                    List<String> args = currentSubError.getArgs();
                    Object[] params = args.toArray();
                    JsfUtil.addErrorMessage(JsfUtil.getMsgString(messageKey, params));
                }
            }
            context.addCallbackParam("commitError", commitError);
            return false;

        } catch (DatabaseGenericException e) {

            logger.error("DatabaseGenericException :", e);
            JsfUtil.addErrorMessage(e.getMessage());
            context.addCallbackParam("commitError", commitError);
            return false;

        }catch (Exception e) {
            logger.error("Unhandled Exception :", e);
            JsfUtil.addErrorMessage("Unhandled Exception :" + e.getMessage());
            context.addCallbackParam("commitError", commitError);
            return false;
        }
        return true;
    }

}
