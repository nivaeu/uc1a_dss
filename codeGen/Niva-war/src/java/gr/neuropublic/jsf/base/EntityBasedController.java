package gr.neuropublic.jsf.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.Serializable;
import javax.faces.event.ComponentSystemEvent;


public abstract class EntityBasedController<T>  implements Serializable{
    
    private static Logger logger = LoggerFactory.getLogger(EntityBasedController.class);
    
    protected T current;
    
    public T getCurrent() {
        return current;
    }
    
    public void setCurrent(T current) {
	
		logger.info("############### setCurrent() called: current = " + current);
        this.current = current;
    }

    public void preRenderView(ComponentSystemEvent event) {

    }	
    
   
}