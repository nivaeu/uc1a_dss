package gr.neuropublic.jsf.base;

public interface IPendingChanges {
    public boolean getPendingChanges();
    public void setPendingChanges(boolean pendingChanges);
}
