/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.jsf.base;

import gr.neuropublic.exceptions.DatabaseConstraintViolationException;
import gr.neuropublic.exceptions.DatabaseGenericException;
import gr.neuropublic.exceptions.ValidationException;
import gr.neuropublic.jsf.util.JsfUtil;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import java.util.List;
import javax.faces.event.ComponentSystemEvent;
import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import gr.neuropublic.functional.Action;

/**
 *
 * @author gmamais
 */
public abstract class BaseController  extends  BaseForm{

    public abstract IPendingChanges getPendingChangesInterface();
//    protected abstract IServiceBean getServiceBean();

    public abstract String getUserName();
    
    private static Logger logger = LoggerFactory.getLogger(BaseController.class);
    
    public abstract void commitToOverride();
    
    public void preRenderView(ComponentSystemEvent event) {

    }	
    
    
    public boolean commit()
    {
        logger.info("commit");
        RequestContext context = RequestContext.getCurrentInstance();        
        
        boolean commitError = true; 

        if (getPendingChangesInterface().getPendingChanges())
        {
            try
            {
                commitToOverride();

                getPendingChangesInterface().setPendingChanges(false);
                JsfUtil.addSuccessMessage((JsfUtil.getMsgString("generic.entitySuccessSave")));
                commitError = false;
                context.addCallbackParam("commitError", commitError);
            } catch (DatabaseConstraintViolationException e) {
                JsfUtil.addErrorMessage(JsfUtil.getMsgString(e.getMessage()));
                context.addCallbackParam("commitError", commitError);
                return false;
            } catch (ValidationException e) {
                String[] messages = e.getMessage().split("%%");
                for (String currentMessage : messages)
                {
                    //create EntityValidationError from message 
                    EntityValidationError entityValidationError = EntityValidationError.createFromString(currentMessage);

                    //get suberrors
                    List<EntityValidationSubError> subErrorsList = entityValidationError.getSubErrors();
                    for (EntityValidationSubError currentSubError : subErrorsList)
                    {
                        String messageKey = currentSubError.getErrorId();
                        List<String> args = currentSubError.getArgs();
                        Object[] params = args.toArray();
                        JsfUtil.addErrorMessage(JsfUtil.getMsgString(messageKey, params));
                    }
                }
                context.addCallbackParam("commitError", commitError);
                return false;
            } catch (DatabaseGenericException e) {

                logger.error("DatabaseGenericException :", e);
                JsfUtil.addErrorMessage(e.getMessage());
                context.addCallbackParam("commitError", commitError);
                return false;

            } catch (Exception e) {
                logger.error("Unhandled Exception :", e);
                JsfUtil.addErrorMessage("Unhandled Exception :" + e.getMessage());
                context.addCallbackParam("commitError", commitError);
                return false;
            }
        }
        else
        {
            JsfUtil.addWarningMessage(JsfUtil.getMsgString("generic.NoPendingChanges"));
            context.addCallbackParam("commitError", commitError);
            return false;
        }

        return true;
    }
    
    
    
}
