package gr.neuropublic.jsf.util;

import javax.servlet.http.HttpServletRequest;
import javax.faces.context.FacesContext;

public class FacesUtil {

    public static String getContextPath() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        return request.getContextPath(); 
    }

}