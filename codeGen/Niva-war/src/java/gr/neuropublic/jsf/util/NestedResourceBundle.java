package gr.neuropublic.jsf.util;

import java.util.Enumeration;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * ResourceBundle class used to support two different bundles: base, and parent
 * if key is not found in base bundle, then parent bundle is searched for
 *
 * @author g_dimitriadis
 */
@ManagedBean
public class NestedResourceBundle extends ResourceBundle {

    private static Logger l =
            LoggerFactory.getLogger(NestedResourceBundle.class);
    private String baseResource;
    private String parentResource;

    public String getBaseResource() {
        return baseResource;
    }

    public void setBaseResource(String baseResource) {
        this.baseResource = baseResource;
    }

    public String getParentResource() {
        return parentResource;
    }

    public void setParentResource(String parentResource) {
        this.parentResource = parentResource;
    }
    private ResourceBundle baseBundle;

    @PostConstruct
    protected void init() {
        Locale currentLocale = JsfUtil.getLocale();
        baseBundle = java.util.ResourceBundle.getBundle(baseResource,
                currentLocale);
        if (parentResource != null) { //property not mandatory
            setParent(java.util.ResourceBundle.getBundle(parentResource,
                    currentLocale));
        }
    }

    @Override
    protected Object handleGetObject(String key) {

        try {
            return baseBundle.getObject(key);
        } catch (MissingResourceException e) {
            return null;
        }
    }

    @Override
    public Enumeration<String> getKeys() {
        return baseBundle.getKeys();
    }
}