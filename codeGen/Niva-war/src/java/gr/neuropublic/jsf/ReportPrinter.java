package gr.neuropublic.jsf;


import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.util.Map;
import java.util.HashMap;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.ejb.Remote;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.dbutils.DbUtils;

import org.python.core.PyException;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRRuntimeException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.j2ee.servlets.BaseHttpServlet;

import gr.neuropublic.mutil.base.FileUtil;
import gr.neuropublic.mutil.base.Quad;
import gr.neuropublic.mutil.reportprocessor2.DBPythonExposer;
import java.util.Locale;
import net.sf.jasperreports.engine.JRParameter;

public class ReportPrinter {

    private static Logger l = LoggerFactory.getLogger(ReportPrinter.class);

    public static enum ExportFormat {
        XLS("xls"), PDF("pdf"), DOC("doc");
        
        private String extension;
        private ExportFormat(String extension) {
            this.extension = extension;
        }
        public String getExtension() {
            return this.extension;
        }    
    }

    public static ExportFormat getExportFormatByValue(int value) 
    {
        switch(value) {
            case 0: 
                return ExportFormat.XLS;
            case 1:
                return ExportFormat.PDF;
            case 2:
                return ExportFormat.DOC;
            default: throw new RuntimeException("This format value is unsupported for the moment: "+value);
        }
    }

    public static JasperPrint print(String dbCoords, String schemaName, String reportid, ExportFormat ef, Date applicableDate, Map<String, Object> paramsFromForm) throws Exception {
        InitialContext cxt = new InitialContext();
        DataSource ds = (DataSource) cxt.lookup( dbCoords ); // "java:/cashflow" );
        Connection conn = ds.getConnection();
        l.debug(String.format("connection's  initial autocommit status is: %b", conn.getAutoCommit()));
        conn.setAutoCommit(false);
        l.debug(String.format("connection's eventual autocommit status is: %b", conn.getAutoCommit()));
        try {
            // PythonExposer pythonExposer = new PythonExposer(conn);
            String reportName = getReportName(conn, schemaName, reportid, applicableDate);
            File reportFolder = getReportFolder(reportName);
            String script = getPythonScript(reportFolder);
            Quad<Map<String, Object>, String, String, PyException> out = DBPythonExposer.prepareParameters(conn, paramsFromForm, script);
            if (out.d == null)
                conn.commit();
            else throw out.d;
            Map<String, Object> params = out.a;
            for (String key : params.keySet())
                l.info(String.format("%s --> %s", key, params.get(key)));
            Map<String, Object> effectiveParams = new HashMap<String, Object>();
            //if (paramsFromForm!=null) effectiveParams.putAll(paramsFromForm);
            if (params        !=null) effectiveParams.putAll(params);
            File jasperF = getJasperFile(reportFolder, ef);
            return print(effectiveParams, jasperF, conn);
        } finally {
            DbUtils.closeQuietly(conn);            
        }
    }
	
    public static void sendToFormat(JasperPrint jasperPrint, ExportFormat ef) throws IOException {
        sendToFormatServletAndConcludeJSFResponse(jasperPrint, ef, "report");
    }


    public static void sendToFormatServletAndConcludeJSFResponse(JasperPrint jasperPrint, ExportFormat ef, String reportName) throws IOException {
        ExternalContext externalContext = getExternalContext();
        ((HttpSession) externalContext.getSession(false)).setAttribute(BaseHttpServlet.DEFAULT_JASPER_PRINT_SESSION_ATTRIBUTE, jasperPrint);
        externalContext.redirect(externalContext.getRequestContextPath()+serviceName(ef)+"/"+reportName+"."+ef.getExtension());
        FacesContext.getCurrentInstance().responseComplete();
    }

    private static ExternalContext getExternalContext() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        return facesContext.getExternalContext();
    }


    private static String serviceName(ExportFormat format) {
        String service = null;
        switch(format) {
            case XLS: service = "/servlets/xls";
                break;
            case PDF: service = "/servlets/pdf";
                break;
            default: throw new RuntimeException("This format is unsupported for the moment: "+format);
        }
        return service;
    }

    private static String getReportName(Connection conn, String schemaName, String reportId, Date applicableDate) throws SQLException {
        PreparedStatement  pstm = null;
        ResultSet            rs = null;
        try {
            String stmtStr = String.format("SELECT rprt_name FROM %s.reports WHERE rprt_code=? AND rprt_valid_from="+
                                           "(SELECT MAX(rprt_valid_from) FROM %s.reports WHERE rprt_code=? AND rprt_valid_from  <= ?)"
                                           , schemaName, schemaName);
            pstm  = conn.prepareStatement(stmtStr);
            pstm.setString     (1, reportId);
            pstm.setString     (2, reportId);
            pstm.setDate       (3, applicableDate);
            rs = pstm.executeQuery();
            boolean beenHereBefore = false;
            String reportName      = null;
            while (rs.next()) {
                if (beenHereBefore) throw new RuntimeException();
                reportName = rs.getString(1);
                beenHereBefore = true;
            }
            return reportName;
        } finally {
            DbUtils.closeQuietly(null, pstm, rs);
        }
        // return "report-zero";
    }

    private static ServletContext getServletContext() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        ServletContext servletContext = (ServletContext) externalContext.getContext();
        return servletContext;
    }
    
    private static File getReportFolder(String reportName) {
        l.info("report name = "+reportName);
        String reportFolderName = getServletContext().getRealPath("/reports/" + reportName);
        File retValue = new File(reportFolderName);
        if (!retValue.exists()) throw new RuntimeException(String.format("folder: %s does not exist", reportFolderName));
        return retValue;
    }

    private static String getPythonScript(File reportFolder) throws Exception {
        File pythonFile = FileUtil.getSingleFile(reportFolder, ".*\\.py");
        l.info("Python script found as: "+pythonFile.getAbsolutePath());
      	return FileUtil.readUTF8FileAsSingleString(pythonFile, "\n");
    }

    private static File getJasperFile(File reportFolder, ExportFormat ef) throws Exception {
        File jasperFile = null;
        switch (ef) {
        case PDF:
            jasperFile = FileUtil.getSingleFile(reportFolder, ".*\\.forPDF\\.jasper");
            break;
        case XLS:
            jasperFile = FileUtil.getSingleFile(reportFolder, ".*\\.forExcel\\.jasper");
            break;
        default:
            throw new RuntimeException("format unssuppored at the moment: "+ef);   
        }
        l.info("Jasper file found as: "+jasperFile.getAbsolutePath());
        return jasperFile;
    }

    private static JasperPrint print(Map parameters, File jasperFile, Connection conn) throws JRException {
        Locale locale = new Locale("el", "GR");
        parameters.put(JRParameter.REPORT_LOCALE, locale);
        return JasperFillManager.fillReport(
                                    jasperFile.getAbsolutePath(),
                                    parameters,
                                    conn);
    }

} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
