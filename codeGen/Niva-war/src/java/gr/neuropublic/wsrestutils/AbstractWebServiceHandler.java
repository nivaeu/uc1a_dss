/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.wsrestutils;
import gr.neuropublic.base.IAbstractService;
import gr.neuropublic.base.ISessionsCache;
import gr.neuropublic.base.IUserManagementService;
import gr.neuropublic.base.UserSession;
import gr.neuropublic.exceptions.GenericApplicationException;
import gr.neuropublic.functional.Func;
import gr.neuropublic.functional.Func1;
import gr.neuropublic.mutil.base.Pair;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import javax.ejb.EJB;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import org.slf4j.LoggerFactory;

public abstract class AbstractWebServiceHandler {
    
    private final org.slf4j.Logger logger = LoggerFactory.getLogger(AbstractWebServiceHandler.class);
    
    public com.vividsolutions.jts.geom.Geometry createGeometryFromJson(String jsonData, int srid) {
        try {
            org.geotools.geojson.geom.GeometryJSON gjson = new org.geotools.geojson.geom.GeometryJSON(6);
            com.vividsolutions.jts.geom.Geometry geo = gjson.read(jsonData);
            geo.setSRID(srid);
            return geo;
        } catch (IOException ex) {
            logger.error("Error in reading geoJSON DATA ",ex);
            throw new GenericApplicationException(ex.getMessage());
        }
    }

    public ArrayList<Long> dateListToJsonPrepare (ArrayList<Date> dateList) {
        ArrayList<Long> timeList = new ArrayList<>();
        for (Date date:dateList){
            timeList.add(dateToJsonPrepare(date));
        }
        return timeList;
    }

    public Long dateToJsonPrepare (Date date) {
        if (date == null) {
            return null;
        } 
        return date.getTime();
    }

    public abstract IUserManagementService getUserManagementService() ;
    public abstract ISessionsCache getSessionsCache();
    
    
    public abstract void initializeDatabaseSession(UserSession usrSession);
    
    public  Response handleWebRequest(String sessionId, String cookieName, String cookiePath, Func1<UserSession, Response> javaClosure) {
        return handleWebRequest(sessionId, null, (new Pair<String,String>(null, null)), true, cookieName, cookiePath, javaClosure);
    }
    public  Response handleWebRequest(String sessionId, String ssoSessionId, Pair<String,String> subsCriberCookie, String cookieName, String cookiePath, Func1<UserSession, Response> javaClosure) {
        return handleWebRequest(sessionId, ssoSessionId, subsCriberCookie, true, cookieName, cookiePath, javaClosure);
    }
    public  Response handleWebRequest(String sessionId, boolean resetMemCacheTimer, String cookieName, String cookiePath, Func1<UserSession, Response> javaClosure) {
        return handleWebRequest(sessionId, null, (new Pair<String,String>(null, null)), resetMemCacheTimer, cookieName, cookiePath, javaClosure);
    }
    public  Response handleWebRequest(String sessionId, String ssoSessionId, Pair<String,String> subsCriberCookie, boolean resetMemCacheTimer, String cookieName, String cookiePath, Func1<UserSession, Response> javaClosure) {
        try {
            UserSession usrSession = getSessionsCache().getEntry(sessionId, ssoSessionId, resetMemCacheTimer);
            if (usrSession != null) {
                if(subsCriberCookie != null && subsCriberCookie.a != null) {
                    gr.neuropublic.base.CryptoUtils crypto = new gr.neuropublic.base.CryptoUtils(UserSession.class.getName());
                    String decryptedSubsCookie = "";
                    try{
                        if (subsCriberCookie.b != null)
                            decryptedSubsCookie = crypto.DecryptString(subsCriberCookie.b);
                    } catch(Exception ex){}
                    if (!decryptedSubsCookie.equals(usrSession.subsCode)) {
                        //throw new GenericApplicationException("Invalid Session");
                        String reqCookie = (cookieName != null ? cookieName +"="+(sessionId != null? sessionId : "null") : "Invalid Session");
                        logger.error("Invalid Session for app cookie: " + reqCookie);
                        return Response.status(Response.Status.NOT_ACCEPTABLE).header(
                            "Set-Cookie",
                            cookieName+"="+sessionId+"; "+
                            "Version=1; Max-Age=0; Path=" + cookiePath + "; " +
                            "Expires=Thu, 01 Jan 1970 00:00:00 GMT").entity("Invalid Session").build();
                    }
                }
                initializeDatabaseSession(usrSession);
                return javaClosure.lambda(usrSession);
            } else {
                //NewCookie sessionCookie = new NewCookie(cookieName, sessionId, cookiePath, null, NewCookie.DEFAULT_VERSION, null, 0, false);    
                //return Response.status(Response.Status.NOT_ACCEPTABLE).cookie(sessionCookie).type("text/html;charset=UTF-8").entity("NOT_AUTHORIZED").build();
                String reqCookie = (cookieName != null ? cookieName +"="+(sessionId != null? sessionId : "null") : "no sessionId");
                logger.error("No session found for app cookie: " + reqCookie);
                return Response.status(Response.Status.NOT_ACCEPTABLE).header(
                    "Set-Cookie",
                    cookieName+"="+sessionId+"; "+
                    "Version=1; Max-Age=0; Path=" + cookiePath + "; " +
                    "Expires=Thu, 01 Jan 1970 00:00:00 GMT").entity("No session found (" + reqCookie + ")").build();
            }
        } catch (javax.ejb.EJBTransactionRolledbackException e) {
            String errMsg = null;
            for (Throwable t = e.getCause(); t != null; t = t.getCause()) {
                if (t instanceof org.hibernate.exception.ConstraintViolationException) {
                    errMsg = getConstraintFromException((org.hibernate.exception.ConstraintViolationException) t);
                    break;
                }
            }
            if (errMsg == null) {
                if (e != null && e.getMessage() != null) {
                    String errMessage = GenericApplicationException.searchOracleApplicationError(e.getMessage());
                    errMsg = GenericApplicationException.parsePostgresqlError(errMessage);
                } else {
                    errMsg = "UNKNOWN_ERROR";
                }
            }
            return Response.status(Response.Status.NOT_ACCEPTABLE).type("text/html;charset=UTF-8").entity(errMsg).build();
        } catch (NullPointerException e) {
            String errMsg = (e != null && e.getMessage() != null ? e.getMessage() : "NullPointerException");
            return Response.status(Response.Status.NOT_ACCEPTABLE).type("text/html;charset=UTF-8").entity(errMsg).build();
        } catch (Exception e) {
            String errMsg = (e != null && e.getMessage() != null ? e.getMessage() : "UNKNOWN_ERROR");
            return Response.status(Response.Status.NOT_ACCEPTABLE).type("text/html;charset=UTF-8").entity(errMsg).build();
        }
    }

    public static String getConstraintFromException(org.hibernate.exception.ConstraintViolationException constraintException) {
        
        String constraintName = constraintException.getConstraintName();
        if (constraintName != null) {
            return constraintName;
        }
        
        String message = constraintException.getMessage();
        if (message != null) {
			String searchWord = "ORA-02290: check constraint (";
			Integer idx = message.indexOf(searchWord);
			if (idx != -1) {
				Integer rightPar = message.indexOf(')',idx);
				return message.substring(idx+searchWord.length(),rightPar);
			}
		}
        return message;
    }
    
    public  Response handleWebRequest(Func<Response> javaClosure) {
        try {
            return javaClosure.lambda();
        } catch (NullPointerException e) {
            String errMsg = (e != null && e.getMessage() != null ? e.getMessage() : "NullPointerException");
            return Response.status(Response.Status.NOT_ACCEPTABLE).type("text/html;charset=UTF-8").entity(errMsg).build();
        } catch (Exception e) {
            String errMsg = (e != null && e.getMessage() != null ? e.getMessage() : "UNKNOWN_ERROR");
            return Response.status(Response.Status.NOT_ACCEPTABLE).type("text/html;charset=UTF-8").entity(errMsg).build();
        }
    }

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
