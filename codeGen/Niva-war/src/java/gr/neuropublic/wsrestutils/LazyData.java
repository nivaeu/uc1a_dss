/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.wsrestutils;

import gr.neuropublic.base.SqlQueryRow;
import gr.neuropublic.exceptions.GenericApplicationException;
import gr.neuropublic.validators.ICheckedEntity;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gmamais
 */
public class LazyData {
    
    public enum requestType {
        DATA,
        COUNT
    }    

    public int count=0;
    
    public Object data;
    
    public String toJsonUnencrypted() {
        return _toJson(null);
    }
    public String toJson(gr.neuropublic.base.CryptoUtils crypto) {
        if (crypto == null)
            throw new GenericApplicationException("Crypto is null in toJson of LazyData!");
        return _toJson(crypto);
    }
    private String _toJson(gr.neuropublic.base.CryptoUtils crypto) {
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        sb.append("\"count\":"); sb.append(count);sb.append(",");
        List<ICheckedEntity> entities = (List<ICheckedEntity>)data;
        sb.append("\"data\":[");
        
        java.util.Set<String> tmp = new java.util.HashSet<String>();
        int length = entities.size();
        if (length>0) {
            sb.append(entities.get(0).toJson(tmp, crypto));
            for(int i=1;i<length; i++) {
                sb.append(',');
                sb.append(entities.get(i).toJson(tmp, crypto));
            }
        }
        
        sb.append("]}"); 
        return sb.toString();
    }
    
    public String toJson2() {
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        sb.append("\"count\":"); sb.append(count);sb.append(",");
        List<SqlQueryRow> entities = (List<SqlQueryRow>)data;
        sb.append("\"data\":[");
        
        int length = entities.size();
        if (length>0) {
            sb.append(entities.get(0).toJson());
            for(int i=1;i<length; i++) {
                sb.append(',');
                sb.append(entities.get(i).toJson());
            }
        }
        
        sb.append("]}"); 
        return sb.toString();
    }
    
    
    
    public byte[] toGeoJson(int srid) {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"crs\": {\"type\": \"EPSG\", \"properties\": {\"code\": \"" + srid + "\"}}, \"type\": \"FeatureCollection\", \"features\": ["); 
        List<SqlQueryRow> entities = (List<SqlQueryRow>)data;

        int length = entities.size();
        if (length>0) {
            entities.get(0).toGeoJson(sb);
            for(int i=1;i<length; i++) {
                sb.append(',');
                entities.get(i).toGeoJson(sb);
            }
        }
        
        
        sb.append("]}"); 
        try {
            return sb.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(LazyData.class.getName()).log(Level.SEVERE, null, ex);
            throw new GenericApplicationException(ex.getMessage());
        }
    }
    
	
    public String toJsonData(gr.neuropublic.base.CryptoUtils crypto) {
        StringBuilder sb = new StringBuilder();
        List<ICheckedEntity> entities = (List<ICheckedEntity>)data;
        sb.append("[");
        
        java.util.Set<String> tmp = new java.util.HashSet<String>();
        int length = entities.size();
        if (length>0) {
            sb.append(entities.get(0).toJson(tmp, crypto));
            for(int i=1;i<length; i++) {
                sb.append(',');
                sb.append(entities.get(i).toJson(tmp, crypto));
            }
        }
        
        sb.append("]"); 
        return sb.toString();
    }
	
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
