package gr.neuropublic.wsrestutils;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

/**
 *
 * @author gmamais
 */
public  class LazyFieldExclusionStrategy implements ExclusionStrategy {

    public boolean shouldSkipClass(Class<?> clazz) {
      return false;
    }

    public boolean shouldSkipField(FieldAttributes f) {
      return f.getDeclaredClass().getCanonicalName().contains("[]") || f.getDeclaredClass().getCanonicalName().contains("$$_javassist") || (f.getAnnotation(javax.persistence.OneToMany.class) != null);
    }
}    
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
