/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.wsrestutils;

/**
 *
 * @author gmamais
 */
public class DynamicLayerInfo {
    public String code;
    public String sqlQuery;
    //public String jdbcDriver; 
    //public String jdbcConnectionURL;
    //public String user; 
    //public String pass;
	public String service;
    public int SRID;
    public int nMaxWindowDX;
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
