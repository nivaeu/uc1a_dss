package gr.neuropublic.enums;


public enum EditMode
{

    CREATE(1, "Create"),
    UPDATE(2, "Update");

    EditMode(Number value, String description)
    {
        this.value = value;
        this.description = description;
    }

    private Number value;

    public Number getValue()
    {
        return value;
    }

    public void setValue(Number value)
    {
        this.value = value;
    }

    private String description;

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
