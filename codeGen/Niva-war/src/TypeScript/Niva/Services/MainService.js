//A2BF38ED5159E589D1B1C8B6309F92DA
/// <reference path="../ServicesBase/MainServiceBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Services;
(function (Services) {
    var MainService = (function (_super) {
        __extends(MainService, _super);
        function MainService() {
            _super.apply(this, arguments);
        }
        return MainService;
    })(Services.MainServiceBase);
    Services.MainService = MainService;
})(Services || (Services = {}));
//# sourceMappingURL=MainService.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
