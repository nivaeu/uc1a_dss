//A2BF38ED5159E589D1B1C8B6309F92DA
/// <reference path="../ServicesBase/MainServiceBase.ts" />

module Services {

    export class MainService extends MainServiceBase {
    }

}

 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
