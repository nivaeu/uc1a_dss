//AB9101FC56873AB5E2CF4A361D44FD73
//Εxtended classes
/// <reference path="../EntitiesBase/IntegrateddecisionBase.ts" />

module Entities {

    export class Integrateddecision extends Entities.IntegrateddecisionBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Integrateddecision> {
            return Entities.IntegrateddecisionBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
