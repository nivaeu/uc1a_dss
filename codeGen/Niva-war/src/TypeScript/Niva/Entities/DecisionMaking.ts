//878BEBA3CA4ECA36AE58A14BA4F2AF83
//Εxtended classes
/// <reference path="../EntitiesBase/DecisionMakingBase.ts" />

module Entities {

    export class DecisionMaking extends Entities.DecisionMakingBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<DecisionMaking> {
            return Entities.DecisionMakingBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
