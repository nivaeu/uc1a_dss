//0FFD49D2400DDDD038E9EB98547369D2
//Εxtended classes
/// <reference path="../EntitiesBase/StatisticBase.ts" />

module Entities {

    export class Statistic extends Entities.StatisticBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Statistic> {
            return Entities.StatisticBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
