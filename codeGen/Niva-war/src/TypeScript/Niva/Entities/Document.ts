//1109DF6A0AE8BF72A5FFB15706754C3E
//Εxtended classes
/// <reference path="../EntitiesBase/DocumentBase.ts" />

module Entities {

    export class Document extends Entities.DocumentBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Document> {
            return Entities.DocumentBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
