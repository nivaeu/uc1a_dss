//52D2E6DC036609E5B2009F7E47994BAA
//Εxtended classes
/// <reference path="../EntitiesBase/ParcelDecisionBase.ts" />

module Entities {

    export class ParcelDecision extends Entities.ParcelDecisionBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<ParcelDecision> {
            return Entities.ParcelDecisionBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
