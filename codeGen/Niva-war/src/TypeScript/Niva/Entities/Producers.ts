//67482CCA7D76EDC69BE72E1317DB9B50
//Εxtended classes
/// <reference path="../EntitiesBase/ProducersBase.ts" />

module Entities {

    export class Producers extends Entities.ProducersBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Producers> {
            return Entities.ProducersBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
