//925BC14783A547EE944145A81A3CD0A1
//Εxtended classes
/// <reference path="../EntitiesBase/EcGroupBase.ts" />

module Entities {

    export class EcGroup extends Entities.EcGroupBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<EcGroup> {
            return Entities.EcGroupBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
