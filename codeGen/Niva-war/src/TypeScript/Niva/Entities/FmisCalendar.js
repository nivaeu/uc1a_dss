//1E38B10F06923344BE23B5A94E560425
//Εxtended classes
/// <reference path="../EntitiesBase/FmisCalendarBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var FmisCalendar = (function (_super) {
        __extends(FmisCalendar, _super);
        function FmisCalendar() {
            _super.apply(this, arguments);
        }
        FmisCalendar.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.FmisCalendarBase.fromJSONComplete(data, deserializedEntities);
        };
        return FmisCalendar;
    })(Entities.FmisCalendarBase);
    Entities.FmisCalendar = FmisCalendar;
})(Entities || (Entities = {}));
//# sourceMappingURL=FmisCalendar.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
