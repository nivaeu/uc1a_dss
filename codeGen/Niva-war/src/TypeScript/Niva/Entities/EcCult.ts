//679F4CAA3C958057503C79BC3484FF5A
//Εxtended classes
/// <reference path="../EntitiesBase/EcCultBase.ts" />

module Entities {

    export class EcCult extends Entities.EcCultBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<EcCult> {
            return Entities.EcCultBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
