//B56E3E1C6E01E5888A2A0E2AEC2091D7
//Εxtended classes
/// <reference path="../EntitiesBase/ClassifierBase.ts" />

module Entities {

    export class Classifier extends Entities.ClassifierBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Classifier> {
            return Entities.ClassifierBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
