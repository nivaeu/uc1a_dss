//CBB799A9D06D643A0287B914444081F6
//Εxtended classes
/// <reference path="../EntitiesBase/SuperClasBase.ts" />

module Entities {

    export class SuperClas extends Entities.SuperClasBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<SuperClas> {
            return Entities.SuperClasBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
