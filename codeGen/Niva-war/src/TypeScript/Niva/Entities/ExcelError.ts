//CB3B97821C9756A90CFD9F0AACF5E1F2
//Εxtended classes
/// <reference path="../EntitiesBase/ExcelErrorBase.ts" />

module Entities {

    export class ExcelError extends Entities.ExcelErrorBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<ExcelError> {
            return Entities.ExcelErrorBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
