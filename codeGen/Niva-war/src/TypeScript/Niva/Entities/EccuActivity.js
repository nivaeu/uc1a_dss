//E27181D2C5F6093BB74230B3943DFFCE
//Εxtended classes
/// <reference path="../EntitiesBase/EccuActivityBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var EccuActivity = (function (_super) {
        __extends(EccuActivity, _super);
        function EccuActivity() {
            _super.apply(this, arguments);
        }
        EccuActivity.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.EccuActivityBase.fromJSONComplete(data, deserializedEntities);
        };
        return EccuActivity;
    })(Entities.EccuActivityBase);
    Entities.EccuActivity = EccuActivity;
})(Entities || (Entities = {}));
//# sourceMappingURL=EccuActivity.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
