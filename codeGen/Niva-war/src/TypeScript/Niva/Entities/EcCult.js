//679F4CAA3C958057503C79BC3484FF5A
//Εxtended classes
/// <reference path="../EntitiesBase/EcCultBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var EcCult = (function (_super) {
        __extends(EcCult, _super);
        function EcCult() {
            _super.apply(this, arguments);
        }
        EcCult.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.EcCultBase.fromJSONComplete(data, deserializedEntities);
        };
        return EcCult;
    })(Entities.EcCultBase);
    Entities.EcCult = EcCult;
})(Entities || (Entities = {}));
//# sourceMappingURL=EcCult.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
