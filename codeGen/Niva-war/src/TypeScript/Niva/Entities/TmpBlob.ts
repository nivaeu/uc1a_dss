//63041AD5E549498F89CC1BA6F0E2F280
//Εxtended classes
/// <reference path="../EntitiesBase/TmpBlobBase.ts" />

module Entities {

    export class TmpBlob extends Entities.TmpBlobBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<TmpBlob> {
            return Entities.TmpBlobBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
