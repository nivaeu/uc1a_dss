//25EF66C5E035C88393F9D0115D7AFDE3
//Εxtended classes
/// <reference path="../EntitiesBase/ParcActivityBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var ParcActivity = (function (_super) {
        __extends(ParcActivity, _super);
        function ParcActivity() {
            _super.apply(this, arguments);
        }
        ParcActivity.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.ParcActivityBase.fromJSONComplete(data, deserializedEntities);
        };
        return ParcActivity;
    })(Entities.ParcActivityBase);
    Entities.ParcActivity = ParcActivity;
})(Entities || (Entities = {}));
//# sourceMappingURL=ParcActivity.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
