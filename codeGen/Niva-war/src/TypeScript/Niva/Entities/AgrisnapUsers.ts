//8AADE3B9B40495B6BFCD580F5058EFB4
//Εxtended classes
/// <reference path="../EntitiesBase/AgrisnapUsersBase.ts" />

module Entities {

    export class AgrisnapUsers extends Entities.AgrisnapUsersBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<AgrisnapUsers> {
            return Entities.AgrisnapUsersBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
