//AF7491E2ABA8012850E97561E72CBD3E
//Εxtended classes
/// <reference path="../EntitiesBase/ClassificationBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var Classification = (function (_super) {
        __extends(Classification, _super);
        function Classification() {
            _super.apply(this, arguments);
        }
        Classification.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.ClassificationBase.fromJSONComplete(data, deserializedEntities);
        };
        return Classification;
    })(Entities.ClassificationBase);
    Entities.Classification = Classification;
})(Entities || (Entities = {}));
//# sourceMappingURL=Classification.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
