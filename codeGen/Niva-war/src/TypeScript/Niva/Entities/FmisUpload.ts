//789955616F10056F04D87443A31F5BB3
//Εxtended classes
/// <reference path="../EntitiesBase/FmisUploadBase.ts" />

module Entities {

    export class FmisUpload extends Entities.FmisUploadBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<FmisUpload> {
            return Entities.FmisUploadBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
