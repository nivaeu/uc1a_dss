//AC42F303F23F1D9A5E0285EB17948378
//Εxtended classes
/// <reference path="../EntitiesBase/ParcelClasBase.ts" />

module Entities {

    export class ParcelClas extends Entities.ParcelClasBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<ParcelClas> {
            return Entities.ParcelClasBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
