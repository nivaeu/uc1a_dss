//1EC91412567CD2E07112D9939E1523B7
//Εxtended classes
/// <reference path="../EntitiesBase/GpDecisionBase.ts" />

module Entities {

    export class GpDecision extends Entities.GpDecisionBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<GpDecision> {
            return Entities.GpDecisionBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
