//588E0044C92B5C55BADB13B586228E44
//Εxtended classes
/// <reference path="../EntitiesBase/GpRequestsContextsBase.ts" />

module Entities {

    export class GpRequestsContexts extends Entities.GpRequestsContextsBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<GpRequestsContexts> {
            return Entities.GpRequestsContextsBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
