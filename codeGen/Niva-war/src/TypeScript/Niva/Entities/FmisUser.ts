//0AC14049FFE5AC045D0F6C91E1C65ED4
//Εxtended classes
/// <reference path="../EntitiesBase/FmisUserBase.ts" />

module Entities {

    export class FmisUser extends Entities.FmisUserBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<FmisUser> {
            return Entities.FmisUserBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
