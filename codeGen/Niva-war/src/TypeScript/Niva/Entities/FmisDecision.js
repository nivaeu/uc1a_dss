//9D34200D5BC4FC4596F1DE9220D4AFCE
//Εxtended classes
/// <reference path="../EntitiesBase/FmisDecisionBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var FmisDecision = (function (_super) {
        __extends(FmisDecision, _super);
        function FmisDecision() {
            _super.apply(this, arguments);
        }
        FmisDecision.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.FmisDecisionBase.fromJSONComplete(data, deserializedEntities);
        };
        return FmisDecision;
    })(Entities.FmisDecisionBase);
    Entities.FmisDecision = FmisDecision;
})(Entities || (Entities = {}));
//# sourceMappingURL=FmisDecision.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
