//E9BCA2B557C99BC1BD6A2BA0E968DAA6
//Εxtended classes
/// <reference path="../EntitiesBase/AgencyBase.ts" />

module Entities {

    export class Agency extends Entities.AgencyBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Agency> {
            return Entities.AgencyBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
