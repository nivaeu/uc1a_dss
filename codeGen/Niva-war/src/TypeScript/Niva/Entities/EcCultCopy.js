//83BC4A74FD68D3C57FF3A0EEEE7B63AD
//Εxtended classes
/// <reference path="../EntitiesBase/EcCultCopyBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var EcCultCopy = (function (_super) {
        __extends(EcCultCopy, _super);
        function EcCultCopy() {
            _super.apply(this, arguments);
        }
        EcCultCopy.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.EcCultCopyBase.fromJSONComplete(data, deserializedEntities);
        };
        return EcCultCopy;
    })(Entities.EcCultCopyBase);
    Entities.EcCultCopy = EcCultCopy;
})(Entities || (Entities = {}));
//# sourceMappingURL=EcCultCopy.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
