//925BC14783A547EE944145A81A3CD0A1
//Εxtended classes
/// <reference path="../EntitiesBase/EcGroupBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var EcGroup = (function (_super) {
        __extends(EcGroup, _super);
        function EcGroup() {
            _super.apply(this, arguments);
        }
        EcGroup.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.EcGroupBase.fromJSONComplete(data, deserializedEntities);
        };
        return EcGroup;
    })(Entities.EcGroupBase);
    Entities.EcGroup = EcGroup;
})(Entities || (Entities = {}));
//# sourceMappingURL=EcGroup.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
