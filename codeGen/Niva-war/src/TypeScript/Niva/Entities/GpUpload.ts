//8D57FFB7AE38A742A550B99E8DC72680
//Εxtended classes
/// <reference path="../EntitiesBase/GpUploadBase.ts" />

module Entities {

    export class GpUpload extends Entities.GpUploadBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<GpUpload> {
            return Entities.GpUploadBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
