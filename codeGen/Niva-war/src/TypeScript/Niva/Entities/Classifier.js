//B56E3E1C6E01E5888A2A0E2AEC2091D7
//Εxtended classes
/// <reference path="../EntitiesBase/ClassifierBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var Classifier = (function (_super) {
        __extends(Classifier, _super);
        function Classifier() {
            _super.apply(this, arguments);
        }
        Classifier.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            return Entities.ClassifierBase.fromJSONComplete(data, deserializedEntities);
        };
        return Classifier;
    })(Entities.ClassifierBase);
    Entities.Classifier = Classifier;
})(Entities || (Entities = {}));
//# sourceMappingURL=Classifier.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
