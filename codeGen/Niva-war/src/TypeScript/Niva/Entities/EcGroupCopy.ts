//B22C91017AA9CB00834401391B017D44
//Εxtended classes
/// <reference path="../EntitiesBase/EcGroupCopyBase.ts" />

module Entities {

    export class EcGroupCopy extends Entities.EcGroupCopyBase 
    { 
        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<EcGroupCopy> {
            return Entities.EcGroupCopyBase.fromJSONComplete(data, deserializedEntities);
        }
    } 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
