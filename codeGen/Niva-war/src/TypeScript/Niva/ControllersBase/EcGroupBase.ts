/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/EcGroupBase.ts" />
/// <reference path="../EntitiesBase/EcCultBase.ts" />
/// <reference path="../EntitiesBase/EcCultDetailBase.ts" />
/// <reference path="../EntitiesBase/CultivationBase.ts" />
/// <reference path="LovCultivationBase.ts" />
/// <reference path="../EntitiesBase/CoverTypeBase.ts" />
/// <reference path="LovCoverTypeBase.ts" />
/// <reference path="../Controllers/EcGroup.ts" />
module Controllers.EcGroup {
    export class PageModelBase extends AbstractPageModel {
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        modelGrpEcGroup:ModelGrpEcGroup;
        modelGrpEcCult:ModelGrpEcCult;
        modelGrpEcCultDetail:ModelGrpEcCultDetail;
        modelEcCultCover:ModelEcCultCover;
        modelEcCultDetailCover:ModelEcCultDetailCover;
        modelEcCultSuper:ModelEcCultSuper;
        modelEcCultDetailSuper:ModelEcCultDetailSuper;
        controller: PageController;
        constructor(public $scope: IPageScope) { super($scope); }
    }


    export interface IPageScopeBase extends IAbstractPageScope {
        globals: Globals;
        _saveIsDisabled():boolean; 
        _cancelIsDisabled():boolean; 
        pageModel : PageModel;
        onSaveBtnAction(): void;
        onNewBtnAction(): void;
        onDeleteBtnAction():void;

    }

    export class PageControllerBase extends AbstractPageController {
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model:PageModel)
        {
            super($scope, $http, $timeout, Plato, model);
            var self: PageControllerBase = this;
            model.controller = <PageController>self;
            $scope.pageModel = self.model;

            $scope._saveIsDisabled = 
                () => {
                    return self._saveIsDisabled();
                };
            $scope._cancelIsDisabled = 
                () => {
                    return self._cancelIsDisabled();
                };

            $scope.onSaveBtnAction = () => { 
                self.onSaveBtnAction((response:NpTypes.SaveResponce) => {self.onSuccesfullSaveFromButton(response);});
            };
            

            $scope.onNewBtnAction = () => { 
                self.onNewBtnAction();
            };
            
            $scope.onDeleteBtnAction = () => { 
                self.onDeleteBtnAction();
            };


            $timeout(function() {
                $('#EcGroup').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                $('#NpMainContent').scrollTop(0);
            }, 0);

        }
        public get ControllerClassName(): string {
            return "PageController";
        }
        public get HtmlDivId(): string {
            return "EcGroup";
        }
    
        public _getPageTitle(): string {
            return "Eligibility Criteria";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                ];
            }
            return this._items;
        }

        public _saveIsDisabled():boolean {
            var self = this;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _cancelIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }

        public get pageModel():PageModel {
            var self = this;
            return self.model;
        }

        
        public update():void {
            var self = this;
            self.model.modelGrpEcGroup.controller.updateUI();
        }

        public refreshVisibleEntities(newEntitiesIds: NpTypes.NewEntityId[]):void {
            var self = this;
            self.model.modelGrpEcGroup.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpEcCult.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpEcCultDetail.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelEcCultCover.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelEcCultDetailCover.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelEcCultSuper.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelEcCultDetailSuper.controller.refreshVisibleEntities(newEntitiesIds);
        }

        public refreshGroups(newEntitiesIds: NpTypes.NewEntityId[]): void {
            var self = this;
            self.model.modelGrpEcGroup.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpEcCult.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpEcCultDetail.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelEcCultCover.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelEcCultDetailCover.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelEcCultSuper.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelEcCultDetailSuper.controller.refreshVisibleEntities(newEntitiesIds);
        }

        _firstLevelGroupControllers: Array<AbstractGroupController>=undefined;
        public get FirstLevelGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelGroupControllers === undefined) {
                self._firstLevelGroupControllers = [
                    self.model.modelGrpEcGroup.controller
                ];
            }
            return this._firstLevelGroupControllers;
        }

        _allGroupControllers: Array<AbstractGroupController>=undefined;
        public get AllGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._allGroupControllers === undefined) {
                self._allGroupControllers = [
                    self.model.modelGrpEcGroup.controller,
                    self.model.modelGrpEcCult.controller,
                    self.model.modelGrpEcCultDetail.controller,
                    self.model.modelEcCultCover.controller,
                    self.model.modelEcCultDetailCover.controller,
                    self.model.modelEcCultSuper.controller,
                    self.model.modelEcCultDetailSuper.controller
                ];
            }
            return this._allGroupControllers;
        }

        public getPageChanges(bForDelete:boolean = false):Array<ChangeToCommit> {
            var self = this;
            var pageChanges: Array<ChangeToCommit> = [];
            if (bForDelete) {
                pageChanges = pageChanges.concat(self.model.modelGrpEcGroup.controller.getChangesToCommitForDeletion());
                pageChanges = pageChanges.concat(self.model.modelGrpEcGroup.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpEcCult.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpEcCultDetail.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelEcCultCover.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelEcCultDetailCover.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelEcCultSuper.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelEcCultDetailSuper.controller.getDeleteChangesToCommit());
            } else {

                pageChanges = pageChanges.concat(self.model.modelGrpEcGroup.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpEcCult.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpEcCultDetail.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelEcCultCover.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelEcCultDetailCover.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelEcCultSuper.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelEcCultDetailSuper.controller.getChangesToCommit());
            }
            
            var hasEcGroup = pageChanges.some(change => change.entityName === "EcGroup")
            if (!hasEcGroup) {
                var validateEntity = new ChangeToCommit(ChangeStatus.UPDATE, 0, "EcGroup", self.model.modelGrpEcGroup.controller.Current);
                pageChanges = pageChanges.concat(validateEntity);
            }
            return pageChanges;
        }

        private getSynchronizeChangesWithDbUrl():string { 
            return "/Niva/rest/MainService/synchronizeChangesWithDb_EcGroup"; 
        }
        
        public getSynchronizeChangesWithDbData(bForDelete:boolean = false):any {
            var self = this;
            var paramData:any = {}
            paramData.data = self.getPageChanges(bForDelete);
            return paramData;
        }


        public onSuccesfullSaveFromButton(response:NpTypes.SaveResponce) {
            var self = this;
            super.onSuccesfullSaveFromButton(response);
            if (!isVoid(response.warningMessages)) {
                NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
            }
            if (!isVoid(response.infoMessages)) {
                NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
            }

            self.model.modelGrpEcGroup.controller.cleanUpAfterSave();
            self.model.modelGrpEcCult.controller.cleanUpAfterSave();
            self.model.modelGrpEcCultDetail.controller.cleanUpAfterSave();
            self.model.modelEcCultCover.controller.cleanUpAfterSave();
            self.model.modelEcCultDetailCover.controller.cleanUpAfterSave();
            self.model.modelEcCultSuper.controller.cleanUpAfterSave();
            self.model.modelEcCultDetailSuper.controller.cleanUpAfterSave();
            self.$scope.globals.isCurrentTransactionDirty = false;
            self.refreshGroups(response.newEntitiesIds);
        }

        public onFailuredSave(data:any, status:number) {
            var self = this;
            if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
            var errors = Messages.dynamicMessage(data);
            NpTypes.AlertMessage.addDanger(self.model, errors);
            messageBox( self.$scope, self.Plato,"MessageBox_Attention_Title",
                "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>",
                IconKind.ERROR, [new Tuple2("OK", () => {}),], 0, 0,'50em');
        }
        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }
        
        public onSaveBtnAction(onSuccess:(response:NpTypes.SaveResponce)=>void): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            var errors = self.validatePage();
            if (errors.length > 0) {
                var errMessage = errors.join('<br>');
                NpTypes.AlertMessage.addDanger(self.model, errMessage);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title",
                    "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>",
                    IconKind.ERROR,[new Tuple2("OK", () => {}),], 0, 0,'50em');
                return;
            }

            if (self.$scope.globals.isCurrentTransactionDirty === false) {
                var jqSaveBtn = self.SaveBtnJQueryHandler;
                self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                return;
            }

            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData();
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success((response:NpTypes.SaveResponce, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;

                    if (!self.shownAsDialog()) {
                        var breadcrumbStepModel = <IFormPageBreadcrumbStepModel<Entities.EcGroup>>self.$scope.getCurrentPageModel();
                        var newEcGroupId = response.newEntitiesIds.firstOrNull(x => x.entityName === 'EcGroup');
                        if (!isVoid(newEcGroupId)) {
                            if (!isVoid(breadcrumbStepModel)) {
                                breadcrumbStepModel.selectedEntity = Entities.EcGroup.CreateById(newEcGroupId.databaseId);
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        } else {
                            if (!isVoid(breadcrumbStepModel) && !(isVoid(breadcrumbStepModel.selectedEntity))) {
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        self.$scope.setCurrentPageModel(breadcrumbStepModel);
                    }

                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status)
            });
        }

        public getInitialEntity(): Entities.EcGroup {
            if (this.shownAsDialog()) {
                return <Entities.EcGroup>this.dialogSelectedEntity();
            } else {
                var breadCrumbStepModel = <IFormPageBreadcrumbStepModel<Entities.EcGroup>>this.$scope.getPageModelByURL('/EcGroup');
                if (isVoid(breadCrumbStepModel)) {
                    return null;
                } else {
                    return isVoid(breadCrumbStepModel.selectedEntity) ? null : breadCrumbStepModel.selectedEntity;
                }
            }
        }



        public onPageUnload(actualNavigation: (pageScopeYouAreLeavingFrom?: NpTypes.IApplicationScope) => void) {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => {
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                actualNavigation(self.$scope);
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                actualNavigation(self.$scope);
            }
        }


        public onNewBtnAction(): void {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => { 
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                self.addNewRecord();
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {self.addNewRecord();}),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                self.addNewRecord();
            }
        }

        public addNewRecord(): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            self.model.modelGrpEcGroup.controller.cleanUpAfterSave();
            self.model.modelGrpEcCult.controller.cleanUpAfterSave();
            self.model.modelGrpEcCultDetail.controller.cleanUpAfterSave();
            self.model.modelEcCultCover.controller.cleanUpAfterSave();
            self.model.modelEcCultDetailCover.controller.cleanUpAfterSave();
            self.model.modelEcCultSuper.controller.cleanUpAfterSave();
            self.model.modelEcCultDetailSuper.controller.cleanUpAfterSave();
            self.model.modelGrpEcGroup.controller.createNewEntityAndAddToUI();
        }

        public onDeleteBtnAction():void {
            var self = this;
            messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("EntityDeletionWarningMsg"), IconKind.QUESTION, 
                [
                    new Tuple2("MessageBox_Button_Yes", () => {self.deleteRecord();}),
                    new Tuple2("MessageBox_Button_No", () => {})
                ], 1,1);
        }

        public deleteRecord(): void {
            var self = this;
            if (self.Mode === EditMode.NEW) {
                console.log("Delete pressed in a form while being in new mode!");
                return;
            }
            NpTypes.AlertMessage.clearAlerts(self.model);
            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData(true);
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.$scope.navigateBackward();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.model, Messages.dynamicMessage(data));
            });
        }
        
        public get Mode(): EditMode {
            var self = this;
            if (isVoid(self.model.modelGrpEcGroup) || isVoid(self.model.modelGrpEcGroup.controller))
                return EditMode.NEW;
            return self.model.modelGrpEcGroup.controller.Mode;
        }

        public _newIsDisabled(): boolean {
            var self = this;
            if (isVoid(self.model.modelGrpEcGroup) || isVoid(self.model.modelGrpEcGroup.controller))
                return true;
            return self.model.modelGrpEcGroup.controller._newIsDisabled();
        }
        public _deleteIsDisabled(): boolean {
            var self = this;
            if (self.Mode === EditMode.NEW)
                return true;
            if (isVoid(self.model.modelGrpEcGroup) || isVoid(self.model.modelGrpEcGroup.controller))
                return true;
            return self.model.modelGrpEcGroup.controller._deleteIsDisabled(self.model.modelGrpEcGroup.controller.Current);
        }


    }


    

    // GROUP GrpEcGroup

    export class ModelGrpEcGroupBase extends Controllers.AbstractGroupFormModel {
        modelGrpEcCult:ModelGrpEcCult;
        modelEcCultCover:ModelEcCultCover;
        modelEcCultSuper:ModelEcCultSuper;
        controller: ControllerGrpEcGroup;
        public get ecgrId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcGroup>this.selectedEntities[0]).ecgrId;
        }

        public set ecgrId(ecgrId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcGroup>this.selectedEntities[0]).ecgrId = ecgrId_newVal;
        }

        public get _ecgrId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._ecgrId;
        }

        public get description():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcGroup>this.selectedEntities[0]).description;
        }

        public set description(description_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcGroup>this.selectedEntities[0]).description = description_newVal;
        }

        public get _description():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._description;
        }

        public get name():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcGroup>this.selectedEntities[0]).name;
        }

        public set name(name_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcGroup>this.selectedEntities[0]).name = name_newVal;
        }

        public get _name():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._name;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcGroup>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcGroup>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get recordtype():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcGroup>this.selectedEntities[0]).recordtype;
        }

        public set recordtype(recordtype_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcGroup>this.selectedEntities[0]).recordtype = recordtype_newVal;
        }

        public get _recordtype():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._recordtype;
        }

        public get cropLevel():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcGroup>this.selectedEntities[0]).cropLevel;
        }

        public set cropLevel(cropLevel_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcGroup>this.selectedEntities[0]).cropLevel = cropLevel_newVal;
        }

        public get _cropLevel():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cropLevel;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpEcGroup) { super($scope); }
    }



    export interface IScopeGrpEcGroupBase extends Controllers.IAbstractFormGroupScope, IPageScope{
        globals: Globals;
        modelGrpEcGroup : ModelGrpEcGroup;
        GrpEcGroup_itm__name_disabled(ecGroup:Entities.EcGroup):boolean; 
        GrpEcGroup_itm__description_disabled(ecGroup:Entities.EcGroup):boolean; 
        cropLevelChange(ecGroup:Entities.EcGroup):void; 
        GrpEcGroup_itm__cropLevel_disabled(ecGroup:Entities.EcGroup):boolean; 
        GrpEcGroup_0_disabled():boolean; 
        GrpEcGroup_0_invisible():boolean; 
        GrpEcGroup_1_disabled():boolean; 
        GrpEcGroup_1_invisible():boolean; 
        GrpEcGroup_2_disabled():boolean; 
        GrpEcGroup_2_invisible():boolean; 
        child_group_GrpEcCult_isInvisible():boolean; 
        child_group_EcCultCover_isInvisible():boolean; 
        child_group_EcCultSuper_isInvisible():boolean; 

    }



    export class ControllerGrpEcGroupBase extends Controllers.AbstractGroupFormController {
        constructor(
            public $scope: IScopeGrpEcGroup,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpEcGroup)
        {
            super($scope, $http, $timeout, Plato, model);
            var self:ControllerGrpEcGroupBase = this;
            model.controller = <ControllerGrpEcGroup>self;
            $scope.modelGrpEcGroup = self.model;

            var selectedEntity: Entities.EcGroup = this.$scope.pageModel.controller.getInitialEntity();
            if (isVoid(selectedEntity)) {
                self.createNewEntityAndAddToUI();
            } else {
                var clonedEntity = Entities.EcGroup.Create();
                clonedEntity.updateInstance(selectedEntity);
                $scope.modelGrpEcGroup.visibleEntities[0] = clonedEntity;
                $scope.modelGrpEcGroup.selectedEntities[0] = clonedEntity;
            } 
            $scope.GrpEcGroup_itm__name_disabled = 
                (ecGroup:Entities.EcGroup) => {
                    return self.GrpEcGroup_itm__name_disabled(ecGroup);
                };
            $scope.GrpEcGroup_itm__description_disabled = 
                (ecGroup:Entities.EcGroup) => {
                    return self.GrpEcGroup_itm__description_disabled(ecGroup);
                };
            $scope.cropLevelChange = 
                (ecGroup:Entities.EcGroup) => {
                    self.cropLevelChange(ecGroup);
                };
            $scope.GrpEcGroup_itm__cropLevel_disabled = 
                (ecGroup:Entities.EcGroup) => {
                    return self.GrpEcGroup_itm__cropLevel_disabled(ecGroup);
                };
            $scope.GrpEcGroup_0_disabled = 
                () => {
                    return self.GrpEcGroup_0_disabled();
                };
            $scope.GrpEcGroup_0_invisible = 
                () => {
                    return self.GrpEcGroup_0_invisible();
                };
            $scope.GrpEcGroup_1_disabled = 
                () => {
                    return self.GrpEcGroup_1_disabled();
                };
            $scope.GrpEcGroup_1_invisible = 
                () => {
                    return self.GrpEcGroup_1_invisible();
                };
            $scope.GrpEcGroup_2_disabled = 
                () => {
                    return self.GrpEcGroup_2_disabled();
                };
            $scope.GrpEcGroup_2_invisible = 
                () => {
                    return self.GrpEcGroup_2_invisible();
                };
            $scope.child_group_GrpEcCult_isInvisible = 
                () => {
                    return self.child_group_GrpEcCult_isInvisible();
                };
            $scope.child_group_EcCultCover_isInvisible = 
                () => {
                    return self.child_group_EcCultCover_isInvisible();
                };
            $scope.child_group_EcCultSuper_isInvisible = 
                () => {
                    return self.child_group_EcCultSuper_isInvisible();
                };


            $scope.pageModel.modelGrpEcGroup = $scope.modelGrpEcGroup;


    /*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcGroup[] , oldVisible:Entities.EcGroup[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
            //bind entity child collection with child controller merged items
            Entities.EcGroup.ecCultCollection = (ecGroup, func) => {
                this.model.modelGrpEcCult.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, ecGroup);
            }
            //bind entity child collection with child controller merged items
            Entities.EcGroup.ecCultCollection = (ecGroup, func) => {
                this.model.modelEcCultCover.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, ecGroup);
            }
            //bind entity child collection with child controller merged items
            Entities.EcGroup.ecCultCollection = (ecGroup, func) => {
                this.model.modelEcCultSuper.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, ecGroup);
            }

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
                //unbind entity child collection with child controller merged items
                Entities.EcGroup.ecCultCollection = null;//(ecGroup, func) => { }
                //unbind entity child collection with child controller merged items
                Entities.EcGroup.ecCultCollection = null;//(ecGroup, func) => { }
                //unbind entity child collection with child controller merged items
                Entities.EcGroup.ecCultCollection = null;//(ecGroup, func) => { }
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpEcGroup";
        }
        public get HtmlDivId(): string {
            return "EcGroup_ControllerGrpEcGroup";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new TextItem (
                        (ent?: Entities.EcGroup) => 'Name',
                        false ,
                        (ent?: Entities.EcGroup) => ent.name,  
                        (ent?: Entities.EcGroup) => ent._name,  
                        (ent?: Entities.EcGroup) => true, 
                        (vl: string, ent?: Entities.EcGroup) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new TextItem (
                        (ent?: Entities.EcGroup) => 'Description',
                        false ,
                        (ent?: Entities.EcGroup) => ent.description,  
                        (ent?: Entities.EcGroup) => ent._description,  
                        (ent?: Entities.EcGroup) => false, 
                        (vl: string, ent?: Entities.EcGroup) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new StaticListItem<number> (
                        (ent?: Entities.EcGroup) => 'Crop Level',
                        false ,
                        (ent?: Entities.EcGroup) => ent.cropLevel,  
                        (ent?: Entities.EcGroup) => ent._cropLevel,  
                        (ent?: Entities.EcGroup) => true, //isRequired
                        (vl: number, ent?: Entities.EcGroup) => new NpTypes.ValidationResult(true, ""))
                ];
            }
            return this._items;
        }

        

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpEcGroup():ModelGrpEcGroup {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "EcGroup";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
        }

        public setCurrentFormPageBreadcrumpStepModel() {
            var self = this;
            if (self.$scope.pageModel.controller.shownAsDialog())
                return;

            var breadCrumbStepModel: IFormPageBreadcrumbStepModel<Entities.EcGroup> = { selectedEntity: null };
            if (!isVoid(self.Current)) {
                var selectedEntity: Entities.EcGroup = Entities.EcGroup.Create();
                selectedEntity.updateInstance(self.Current);
                breadCrumbStepModel.selectedEntity = selectedEntity;
            } else {
                breadCrumbStepModel.selectedEntity = null;
            }
            self.$scope.setCurrentPageModel(breadCrumbStepModel);
        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.EcGroup.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.EcGroup {
            var self = this;
            return <Entities.EcGroup>self.$scope.modelGrpEcGroup.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.EcGroup, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  false;

        }







        public constructEntity(): Entities.EcGroup {
            var self = this;
            var ret = new Entities.EcGroup(
                /*ecgrId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*description:string*/ null,
                /*name:string*/ null,
                /*rowVersion:number*/ null,
                /*recordtype:number*/ 0,
                /*cropLevel:number*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.EcGroup = <Entities.EcGroup>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.$scope.modelGrpEcGroup.visibleEntities[0] = newEnt;
            self.$scope.modelGrpEcGroup.selectedEntities[0] = newEnt;
            return newEnt;

        }

        public cloneEntity(src: Entities.EcGroup): Entities.EcGroup {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.EcGroup, calledByParent: boolean= false): Entities.EcGroup {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.EcGroup.Create();
            ret.updateInstance(src);
            ret.ecgrId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.visibleEntities[0] = ret;
            this.model.selectedEntities[0] = ret;

            this.$timeout( () => {
                this.modelGrpEcGroup.modelGrpEcCult.controller.cloneAllEntitiesUnderParent(src, ret);
                this.modelGrpEcGroup.modelEcCultCover.controller.cloneAllEntitiesUnderParent(src, ret);
                this.modelGrpEcGroup.modelEcCultSuper.controller.cloneAllEntitiesUnderParent(src, ret);
            },1);

            return ret;
        }



        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                    self.modelGrpEcGroup.modelGrpEcCult.controller,
                    self.modelGrpEcGroup.modelEcCultCover.controller,
                    self.modelGrpEcGroup.modelEcCultSuper.controller
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.EcGroup, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                self.modelGrpEcGroup.modelGrpEcCult.controller.deleteAllEntitiesUnderParent(ent, () => {
                    self.modelGrpEcGroup.modelEcCultCover.controller.deleteAllEntitiesUnderParent(ent, () => {
                        self.modelGrpEcGroup.modelEcCultSuper.controller.deleteAllEntitiesUnderParent(ent, () => {
                            super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                        });
                    });
                });
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    self.modelGrpEcGroup.modelGrpEcCult.controller.deleteNewEntitiesUnderParent(ent);
                    self.modelGrpEcGroup.modelEcCultCover.controller.deleteNewEntitiesUnderParent(ent);
                    self.modelGrpEcGroup.modelEcCultSuper.controller.deleteNewEntitiesUnderParent(ent);
                    afterDeleteAction();
                });
            }
        }


        public GrpEcGroup_itm__name_disabled(ecGroup:Entities.EcGroup):boolean {
            var self = this;
            if (ecGroup === undefined || ecGroup === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecGroup, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpEcGroup_itm__description_disabled(ecGroup:Entities.EcGroup):boolean {
            var self = this;
            if (ecGroup === undefined || ecGroup === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecGroup, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public cropLevelChange(ecGroup:Entities.EcGroup) {
            console.log("cropLevelChange called")
            console.dir(ecGroup);
        }
        public isCropLevelDisabled(ecGroup:Entities.EcGroup):boolean { 
            console.warn("Unimplemented function isCropLevelDisabled()");
            return false; 
        }
        public GrpEcGroup_itm__cropLevel_disabled(ecGroup:Entities.EcGroup):boolean {
            var self = this;
            if (ecGroup === undefined || ecGroup === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecGroup, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = self.isCropLevelDisabled(ecGroup);
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpEcGroup_0_disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpEcGroup_0_invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  this.isEcGroupChildInv();
            return programmerVal;
        }
        public isEcGroupChildInv():boolean { 
            console.warn("Unimplemented function isEcGroupChildInv()");
            return false; 
        }
        public GrpEcGroup_1_disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpEcGroup_1_invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  this.isEcGroupCoverChildInv();
            return programmerVal;
        }
        public isEcGroupCoverChildInv():boolean { 
            console.warn("Unimplemented function isEcGroupCoverChildInv()");
            return false; 
        }
        public GrpEcGroup_2_disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpEcGroup_2_invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  this.isEcGroupSuperChildInv();
            return programmerVal;
        }
        public isEcGroupSuperChildInv():boolean { 
            console.warn("Unimplemented function isEcGroupSuperChildInv()");
            return false; 
        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  this.isEcGroupDisabled();
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            var self = this;
            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege

            

            var parEntityIsLocked   = false;
            if (parEntityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public _deleteIsDisabled(ecGroup:Entities.EcGroup):boolean  {
            var self = this;
            if (ecGroup === null || ecGroup === undefined || ecGroup.getEntityName() !== "EcGroup")
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_D"))
                return true; // no delete privilege;




            var entityIsLocked   = self.isEntityLocked(ecGroup, LockKind.DeleteLock);
            if (entityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public isEcGroupDisabled():boolean { 
            console.warn("Unimplemented function isEcGroupDisabled()");
            return false; 
        }
        public child_group_GrpEcCult_isInvisible():boolean {
            var self = this;
            if ((self.model.modelGrpEcCult === undefined) || (self.model.modelGrpEcCult.controller === undefined))
                return false;
            return self.model.modelGrpEcCult.controller._isInvisible()
        }
        public child_group_EcCultCover_isInvisible():boolean {
            var self = this;
            if ((self.model.modelEcCultCover === undefined) || (self.model.modelEcCultCover.controller === undefined))
                return false;
            return self.model.modelEcCultCover.controller._isInvisible()
        }
        public child_group_EcCultSuper_isInvisible():boolean {
            var self = this;
            if ((self.model.modelEcCultSuper === undefined) || (self.model.modelEcCultSuper.controller === undefined))
                return false;
            return self.model.modelEcCultSuper.controller._isInvisible()
        }

    }


    // GROUP GrpEcCult

    export class ModelGrpEcCultBase extends Controllers.AbstractGroupTableModel {
        modelGrpEcCultDetail:ModelGrpEcCultDetail;
        controller: ControllerGrpEcCult;
        public get eccuId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCult>this.selectedEntities[0]).eccuId;
        }

        public set eccuId(eccuId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCult>this.selectedEntities[0]).eccuId = eccuId_newVal;
        }

        public get _eccuId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._eccuId;
        }

        public get noneMatchDecision():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCult>this.selectedEntities[0]).noneMatchDecision;
        }

        public set noneMatchDecision(noneMatchDecision_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCult>this.selectedEntities[0]).noneMatchDecision = noneMatchDecision_newVal;
        }

        public get _noneMatchDecision():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._noneMatchDecision;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCult>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCult>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get cultId():Entities.Cultivation {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCult>this.selectedEntities[0]).cultId;
        }

        public set cultId(cultId_newVal:Entities.Cultivation) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCult>this.selectedEntities[0]).cultId = cultId_newVal;
        }

        public get _cultId():NpTypes.UIManyToOneModel<Entities.Cultivation> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cultId;
        }

        public get ecgrId():Entities.EcGroup {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCult>this.selectedEntities[0]).ecgrId;
        }

        public set ecgrId(ecgrId_newVal:Entities.EcGroup) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCult>this.selectedEntities[0]).ecgrId = ecgrId_newVal;
        }

        public get _ecgrId():NpTypes.UIManyToOneModel<Entities.EcGroup> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._ecgrId;
        }

        public get cotyId():Entities.CoverType {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCult>this.selectedEntities[0]).cotyId;
        }

        public set cotyId(cotyId_newVal:Entities.CoverType) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCult>this.selectedEntities[0]).cotyId = cotyId_newVal;
        }

        public get _cotyId():NpTypes.UIManyToOneModel<Entities.CoverType> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cotyId;
        }

        public get sucaId():Entities.SuperClas {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCult>this.selectedEntities[0]).sucaId;
        }

        public set sucaId(sucaId_newVal:Entities.SuperClas) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCult>this.selectedEntities[0]).sucaId = sucaId_newVal;
        }

        public get _sucaId():NpTypes.UIManyToOneModel<Entities.SuperClas> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._sucaId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpEcCult) { super($scope); }
    }



    export interface IScopeGrpEcCultBase extends Controllers.IAbstractTableGroupScope, IScopeGrpEcGroupBase{
        globals: Globals;
        modelGrpEcCult : ModelGrpEcCult;
        GrpEcCult_itm__cultId_disabled(ecCult:Entities.EcCult):boolean; 
        showLov_GrpEcCult_itm__cultId(ecCult:Entities.EcCult):void; 
        child_group_GrpEcCultDetail_isInvisible():boolean; 

    }



    export class ControllerGrpEcCultBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpEcCult,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpEcCult)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 1});
            var self:ControllerGrpEcCultBase = this;
            model.controller = <ControllerGrpEcCult>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpEcCult = self.model;

            $scope.GrpEcCult_itm__cultId_disabled = 
                (ecCult:Entities.EcCult) => {
                    return self.GrpEcCult_itm__cultId_disabled(ecCult);
                };
            $scope.showLov_GrpEcCult_itm__cultId = 
                (ecCult:Entities.EcCult) => {
                    $timeout( () => {        
                        self.showLov_GrpEcCult_itm__cultId(ecCult);
                    }, 0);
                };
            $scope.child_group_GrpEcCultDetail_isInvisible = 
                () => {
                    return self.child_group_GrpEcCultDetail_isInvisible();
                };


            $scope.pageModel.modelGrpEcCult = $scope.modelGrpEcCult;


            $scope.clearBtnAction = () => { 
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpEcGroup.modelGrpEcCult = $scope.modelGrpEcCult;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpEcGroup.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCult[] , oldVisible:Entities.EcCult[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
            //bind entity child collection with child controller merged items
            Entities.EcCult.ecCultDetailCollection = (ecCult, func) => {
                this.model.modelGrpEcCultDetail.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, ecCult);
            }

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
                //unbind entity child collection with child controller merged items
                Entities.EcCult.ecCultDetailCollection = null;//(ecCult, func) => { }
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpEcCult";
        }
        public get HtmlDivId(): string {
            return "EcGroup_ControllerGrpEcCult";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new LovItem (
                        (ent?: Entities.EcCult) => 'Crop',
                        false ,
                        (ent?: Entities.EcCult) => ent.cultId,  
                        (ent?: Entities.EcCult) => ent._cultId,  
                        (ent?: Entities.EcCult) => false, //isRequired
                        (vl: Entities.Cultivation, ent?: Entities.EcCult) => new NpTypes.ValidationResult(true, ""))
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Cultivation Criteria";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridClone\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);cloneEntity(row.entity)\" data-ng-disabled=\"newIsDisabled()\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined},
                { cellClass:'cellToolTip', field:'cultId.name', displayName:'getALString("Crop", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'14%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <div data-on-key-down='!GrpEcCult_itm__cultId_disabled(row.entity) && showLov_GrpEcCult_itm__cultId(row.entity)' data-keys='[123]' > \
                    <input data-np-source='row.entity.cultId.name' data-np-ui-model='row.entity._cultId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='GrpEcCult_itm__cultId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_GrpEcCult_itm__cultId(row.entity)'   data-ng-disabled=\"GrpEcCult_itm__cultId_disabled(row.entity)\"   />  \
                    </div> \
                </div>"},
                { cellClass:'cellToolTip', field:'cultId.code', displayName:'getALString("Crop Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'9%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCult_itm__cultId_code' data-ng-model='row.entity.cultId.code' data-np-ui-model='row.entity.cultId._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultId.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep='.' data-np-thSep='' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'cultId.cotyId.name', displayName:'getALString("Land Cover", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'9%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCult_itm__cultId_cotyId_name' data-ng-model='row.entity.cultId.cotyId.name' data-np-ui-model='row.entity.cultId.cotyId._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultId.cotyId.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpEcCult():ModelGrpEcCult {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "EcCult";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.EcGroup): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.EcCult.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.ecgrId = this.Parent;
                } else {
                    x.ecgrId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.EcCult {
            var self = this;
            return <Entities.EcCult>self.$scope.modelGrpEcCult.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.EcCult, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpEcGroup.controller.isEntityLocked(<Entities.EcGroup>cur.ecgrId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCult/findAllByCriteriaRange_EcGroupGrpEcCult_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCult/findAllByCriteriaRange_EcGroupGrpEcCult";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCult/findAllByCriteriaRange_EcGroupGrpEcCult_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.EcCult[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.EcCult[]) => void> = [];


        public getMergedItems(func: (items: Entities.EcCult[]) => void, bContinuousCaller:boolean=true, parent?: Entities.EcGroup, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.EcCult[]):void {
                var mergedEntities = <Entities.EcCult[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.EcCult>e.a).ecgrId)).
                        map(e => <Entities.EcCult>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "EcCult/findAllByCriteriaRange_EcGroupGrpEcCult";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['ecgrId_ecgrId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.EcCult[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.EcCult):boolean {
            if (this.Parent === undefined || x.ecgrId === undefined)
                return false;
            return x.ecgrId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.EcGroup {
            var self = this;
            return self.ecgrId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpEcGroup.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get ecgrId():Entities.EcGroup {
            var self = this;
            return <Entities.EcGroup>self.$scope.modelGrpEcGroup.selectedEntities[0];
        }









        public constructEntity(): Entities.EcCult {
            var self = this;
            var ret = new Entities.EcCult(
                /*eccuId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*noneMatchDecision:number*/ null,
                /*rowVersion:number*/ null,
                /*cultId:Entities.Cultivation*/ null,
                /*ecgrId:Entities.EcGroup*/ null,
                /*cotyId:Entities.CoverType*/ null,
                /*sucaId:Entities.SuperClas*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.EcCult = <Entities.EcCult>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.ecgrId = self.ecgrId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.EcCult): Entities.EcCult {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.EcCult, calledByParent: boolean= false): Entities.EcCult {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.EcCult.Create();
            ret.updateInstance(src);
            ret.eccuId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
                this.modelGrpEcCult.modelGrpEcCultDetail.controller.cloneAllEntitiesUnderParent(src, ret);
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.EcGroup, newParent:Entities.EcGroup) {
        
            this.model.totalItems = 0;

            this.getMergedItems(ecCultList => {
                ecCultList.forEach(ecCult => {
                    var ecCultCloned = this.cloneEntity_internal(ecCult, true);
                    ecCultCloned.ecgrId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(ecGroup: Entities.EcGroup) {
        

            var self = this;
            var toBeDeleted:Array<Entities.EcCult> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var ecCult = <Entities.EcCult>change.a
                if (ecGroup.getKey() === ecCult.ecgrId.getKey()) {
                    toBeDeleted.push(ecCult);
                }
            }

            _.each(toBeDeleted, (x:Entities.EcCult) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
                self.modelGrpEcCult.modelGrpEcCultDetail.controller.deleteNewEntitiesUnderParent(x);
            });

        }

        public deleteAllEntitiesUnderParent(ecGroup: Entities.EcGroup, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteEcCultList(ecCultList: Entities.EcCult[]) {
                if (ecCultList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = ecCultList[0];
                    var tail = ecCultList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteEcCultList(tail);
                    });
                }
            }


            this.getMergedItems(ecCultList => {
                deleteEcCultList(ecCultList);
            }, false, ecGroup);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                    self.modelGrpEcCult.modelGrpEcCultDetail.controller
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.EcCult, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                self.modelGrpEcCult.modelGrpEcCultDetail.controller.deleteAllEntitiesUnderParent(ent, () => {
                    super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                });
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    self.modelGrpEcCult.modelGrpEcCultDetail.controller.deleteNewEntitiesUnderParent(ent);
                    afterDeleteAction();
                });
            }
        }


        public GrpEcCult_itm__cultId_disabled(ecCult:Entities.EcCult):boolean {
            var self = this;
            if (ecCult === undefined || ecCult === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCult, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_GrpEcCult_itm__cultId:ModelLovCultivationBase;
        public showLov_GrpEcCult_itm__cultId(ecCult:Entities.EcCult) {
            var self = this;
            if (ecCult === undefined)
                return;
            var uimodel:NpTypes.IUIModel = ecCult._cultId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Cultivation';
            dialogOptions.previousModel= self.cachedLovModel_GrpEcCult_itm__cultId;
            dialogOptions.className = "ControllerLovCultivation";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var cultivation1:Entities.Cultivation =   <Entities.Cultivation>selectedEntity;
                uimodel.clearAllErrors();
                if(false && isVoid(cultivation1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(cultivation1) && cultivation1.isEqual(ecCult.cultId))
                    return;
                ecCult.cultId = cultivation1;
                self.markEntityAsUpdated(ecCult, 'GrpEcCult_itm__cultId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovCultivationBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpEcCult_itm__cultId = lovModel;
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovCultivationBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['code'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/Cultivation.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController.GrpEcGroup_0_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController.GrpEcGroup_0_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            var self = this;
            if (self.Parent === null || self.Parent === undefined)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege

            

            var parEntityIsLocked   = self.ParentController.isEntityLocked(self.Parent, LockKind.UpdateLock);
            if (parEntityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public _deleteIsDisabled(ecCult:Entities.EcCult):boolean  {
            var self = this;
            if (ecCult === null || ecCult === undefined || ecCult.getEntityName() !== "EcCult")
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_D"))
                return true; // no delete privilege;




            var entityIsLocked   = self.isEntityLocked(ecCult, LockKind.DeleteLock);
            if (entityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public child_group_GrpEcCultDetail_isInvisible():boolean {
            var self = this;
            if ((self.model.modelGrpEcCultDetail === undefined) || (self.model.modelGrpEcCultDetail.controller === undefined))
                return false;
            return self.model.modelGrpEcCultDetail.controller._isInvisible()
        }

    }


    // GROUP GrpEcCultDetail

    export class ModelGrpEcCultDetailBase extends Controllers.AbstractGroupTableModel {
        controller: ControllerGrpEcCultDetail;
        public get eccdId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).eccdId;
        }

        public set eccdId(eccdId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).eccdId = eccdId_newVal;
        }

        public get _eccdId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._eccdId;
        }

        public get orderingNumber():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).orderingNumber;
        }

        public set orderingNumber(orderingNumber_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).orderingNumber = orderingNumber_newVal;
        }

        public get _orderingNumber():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._orderingNumber;
        }

        public get agreesDeclar():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).agreesDeclar;
        }

        public set agreesDeclar(agreesDeclar_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).agreesDeclar = agreesDeclar_newVal;
        }

        public get _agreesDeclar():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._agreesDeclar;
        }

        public get comparisonOper():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).comparisonOper;
        }

        public set comparisonOper(comparisonOper_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).comparisonOper = comparisonOper_newVal;
        }

        public get _comparisonOper():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._comparisonOper;
        }

        public get probabThres():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).probabThres;
        }

        public set probabThres(probabThres_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).probabThres = probabThres_newVal;
        }

        public get _probabThres():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probabThres;
        }

        public get decisionLight():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).decisionLight;
        }

        public set decisionLight(decisionLight_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).decisionLight = decisionLight_newVal;
        }

        public get _decisionLight():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._decisionLight;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get agreesDeclar2():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).agreesDeclar2;
        }

        public set agreesDeclar2(agreesDeclar2_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).agreesDeclar2 = agreesDeclar2_newVal;
        }

        public get _agreesDeclar2():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._agreesDeclar2;
        }

        public get comparisonOper2():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).comparisonOper2;
        }

        public set comparisonOper2(comparisonOper2_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).comparisonOper2 = comparisonOper2_newVal;
        }

        public get _comparisonOper2():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._comparisonOper2;
        }

        public get probabThres2():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).probabThres2;
        }

        public set probabThres2(probabThres2_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).probabThres2 = probabThres2_newVal;
        }

        public get _probabThres2():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probabThres2;
        }

        public get probabThresSum():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).probabThresSum;
        }

        public set probabThresSum(probabThresSum_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).probabThresSum = probabThresSum_newVal;
        }

        public get _probabThresSum():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probabThresSum;
        }

        public get comparisonOper3():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).comparisonOper3;
        }

        public set comparisonOper3(comparisonOper3_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).comparisonOper3 = comparisonOper3_newVal;
        }

        public get _comparisonOper3():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._comparisonOper3;
        }

        public get eccuId():Entities.EcCult {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).eccuId;
        }

        public set eccuId(eccuId_newVal:Entities.EcCult) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).eccuId = eccuId_newVal;
        }

        public get _eccuId():NpTypes.UIManyToOneModel<Entities.EcCult> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._eccuId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpEcCultDetail) { super($scope); }
    }



    export interface IScopeGrpEcCultDetailBase extends Controllers.IAbstractTableGroupScope, IScopeGrpEcCultBase{
        globals: Globals;
        modelGrpEcCultDetail : ModelGrpEcCultDetail;
        GrpEcCultDetail_itm__orderingNumber_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        GrpEcCultDetail_itm__agreesDeclar_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        GrpEcCultDetail_itm__comparisonOper_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        GrpEcCultDetail_itm__probabThres_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        GrpEcCultDetail_itm__agreesDeclar2_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        GrpEcCultDetail_itm__comparisonOper2_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        GrpEcCultDetail_itm__probabThres2_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        GrpEcCultDetail_itm__comparisonOper3_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        GrpEcCultDetail_itm__probabThresSum_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        GrpEcCultDetail_itm__decisionLight_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 

    }



    export class ControllerGrpEcCultDetailBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpEcCultDetail,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpEcCultDetail)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 3});
            var self:ControllerGrpEcCultDetailBase = this;
            model.controller = <ControllerGrpEcCultDetail>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpEcCultDetail = self.model;

            $scope.GrpEcCultDetail_itm__orderingNumber_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.GrpEcCultDetail_itm__orderingNumber_disabled(ecCultDetail);
                };
            $scope.GrpEcCultDetail_itm__agreesDeclar_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.GrpEcCultDetail_itm__agreesDeclar_disabled(ecCultDetail);
                };
            $scope.GrpEcCultDetail_itm__comparisonOper_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.GrpEcCultDetail_itm__comparisonOper_disabled(ecCultDetail);
                };
            $scope.GrpEcCultDetail_itm__probabThres_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.GrpEcCultDetail_itm__probabThres_disabled(ecCultDetail);
                };
            $scope.GrpEcCultDetail_itm__agreesDeclar2_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.GrpEcCultDetail_itm__agreesDeclar2_disabled(ecCultDetail);
                };
            $scope.GrpEcCultDetail_itm__comparisonOper2_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.GrpEcCultDetail_itm__comparisonOper2_disabled(ecCultDetail);
                };
            $scope.GrpEcCultDetail_itm__probabThres2_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.GrpEcCultDetail_itm__probabThres2_disabled(ecCultDetail);
                };
            $scope.GrpEcCultDetail_itm__comparisonOper3_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.GrpEcCultDetail_itm__comparisonOper3_disabled(ecCultDetail);
                };
            $scope.GrpEcCultDetail_itm__probabThresSum_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.GrpEcCultDetail_itm__probabThresSum_disabled(ecCultDetail);
                };
            $scope.GrpEcCultDetail_itm__decisionLight_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.GrpEcCultDetail_itm__decisionLight_disabled(ecCultDetail);
                };


            $scope.pageModel.modelGrpEcCultDetail = $scope.modelGrpEcCultDetail;


            $scope.clearBtnAction = () => { 
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpEcCult.modelGrpEcCultDetail = $scope.modelGrpEcCultDetail;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpEcCult.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCultDetail[] , oldVisible:Entities.EcCultDetail[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpEcCultDetail";
        }
        public get HtmlDivId(): string {
            return "EcGroup_ControllerGrpEcCultDetail";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new NumberItem (
                        (ent?: Entities.EcCultDetail) => 'Evaluation \n Order',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.orderingNumber,  
                        (ent?: Entities.EcCultDetail) => ent._orderingNumber,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined,
                        0),
                    new StaticListItem<number> (
                        (ent?: Entities.EcCultDetail) => '1st \ Prediction ',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.agreesDeclar,  
                        (ent?: Entities.EcCultDetail) => ent._agreesDeclar,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, "")),
                    new StaticListItem<number> (
                        (ent?: Entities.EcCultDetail) => 'Comparison \n Operator 1st',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.comparisonOper,  
                        (ent?: Entities.EcCultDetail) => ent._comparisonOper,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, "")),
                    new NumberItem (
                        (ent?: Entities.EcCultDetail) => 'Confidence \n Pred. 1st (%)',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.probabThres,  
                        (ent?: Entities.EcCultDetail) => ent._probabThres,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined,
                        2),
                    new StaticListItem<number> (
                        (ent?: Entities.EcCultDetail) => '2nd \ Prediction ',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.agreesDeclar2,  
                        (ent?: Entities.EcCultDetail) => ent._agreesDeclar2,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, "")),
                    new StaticListItem<number> (
                        (ent?: Entities.EcCultDetail) => 'Comparison \n Operator 2nd',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.comparisonOper2,  
                        (ent?: Entities.EcCultDetail) => ent._comparisonOper2,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, "")),
                    new NumberItem (
                        (ent?: Entities.EcCultDetail) => 'Confidence \n Pred. 2nd (%)',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.probabThres2,  
                        (ent?: Entities.EcCultDetail) => ent._probabThres2,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined,
                        2),
                    new StaticListItem<number> (
                        (ent?: Entities.EcCultDetail) => 'Comparison \n Operator 1st + 2nd',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.comparisonOper3,  
                        (ent?: Entities.EcCultDetail) => ent._comparisonOper3,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, "")),
                    new NumberItem (
                        (ent?: Entities.EcCultDetail) => 'Confidence \n Prediction \n 1st + 2nd (%)',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.probabThresSum,  
                        (ent?: Entities.EcCultDetail) => ent._probabThresSum,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined,
                        2),
                    new StaticListItem<number> (
                        (ent?: Entities.EcCultDetail) => 'Traffic \n Light Code',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.decisionLight,  
                        (ent?: Entities.EcCultDetail) => ent._decisionLight,  
                        (ent?: Entities.EcCultDetail) => true, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, ""))
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Criteria";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'orderingNumber', displayName:'getALString("Evaluation \n Order", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'5%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCultDetail_itm__orderingNumber' data-ng-model='row.entity.orderingNumber' data-np-ui-model='row.entity._orderingNumber' data-ng-change='markEntityAsUpdated(row.entity,&quot;orderingNumber&quot;)' data-ng-readonly='GrpEcCultDetail_itm__orderingNumber_disabled(row.entity)' data-np-number='dummy' data-np-decimals='0' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'agreesDeclar', displayName:'getALString("1st \ Prediction ", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetail_itm__agreesDeclar' data-ng-model='row.entity.agreesDeclar' data-np-ui-model='row.entity._agreesDeclar' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='GrpEcCultDetail_itm__agreesDeclar_disabled(row.entity)' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'comparisonOper', displayName:'getALString("Comparison \n Operator 1st", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'11%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetail_itm__comparisonOper' data-ng-model='row.entity.comparisonOper' data-np-ui-model='row.entity._comparisonOper' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='GrpEcCultDetail_itm__comparisonOper_disabled(row.entity)' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'probabThres', displayName:'getALString("Confidence \n Pred. 1st (%)", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCultDetail_itm__probabThres' data-ng-model='row.entity.probabThres' data-np-ui-model='row.entity._probabThres' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres&quot;)' data-ng-readonly='GrpEcCultDetail_itm__probabThres_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'agreesDeclar2', displayName:'getALString("2nd \ Prediction ", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetail_itm__agreesDeclar2' data-ng-model='row.entity.agreesDeclar2' data-np-ui-model='row.entity._agreesDeclar2' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='GrpEcCultDetail_itm__agreesDeclar2_disabled(row.entity)' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'comparisonOper2', displayName:'getALString("Comparison \n Operator 2nd", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'11%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetail_itm__comparisonOper2' data-ng-model='row.entity.comparisonOper2' data-np-ui-model='row.entity._comparisonOper2' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='GrpEcCultDetail_itm__comparisonOper2_disabled(row.entity)' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'probabThres2', displayName:'getALString("Confidence \n Pred. 2nd (%)", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCultDetail_itm__probabThres2' data-ng-model='row.entity.probabThres2' data-np-ui-model='row.entity._probabThres2' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres2&quot;)' data-ng-readonly='GrpEcCultDetail_itm__probabThres2_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'comparisonOper3', displayName:'getALString("Comparison \n Operator 1st + 2nd", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'11%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetail_itm__comparisonOper3' data-ng-model='row.entity.comparisonOper3' data-np-ui-model='row.entity._comparisonOper3' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper3&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='GrpEcCultDetail_itm__comparisonOper3_disabled(row.entity)' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'probabThresSum', displayName:'getALString("Confidence \n Prediction \n 1st + 2nd (%)", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCultDetail_itm__probabThresSum' data-ng-model='row.entity.probabThresSum' data-np-ui-model='row.entity._probabThresSum' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThresSum&quot;)' data-ng-readonly='GrpEcCultDetail_itm__probabThresSum_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'decisionLight', displayName:'getALString("Traffic \n Light Code", true)', requiredAsterisk:self.$scope.globals.hasPrivilege("Niva_EcGroup_W"), resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetail_itm__decisionLight' data-ng-model='row.entity.decisionLight' data-np-ui-model='row.entity._decisionLight' data-ng-change='markEntityAsUpdated(row.entity,&quot;decisionLight&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Green\"), value:1}, {label:getALString(\"Yellow\"), value:2}, {label:getALString(\"Red\"), value:3}, {label:getALString(\"Undefined\"), value:4}]' data-ng-disabled='GrpEcCultDetail_itm__decisionLight_disabled(row.entity)' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpEcCultDetail():ModelGrpEcCultDetail {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "EcCultDetail";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.EcCult): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.EcCultDetail.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.eccuId = this.Parent;
                } else {
                    x.eccuId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.EcCultDetail {
            var self = this;
            return <Entities.EcCultDetail>self.$scope.modelGrpEcCultDetail.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.EcCultDetail, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpEcCult.controller.isEntityLocked(<Entities.EcCult>cur.eccuId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupGrpEcCultDetail_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupGrpEcCultDetail";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupGrpEcCultDetail_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.EcCultDetail[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.EcCultDetail[]) => void> = [];


        public getMergedItems(func: (items: Entities.EcCultDetail[]) => void, bContinuousCaller:boolean=true, parent?: Entities.EcCult, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.EcCultDetail[]):void {
                var mergedEntities = <Entities.EcCultDetail[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.EcCultDetail>e.a).eccuId)).
                        map(e => <Entities.EcCultDetail>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupGrpEcCultDetail";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['eccuId_eccuId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.EcCultDetail[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.EcCultDetail):boolean {
            if (this.Parent === undefined || x.eccuId === undefined)
                return false;
            return x.eccuId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.EcCult {
            var self = this;
            return self.eccuId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpEcCult.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get eccuId():Entities.EcCult {
            var self = this;
            return <Entities.EcCult>self.$scope.modelGrpEcCult.selectedEntities[0];
        }









        public constructEntity(): Entities.EcCultDetail {
            var self = this;
            var ret = new Entities.EcCultDetail(
                /*eccdId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*orderingNumber:number*/ null,
                /*agreesDeclar:number*/ null,
                /*comparisonOper:number*/ null,
                /*probabThres:number*/ null,
                /*decisionLight:number*/ null,
                /*rowVersion:number*/ null,
                /*agreesDeclar2:number*/ null,
                /*comparisonOper2:number*/ null,
                /*probabThres2:number*/ null,
                /*probabThresSum:number*/ null,
                /*comparisonOper3:number*/ null,
                /*eccuId:Entities.EcCult*/ null);
            this.getMergedItems(items => {
                ret.orderingNumber = items.maxBy(i => i.orderingNumber, 0) + 1;
            }, false);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.EcCultDetail = <Entities.EcCultDetail>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.eccuId = self.eccuId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.EcCultDetail): Entities.EcCultDetail {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.EcCultDetail, calledByParent: boolean= false): Entities.EcCultDetail {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.EcCultDetail.Create();
            ret.updateInstance(src);
            ret.eccdId = tmpId;
            if (!calledByParent) {
                this.getMergedItems(items => {
                    ret.orderingNumber = items.maxBy(i => i.orderingNumber, 0) + 1;
                }, false);
            }
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.EcCult, newParent:Entities.EcCult) {
        
            this.model.totalItems = 0;

            this.getMergedItems(ecCultDetailList => {
                ecCultDetailList.forEach(ecCultDetail => {
                    var ecCultDetailCloned = this.cloneEntity_internal(ecCultDetail, true);
                    ecCultDetailCloned.eccuId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(ecCult: Entities.EcCult) {
        

            var self = this;
            var toBeDeleted:Array<Entities.EcCultDetail> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var ecCultDetail = <Entities.EcCultDetail>change.a
                if (ecCult.getKey() === ecCultDetail.eccuId.getKey()) {
                    toBeDeleted.push(ecCultDetail);
                }
            }

            _.each(toBeDeleted, (x:Entities.EcCultDetail) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
            });

        }

        public deleteAllEntitiesUnderParent(ecCult: Entities.EcCult, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteEcCultDetailList(ecCultDetailList: Entities.EcCultDetail[]) {
                if (ecCultDetailList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = ecCultDetailList[0];
                    var tail = ecCultDetailList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteEcCultDetailList(tail);
                    });
                }
            }


            this.getMergedItems(ecCultDetailList => {
                deleteEcCultDetailList(ecCultDetailList);
            }, false, ecCult);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.EcCultDetail, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        public GrpEcCultDetail_itm__orderingNumber_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpEcCultDetail_itm__agreesDeclar_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpEcCultDetail_itm__comparisonOper_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpEcCultDetail_itm__probabThres_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpEcCultDetail_itm__agreesDeclar2_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpEcCultDetail_itm__comparisonOper2_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpEcCultDetail_itm__probabThres2_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpEcCultDetail_itm__comparisonOper3_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpEcCultDetail_itm__probabThresSum_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpEcCultDetail_itm__decisionLight_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            var self = this;
            if (self.Parent === null || self.Parent === undefined)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege

            

            var parEntityIsLocked   = self.ParentController.isEntityLocked(self.Parent, LockKind.UpdateLock);
            if (parEntityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public _deleteIsDisabled(ecCultDetail:Entities.EcCultDetail):boolean  {
            var self = this;
            if (ecCultDetail === null || ecCultDetail === undefined || ecCultDetail.getEntityName() !== "EcCultDetail")
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_D"))
                return true; // no delete privilege;




            var entityIsLocked   = self.isEntityLocked(ecCultDetail, LockKind.DeleteLock);
            if (entityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }


    }


    // GROUP EcCultCover

    export class ModelEcCultCoverBase extends Controllers.AbstractGroupTableModel {
        modelEcCultDetailCover:ModelEcCultDetailCover;
        controller: ControllerEcCultCover;
        public get eccuId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCult>this.selectedEntities[0]).eccuId;
        }

        public set eccuId(eccuId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCult>this.selectedEntities[0]).eccuId = eccuId_newVal;
        }

        public get _eccuId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._eccuId;
        }

        public get noneMatchDecision():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCult>this.selectedEntities[0]).noneMatchDecision;
        }

        public set noneMatchDecision(noneMatchDecision_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCult>this.selectedEntities[0]).noneMatchDecision = noneMatchDecision_newVal;
        }

        public get _noneMatchDecision():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._noneMatchDecision;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCult>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCult>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get cultId():Entities.Cultivation {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCult>this.selectedEntities[0]).cultId;
        }

        public set cultId(cultId_newVal:Entities.Cultivation) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCult>this.selectedEntities[0]).cultId = cultId_newVal;
        }

        public get _cultId():NpTypes.UIManyToOneModel<Entities.Cultivation> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cultId;
        }

        public get ecgrId():Entities.EcGroup {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCult>this.selectedEntities[0]).ecgrId;
        }

        public set ecgrId(ecgrId_newVal:Entities.EcGroup) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCult>this.selectedEntities[0]).ecgrId = ecgrId_newVal;
        }

        public get _ecgrId():NpTypes.UIManyToOneModel<Entities.EcGroup> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._ecgrId;
        }

        public get cotyId():Entities.CoverType {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCult>this.selectedEntities[0]).cotyId;
        }

        public set cotyId(cotyId_newVal:Entities.CoverType) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCult>this.selectedEntities[0]).cotyId = cotyId_newVal;
        }

        public get _cotyId():NpTypes.UIManyToOneModel<Entities.CoverType> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cotyId;
        }

        public get sucaId():Entities.SuperClas {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCult>this.selectedEntities[0]).sucaId;
        }

        public set sucaId(sucaId_newVal:Entities.SuperClas) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCult>this.selectedEntities[0]).sucaId = sucaId_newVal;
        }

        public get _sucaId():NpTypes.UIManyToOneModel<Entities.SuperClas> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._sucaId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeEcCultCover) { super($scope); }
    }



    export interface IScopeEcCultCoverBase extends Controllers.IAbstractTableGroupScope, IScopeGrpEcGroupBase{
        globals: Globals;
        modelEcCultCover : ModelEcCultCover;
        EcCultCover_itm__cotyId_disabled(ecCult:Entities.EcCult):boolean; 
        showLov_EcCultCover_itm__cotyId(ecCult:Entities.EcCult):void; 
        child_group_EcCultDetailCover_isInvisible():boolean; 

    }



    export class ControllerEcCultCoverBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeEcCultCover,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelEcCultCover)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 1});
            var self:ControllerEcCultCoverBase = this;
            model.controller = <ControllerEcCultCover>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelEcCultCover = self.model;

            $scope.EcCultCover_itm__cotyId_disabled = 
                (ecCult:Entities.EcCult) => {
                    return self.EcCultCover_itm__cotyId_disabled(ecCult);
                };
            $scope.showLov_EcCultCover_itm__cotyId = 
                (ecCult:Entities.EcCult) => {
                    $timeout( () => {        
                        self.showLov_EcCultCover_itm__cotyId(ecCult);
                    }, 0);
                };
            $scope.child_group_EcCultDetailCover_isInvisible = 
                () => {
                    return self.child_group_EcCultDetailCover_isInvisible();
                };


            $scope.pageModel.modelEcCultCover = $scope.modelEcCultCover;


            $scope.clearBtnAction = () => { 
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpEcGroup.modelEcCultCover = $scope.modelEcCultCover;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpEcGroup.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCult[] , oldVisible:Entities.EcCult[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
            //bind entity child collection with child controller merged items
            Entities.EcCult.ecCultDetailCollection = (ecCult, func) => {
                this.model.modelEcCultDetailCover.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, ecCult);
            }

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
                //unbind entity child collection with child controller merged items
                Entities.EcCult.ecCultDetailCollection = null;//(ecCult, func) => { }
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerEcCultCover";
        }
        public get HtmlDivId(): string {
            return "EcGroup_ControllerEcCultCover";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new LovItem (
                        (ent?: Entities.EcCult) => 'Land Cover',
                        false ,
                        (ent?: Entities.EcCult) => ent.cotyId,  
                        (ent?: Entities.EcCult) => ent._cotyId,  
                        (ent?: Entities.EcCult) => false, //isRequired
                        (vl: Entities.CoverType, ent?: Entities.EcCult) => new NpTypes.ValidationResult(true, ""))
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Land Cover Criteria";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridClone\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);cloneEntity(row.entity)\" data-ng-disabled=\"newIsDisabled()\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined},
                { cellClass:'cellToolTip', field:'cotyId.name', displayName:'getALString("Land Cover", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'23%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <div data-on-key-down='!EcCultCover_itm__cotyId_disabled(row.entity) && showLov_EcCultCover_itm__cotyId(row.entity)' data-keys='[123]' > \
                    <input data-np-source='row.entity.cotyId.name' data-np-ui-model='row.entity._cotyId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='EcCultCover_itm__cotyId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_EcCultCover_itm__cotyId(row.entity)'   data-ng-disabled=\"EcCultCover_itm__cotyId_disabled(row.entity)\"   />  \
                    </div> \
                </div>"},
                { cellClass:'cellToolTip', field:'cotyId.code', displayName:'getALString("Land Cover Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultCover_itm__cotyId_code' data-ng-model='row.entity.cotyId.code' data-np-ui-model='row.entity.cotyId._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cotyId.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='0' data-np-decSep='.' data-np-thSep='' class='ngCellNumber' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelEcCultCover():ModelEcCultCover {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "EcCult";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.EcGroup): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.EcCult.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.ecgrId = this.Parent;
                } else {
                    x.ecgrId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.EcCult {
            var self = this;
            return <Entities.EcCult>self.$scope.modelEcCultCover.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.EcCult, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpEcGroup.controller.isEntityLocked(<Entities.EcGroup>cur.ecgrId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCult/findAllByCriteriaRange_EcGroupEcCultCover_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCult/findAllByCriteriaRange_EcGroupEcCultCover";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCult/findAllByCriteriaRange_EcGroupEcCultCover_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.EcCult[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.EcCult[]) => void> = [];


        public getMergedItems(func: (items: Entities.EcCult[]) => void, bContinuousCaller:boolean=true, parent?: Entities.EcGroup, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.EcCult[]):void {
                var mergedEntities = <Entities.EcCult[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.EcCult>e.a).ecgrId)).
                        map(e => <Entities.EcCult>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "EcCult/findAllByCriteriaRange_EcGroupEcCultCover";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['ecgrId_ecgrId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.EcCult[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.EcCult):boolean {
            if (this.Parent === undefined || x.ecgrId === undefined)
                return false;
            return x.ecgrId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.EcGroup {
            var self = this;
            return self.ecgrId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpEcGroup.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get ecgrId():Entities.EcGroup {
            var self = this;
            return <Entities.EcGroup>self.$scope.modelGrpEcGroup.selectedEntities[0];
        }









        public constructEntity(): Entities.EcCult {
            var self = this;
            var ret = new Entities.EcCult(
                /*eccuId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*noneMatchDecision:number*/ null,
                /*rowVersion:number*/ null,
                /*cultId:Entities.Cultivation*/ null,
                /*ecgrId:Entities.EcGroup*/ null,
                /*cotyId:Entities.CoverType*/ null,
                /*sucaId:Entities.SuperClas*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.EcCult = <Entities.EcCult>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.ecgrId = self.ecgrId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.EcCult): Entities.EcCult {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.EcCult, calledByParent: boolean= false): Entities.EcCult {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.EcCult.Create();
            ret.updateInstance(src);
            ret.eccuId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
                this.modelEcCultCover.modelEcCultDetailCover.controller.cloneAllEntitiesUnderParent(src, ret);
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.EcGroup, newParent:Entities.EcGroup) {
        
            this.model.totalItems = 0;

            this.getMergedItems(ecCultList => {
                ecCultList.forEach(ecCult => {
                    var ecCultCloned = this.cloneEntity_internal(ecCult, true);
                    ecCultCloned.ecgrId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(ecGroup: Entities.EcGroup) {
        

            var self = this;
            var toBeDeleted:Array<Entities.EcCult> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var ecCult = <Entities.EcCult>change.a
                if (ecGroup.getKey() === ecCult.ecgrId.getKey()) {
                    toBeDeleted.push(ecCult);
                }
            }

            _.each(toBeDeleted, (x:Entities.EcCult) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
                self.modelEcCultCover.modelEcCultDetailCover.controller.deleteNewEntitiesUnderParent(x);
            });

        }

        public deleteAllEntitiesUnderParent(ecGroup: Entities.EcGroup, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteEcCultList(ecCultList: Entities.EcCult[]) {
                if (ecCultList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = ecCultList[0];
                    var tail = ecCultList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteEcCultList(tail);
                    });
                }
            }


            this.getMergedItems(ecCultList => {
                deleteEcCultList(ecCultList);
            }, false, ecGroup);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                    self.modelEcCultCover.modelEcCultDetailCover.controller
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.EcCult, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                self.modelEcCultCover.modelEcCultDetailCover.controller.deleteAllEntitiesUnderParent(ent, () => {
                    super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                });
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    self.modelEcCultCover.modelEcCultDetailCover.controller.deleteNewEntitiesUnderParent(ent);
                    afterDeleteAction();
                });
            }
        }


        public EcCultCover_itm__cotyId_disabled(ecCult:Entities.EcCult):boolean {
            var self = this;
            if (ecCult === undefined || ecCult === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCult, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_EcCultCover_itm__cotyId:ModelLovCoverTypeBase;
        public showLov_EcCultCover_itm__cotyId(ecCult:Entities.EcCult) {
            var self = this;
            if (ecCult === undefined)
                return;
            var uimodel:NpTypes.IUIModel = ecCult._cotyId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Land Cover';
            dialogOptions.previousModel= self.cachedLovModel_EcCultCover_itm__cotyId;
            dialogOptions.className = "ControllerLovCoverType";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var coverType1:Entities.CoverType =   <Entities.CoverType>selectedEntity;
                uimodel.clearAllErrors();
                if(false && isVoid(coverType1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(coverType1) && coverType1.isEqual(ecCult.cotyId))
                    return;
                ecCult.cotyId = coverType1;
                self.markEntityAsUpdated(ecCult, 'EcCultCover_itm__cotyId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovCoverTypeBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_EcCultCover_itm__cotyId = lovModel;
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovCoverTypeBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['code'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/CoverType.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController.GrpEcGroup_1_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController.GrpEcGroup_1_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            var self = this;
            if (self.Parent === null || self.Parent === undefined)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege

            

            var parEntityIsLocked   = self.ParentController.isEntityLocked(self.Parent, LockKind.UpdateLock);
            if (parEntityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public _deleteIsDisabled(ecCult:Entities.EcCult):boolean  {
            var self = this;
            if (ecCult === null || ecCult === undefined || ecCult.getEntityName() !== "EcCult")
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_D"))
                return true; // no delete privilege;




            var entityIsLocked   = self.isEntityLocked(ecCult, LockKind.DeleteLock);
            if (entityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public child_group_EcCultDetailCover_isInvisible():boolean {
            var self = this;
            if ((self.model.modelEcCultDetailCover === undefined) || (self.model.modelEcCultDetailCover.controller === undefined))
                return false;
            return self.model.modelEcCultDetailCover.controller._isInvisible()
        }

    }


    // GROUP EcCultDetailCover

    export class ModelEcCultDetailCoverBase extends Controllers.AbstractGroupTableModel {
        controller: ControllerEcCultDetailCover;
        public get eccdId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).eccdId;
        }

        public set eccdId(eccdId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).eccdId = eccdId_newVal;
        }

        public get _eccdId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._eccdId;
        }

        public get orderingNumber():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).orderingNumber;
        }

        public set orderingNumber(orderingNumber_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).orderingNumber = orderingNumber_newVal;
        }

        public get _orderingNumber():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._orderingNumber;
        }

        public get agreesDeclar():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).agreesDeclar;
        }

        public set agreesDeclar(agreesDeclar_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).agreesDeclar = agreesDeclar_newVal;
        }

        public get _agreesDeclar():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._agreesDeclar;
        }

        public get comparisonOper():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).comparisonOper;
        }

        public set comparisonOper(comparisonOper_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).comparisonOper = comparisonOper_newVal;
        }

        public get _comparisonOper():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._comparisonOper;
        }

        public get probabThres():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).probabThres;
        }

        public set probabThres(probabThres_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).probabThres = probabThres_newVal;
        }

        public get _probabThres():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probabThres;
        }

        public get decisionLight():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).decisionLight;
        }

        public set decisionLight(decisionLight_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).decisionLight = decisionLight_newVal;
        }

        public get _decisionLight():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._decisionLight;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get agreesDeclar2():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).agreesDeclar2;
        }

        public set agreesDeclar2(agreesDeclar2_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).agreesDeclar2 = agreesDeclar2_newVal;
        }

        public get _agreesDeclar2():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._agreesDeclar2;
        }

        public get comparisonOper2():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).comparisonOper2;
        }

        public set comparisonOper2(comparisonOper2_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).comparisonOper2 = comparisonOper2_newVal;
        }

        public get _comparisonOper2():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._comparisonOper2;
        }

        public get probabThres2():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).probabThres2;
        }

        public set probabThres2(probabThres2_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).probabThres2 = probabThres2_newVal;
        }

        public get _probabThres2():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probabThres2;
        }

        public get probabThresSum():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).probabThresSum;
        }

        public set probabThresSum(probabThresSum_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).probabThresSum = probabThresSum_newVal;
        }

        public get _probabThresSum():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probabThresSum;
        }

        public get comparisonOper3():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).comparisonOper3;
        }

        public set comparisonOper3(comparisonOper3_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).comparisonOper3 = comparisonOper3_newVal;
        }

        public get _comparisonOper3():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._comparisonOper3;
        }

        public get eccuId():Entities.EcCult {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).eccuId;
        }

        public set eccuId(eccuId_newVal:Entities.EcCult) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).eccuId = eccuId_newVal;
        }

        public get _eccuId():NpTypes.UIManyToOneModel<Entities.EcCult> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._eccuId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeEcCultDetailCover) { super($scope); }
    }



    export interface IScopeEcCultDetailCoverBase extends Controllers.IAbstractTableGroupScope, IScopeEcCultCoverBase{
        globals: Globals;
        modelEcCultDetailCover : ModelEcCultDetailCover;
        EcCultDetailCover_itm__orderingNumber_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        EcCultDetailCover_itm__agreesDeclar_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        EcCultDetailCover_itm__comparisonOper_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        EcCultDetailCover_itm__probabThres_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        EcCultDetailCover_itm__agreesDeclar2_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        EcCultDetailCover_itm__comparisonOper2_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        EcCultDetailCover_itm__probabThres2_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        EcCultDetailCover_itm__comparisonOper3_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        EcCultDetailCover_itm__probabThresSum_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        EcCultDetailCover_itm__decisionLight_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 

    }



    export class ControllerEcCultDetailCoverBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeEcCultDetailCover,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelEcCultDetailCover)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 3});
            var self:ControllerEcCultDetailCoverBase = this;
            model.controller = <ControllerEcCultDetailCover>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelEcCultDetailCover = self.model;

            $scope.EcCultDetailCover_itm__orderingNumber_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.EcCultDetailCover_itm__orderingNumber_disabled(ecCultDetail);
                };
            $scope.EcCultDetailCover_itm__agreesDeclar_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.EcCultDetailCover_itm__agreesDeclar_disabled(ecCultDetail);
                };
            $scope.EcCultDetailCover_itm__comparisonOper_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.EcCultDetailCover_itm__comparisonOper_disabled(ecCultDetail);
                };
            $scope.EcCultDetailCover_itm__probabThres_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.EcCultDetailCover_itm__probabThres_disabled(ecCultDetail);
                };
            $scope.EcCultDetailCover_itm__agreesDeclar2_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.EcCultDetailCover_itm__agreesDeclar2_disabled(ecCultDetail);
                };
            $scope.EcCultDetailCover_itm__comparisonOper2_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.EcCultDetailCover_itm__comparisonOper2_disabled(ecCultDetail);
                };
            $scope.EcCultDetailCover_itm__probabThres2_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.EcCultDetailCover_itm__probabThres2_disabled(ecCultDetail);
                };
            $scope.EcCultDetailCover_itm__comparisonOper3_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.EcCultDetailCover_itm__comparisonOper3_disabled(ecCultDetail);
                };
            $scope.EcCultDetailCover_itm__probabThresSum_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.EcCultDetailCover_itm__probabThresSum_disabled(ecCultDetail);
                };
            $scope.EcCultDetailCover_itm__decisionLight_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.EcCultDetailCover_itm__decisionLight_disabled(ecCultDetail);
                };


            $scope.pageModel.modelEcCultDetailCover = $scope.modelEcCultDetailCover;


            $scope.clearBtnAction = () => { 
                self.updateGrid(0, false, true);
            };



            $scope.modelEcCultCover.modelEcCultDetailCover = $scope.modelEcCultDetailCover;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelEcCultCover.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCultDetail[] , oldVisible:Entities.EcCultDetail[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerEcCultDetailCover";
        }
        public get HtmlDivId(): string {
            return "EcGroup_ControllerEcCultDetailCover";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new NumberItem (
                        (ent?: Entities.EcCultDetail) => 'Evaluation \n Order',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.orderingNumber,  
                        (ent?: Entities.EcCultDetail) => ent._orderingNumber,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined,
                        0),
                    new StaticListItem<number> (
                        (ent?: Entities.EcCultDetail) => '1st \ Prediction ',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.agreesDeclar,  
                        (ent?: Entities.EcCultDetail) => ent._agreesDeclar,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, "")),
                    new StaticListItem<number> (
                        (ent?: Entities.EcCultDetail) => 'Comparison \n Operator 1st',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.comparisonOper,  
                        (ent?: Entities.EcCultDetail) => ent._comparisonOper,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, "")),
                    new NumberItem (
                        (ent?: Entities.EcCultDetail) => 'Confidence \n Pred. 1st (%)',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.probabThres,  
                        (ent?: Entities.EcCultDetail) => ent._probabThres,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined,
                        2),
                    new StaticListItem<number> (
                        (ent?: Entities.EcCultDetail) => '2nd \ Prediction ',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.agreesDeclar2,  
                        (ent?: Entities.EcCultDetail) => ent._agreesDeclar2,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, "")),
                    new StaticListItem<number> (
                        (ent?: Entities.EcCultDetail) => 'Comparison \n Operator 2nd',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.comparisonOper2,  
                        (ent?: Entities.EcCultDetail) => ent._comparisonOper2,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, "")),
                    new NumberItem (
                        (ent?: Entities.EcCultDetail) => 'Confidence \n Pred. 2nd (%)',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.probabThres2,  
                        (ent?: Entities.EcCultDetail) => ent._probabThres2,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined,
                        2),
                    new StaticListItem<number> (
                        (ent?: Entities.EcCultDetail) => 'Comparison \n Operator 1st + 2nd',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.comparisonOper3,  
                        (ent?: Entities.EcCultDetail) => ent._comparisonOper3,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, "")),
                    new NumberItem (
                        (ent?: Entities.EcCultDetail) => 'Confidence \n Prediction \n 1st + 2nd (%)',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.probabThresSum,  
                        (ent?: Entities.EcCultDetail) => ent._probabThresSum,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined,
                        2),
                    new StaticListItem<number> (
                        (ent?: Entities.EcCultDetail) => 'Traffic \n Light Code',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.decisionLight,  
                        (ent?: Entities.EcCultDetail) => ent._decisionLight,  
                        (ent?: Entities.EcCultDetail) => true, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, ""))
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Criteria";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'orderingNumber', displayName:'getALString("Evaluation \n Order", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'5%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCover_itm__orderingNumber' data-ng-model='row.entity.orderingNumber' data-np-ui-model='row.entity._orderingNumber' data-ng-change='markEntityAsUpdated(row.entity,&quot;orderingNumber&quot;)' data-ng-readonly='EcCultDetailCover_itm__orderingNumber_disabled(row.entity)' data-np-number='dummy' data-np-decimals='0' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'agreesDeclar', displayName:'getALString("1st \ Prediction ", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCover_itm__agreesDeclar' data-ng-model='row.entity.agreesDeclar' data-np-ui-model='row.entity._agreesDeclar' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='EcCultDetailCover_itm__agreesDeclar_disabled(row.entity)' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'comparisonOper', displayName:'getALString("Comparison \n Operator 1st", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'11%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCover_itm__comparisonOper' data-ng-model='row.entity.comparisonOper' data-np-ui-model='row.entity._comparisonOper' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='EcCultDetailCover_itm__comparisonOper_disabled(row.entity)' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'probabThres', displayName:'getALString("Confidence \n Pred. 1st (%)", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCover_itm__probabThres' data-ng-model='row.entity.probabThres' data-np-ui-model='row.entity._probabThres' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres&quot;)' data-ng-readonly='EcCultDetailCover_itm__probabThres_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'agreesDeclar2', displayName:'getALString("2nd \ Prediction ", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCover_itm__agreesDeclar2' data-ng-model='row.entity.agreesDeclar2' data-np-ui-model='row.entity._agreesDeclar2' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='EcCultDetailCover_itm__agreesDeclar2_disabled(row.entity)' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'comparisonOper2', displayName:'getALString("Comparison \n Operator 2nd", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'11%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCover_itm__comparisonOper2' data-ng-model='row.entity.comparisonOper2' data-np-ui-model='row.entity._comparisonOper2' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='EcCultDetailCover_itm__comparisonOper2_disabled(row.entity)' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'probabThres2', displayName:'getALString("Confidence \n Pred. 2nd (%)", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCover_itm__probabThres2' data-ng-model='row.entity.probabThres2' data-np-ui-model='row.entity._probabThres2' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres2&quot;)' data-ng-readonly='EcCultDetailCover_itm__probabThres2_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'comparisonOper3', displayName:'getALString("Comparison \n Operator 1st + 2nd", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'11%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCover_itm__comparisonOper3' data-ng-model='row.entity.comparisonOper3' data-np-ui-model='row.entity._comparisonOper3' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper3&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='EcCultDetailCover_itm__comparisonOper3_disabled(row.entity)' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'probabThresSum', displayName:'getALString("Confidence \n Prediction \n 1st + 2nd (%)", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCover_itm__probabThresSum' data-ng-model='row.entity.probabThresSum' data-np-ui-model='row.entity._probabThresSum' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThresSum&quot;)' data-ng-readonly='EcCultDetailCover_itm__probabThresSum_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'decisionLight', displayName:'getALString("Traffic \n Light Code", true)', requiredAsterisk:self.$scope.globals.hasPrivilege("Niva_EcGroup_W"), resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCover_itm__decisionLight' data-ng-model='row.entity.decisionLight' data-np-ui-model='row.entity._decisionLight' data-ng-change='markEntityAsUpdated(row.entity,&quot;decisionLight&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Green\"), value:1}, {label:getALString(\"Yellow\"), value:2}, {label:getALString(\"Red\"), value:3}, {label:getALString(\"Undefined\"), value:4}]' data-ng-disabled='EcCultDetailCover_itm__decisionLight_disabled(row.entity)' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelEcCultDetailCover():ModelEcCultDetailCover {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "EcCultDetail";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.EcCult): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.EcCultDetail.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.eccuId = this.Parent;
                } else {
                    x.eccuId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.EcCultDetail {
            var self = this;
            return <Entities.EcCultDetail>self.$scope.modelEcCultDetailCover.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.EcCultDetail, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelEcCultCover.controller.isEntityLocked(<Entities.EcCult>cur.eccuId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupEcCultDetailCover_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupEcCultDetailCover";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupEcCultDetailCover_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.EcCultDetail[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.EcCultDetail[]) => void> = [];


        public getMergedItems(func: (items: Entities.EcCultDetail[]) => void, bContinuousCaller:boolean=true, parent?: Entities.EcCult, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.EcCultDetail[]):void {
                var mergedEntities = <Entities.EcCultDetail[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.EcCultDetail>e.a).eccuId)).
                        map(e => <Entities.EcCultDetail>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupEcCultDetailCover";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['eccuId_eccuId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.EcCultDetail[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.EcCultDetail):boolean {
            if (this.Parent === undefined || x.eccuId === undefined)
                return false;
            return x.eccuId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.EcCult {
            var self = this;
            return self.eccuId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelEcCultCover.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get eccuId():Entities.EcCult {
            var self = this;
            return <Entities.EcCult>self.$scope.modelEcCultCover.selectedEntities[0];
        }









        public constructEntity(): Entities.EcCultDetail {
            var self = this;
            var ret = new Entities.EcCultDetail(
                /*eccdId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*orderingNumber:number*/ null,
                /*agreesDeclar:number*/ null,
                /*comparisonOper:number*/ null,
                /*probabThres:number*/ null,
                /*decisionLight:number*/ null,
                /*rowVersion:number*/ null,
                /*agreesDeclar2:number*/ null,
                /*comparisonOper2:number*/ null,
                /*probabThres2:number*/ null,
                /*probabThresSum:number*/ null,
                /*comparisonOper3:number*/ null,
                /*eccuId:Entities.EcCult*/ null);
            this.getMergedItems(items => {
                ret.orderingNumber = items.maxBy(i => i.orderingNumber, 0) + 1;
            }, false);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.EcCultDetail = <Entities.EcCultDetail>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.eccuId = self.eccuId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.EcCultDetail): Entities.EcCultDetail {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.EcCultDetail, calledByParent: boolean= false): Entities.EcCultDetail {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.EcCultDetail.Create();
            ret.updateInstance(src);
            ret.eccdId = tmpId;
            if (!calledByParent) {
                this.getMergedItems(items => {
                    ret.orderingNumber = items.maxBy(i => i.orderingNumber, 0) + 1;
                }, false);
            }
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.EcCult, newParent:Entities.EcCult) {
        
            this.model.totalItems = 0;

            this.getMergedItems(ecCultDetailList => {
                ecCultDetailList.forEach(ecCultDetail => {
                    var ecCultDetailCloned = this.cloneEntity_internal(ecCultDetail, true);
                    ecCultDetailCloned.eccuId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(ecCult: Entities.EcCult) {
        

            var self = this;
            var toBeDeleted:Array<Entities.EcCultDetail> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var ecCultDetail = <Entities.EcCultDetail>change.a
                if (ecCult.getKey() === ecCultDetail.eccuId.getKey()) {
                    toBeDeleted.push(ecCultDetail);
                }
            }

            _.each(toBeDeleted, (x:Entities.EcCultDetail) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
            });

        }

        public deleteAllEntitiesUnderParent(ecCult: Entities.EcCult, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteEcCultDetailList(ecCultDetailList: Entities.EcCultDetail[]) {
                if (ecCultDetailList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = ecCultDetailList[0];
                    var tail = ecCultDetailList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteEcCultDetailList(tail);
                    });
                }
            }


            this.getMergedItems(ecCultDetailList => {
                deleteEcCultDetailList(ecCultDetailList);
            }, false, ecCult);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.EcCultDetail, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        public EcCultDetailCover_itm__orderingNumber_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public EcCultDetailCover_itm__agreesDeclar_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public EcCultDetailCover_itm__comparisonOper_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public EcCultDetailCover_itm__probabThres_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public EcCultDetailCover_itm__agreesDeclar2_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public EcCultDetailCover_itm__comparisonOper2_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public EcCultDetailCover_itm__probabThres2_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public EcCultDetailCover_itm__comparisonOper3_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public EcCultDetailCover_itm__probabThresSum_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public EcCultDetailCover_itm__decisionLight_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            var self = this;
            if (self.Parent === null || self.Parent === undefined)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege

            

            var parEntityIsLocked   = self.ParentController.isEntityLocked(self.Parent, LockKind.UpdateLock);
            if (parEntityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public _deleteIsDisabled(ecCultDetail:Entities.EcCultDetail):boolean  {
            var self = this;
            if (ecCultDetail === null || ecCultDetail === undefined || ecCultDetail.getEntityName() !== "EcCultDetail")
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_D"))
                return true; // no delete privilege;




            var entityIsLocked   = self.isEntityLocked(ecCultDetail, LockKind.DeleteLock);
            if (entityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }


    }


    // GROUP EcCultSuper

    export class ModelEcCultSuperBase extends Controllers.AbstractGroupTableModel {
        modelEcCultDetailSuper:ModelEcCultDetailSuper;
        controller: ControllerEcCultSuper;
        public get eccuId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCult>this.selectedEntities[0]).eccuId;
        }

        public set eccuId(eccuId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCult>this.selectedEntities[0]).eccuId = eccuId_newVal;
        }

        public get _eccuId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._eccuId;
        }

        public get noneMatchDecision():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCult>this.selectedEntities[0]).noneMatchDecision;
        }

        public set noneMatchDecision(noneMatchDecision_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCult>this.selectedEntities[0]).noneMatchDecision = noneMatchDecision_newVal;
        }

        public get _noneMatchDecision():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._noneMatchDecision;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCult>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCult>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get cultId():Entities.Cultivation {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCult>this.selectedEntities[0]).cultId;
        }

        public set cultId(cultId_newVal:Entities.Cultivation) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCult>this.selectedEntities[0]).cultId = cultId_newVal;
        }

        public get _cultId():NpTypes.UIManyToOneModel<Entities.Cultivation> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cultId;
        }

        public get ecgrId():Entities.EcGroup {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCult>this.selectedEntities[0]).ecgrId;
        }

        public set ecgrId(ecgrId_newVal:Entities.EcGroup) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCult>this.selectedEntities[0]).ecgrId = ecgrId_newVal;
        }

        public get _ecgrId():NpTypes.UIManyToOneModel<Entities.EcGroup> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._ecgrId;
        }

        public get cotyId():Entities.CoverType {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCult>this.selectedEntities[0]).cotyId;
        }

        public set cotyId(cotyId_newVal:Entities.CoverType) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCult>this.selectedEntities[0]).cotyId = cotyId_newVal;
        }

        public get _cotyId():NpTypes.UIManyToOneModel<Entities.CoverType> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cotyId;
        }

        public get sucaId():Entities.SuperClas {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCult>this.selectedEntities[0]).sucaId;
        }

        public set sucaId(sucaId_newVal:Entities.SuperClas) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCult>this.selectedEntities[0]).sucaId = sucaId_newVal;
        }

        public get _sucaId():NpTypes.UIManyToOneModel<Entities.SuperClas> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._sucaId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeEcCultSuper) { super($scope); }
    }



    export interface IScopeEcCultSuperBase extends Controllers.IAbstractTableGroupScope, IScopeGrpEcGroupBase{
        globals: Globals;
        modelEcCultSuper : ModelEcCultSuper;
        EcCultSuper_itm__cultId_disabled(ecCult:Entities.EcCult):boolean; 
        showLov_EcCultSuper_itm__cultId(ecCult:Entities.EcCult):void; 
        child_group_EcCultDetailSuper_isInvisible():boolean; 

    }



    export class ControllerEcCultSuperBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeEcCultSuper,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelEcCultSuper)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 1});
            var self:ControllerEcCultSuperBase = this;
            model.controller = <ControllerEcCultSuper>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelEcCultSuper = self.model;

            $scope.EcCultSuper_itm__cultId_disabled = 
                (ecCult:Entities.EcCult) => {
                    return self.EcCultSuper_itm__cultId_disabled(ecCult);
                };
            $scope.showLov_EcCultSuper_itm__cultId = 
                (ecCult:Entities.EcCult) => {
                    $timeout( () => {        
                        self.showLov_EcCultSuper_itm__cultId(ecCult);
                    }, 0);
                };
            $scope.child_group_EcCultDetailSuper_isInvisible = 
                () => {
                    return self.child_group_EcCultDetailSuper_isInvisible();
                };


            $scope.pageModel.modelEcCultSuper = $scope.modelEcCultSuper;


            $scope.clearBtnAction = () => { 
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpEcGroup.modelEcCultSuper = $scope.modelEcCultSuper;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpEcGroup.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCult[] , oldVisible:Entities.EcCult[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
            //bind entity child collection with child controller merged items
            Entities.EcCult.ecCultDetailCollection = (ecCult, func) => {
                this.model.modelEcCultDetailSuper.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, ecCult);
            }

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
                //unbind entity child collection with child controller merged items
                Entities.EcCult.ecCultDetailCollection = null;//(ecCult, func) => { }
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerEcCultSuper";
        }
        public get HtmlDivId(): string {
            return "EcGroup_ControllerEcCultSuper";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new LovItem (
                        (ent?: Entities.EcCult) => 'Crop',
                        false ,
                        (ent?: Entities.EcCult) => ent.cultId,  
                        (ent?: Entities.EcCult) => ent._cultId,  
                        (ent?: Entities.EcCult) => false, //isRequired
                        (vl: Entities.Cultivation, ent?: Entities.EcCult) => new NpTypes.ValidationResult(true, ""))
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Crop to Land Cover Criteria";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridClone\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);cloneEntity(row.entity)\" data-ng-disabled=\"newIsDisabled()\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined},
                { cellClass:'cellToolTip', field:'cultId.name', displayName:'getALString("Crop", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'23%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <div data-on-key-down='!EcCultSuper_itm__cultId_disabled(row.entity) && showLov_EcCultSuper_itm__cultId(row.entity)' data-keys='[123]' > \
                    <input data-np-source='row.entity.cultId.name' data-np-ui-model='row.entity._cultId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='EcCultSuper_itm__cultId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_EcCultSuper_itm__cultId(row.entity)'   data-ng-disabled=\"EcCultSuper_itm__cultId_disabled(row.entity)\"   />  \
                    </div> \
                </div>"},
                { cellClass:'cellToolTip', field:'cultId.code', displayName:'getALString("Crop Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultSuper_itm__cultId_code' data-ng-model='row.entity.cultId.code' data-np-ui-model='row.entity.cultId._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultId.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'cultId.cotyId.name', displayName:'getALString("Cover Type", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'14%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultSuper_itm__cultId_cotyId_name' data-ng-model='row.entity.cultId.cotyId.name' data-np-ui-model='row.entity.cultId.cotyId._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultId.cotyId.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'cultId.cotyId.code', displayName:'getALString("Cover Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultSuper_itm__cultId_cotyId_code' data-ng-model='row.entity.cultId.cotyId.code' data-np-ui-model='row.entity.cultId.cotyId._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultId.cotyId.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='0' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelEcCultSuper():ModelEcCultSuper {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "EcCult";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.EcGroup): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.EcCult.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.ecgrId = this.Parent;
                } else {
                    x.ecgrId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.EcCult {
            var self = this;
            return <Entities.EcCult>self.$scope.modelEcCultSuper.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.EcCult, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpEcGroup.controller.isEntityLocked(<Entities.EcGroup>cur.ecgrId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCult/findAllByCriteriaRange_EcGroupEcCultSuper_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCult/findAllByCriteriaRange_EcGroupEcCultSuper";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCult/findAllByCriteriaRange_EcGroupEcCultSuper_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgrId)) {
                    paramData['ecgrId_ecgrId'] = self.Parent.ecgrId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.EcCult[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.EcCult[]) => void> = [];


        public getMergedItems(func: (items: Entities.EcCult[]) => void, bContinuousCaller:boolean=true, parent?: Entities.EcGroup, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.EcCult[]):void {
                var mergedEntities = <Entities.EcCult[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.EcCult>e.a).ecgrId)).
                        map(e => <Entities.EcCult>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "EcCult/findAllByCriteriaRange_EcGroupEcCultSuper";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['ecgrId_ecgrId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.EcCult[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.EcCult):boolean {
            if (this.Parent === undefined || x.ecgrId === undefined)
                return false;
            return x.ecgrId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.EcGroup {
            var self = this;
            return self.ecgrId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpEcGroup.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get ecgrId():Entities.EcGroup {
            var self = this;
            return <Entities.EcGroup>self.$scope.modelGrpEcGroup.selectedEntities[0];
        }









        public constructEntity(): Entities.EcCult {
            var self = this;
            var ret = new Entities.EcCult(
                /*eccuId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*noneMatchDecision:number*/ null,
                /*rowVersion:number*/ null,
                /*cultId:Entities.Cultivation*/ null,
                /*ecgrId:Entities.EcGroup*/ null,
                /*cotyId:Entities.CoverType*/ null,
                /*sucaId:Entities.SuperClas*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.EcCult = <Entities.EcCult>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.ecgrId = self.ecgrId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.EcCult): Entities.EcCult {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.EcCult, calledByParent: boolean= false): Entities.EcCult {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.EcCult.Create();
            ret.updateInstance(src);
            ret.eccuId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
                this.modelEcCultSuper.modelEcCultDetailSuper.controller.cloneAllEntitiesUnderParent(src, ret);
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.EcGroup, newParent:Entities.EcGroup) {
        
            this.model.totalItems = 0;

            this.getMergedItems(ecCultList => {
                ecCultList.forEach(ecCult => {
                    var ecCultCloned = this.cloneEntity_internal(ecCult, true);
                    ecCultCloned.ecgrId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(ecGroup: Entities.EcGroup) {
        

            var self = this;
            var toBeDeleted:Array<Entities.EcCult> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var ecCult = <Entities.EcCult>change.a
                if (ecGroup.getKey() === ecCult.ecgrId.getKey()) {
                    toBeDeleted.push(ecCult);
                }
            }

            _.each(toBeDeleted, (x:Entities.EcCult) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
                self.modelEcCultSuper.modelEcCultDetailSuper.controller.deleteNewEntitiesUnderParent(x);
            });

        }

        public deleteAllEntitiesUnderParent(ecGroup: Entities.EcGroup, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteEcCultList(ecCultList: Entities.EcCult[]) {
                if (ecCultList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = ecCultList[0];
                    var tail = ecCultList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteEcCultList(tail);
                    });
                }
            }


            this.getMergedItems(ecCultList => {
                deleteEcCultList(ecCultList);
            }, false, ecGroup);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                    self.modelEcCultSuper.modelEcCultDetailSuper.controller
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.EcCult, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                self.modelEcCultSuper.modelEcCultDetailSuper.controller.deleteAllEntitiesUnderParent(ent, () => {
                    super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                });
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    self.modelEcCultSuper.modelEcCultDetailSuper.controller.deleteNewEntitiesUnderParent(ent);
                    afterDeleteAction();
                });
            }
        }


        public EcCultSuper_itm__cultId_disabled(ecCult:Entities.EcCult):boolean {
            var self = this;
            if (ecCult === undefined || ecCult === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCult, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_EcCultSuper_itm__cultId:ModelLovCultivationBase;
        public showLov_EcCultSuper_itm__cultId(ecCult:Entities.EcCult) {
            var self = this;
            if (ecCult === undefined)
                return;
            var uimodel:NpTypes.IUIModel = ecCult._cultId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Cultivation';
            dialogOptions.previousModel= self.cachedLovModel_EcCultSuper_itm__cultId;
            dialogOptions.className = "ControllerLovCultivation";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var cultivation1:Entities.Cultivation =   <Entities.Cultivation>selectedEntity;
                uimodel.clearAllErrors();
                if(false && isVoid(cultivation1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(cultivation1) && cultivation1.isEqual(ecCult.cultId))
                    return;
                ecCult.cultId = cultivation1;
                self.markEntityAsUpdated(ecCult, 'EcCultSuper_itm__cultId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovCultivationBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_EcCultSuper_itm__cultId = lovModel;
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovCultivationBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['code'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/Cultivation.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController.GrpEcGroup_2_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController.GrpEcGroup_2_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            var self = this;
            if (self.Parent === null || self.Parent === undefined)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege

            

            var parEntityIsLocked   = self.ParentController.isEntityLocked(self.Parent, LockKind.UpdateLock);
            if (parEntityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public _deleteIsDisabled(ecCult:Entities.EcCult):boolean  {
            var self = this;
            if (ecCult === null || ecCult === undefined || ecCult.getEntityName() !== "EcCult")
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_D"))
                return true; // no delete privilege;




            var entityIsLocked   = self.isEntityLocked(ecCult, LockKind.DeleteLock);
            if (entityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public child_group_EcCultDetailSuper_isInvisible():boolean {
            var self = this;
            if ((self.model.modelEcCultDetailSuper === undefined) || (self.model.modelEcCultDetailSuper.controller === undefined))
                return false;
            return self.model.modelEcCultDetailSuper.controller._isInvisible()
        }

    }


    // GROUP EcCultDetailSuper

    export class ModelEcCultDetailSuperBase extends Controllers.AbstractGroupTableModel {
        controller: ControllerEcCultDetailSuper;
        public get eccdId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).eccdId;
        }

        public set eccdId(eccdId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).eccdId = eccdId_newVal;
        }

        public get _eccdId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._eccdId;
        }

        public get orderingNumber():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).orderingNumber;
        }

        public set orderingNumber(orderingNumber_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).orderingNumber = orderingNumber_newVal;
        }

        public get _orderingNumber():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._orderingNumber;
        }

        public get agreesDeclar():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).agreesDeclar;
        }

        public set agreesDeclar(agreesDeclar_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).agreesDeclar = agreesDeclar_newVal;
        }

        public get _agreesDeclar():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._agreesDeclar;
        }

        public get comparisonOper():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).comparisonOper;
        }

        public set comparisonOper(comparisonOper_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).comparisonOper = comparisonOper_newVal;
        }

        public get _comparisonOper():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._comparisonOper;
        }

        public get probabThres():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).probabThres;
        }

        public set probabThres(probabThres_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).probabThres = probabThres_newVal;
        }

        public get _probabThres():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probabThres;
        }

        public get decisionLight():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).decisionLight;
        }

        public set decisionLight(decisionLight_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).decisionLight = decisionLight_newVal;
        }

        public get _decisionLight():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._decisionLight;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get agreesDeclar2():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).agreesDeclar2;
        }

        public set agreesDeclar2(agreesDeclar2_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).agreesDeclar2 = agreesDeclar2_newVal;
        }

        public get _agreesDeclar2():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._agreesDeclar2;
        }

        public get comparisonOper2():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).comparisonOper2;
        }

        public set comparisonOper2(comparisonOper2_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).comparisonOper2 = comparisonOper2_newVal;
        }

        public get _comparisonOper2():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._comparisonOper2;
        }

        public get probabThres2():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).probabThres2;
        }

        public set probabThres2(probabThres2_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).probabThres2 = probabThres2_newVal;
        }

        public get _probabThres2():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probabThres2;
        }

        public get probabThresSum():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).probabThresSum;
        }

        public set probabThresSum(probabThresSum_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).probabThresSum = probabThresSum_newVal;
        }

        public get _probabThresSum():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._probabThresSum;
        }

        public get comparisonOper3():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).comparisonOper3;
        }

        public set comparisonOper3(comparisonOper3_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).comparisonOper3 = comparisonOper3_newVal;
        }

        public get _comparisonOper3():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._comparisonOper3;
        }

        public get eccuId():Entities.EcCult {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.EcCultDetail>this.selectedEntities[0]).eccuId;
        }

        public set eccuId(eccuId_newVal:Entities.EcCult) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.EcCultDetail>this.selectedEntities[0]).eccuId = eccuId_newVal;
        }

        public get _eccuId():NpTypes.UIManyToOneModel<Entities.EcCult> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._eccuId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeEcCultDetailSuper) { super($scope); }
    }



    export interface IScopeEcCultDetailSuperBase extends Controllers.IAbstractTableGroupScope, IScopeEcCultSuperBase{
        globals: Globals;
        modelEcCultDetailSuper : ModelEcCultDetailSuper;
        EcCultDetailSuper_itm__orderingNumber_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        EcCultDetailSuper_itm__agreesDeclar_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        EcCultDetailSuper_itm__comparisonOper_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        EcCultDetailSuper_itm__probabThres_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        EcCultDetailSuper_itm__agreesDeclar2_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        EcCultDetailSuper_itm__comparisonOper2_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        EcCultDetailSuper_itm__probabThres2_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        EcCultDetailSuper_itm__comparisonOper3_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        EcCultDetailSuper_itm__probabThresSum_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 
        EcCultDetailSuper_itm__decisionLight_disabled(ecCultDetail:Entities.EcCultDetail):boolean; 

    }



    export class ControllerEcCultDetailSuperBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeEcCultDetailSuper,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelEcCultDetailSuper)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 3});
            var self:ControllerEcCultDetailSuperBase = this;
            model.controller = <ControllerEcCultDetailSuper>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelEcCultDetailSuper = self.model;

            $scope.EcCultDetailSuper_itm__orderingNumber_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.EcCultDetailSuper_itm__orderingNumber_disabled(ecCultDetail);
                };
            $scope.EcCultDetailSuper_itm__agreesDeclar_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.EcCultDetailSuper_itm__agreesDeclar_disabled(ecCultDetail);
                };
            $scope.EcCultDetailSuper_itm__comparisonOper_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.EcCultDetailSuper_itm__comparisonOper_disabled(ecCultDetail);
                };
            $scope.EcCultDetailSuper_itm__probabThres_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.EcCultDetailSuper_itm__probabThres_disabled(ecCultDetail);
                };
            $scope.EcCultDetailSuper_itm__agreesDeclar2_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.EcCultDetailSuper_itm__agreesDeclar2_disabled(ecCultDetail);
                };
            $scope.EcCultDetailSuper_itm__comparisonOper2_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.EcCultDetailSuper_itm__comparisonOper2_disabled(ecCultDetail);
                };
            $scope.EcCultDetailSuper_itm__probabThres2_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.EcCultDetailSuper_itm__probabThres2_disabled(ecCultDetail);
                };
            $scope.EcCultDetailSuper_itm__comparisonOper3_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.EcCultDetailSuper_itm__comparisonOper3_disabled(ecCultDetail);
                };
            $scope.EcCultDetailSuper_itm__probabThresSum_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.EcCultDetailSuper_itm__probabThresSum_disabled(ecCultDetail);
                };
            $scope.EcCultDetailSuper_itm__decisionLight_disabled = 
                (ecCultDetail:Entities.EcCultDetail) => {
                    return self.EcCultDetailSuper_itm__decisionLight_disabled(ecCultDetail);
                };


            $scope.pageModel.modelEcCultDetailSuper = $scope.modelEcCultDetailSuper;


            $scope.clearBtnAction = () => { 
                self.updateGrid(0, false, true);
            };



            $scope.modelEcCultSuper.modelEcCultDetailSuper = $scope.modelEcCultDetailSuper;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelEcCultSuper.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCultDetail[] , oldVisible:Entities.EcCultDetail[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerEcCultDetailSuper";
        }
        public get HtmlDivId(): string {
            return "EcGroup_ControllerEcCultDetailSuper";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new NumberItem (
                        (ent?: Entities.EcCultDetail) => 'Evaluation \n Order',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.orderingNumber,  
                        (ent?: Entities.EcCultDetail) => ent._orderingNumber,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined,
                        0),
                    new StaticListItem<number> (
                        (ent?: Entities.EcCultDetail) => '1st \ Prediction ',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.agreesDeclar,  
                        (ent?: Entities.EcCultDetail) => ent._agreesDeclar,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, "")),
                    new StaticListItem<number> (
                        (ent?: Entities.EcCultDetail) => 'Comparison \n Operator 1st',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.comparisonOper,  
                        (ent?: Entities.EcCultDetail) => ent._comparisonOper,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, "")),
                    new NumberItem (
                        (ent?: Entities.EcCultDetail) => 'Confidence \n Pred. 1st (%)',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.probabThres,  
                        (ent?: Entities.EcCultDetail) => ent._probabThres,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined,
                        2),
                    new StaticListItem<number> (
                        (ent?: Entities.EcCultDetail) => '2nd \ Prediction ',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.agreesDeclar2,  
                        (ent?: Entities.EcCultDetail) => ent._agreesDeclar2,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, "")),
                    new StaticListItem<number> (
                        (ent?: Entities.EcCultDetail) => 'Comparison \n Operator 2nd',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.comparisonOper2,  
                        (ent?: Entities.EcCultDetail) => ent._comparisonOper2,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, "")),
                    new NumberItem (
                        (ent?: Entities.EcCultDetail) => 'Confidence \n Pred. 2nd (%)',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.probabThres2,  
                        (ent?: Entities.EcCultDetail) => ent._probabThres2,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined,
                        2),
                    new StaticListItem<number> (
                        (ent?: Entities.EcCultDetail) => 'Comparison \n Operator 1st + 2nd',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.comparisonOper3,  
                        (ent?: Entities.EcCultDetail) => ent._comparisonOper3,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, "")),
                    new NumberItem (
                        (ent?: Entities.EcCultDetail) => 'Confidence \n Prediction \n 1st + 2nd (%)',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.probabThresSum,  
                        (ent?: Entities.EcCultDetail) => ent._probabThresSum,  
                        (ent?: Entities.EcCultDetail) => false, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined,
                        2),
                    new StaticListItem<number> (
                        (ent?: Entities.EcCultDetail) => 'Traffic \n Light Code',
                        false ,
                        (ent?: Entities.EcCultDetail) => ent.decisionLight,  
                        (ent?: Entities.EcCultDetail) => ent._decisionLight,  
                        (ent?: Entities.EcCultDetail) => true, //isRequired
                        (vl: number, ent?: Entities.EcCultDetail) => new NpTypes.ValidationResult(true, ""))
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Criteria";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'orderingNumber', displayName:'getALString("Evaluation \n Order", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'5%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailSuper_itm__orderingNumber' data-ng-model='row.entity.orderingNumber' data-np-ui-model='row.entity._orderingNumber' data-ng-change='markEntityAsUpdated(row.entity,&quot;orderingNumber&quot;)' data-ng-readonly='EcCultDetailSuper_itm__orderingNumber_disabled(row.entity)' data-np-number='dummy' data-np-decimals='0' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'agreesDeclar', displayName:'getALString("1st \ Prediction ", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailSuper_itm__agreesDeclar' data-ng-model='row.entity.agreesDeclar' data-np-ui-model='row.entity._agreesDeclar' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='EcCultDetailSuper_itm__agreesDeclar_disabled(row.entity)' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'comparisonOper', displayName:'getALString("Comparison \n Operator 1st", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'11%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailSuper_itm__comparisonOper' data-ng-model='row.entity.comparisonOper' data-np-ui-model='row.entity._comparisonOper' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='EcCultDetailSuper_itm__comparisonOper_disabled(row.entity)' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'probabThres', displayName:'getALString("Confidence \n Pred. 1st (%)", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailSuper_itm__probabThres' data-ng-model='row.entity.probabThres' data-np-ui-model='row.entity._probabThres' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres&quot;)' data-ng-readonly='EcCultDetailSuper_itm__probabThres_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'agreesDeclar2', displayName:'getALString("2nd \ Prediction ", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailSuper_itm__agreesDeclar2' data-ng-model='row.entity.agreesDeclar2' data-np-ui-model='row.entity._agreesDeclar2' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='EcCultDetailSuper_itm__agreesDeclar2_disabled(row.entity)' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'comparisonOper2', displayName:'getALString("Comparison \n Operator 2nd", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'11%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailSuper_itm__comparisonOper2' data-ng-model='row.entity.comparisonOper2' data-np-ui-model='row.entity._comparisonOper2' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='EcCultDetailSuper_itm__comparisonOper2_disabled(row.entity)' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'probabThres2', displayName:'getALString("Confidence \n Pred. 2nd (%)", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailSuper_itm__probabThres2' data-ng-model='row.entity.probabThres2' data-np-ui-model='row.entity._probabThres2' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres2&quot;)' data-ng-readonly='EcCultDetailSuper_itm__probabThres2_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'comparisonOper3', displayName:'getALString("Comparison \n Operator 1st + 2nd", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'11%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailSuper_itm__comparisonOper3' data-ng-model='row.entity.comparisonOper3' data-np-ui-model='row.entity._comparisonOper3' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper3&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='EcCultDetailSuper_itm__comparisonOper3_disabled(row.entity)' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'probabThresSum', displayName:'getALString("Confidence \n Prediction \n 1st + 2nd (%)", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailSuper_itm__probabThresSum' data-ng-model='row.entity.probabThresSum' data-np-ui-model='row.entity._probabThresSum' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThresSum&quot;)' data-ng-readonly='EcCultDetailSuper_itm__probabThresSum_disabled(row.entity)' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'decisionLight', displayName:'getALString("Traffic \n Light Code", true)', requiredAsterisk:self.$scope.globals.hasPrivilege("Niva_EcGroup_W"), resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailSuper_itm__decisionLight' data-ng-model='row.entity.decisionLight' data-np-ui-model='row.entity._decisionLight' data-ng-change='markEntityAsUpdated(row.entity,&quot;decisionLight&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Green\"), value:1}, {label:getALString(\"Yellow\"), value:2}, {label:getALString(\"Red\"), value:3}, {label:getALString(\"Undefined\"), value:4}]' data-ng-disabled='EcCultDetailSuper_itm__decisionLight_disabled(row.entity)' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelEcCultDetailSuper():ModelEcCultDetailSuper {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "EcCultDetail";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.EcCult): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.EcCultDetail.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.eccuId = this.Parent;
                } else {
                    x.eccuId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.EcCultDetail {
            var self = this;
            return <Entities.EcCultDetail>self.$scope.modelEcCultDetailSuper.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.EcCultDetail, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelEcCultSuper.controller.isEntityLocked(<Entities.EcCult>cur.eccuId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupEcCultDetailSuper_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupEcCultDetailSuper";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupEcCultDetailSuper_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.eccuId)) {
                    paramData['eccuId_eccuId'] = self.Parent.eccuId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.EcCultDetail[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.EcCultDetail[]) => void> = [];


        public getMergedItems(func: (items: Entities.EcCultDetail[]) => void, bContinuousCaller:boolean=true, parent?: Entities.EcCult, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.EcCultDetail[]):void {
                var mergedEntities = <Entities.EcCultDetail[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.EcCultDetail>e.a).eccuId)).
                        map(e => <Entities.EcCultDetail>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "EcCultDetail/findAllByCriteriaRange_EcGroupEcCultDetailSuper";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['eccuId_eccuId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.EcCultDetail[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.EcCultDetail):boolean {
            if (this.Parent === undefined || x.eccuId === undefined)
                return false;
            return x.eccuId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.EcCult {
            var self = this;
            return self.eccuId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelEcCultSuper.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get eccuId():Entities.EcCult {
            var self = this;
            return <Entities.EcCult>self.$scope.modelEcCultSuper.selectedEntities[0];
        }









        public constructEntity(): Entities.EcCultDetail {
            var self = this;
            var ret = new Entities.EcCultDetail(
                /*eccdId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*orderingNumber:number*/ null,
                /*agreesDeclar:number*/ null,
                /*comparisonOper:number*/ null,
                /*probabThres:number*/ null,
                /*decisionLight:number*/ null,
                /*rowVersion:number*/ null,
                /*agreesDeclar2:number*/ null,
                /*comparisonOper2:number*/ null,
                /*probabThres2:number*/ null,
                /*probabThresSum:number*/ null,
                /*comparisonOper3:number*/ null,
                /*eccuId:Entities.EcCult*/ null);
            this.getMergedItems(items => {
                ret.orderingNumber = items.maxBy(i => i.orderingNumber, 0) + 1;
            }, false);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.EcCultDetail = <Entities.EcCultDetail>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.eccuId = self.eccuId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.EcCultDetail): Entities.EcCultDetail {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.EcCultDetail, calledByParent: boolean= false): Entities.EcCultDetail {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.EcCultDetail.Create();
            ret.updateInstance(src);
            ret.eccdId = tmpId;
            if (!calledByParent) {
                this.getMergedItems(items => {
                    ret.orderingNumber = items.maxBy(i => i.orderingNumber, 0) + 1;
                }, false);
            }
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.EcCult, newParent:Entities.EcCult) {
        
            this.model.totalItems = 0;

            this.getMergedItems(ecCultDetailList => {
                ecCultDetailList.forEach(ecCultDetail => {
                    var ecCultDetailCloned = this.cloneEntity_internal(ecCultDetail, true);
                    ecCultDetailCloned.eccuId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(ecCult: Entities.EcCult) {
        

            var self = this;
            var toBeDeleted:Array<Entities.EcCultDetail> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var ecCultDetail = <Entities.EcCultDetail>change.a
                if (ecCult.getKey() === ecCultDetail.eccuId.getKey()) {
                    toBeDeleted.push(ecCultDetail);
                }
            }

            _.each(toBeDeleted, (x:Entities.EcCultDetail) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
            });

        }

        public deleteAllEntitiesUnderParent(ecCult: Entities.EcCult, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteEcCultDetailList(ecCultDetailList: Entities.EcCultDetail[]) {
                if (ecCultDetailList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = ecCultDetailList[0];
                    var tail = ecCultDetailList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteEcCultDetailList(tail);
                    });
                }
            }


            this.getMergedItems(ecCultDetailList => {
                deleteEcCultDetailList(ecCultDetailList);
            }, false, ecCult);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.EcCultDetail, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        public EcCultDetailSuper_itm__orderingNumber_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public EcCultDetailSuper_itm__agreesDeclar_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public EcCultDetailSuper_itm__comparisonOper_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public EcCultDetailSuper_itm__probabThres_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public EcCultDetailSuper_itm__agreesDeclar2_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public EcCultDetailSuper_itm__comparisonOper2_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public EcCultDetailSuper_itm__probabThres2_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public EcCultDetailSuper_itm__comparisonOper3_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public EcCultDetailSuper_itm__probabThresSum_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public EcCultDetailSuper_itm__decisionLight_disabled(ecCultDetail:Entities.EcCultDetail):boolean {
            var self = this;
            if (ecCultDetail === undefined || ecCultDetail === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(ecCultDetail, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            var self = this;
            if (self.Parent === null || self.Parent === undefined)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_W"))
                return true; // no write privilege

            

            var parEntityIsLocked   = self.ParentController.isEntityLocked(self.Parent, LockKind.UpdateLock);
            if (parEntityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public _deleteIsDisabled(ecCultDetail:Entities.EcCultDetail):boolean  {
            var self = this;
            if (ecCultDetail === null || ecCultDetail === undefined || ecCultDetail.getEntityName() !== "EcCultDetail")
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_EcGroup_D"))
                return true; // no delete privilege;




            var entityIsLocked   = self.isEntityLocked(ecCultDetail, LockKind.DeleteLock);
            if (entityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }


    }

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
