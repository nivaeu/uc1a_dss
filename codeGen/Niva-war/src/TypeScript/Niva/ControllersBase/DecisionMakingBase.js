var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/DecisionMakingBase.ts" />
/// <reference path="../EntitiesBase/ParcelDecisionBase.ts" />
/// <reference path="../EntitiesBase/EcGroupCopyBase.ts" />
/// <reference path="../EntitiesBase/EcCultCopyBase.ts" />
/// <reference path="../EntitiesBase/EcCultDetailCopyBase.ts" />
/// <reference path="../EntitiesBase/ClassificationBase.ts" />
/// <reference path="LovClassificationBase.ts" />
/// <reference path="../EntitiesBase/EcGroupBase.ts" />
/// <reference path="LovEcGroupBase.ts" />
/// <reference path="../EntitiesBase/CultivationBase.ts" />
/// <reference path="LovCultivationBase.ts" />
/// <reference path="../EntitiesBase/CoverTypeBase.ts" />
/// <reference path="LovCoverTypeBase.ts" />
/// <reference path="../Controllers/DecisionMaking.ts" />
var Controllers;
(function (Controllers) {
    var DecisionMaking;
    (function (DecisionMaking) {
        var PageModelBase = (function (_super) {
            __extends(PageModelBase, _super);
            function PageModelBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(PageModelBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return PageModelBase;
        })(Controllers.AbstractPageModel);
        DecisionMaking.PageModelBase = PageModelBase;
        var PageControllerBase = (function (_super) {
            __extends(PageControllerBase, _super);
            function PageControllerBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelGroupControllers = undefined;
                this._allGroupControllers = undefined;
                var self = this;
                model.controller = self;
                $scope.pageModel = self.model;
                $scope._saveIsDisabled =
                    function () {
                        return self._saveIsDisabled();
                    };
                $scope._cancelIsDisabled =
                    function () {
                        return self._cancelIsDisabled();
                    };
                $scope.onSaveBtnAction = function () {
                    self.onSaveBtnAction(function (response) { self.onSuccesfullSaveFromButton(response); });
                };
                $scope.onNewBtnAction = function () {
                    self.onNewBtnAction();
                };
                $scope.onDeleteBtnAction = function () {
                    self.onDeleteBtnAction();
                };
                $timeout(function () {
                    $('#DecisionMaking').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                    $('#NpMainContent').scrollTop(0);
                }, 0);
            }
            Object.defineProperty(PageControllerBase.prototype, "ControllerClassName", {
                get: function () {
                    return "PageController";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "HtmlDivId", {
                get: function () {
                    return "DecisionMaking";
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._getPageTitle = function () {
                return "Decision Making";
            };
            Object.defineProperty(PageControllerBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._saveIsDisabled = function () {
                var self = this;
                if (!self.$scope.globals.hasPrivilege("Niva_DecisionMaking_W"))
                    return true; // 
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            PageControllerBase.prototype._cancelIsDisabled = function () {
                var self = this;
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            Object.defineProperty(PageControllerBase.prototype, "pageModel", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.update = function () {
                var self = this;
                self.model.modelGrpDema.controller.updateUI();
            };
            PageControllerBase.prototype.refreshVisibleEntities = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpDema.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpParcelDecision.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpEcGroupCopy.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpEcCultCopy.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpEcCultDetailCopy.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelEcCultCopyCover.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelEcCultDetailCopyCover.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelEcCultCopySuper.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelEcCultDetailCopySuper.controller.refreshVisibleEntities(newEntitiesIds);
            };
            PageControllerBase.prototype.refreshGroups = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpDema.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpParcelDecision.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpEcGroupCopy.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpEcCultCopy.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpEcCultDetailCopy.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelEcCultCopyCover.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelEcCultDetailCopyCover.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelEcCultCopySuper.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelEcCultDetailCopySuper.controller.refreshVisibleEntities(newEntitiesIds);
            };
            Object.defineProperty(PageControllerBase.prototype, "FirstLevelGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._firstLevelGroupControllers === undefined) {
                        self._firstLevelGroupControllers = [
                            self.model.modelGrpDema.controller
                        ];
                    }
                    return this._firstLevelGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "AllGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._allGroupControllers === undefined) {
                        self._allGroupControllers = [
                            self.model.modelGrpDema.controller,
                            self.model.modelGrpParcelDecision.controller,
                            self.model.modelGrpEcGroupCopy.controller,
                            self.model.modelGrpEcCultCopy.controller,
                            self.model.modelGrpEcCultDetailCopy.controller,
                            self.model.modelEcCultCopyCover.controller,
                            self.model.modelEcCultDetailCopyCover.controller,
                            self.model.modelEcCultCopySuper.controller,
                            self.model.modelEcCultDetailCopySuper.controller
                        ];
                    }
                    return this._allGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.getPageChanges = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var pageChanges = [];
                if (bForDelete) {
                    pageChanges = pageChanges.concat(self.model.modelGrpDema.controller.getChangesToCommitForDeletion());
                    pageChanges = pageChanges.concat(self.model.modelGrpDema.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpParcelDecision.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpEcGroupCopy.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpEcCultCopy.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpEcCultDetailCopy.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelEcCultCopyCover.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelEcCultDetailCopyCover.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelEcCultCopySuper.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelEcCultDetailCopySuper.controller.getDeleteChangesToCommit());
                }
                else {
                    pageChanges = pageChanges.concat(self.model.modelGrpDema.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpParcelDecision.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpEcGroupCopy.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpEcCultCopy.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpEcCultDetailCopy.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelEcCultCopyCover.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelEcCultDetailCopyCover.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelEcCultCopySuper.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelEcCultDetailCopySuper.controller.getChangesToCommit());
                }
                var hasDecisionMaking = pageChanges.some(function (change) { return change.entityName === "DecisionMaking"; });
                if (!hasDecisionMaking) {
                    var validateEntity = new Controllers.ChangeToCommit(Controllers.ChangeStatus.UPDATE, 0, "DecisionMaking", self.model.modelGrpDema.controller.Current);
                    pageChanges = pageChanges.concat(validateEntity);
                }
                return pageChanges;
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbUrl = function () {
                return "/Niva/rest/MainService/synchronizeChangesWithDb_DecisionMaking";
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbData = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var paramData = {};
                paramData.data = self.getPageChanges(bForDelete);
                return paramData;
            };
            PageControllerBase.prototype.onSuccesfullSaveFromButton = function (response) {
                var self = this;
                _super.prototype.onSuccesfullSaveFromButton.call(this, response);
                if (!isVoid(response.warningMessages)) {
                    NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
                }
                if (!isVoid(response.infoMessages)) {
                    NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
                }
                self.model.modelGrpDema.controller.cleanUpAfterSave();
                self.model.modelGrpParcelDecision.controller.cleanUpAfterSave();
                self.model.modelGrpEcGroupCopy.controller.cleanUpAfterSave();
                self.model.modelGrpEcCultCopy.controller.cleanUpAfterSave();
                self.model.modelGrpEcCultDetailCopy.controller.cleanUpAfterSave();
                self.model.modelEcCultCopyCover.controller.cleanUpAfterSave();
                self.model.modelEcCultDetailCopyCover.controller.cleanUpAfterSave();
                self.model.modelEcCultCopySuper.controller.cleanUpAfterSave();
                self.model.modelEcCultDetailCopySuper.controller.cleanUpAfterSave();
                self.$scope.globals.isCurrentTransactionDirty = false;
                self.refreshGroups(response.newEntitiesIds);
            };
            PageControllerBase.prototype.onFailuredSave = function (data, status) {
                var self = this;
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                var errors = Messages.dynamicMessage(data);
                NpTypes.AlertMessage.addDanger(self.model, errors);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
            };
            PageControllerBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            PageControllerBase.prototype.onSaveBtnAction = function (onSuccess) {
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                var errors = self.validatePage();
                if (errors.length > 0) {
                    var errMessage = errors.join('<br>');
                    NpTypes.AlertMessage.addDanger(self.model, errMessage);
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
                    return;
                }
                if (self.$scope.globals.isCurrentTransactionDirty === false) {
                    var jqSaveBtn = self.SaveBtnJQueryHandler;
                    self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                    return;
                }
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData();
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    if (!self.shownAsDialog()) {
                        var breadcrumbStepModel = self.$scope.getCurrentPageModel();
                        var newDecisionMakingId = response.newEntitiesIds.firstOrNull(function (x) { return x.entityName === 'DecisionMaking'; });
                        if (!isVoid(newDecisionMakingId)) {
                            if (!isVoid(breadcrumbStepModel)) {
                                breadcrumbStepModel.selectedEntity = Entities.DecisionMaking.CreateById(newDecisionMakingId.databaseId);
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        else {
                            if (!isVoid(breadcrumbStepModel) && !(isVoid(breadcrumbStepModel.selectedEntity))) {
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        self.$scope.setCurrentPageModel(breadcrumbStepModel);
                    }
                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status);
                });
            };
            PageControllerBase.prototype.getInitialEntity = function () {
                if (this.shownAsDialog()) {
                    return this.dialogSelectedEntity();
                }
                else {
                    var breadCrumbStepModel = this.$scope.getPageModelByURL('/DecisionMaking');
                    if (isVoid(breadCrumbStepModel)) {
                        return null;
                    }
                    else {
                        return isVoid(breadCrumbStepModel.selectedEntity) ? null : breadCrumbStepModel.selectedEntity;
                    }
                }
            };
            PageControllerBase.prototype.onPageUnload = function (actualNavigation) {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                actualNavigation(self.$scope);
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    actualNavigation(self.$scope);
                }
            };
            PageControllerBase.prototype.onNewBtnAction = function () {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                self.addNewRecord();
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () { self.addNewRecord(); }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    self.addNewRecord();
                }
            };
            PageControllerBase.prototype.addNewRecord = function () {
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                self.model.modelGrpDema.controller.cleanUpAfterSave();
                self.model.modelGrpParcelDecision.controller.cleanUpAfterSave();
                self.model.modelGrpEcGroupCopy.controller.cleanUpAfterSave();
                self.model.modelGrpEcCultCopy.controller.cleanUpAfterSave();
                self.model.modelGrpEcCultDetailCopy.controller.cleanUpAfterSave();
                self.model.modelEcCultCopyCover.controller.cleanUpAfterSave();
                self.model.modelEcCultDetailCopyCover.controller.cleanUpAfterSave();
                self.model.modelEcCultCopySuper.controller.cleanUpAfterSave();
                self.model.modelEcCultDetailCopySuper.controller.cleanUpAfterSave();
                self.model.modelGrpDema.controller.createNewEntityAndAddToUI();
            };
            PageControllerBase.prototype.onDeleteBtnAction = function () {
                var self = this;
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("EntityDeletionWarningMsg"), IconKind.QUESTION, [
                    new Tuple2("MessageBox_Button_Yes", function () { self.deleteRecord(); }),
                    new Tuple2("MessageBox_Button_No", function () { })
                ], 1, 1);
            };
            PageControllerBase.prototype.deleteRecord = function () {
                var self = this;
                if (self.Mode === Controllers.EditMode.NEW) {
                    console.log("Delete pressed in a form while being in new mode!");
                    return;
                }
                NpTypes.AlertMessage.clearAlerts(self.model);
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData(true);
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.$scope.navigateBackward();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.model, Messages.dynamicMessage(data));
                });
            };
            Object.defineProperty(PageControllerBase.prototype, "Mode", {
                get: function () {
                    var self = this;
                    if (isVoid(self.model.modelGrpDema) || isVoid(self.model.modelGrpDema.controller))
                        return Controllers.EditMode.NEW;
                    return self.model.modelGrpDema.controller.Mode;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._newIsDisabled = function () {
                var self = this;
                if (isVoid(self.model.modelGrpDema) || isVoid(self.model.modelGrpDema.controller))
                    return true;
                return self.model.modelGrpDema.controller._newIsDisabled();
            };
            PageControllerBase.prototype._deleteIsDisabled = function () {
                var self = this;
                if (self.Mode === Controllers.EditMode.NEW)
                    return true;
                if (isVoid(self.model.modelGrpDema) || isVoid(self.model.modelGrpDema.controller))
                    return true;
                return self.model.modelGrpDema.controller._deleteIsDisabled(self.model.modelGrpDema.controller.Current);
            };
            return PageControllerBase;
        })(Controllers.AbstractPageController);
        DecisionMaking.PageControllerBase = PageControllerBase;
        // GROUP GrpDema
        var ModelGrpDemaBase = (function (_super) {
            __extends(ModelGrpDemaBase, _super);
            function ModelGrpDemaBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpDemaBase.prototype, "demaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].demaId;
                },
                set: function (demaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].demaId = demaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_demaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._demaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "description", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].description;
                },
                set: function (description_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].description = description_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_description", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._description;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "dateTime", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].dateTime;
                },
                set: function (dateTime_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].dateTime = dateTime_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_dateTime", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._dateTime;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "recordtype", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].recordtype;
                },
                set: function (recordtype_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].recordtype = recordtype_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_recordtype", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._recordtype;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "hasBeenRun", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].hasBeenRun;
                },
                set: function (hasBeenRun_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].hasBeenRun = hasBeenRun_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_hasBeenRun", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._hasBeenRun;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "hasPopulatedIntegratedDecisionsAndIssues", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].hasPopulatedIntegratedDecisionsAndIssues;
                },
                set: function (hasPopulatedIntegratedDecisionsAndIssues_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].hasPopulatedIntegratedDecisionsAndIssues = hasPopulatedIntegratedDecisionsAndIssues_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_hasPopulatedIntegratedDecisionsAndIssues", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._hasPopulatedIntegratedDecisionsAndIssues;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "clasId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].clasId;
                },
                set: function (clasId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].clasId = clasId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_clasId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._clasId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "ecgrId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].ecgrId;
                },
                set: function (ecgrId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].ecgrId = ecgrId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_ecgrId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._ecgrId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpDemaBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpDemaBase;
        })(Controllers.AbstractGroupFormModel);
        DecisionMaking.ModelGrpDemaBase = ModelGrpDemaBase;
        var ControllerGrpDemaBase = (function (_super) {
            __extends(ControllerGrpDemaBase, _super);
            function ControllerGrpDemaBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                $scope.modelGrpDema = self.model;
                var selectedEntity = this.$scope.pageModel.controller.getInitialEntity();
                if (isVoid(selectedEntity)) {
                    self.createNewEntityAndAddToUI();
                }
                else {
                    var clonedEntity = Entities.DecisionMaking.Create();
                    clonedEntity.updateInstance(selectedEntity);
                    $scope.modelGrpDema.visibleEntities[0] = clonedEntity;
                    $scope.modelGrpDema.selectedEntities[0] = clonedEntity;
                }
                $scope.GrpDema_itm__clasId_disabled =
                    function (decisionMaking) {
                        return self.GrpDema_itm__clasId_disabled(decisionMaking);
                    };
                $scope.showLov_GrpDema_itm__clasId =
                    function (decisionMaking) {
                        $timeout(function () {
                            self.showLov_GrpDema_itm__clasId(decisionMaking);
                        }, 0);
                    };
                $scope.GrpDema_itm__ecgrId_disabled =
                    function (decisionMaking) {
                        return self.GrpDema_itm__ecgrId_disabled(decisionMaking);
                    };
                $scope.showLov_GrpDema_itm__ecgrId =
                    function (decisionMaking) {
                        $timeout(function () {
                            self.showLov_GrpDema_itm__ecgrId(decisionMaking);
                        }, 0);
                    };
                $scope.GrpDema_itm__description_disabled =
                    function (decisionMaking) {
                        return self.GrpDema_itm__description_disabled(decisionMaking);
                    };
                $scope.finalizeButtonId_disabled =
                    function (decisionMaking) {
                        return self.finalizeButtonId_disabled(decisionMaking);
                    };
                $scope.finalizationAction =
                    function (decisionMaking) {
                        self.finalizationAction(decisionMaking);
                    };
                $scope.GrpDema_0_disabled =
                    function () {
                        return self.GrpDema_0_disabled();
                    };
                $scope.GrpDema_0_invisible =
                    function () {
                        return self.GrpDema_0_invisible();
                    };
                $scope.GrpDema_0_0_disabled =
                    function () {
                        return self.GrpDema_0_0_disabled();
                    };
                $scope.GrpDema_0_0_invisible =
                    function () {
                        return self.GrpDema_0_0_invisible();
                    };
                $scope.GrpDema_0_0_0_disabled =
                    function (decisionMaking) {
                        return self.GrpDema_0_0_0_disabled(decisionMaking);
                    };
                $scope.actionUpdateIntegratedDecisionsNIssuesFromDema =
                    function (decisionMaking) {
                        self.actionUpdateIntegratedDecisionsNIssuesFromDema(decisionMaking);
                    };
                $scope.child_group_GrpParcelDecision_isInvisible =
                    function () {
                        return self.child_group_GrpParcelDecision_isInvisible();
                    };
                $scope.child_group_GrpEcGroupCopy_isInvisible =
                    function () {
                        return self.child_group_GrpEcGroupCopy_isInvisible();
                    };
                $scope.pageModel.modelGrpDema = $scope.modelGrpDema;
                /*
                        $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                            $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.DecisionMaking[] , oldVisible:Entities.DecisionMaking[]) => {
                                console.log("visible entities changed",newVisible);
                                self.onVisibleItemsChange(newVisible, oldVisible);
                            } )  ));
                */
                //bind entity child collection with child controller merged items
                Entities.DecisionMaking.parcelDecisionCollection = function (decisionMaking, func) {
                    _this.model.modelGrpParcelDecision.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, decisionMaking);
                };
                //bind entity child collection with child controller merged items
                Entities.DecisionMaking.ecGroupCopyCollection = function (decisionMaking, func) {
                    _this.model.modelGrpEcGroupCopy.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, decisionMaking);
                };
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                    //unbind entity child collection with child controller merged items
                    Entities.DecisionMaking.parcelDecisionCollection = null; //(decisionMaking, func) => { }
                    //unbind entity child collection with child controller merged items
                    Entities.DecisionMaking.ecGroupCopyCollection = null; //(decisionMaking, func) => { }
                });
            }
            ControllerGrpDemaBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpDemaBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpDema";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpDemaBase.prototype, "HtmlDivId", {
                get: function () {
                    return "DecisionMaking_ControllerGrpDema";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpDemaBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.LovItem(function (ent) { return 'Data Import Name'; }, false, function (ent) { return ent.clasId; }, function (ent) { return ent._clasId; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.LovItem(function (ent) { return 'BRE Rules Name'; }, false, function (ent) { return ent.ecgrId; }, function (ent) { return ent._ecgrId; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.TextItem(function (ent) { return 'Description'; }, false, function (ent) { return ent.description; }, function (ent) { return ent._description; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.DateItem(function (ent) { return 'Date'; }, false, function (ent) { return ent.dateTime; }, function (ent) { return ent._dateTime; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined)
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpDemaBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpDemaBase.prototype, "modelGrpDema", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpDemaBase.prototype.getEntityName = function () {
                return "DecisionMaking";
            };
            ControllerGrpDemaBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
            };
            ControllerGrpDemaBase.prototype.setCurrentFormPageBreadcrumpStepModel = function () {
                var self = this;
                if (self.$scope.pageModel.controller.shownAsDialog())
                    return;
                var breadCrumbStepModel = { selectedEntity: null };
                if (!isVoid(self.Current)) {
                    var selectedEntity = Entities.DecisionMaking.Create();
                    selectedEntity.updateInstance(self.Current);
                    breadCrumbStepModel.selectedEntity = selectedEntity;
                }
                else {
                    breadCrumbStepModel.selectedEntity = null;
                }
                self.$scope.setCurrentPageModel(breadCrumbStepModel);
            };
            ControllerGrpDemaBase.prototype.getEntitiesFromJSON = function (webResponse) {
                var _this = this;
                var entlist = Entities.DecisionMaking.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpDemaBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpDema.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpDemaBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return false;
            };
            ControllerGrpDemaBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.DecisionMaking(
                /*demaId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*description:string*/ null, 
                /*dateTime:Date*/ new Date, 
                /*rowVersion:number*/ null, 
                /*recordtype:number*/ 0, 
                /*hasBeenRun:boolean*/ null, 
                /*hasPopulatedIntegratedDecisionsAndIssues:boolean*/ null, 
                /*clasId:Entities.Classification*/ null, 
                /*ecgrId:Entities.EcGroup*/ null);
                return ret;
            };
            ControllerGrpDemaBase.prototype.createNewEntityAndAddToUI = function () {
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.$scope.modelGrpDema.visibleEntities[0] = newEnt;
                self.$scope.modelGrpDema.selectedEntities[0] = newEnt;
                return newEnt;
            };
            ControllerGrpDemaBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpDemaBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                var _this = this;
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.DecisionMaking.Create();
                ret.updateInstance(src);
                ret.demaId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.visibleEntities[0] = ret;
                this.model.selectedEntities[0] = ret;
                this.$timeout(function () {
                    _this.modelGrpDema.modelGrpParcelDecision.controller.cloneAllEntitiesUnderParent(src, ret);
                    _this.modelGrpDema.modelGrpEcGroupCopy.controller.cloneAllEntitiesUnderParent(src, ret);
                }, 1);
                return ret;
            };
            Object.defineProperty(ControllerGrpDemaBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [
                            self.modelGrpDema.modelGrpParcelDecision.controller,
                            self.modelGrpDema.modelGrpEcGroupCopy.controller
                        ];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpDemaBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                var _this = this;
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    self.modelGrpDema.modelGrpParcelDecision.controller.deleteAllEntitiesUnderParent(ent, function () {
                        self.modelGrpDema.modelGrpEcGroupCopy.controller.deleteAllEntitiesUnderParent(ent, function () {
                            _super.prototype.deleteEntity.call(_this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                        });
                    });
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        self.modelGrpDema.modelGrpParcelDecision.controller.deleteNewEntitiesUnderParent(ent);
                        self.modelGrpDema.modelGrpEcGroupCopy.controller.deleteNewEntitiesUnderParent(ent);
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpDemaBase.prototype.decisionMakingHasBeenRun = function (decisionMaking) {
                console.warn("Unimplemented function decisionMakingHasBeenRun()");
                return false;
            };
            ControllerGrpDemaBase.prototype.GrpDema_itm__clasId_disabled = function (decisionMaking) {
                var self = this;
                if (decisionMaking === undefined || decisionMaking === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_DecisionMaking_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(decisionMaking, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = self.decisionMakingHasBeenRun(decisionMaking);
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpDemaBase.prototype.showLov_GrpDema_itm__clasId = function (decisionMaking) {
                var self = this;
                if (decisionMaking === undefined)
                    return;
                var uimodel = decisionMaking._clasId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Data Import Name';
                dialogOptions.previousModel = self.cachedLovModel_GrpDema_itm__clasId;
                dialogOptions.className = "ControllerLovClassification";
                dialogOptions.onSelect = function (selectedEntity) {
                    var classification1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (true && isVoid(classification1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(classification1) && classification1.isEqual(decisionMaking.clasId))
                        return;
                    decisionMaking.clasId = classification1;
                    self.markEntityAsUpdated(decisionMaking, 'GrpDema_itm__clasId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpDema_itm__clasId = lovModel;
                    var wsPath = "Classification/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                        paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                        paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                        paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "Classification/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                        paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                        paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                        paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                        paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                        paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                        paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                dialogOptions.shownCols['description'] = true;
                dialogOptions.shownCols['dateTime'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/Classification.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpDemaBase.prototype.GrpDema_itm__ecgrId_disabled = function (decisionMaking) {
                var self = this;
                if (decisionMaking === undefined || decisionMaking === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_DecisionMaking_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(decisionMaking, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = self.decisionMakingHasBeenRun(decisionMaking);
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpDemaBase.prototype.showLov_GrpDema_itm__ecgrId = function (decisionMaking) {
                var self = this;
                if (decisionMaking === undefined)
                    return;
                var uimodel = decisionMaking._ecgrId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'BRE Rules';
                dialogOptions.previousModel = self.cachedLovModel_GrpDema_itm__ecgrId;
                dialogOptions.className = "ControllerLovEcGroup";
                dialogOptions.onSelect = function (selectedEntity) {
                    var ecGroup1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (true && isVoid(ecGroup1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(ecGroup1) && ecGroup1.isEqual(decisionMaking.ecgrId))
                        return;
                    decisionMaking.ecgrId = ecGroup1;
                    self.markEntityAsUpdated(decisionMaking, 'GrpDema_itm__ecgrId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpDema_itm__ecgrId = lovModel;
                    var wsPath = "EcGroup/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_description)) {
                        paramData['fsch_EcGroupLov_description'] = lovModel.fsch_EcGroupLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_name)) {
                        paramData['fsch_EcGroupLov_name'] = lovModel.fsch_EcGroupLov_name;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "EcGroup/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_description)) {
                        paramData['fsch_EcGroupLov_description'] = lovModel.fsch_EcGroupLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_name)) {
                        paramData['fsch_EcGroupLov_name'] = lovModel.fsch_EcGroupLov_name;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_description)) {
                        paramData['fsch_EcGroupLov_description'] = lovModel.fsch_EcGroupLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_name)) {
                        paramData['fsch_EcGroupLov_name'] = lovModel.fsch_EcGroupLov_name;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                dialogOptions.shownCols['description'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/EcGroup.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpDemaBase.prototype.isFieldsDisabled = function (decisionMaking) {
                console.warn("Unimplemented function isFieldsDisabled()");
                return false;
            };
            ControllerGrpDemaBase.prototype.GrpDema_itm__description_disabled = function (decisionMaking) {
                var self = this;
                if (decisionMaking === undefined || decisionMaking === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_DecisionMaking_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(decisionMaking, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = self.isFieldsDisabled(decisionMaking);
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpDemaBase.prototype.finalizeButtonId_disabled = function (decisionMaking) {
                var self = this;
                if (decisionMaking === undefined || decisionMaking === null)
                    return true;
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = self.decisionMakingHasBeenRun(decisionMaking);
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            ControllerGrpDemaBase.prototype.finalizationAction = function (decisionMaking) {
                var self = this;
                console.log("Method: finalizationAction() called");
                console.log(decisionMaking);
            };
            ControllerGrpDemaBase.prototype.GrpDema_0_disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpDemaBase.prototype.GrpDema_0_invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = this.decisionMakingHasNotBeenRun();
                return programmerVal;
            };
            ControllerGrpDemaBase.prototype.decisionMakingHasNotBeenRun = function () {
                console.warn("Unimplemented function decisionMakingHasNotBeenRun()");
                return false;
            };
            ControllerGrpDemaBase.prototype.GrpDema_0_0_disabled = function () {
                if (false)
                    return true;
                var parControl = this.GrpDema_0_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpDemaBase.prototype.GrpDema_0_0_invisible = function () {
                if (false)
                    return true;
                var parControl = this.GrpDema_0_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpDemaBase.prototype.GrpDema_0_0_0_disabled = function (decisionMaking) {
                var self = this;
                if (decisionMaking === undefined || decisionMaking === null)
                    return true;
                var isContainerControlDisabled = self.GrpDema_0_0_disabled();
                var disabledByProgrammerMethod = self.parcelsIntegratedDecisionsAndIssuesHaveBeenPopulated(decisionMaking);
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            ControllerGrpDemaBase.prototype.parcelsIntegratedDecisionsAndIssuesHaveBeenPopulated = function (decisionMaking) {
                console.warn("Unimplemented function parcelsIntegratedDecisionsAndIssuesHaveBeenPopulated()");
                return false;
            };
            ControllerGrpDemaBase.prototype.actionUpdateIntegratedDecisionsNIssuesFromDema = function (decisionMaking) {
                var self = this;
                console.log("Method: actionUpdateIntegratedDecisionsNIssuesFromDema() called");
                console.log(decisionMaking);
            };
            ControllerGrpDemaBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpDemaBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpDemaBase.prototype._newIsDisabled = function () {
                var self = this;
                if (!self.$scope.globals.hasPrivilege("Niva_DecisionMaking_W"))
                    return true; // no write privilege
                var parEntityIsLocked = false;
                if (parEntityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpDemaBase.prototype._deleteIsDisabled = function (decisionMaking) {
                var self = this;
                if (decisionMaking === null || decisionMaking === undefined || decisionMaking.getEntityName() !== "DecisionMaking")
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_DecisionMaking_D"))
                    return true; // no delete privilege;
                var entityIsLocked = self.isEntityLocked(decisionMaking, Controllers.LockKind.DeleteLock);
                if (entityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return self.isFieldsDisabled(decisionMaking);
            };
            ControllerGrpDemaBase.prototype.child_group_GrpParcelDecision_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpParcelDecision === undefined) || (self.model.modelGrpParcelDecision.controller === undefined))
                    return false;
                return self.model.modelGrpParcelDecision.controller._isInvisible();
            };
            ControllerGrpDemaBase.prototype.child_group_GrpEcGroupCopy_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpEcGroupCopy === undefined) || (self.model.modelGrpEcGroupCopy.controller === undefined))
                    return false;
                return self.model.modelGrpEcGroupCopy.controller._isInvisible();
            };
            return ControllerGrpDemaBase;
        })(Controllers.AbstractGroupFormController);
        DecisionMaking.ControllerGrpDemaBase = ControllerGrpDemaBase;
        // GROUP GrpParcelDecision
        var ModelGrpParcelDecisionBase = (function (_super) {
            __extends(ModelGrpParcelDecisionBase, _super);
            function ModelGrpParcelDecisionBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
                this._fsch_decisionLight = new NpTypes.UINumberModel(undefined);
                this._fsch_Parcel_Identifier = new NpTypes.UIStringModel(undefined);
                this._fsch_Code = new NpTypes.UIStringModel(undefined);
                this._fsch_prodCode = new NpTypes.UINumberModel(undefined);
            }
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "padeId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].padeId;
                },
                set: function (padeId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].padeId = padeId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_padeId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._padeId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "decisionLight", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].decisionLight;
                },
                set: function (decisionLight_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].decisionLight = decisionLight_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_decisionLight", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._decisionLight;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "demaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].demaId;
                },
                set: function (demaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].demaId = demaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_demaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._demaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "pclaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].pclaId;
                },
                set: function (pclaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].pclaId = pclaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_pclaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._pclaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "fsch_decisionLight", {
                get: function () {
                    return this._fsch_decisionLight.value;
                },
                set: function (vl) {
                    this._fsch_decisionLight.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "fsch_Parcel_Identifier", {
                get: function () {
                    return this._fsch_Parcel_Identifier.value;
                },
                set: function (vl) {
                    this._fsch_Parcel_Identifier.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "fsch_Code", {
                get: function () {
                    return this._fsch_Code.value;
                },
                set: function (vl) {
                    this._fsch_Code.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "fsch_prodCode", {
                get: function () {
                    return this._fsch_prodCode.value;
                },
                set: function (vl) {
                    this._fsch_prodCode.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelDecisionBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpParcelDecisionBase;
        })(Controllers.AbstractGroupTableModel);
        DecisionMaking.ModelGrpParcelDecisionBase = ModelGrpParcelDecisionBase;
        var ControllerGrpParcelDecisionBase = (function (_super) {
            __extends(ControllerGrpParcelDecisionBase, _super);
            function ControllerGrpParcelDecisionBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 3 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpParcelDecision = self.model;
                $scope._disabled =
                    function () {
                        return self._disabled();
                    };
                $scope._invisible =
                    function () {
                        return self._invisible();
                    };
                $scope.GrpParcelDecision_srchr__fsch_decisionLight_disabled =
                    function (parcelDecision) {
                        return self.GrpParcelDecision_srchr__fsch_decisionLight_disabled(parcelDecision);
                    };
                $scope.GrpParcelDecision_srchr__fsch_Parcel_Identifier_disabled =
                    function (parcelDecision) {
                        return self.GrpParcelDecision_srchr__fsch_Parcel_Identifier_disabled(parcelDecision);
                    };
                $scope.GrpParcelDecision_srchr__fsch_Code_disabled =
                    function (parcelDecision) {
                        return self.GrpParcelDecision_srchr__fsch_Code_disabled(parcelDecision);
                    };
                $scope.GrpParcelDecision_srchr__fsch_prodCode_disabled =
                    function (parcelDecision) {
                        return self.GrpParcelDecision_srchr__fsch_prodCode_disabled(parcelDecision);
                    };
                $scope.GrpParcelDecision_0_disabled =
                    function () {
                        return self.GrpParcelDecision_0_disabled();
                    };
                $scope.GrpParcelDecision_0_invisible =
                    function () {
                        return self.GrpParcelDecision_0_invisible();
                    };
                $scope.GrpParcelDecision_0_0_disabled =
                    function (parcelDecision) {
                        return self.GrpParcelDecision_0_0_disabled(parcelDecision);
                    };
                $scope.exportParcelDecisionsToCSV =
                    function (parcelDecision) {
                        self.exportParcelDecisionsToCSV(parcelDecision);
                    };
                $scope.pageModel.modelGrpParcelDecision = $scope.modelGrpParcelDecision;
                $scope.clearBtnAction = function () {
                    self.modelGrpParcelDecision.fsch_decisionLight = undefined;
                    self.modelGrpParcelDecision.fsch_Parcel_Identifier = undefined;
                    self.modelGrpParcelDecision.fsch_Code = undefined;
                    self.modelGrpParcelDecision.fsch_prodCode = undefined;
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpDema.modelGrpParcelDecision = $scope.modelGrpParcelDecision;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpDema.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.ParcelDecision[] , oldVisible:Entities.ParcelDecision[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            ControllerGrpParcelDecisionBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpParcelDecision";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "HtmlDivId", {
                get: function () {
                    return "DecisionMaking_ControllerGrpParcelDecision";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.StaticListItem(function (ent) { return 'Decision Light'; }, true, function (ent) { return self.model.fsch_decisionLight; }, function (ent) { return self.model._fsch_decisionLight; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }),
                            new Controllers.TextItem(function (ent) { return 'Identifier'; }, true, function (ent) { return self.model.fsch_Parcel_Identifier; }, function (ent) { return self.model._fsch_Parcel_Identifier; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'Parcel Code'; }, true, function (ent) { return self.model.fsch_Code; }, function (ent) { return self.model._fsch_Code; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.NumberItem(function (ent) { return 'Farmer Code'; }, true, function (ent) { return self.model.fsch_prodCode; }, function (ent) { return self.model._fsch_prodCode; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 0)
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelDecisionBase.prototype.gridTitle = function () {
                return "Parcel Decision";
            };
            ControllerGrpParcelDecisionBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpParcelDecisionBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'decisionLight', displayName: 'getALString("Decision Light", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='GrpParcelDecision_itm__decisionLight' data-ng-model='row.entity.decisionLight' data-np-ui-model='row.entity._decisionLight' data-ng-change='markEntityAsUpdated(row.entity,&quot;decisionLight&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Green\"), value:1}, {label:getALString(\"Yellow\"), value:2}, {label:getALString(\"Red\"), value:3}, {label:getALString(\"Undefined\"), value:4}]' data-ng-disabled='true' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.parcIdentifier', displayName: 'getALString("Identifier", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_parcIdentifier' data-ng-model='row.entity.pclaId.parcIdentifier' data-np-ui-model='row.entity.pclaId._parcIdentifier' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.parcIdentifier&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.parcCode', displayName: 'getALString("Parcel \n Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_parcCode' data-ng-model='row.entity.pclaId.parcCode' data-np-ui-model='row.entity.pclaId._parcCode' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.parcCode&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.prodCode', displayName: 'getALString("Farmer \n Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '6%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_prodCode' data-ng-model='row.entity.pclaId.prodCode' data-np-ui-model='row.entity.pclaId._prodCode' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.prodCode&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.cultIdDecl.code', displayName: 'getALString("Cultivation \n Declared Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '7%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_cultIdDecl_code' data-ng-model='row.entity.pclaId.cultIdDecl.code' data-np-ui-model='row.entity.pclaId.cultIdDecl._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdDecl.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.cultIdDecl.name', displayName: 'getALString("Cultivation \n Declared Name", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '9%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_cultIdDecl_name' data-ng-model='row.entity.pclaId.cultIdDecl.name' data-np-ui-model='row.entity.pclaId.cultIdDecl._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdDecl.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.cultIdPred.code', displayName: 'getALString("Cultivation \n 1st Prediction \n Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '7%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_cultIdPred_code' data-ng-model='row.entity.pclaId.cultIdPred.code' data-np-ui-model='row.entity.pclaId.cultIdPred._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdPred.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.cultIdPred.name', displayName: 'getALString("Cultivation \n 1st Prediction \n Name", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '10%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_cultIdPred_name' data-ng-model='row.entity.pclaId.cultIdPred.name' data-np-ui-model='row.entity.pclaId.cultIdPred._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdPred.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.probPred', displayName: 'getALString("Probability \n 1st Prediction", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_probPred' data-ng-model='row.entity.pclaId.probPred' data-np-ui-model='row.entity.pclaId._probPred' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.probPred&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.cultIdPred2.code', displayName: 'getALString("Cultivation \n 2nd Prediction  \nCode", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '7%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_cultIdPred2_code' data-ng-model='row.entity.pclaId.cultIdPred2.code' data-np-ui-model='row.entity.pclaId.cultIdPred2._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdPred2.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.cultIdPred2.name', displayName: 'getALString("Cultivation \n 2nd Prediction \n Name", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '10%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_cultIdPred2_name' data-ng-model='row.entity.pclaId.cultIdPred2.name' data-np-ui-model='row.entity.pclaId.cultIdPred2._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdPred2.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'pclaId.probPred2', displayName: 'getALString("Probability \n 2nd Prediction", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelDecision_itm__pclaId_probPred2' data-ng-model='row.entity.pclaId.probPred2' data-np-ui-model='row.entity.pclaId._probPred2' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.probPred2&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "modelGrpParcelDecision", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelDecisionBase.prototype.getEntityName = function () {
                return "ParcelDecision";
            };
            ControllerGrpParcelDecisionBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerGrpParcelDecisionBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.ParcelDecision.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.demaId = _this.Parent;
                    }
                    else {
                        x.demaId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelDecision.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelDecisionBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpDema.controller.isEntityLocked(cur.demaId, lockKind);
            };
            ControllerGrpParcelDecisionBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_decisionLight)) {
                    paramData['fsch_decisionLight'] = self.modelGrpParcelDecision.fsch_decisionLight;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpParcelDecision.fsch_prodCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_Code)) {
                    paramData['fsch_Code'] = self.modelGrpParcelDecision.fsch_Code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_Parcel_Identifier)) {
                    paramData['fsch_Parcel_Identifier'] = self.modelGrpParcelDecision.fsch_Parcel_Identifier;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpParcelDecisionBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "ParcelDecision/findAllByCriteriaRange_DecisionMakingGrpParcelDecision_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_decisionLight)) {
                    paramData['fsch_decisionLight'] = self.modelGrpParcelDecision.fsch_decisionLight;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpParcelDecision.fsch_prodCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_Code)) {
                    paramData['fsch_Code'] = self.modelGrpParcelDecision.fsch_Code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_Parcel_Identifier)) {
                    paramData['fsch_Parcel_Identifier'] = self.modelGrpParcelDecision.fsch_Parcel_Identifier;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpParcelDecisionBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "ParcelDecision/findAllByCriteriaRange_DecisionMakingGrpParcelDecision";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_decisionLight)) {
                    paramData['fsch_decisionLight'] = self.modelGrpParcelDecision.fsch_decisionLight;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpParcelDecision.fsch_prodCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_Code)) {
                    paramData['fsch_Code'] = self.modelGrpParcelDecision.fsch_Code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_Parcel_Identifier)) {
                    paramData['fsch_Parcel_Identifier'] = self.modelGrpParcelDecision.fsch_Parcel_Identifier;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpParcelDecisionBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "ParcelDecision/findAllByCriteriaRange_DecisionMakingGrpParcelDecision_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_decisionLight)) {
                    paramData['fsch_decisionLight'] = self.modelGrpParcelDecision.fsch_decisionLight;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpParcelDecision.fsch_prodCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_Code)) {
                    paramData['fsch_Code'] = self.modelGrpParcelDecision.fsch_Code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelDecision) && !isVoid(self.modelGrpParcelDecision.fsch_Parcel_Identifier)) {
                    paramData['fsch_Parcel_Identifier'] = self.modelGrpParcelDecision.fsch_Parcel_Identifier;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpParcelDecisionBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.demaId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "ParcelDecision/findAllByCriteriaRange_DecisionMakingGrpParcelDecision";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['demaId_demaId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpParcelDecisionBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.demaId === undefined)
                    return false;
                return x.demaId.getKey() === this.Parent.getKey();
            };
            ControllerGrpParcelDecisionBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.demaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpDema.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "demaId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpDema.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelDecisionBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.ParcelDecision(
                /*padeId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*decisionLight:number*/ null, 
                /*rowVersion:number*/ null, 
                /*demaId:Entities.DecisionMaking*/ null, 
                /*pclaId:Entities.ParcelClas*/ null);
                return ret;
            };
            ControllerGrpParcelDecisionBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.demaId = self.demaId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpParcelDecisionBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpParcelDecisionBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.ParcelDecision.Create();
                ret.updateInstance(src);
                ret.padeId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            ControllerGrpParcelDecisionBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (parcelDecisionList) {
                    parcelDecisionList.forEach(function (parcelDecision) {
                        var parcelDecisionCloned = _this.cloneEntity_internal(parcelDecision, true);
                        parcelDecisionCloned.demaId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpParcelDecisionBase.prototype.deleteNewEntitiesUnderParent = function (decisionMaking) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var parcelDecision = change.a;
                    if (decisionMaking.getKey() === parcelDecision.demaId.getKey()) {
                        toBeDeleted.push(parcelDecision);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                });
            };
            ControllerGrpParcelDecisionBase.prototype.deleteAllEntitiesUnderParent = function (decisionMaking, afterDeleteAction) {
                var self = this;
                function deleteParcelDecisionList(parcelDecisionList) {
                    if (parcelDecisionList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = parcelDecisionList[0];
                        var tail = parcelDecisionList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteParcelDecisionList(tail);
                        });
                    }
                }
                this.getMergedItems(function (parcelDecisionList) {
                    deleteParcelDecisionList(parcelDecisionList);
                }, false, decisionMaking);
            };
            Object.defineProperty(ControllerGrpParcelDecisionBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelDecisionBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpParcelDecisionBase.prototype._disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelDecisionBase.prototype._invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelDecisionBase.prototype.GrpParcelDecision_srchr__fsch_decisionLight_disabled = function (parcelDecision) {
                var self = this;
                return false;
            };
            ControllerGrpParcelDecisionBase.prototype.GrpParcelDecision_srchr__fsch_Parcel_Identifier_disabled = function (parcelDecision) {
                var self = this;
                return false;
            };
            ControllerGrpParcelDecisionBase.prototype.GrpParcelDecision_srchr__fsch_Code_disabled = function (parcelDecision) {
                var self = this;
                return false;
            };
            ControllerGrpParcelDecisionBase.prototype.GrpParcelDecision_srchr__fsch_prodCode_disabled = function (parcelDecision) {
                var self = this;
                return false;
            };
            ControllerGrpParcelDecisionBase.prototype.GrpParcelDecision_0_disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelDecisionBase.prototype.GrpParcelDecision_0_invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelDecisionBase.prototype.GrpParcelDecision_0_0_disabled = function (parcelDecision) {
                var self = this;
                if (parcelDecision === undefined || parcelDecision === null)
                    return true;
                var isContainerControlDisabled = self.GrpParcelDecision_0_disabled();
                var disabledByProgrammerMethod = self.isExportToCSVDisabled(parcelDecision);
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            ControllerGrpParcelDecisionBase.prototype.isExportToCSVDisabled = function (parcelDecision) {
                console.warn("Unimplemented function isExportToCSVDisabled()");
                return false;
            };
            ControllerGrpParcelDecisionBase.prototype.exportParcelDecisionsToCSV = function (parcelDecision) {
                var self = this;
                console.log("Method: exportParcelDecisionsToCSV() called");
                console.log(parcelDecision);
            };
            ControllerGrpParcelDecisionBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController.GrpDema_0_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelDecisionBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController.GrpDema_0_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelDecisionBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerGrpParcelDecisionBase.prototype._deleteIsDisabled = function (parcelDecision) {
                return true;
            };
            return ControllerGrpParcelDecisionBase;
        })(Controllers.AbstractGroupTableController);
        DecisionMaking.ControllerGrpParcelDecisionBase = ControllerGrpParcelDecisionBase;
        // GROUP GrpEcGroupCopy
        var ModelGrpEcGroupCopyBase = (function (_super) {
            __extends(ModelGrpEcGroupCopyBase, _super);
            function ModelGrpEcGroupCopyBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "ecgcId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].ecgcId;
                },
                set: function (ecgcId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].ecgcId = ecgcId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "_ecgcId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._ecgcId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "description", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].description;
                },
                set: function (description_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].description = description_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "_description", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._description;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "name", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].name;
                },
                set: function (name_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].name = name_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "_name", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._name;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "recordtype", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].recordtype;
                },
                set: function (recordtype_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].recordtype = recordtype_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "_recordtype", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._recordtype;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "cropLevel", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cropLevel;
                },
                set: function (cropLevel_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cropLevel = cropLevel_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "_cropLevel", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cropLevel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "demaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].demaId;
                },
                set: function (demaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].demaId = demaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "_demaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._demaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcGroupCopyBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpEcGroupCopyBase;
        })(Controllers.AbstractGroupTableModel);
        DecisionMaking.ModelGrpEcGroupCopyBase = ModelGrpEcGroupCopyBase;
        var ControllerGrpEcGroupCopyBase = (function (_super) {
            __extends(ControllerGrpEcGroupCopyBase, _super);
            function ControllerGrpEcGroupCopyBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpEcGroupCopy = self.model;
                $scope.GrpEcGroupCopy_0_disabled =
                    function () {
                        return self.GrpEcGroupCopy_0_disabled();
                    };
                $scope.GrpEcGroupCopy_0_invisible =
                    function () {
                        return self.GrpEcGroupCopy_0_invisible();
                    };
                $scope.GrpEcGroupCopy_1_disabled =
                    function () {
                        return self.GrpEcGroupCopy_1_disabled();
                    };
                $scope.GrpEcGroupCopy_1_invisible =
                    function () {
                        return self.GrpEcGroupCopy_1_invisible();
                    };
                $scope.GrpEcGroupCopy_2_disabled =
                    function () {
                        return self.GrpEcGroupCopy_2_disabled();
                    };
                $scope.GrpEcGroupCopy_2_invisible =
                    function () {
                        return self.GrpEcGroupCopy_2_invisible();
                    };
                $scope.child_group_GrpEcCultCopy_isInvisible =
                    function () {
                        return self.child_group_GrpEcCultCopy_isInvisible();
                    };
                $scope.child_group_EcCultCopyCover_isInvisible =
                    function () {
                        return self.child_group_EcCultCopyCover_isInvisible();
                    };
                $scope.child_group_EcCultCopySuper_isInvisible =
                    function () {
                        return self.child_group_EcCultCopySuper_isInvisible();
                    };
                $scope.pageModel.modelGrpEcGroupCopy = $scope.modelGrpEcGroupCopy;
                $scope.clearBtnAction = function () {
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpDema.modelGrpEcGroupCopy = $scope.modelGrpEcGroupCopy;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpDema.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcGroupCopy[] , oldVisible:Entities.EcGroupCopy[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                //bind entity child collection with child controller merged items
                Entities.EcGroupCopy.ecCultCopyCollection = function (ecGroupCopy, func) {
                    _this.model.modelGrpEcCultCopy.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, ecGroupCopy);
                };
                //bind entity child collection with child controller merged items
                Entities.EcGroupCopy.ecCultCopyCollection = function (ecGroupCopy, func) {
                    _this.model.modelEcCultCopyCover.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, ecGroupCopy);
                };
                //bind entity child collection with child controller merged items
                Entities.EcGroupCopy.ecCultCopyCollection = function (ecGroupCopy, func) {
                    _this.model.modelEcCultCopySuper.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, ecGroupCopy);
                };
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                    //unbind entity child collection with child controller merged items
                    Entities.EcGroupCopy.ecCultCopyCollection = null; //(ecGroupCopy, func) => { }
                    //unbind entity child collection with child controller merged items
                    Entities.EcGroupCopy.ecCultCopyCollection = null; //(ecGroupCopy, func) => { }
                    //unbind entity child collection with child controller merged items
                    Entities.EcGroupCopy.ecCultCopyCollection = null; //(ecGroupCopy, func) => { }
                });
            }
            ControllerGrpEcGroupCopyBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpEcGroupCopyBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpEcGroupCopy";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcGroupCopyBase.prototype, "HtmlDivId", {
                get: function () {
                    return "DecisionMaking_ControllerGrpEcGroupCopy";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcGroupCopyBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcGroupCopyBase.prototype.gridTitle = function () {
                return "BRE";
            };
            ControllerGrpEcGroupCopyBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpEcGroupCopyBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'name', displayName: 'getALString("Name", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcGroupCopy_itm__name' data-ng-model='row.entity.name' data-np-ui-model='row.entity._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;name&quot;)' data-np-required='true' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'description', displayName: 'getALString("Description", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcGroupCopy_itm__description' data-ng-model='row.entity.description' data-np-ui-model='row.entity._description' data-ng-change='markEntityAsUpdated(row.entity,&quot;description&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cropLevel', displayName: 'getALString("Crop Level", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcGroupCopy_itm__cropLevel' data-ng-model='row.entity.cropLevel' data-np-ui-model='row.entity._cropLevel' data-ng-change='markEntityAsUpdated(row.entity,&quot;cropLevel&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Crop\"), value:0}, {label:getALString(\"Land Cover\"), value:1}, {label:getALString(\"Crop to Land Cover\"), value:2}]' data-ng-disabled='true' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpEcGroupCopyBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcGroupCopyBase.prototype, "modelGrpEcGroupCopy", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcGroupCopyBase.prototype.getEntityName = function () {
                return "EcGroupCopy";
            };
            ControllerGrpEcGroupCopyBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerGrpEcGroupCopyBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.EcGroupCopy.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.demaId = _this.Parent;
                    }
                    else {
                        x.demaId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpEcGroupCopyBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcGroupCopy.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcGroupCopyBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpDema.controller.isEntityLocked(cur.demaId, lockKind);
            };
            ControllerGrpEcGroupCopyBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpEcGroupCopyBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcGroupCopy/findAllByCriteriaRange_DecisionMakingGrpEcGroupCopy_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpEcGroupCopyBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcGroupCopy/findAllByCriteriaRange_DecisionMakingGrpEcGroupCopy";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpEcGroupCopyBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcGroupCopy/findAllByCriteriaRange_DecisionMakingGrpEcGroupCopy_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpEcGroupCopyBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.demaId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "EcGroupCopy/findAllByCriteriaRange_DecisionMakingGrpEcGroupCopy";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['demaId_demaId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpEcGroupCopyBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.demaId === undefined)
                    return false;
                return x.demaId.getKey() === this.Parent.getKey();
            };
            ControllerGrpEcGroupCopyBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpEcGroupCopyBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.demaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcGroupCopyBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpDema.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcGroupCopyBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcGroupCopyBase.prototype, "demaId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpDema.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcGroupCopyBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.EcGroupCopy(
                /*ecgcId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*description:string*/ null, 
                /*name:string*/ null, 
                /*rowVersion:number*/ null, 
                /*recordtype:number*/ null, 
                /*cropLevel:number*/ null, 
                /*demaId:Entities.DecisionMaking*/ null);
                return ret;
            };
            ControllerGrpEcGroupCopyBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.demaId = self.demaId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpEcGroupCopyBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpEcGroupCopyBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                var _this = this;
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.EcGroupCopy.Create();
                ret.updateInstance(src);
                ret.ecgcId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                    _this.modelGrpEcGroupCopy.modelGrpEcCultCopy.controller.cloneAllEntitiesUnderParent(src, ret);
                    _this.modelGrpEcGroupCopy.modelEcCultCopyCover.controller.cloneAllEntitiesUnderParent(src, ret);
                    _this.modelGrpEcGroupCopy.modelEcCultCopySuper.controller.cloneAllEntitiesUnderParent(src, ret);
                }, 1);
                return ret;
            };
            ControllerGrpEcGroupCopyBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (ecGroupCopyList) {
                    ecGroupCopyList.forEach(function (ecGroupCopy) {
                        var ecGroupCopyCloned = _this.cloneEntity_internal(ecGroupCopy, true);
                        ecGroupCopyCloned.demaId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpEcGroupCopyBase.prototype.deleteNewEntitiesUnderParent = function (decisionMaking) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var ecGroupCopy = change.a;
                    if (decisionMaking.getKey() === ecGroupCopy.demaId.getKey()) {
                        toBeDeleted.push(ecGroupCopy);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                    self.modelGrpEcGroupCopy.modelGrpEcCultCopy.controller.deleteNewEntitiesUnderParent(x);
                    self.modelGrpEcGroupCopy.modelEcCultCopyCover.controller.deleteNewEntitiesUnderParent(x);
                    self.modelGrpEcGroupCopy.modelEcCultCopySuper.controller.deleteNewEntitiesUnderParent(x);
                });
            };
            ControllerGrpEcGroupCopyBase.prototype.deleteAllEntitiesUnderParent = function (decisionMaking, afterDeleteAction) {
                var self = this;
                function deleteEcGroupCopyList(ecGroupCopyList) {
                    if (ecGroupCopyList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = ecGroupCopyList[0];
                        var tail = ecGroupCopyList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteEcGroupCopyList(tail);
                        });
                    }
                }
                this.getMergedItems(function (ecGroupCopyList) {
                    deleteEcGroupCopyList(ecGroupCopyList);
                }, false, decisionMaking);
            };
            Object.defineProperty(ControllerGrpEcGroupCopyBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [
                            self.modelGrpEcGroupCopy.modelGrpEcCultCopy.controller,
                            self.modelGrpEcGroupCopy.modelEcCultCopyCover.controller,
                            self.modelGrpEcGroupCopy.modelEcCultCopySuper.controller
                        ];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcGroupCopyBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                var _this = this;
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    self.modelGrpEcGroupCopy.modelGrpEcCultCopy.controller.deleteAllEntitiesUnderParent(ent, function () {
                        self.modelGrpEcGroupCopy.modelEcCultCopyCover.controller.deleteAllEntitiesUnderParent(ent, function () {
                            self.modelGrpEcGroupCopy.modelEcCultCopySuper.controller.deleteAllEntitiesUnderParent(ent, function () {
                                _super.prototype.deleteEntity.call(_this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                            });
                        });
                    });
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        self.modelGrpEcGroupCopy.modelGrpEcCultCopy.controller.deleteNewEntitiesUnderParent(ent);
                        self.modelGrpEcGroupCopy.modelEcCultCopyCover.controller.deleteNewEntitiesUnderParent(ent);
                        self.modelGrpEcGroupCopy.modelEcCultCopySuper.controller.deleteNewEntitiesUnderParent(ent);
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpEcGroupCopyBase.prototype.GrpEcGroupCopy_0_disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpEcGroupCopyBase.prototype.GrpEcGroupCopy_0_invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = this.isEcGroupChildInv();
                return programmerVal;
            };
            ControllerGrpEcGroupCopyBase.prototype.isEcGroupChildInv = function () {
                console.warn("Unimplemented function isEcGroupChildInv()");
                return false;
            };
            ControllerGrpEcGroupCopyBase.prototype.GrpEcGroupCopy_1_disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpEcGroupCopyBase.prototype.GrpEcGroupCopy_1_invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = this.isEcGroupCoverChildInv();
                return programmerVal;
            };
            ControllerGrpEcGroupCopyBase.prototype.isEcGroupCoverChildInv = function () {
                console.warn("Unimplemented function isEcGroupCoverChildInv()");
                return false;
            };
            ControllerGrpEcGroupCopyBase.prototype.GrpEcGroupCopy_2_disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpEcGroupCopyBase.prototype.GrpEcGroupCopy_2_invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = this.isEcGroupSuperChildInv();
                return programmerVal;
            };
            ControllerGrpEcGroupCopyBase.prototype.isEcGroupSuperChildInv = function () {
                console.warn("Unimplemented function isEcGroupSuperChildInv()");
                return false;
            };
            ControllerGrpEcGroupCopyBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController.GrpDema_0_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpEcGroupCopyBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (true)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpEcGroupCopyBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerGrpEcGroupCopyBase.prototype._deleteIsDisabled = function (ecGroupCopy) {
                return true;
            };
            ControllerGrpEcGroupCopyBase.prototype.child_group_GrpEcCultCopy_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpEcCultCopy === undefined) || (self.model.modelGrpEcCultCopy.controller === undefined))
                    return false;
                return self.model.modelGrpEcCultCopy.controller._isInvisible();
            };
            ControllerGrpEcGroupCopyBase.prototype.child_group_EcCultCopyCover_isInvisible = function () {
                var self = this;
                if ((self.model.modelEcCultCopyCover === undefined) || (self.model.modelEcCultCopyCover.controller === undefined))
                    return false;
                return self.model.modelEcCultCopyCover.controller._isInvisible();
            };
            ControllerGrpEcGroupCopyBase.prototype.child_group_EcCultCopySuper_isInvisible = function () {
                var self = this;
                if ((self.model.modelEcCultCopySuper === undefined) || (self.model.modelEcCultCopySuper.controller === undefined))
                    return false;
                return self.model.modelEcCultCopySuper.controller._isInvisible();
            };
            return ControllerGrpEcGroupCopyBase;
        })(Controllers.AbstractGroupTableController);
        DecisionMaking.ControllerGrpEcGroupCopyBase = ControllerGrpEcGroupCopyBase;
        // GROUP GrpEcCultCopy
        var ModelGrpEcCultCopyBase = (function (_super) {
            __extends(ModelGrpEcCultCopyBase, _super);
            function ModelGrpEcCultCopyBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "ecccId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].ecccId;
                },
                set: function (ecccId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].ecccId = ecccId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "_ecccId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._ecccId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "noneMatchDecision", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].noneMatchDecision;
                },
                set: function (noneMatchDecision_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].noneMatchDecision = noneMatchDecision_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "_noneMatchDecision", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._noneMatchDecision;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cultId;
                },
                set: function (cultId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cultId = cultId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "_cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cultId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "ecgcId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].ecgcId;
                },
                set: function (ecgcId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].ecgcId = ecgcId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "_ecgcId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._ecgcId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cotyId;
                },
                set: function (cotyId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cotyId = cotyId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "_cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cotyId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "sucaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].sucaId;
                },
                set: function (sucaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].sucaId = sucaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "_sucaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._sucaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultCopyBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpEcCultCopyBase;
        })(Controllers.AbstractGroupTableModel);
        DecisionMaking.ModelGrpEcCultCopyBase = ModelGrpEcCultCopyBase;
        var ControllerGrpEcCultCopyBase = (function (_super) {
            __extends(ControllerGrpEcCultCopyBase, _super);
            function ControllerGrpEcCultCopyBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpEcCultCopy = self.model;
                $scope.showLov_GrpEcCultCopy_itm__cultId =
                    function (ecCultCopy) {
                        $timeout(function () {
                            self.showLov_GrpEcCultCopy_itm__cultId(ecCultCopy);
                        }, 0);
                    };
                $scope.child_group_GrpEcCultDetailCopy_isInvisible =
                    function () {
                        return self.child_group_GrpEcCultDetailCopy_isInvisible();
                    };
                $scope.pageModel.modelGrpEcCultCopy = $scope.modelGrpEcCultCopy;
                $scope.clearBtnAction = function () {
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpEcGroupCopy.modelGrpEcCultCopy = $scope.modelGrpEcCultCopy;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpEcGroupCopy.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCultCopy[] , oldVisible:Entities.EcCultCopy[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                //bind entity child collection with child controller merged items
                Entities.EcCultCopy.ecCultDetailCopyCollection = function (ecCultCopy, func) {
                    _this.model.modelGrpEcCultDetailCopy.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, ecCultCopy);
                };
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                    //unbind entity child collection with child controller merged items
                    Entities.EcCultCopy.ecCultDetailCopyCollection = null; //(ecCultCopy, func) => { }
                });
            }
            ControllerGrpEcCultCopyBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpEcCultCopyBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpEcCultCopy";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultCopyBase.prototype, "HtmlDivId", {
                get: function () {
                    return "DecisionMaking_ControllerGrpEcCultCopy";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultCopyBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcCultCopyBase.prototype.gridTitle = function () {
                return "Cultivation Criteria";
            };
            ControllerGrpEcCultCopyBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpEcCultCopyBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'cultId.name', displayName: 'getALString("Crop", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '9%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <div  > \
                    <input data-np-source='row.entity.cultId.name' data-np-ui-model='row.entity._cultId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='GrpEcCultCopy_itm__cultId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_GrpEcCultCopy_itm__cultId(row.entity)'   data-ng-disabled=\"true\"   />  \
                    </div> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cultId.code', displayName: 'getALString("Crop Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '6%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCultCopy_itm__cultId_code' data-ng-model='row.entity.cultId.code' data-np-ui-model='row.entity.cultId._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultId.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cultId.cotyId.name', displayName: 'getALString("Land Cover", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '9%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCultCopy_itm__cultId_cotyId_name' data-ng-model='row.entity.cultId.cotyId.name' data-np-ui-model='row.entity.cultId.cotyId._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultId.cotyId.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpEcCultCopyBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultCopyBase.prototype, "modelGrpEcCultCopy", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcCultCopyBase.prototype.getEntityName = function () {
                return "EcCultCopy";
            };
            ControllerGrpEcCultCopyBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerGrpEcCultCopyBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.EcCultCopy.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.ecgcId = _this.Parent;
                    }
                    else {
                        x.ecgcId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpEcCultCopyBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcCultCopy.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcCultCopyBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpEcGroupCopy.controller.isEntityLocked(cur.ecgcId, lockKind);
            };
            ControllerGrpEcCultCopyBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpEcCultCopyBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingGrpEcCultCopy_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpEcCultCopyBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingGrpEcCultCopy";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpEcCultCopyBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingGrpEcCultCopy_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpEcCultCopyBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.ecgcId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingGrpEcCultCopy";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['ecgcId_ecgcId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpEcCultCopyBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.ecgcId === undefined)
                    return false;
                return x.ecgcId.getKey() === this.Parent.getKey();
            };
            ControllerGrpEcCultCopyBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpEcCultCopyBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.ecgcId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultCopyBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcGroupCopy.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultCopyBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultCopyBase.prototype, "ecgcId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcGroupCopy.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcCultCopyBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.EcCultCopy(
                /*ecccId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*noneMatchDecision:number*/ null, 
                /*rowVersion:number*/ null, 
                /*cultId:Entities.Cultivation*/ null, 
                /*ecgcId:Entities.EcGroupCopy*/ null, 
                /*cotyId:Entities.CoverType*/ null, 
                /*sucaId:Entities.SuperClas*/ null);
                return ret;
            };
            ControllerGrpEcCultCopyBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.ecgcId = self.ecgcId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpEcCultCopyBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpEcCultCopyBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                var _this = this;
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.EcCultCopy.Create();
                ret.updateInstance(src);
                ret.ecccId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                    _this.modelGrpEcCultCopy.modelGrpEcCultDetailCopy.controller.cloneAllEntitiesUnderParent(src, ret);
                }, 1);
                return ret;
            };
            ControllerGrpEcCultCopyBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (ecCultCopyList) {
                    ecCultCopyList.forEach(function (ecCultCopy) {
                        var ecCultCopyCloned = _this.cloneEntity_internal(ecCultCopy, true);
                        ecCultCopyCloned.ecgcId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpEcCultCopyBase.prototype.deleteNewEntitiesUnderParent = function (ecGroupCopy) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var ecCultCopy = change.a;
                    if (ecGroupCopy.getKey() === ecCultCopy.ecgcId.getKey()) {
                        toBeDeleted.push(ecCultCopy);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                    self.modelGrpEcCultCopy.modelGrpEcCultDetailCopy.controller.deleteNewEntitiesUnderParent(x);
                });
            };
            ControllerGrpEcCultCopyBase.prototype.deleteAllEntitiesUnderParent = function (ecGroupCopy, afterDeleteAction) {
                var self = this;
                function deleteEcCultCopyList(ecCultCopyList) {
                    if (ecCultCopyList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = ecCultCopyList[0];
                        var tail = ecCultCopyList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteEcCultCopyList(tail);
                        });
                    }
                }
                this.getMergedItems(function (ecCultCopyList) {
                    deleteEcCultCopyList(ecCultCopyList);
                }, false, ecGroupCopy);
            };
            Object.defineProperty(ControllerGrpEcCultCopyBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [
                            self.modelGrpEcCultCopy.modelGrpEcCultDetailCopy.controller
                        ];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcCultCopyBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                var _this = this;
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    self.modelGrpEcCultCopy.modelGrpEcCultDetailCopy.controller.deleteAllEntitiesUnderParent(ent, function () {
                        _super.prototype.deleteEntity.call(_this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                    });
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        self.modelGrpEcCultCopy.modelGrpEcCultDetailCopy.controller.deleteNewEntitiesUnderParent(ent);
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpEcCultCopyBase.prototype.showLov_GrpEcCultCopy_itm__cultId = function (ecCultCopy) {
                var self = this;
                if (ecCultCopy === undefined)
                    return;
                var uimodel = ecCultCopy._cultId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Cultivation';
                dialogOptions.previousModel = self.cachedLovModel_GrpEcCultCopy_itm__cultId;
                dialogOptions.className = "ControllerLovCultivation";
                dialogOptions.onSelect = function (selectedEntity) {
                    var cultivation1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (false && isVoid(cultivation1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(cultivation1) && cultivation1.isEqual(ecCultCopy.cultId))
                        return;
                    ecCultCopy.cultId = cultivation1;
                    self.markEntityAsUpdated(ecCultCopy, 'GrpEcCultCopy_itm__cultId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpEcCultCopy_itm__cultId = lovModel;
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                dialogOptions.shownCols['code'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/Cultivation.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpEcCultCopyBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController.GrpEcGroupCopy_0_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpEcCultCopyBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController.GrpEcGroupCopy_0_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpEcCultCopyBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerGrpEcCultCopyBase.prototype._deleteIsDisabled = function (ecCultCopy) {
                return true;
            };
            ControllerGrpEcCultCopyBase.prototype.child_group_GrpEcCultDetailCopy_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpEcCultDetailCopy === undefined) || (self.model.modelGrpEcCultDetailCopy.controller === undefined))
                    return false;
                return self.model.modelGrpEcCultDetailCopy.controller._isInvisible();
            };
            return ControllerGrpEcCultCopyBase;
        })(Controllers.AbstractGroupTableController);
        DecisionMaking.ControllerGrpEcCultCopyBase = ControllerGrpEcCultCopyBase;
        // GROUP GrpEcCultDetailCopy
        var ModelGrpEcCultDetailCopyBase = (function (_super) {
            __extends(ModelGrpEcCultDetailCopyBase, _super);
            function ModelGrpEcCultDetailCopyBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "ecdcId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].ecdcId;
                },
                set: function (ecdcId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].ecdcId = ecdcId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_ecdcId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._ecdcId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "orderingNumber", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].orderingNumber;
                },
                set: function (orderingNumber_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].orderingNumber = orderingNumber_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_orderingNumber", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._orderingNumber;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "agreesDeclar", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].agreesDeclar;
                },
                set: function (agreesDeclar_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].agreesDeclar = agreesDeclar_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_agreesDeclar", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._agreesDeclar;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "comparisonOper", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].comparisonOper;
                },
                set: function (comparisonOper_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].comparisonOper = comparisonOper_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_comparisonOper", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._comparisonOper;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "probabThres", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probabThres;
                },
                set: function (probabThres_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probabThres = probabThres_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_probabThres", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probabThres;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "decisionLight", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].decisionLight;
                },
                set: function (decisionLight_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].decisionLight = decisionLight_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_decisionLight", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._decisionLight;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "agreesDeclar2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].agreesDeclar2;
                },
                set: function (agreesDeclar2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].agreesDeclar2 = agreesDeclar2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_agreesDeclar2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._agreesDeclar2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "comparisonOper2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].comparisonOper2;
                },
                set: function (comparisonOper2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].comparisonOper2 = comparisonOper2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_comparisonOper2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._comparisonOper2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "probabThres2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probabThres2;
                },
                set: function (probabThres2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probabThres2 = probabThres2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_probabThres2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probabThres2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "probabThresSum", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probabThresSum;
                },
                set: function (probabThresSum_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probabThresSum = probabThresSum_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_probabThresSum", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probabThresSum;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "comparisonOper3", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].comparisonOper3;
                },
                set: function (comparisonOper3_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].comparisonOper3 = comparisonOper3_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_comparisonOper3", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._comparisonOper3;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "ecccId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].ecccId;
                },
                set: function (ecccId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].ecccId = ecccId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_ecccId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._ecccId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpEcCultDetailCopyBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpEcCultDetailCopyBase;
        })(Controllers.AbstractGroupTableModel);
        DecisionMaking.ModelGrpEcCultDetailCopyBase = ModelGrpEcCultDetailCopyBase;
        var ControllerGrpEcCultDetailCopyBase = (function (_super) {
            __extends(ControllerGrpEcCultDetailCopyBase, _super);
            function ControllerGrpEcCultDetailCopyBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 3 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpEcCultDetailCopy = self.model;
                $scope.pageModel.modelGrpEcCultDetailCopy = $scope.modelGrpEcCultDetailCopy;
                $scope.clearBtnAction = function () {
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpEcCultCopy.modelGrpEcCultDetailCopy = $scope.modelGrpEcCultDetailCopy;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpEcCultCopy.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCultDetailCopy[] , oldVisible:Entities.EcCultDetailCopy[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            ControllerGrpEcCultDetailCopyBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpEcCultDetailCopyBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpEcCultDetailCopy";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultDetailCopyBase.prototype, "HtmlDivId", {
                get: function () {
                    return "DecisionMaking_ControllerGrpEcCultDetailCopy";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultDetailCopyBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcCultDetailCopyBase.prototype.gridTitle = function () {
                return "Criteria";
            };
            ControllerGrpEcCultDetailCopyBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpEcCultDetailCopyBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'orderingNumber', displayName: 'getALString("Ordering \n No", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '5%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCultDetailCopy_itm__orderingNumber' data-ng-model='row.entity.orderingNumber' data-np-ui-model='row.entity._orderingNumber' data-ng-change='markEntityAsUpdated(row.entity,&quot;orderingNumber&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='0' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'agreesDeclar', displayName: 'getALString("1st \ Prediction ", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetailCopy_itm__agreesDeclar' data-ng-model='row.entity.agreesDeclar' data-np-ui-model='row.entity._agreesDeclar' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='true' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'comparisonOper', displayName: 'getALString("Comparison \n Operator 1st", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '11%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetailCopy_itm__comparisonOper' data-ng-model='row.entity.comparisonOper' data-np-ui-model='row.entity._comparisonOper' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='true' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'probabThres', displayName: 'getALString("Confidence \n Pred. 1st (%)", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCultDetailCopy_itm__probabThres' data-ng-model='row.entity.probabThres' data-np-ui-model='row.entity._probabThres' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'agreesDeclar2', displayName: 'getALString("2nd \ Prediction ", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetailCopy_itm__agreesDeclar2' data-ng-model='row.entity.agreesDeclar2' data-np-ui-model='row.entity._agreesDeclar2' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='true' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'comparisonOper2', displayName: 'getALString("Comparison \n Operator 2nd", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '11%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetailCopy_itm__comparisonOper2' data-ng-model='row.entity.comparisonOper2' data-np-ui-model='row.entity._comparisonOper2' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='true' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'probabThres2', displayName: 'getALString("Confidence \n Pred. 2nd (%)", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCultDetailCopy_itm__probabThres2' data-ng-model='row.entity.probabThres2' data-np-ui-model='row.entity._probabThres2' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres2&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'comparisonOper3', displayName: 'getALString("Comparison \n Operator 1st + 2nd", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '11%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetailCopy_itm__comparisonOper3' data-ng-model='row.entity.comparisonOper3' data-np-ui-model='row.entity._comparisonOper3' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper3&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='true' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'probabThresSum', displayName: 'getALString("Confidence \n Prediction \n 1st + 2nd (%)", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpEcCultDetailCopy_itm__probabThresSum' data-ng-model='row.entity.probabThresSum' data-np-ui-model='row.entity._probabThresSum' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThresSum&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'decisionLight', displayName: 'getALString("Traffic \n Light Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='GrpEcCultDetailCopy_itm__decisionLight' data-ng-model='row.entity.decisionLight' data-np-ui-model='row.entity._decisionLight' data-ng-change='markEntityAsUpdated(row.entity,&quot;decisionLight&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Green\"), value:1}, {label:getALString(\"Yellow\"), value:2}, {label:getALString(\"Red\"), value:3}, {label:getALString(\"Undefined\"), value:4}]' data-ng-disabled='true' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpEcCultDetailCopyBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultDetailCopyBase.prototype, "modelGrpEcCultDetailCopy", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcCultDetailCopyBase.prototype.getEntityName = function () {
                return "EcCultDetailCopy";
            };
            ControllerGrpEcCultDetailCopyBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerGrpEcCultDetailCopyBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.EcCultDetailCopy.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.ecccId = _this.Parent;
                    }
                    else {
                        x.ecccId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpEcCultDetailCopyBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcCultDetailCopy.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcCultDetailCopyBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpEcCultCopy.controller.isEntityLocked(cur.ecccId, lockKind);
            };
            ControllerGrpEcCultDetailCopyBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpEcCultDetailCopyBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingGrpEcCultDetailCopy_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpEcCultDetailCopyBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingGrpEcCultDetailCopy";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpEcCultDetailCopyBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingGrpEcCultDetailCopy_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpEcCultDetailCopyBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.ecccId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingGrpEcCultDetailCopy";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['ecccId_ecccId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpEcCultDetailCopyBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.ecccId === undefined)
                    return false;
                return x.ecccId.getKey() === this.Parent.getKey();
            };
            ControllerGrpEcCultDetailCopyBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpEcCultDetailCopyBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.ecccId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultDetailCopyBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcCultCopy.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultDetailCopyBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpEcCultDetailCopyBase.prototype, "ecccId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcCultCopy.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcCultDetailCopyBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.EcCultDetailCopy(
                /*ecdcId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*orderingNumber:number*/ null, 
                /*agreesDeclar:number*/ null, 
                /*comparisonOper:number*/ null, 
                /*probabThres:number*/ null, 
                /*decisionLight:number*/ null, 
                /*rowVersion:number*/ null, 
                /*agreesDeclar2:number*/ null, 
                /*comparisonOper2:number*/ null, 
                /*probabThres2:number*/ null, 
                /*probabThresSum:number*/ null, 
                /*comparisonOper3:number*/ null, 
                /*ecccId:Entities.EcCultCopy*/ null);
                this.getMergedItems(function (items) {
                    ret.orderingNumber = items.maxBy(function (i) { return i.orderingNumber; }, 0) + 1;
                }, false);
                return ret;
            };
            ControllerGrpEcCultDetailCopyBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.ecccId = self.ecccId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpEcCultDetailCopyBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpEcCultDetailCopyBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.EcCultDetailCopy.Create();
                ret.updateInstance(src);
                ret.ecdcId = tmpId;
                if (!calledByParent) {
                    this.getMergedItems(function (items) {
                        ret.orderingNumber = items.maxBy(function (i) { return i.orderingNumber; }, 0) + 1;
                    }, false);
                }
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            ControllerGrpEcCultDetailCopyBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (ecCultDetailCopyList) {
                    ecCultDetailCopyList.forEach(function (ecCultDetailCopy) {
                        var ecCultDetailCopyCloned = _this.cloneEntity_internal(ecCultDetailCopy, true);
                        ecCultDetailCopyCloned.ecccId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpEcCultDetailCopyBase.prototype.deleteNewEntitiesUnderParent = function (ecCultCopy) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var ecCultDetailCopy = change.a;
                    if (ecCultCopy.getKey() === ecCultDetailCopy.ecccId.getKey()) {
                        toBeDeleted.push(ecCultDetailCopy);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                });
            };
            ControllerGrpEcCultDetailCopyBase.prototype.deleteAllEntitiesUnderParent = function (ecCultCopy, afterDeleteAction) {
                var self = this;
                function deleteEcCultDetailCopyList(ecCultDetailCopyList) {
                    if (ecCultDetailCopyList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = ecCultDetailCopyList[0];
                        var tail = ecCultDetailCopyList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteEcCultDetailCopyList(tail);
                        });
                    }
                }
                this.getMergedItems(function (ecCultDetailCopyList) {
                    deleteEcCultDetailCopyList(ecCultDetailCopyList);
                }, false, ecCultCopy);
            };
            Object.defineProperty(ControllerGrpEcCultDetailCopyBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpEcCultDetailCopyBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpEcCultDetailCopyBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpEcCultDetailCopyBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpEcCultDetailCopyBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerGrpEcCultDetailCopyBase.prototype._deleteIsDisabled = function (ecCultDetailCopy) {
                return true;
            };
            return ControllerGrpEcCultDetailCopyBase;
        })(Controllers.AbstractGroupTableController);
        DecisionMaking.ControllerGrpEcCultDetailCopyBase = ControllerGrpEcCultDetailCopyBase;
        // GROUP EcCultCopyCover
        var ModelEcCultCopyCoverBase = (function (_super) {
            __extends(ModelEcCultCopyCoverBase, _super);
            function ModelEcCultCopyCoverBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "ecccId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].ecccId;
                },
                set: function (ecccId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].ecccId = ecccId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "_ecccId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._ecccId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "noneMatchDecision", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].noneMatchDecision;
                },
                set: function (noneMatchDecision_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].noneMatchDecision = noneMatchDecision_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "_noneMatchDecision", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._noneMatchDecision;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cultId;
                },
                set: function (cultId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cultId = cultId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "_cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cultId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "ecgcId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].ecgcId;
                },
                set: function (ecgcId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].ecgcId = ecgcId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "_ecgcId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._ecgcId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cotyId;
                },
                set: function (cotyId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cotyId = cotyId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "_cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cotyId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "sucaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].sucaId;
                },
                set: function (sucaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].sucaId = sucaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "_sucaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._sucaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopyCoverBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelEcCultCopyCoverBase;
        })(Controllers.AbstractGroupTableModel);
        DecisionMaking.ModelEcCultCopyCoverBase = ModelEcCultCopyCoverBase;
        var ControllerEcCultCopyCoverBase = (function (_super) {
            __extends(ControllerEcCultCopyCoverBase, _super);
            function ControllerEcCultCopyCoverBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelEcCultCopyCover = self.model;
                $scope.showLov_EcCultCopyCover_itm__cotyId =
                    function (ecCultCopy) {
                        $timeout(function () {
                            self.showLov_EcCultCopyCover_itm__cotyId(ecCultCopy);
                        }, 0);
                    };
                $scope.child_group_EcCultDetailCopyCover_isInvisible =
                    function () {
                        return self.child_group_EcCultDetailCopyCover_isInvisible();
                    };
                $scope.pageModel.modelEcCultCopyCover = $scope.modelEcCultCopyCover;
                $scope.clearBtnAction = function () {
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpEcGroupCopy.modelEcCultCopyCover = $scope.modelEcCultCopyCover;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpEcGroupCopy.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCultCopy[] , oldVisible:Entities.EcCultCopy[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                //bind entity child collection with child controller merged items
                Entities.EcCultCopy.ecCultDetailCopyCollection = function (ecCultCopy, func) {
                    _this.model.modelEcCultDetailCopyCover.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, ecCultCopy);
                };
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                    //unbind entity child collection with child controller merged items
                    Entities.EcCultCopy.ecCultDetailCopyCollection = null; //(ecCultCopy, func) => { }
                });
            }
            ControllerEcCultCopyCoverBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerEcCultCopyCoverBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerEcCultCopyCover";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultCopyCoverBase.prototype, "HtmlDivId", {
                get: function () {
                    return "DecisionMaking_ControllerEcCultCopyCover";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultCopyCoverBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultCopyCoverBase.prototype.gridTitle = function () {
                return "Land Cover Criteria";
            };
            ControllerEcCultCopyCoverBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerEcCultCopyCoverBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'cotyId.name', displayName: 'getALString("Land Cover", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '9%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <div  > \
                    <input data-np-source='row.entity.cotyId.name' data-np-ui-model='row.entity._cotyId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='EcCultCopyCover_itm__cotyId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_EcCultCopyCover_itm__cotyId(row.entity)'   data-ng-disabled=\"true\"   />  \
                    </div> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cotyId.code', displayName: 'getALString("Land Cover Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '7%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultCopyCover_itm__cotyId_code' data-ng-model='row.entity.cotyId.code' data-np-ui-model='row.entity.cotyId._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cotyId.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerEcCultCopyCoverBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultCopyCoverBase.prototype, "modelEcCultCopyCover", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultCopyCoverBase.prototype.getEntityName = function () {
                return "EcCultCopy";
            };
            ControllerEcCultCopyCoverBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerEcCultCopyCoverBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.EcCultCopy.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.ecgcId = _this.Parent;
                    }
                    else {
                        x.ecgcId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerEcCultCopyCoverBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelEcCultCopyCover.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultCopyCoverBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpEcGroupCopy.controller.isEntityLocked(cur.ecgcId, lockKind);
            };
            ControllerEcCultCopyCoverBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerEcCultCopyCoverBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingEcCultCopyCover_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultCopyCoverBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingEcCultCopyCover";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultCopyCoverBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingEcCultCopyCover_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultCopyCoverBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.ecgcId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingEcCultCopyCover";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['ecgcId_ecgcId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerEcCultCopyCoverBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.ecgcId === undefined)
                    return false;
                return x.ecgcId.getKey() === this.Parent.getKey();
            };
            ControllerEcCultCopyCoverBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerEcCultCopyCoverBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.ecgcId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultCopyCoverBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcGroupCopy.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultCopyCoverBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultCopyCoverBase.prototype, "ecgcId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcGroupCopy.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultCopyCoverBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.EcCultCopy(
                /*ecccId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*noneMatchDecision:number*/ null, 
                /*rowVersion:number*/ null, 
                /*cultId:Entities.Cultivation*/ null, 
                /*ecgcId:Entities.EcGroupCopy*/ null, 
                /*cotyId:Entities.CoverType*/ null, 
                /*sucaId:Entities.SuperClas*/ null);
                return ret;
            };
            ControllerEcCultCopyCoverBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.ecgcId = self.ecgcId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerEcCultCopyCoverBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerEcCultCopyCoverBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                var _this = this;
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.EcCultCopy.Create();
                ret.updateInstance(src);
                ret.ecccId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                    _this.modelEcCultCopyCover.modelEcCultDetailCopyCover.controller.cloneAllEntitiesUnderParent(src, ret);
                }, 1);
                return ret;
            };
            ControllerEcCultCopyCoverBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (ecCultCopyList) {
                    ecCultCopyList.forEach(function (ecCultCopy) {
                        var ecCultCopyCloned = _this.cloneEntity_internal(ecCultCopy, true);
                        ecCultCopyCloned.ecgcId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerEcCultCopyCoverBase.prototype.deleteNewEntitiesUnderParent = function (ecGroupCopy) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var ecCultCopy = change.a;
                    if (ecGroupCopy.getKey() === ecCultCopy.ecgcId.getKey()) {
                        toBeDeleted.push(ecCultCopy);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                    self.modelEcCultCopyCover.modelEcCultDetailCopyCover.controller.deleteNewEntitiesUnderParent(x);
                });
            };
            ControllerEcCultCopyCoverBase.prototype.deleteAllEntitiesUnderParent = function (ecGroupCopy, afterDeleteAction) {
                var self = this;
                function deleteEcCultCopyList(ecCultCopyList) {
                    if (ecCultCopyList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = ecCultCopyList[0];
                        var tail = ecCultCopyList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteEcCultCopyList(tail);
                        });
                    }
                }
                this.getMergedItems(function (ecCultCopyList) {
                    deleteEcCultCopyList(ecCultCopyList);
                }, false, ecGroupCopy);
            };
            Object.defineProperty(ControllerEcCultCopyCoverBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [
                            self.modelEcCultCopyCover.modelEcCultDetailCopyCover.controller
                        ];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultCopyCoverBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                var _this = this;
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    self.modelEcCultCopyCover.modelEcCultDetailCopyCover.controller.deleteAllEntitiesUnderParent(ent, function () {
                        _super.prototype.deleteEntity.call(_this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                    });
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        self.modelEcCultCopyCover.modelEcCultDetailCopyCover.controller.deleteNewEntitiesUnderParent(ent);
                        afterDeleteAction();
                    });
                }
            };
            ControllerEcCultCopyCoverBase.prototype.showLov_EcCultCopyCover_itm__cotyId = function (ecCultCopy) {
                var self = this;
                if (ecCultCopy === undefined)
                    return;
                var uimodel = ecCultCopy._cotyId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Land Cover';
                dialogOptions.previousModel = self.cachedLovModel_EcCultCopyCover_itm__cotyId;
                dialogOptions.className = "ControllerLovCoverType";
                dialogOptions.onSelect = function (selectedEntity) {
                    var coverType1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (false && isVoid(coverType1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(coverType1) && coverType1.isEqual(ecCultCopy.cotyId))
                        return;
                    ecCultCopy.cotyId = coverType1;
                    self.markEntityAsUpdated(ecCultCopy, 'EcCultCopyCover_itm__cotyId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_EcCultCopyCover_itm__cotyId = lovModel;
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_code)) {
                        paramData['fsch_CoverTypeLov_code'] = lovModel.fsch_CoverTypeLov_code;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CoverTypeLov_name)) {
                        paramData['fsch_CoverTypeLov_name'] = lovModel.fsch_CoverTypeLov_name;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                dialogOptions.shownCols['code'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/CoverType.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerEcCultCopyCoverBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController.GrpEcGroupCopy_1_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerEcCultCopyCoverBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController.GrpEcGroupCopy_1_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerEcCultCopyCoverBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerEcCultCopyCoverBase.prototype._deleteIsDisabled = function (ecCultCopy) {
                return true;
            };
            ControllerEcCultCopyCoverBase.prototype.child_group_EcCultDetailCopyCover_isInvisible = function () {
                var self = this;
                if ((self.model.modelEcCultDetailCopyCover === undefined) || (self.model.modelEcCultDetailCopyCover.controller === undefined))
                    return false;
                return self.model.modelEcCultDetailCopyCover.controller._isInvisible();
            };
            return ControllerEcCultCopyCoverBase;
        })(Controllers.AbstractGroupTableController);
        DecisionMaking.ControllerEcCultCopyCoverBase = ControllerEcCultCopyCoverBase;
        // GROUP EcCultDetailCopyCover
        var ModelEcCultDetailCopyCoverBase = (function (_super) {
            __extends(ModelEcCultDetailCopyCoverBase, _super);
            function ModelEcCultDetailCopyCoverBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "ecdcId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].ecdcId;
                },
                set: function (ecdcId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].ecdcId = ecdcId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_ecdcId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._ecdcId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "orderingNumber", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].orderingNumber;
                },
                set: function (orderingNumber_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].orderingNumber = orderingNumber_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_orderingNumber", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._orderingNumber;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "agreesDeclar", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].agreesDeclar;
                },
                set: function (agreesDeclar_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].agreesDeclar = agreesDeclar_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_agreesDeclar", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._agreesDeclar;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "comparisonOper", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].comparisonOper;
                },
                set: function (comparisonOper_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].comparisonOper = comparisonOper_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_comparisonOper", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._comparisonOper;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "probabThres", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probabThres;
                },
                set: function (probabThres_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probabThres = probabThres_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_probabThres", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probabThres;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "decisionLight", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].decisionLight;
                },
                set: function (decisionLight_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].decisionLight = decisionLight_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_decisionLight", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._decisionLight;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "agreesDeclar2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].agreesDeclar2;
                },
                set: function (agreesDeclar2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].agreesDeclar2 = agreesDeclar2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_agreesDeclar2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._agreesDeclar2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "comparisonOper2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].comparisonOper2;
                },
                set: function (comparisonOper2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].comparisonOper2 = comparisonOper2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_comparisonOper2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._comparisonOper2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "probabThres2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probabThres2;
                },
                set: function (probabThres2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probabThres2 = probabThres2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_probabThres2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probabThres2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "probabThresSum", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probabThresSum;
                },
                set: function (probabThresSum_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probabThresSum = probabThresSum_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_probabThresSum", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probabThresSum;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "comparisonOper3", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].comparisonOper3;
                },
                set: function (comparisonOper3_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].comparisonOper3 = comparisonOper3_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_comparisonOper3", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._comparisonOper3;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "ecccId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].ecccId;
                },
                set: function (ecccId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].ecccId = ecccId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_ecccId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._ecccId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopyCoverBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelEcCultDetailCopyCoverBase;
        })(Controllers.AbstractGroupTableModel);
        DecisionMaking.ModelEcCultDetailCopyCoverBase = ModelEcCultDetailCopyCoverBase;
        var ControllerEcCultDetailCopyCoverBase = (function (_super) {
            __extends(ControllerEcCultDetailCopyCoverBase, _super);
            function ControllerEcCultDetailCopyCoverBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 3 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelEcCultDetailCopyCover = self.model;
                $scope.pageModel.modelEcCultDetailCopyCover = $scope.modelEcCultDetailCopyCover;
                $scope.clearBtnAction = function () {
                    self.updateGrid(0, false, true);
                };
                $scope.modelEcCultCopyCover.modelEcCultDetailCopyCover = $scope.modelEcCultDetailCopyCover;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelEcCultCopyCover.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCultDetailCopy[] , oldVisible:Entities.EcCultDetailCopy[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            ControllerEcCultDetailCopyCoverBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerEcCultDetailCopyCoverBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerEcCultDetailCopyCover";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailCopyCoverBase.prototype, "HtmlDivId", {
                get: function () {
                    return "DecisionMaking_ControllerEcCultDetailCopyCover";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailCopyCoverBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultDetailCopyCoverBase.prototype.gridTitle = function () {
                return "Criteria";
            };
            ControllerEcCultDetailCopyCoverBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerEcCultDetailCopyCoverBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'orderingNumber', displayName: 'getALString("Ordering \n No", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '5%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCopyCover_itm__orderingNumber' data-ng-model='row.entity.orderingNumber' data-np-ui-model='row.entity._orderingNumber' data-ng-change='markEntityAsUpdated(row.entity,&quot;orderingNumber&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='0' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'agreesDeclar', displayName: 'getALString("1st \ Prediction ", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopyCover_itm__agreesDeclar' data-ng-model='row.entity.agreesDeclar' data-np-ui-model='row.entity._agreesDeclar' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='true' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'comparisonOper', displayName: 'getALString("Comparison \n Operator 1st", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '11%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopyCover_itm__comparisonOper' data-ng-model='row.entity.comparisonOper' data-np-ui-model='row.entity._comparisonOper' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='true' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'probabThres', displayName: 'getALString("Confidence \n Pred. 1st (%)", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCopyCover_itm__probabThres' data-ng-model='row.entity.probabThres' data-np-ui-model='row.entity._probabThres' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'agreesDeclar2', displayName: 'getALString("2nd \ Prediction ", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopyCover_itm__agreesDeclar2' data-ng-model='row.entity.agreesDeclar2' data-np-ui-model='row.entity._agreesDeclar2' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='true' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'comparisonOper2', displayName: 'getALString("Comparison \n Operator 2nd", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '11%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopyCover_itm__comparisonOper2' data-ng-model='row.entity.comparisonOper2' data-np-ui-model='row.entity._comparisonOper2' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='true' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'probabThres2', displayName: 'getALString("Confidence \n Pred. 2nd (%)", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCopyCover_itm__probabThres2' data-ng-model='row.entity.probabThres2' data-np-ui-model='row.entity._probabThres2' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres2&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'comparisonOper3', displayName: 'getALString("Comparison \n Operator 1st + 2nd", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '11%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopyCover_itm__comparisonOper3' data-ng-model='row.entity.comparisonOper3' data-np-ui-model='row.entity._comparisonOper3' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper3&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='true' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'probabThresSum', displayName: 'getALString("Confidence \n Prediction \n 1st + 2nd (%)", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCopyCover_itm__probabThresSum' data-ng-model='row.entity.probabThresSum' data-np-ui-model='row.entity._probabThresSum' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThresSum&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'decisionLight', displayName: 'getALString("Traffic \n Light Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopyCover_itm__decisionLight' data-ng-model='row.entity.decisionLight' data-np-ui-model='row.entity._decisionLight' data-ng-change='markEntityAsUpdated(row.entity,&quot;decisionLight&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Green\"), value:1}, {label:getALString(\"Yellow\"), value:2}, {label:getALString(\"Red\"), value:3}, {label:getALString(\"Undefined\"), value:4}]' data-ng-disabled='true' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerEcCultDetailCopyCoverBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailCopyCoverBase.prototype, "modelEcCultDetailCopyCover", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultDetailCopyCoverBase.prototype.getEntityName = function () {
                return "EcCultDetailCopy";
            };
            ControllerEcCultDetailCopyCoverBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerEcCultDetailCopyCoverBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.EcCultDetailCopy.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.ecccId = _this.Parent;
                    }
                    else {
                        x.ecccId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerEcCultDetailCopyCoverBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelEcCultDetailCopyCover.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultDetailCopyCoverBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelEcCultCopyCover.controller.isEntityLocked(cur.ecccId, lockKind);
            };
            ControllerEcCultDetailCopyCoverBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerEcCultDetailCopyCoverBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingEcCultDetailCopyCover_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultDetailCopyCoverBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingEcCultDetailCopyCover";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultDetailCopyCoverBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingEcCultDetailCopyCover_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultDetailCopyCoverBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.ecccId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingEcCultDetailCopyCover";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['ecccId_ecccId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerEcCultDetailCopyCoverBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.ecccId === undefined)
                    return false;
                return x.ecccId.getKey() === this.Parent.getKey();
            };
            ControllerEcCultDetailCopyCoverBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerEcCultDetailCopyCoverBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.ecccId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailCopyCoverBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelEcCultCopyCover.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailCopyCoverBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailCopyCoverBase.prototype, "ecccId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelEcCultCopyCover.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultDetailCopyCoverBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.EcCultDetailCopy(
                /*ecdcId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*orderingNumber:number*/ null, 
                /*agreesDeclar:number*/ null, 
                /*comparisonOper:number*/ null, 
                /*probabThres:number*/ null, 
                /*decisionLight:number*/ null, 
                /*rowVersion:number*/ null, 
                /*agreesDeclar2:number*/ null, 
                /*comparisonOper2:number*/ null, 
                /*probabThres2:number*/ null, 
                /*probabThresSum:number*/ null, 
                /*comparisonOper3:number*/ null, 
                /*ecccId:Entities.EcCultCopy*/ null);
                this.getMergedItems(function (items) {
                    ret.orderingNumber = items.maxBy(function (i) { return i.orderingNumber; }, 0) + 1;
                }, false);
                return ret;
            };
            ControllerEcCultDetailCopyCoverBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.ecccId = self.ecccId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerEcCultDetailCopyCoverBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerEcCultDetailCopyCoverBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.EcCultDetailCopy.Create();
                ret.updateInstance(src);
                ret.ecdcId = tmpId;
                if (!calledByParent) {
                    this.getMergedItems(function (items) {
                        ret.orderingNumber = items.maxBy(function (i) { return i.orderingNumber; }, 0) + 1;
                    }, false);
                }
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            ControllerEcCultDetailCopyCoverBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (ecCultDetailCopyList) {
                    ecCultDetailCopyList.forEach(function (ecCultDetailCopy) {
                        var ecCultDetailCopyCloned = _this.cloneEntity_internal(ecCultDetailCopy, true);
                        ecCultDetailCopyCloned.ecccId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerEcCultDetailCopyCoverBase.prototype.deleteNewEntitiesUnderParent = function (ecCultCopy) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var ecCultDetailCopy = change.a;
                    if (ecCultCopy.getKey() === ecCultDetailCopy.ecccId.getKey()) {
                        toBeDeleted.push(ecCultDetailCopy);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                });
            };
            ControllerEcCultDetailCopyCoverBase.prototype.deleteAllEntitiesUnderParent = function (ecCultCopy, afterDeleteAction) {
                var self = this;
                function deleteEcCultDetailCopyList(ecCultDetailCopyList) {
                    if (ecCultDetailCopyList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = ecCultDetailCopyList[0];
                        var tail = ecCultDetailCopyList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteEcCultDetailCopyList(tail);
                        });
                    }
                }
                this.getMergedItems(function (ecCultDetailCopyList) {
                    deleteEcCultDetailCopyList(ecCultDetailCopyList);
                }, false, ecCultCopy);
            };
            Object.defineProperty(ControllerEcCultDetailCopyCoverBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultDetailCopyCoverBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerEcCultDetailCopyCoverBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerEcCultDetailCopyCoverBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerEcCultDetailCopyCoverBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerEcCultDetailCopyCoverBase.prototype._deleteIsDisabled = function (ecCultDetailCopy) {
                return true;
            };
            return ControllerEcCultDetailCopyCoverBase;
        })(Controllers.AbstractGroupTableController);
        DecisionMaking.ControllerEcCultDetailCopyCoverBase = ControllerEcCultDetailCopyCoverBase;
        // GROUP EcCultCopySuper
        var ModelEcCultCopySuperBase = (function (_super) {
            __extends(ModelEcCultCopySuperBase, _super);
            function ModelEcCultCopySuperBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "ecccId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].ecccId;
                },
                set: function (ecccId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].ecccId = ecccId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "_ecccId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._ecccId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "noneMatchDecision", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].noneMatchDecision;
                },
                set: function (noneMatchDecision_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].noneMatchDecision = noneMatchDecision_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "_noneMatchDecision", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._noneMatchDecision;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cultId;
                },
                set: function (cultId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cultId = cultId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "_cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cultId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "ecgcId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].ecgcId;
                },
                set: function (ecgcId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].ecgcId = ecgcId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "_ecgcId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._ecgcId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cotyId;
                },
                set: function (cotyId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cotyId = cotyId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "_cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cotyId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "sucaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].sucaId;
                },
                set: function (sucaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].sucaId = sucaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "_sucaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._sucaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultCopySuperBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelEcCultCopySuperBase;
        })(Controllers.AbstractGroupTableModel);
        DecisionMaking.ModelEcCultCopySuperBase = ModelEcCultCopySuperBase;
        var ControllerEcCultCopySuperBase = (function (_super) {
            __extends(ControllerEcCultCopySuperBase, _super);
            function ControllerEcCultCopySuperBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelEcCultCopySuper = self.model;
                $scope.showLov_EcCultCopySuper_itm__cultId =
                    function (ecCultCopy) {
                        $timeout(function () {
                            self.showLov_EcCultCopySuper_itm__cultId(ecCultCopy);
                        }, 0);
                    };
                $scope.child_group_EcCultDetailCopySuper_isInvisible =
                    function () {
                        return self.child_group_EcCultDetailCopySuper_isInvisible();
                    };
                $scope.pageModel.modelEcCultCopySuper = $scope.modelEcCultCopySuper;
                $scope.clearBtnAction = function () {
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpEcGroupCopy.modelEcCultCopySuper = $scope.modelEcCultCopySuper;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpEcGroupCopy.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCultCopy[] , oldVisible:Entities.EcCultCopy[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                //bind entity child collection with child controller merged items
                Entities.EcCultCopy.ecCultDetailCopyCollection = function (ecCultCopy, func) {
                    _this.model.modelEcCultDetailCopySuper.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, ecCultCopy);
                };
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                    //unbind entity child collection with child controller merged items
                    Entities.EcCultCopy.ecCultDetailCopyCollection = null; //(ecCultCopy, func) => { }
                });
            }
            ControllerEcCultCopySuperBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerEcCultCopySuperBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerEcCultCopySuper";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultCopySuperBase.prototype, "HtmlDivId", {
                get: function () {
                    return "DecisionMaking_ControllerEcCultCopySuper";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultCopySuperBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultCopySuperBase.prototype.gridTitle = function () {
                return "Crop to Land Cover Criteria";
            };
            ControllerEcCultCopySuperBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerEcCultCopySuperBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'cultId.name', displayName: 'getALString("Crop", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '9%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <div  > \
                    <input data-np-source='row.entity.cultId.name' data-np-ui-model='row.entity._cultId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='EcCultCopySuper_itm__cultId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_EcCultCopySuper_itm__cultId(row.entity)'   data-ng-disabled=\"true\"   />  \
                    </div> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cultId.code', displayName: 'getALString("Crop Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultCopySuper_itm__cultId_code' data-ng-model='row.entity.cultId.code' data-np-ui-model='row.entity.cultId._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultId.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cultId.cotyId.name', displayName: 'getALString("Cover Type", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '9%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultCopySuper_itm__cultId_cotyId_name' data-ng-model='row.entity.cultId.cotyId.name' data-np-ui-model='row.entity.cultId.cotyId._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;cultId.cotyId.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'cotyId.code', displayName: 'getALString("Land Cover Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultCopySuper_itm__cotyId_code' data-ng-model='row.entity.cotyId.code' data-np-ui-model='row.entity.cotyId._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;cotyId.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerEcCultCopySuperBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultCopySuperBase.prototype, "modelEcCultCopySuper", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultCopySuperBase.prototype.getEntityName = function () {
                return "EcCultCopy";
            };
            ControllerEcCultCopySuperBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerEcCultCopySuperBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.EcCultCopy.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.ecgcId = _this.Parent;
                    }
                    else {
                        x.ecgcId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerEcCultCopySuperBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelEcCultCopySuper.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultCopySuperBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpEcGroupCopy.controller.isEntityLocked(cur.ecgcId, lockKind);
            };
            ControllerEcCultCopySuperBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerEcCultCopySuperBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingEcCultCopySuper_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultCopySuperBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingEcCultCopySuper";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultCopySuperBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingEcCultCopySuper_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecgcId)) {
                    paramData['ecgcId_ecgcId'] = self.Parent.ecgcId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultCopySuperBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.ecgcId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "EcCultCopy/findAllByCriteriaRange_DecisionMakingEcCultCopySuper";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['ecgcId_ecgcId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerEcCultCopySuperBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.ecgcId === undefined)
                    return false;
                return x.ecgcId.getKey() === this.Parent.getKey();
            };
            ControllerEcCultCopySuperBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerEcCultCopySuperBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.ecgcId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultCopySuperBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcGroupCopy.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultCopySuperBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultCopySuperBase.prototype, "ecgcId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpEcGroupCopy.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultCopySuperBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.EcCultCopy(
                /*ecccId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*noneMatchDecision:number*/ null, 
                /*rowVersion:number*/ null, 
                /*cultId:Entities.Cultivation*/ null, 
                /*ecgcId:Entities.EcGroupCopy*/ null, 
                /*cotyId:Entities.CoverType*/ null, 
                /*sucaId:Entities.SuperClas*/ null);
                return ret;
            };
            ControllerEcCultCopySuperBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.ecgcId = self.ecgcId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerEcCultCopySuperBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerEcCultCopySuperBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                var _this = this;
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.EcCultCopy.Create();
                ret.updateInstance(src);
                ret.ecccId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                    _this.modelEcCultCopySuper.modelEcCultDetailCopySuper.controller.cloneAllEntitiesUnderParent(src, ret);
                }, 1);
                return ret;
            };
            ControllerEcCultCopySuperBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (ecCultCopyList) {
                    ecCultCopyList.forEach(function (ecCultCopy) {
                        var ecCultCopyCloned = _this.cloneEntity_internal(ecCultCopy, true);
                        ecCultCopyCloned.ecgcId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerEcCultCopySuperBase.prototype.deleteNewEntitiesUnderParent = function (ecGroupCopy) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var ecCultCopy = change.a;
                    if (ecGroupCopy.getKey() === ecCultCopy.ecgcId.getKey()) {
                        toBeDeleted.push(ecCultCopy);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                    self.modelEcCultCopySuper.modelEcCultDetailCopySuper.controller.deleteNewEntitiesUnderParent(x);
                });
            };
            ControllerEcCultCopySuperBase.prototype.deleteAllEntitiesUnderParent = function (ecGroupCopy, afterDeleteAction) {
                var self = this;
                function deleteEcCultCopyList(ecCultCopyList) {
                    if (ecCultCopyList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = ecCultCopyList[0];
                        var tail = ecCultCopyList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteEcCultCopyList(tail);
                        });
                    }
                }
                this.getMergedItems(function (ecCultCopyList) {
                    deleteEcCultCopyList(ecCultCopyList);
                }, false, ecGroupCopy);
            };
            Object.defineProperty(ControllerEcCultCopySuperBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [
                            self.modelEcCultCopySuper.modelEcCultDetailCopySuper.controller
                        ];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultCopySuperBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                var _this = this;
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    self.modelEcCultCopySuper.modelEcCultDetailCopySuper.controller.deleteAllEntitiesUnderParent(ent, function () {
                        _super.prototype.deleteEntity.call(_this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                    });
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        self.modelEcCultCopySuper.modelEcCultDetailCopySuper.controller.deleteNewEntitiesUnderParent(ent);
                        afterDeleteAction();
                    });
                }
            };
            ControllerEcCultCopySuperBase.prototype.showLov_EcCultCopySuper_itm__cultId = function (ecCultCopy) {
                var self = this;
                if (ecCultCopy === undefined)
                    return;
                var uimodel = ecCultCopy._cultId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'Cultivation';
                dialogOptions.previousModel = self.cachedLovModel_EcCultCopySuper_itm__cultId;
                dialogOptions.className = "ControllerLovCultivation";
                dialogOptions.onSelect = function (selectedEntity) {
                    var cultivation1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (false && isVoid(cultivation1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(cultivation1) && cultivation1.isEqual(ecCultCopy.cultId))
                        return;
                    ecCultCopy.cultId = cultivation1;
                    self.markEntityAsUpdated(ecCultCopy, 'EcCultCopySuper_itm__cultId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_EcCultCopySuper_itm__cultId = lovModel;
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['name'] = true;
                dialogOptions.shownCols['code'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/Cultivation.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerEcCultCopySuperBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController.GrpEcGroupCopy_2_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerEcCultCopySuperBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController.GrpEcGroupCopy_2_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerEcCultCopySuperBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerEcCultCopySuperBase.prototype._deleteIsDisabled = function (ecCultCopy) {
                return true;
            };
            ControllerEcCultCopySuperBase.prototype.child_group_EcCultDetailCopySuper_isInvisible = function () {
                var self = this;
                if ((self.model.modelEcCultDetailCopySuper === undefined) || (self.model.modelEcCultDetailCopySuper.controller === undefined))
                    return false;
                return self.model.modelEcCultDetailCopySuper.controller._isInvisible();
            };
            return ControllerEcCultCopySuperBase;
        })(Controllers.AbstractGroupTableController);
        DecisionMaking.ControllerEcCultCopySuperBase = ControllerEcCultCopySuperBase;
        // GROUP EcCultDetailCopySuper
        var ModelEcCultDetailCopySuperBase = (function (_super) {
            __extends(ModelEcCultDetailCopySuperBase, _super);
            function ModelEcCultDetailCopySuperBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "ecdcId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].ecdcId;
                },
                set: function (ecdcId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].ecdcId = ecdcId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_ecdcId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._ecdcId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "orderingNumber", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].orderingNumber;
                },
                set: function (orderingNumber_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].orderingNumber = orderingNumber_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_orderingNumber", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._orderingNumber;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "agreesDeclar", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].agreesDeclar;
                },
                set: function (agreesDeclar_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].agreesDeclar = agreesDeclar_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_agreesDeclar", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._agreesDeclar;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "comparisonOper", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].comparisonOper;
                },
                set: function (comparisonOper_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].comparisonOper = comparisonOper_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_comparisonOper", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._comparisonOper;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "probabThres", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probabThres;
                },
                set: function (probabThres_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probabThres = probabThres_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_probabThres", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probabThres;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "decisionLight", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].decisionLight;
                },
                set: function (decisionLight_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].decisionLight = decisionLight_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_decisionLight", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._decisionLight;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "agreesDeclar2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].agreesDeclar2;
                },
                set: function (agreesDeclar2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].agreesDeclar2 = agreesDeclar2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_agreesDeclar2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._agreesDeclar2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "comparisonOper2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].comparisonOper2;
                },
                set: function (comparisonOper2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].comparisonOper2 = comparisonOper2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_comparisonOper2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._comparisonOper2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "probabThres2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probabThres2;
                },
                set: function (probabThres2_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probabThres2 = probabThres2_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_probabThres2", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probabThres2;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "probabThresSum", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].probabThresSum;
                },
                set: function (probabThresSum_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].probabThresSum = probabThresSum_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_probabThresSum", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._probabThresSum;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "comparisonOper3", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].comparisonOper3;
                },
                set: function (comparisonOper3_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].comparisonOper3 = comparisonOper3_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_comparisonOper3", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._comparisonOper3;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "ecccId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].ecccId;
                },
                set: function (ecccId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].ecccId = ecccId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_ecccId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._ecccId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelEcCultDetailCopySuperBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelEcCultDetailCopySuperBase;
        })(Controllers.AbstractGroupTableModel);
        DecisionMaking.ModelEcCultDetailCopySuperBase = ModelEcCultDetailCopySuperBase;
        var ControllerEcCultDetailCopySuperBase = (function (_super) {
            __extends(ControllerEcCultDetailCopySuperBase, _super);
            function ControllerEcCultDetailCopySuperBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 3 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelEcCultDetailCopySuper = self.model;
                $scope.pageModel.modelEcCultDetailCopySuper = $scope.modelEcCultDetailCopySuper;
                $scope.clearBtnAction = function () {
                    self.updateGrid(0, false, true);
                };
                $scope.modelEcCultCopySuper.modelEcCultDetailCopySuper = $scope.modelEcCultDetailCopySuper;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelEcCultCopySuper.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.EcCultDetailCopy[] , oldVisible:Entities.EcCultDetailCopy[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            ControllerEcCultDetailCopySuperBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerEcCultDetailCopySuperBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerEcCultDetailCopySuper";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailCopySuperBase.prototype, "HtmlDivId", {
                get: function () {
                    return "DecisionMaking_ControllerEcCultDetailCopySuper";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailCopySuperBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultDetailCopySuperBase.prototype.gridTitle = function () {
                return "Criteria";
            };
            ControllerEcCultDetailCopySuperBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerEcCultDetailCopySuperBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'orderingNumber', displayName: 'getALString("Ordering \n No", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '5%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCopySuper_itm__orderingNumber' data-ng-model='row.entity.orderingNumber' data-np-ui-model='row.entity._orderingNumber' data-ng-change='markEntityAsUpdated(row.entity,&quot;orderingNumber&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='0' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'agreesDeclar', displayName: 'getALString("1st \ Prediction ", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopySuper_itm__agreesDeclar' data-ng-model='row.entity.agreesDeclar' data-np-ui-model='row.entity._agreesDeclar' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='true' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'comparisonOper', displayName: 'getALString("Comparison \n Operator 1st", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '11%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopySuper_itm__comparisonOper' data-ng-model='row.entity.comparisonOper' data-np-ui-model='row.entity._comparisonOper' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='true' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'probabThres', displayName: 'getALString("Confidence \n Pred. 1st (%)", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCopySuper_itm__probabThres' data-ng-model='row.entity.probabThres' data-np-ui-model='row.entity._probabThres' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'agreesDeclar2', displayName: 'getALString("2nd \ Prediction ", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopySuper_itm__agreesDeclar2' data-ng-model='row.entity.agreesDeclar2' data-np-ui-model='row.entity._agreesDeclar2' data-ng-change='markEntityAsUpdated(row.entity,&quot;agreesDeclar2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Disagree\"), value:0}, {label:getALString(\"Agree\"), value:1}]' data-ng-disabled='true' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'comparisonOper2', displayName: 'getALString("Comparison \n Operator 2nd", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '11%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopySuper_itm__comparisonOper2' data-ng-model='row.entity.comparisonOper2' data-np-ui-model='row.entity._comparisonOper2' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper2&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='true' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'probabThres2', displayName: 'getALString("Confidence \n Pred. 2nd (%)", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCopySuper_itm__probabThres2' data-ng-model='row.entity.probabThres2' data-np-ui-model='row.entity._probabThres2' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThres2&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'comparisonOper3', displayName: 'getALString("Comparison \n Operator 1st + 2nd", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '11%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopySuper_itm__comparisonOper3' data-ng-model='row.entity.comparisonOper3' data-np-ui-model='row.entity._comparisonOper3' data-ng-change='markEntityAsUpdated(row.entity,&quot;comparisonOper3&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Greater\"), value:1}, {label:getALString(\"Greater or equal\"), value:2}, {label:getALString(\"Less\"), value:3}, {label:getALString(\"Less or equal\"), value:4}]' data-ng-disabled='true' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'probabThresSum', displayName: 'getALString("Confidence \n Prediction \n 1st + 2nd (%)", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='EcCultDetailCopySuper_itm__probabThresSum' data-ng-model='row.entity.probabThresSum' data-np-ui-model='row.entity._probabThresSum' data-ng-change='markEntityAsUpdated(row.entity,&quot;probabThresSum&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'decisionLight', displayName: 'getALString("Traffic \n Light Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <select name='EcCultDetailCopySuper_itm__decisionLight' data-ng-model='row.entity.decisionLight' data-np-ui-model='row.entity._decisionLight' data-ng-change='markEntityAsUpdated(row.entity,&quot;decisionLight&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Green\"), value:1}, {label:getALString(\"Yellow\"), value:2}, {label:getALString(\"Red\"), value:3}, {label:getALString(\"Undefined\"), value:4}]' data-ng-disabled='true' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerEcCultDetailCopySuperBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailCopySuperBase.prototype, "modelEcCultDetailCopySuper", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultDetailCopySuperBase.prototype.getEntityName = function () {
                return "EcCultDetailCopy";
            };
            ControllerEcCultDetailCopySuperBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerEcCultDetailCopySuperBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.EcCultDetailCopy.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.ecccId = _this.Parent;
                    }
                    else {
                        x.ecccId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerEcCultDetailCopySuperBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelEcCultDetailCopySuper.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultDetailCopySuperBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelEcCultCopySuper.controller.isEntityLocked(cur.ecccId, lockKind);
            };
            ControllerEcCultDetailCopySuperBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerEcCultDetailCopySuperBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingEcCultDetailCopySuper_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultDetailCopySuperBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingEcCultDetailCopySuper";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultDetailCopySuperBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingEcCultDetailCopySuper_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.ecccId)) {
                    paramData['ecccId_ecccId'] = self.Parent.ecccId;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerEcCultDetailCopySuperBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.ecccId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "EcCultDetailCopy/findAllByCriteriaRange_DecisionMakingEcCultDetailCopySuper";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['ecccId_ecccId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerEcCultDetailCopySuperBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.ecccId === undefined)
                    return false;
                return x.ecccId.getKey() === this.Parent.getKey();
            };
            ControllerEcCultDetailCopySuperBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerEcCultDetailCopySuperBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.ecccId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailCopySuperBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelEcCultCopySuper.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailCopySuperBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerEcCultDetailCopySuperBase.prototype, "ecccId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelEcCultCopySuper.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultDetailCopySuperBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.EcCultDetailCopy(
                /*ecdcId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*orderingNumber:number*/ null, 
                /*agreesDeclar:number*/ null, 
                /*comparisonOper:number*/ null, 
                /*probabThres:number*/ null, 
                /*decisionLight:number*/ null, 
                /*rowVersion:number*/ null, 
                /*agreesDeclar2:number*/ null, 
                /*comparisonOper2:number*/ null, 
                /*probabThres2:number*/ null, 
                /*probabThresSum:number*/ null, 
                /*comparisonOper3:number*/ null, 
                /*ecccId:Entities.EcCultCopy*/ null);
                this.getMergedItems(function (items) {
                    ret.orderingNumber = items.maxBy(function (i) { return i.orderingNumber; }, 0) + 1;
                }, false);
                return ret;
            };
            ControllerEcCultDetailCopySuperBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.ecccId = self.ecccId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerEcCultDetailCopySuperBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerEcCultDetailCopySuperBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.EcCultDetailCopy.Create();
                ret.updateInstance(src);
                ret.ecdcId = tmpId;
                if (!calledByParent) {
                    this.getMergedItems(function (items) {
                        ret.orderingNumber = items.maxBy(function (i) { return i.orderingNumber; }, 0) + 1;
                    }, false);
                }
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            ControllerEcCultDetailCopySuperBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (ecCultDetailCopyList) {
                    ecCultDetailCopyList.forEach(function (ecCultDetailCopy) {
                        var ecCultDetailCopyCloned = _this.cloneEntity_internal(ecCultDetailCopy, true);
                        ecCultDetailCopyCloned.ecccId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerEcCultDetailCopySuperBase.prototype.deleteNewEntitiesUnderParent = function (ecCultCopy) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var ecCultDetailCopy = change.a;
                    if (ecCultCopy.getKey() === ecCultDetailCopy.ecccId.getKey()) {
                        toBeDeleted.push(ecCultDetailCopy);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                });
            };
            ControllerEcCultDetailCopySuperBase.prototype.deleteAllEntitiesUnderParent = function (ecCultCopy, afterDeleteAction) {
                var self = this;
                function deleteEcCultDetailCopyList(ecCultDetailCopyList) {
                    if (ecCultDetailCopyList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = ecCultDetailCopyList[0];
                        var tail = ecCultDetailCopyList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteEcCultDetailCopyList(tail);
                        });
                    }
                }
                this.getMergedItems(function (ecCultDetailCopyList) {
                    deleteEcCultDetailCopyList(ecCultDetailCopyList);
                }, false, ecCultCopy);
            };
            Object.defineProperty(ControllerEcCultDetailCopySuperBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerEcCultDetailCopySuperBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerEcCultDetailCopySuperBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerEcCultDetailCopySuperBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerEcCultDetailCopySuperBase.prototype._newIsDisabled = function () {
                return true;
            };
            ControllerEcCultDetailCopySuperBase.prototype._deleteIsDisabled = function (ecCultDetailCopy) {
                return true;
            };
            return ControllerEcCultDetailCopySuperBase;
        })(Controllers.AbstractGroupTableController);
        DecisionMaking.ControllerEcCultDetailCopySuperBase = ControllerEcCultDetailCopySuperBase;
    })(DecisionMaking = Controllers.DecisionMaking || (Controllers.DecisionMaking = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=DecisionMakingBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
