/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />

// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/IntegrateddecisionBase.ts" />
/// <reference path="../EntitiesBase/ParcelClasBase.ts" />
/// <reference path="LovParcelClasBase.ts" />
/// <reference path="../EntitiesBase/DecisionMakingBase.ts" />
/// <reference path="LovDecisionMakingBase.ts" />
/// <reference path="../Controllers/IntegrateddecisionSearch.ts" />

module Controllers {

    export class ModelIntegrateddecisionSearchBase extends Controllers.AbstractSearchPageModel {
        _decisionCode:NpTypes.UINumberModel = new NpTypes.UINumberModel(undefined);
        public get decisionCode():number {
            return this._decisionCode.value;
        }
        public set decisionCode(vl:number) {
            this._decisionCode.value = vl;
        }
        _dteUpdate:NpTypes.UIDateModel = new NpTypes.UIDateModel(undefined);
        public get dteUpdate():Date {
            return this._dteUpdate.value;
        }
        public set dteUpdate(vl:Date) {
            this._dteUpdate.value = vl;
        }
        _usrUpdate:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get usrUpdate():string {
            return this._usrUpdate.value;
        }
        public set usrUpdate(vl:string) {
            this._usrUpdate.value = vl;
        }
        _pclaId:NpTypes.UIManyToOneModel<Entities.ParcelClas> = new NpTypes.UIManyToOneModel<Entities.ParcelClas>(undefined);
        public get pclaId():Entities.ParcelClas {
            return this._pclaId.value;
        }
        public set pclaId(vl:Entities.ParcelClas) {
            this._pclaId.value = vl;
        }
        _demaId:NpTypes.UIManyToOneModel<Entities.DecisionMaking> = new NpTypes.UIManyToOneModel<Entities.DecisionMaking>(undefined);
        public get demaId():Entities.DecisionMaking {
            return this._demaId.value;
        }
        public set demaId(vl:Entities.DecisionMaking) {
            this._demaId.value = vl;
        }
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        controller: ControllerIntegrateddecisionSearch;
        constructor(public $scope: IScopeIntegrateddecisionSearch) { super($scope); }
    }
    
    export class ModelIntegrateddecisionSearchPersistedModel implements NpTypes.IBreadcrumbStepModel {
        _pageSize: number;
        _currentPage: number;
        _currentRow: number;
        sortField: string;
        sortOrder: string;
        decisionCode:number;
        dteUpdate:Date;
        usrUpdate:string;
        pclaId:Entities.ParcelClas;
        demaId:Entities.DecisionMaking;
    }



    export interface IScopeIntegrateddecisionSearchBase extends Controllers.IAbstractSearchPageScope {
        globals: Globals;
        pageModel : ModelIntegrateddecisionSearch;
        _newIsDisabled():boolean; 
        _searchIsDisabled():boolean; 
        _deleteIsDisabled(integrateddecision:Entities.Integrateddecision):boolean; 
        _editBtnIsDisabled(integrateddecision:Entities.Integrateddecision):boolean; 
        _cancelIsDisabled():boolean; 
        d__decisionCode_disabled():boolean; 
        d__dteUpdate_disabled():boolean; 
        d__usrUpdate_disabled():boolean; 
        d__pclaId_disabled():boolean; 
        showLov_d__pclaId():void; 
        d__demaId_disabled():boolean; 
        showLov_d__demaId():void; 
    }

    export class ControllerIntegrateddecisionSearchBase extends Controllers.AbstractSearchPageController {
        constructor(
            public $scope: IScopeIntegrateddecisionSearch,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model:ModelIntegrateddecisionSearch)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 1});
            var self: ControllerIntegrateddecisionSearchBase = this;
            model.controller = <ControllerIntegrateddecisionSearch>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;
            $scope.pageModel = self.model;
            model.pageTitle = 'Κριτήρια Αναζήτησης';
            
            var lastSearchPageModel = <ModelIntegrateddecisionSearchPersistedModel>$scope.getPageModelByURL('/IntegrateddecisionSearch');
            var rowIndexToSelect: number = 0;
            if (lastSearchPageModel !== undefined && lastSearchPageModel !== null) {
                self.model.pagingOptions.pageSize = lastSearchPageModel._pageSize;
                self.model.pagingOptions.currentPage = lastSearchPageModel._currentPage;
                rowIndexToSelect = lastSearchPageModel._currentRow;
                self.model.sortField = lastSearchPageModel.sortField;
                self.model.sortOrder = lastSearchPageModel.sortOrder;
                self.pageModel.decisionCode = lastSearchPageModel.decisionCode;
                self.pageModel.dteUpdate = lastSearchPageModel.dteUpdate;
                self.pageModel.usrUpdate = lastSearchPageModel.usrUpdate;
                self.pageModel.pclaId = lastSearchPageModel.pclaId;
                self.pageModel.demaId = lastSearchPageModel.demaId;
            }


            $scope._newIsDisabled = 
                () => {
                    return self._newIsDisabled();
                };
            $scope._searchIsDisabled = 
                () => {
                    return self._searchIsDisabled();
                };
            $scope._deleteIsDisabled = 
                (integrateddecision:Entities.Integrateddecision) => {
                    return self._deleteIsDisabled(integrateddecision);
                };
            $scope._editBtnIsDisabled = 
                (integrateddecision:Entities.Integrateddecision) => {
                    return self._editBtnIsDisabled(integrateddecision);
                };
            $scope._cancelIsDisabled = 
                () => {
                    return self._cancelIsDisabled();
                };
            $scope.d__decisionCode_disabled = 
                () => {
                    return self.d__decisionCode_disabled();
                };
            $scope.d__dteUpdate_disabled = 
                () => {
                    return self.d__dteUpdate_disabled();
                };
            $scope.d__usrUpdate_disabled = 
                () => {
                    return self.d__usrUpdate_disabled();
                };
            $scope.d__pclaId_disabled = 
                () => {
                    return self.d__pclaId_disabled();
                };
            $scope.showLov_d__pclaId = 
                () => {
                    $timeout( () => {        
                        self.showLov_d__pclaId();
                    }, 0);
                };
            $scope.d__demaId_disabled = 
                () => {
                    return self.d__demaId_disabled();
                };
            $scope.showLov_d__demaId = 
                () => {
                    $timeout( () => {        
                        self.showLov_d__demaId();
                    }, 0);
                };

            $scope.clearBtnAction = () => { 
                self.pageModel.decisionCode = undefined;
                self.pageModel.dteUpdate = undefined;
                self.pageModel.usrUpdate = undefined;
                self.pageModel.pclaId = undefined;
                self.pageModel.demaId = undefined;
                self.updateGrid(0, false, true);
            };

            $scope.newBtnAction = () => { 
                $scope.pageModel.newBtnPressed = true;
                self.onNew();
            };

            $scope.onEdit = (x:Entities.Integrateddecision):void => {
                $scope.pageModel.newBtnPressed = false;
                return self.onEdit(x);
            };

            $timeout(function() {
                $('#Search_Integrateddecision').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
            }, 0);

            self.updateUI(rowIndexToSelect);
        }

        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerIntegrateddecisionSearch";
        }
        public get HtmlDivId(): string {
            return "Search_Integrateddecision";
        }
    
        public _getPageTitle(): string {
            return "Integrateddecision";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridEdit\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);onEdit(row.entity)\"> </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined},
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'decisionCode', displayName:'getALString("Κωδικός", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.decisionCode.toStringFormatted(\",\", \".\", 0)' style='width: 100%;text-align: right;padding-right: 6px;' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'dteUpdate', displayName:'getALString("Ημερομηνία", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <input name='_dteUpdate' data-ng-model='row.entity.dteUpdate' data-np-ui-model='row.entity._dteUpdate' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteUpdate&quot;)' data-ng-readonly='true' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'usrUpdate', displayName:'getALString("Ημερομηνία", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.usrUpdate' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.probPred', displayName:'getALString("pclaId", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.pclaId.probPred' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'demaId.description', displayName:'getALString("demaId", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()' data-ng-dblclick='onEdit(row.entity)'> \
                    <label data-ng-bind='row.entity.demaId.description' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }


        public getPersistedModel():ModelIntegrateddecisionSearchPersistedModel {
            var self = this;
            var ret = new ModelIntegrateddecisionSearchPersistedModel();
            ret._pageSize = self.model.pagingOptions.pageSize;
            ret._currentPage = self.model.pagingOptions.currentPage;
            ret._currentRow = self.CurrentRowIndex;
            ret.sortField = self.model.sortField;
            ret.sortOrder = self.model.sortOrder;
            ret.decisionCode = self.pageModel.decisionCode;
            ret.dteUpdate = self.pageModel.dteUpdate;
            ret.usrUpdate = self.pageModel.usrUpdate;
            ret.pclaId = self.pageModel.pclaId;
            ret.demaId = self.pageModel.demaId;
            return ret;
        }

        public get pageModel():ModelIntegrateddecisionSearch {
            return <ModelIntegrateddecisionSearch>this.model;
        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            return Entities.Integrateddecision.fromJSONComplete(webResponse.data);
        }

        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
            var wsPath = "Integrateddecision/findLazyIntegrateddecision";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};

            paramData['fromRowIndex'] = paramIndexFrom;
            paramData['toRowIndex'] = paramIndexTo;

            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.decisionCode)) {
                paramData['decisionCode'] = self.pageModel.decisionCode;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.dteUpdate)) {
                paramData['dteUpdate'] = self.pageModel.dteUpdate;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.usrUpdate)) {
                paramData['usrUpdate'] = self.pageModel.usrUpdate;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.pclaId) && !isVoid(self.pageModel.pclaId.pclaId)) {
                paramData['pclaId_pclaId'] = self.pageModel.pclaId.pclaId;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.demaId) && !isVoid(self.pageModel.demaId.demaId)) {
                paramData['demaId_demaId'] = self.pageModel.demaId.demaId;
            }

            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var promise = self.$http.post(
                url,
                paramData,
                {timeout:self.$scope.globals.timeoutInMS, cache:false});
            var wsStartTs = (new Date).getTime();
            return promise.success(() => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error((data, status, header, config) => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });

        }

        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
            var wsPath = "Integrateddecision/findLazyIntegrateddecision_count";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.decisionCode)) {
                paramData['decisionCode'] = self.pageModel.decisionCode;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.dteUpdate)) {
                paramData['dteUpdate'] = self.pageModel.dteUpdate;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.usrUpdate)) {
                paramData['usrUpdate'] = self.pageModel.usrUpdate;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.pclaId) && !isVoid(self.pageModel.pclaId.pclaId)) {
                paramData['pclaId_pclaId'] = self.pageModel.pclaId.pclaId;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.demaId) && !isVoid(self.pageModel.demaId.demaId)) {
                paramData['demaId_demaId'] = self.pageModel.demaId.demaId;
            }
            
            var promise = self.$http.post(
                url,
                paramData,
                { timeout: self.$scope.globals.timeoutInMS, cache: false });
            var wsStartTs = (new Date).getTime();
            return promise.success(() => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
            }).error((data, status, header, config) => {
                self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
            });
        }

        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
            var paramData = {};

            if (self.model.sortField !== undefined) {
                paramData['sortField'] = self.model.sortField;
                paramData['sortOrder'] = self.model.sortOrder == 'asc';
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.decisionCode)) {
                paramData['decisionCode'] = self.pageModel.decisionCode;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.dteUpdate)) {
                paramData['dteUpdate'] = self.pageModel.dteUpdate;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.usrUpdate)) {
                paramData['usrUpdate'] = self.pageModel.usrUpdate;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.pclaId) && !isVoid(self.pageModel.pclaId.pclaId)) {
                paramData['pclaId_pclaId'] = self.pageModel.pclaId.pclaId;
            }
            if (!isVoid(self) && !isVoid(self.pageModel) && !isVoid(self.pageModel.demaId) && !isVoid(self.pageModel.demaId.demaId)) {
                paramData['demaId_demaId'] = self.pageModel.demaId.demaId;
            }
                
            var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
            return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
        }


        public get Current():Entities.Integrateddecision {
            var self = this;
            return <Entities.Integrateddecision>self.$scope.pageModel.selectedEntities[0];
        }

        public onNew():void {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            self.$scope.navigateForward('/Integrateddecision', self._getPageTitle(), {});
        }

        public onEdit(x:Entities.Integrateddecision):void {
            var self = this;
            self.$scope.setCurrentPageModel(self.getPersistedModel());
            var visitedPageModel:IFormPageBreadcrumbStepModel<Entities.Integrateddecision>= {"selectedEntity":x};
            self.$scope.navigateForward('/Integrateddecision', self._getPageTitle(), visitedPageModel);
        }
        
        private getSynchronizeChangesWithDbUrl(): string {
            return "/Niva/rest/MainService/synchronizeChangesWithDb_Integrateddecision";
        }
        public getSynchronizeChangesWithDbData(x:Entities.Integrateddecision):any {
            var self = this;
            var paramData:any = {
                data: <ChangeToCommit[]>[]
            };
            var changeToCommit = new ChangeToCommit(ChangeStatus.DELETE, Utils.getTimeInMS(), x.getEntityName(), x);

            paramData.data.push(changeToCommit);
            return paramData;
        }

        public deleteRecord(x:Entities.Integrateddecision):void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);

            console.log("deleting Integrateddecision with PK field:" + x.integrateddecisionsId);
            console.log(x);
            var url = this.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = this.getSynchronizeChangesWithDbData(x);

            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, Messages.dynamicMessage("EntitySuccessDeletion2"));
                    self.updateGrid();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.$scope.pageModel, Messages.dynamicMessage(data));
            });
        }
        
        public _newIsDisabled():boolean {
            var self = this;

            if (!self.$scope.globals.hasPrivilege("Niva_Integrateddecision_W"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _searchIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _deleteIsDisabled(integrateddecision:Entities.Integrateddecision):boolean {
            var self = this;
            if (integrateddecision === undefined || integrateddecision === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Integrateddecision_D"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _editBtnIsDisabled(integrateddecision:Entities.Integrateddecision):boolean {
            var self = this;
            if (integrateddecision === undefined || integrateddecision === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Integrateddecision_R"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _cancelIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public d__decisionCode_disabled():boolean {
            var self = this;


            var entityIsDisabled   = false;
            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public d__dteUpdate_disabled():boolean {
            var self = this;


            var entityIsDisabled   = false;
            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public d__usrUpdate_disabled():boolean {
            var self = this;


            var entityIsDisabled   = false;
            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public d__pclaId_disabled():boolean {
            var self = this;


            var entityIsDisabled   = false;
            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_d__pclaId:ModelLovParcelClasBase;
        public showLov_d__pclaId() {
            var self = this;
            var uimodel:NpTypes.IUIModel = self.pageModel._pclaId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'ParcelClas';
            dialogOptions.previousModel= self.cachedLovModel_d__pclaId;
            dialogOptions.className = "ControllerLovParcelClas";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var parcelClas1:Entities.ParcelClas =   <Entities.ParcelClas>selectedEntity;
                uimodel.clearAllErrors();
                if(false && isVoid(parcelClas1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(parcelClas1) && parcelClas1.isEqual(self.pageModel.pclaId))
                    return;
                self.pageModel.pclaId = parcelClas1;
            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovParcelClasBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_d__pclaId = lovModel;
                    var wsPath = "ParcelClas/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred)) {
                        paramData['fsch_ParcelClasLov_probPred'] = lovModel.fsch_ParcelClasLov_probPred;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred2)) {
                        paramData['fsch_ParcelClasLov_probPred2'] = lovModel.fsch_ParcelClasLov_probPred2;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_prodCode)) {
                        paramData['fsch_ParcelClasLov_prodCode'] = lovModel.fsch_ParcelClasLov_prodCode;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_parcIdentifier)) {
                        paramData['fsch_ParcelClasLov_parcIdentifier'] = lovModel.fsch_ParcelClasLov_parcIdentifier;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovParcelClasBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "ParcelClas/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred)) {
                        paramData['fsch_ParcelClasLov_probPred'] = lovModel.fsch_ParcelClasLov_probPred;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred2)) {
                        paramData['fsch_ParcelClasLov_probPred2'] = lovModel.fsch_ParcelClasLov_probPred2;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_prodCode)) {
                        paramData['fsch_ParcelClasLov_prodCode'] = lovModel.fsch_ParcelClasLov_prodCode;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_parcIdentifier)) {
                        paramData['fsch_ParcelClasLov_parcIdentifier'] = lovModel.fsch_ParcelClasLov_parcIdentifier;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred)) {
                        paramData['fsch_ParcelClasLov_probPred'] = lovModel.fsch_ParcelClasLov_probPred;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred2)) {
                        paramData['fsch_ParcelClasLov_probPred2'] = lovModel.fsch_ParcelClasLov_probPred2;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_prodCode)) {
                        paramData['fsch_ParcelClasLov_prodCode'] = lovModel.fsch_ParcelClasLov_prodCode;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_parcIdentifier)) {
                        paramData['fsch_ParcelClasLov_parcIdentifier'] = lovModel.fsch_ParcelClasLov_parcIdentifier;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['probPred'] = true;
            dialogOptions.shownCols['probPred2'] = true;
            dialogOptions.shownCols['prodCode'] = true;
            dialogOptions.shownCols['parcIdentifier'] = true;
            dialogOptions.shownCols['parcCode'] = true;
            dialogOptions.shownCols['geom4326'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/ParcelClas.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public d__demaId_disabled():boolean {
            var self = this;


            var entityIsDisabled   = false;
            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_d__demaId:ModelLovDecisionMakingBase;
        public showLov_d__demaId() {
            var self = this;
            var uimodel:NpTypes.IUIModel = self.pageModel._demaId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'DecisionMaking';
            dialogOptions.previousModel= self.cachedLovModel_d__demaId;
            dialogOptions.className = "ControllerLovDecisionMaking";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var decisionMaking1:Entities.DecisionMaking =   <Entities.DecisionMaking>selectedEntity;
                uimodel.clearAllErrors();
                if(false && isVoid(decisionMaking1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(decisionMaking1) && decisionMaking1.isEqual(self.pageModel.demaId))
                    return;
                self.pageModel.demaId = decisionMaking1;
            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovDecisionMakingBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_d__demaId = lovModel;
                    var wsPath = "DecisionMaking/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_DecisionMakingLov_description)) {
                        paramData['fsch_DecisionMakingLov_description'] = lovModel.fsch_DecisionMakingLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_DecisionMakingLov_dateTime)) {
                        paramData['fsch_DecisionMakingLov_dateTime'] = lovModel.fsch_DecisionMakingLov_dateTime;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovDecisionMakingBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "DecisionMaking/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_DecisionMakingLov_description)) {
                        paramData['fsch_DecisionMakingLov_description'] = lovModel.fsch_DecisionMakingLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_DecisionMakingLov_dateTime)) {
                        paramData['fsch_DecisionMakingLov_dateTime'] = lovModel.fsch_DecisionMakingLov_dateTime;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_DecisionMakingLov_description)) {
                        paramData['fsch_DecisionMakingLov_description'] = lovModel.fsch_DecisionMakingLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_DecisionMakingLov_dateTime)) {
                        paramData['fsch_DecisionMakingLov_dateTime'] = lovModel.fsch_DecisionMakingLov_dateTime;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['description'] = true;
            dialogOptions.shownCols['dateTime'] = true;
            dialogOptions.shownCols['recordtype'] = true;
            dialogOptions.shownCols['hasBeenRun'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/DecisionMaking.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }


    }
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
