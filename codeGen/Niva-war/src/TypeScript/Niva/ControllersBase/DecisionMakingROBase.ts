/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/DecisionMakingBase.ts" />
/// <reference path="../EntitiesBase/IntegrateddecisionBase.ts" />
/// <reference path="LovDecisionMakingBase.ts" />
/// <reference path="../EntitiesBase/ClassificationBase.ts" />
/// <reference path="LovClassificationBase.ts" />
/// <reference path="../EntitiesBase/EcGroupBase.ts" />
/// <reference path="LovEcGroupBase.ts" />
/// <reference path="../EntitiesBase/CultivationBase.ts" />
/// <reference path="LovCultivationBase.ts" />
/// <reference path="../Controllers/DecisionMakingRO.ts" />
module Controllers.DecisionMakingRO {
    export class PageModelBase extends AbstractPageModel {
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        modelGrpDema:ModelGrpDema;
        modelGrpIntegrateddecision:ModelGrpIntegrateddecision;
        controller: PageController;
        constructor(public $scope: IPageScope) { super($scope); }
    }


    export interface IPageScopeBase extends IAbstractPageScope {
        globals: Globals;
        _saveIsDisabled():boolean; 
        _cancelIsDisabled():boolean; 
        pageModel : PageModel;
        onSaveBtnAction(): void;
        onNewBtnAction(): void;
        onDeleteBtnAction():void;

    }

    export class PageControllerBase extends AbstractPageController {
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model:PageModel)
        {
            super($scope, $http, $timeout, Plato, model);
            var self: PageControllerBase = this;
            model.controller = <PageController>self;
            $scope.pageModel = self.model;

            $scope._saveIsDisabled = 
                () => {
                    return self._saveIsDisabled();
                };
            $scope._cancelIsDisabled = 
                () => {
                    return self._cancelIsDisabled();
                };

            $scope.onSaveBtnAction = () => { 
                self.onSaveBtnAction((response:NpTypes.SaveResponce) => {self.onSuccesfullSaveFromButton(response);});
            };
            

            $scope.onNewBtnAction = () => { 
                self.onNewBtnAction();
            };
            
            $scope.onDeleteBtnAction = () => { 
                self.onDeleteBtnAction();
            };


            $timeout(function() {
                $('#DecisionMakingRO').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                $('#NpMainContent').scrollTop(0);
            }, 0);

            var selectedEntity: Entities.DecisionMaking = this.getInitialEntity();
            if (!isVoid(selectedEntity)) {
                this.model.globalDema = Entities.DecisionMaking.Create();
                this.model.globalDema.updateInstance(selectedEntity);
            }
        }
        public get ControllerClassName(): string {
            return "PageController";
        }
        public get HtmlDivId(): string {
            return "DecisionMakingRO";
        }
    
        public _getPageTitle(): string {
            return "Field Map";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                ];
            }
            return this._items;
        }

        public _saveIsDisabled():boolean  {
            return true;
        }
        public _cancelIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }

        public get pageModel():PageModel {
            var self = this;
            return self.model;
        }

        
        public update():void {
            var self = this;
            self.model.modelGrpDema.controller.updateUI();
        }

        public refreshVisibleEntities(newEntitiesIds: NpTypes.NewEntityId[]):void {
            var self = this;
            self.model.modelGrpDema.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpIntegrateddecision.controller.refreshVisibleEntities(newEntitiesIds);
        }

        public refreshGroups(newEntitiesIds: NpTypes.NewEntityId[]): void {
            var self = this;
            self.model.modelGrpDema.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpIntegrateddecision.controller.refreshVisibleEntities(newEntitiesIds);
        }

        _firstLevelGroupControllers: Array<AbstractGroupController>=undefined;
        public get FirstLevelGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelGroupControllers === undefined) {
                self._firstLevelGroupControllers = [
                    self.model.modelGrpDema.controller
                ];
            }
            return this._firstLevelGroupControllers;
        }

        _allGroupControllers: Array<AbstractGroupController>=undefined;
        public get AllGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._allGroupControllers === undefined) {
                self._allGroupControllers = [
                    self.model.modelGrpDema.controller,
                    self.model.modelGrpIntegrateddecision.controller
                ];
            }
            return this._allGroupControllers;
        }

        public getPageChanges(bForDelete:boolean = false):Array<ChangeToCommit> {
            var self = this;
            var pageChanges: Array<ChangeToCommit> = [];
            if (bForDelete) {
                pageChanges = pageChanges.concat(self.model.modelGrpDema.controller.getChangesToCommitForDeletion());
                pageChanges = pageChanges.concat(self.model.modelGrpDema.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpIntegrateddecision.controller.getDeleteChangesToCommit());
            } else {

                pageChanges = pageChanges.concat(self.model.modelGrpDema.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpIntegrateddecision.controller.getChangesToCommit());
            }
            
            var hasDecisionMaking = pageChanges.some(change => change.entityName === "DecisionMaking")
            if (!hasDecisionMaking) {
                var masterEntity = new ChangeToCommit(ChangeStatus.UPDATE, 0, "DecisionMaking", self.model.globalDema);
                pageChanges = pageChanges.concat(masterEntity);
            }
            return pageChanges;
        }

        private getSynchronizeChangesWithDbUrl():string { 
            return "/Niva/rest/MainService/synchronizeChangesWithDb_DecisionMakingRO"; 
        }
        
        public getSynchronizeChangesWithDbData(bForDelete:boolean = false):any {
            var self = this;
            var paramData:any = {}
            paramData.data = self.getPageChanges(bForDelete);
            return paramData;
        }


        public onSuccesfullSaveFromButton(response:NpTypes.SaveResponce) {
            var self = this;
            super.onSuccesfullSaveFromButton(response);
            if (!isVoid(response.warningMessages)) {
                NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
            }
            if (!isVoid(response.infoMessages)) {
                NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
            }

            self.model.modelGrpDema.controller.cleanUpAfterSave();
            self.model.modelGrpIntegrateddecision.controller.cleanUpAfterSave();
            self.$scope.globals.isCurrentTransactionDirty = false;
            self.refreshGroups(response.newEntitiesIds);
        }

        public onFailuredSave(data:any, status:number) {
            var self = this;
            if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
            var errors = Messages.dynamicMessage(data);
            NpTypes.AlertMessage.addDanger(self.model, errors);
            messageBox( self.$scope, self.Plato,"MessageBox_Attention_Title",
                "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>",
                IconKind.ERROR, [new Tuple2("OK", () => {}),], 0, 0,'50em');
        }
        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }
        
        public onSaveBtnAction(onSuccess:(response:NpTypes.SaveResponce)=>void): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            var errors = self.validatePage();
            if (errors.length > 0) {
                var errMessage = errors.join('<br>');
                NpTypes.AlertMessage.addDanger(self.model, errMessage);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title",
                    "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>",
                    IconKind.ERROR,[new Tuple2("OK", () => {}),], 0, 0,'50em');
                return;
            }

            if (self.$scope.globals.isCurrentTransactionDirty === false) {
                var jqSaveBtn = self.SaveBtnJQueryHandler;
                self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                return;
            }

            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData();
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success((response:NpTypes.SaveResponce, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;

                    var newDecisionMakingId = response.newEntitiesIds.firstOrNull(x => x.entityName === 'DecisionMaking');
                    if (!isVoid(newDecisionMakingId)) {
                        this.model.globalDema = Entities.DecisionMaking.Create();
                        this.model.globalDema.demaId = newDecisionMakingId.databaseId;
                        self.$scope.globals.globalDema.refresh(self);
                    }
                    if (!self.shownAsDialog()) {
                        var breadcrumbStepModel = <IFormPageBreadcrumbStepModel<Entities.DecisionMaking>>self.$scope.getCurrentPageModel();
                        if (!isVoid(newDecisionMakingId)) {
                            if (!isVoid(breadcrumbStepModel)) {
                                breadcrumbStepModel.selectedEntity = Entities.DecisionMaking.CreateById(newDecisionMakingId.databaseId);
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        } else {
                            if (!isVoid(breadcrumbStepModel) && !(isVoid(breadcrumbStepModel.selectedEntity))) {
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        self.$scope.setCurrentPageModel(breadcrumbStepModel);
                    }

                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status)
            });
        }

        public getInitialEntity(): Entities.DecisionMaking {
            if (this.shownAsDialog()) {
                return <Entities.DecisionMaking>this.dialogSelectedEntity();
            } else {
                var breadCrumbStepModel = <IFormPageBreadcrumbStepModel<Entities.DecisionMaking>>this.$scope.getPageModelByURL('/DecisionMakingRO');
                if (isVoid(breadCrumbStepModel)) {
                    return null;
                } else {
                    return isVoid(breadCrumbStepModel.selectedEntity) ? null : breadCrumbStepModel.selectedEntity;
                }
            }
        }



        public onPageUnload(actualNavigation: (pageScopeYouAreLeavingFrom?: NpTypes.IApplicationScope) => void) {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => {
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                actualNavigation(self.$scope);
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                actualNavigation(self.$scope);
            }
        }


        public onNewBtnAction(): void {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => { 
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                self.addNewRecord();
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {self.addNewRecord();}),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                self.addNewRecord();
            }
        }

        public addNewRecord(): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            self.model.modelGrpDema.controller.cleanUpAfterSave();
            self.model.modelGrpIntegrateddecision.controller.cleanUpAfterSave();
            self.model.modelGrpDema.controller.createNewEntityAndAddToUI();
        }

        public onDeleteBtnAction():void {
            var self = this;
            messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("EntityDeletionWarningMsg"), IconKind.QUESTION, 
                [
                    new Tuple2("MessageBox_Button_Yes", () => {self.deleteRecord();}),
                    new Tuple2("MessageBox_Button_No", () => {})
                ], 1,1);
        }

        public deleteRecord(): void {
            var self = this;
            if (self.Mode === EditMode.NEW) {
                console.log("Delete pressed in a form while being in new mode!");
                return;
            }
            NpTypes.AlertMessage.clearAlerts(self.model);
            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData(true);
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.model.globalDema = null;
                    self.$scope.navigateBackward();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.model, Messages.dynamicMessage(data));
            });
        }
        
        public get Mode(): EditMode {
            var self = this;
            if (isVoid(self.model.modelGrpDema) || isVoid(self.model.modelGrpDema.controller))
                return EditMode.NEW;
            return self.model.modelGrpDema.controller.Mode;
        }

        public _newIsDisabled(): boolean {
            var self = this;
            if (isVoid(self.model.modelGrpDema) || isVoid(self.model.modelGrpDema.controller))
                return true;
            return self.model.modelGrpDema.controller._newIsDisabled();
        }
        public _deleteIsDisabled(): boolean {
            var self = this;
            if (self.Mode === EditMode.NEW)
                return true;
            if (isVoid(self.model.modelGrpDema) || isVoid(self.model.modelGrpDema.controller))
                return true;
            return self.model.modelGrpDema.controller._deleteIsDisabled(self.model.modelGrpDema.controller.Current);
        }


    }


    

    // GROUP GrpDema

    export class ModelGrpDemaBase extends Controllers.AbstractGroupFormModel {
        modelGrpIntegrateddecision:ModelGrpIntegrateddecision;
        controller: ControllerGrpDema;
        public get demaId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.DecisionMaking>this.selectedEntities[0]).demaId;
        }

        public set demaId(demaId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.DecisionMaking>this.selectedEntities[0]).demaId = demaId_newVal;
        }

        public get _demaId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._demaId;
        }

        public get description():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.DecisionMaking>this.selectedEntities[0]).description;
        }

        public set description(description_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.DecisionMaking>this.selectedEntities[0]).description = description_newVal;
        }

        public get _description():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._description;
        }

        public get dateTime():Date {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.DecisionMaking>this.selectedEntities[0]).dateTime;
        }

        public set dateTime(dateTime_newVal:Date) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.DecisionMaking>this.selectedEntities[0]).dateTime = dateTime_newVal;
        }

        public get _dateTime():NpTypes.UIDateModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._dateTime;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.DecisionMaking>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.DecisionMaking>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get recordtype():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.DecisionMaking>this.selectedEntities[0]).recordtype;
        }

        public set recordtype(recordtype_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.DecisionMaking>this.selectedEntities[0]).recordtype = recordtype_newVal;
        }

        public get _recordtype():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._recordtype;
        }

        public get hasBeenRun():boolean {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.DecisionMaking>this.selectedEntities[0]).hasBeenRun;
        }

        public set hasBeenRun(hasBeenRun_newVal:boolean) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.DecisionMaking>this.selectedEntities[0]).hasBeenRun = hasBeenRun_newVal;
        }

        public get _hasBeenRun():NpTypes.UIBoolModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._hasBeenRun;
        }

        public get hasPopulatedIntegratedDecisionsAndIssues():boolean {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.DecisionMaking>this.selectedEntities[0]).hasPopulatedIntegratedDecisionsAndIssues;
        }

        public set hasPopulatedIntegratedDecisionsAndIssues(hasPopulatedIntegratedDecisionsAndIssues_newVal:boolean) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.DecisionMaking>this.selectedEntities[0]).hasPopulatedIntegratedDecisionsAndIssues = hasPopulatedIntegratedDecisionsAndIssues_newVal;
        }

        public get _hasPopulatedIntegratedDecisionsAndIssues():NpTypes.UIBoolModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._hasPopulatedIntegratedDecisionsAndIssues;
        }

        public get clasId():Entities.Classification {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.DecisionMaking>this.selectedEntities[0]).clasId;
        }

        public set clasId(clasId_newVal:Entities.Classification) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.DecisionMaking>this.selectedEntities[0]).clasId = clasId_newVal;
        }

        public get _clasId():NpTypes.UIManyToOneModel<Entities.Classification> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._clasId;
        }

        public get ecgrId():Entities.EcGroup {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.DecisionMaking>this.selectedEntities[0]).ecgrId;
        }

        public set ecgrId(ecgrId_newVal:Entities.EcGroup) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.DecisionMaking>this.selectedEntities[0]).ecgrId = ecgrId_newVal;
        }

        public get _ecgrId():NpTypes.UIManyToOneModel<Entities.EcGroup> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._ecgrId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpDema) { super($scope); }
    }



    export interface IScopeGrpDemaBase extends Controllers.IAbstractFormGroupScope, IPageScope{
        globals: Globals;
        modelGrpDema : ModelGrpDema;
        GrpDema_itm__description_disabled(decisionMaking:Entities.DecisionMaking):boolean; 
        GrpDema_itm__clasId_disabled(decisionMaking:Entities.DecisionMaking):boolean; 
        showLov_GrpDema_itm__clasId(decisionMaking:Entities.DecisionMaking):void; 
        GrpDema_itm__ecgrId_disabled(decisionMaking:Entities.DecisionMaking):boolean; 
        showLov_GrpDema_itm__ecgrId(decisionMaking:Entities.DecisionMaking):void; 
        GrpDema_itm__dateTime_disabled(decisionMaking:Entities.DecisionMaking):boolean; 
        GrpDema_0_disabled():boolean; 
        GrpDema_0_invisible():boolean; 
        child_group_GrpIntegrateddecision_isInvisible():boolean; 

    }



    export class ControllerGrpDemaBase extends Controllers.AbstractGroupFormController {
        constructor(
            public $scope: IScopeGrpDema,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpDema)
        {
            super($scope, $http, $timeout, Plato, model);
            var self:ControllerGrpDemaBase = this;
            model.controller = <ControllerGrpDema>self;
            $scope.modelGrpDema = self.model;

            var selectedEntity: Entities.DecisionMaking = this.$scope.pageModel.controller.getInitialEntity();
            if (isVoid(selectedEntity)) {
                self.createNewEntityAndAddToUI();
            } else {
                var clonedEntity = Entities.DecisionMaking.Create();
                clonedEntity.updateInstance(selectedEntity);
                $scope.modelGrpDema.visibleEntities[0] = clonedEntity;
                $scope.modelGrpDema.selectedEntities[0] = clonedEntity;
            } 
            $scope.GrpDema_itm__description_disabled = 
                (decisionMaking:Entities.DecisionMaking) => {
                    return self.GrpDema_itm__description_disabled(decisionMaking);
                };
            $scope.GrpDema_itm__clasId_disabled = 
                (decisionMaking:Entities.DecisionMaking) => {
                    return self.GrpDema_itm__clasId_disabled(decisionMaking);
                };
            $scope.showLov_GrpDema_itm__clasId = 
                (decisionMaking:Entities.DecisionMaking) => {
                    $timeout( () => {        
                        self.showLov_GrpDema_itm__clasId(decisionMaking);
                    }, 0);
                };
            $scope.GrpDema_itm__ecgrId_disabled = 
                (decisionMaking:Entities.DecisionMaking) => {
                    return self.GrpDema_itm__ecgrId_disabled(decisionMaking);
                };
            $scope.showLov_GrpDema_itm__ecgrId = 
                (decisionMaking:Entities.DecisionMaking) => {
                    $timeout( () => {        
                        self.showLov_GrpDema_itm__ecgrId(decisionMaking);
                    }, 0);
                };
            $scope.GrpDema_itm__dateTime_disabled = 
                (decisionMaking:Entities.DecisionMaking) => {
                    return self.GrpDema_itm__dateTime_disabled(decisionMaking);
                };
            $scope.GrpDema_0_disabled = 
                () => {
                    return self.GrpDema_0_disabled();
                };
            $scope.GrpDema_0_invisible = 
                () => {
                    return self.GrpDema_0_invisible();
                };
            $scope.child_group_GrpIntegrateddecision_isInvisible = 
                () => {
                    return self.child_group_GrpIntegrateddecision_isInvisible();
                };


            $scope.pageModel.modelGrpDema = $scope.modelGrpDema;


    /*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.DecisionMaking[] , oldVisible:Entities.DecisionMaking[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
            //bind entity child collection with child controller merged items
            Entities.DecisionMaking.integrateddecisionCollection = (decisionMaking, func) => {
                this.model.modelGrpIntegrateddecision.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, decisionMaking);
            }

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
                //unbind entity child collection with child controller merged items
                Entities.DecisionMaking.integrateddecisionCollection = null;//(decisionMaking, func) => { }
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpDema";
        }
        public get HtmlDivId(): string {
            return "DecisionMakingRO_ControllerGrpDema";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new TextItem (
                        (ent?: Entities.DecisionMaking) => 'Decision Making',
                        false ,
                        (ent?: Entities.DecisionMaking) => ent.description,  
                        (ent?: Entities.DecisionMaking) => ent._description,  
                        (ent?: Entities.DecisionMaking) => false, 
                        (vl: string, ent?: Entities.DecisionMaking) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new LovItem (
                        (ent?: Entities.DecisionMaking) => 'Classification',
                        false ,
                        (ent?: Entities.DecisionMaking) => ent.clasId,  
                        (ent?: Entities.DecisionMaking) => ent._clasId,  
                        (ent?: Entities.DecisionMaking) => true, //isRequired
                        (vl: Entities.Classification, ent?: Entities.DecisionMaking) => new NpTypes.ValidationResult(true, "")),
                    new LovItem (
                        (ent?: Entities.DecisionMaking) => 'BRE Rules Name',
                        false ,
                        (ent?: Entities.DecisionMaking) => ent.ecgrId,  
                        (ent?: Entities.DecisionMaking) => ent._ecgrId,  
                        (ent?: Entities.DecisionMaking) => true, //isRequired
                        (vl: Entities.EcGroup, ent?: Entities.DecisionMaking) => new NpTypes.ValidationResult(true, "")),
                    new DateItem (
                        (ent?: Entities.DecisionMaking) => 'Date',
                        false ,
                        (ent?: Entities.DecisionMaking) => ent.dateTime,  
                        (ent?: Entities.DecisionMaking) => ent._dateTime,  
                        (ent?: Entities.DecisionMaking) => true, //isRequired
                        (vl: Date, ent?: Entities.DecisionMaking) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined)
                ];
            }
            return this._items;
        }

        

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpDema():ModelGrpDema {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "DecisionMaking";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
        }

        public setCurrentFormPageBreadcrumpStepModel() {
            var self = this;
            if (self.$scope.pageModel.controller.shownAsDialog())
                return;

            var breadCrumbStepModel: IFormPageBreadcrumbStepModel<Entities.DecisionMaking> = { selectedEntity: null };
            if (!isVoid(self.Current)) {
                var selectedEntity: Entities.DecisionMaking = Entities.DecisionMaking.Create();
                selectedEntity.updateInstance(self.Current);
                breadCrumbStepModel.selectedEntity = selectedEntity;
            } else {
                breadCrumbStepModel.selectedEntity = null;
            }
            self.$scope.setCurrentPageModel(breadCrumbStepModel);
        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.DecisionMaking.fromJSONComplete(webResponse.data);
            return entlist;
        }

        public get Current():Entities.DecisionMaking {
            var self = this;
            return <Entities.DecisionMaking>self.$scope.modelGrpDema.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.DecisionMaking, lockKind:LockKind):boolean  {
        
            return true;

        }







        public constructEntity(): Entities.DecisionMaking {
            var self = this;
            var ret = new Entities.DecisionMaking(
                /*demaId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*description:string*/ null,
                /*dateTime:Date*/ null,
                /*rowVersion:number*/ null,
                /*recordtype:number*/ null,
                /*hasBeenRun:boolean*/ null,
                /*hasPopulatedIntegratedDecisionsAndIssues:boolean*/ null,
                /*clasId:Entities.Classification*/ null,
                /*ecgrId:Entities.EcGroup*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(): NpTypes.IBaseEntity {
                throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "createNewEntityAndAddToUI() called for a view");

        }

        public cloneEntity(src: Entities.DecisionMaking): Entities.DecisionMaking {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.DecisionMaking, calledByParent: boolean= false): Entities.DecisionMaking {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.DecisionMaking.Create();
            ret.updateInstance(src);
            ret.demaId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.visibleEntities[0] = ret;
            this.model.selectedEntities[0] = ret;

            this.$timeout( () => {
                this.modelGrpDema.modelGrpIntegrateddecision.controller.cloneAllEntitiesUnderParent(src, ret);
            },1);

            return ret;
        }



        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                    self.modelGrpDema.modelGrpIntegrateddecision.controller
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.DecisionMaking, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        }


        public GrpDema_itm__description_disabled(decisionMaking:Entities.DecisionMaking):boolean {
            var self = this;
            if (decisionMaking === undefined || decisionMaking === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_DecisionMakingRO_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(decisionMaking, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpDema_itm__clasId_disabled(decisionMaking:Entities.DecisionMaking):boolean {
            var self = this;
            if (decisionMaking === undefined || decisionMaking === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_DecisionMakingRO_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(decisionMaking, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_GrpDema_itm__clasId:ModelLovClassificationBase;
        public showLov_GrpDema_itm__clasId(decisionMaking:Entities.DecisionMaking) {
            var self = this;
            if (decisionMaking === undefined)
                return;
            var uimodel:NpTypes.IUIModel = decisionMaking._clasId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Data Import Name';
            dialogOptions.previousModel= self.cachedLovModel_GrpDema_itm__clasId;
            dialogOptions.className = "ControllerLovClassification";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var classification1:Entities.Classification =   <Entities.Classification>selectedEntity;
                uimodel.clearAllErrors();
                if(true && isVoid(classification1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(classification1) && classification1.isEqual(decisionMaking.clasId))
                    return;
                decisionMaking.clasId = classification1;
                self.markEntityAsUpdated(decisionMaking, 'GrpDema_itm__clasId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovClassificationBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpDema_itm__clasId = lovModel;
                    var wsPath = "Classification/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                        paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                        paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                        paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovClassificationBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "Classification/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                        paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                        paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                        paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_name)) {
                        paramData['fsch_ClassificationLov_name'] = lovModel.fsch_ClassificationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_description)) {
                        paramData['fsch_ClassificationLov_description'] = lovModel.fsch_ClassificationLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ClassificationLov_dateTime)) {
                        paramData['fsch_ClassificationLov_dateTime'] = lovModel.fsch_ClassificationLov_dateTime;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['description'] = true;
            dialogOptions.shownCols['dateTime'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/Classification.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public GrpDema_itm__ecgrId_disabled(decisionMaking:Entities.DecisionMaking):boolean {
            var self = this;
            if (decisionMaking === undefined || decisionMaking === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_DecisionMakingRO_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(decisionMaking, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        cachedLovModel_GrpDema_itm__ecgrId:ModelLovEcGroupBase;
        public showLov_GrpDema_itm__ecgrId(decisionMaking:Entities.DecisionMaking) {
            var self = this;
            if (decisionMaking === undefined)
                return;
            var uimodel:NpTypes.IUIModel = decisionMaking._ecgrId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'BRE Rules';
            dialogOptions.previousModel= self.cachedLovModel_GrpDema_itm__ecgrId;
            dialogOptions.className = "ControllerLovEcGroup";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var ecGroup1:Entities.EcGroup =   <Entities.EcGroup>selectedEntity;
                uimodel.clearAllErrors();
                if(true && isVoid(ecGroup1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(ecGroup1) && ecGroup1.isEqual(decisionMaking.ecgrId))
                    return;
                decisionMaking.ecgrId = ecGroup1;
                self.markEntityAsUpdated(decisionMaking, 'GrpDema_itm__ecgrId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovEcGroupBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpDema_itm__ecgrId = lovModel;
                    var wsPath = "EcGroup/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_description)) {
                        paramData['fsch_EcGroupLov_description'] = lovModel.fsch_EcGroupLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_name)) {
                        paramData['fsch_EcGroupLov_name'] = lovModel.fsch_EcGroupLov_name;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovEcGroupBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "EcGroup/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_description)) {
                        paramData['fsch_EcGroupLov_description'] = lovModel.fsch_EcGroupLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_name)) {
                        paramData['fsch_EcGroupLov_name'] = lovModel.fsch_EcGroupLov_name;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_description)) {
                        paramData['fsch_EcGroupLov_description'] = lovModel.fsch_EcGroupLov_description;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_EcGroupLov_name)) {
                        paramData['fsch_EcGroupLov_name'] = lovModel.fsch_EcGroupLov_name;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;
            dialogOptions.shownCols['description'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/EcGroup.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public GrpDema_itm__dateTime_disabled(decisionMaking:Entities.DecisionMaking):boolean {
            var self = this;
            if (decisionMaking === undefined || decisionMaking === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_DecisionMakingRO_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(decisionMaking, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpDema_0_disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpDema_0_invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            return true;
        }
        public _deleteIsDisabled(decisionMaking:Entities.DecisionMaking):boolean  {
            return true;
        }
        public child_group_GrpIntegrateddecision_isInvisible():boolean {
            var self = this;
            if ((self.model.modelGrpIntegrateddecision === undefined) || (self.model.modelGrpIntegrateddecision.controller === undefined))
                return false;
            return self.model.modelGrpIntegrateddecision.controller._isInvisible()
        }

    }


    // GROUP GrpIntegrateddecision

    export class ModelGrpIntegrateddecisionBase extends Controllers.AbstractGroupTableModel {
        controller: ControllerGrpIntegrateddecision;
        public get integrateddecisionsId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Integrateddecision>this.selectedEntities[0]).integrateddecisionsId;
        }

        public set integrateddecisionsId(integrateddecisionsId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Integrateddecision>this.selectedEntities[0]).integrateddecisionsId = integrateddecisionsId_newVal;
        }

        public get _integrateddecisionsId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._integrateddecisionsId;
        }

        public get decisionCode():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Integrateddecision>this.selectedEntities[0]).decisionCode;
        }

        public set decisionCode(decisionCode_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Integrateddecision>this.selectedEntities[0]).decisionCode = decisionCode_newVal;
        }

        public get _decisionCode():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._decisionCode;
        }

        public get dteUpdate():Date {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Integrateddecision>this.selectedEntities[0]).dteUpdate;
        }

        public set dteUpdate(dteUpdate_newVal:Date) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Integrateddecision>this.selectedEntities[0]).dteUpdate = dteUpdate_newVal;
        }

        public get _dteUpdate():NpTypes.UIDateModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._dteUpdate;
        }

        public get usrUpdate():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Integrateddecision>this.selectedEntities[0]).usrUpdate;
        }

        public set usrUpdate(usrUpdate_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Integrateddecision>this.selectedEntities[0]).usrUpdate = usrUpdate_newVal;
        }

        public get _usrUpdate():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._usrUpdate;
        }

        public get pclaId():Entities.ParcelClas {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Integrateddecision>this.selectedEntities[0]).pclaId;
        }

        public set pclaId(pclaId_newVal:Entities.ParcelClas) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Integrateddecision>this.selectedEntities[0]).pclaId = pclaId_newVal;
        }

        public get _pclaId():NpTypes.UIManyToOneModel<Entities.ParcelClas> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._pclaId;
        }

        public get demaId():Entities.DecisionMaking {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Integrateddecision>this.selectedEntities[0]).demaId;
        }

        public set demaId(demaId_newVal:Entities.DecisionMaking) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Integrateddecision>this.selectedEntities[0]).demaId = demaId_newVal;
        }

        public get _demaId():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._demaId;
        }

        _fsch_decisionCode:NpTypes.UINumberModel = new NpTypes.UINumberModel(undefined);
        public get fsch_decisionCode():number {
            return this._fsch_decisionCode.value;
        }
        public set fsch_decisionCode(vl:number) {
            this._fsch_decisionCode.value = vl;
        }
        _fsch_Parcel_Identifier:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get fsch_Parcel_Identifier():string {
            return this._fsch_Parcel_Identifier.value;
        }
        public set fsch_Parcel_Identifier(vl:string) {
            this._fsch_Parcel_Identifier.value = vl;
        }
        _fsch_prodCode:NpTypes.UINumberModel = new NpTypes.UINumberModel(undefined);
        public get fsch_prodCode():number {
            return this._fsch_prodCode.value;
        }
        public set fsch_prodCode(vl:number) {
            this._fsch_prodCode.value = vl;
        }
        _fsch_Code:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get fsch_Code():string {
            return this._fsch_Code.value;
        }
        public set fsch_Code(vl:string) {
            this._fsch_Code.value = vl;
        }
        _fsch_CultDeclCode:NpTypes.UIManyToOneModel<Entities.Cultivation> = new NpTypes.UIManyToOneModel<Entities.Cultivation>(undefined);
        public get fsch_CultDeclCode():Entities.Cultivation {
            return this._fsch_CultDeclCode.value;
        }
        public set fsch_CultDeclCode(vl:Entities.Cultivation) {
            this._fsch_CultDeclCode.value = vl;
        }
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpIntegrateddecision) { super($scope); }
    }



    export interface IScopeGrpIntegrateddecisionBase extends Controllers.IAbstractTableGroupScope, IScopeGrpDemaBase{
        globals: Globals;
        modelGrpIntegrateddecision : ModelGrpIntegrateddecision;
        _disabled():boolean; 
        _invisible():boolean; 
        GrpIntegrateddecision_srchr__fsch_decisionCode_disabled(integrateddecision:Entities.Integrateddecision):boolean; 
        GrpIntegrateddecision_srchr__fsch_Parcel_Identifier_disabled(integrateddecision:Entities.Integrateddecision):boolean; 
        GrpIntegrateddecision_srchr__fsch_prodCode_disabled(integrateddecision:Entities.Integrateddecision):boolean; 
        GrpIntegrateddecision_srchr__fsch_Code_disabled(integrateddecision:Entities.Integrateddecision):boolean; 
        GrpIntegrateddecision_srchr__fsch_CultDeclCode_disabled(integrateddecision:Entities.Integrateddecision):boolean; 
        showLov_GrpIntegrateddecision_srchr__fsch_CultDeclCode():void; 
        GrpIntegrateddecision_0_disabled():boolean; 
        GrpIntegrateddecision_0_invisible():boolean; 
        GrpIntegrateddecision_0_0_disabled(integrateddecision:Entities.Integrateddecision):boolean; 
        exportIntegratedDecisionsToCSV(integrateddecision:Entities.Integrateddecision):void; 
        GrpIntegrateddecision_0_1_disabled(integrateddecision:Entities.Integrateddecision):boolean; 
        actionPushToCommonsAPI(integrateddecision:Entities.Integrateddecision):void; 
        GrpIntegrateddecision_1_disabled():boolean; 
        GrpIntegrateddecision_1_invisible():boolean; 
        XartoReg_disabled():boolean; 
        XartoReg_invisible():boolean; 

    }



    export class ControllerGrpIntegrateddecisionBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpIntegrateddecision,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpIntegrateddecision)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 5, maxLinesInHeader: 3});
            var self:ControllerGrpIntegrateddecisionBase = this;
            model.controller = <ControllerGrpIntegrateddecision>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpIntegrateddecision = self.model;

            $scope._disabled = 
                () => {
                    return self._disabled();
                };
            $scope._invisible = 
                () => {
                    return self._invisible();
                };
            $scope.GrpIntegrateddecision_srchr__fsch_decisionCode_disabled = 
                (integrateddecision:Entities.Integrateddecision) => {
                    return self.GrpIntegrateddecision_srchr__fsch_decisionCode_disabled(integrateddecision);
                };
            $scope.GrpIntegrateddecision_srchr__fsch_Parcel_Identifier_disabled = 
                (integrateddecision:Entities.Integrateddecision) => {
                    return self.GrpIntegrateddecision_srchr__fsch_Parcel_Identifier_disabled(integrateddecision);
                };
            $scope.GrpIntegrateddecision_srchr__fsch_prodCode_disabled = 
                (integrateddecision:Entities.Integrateddecision) => {
                    return self.GrpIntegrateddecision_srchr__fsch_prodCode_disabled(integrateddecision);
                };
            $scope.GrpIntegrateddecision_srchr__fsch_Code_disabled = 
                (integrateddecision:Entities.Integrateddecision) => {
                    return self.GrpIntegrateddecision_srchr__fsch_Code_disabled(integrateddecision);
                };
            $scope.GrpIntegrateddecision_srchr__fsch_CultDeclCode_disabled = 
                (integrateddecision:Entities.Integrateddecision) => {
                    return self.GrpIntegrateddecision_srchr__fsch_CultDeclCode_disabled(integrateddecision);
                };
            $scope.showLov_GrpIntegrateddecision_srchr__fsch_CultDeclCode = 
                () => {
                    $timeout( () => {        
                        self.showLov_GrpIntegrateddecision_srchr__fsch_CultDeclCode();
                    }, 0);
                };
            $scope.GrpIntegrateddecision_0_disabled = 
                () => {
                    return self.GrpIntegrateddecision_0_disabled();
                };
            $scope.GrpIntegrateddecision_0_invisible = 
                () => {
                    return self.GrpIntegrateddecision_0_invisible();
                };
            $scope.GrpIntegrateddecision_0_0_disabled = 
                (integrateddecision:Entities.Integrateddecision) => {
                    return self.GrpIntegrateddecision_0_0_disabled(integrateddecision);
                };
            $scope.exportIntegratedDecisionsToCSV = 
                (integrateddecision:Entities.Integrateddecision) => {
            self.exportIntegratedDecisionsToCSV(integrateddecision);
                };
            $scope.GrpIntegrateddecision_0_1_disabled = 
                (integrateddecision:Entities.Integrateddecision) => {
                    return self.GrpIntegrateddecision_0_1_disabled(integrateddecision);
                };
            $scope.actionPushToCommonsAPI = 
                (integrateddecision:Entities.Integrateddecision) => {
            self.actionPushToCommonsAPI(integrateddecision);
                };
            $scope.GrpIntegrateddecision_1_disabled = 
                () => {
                    return self.GrpIntegrateddecision_1_disabled();
                };
            $scope.GrpIntegrateddecision_1_invisible = 
                () => {
                    return self.GrpIntegrateddecision_1_invisible();
                };
            $scope.XartoReg_disabled = 
                () => {
                    return self.XartoReg_disabled();
                };
            $scope.XartoReg_invisible = 
                () => {
                    return self.XartoReg_invisible();
                };


            $scope.pageModel.modelGrpIntegrateddecision = $scope.modelGrpIntegrateddecision;


            $scope.clearBtnAction = () => { 
                self.modelGrpIntegrateddecision.fsch_decisionCode = undefined;
                self.modelGrpIntegrateddecision.fsch_Parcel_Identifier = undefined;
                self.modelGrpIntegrateddecision.fsch_prodCode = undefined;
                self.modelGrpIntegrateddecision.fsch_Code = undefined;
                self.modelGrpIntegrateddecision.fsch_CultDeclCode = undefined;
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpDema.modelGrpIntegrateddecision = $scope.modelGrpIntegrateddecision;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpDema.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.Integrateddecision[] , oldVisible:Entities.Integrateddecision[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            this.createPclaId_geom4326Map({
                grp:this,
                Plato:Plato,
                divId:"INPMPDMRO",
                getDataFromEntity:(x: Entities.Integrateddecision) => isVoid(x) ? undefined:x.pclaId.geom4326,
                saveDataToEntity:(x: Entities.Integrateddecision, f: ol.Feature) => { 
                    if (!isVoid(x)) {
                        x.pclaId.geom4326 = f; 
                        this.markEntityAsUpdated(x,"pclaId.geom4326");
                    } 
                },
                isDisabled:(x: Entities.Integrateddecision) => true,
                layers : [
                            new NpGeoLayers.OpenStreetTileLayer(true),
                            new NpGeoLayers.SqlVectorLayer<Entities.Integrateddecision, AbstractSqlLayerFilterModel>({
                                layerId:'INPMPDMRO_ParcelGreen',
                                label:'Green Parcels',
                                isVisible:true,
                                getQueryUrl:() => '/Niva/rest/Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen?',
                                getQueryInputs:(integrateddecision:Entities.Integrateddecision) => { return {     globalDema_demaId : (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.globalDema) && !isVoid(self.modelGrpIntegrateddecision.globalDema.demaId)) ? self.modelGrpIntegrateddecision.globalDema.demaId : null } },
                                fillColor:'rgba(0, 255, 0, 0.8)',
                                borderColor:'rgba(0,0,0,1.0)',
                                penWidth:1,
                                selectable:true,
                                showSpatialSearchFilters:true
                            }),
                            new NpGeoLayers.SqlVectorLayer<Entities.Integrateddecision, AbstractSqlLayerFilterModel>({
                                layerId:'INPMPDMRO_ParcelYellow',
                                label:'Yellow Parcels',
                                isVisible:true,
                                getQueryUrl:() => '/Niva/rest/Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow?',
                                getQueryInputs:(integrateddecision:Entities.Integrateddecision) => { return {     globalDema_demaId : (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.globalDema) && !isVoid(self.modelGrpIntegrateddecision.globalDema.demaId)) ? self.modelGrpIntegrateddecision.globalDema.demaId : null } },
                                fillColor:'rgba(255, 255, 0, 0.8)',
                                borderColor:'rgba(0,0,0,1.0)',
                                penWidth:1,
                                selectable:true,
                                showSpatialSearchFilters:true
                            }),
                            new NpGeoLayers.SqlVectorLayer<Entities.Integrateddecision, AbstractSqlLayerFilterModel>({
                                layerId:'INPMPDMRO_ParcelRed',
                                label:'Red Parcels',
                                isVisible:true,
                                getQueryUrl:() => '/Niva/rest/Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed?',
                                getQueryInputs:(integrateddecision:Entities.Integrateddecision) => { return {     globalDema_demaId : (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.globalDema) && !isVoid(self.modelGrpIntegrateddecision.globalDema.demaId)) ? self.modelGrpIntegrateddecision.globalDema.demaId : null } },
                                fillColor:'rgba(255, 0, 0, 0.8)',
                                borderColor:'rgba(0,0,0,1.0)',
                                penWidth:1,
                                selectable:true,
                                showSpatialSearchFilters:true
                            }),
                            new NpGeoLayers.SqlVectorLayer<Entities.Integrateddecision, AbstractSqlLayerFilterModel>({
                                layerId:'INPMPDMRO_ParcelUndefined',
                                label:'Undefined Parcels',
                                isVisible:true,
                                getQueryUrl:() => '/Niva/rest/Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined?',
                                getQueryInputs:(integrateddecision:Entities.Integrateddecision) => { return {     globalDema_demaId : (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.globalDema) && !isVoid(self.modelGrpIntegrateddecision.globalDema.demaId)) ? self.modelGrpIntegrateddecision.globalDema.demaId : null } },
                                fillColor:'rgba(255,255,255, 0.5)',
                                borderColor:'rgba(0,0,0,0.5)',
                                penWidth:1,
                                selectable:true,
                                showSpatialSearchFilters:true
                            }),
                            new NpGeoLayers.SqlVectorLayer<Entities.Integrateddecision, AbstractSqlLayerFilterModel>({
                                layerId:'INPMPDMRO5',
                                label:'Search1',
                                isVisible:true,
                                isSearch:true
                            }),
                            new NpGeoLayers.SqlVectorLayer<Entities.Integrateddecision, AbstractSqlLayerFilterModel>({
                                layerId:'INPMPDMRO6',
                                label:'Search2',
                                isVisible:true,
                                isSearch:true
                            }),
                            new NpGeoLayers.SqlVectorLayer<Entities.Integrateddecision, AbstractSqlLayerFilterModel>({
                                layerId:'INPMPDMRO7',
                                label:'Search3',
                                isVisible:true,
                                isSearch:true
                            })
                         ],
                coordinateSystem : "EPSG:2100"
            });
            
        }

        public pclaId_geom4326Map : NpGeo.NpMap;

        public createPclaId_geom4326Map(defaultOptions: NpGeo.NpMapConstructionOptions) {
            this.pclaId_geom4326Map = new NpGeo.NpMap(defaultOptions);
        }

        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpIntegrateddecision";
        }
        public get HtmlDivId(): string {
            return "DecisionMakingRO_ControllerGrpIntegrateddecision";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new StaticListItem<number> (
                        (ent?: NpTypes.IBaseEntity) => 'Decision Light',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_decisionCode,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_decisionCode,  
                        (ent?: NpTypes.IBaseEntity) => false, //isRequired
                        (vl: number, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, "")),
                    new TextItem (
                        (ent?: NpTypes.IBaseEntity) => 'Identifier',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_Parcel_Identifier,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_Parcel_Identifier,  
                        (ent?: NpTypes.IBaseEntity) => false, 
                        (vl: string, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new NumberItem (
                        (ent?: NpTypes.IBaseEntity) => 'Farmer Code',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_prodCode,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_prodCode,  
                        (ent?: NpTypes.IBaseEntity) => false, //isRequired
                        (vl: number, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined,
                        0),
                    new TextItem (
                        (ent?: NpTypes.IBaseEntity) => 'Parcel Code',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_Code,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_Code,  
                        (ent?: NpTypes.IBaseEntity) => false, 
                        (vl: string, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new LovItem (
                        (ent?: NpTypes.IBaseEntity) => 'Declared Cultivation',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_CultDeclCode,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_CultDeclCode,  
                        (ent?: NpTypes.IBaseEntity) => false, //isRequired
                        (vl: Entities.Cultivation, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""))
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Parcels";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'decisionCode', displayName:'getALString("Integrated Decision \n Light", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='GrpIntegrateddecision_itm__decisionCode' data-ng-model='row.entity.decisionCode' data-np-ui-model='row.entity._decisionCode' data-ng-change='markEntityAsUpdated(row.entity,&quot;decisionCode&quot;)' data-np-required='true' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:getALString(\"DropDownList_ChooseOne\"), value:null}, {label:getALString(\"Green\"), value:1}, {label:getALString(\"Yellow\"), value:2}, {label:getALString(\"Red\"), value:3}, {label:getALString(\"Undefined\"), value:4}]' data-ng-disabled='true' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.parcIdentifier', displayName:'getALString("Identifier", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_parcIdentifier' data-ng-model='row.entity.pclaId.parcIdentifier' data-np-ui-model='row.entity.pclaId._parcIdentifier' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.parcIdentifier&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.parcCode', displayName:'getALString("Parcel\'s \n Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_parcCode' data-ng-model='row.entity.pclaId.parcCode' data-np-ui-model='row.entity.pclaId._parcCode' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.parcCode&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.prodCode', displayName:'getALString("Farmer\'s \n Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_prodCode' data-ng-model='row.entity.pclaId.prodCode' data-np-ui-model='row.entity.pclaId._prodCode' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.prodCode&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='0' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.cultIdDecl.code', displayName:'getALString("Cultivation \n Declared Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'7%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_cultIdDecl_code' data-ng-model='row.entity.pclaId.cultIdDecl.code' data-np-ui-model='row.entity.pclaId.cultIdDecl._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdDecl.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.cultIdDecl.name', displayName:'getALString("Cultivation \n Declared Name", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'9%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_cultIdDecl_name' data-ng-model='row.entity.pclaId.cultIdDecl.name' data-np-ui-model='row.entity.pclaId.cultIdDecl._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdDecl.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.cultIdPred.code', displayName:'getALString("Cultivation \n 1st Prediction \n Code", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'7%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_cultIdPred_code' data-ng-model='row.entity.pclaId.cultIdPred.code' data-np-ui-model='row.entity.pclaId.cultIdPred._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdPred.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.cultIdPred.name', displayName:'getALString("Cultivation \n 1st Prediction \n Name", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'10%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_cultIdPred_name' data-ng-model='row.entity.pclaId.cultIdPred.name' data-np-ui-model='row.entity.pclaId.cultIdPred._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdPred.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.probPred', displayName:'getALString("Probability \n 1st Prediction", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_probPred' data-ng-model='row.entity.pclaId.probPred' data-np-ui-model='row.entity.pclaId._probPred' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.probPred&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.cultIdPred2.code', displayName:'getALString("Cultivation \n 2nd Prediction  \nCode", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'7%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_cultIdPred2_code' data-ng-model='row.entity.pclaId.cultIdPred2.code' data-np-ui-model='row.entity.pclaId.cultIdPred2._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdPred2.code&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.cultIdPred2.name', displayName:'getALString("Cultivation \n 2nd Prediction \n Name", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'10%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_cultIdPred2_name' data-ng-model='row.entity.pclaId.cultIdPred2.name' data-np-ui-model='row.entity.pclaId.cultIdPred2._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.cultIdPred2.name&quot;)' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'pclaId.probPred2', displayName:'getALString("Probability \n 2nd Prediction", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpIntegrateddecision_itm__pclaId_probPred2' data-ng-model='row.entity.pclaId.probPred2' data-np-ui-model='row.entity.pclaId._probPred2' data-ng-change='markEntityAsUpdated(row.entity,&quot;pclaId.probPred2&quot;)' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='2' data-np-decSep='.' data-np-thSep='.' class='ngCellNumber' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpIntegrateddecision():ModelGrpIntegrateddecision {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "Integrateddecision";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

            this.pclaId_geom4326Map.cleanUndoBuffers();
        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.DecisionMaking): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.Integrateddecision.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.demaId = this.Parent;
                } else {
                    x.demaId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.Integrateddecision {
            var self = this;
            return <Entities.Integrateddecision>self.$scope.modelGrpIntegrateddecision.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.Integrateddecision, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpDema.controller.isEntityLocked(<Entities.DecisionMaking>cur.demaId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_decisionCode)) {
                    paramData['fsch_decisionCode'] = self.modelGrpIntegrateddecision.fsch_decisionCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpIntegrateddecision.fsch_prodCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_Code)) {
                    paramData['fsch_Code'] = self.modelGrpIntegrateddecision.fsch_Code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_Parcel_Identifier)) {
                    paramData['fsch_Parcel_Identifier'] = self.modelGrpIntegrateddecision.fsch_Parcel_Identifier;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_CultDeclCode) && !isVoid(self.modelGrpIntegrateddecision.fsch_CultDeclCode.cultId)) {
                    paramData['fsch_CultDeclCode_cultId'] = self.modelGrpIntegrateddecision.fsch_CultDeclCode.cultId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "Integrateddecision/findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_decisionCode)) {
                    paramData['fsch_decisionCode'] = self.modelGrpIntegrateddecision.fsch_decisionCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpIntegrateddecision.fsch_prodCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_Code)) {
                    paramData['fsch_Code'] = self.modelGrpIntegrateddecision.fsch_Code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_Parcel_Identifier)) {
                    paramData['fsch_Parcel_Identifier'] = self.modelGrpIntegrateddecision.fsch_Parcel_Identifier;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_CultDeclCode) && !isVoid(self.modelGrpIntegrateddecision.fsch_CultDeclCode.cultId)) {
                    paramData['fsch_CultDeclCode_cultId'] = self.modelGrpIntegrateddecision.fsch_CultDeclCode.cultId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "Integrateddecision/findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_decisionCode)) {
                    paramData['fsch_decisionCode'] = self.modelGrpIntegrateddecision.fsch_decisionCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpIntegrateddecision.fsch_prodCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_Code)) {
                    paramData['fsch_Code'] = self.modelGrpIntegrateddecision.fsch_Code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_Parcel_Identifier)) {
                    paramData['fsch_Parcel_Identifier'] = self.modelGrpIntegrateddecision.fsch_Parcel_Identifier;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_CultDeclCode) && !isVoid(self.modelGrpIntegrateddecision.fsch_CultDeclCode.cultId)) {
                    paramData['fsch_CultDeclCode_cultId'] = self.modelGrpIntegrateddecision.fsch_CultDeclCode.cultId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "Integrateddecision/findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_decisionCode)) {
                    paramData['fsch_decisionCode'] = self.modelGrpIntegrateddecision.fsch_decisionCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpIntegrateddecision.fsch_prodCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_Code)) {
                    paramData['fsch_Code'] = self.modelGrpIntegrateddecision.fsch_Code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_Parcel_Identifier)) {
                    paramData['fsch_Parcel_Identifier'] = self.modelGrpIntegrateddecision.fsch_Parcel_Identifier;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_CultDeclCode) && !isVoid(self.modelGrpIntegrateddecision.fsch_CultDeclCode.cultId)) {
                    paramData['fsch_CultDeclCode_cultId'] = self.modelGrpIntegrateddecision.fsch_CultDeclCode.cultId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.Integrateddecision[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.Integrateddecision[]) => void> = [];


        public getMergedItems(func: (items: Entities.Integrateddecision[]) => void, bContinuousCaller:boolean=true, parent?: Entities.DecisionMaking, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.Integrateddecision[]):void {
                var mergedEntities = <Entities.Integrateddecision[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.Integrateddecision>e.a).demaId)).
                        map(e => <Entities.Integrateddecision>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "Integrateddecision/findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['demaId_demaId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.Integrateddecision[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.Integrateddecision):boolean {
            if (this.Parent === undefined || x.demaId === undefined)
                return false;
            return x.demaId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.DecisionMaking {
            var self = this;
            return self.demaId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpDema.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get demaId():Entities.DecisionMaking {
            var self = this;
            return <Entities.DecisionMaking>self.$scope.modelGrpDema.selectedEntities[0];
        }









        public constructEntity(): Entities.Integrateddecision {
            var self = this;
            var ret = new Entities.Integrateddecision(
                /*integrateddecisionsId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*decisionCode:number*/ null,
                /*dteUpdate:Date*/ null,
                /*usrUpdate:string*/ null,
                /*pclaId:Entities.ParcelClas*/ null,
                /*demaId:Entities.DecisionMaking*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.Integrateddecision = <Entities.Integrateddecision>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.demaId = self.demaId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.Integrateddecision): Entities.Integrateddecision {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.Integrateddecision, calledByParent: boolean= false): Entities.Integrateddecision {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.Integrateddecision.Create();
            ret.updateInstance(src);
            ret.integrateddecisionsId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.DecisionMaking, newParent:Entities.DecisionMaking) {
        
            this.model.totalItems = 0;

            this.getMergedItems(integrateddecisionList => {
                integrateddecisionList.forEach(integrateddecision => {
                    var integrateddecisionCloned = this.cloneEntity_internal(integrateddecision, true);
                    integrateddecisionCloned.demaId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(decisionMaking: Entities.DecisionMaking) {
        

            var self = this;
            var toBeDeleted:Array<Entities.Integrateddecision> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var integrateddecision = <Entities.Integrateddecision>change.a
                if (decisionMaking.getKey() === integrateddecision.demaId.getKey()) {
                    toBeDeleted.push(integrateddecision);
                }
            }

            _.each(toBeDeleted, (x:Entities.Integrateddecision) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
            });

        }

        public deleteAllEntitiesUnderParent(decisionMaking: Entities.DecisionMaking, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteIntegrateddecisionList(integrateddecisionList: Entities.Integrateddecision[]) {
                if (integrateddecisionList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = integrateddecisionList[0];
                    var tail = integrateddecisionList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteIntegrateddecisionList(tail);
                    });
                }
            }


            this.getMergedItems(integrateddecisionList => {
                deleteIntegrateddecisionList(integrateddecisionList);
            }, false, decisionMaking);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.Integrateddecision, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        public _disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpIntegrateddecision_srchr__fsch_decisionCode_disabled(integrateddecision:Entities.Integrateddecision):boolean {
            var self = this;


            return false;
        }
        public GrpIntegrateddecision_srchr__fsch_Parcel_Identifier_disabled(integrateddecision:Entities.Integrateddecision):boolean {
            var self = this;


            return false;
        }
        public GrpIntegrateddecision_srchr__fsch_prodCode_disabled(integrateddecision:Entities.Integrateddecision):boolean {
            var self = this;


            return false;
        }
        public GrpIntegrateddecision_srchr__fsch_Code_disabled(integrateddecision:Entities.Integrateddecision):boolean {
            var self = this;


            return false;
        }
        public GrpIntegrateddecision_srchr__fsch_CultDeclCode_disabled(integrateddecision:Entities.Integrateddecision):boolean {
            var self = this;


            return false;
        }
        cachedLovModel_GrpIntegrateddecision_srchr__fsch_CultDeclCode:ModelLovCultivationBase;
        public showLov_GrpIntegrateddecision_srchr__fsch_CultDeclCode() {
            var self = this;
            var uimodel:NpTypes.IUIModel = self.modelGrpIntegrateddecision._fsch_CultDeclCode;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Cultivation';
            dialogOptions.previousModel= self.cachedLovModel_GrpIntegrateddecision_srchr__fsch_CultDeclCode;
            dialogOptions.className = "ControllerLovCultivation";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var cultivation1:Entities.Cultivation =   <Entities.Cultivation>selectedEntity;
                uimodel.clearAllErrors();
                if(false && isVoid(cultivation1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(cultivation1) && cultivation1.isEqual(self.modelGrpIntegrateddecision.fsch_CultDeclCode))
                    return;
                self.modelGrpIntegrateddecision.fsch_CultDeclCode = cultivation1;
            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovCultivationBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpIntegrateddecision_srchr__fsch_CultDeclCode = lovModel;
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovCultivationBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "Cultivation/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_name)) {
                        paramData['fsch_CultivationLov_name'] = lovModel.fsch_CultivationLov_name;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_CultivationLov_code)) {
                        paramData['fsch_CultivationLov_code'] = lovModel.fsch_CultivationLov_code;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['name'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/Cultivation.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public GrpIntegrateddecision_0_disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpIntegrateddecision_0_invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpIntegrateddecision_0_0_disabled(integrateddecision:Entities.Integrateddecision):boolean {
            var self = this;
            if (integrateddecision === undefined || integrateddecision === null)
                return true;


            

            var isContainerControlDisabled   = self.GrpIntegrateddecision_0_disabled();
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public exportIntegratedDecisionsToCSV(integrateddecision:Entities.Integrateddecision) {
            var self = this;
            console.log("Method: exportIntegratedDecisionsToCSV() called")
            console.log(integrateddecision);

        }
        public GrpIntegrateddecision_0_1_disabled(integrateddecision:Entities.Integrateddecision):boolean {
            var self = this;
            if (integrateddecision === undefined || integrateddecision === null)
                return true;


            

            var isContainerControlDisabled   = self.GrpIntegrateddecision_0_disabled();
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public actionPushToCommonsAPI(integrateddecision:Entities.Integrateddecision) {
            var self = this;
            console.log("Method: actionPushToCommonsAPI() called")
            console.log(integrateddecision);

        }
        public GrpIntegrateddecision_1_disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpIntegrateddecision_1_invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public XartoReg_disabled():boolean { 
            if (false)
                return true;
            var parControl = this.GrpIntegrateddecision_1_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public XartoReg_invisible():boolean { 
            if (false)
                return true;
            var parControl = this.GrpIntegrateddecision_1_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController.GrpDema_0_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController.GrpDema_0_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            return true;
        }
        public _deleteIsDisabled(integrateddecision:Entities.Integrateddecision):boolean  {
            return true;
        }

    }

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
