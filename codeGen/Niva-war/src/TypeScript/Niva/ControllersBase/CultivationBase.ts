/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/CultivationBase.ts" />
/// <reference path="../EntitiesBase/CoverTypeBase.ts" />
/// <reference path="../Controllers/Cultivation.ts" />
module Controllers.Cultivation {
    export class PageModelBase extends AbstractPageModel {
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        modelGrpCultivation:ModelGrpCultivation;
        controller: PageController;
        constructor(public $scope: IPageScope) { super($scope); }
    }


    export interface IPageScopeBase extends IAbstractPageScope {
        globals: Globals;
        HeaderSection_0_disabled():boolean; 
        HeaderSection_0_invisible():boolean; 
        createFileUploadComponent_Cultivation_impCults_id():NpTypes.NpFileUpload; 
        HeaderSection_0_1_disabled():boolean; 
        deleteSelectedCrops():void; 
        _saveIsDisabled():boolean; 
        _cancelIsDisabled():boolean; 
        pageModel : PageModel;
        onSaveBtnAction(): void;
    }

    export class PageControllerBase extends AbstractPageController {
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model:PageModel)
        {
            super($scope, $http, $timeout, Plato, model);
            var self: PageControllerBase = this;
            model.controller = <PageController>self;
            $scope.pageModel = self.model;

            $scope.HeaderSection_0_disabled = 
                () => {
                    return self.HeaderSection_0_disabled();
                };
            $scope.HeaderSection_0_invisible = 
                () => {
                    return self.HeaderSection_0_invisible();
                };
            $scope.createFileUploadComponent_Cultivation_impCults_id = 
                () => {
            return self.createFileUploadComponent_Cultivation_impCults_id();
                };
            $scope.HeaderSection_0_1_disabled = 
                () => {
                    return self.HeaderSection_0_1_disabled();
                };
            $scope.deleteSelectedCrops = 
                () => {
            self.deleteSelectedCrops();
                };
            $scope._saveIsDisabled = 
                () => {
                    return self._saveIsDisabled();
                };
            $scope._cancelIsDisabled = 
                () => {
                    return self._cancelIsDisabled();
                };

            $scope.onSaveBtnAction = () => { 
                self.onSaveBtnAction((response:NpTypes.SaveResponce) => {self.onSuccesfullSaveFromButton(response);});
            };
            


            $timeout(function() {
                $('#Cultivation').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                $('#NpMainContent').scrollTop(0);
            }, 0);

        }
        public get ControllerClassName(): string {
            return "PageController";
        }
        public get HtmlDivId(): string {
            return "Cultivation";
        }
    
        public _getPageTitle(): string {
            return "Crops List";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                ];
            }
            return this._items;
        }

        public HeaderSection_0_disabled():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public HeaderSection_0_invisible():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public impCults_id_disabled():boolean {
            var self = this;

            if (!self.$scope.globals.hasPrivilege("Niva_Cultivation_W"))
                return true; // 


            

            var isContainerControlDisabled   = self.HeaderSection_0_disabled();
            var disabledByProgrammerMethod = self.isExcelDisabled();
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public isExcelDisabled():boolean { 
            console.warn("Unimplemented function isExcelDisabled()");
            return false; 
        }
        public createFileUploadComponent_Cultivation_impCults_id() : NpTypes.NpFileUpload {
            var self = this;
            var ret =
                new NpTypes.NpFileUpload(
                    /*formDataAppendCallback:*/
                    (fd: FormData) => {
                    },
                    /*fileUpLoadedCallback:*/
                    (fileName: string, uploadResponseText: any) => {
                        var jsonResponse = JSON.parse(uploadResponseText);
                        if (jsonResponse.totalErrorRows === 0) {
                            messageBox(self.$scope, self.Plato, "MessageBox_Success_Title",
                                Messages.ImportExcel_MessageBox_Success_Message(fileName, jsonResponse.totalRows),
                                IconKind.INFO, [new Tuple2("OK", () => {}),], 0, 0, '30em');
                            self.successUpdate(fileName, jsonResponse);
                        } else {
                            messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title",
                                self.$scope.getALString("ImportExcel_MessageBox_Error_Message"),
                                IconKind.ERROR, [new Tuple2("ImportExcel_MessageBox_Error_ButtonLabel", () => {
                                    Utils.showExcelFileImportResultsDlg(self.$scope, self.Plato, jsonResponse.excelFileId, "ExcelFile", "ExcelError");
                                }),], 0, 0, '30em');
                        }
                    },
                    /*getLabel:*/() => "Import From Excel File",
                    /*isDisabled:*/() => self.impCults_id_disabled(),
                    /*isInvisible:*/() => false,
                    /*postURL:*/ "/Niva/rest/MainService/importExcel_Cultivation_impCults_id",
                    /*helpButtonAction*/ 
                    () => {
                        messageBox(self.$scope, self.Plato, "ImportExcel_MessageBox_Info_Title",
                            self.$scope.getALString("ImportExcel_MessageBox_Info_Message"),
                            IconKind.INFO, [new Tuple2("ImportExcel_MessageBox_Info_ButtonLabel", () => {
                                self.getImportExcel_Cultivation_impCults_id_SpecsAndSample();
                            }),], 0, 0, '30em');
                    },
                    /*extensions:*/ "xls|xlsx",
                    /*maximumSizeInKB:*/ 10240
                );
            return ret;
        }
        public successUpdate(fileName: string, jsonResponse: any):void { 
            console.warn("Unimplemented function successUpdate()");
        }
        public getImportExcel_Cultivation_impCults_id_SpecsAndSample() {
            var self = this;
            var wsPath = "MainService/importExcel_Cultivation_impCults_id_SpecsAndSample";
            var url = "/Niva/rest/" + wsPath;
            var paramData = {};
            paramData['globalLang'] = self.$scope.globals.globalLang;
            var filename = "Niva_Cultivation_SpecsAndSample_" + Utils.convertDateToString(new Date(), "yyyy_MM_dd_HHmm") + ".xls";

            NpTypes.AlertMessage.clearAlerts(self.PageModel);
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                    var data = base64DecToArr(response.data);
                    var blob = new Blob([data], { type: "application/octet-stream" });
                    saveAs(blob, filename);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                });

        }
        public HeaderSection_0_1_disabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = self.HeaderSection_0_disabled();
            var disabledByProgrammerMethod = self.isDeleteSelectedCropsDisabled();
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public isDeleteSelectedCropsDisabled():boolean { 
            console.warn("Unimplemented function isDeleteSelectedCropsDisabled()");
            return false; 
        }
        public deleteSelectedCrops() {
            var self = this;
            console.log("Method: deleteSelectedCrops() called")
        }
        public _saveIsDisabled():boolean {
            var self = this;

            if (!self.$scope.globals.hasPrivilege("Niva_Cultivation_W"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _cancelIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }

        public get pageModel():PageModel {
            var self = this;
            return self.model;
        }

        
        public update():void {
            var self = this;
            self.model.modelGrpCultivation.controller.updateUI();
        }

        public refreshVisibleEntities(newEntitiesIds: NpTypes.NewEntityId[]):void {
            var self = this;
            self.model.modelGrpCultivation.controller.refreshVisibleEntities(newEntitiesIds);
        }

        public refreshGroups(newEntitiesIds: NpTypes.NewEntityId[]): void {
            var self = this;
            self.model.modelGrpCultivation.controller.refreshVisibleEntities(newEntitiesIds);
        }

        _firstLevelGroupControllers: Array<AbstractGroupController>=undefined;
        public get FirstLevelGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelGroupControllers === undefined) {
                self._firstLevelGroupControllers = [
                    self.model.modelGrpCultivation.controller
                ];
            }
            return this._firstLevelGroupControllers;
        }

        _allGroupControllers: Array<AbstractGroupController>=undefined;
        public get AllGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._allGroupControllers === undefined) {
                self._allGroupControllers = [
                    self.model.modelGrpCultivation.controller
                ];
            }
            return this._allGroupControllers;
        }

        public getPageChanges(bForDelete:boolean = false):Array<ChangeToCommit> {
            var self = this;
            var pageChanges: Array<ChangeToCommit> = [];
                pageChanges = pageChanges.concat(self.model.modelGrpCultivation.controller.getChangesToCommit());
            return pageChanges;
        }

        private getSynchronizeChangesWithDbUrl():string { 
            return "/Niva/rest/MainService/synchronizeChangesWithDb_Cultivation"; 
        }
        
        public getSynchronizeChangesWithDbData(bForDelete:boolean = false):any {
            var self = this;
            var paramData:any = {}
            paramData.data = self.getPageChanges(bForDelete);
            return paramData;
        }


        public onSuccesfullSaveFromButton(response:NpTypes.SaveResponce) {
            var self = this;
            super.onSuccesfullSaveFromButton(response);
            if (!isVoid(response.warningMessages)) {
                NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
            }
            if (!isVoid(response.infoMessages)) {
                NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
            }

            self.model.modelGrpCultivation.controller.cleanUpAfterSave();
            self.$scope.globals.isCurrentTransactionDirty = false;
            self.refreshGroups(response.newEntitiesIds);
        }

        public onFailuredSave(data:any, status:number) {
            var self = this;
            if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
            var errors = Messages.dynamicMessage(data);
            NpTypes.AlertMessage.addDanger(self.model, errors);
            messageBox( self.$scope, self.Plato,"MessageBox_Attention_Title",
                "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>",
                IconKind.ERROR, [new Tuple2("OK", () => {}),], 0, 0,'50em');
        }
        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }
        
        public onSaveBtnAction(onSuccess:(response:NpTypes.SaveResponce)=>void): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            var errors = self.validatePage();
            if (errors.length > 0) {
                var errMessage = errors.join('<br>');
                NpTypes.AlertMessage.addDanger(self.model, errMessage);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title",
                    "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>",
                    IconKind.ERROR,[new Tuple2("OK", () => {}),], 0, 0,'50em');
                return;
            }

            if (self.$scope.globals.isCurrentTransactionDirty === false) {
                var jqSaveBtn = self.SaveBtnJQueryHandler;
                self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                return;
            }

            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData();
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success((response:NpTypes.SaveResponce, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status)
            });
        }



        public onPageUnload(actualNavigation: (pageScopeYouAreLeavingFrom?: NpTypes.IApplicationScope) => void) {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => {
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                actualNavigation(self.$scope);
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                actualNavigation(self.$scope);
            }
        }


    }


    

    // GROUP GrpCultivation

    export class ModelGrpCultivationBase extends Controllers.AbstractGroupTableModel {
        controller: ControllerGrpCultivation;
        public get cultId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Cultivation>this.selectedEntities[0]).cultId;
        }

        public set cultId(cultId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Cultivation>this.selectedEntities[0]).cultId = cultId_newVal;
        }

        public get _cultId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cultId;
        }

        public get name():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Cultivation>this.selectedEntities[0]).name;
        }

        public set name(name_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Cultivation>this.selectedEntities[0]).name = name_newVal;
        }

        public get _name():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._name;
        }

        public get code():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Cultivation>this.selectedEntities[0]).code;
        }

        public set code(code_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Cultivation>this.selectedEntities[0]).code = code_newVal;
        }

        public get _code():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._code;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Cultivation>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Cultivation>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get cotyId():Entities.CoverType {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Cultivation>this.selectedEntities[0]).cotyId;
        }

        public set cotyId(cotyId_newVal:Entities.CoverType) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Cultivation>this.selectedEntities[0]).cotyId = cotyId_newVal;
        }

        public get _cotyId():NpTypes.UIManyToOneModel<Entities.CoverType> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._cotyId;
        }

        public get exfiId():Entities.ExcelFile {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.Cultivation>this.selectedEntities[0]).exfiId;
        }

        public set exfiId(exfiId_newVal:Entities.ExcelFile) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.Cultivation>this.selectedEntities[0]).exfiId = exfiId_newVal;
        }

        public get _exfiId():NpTypes.UIManyToOneModel<Entities.ExcelFile> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._exfiId;
        }

        _fsch_name:NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get fsch_name():string {
            return this._fsch_name.value;
        }
        public set fsch_name(vl:string) {
            this._fsch_name.value = vl;
        }
        _fsch_code:NpTypes.UINumberModel = new NpTypes.UINumberModel(undefined);
        public get fsch_code():number {
            return this._fsch_code.value;
        }
        public set fsch_code(vl:number) {
            this._fsch_code.value = vl;
        }
        _fsch_cotyId:NpTypes.UIManyToOneModel<Entities.CoverType> = new NpTypes.UIManyToOneModel<Entities.CoverType>(undefined);
        public get fsch_cotyId():Entities.CoverType {
            return this._fsch_cotyId.value;
        }
        public set fsch_cotyId(vl:Entities.CoverType) {
            this._fsch_cotyId.value = vl;
        }
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpCultivation) { super($scope); }
    }



    export interface IScopeGrpCultivationBase extends Controllers.IAbstractTableGroupScope, IPageScope{
        globals: Globals;
        modelGrpCultivation : ModelGrpCultivation;
        _disabled():boolean; 
        _invisible():boolean; 
        GrpCultivation_srchr__fsch_name_disabled(cultivation:Entities.Cultivation):boolean; 
        GrpCultivation_srchr__fsch_code_disabled(cultivation:Entities.Cultivation):boolean; 
        GrpCultivation_srchr__fsch_cotyId_disabled(cultivation:Entities.Cultivation):boolean; 
        getDropDownItems_GrpCultivation_srchr__fsch_cotyId():Array<SelectItem<Entities.CoverType>>; 
        GrpCultivation_itm__name_disabled(cultivation:Entities.Cultivation):boolean; 
        GrpCultivation_itm__code_disabled(cultivation:Entities.Cultivation):boolean; 
        GrpCultivation_itm__cotyId_disabled(cultivation:Entities.Cultivation):boolean; 
        getDropDownItems_GrpCultivation_itm__cotyId(cultivation:Entities.Cultivation):Array<SelectItem<Entities.CoverType>>; 

    }



    export class ControllerGrpCultivationBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpCultivation,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpCultivation)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 20, maxLinesInHeader: 1});
            var self:ControllerGrpCultivationBase = this;
            model.controller = <ControllerGrpCultivation>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpCultivation = self.model;

            $scope._disabled = 
                () => {
                    return self._disabled();
                };
            $scope._invisible = 
                () => {
                    return self._invisible();
                };
            $scope.GrpCultivation_srchr__fsch_name_disabled = 
                (cultivation:Entities.Cultivation) => {
                    return self.GrpCultivation_srchr__fsch_name_disabled(cultivation);
                };
            $scope.GrpCultivation_srchr__fsch_code_disabled = 
                (cultivation:Entities.Cultivation) => {
                    return self.GrpCultivation_srchr__fsch_code_disabled(cultivation);
                };
            $scope.GrpCultivation_srchr__fsch_cotyId_disabled = 
                (cultivation:Entities.Cultivation) => {
                    return self.GrpCultivation_srchr__fsch_cotyId_disabled(cultivation);
                };
            $scope.getDropDownItems_GrpCultivation_srchr__fsch_cotyId = 
                ():Array<SelectItem<Entities.CoverType>> => {
                    return self.getDropDownItems_GrpCultivation_srchr__fsch_cotyId();
                };
            $scope.GrpCultivation_itm__name_disabled = 
                (cultivation:Entities.Cultivation) => {
                    return self.GrpCultivation_itm__name_disabled(cultivation);
                };
            $scope.GrpCultivation_itm__code_disabled = 
                (cultivation:Entities.Cultivation) => {
                    return self.GrpCultivation_itm__code_disabled(cultivation);
                };
            $scope.GrpCultivation_itm__cotyId_disabled = 
                (cultivation:Entities.Cultivation) => {
                    return self.GrpCultivation_itm__cotyId_disabled(cultivation);
                };
            $scope.getDropDownItems_GrpCultivation_itm__cotyId = 
                (cultivation:Entities.Cultivation):Array<SelectItem<Entities.CoverType>> => {
                    return self.getDropDownItems_GrpCultivation_itm__cotyId(cultivation);
                };


            $scope.pageModel.modelGrpCultivation = $scope.modelGrpCultivation;


            $scope.clearBtnAction = () => { 
                self.modelGrpCultivation.fsch_name = undefined;
                self.modelGrpCultivation.fsch_code = undefined;
                self.modelGrpCultivation.fsch_cotyId = undefined;
                self.updateGrid(0, false, true);
            };


            self.$timeout( () => {
                self.updateUI();

                },0);

    /*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.Cultivation[] , oldVisible:Entities.Cultivation[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpCultivation";
        }
        public get HtmlDivId(): string {
            return "Cultivation_ControllerGrpCultivation";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new TextItem (
                        (ent?: NpTypes.IBaseEntity) => 'Name',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_name,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_name,  
                        (ent?: NpTypes.IBaseEntity) => false, 
                        (vl: string, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new NumberItem (
                        (ent?: NpTypes.IBaseEntity) => 'Code',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_code,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_code,  
                        (ent?: NpTypes.IBaseEntity) => false, //isRequired
                        (vl: number, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined,
                        0),
                    new LovItem (
                        (ent?: NpTypes.IBaseEntity) => 'Cover Type',
                        true ,
                        (ent?: NpTypes.IBaseEntity) => self.model.fsch_cotyId,  
                        (ent?: NpTypes.IBaseEntity) => self.model._fsch_cotyId,  
                        (ent?: NpTypes.IBaseEntity) => false, //isRequired
                        (vl: Entities.CoverType, ent?: NpTypes.IBaseEntity) => new NpTypes.ValidationResult(true, "")),
                    new TextItem (
                        (ent?: Entities.Cultivation) => 'Crop Name',
                        false ,
                        (ent?: Entities.Cultivation) => ent.name,  
                        (ent?: Entities.Cultivation) => ent._name,  
                        (ent?: Entities.Cultivation) => true, 
                        (vl: string, ent?: Entities.Cultivation) => new NpTypes.ValidationResult(true, ""), 
                        undefined),
                    new NumberItem (
                        (ent?: Entities.Cultivation) => 'Code',
                        false ,
                        (ent?: Entities.Cultivation) => ent.code,  
                        (ent?: Entities.Cultivation) => ent._code,  
                        (ent?: Entities.Cultivation) => true, //isRequired
                        (vl: number, ent?: Entities.Cultivation) => new NpTypes.ValidationResult(true, ""), 
                        undefined,
                        undefined,
                        0),
                    new LovItem (
                        (ent?: Entities.Cultivation) => 'Land Cover Name',
                        false ,
                        (ent?: Entities.Cultivation) => ent.cotyId,  
                        (ent?: Entities.Cultivation) => ent._cotyId,  
                        (ent?: Entities.Cultivation) => false, //isRequired
                        (vl: Entities.CoverType, ent?: Entities.Cultivation) => new NpTypes.ValidationResult(true, ""))
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Crops List";
        }

        public isMultiSelect(): boolean {
            return true;
        }

        public getCheckedEntities( func: (entList:Entities.Cultivation[]) => void) {
            var ids = this.getCheckedEntitiesIds();
            if (ids.length > 0) {
                Entities.Cultivation.getEntitiesFromIdsList(this, ids, (entList : Entities.Cultivation[]) => {
                    func(entList);
                });
            } else {
                func([]);
            }
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { width:'22', cellTemplate:"<div class=\"GridSpecialCheckBox\" ><input class=\"npGridSelectCheckBox\" type=\"checkbox\" data-ng-click=\"setSelectedRow(row.rowIndex);selectEntityCheckBoxClicked(row.entity)\" data-ng-checked=\"isEntitySelected(row.entity)\" > </input></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined},
                { cellClass:'cellToolTip', field:'name', displayName:'getALString("Crop Name", true)', requiredAsterisk:self.$scope.globals.hasPrivilege("Niva_Cultivation_W"), resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpCultivation_itm__name' data-ng-model='row.entity.name' data-np-ui-model='row.entity._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;name&quot;)' data-np-required='true' data-ng-readonly='GrpCultivation_itm__name_disabled(row.entity)' data-np-text='dummy' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'code', displayName:'getALString("Code", true)', requiredAsterisk:self.$scope.globals.hasPrivilege("Niva_Cultivation_W"), resizable:true, sortable:true, enableCellEdit:false, width:'8%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpCultivation_itm__code' data-ng-model='row.entity.code' data-np-ui-model='row.entity._code' data-ng-change='markEntityAsUpdated(row.entity,&quot;code&quot;)' data-np-required='true' data-ng-readonly='GrpCultivation_itm__code_disabled(row.entity)' data-np-number='dummy' data-np-decimals='0' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>"},
                { cellClass:'cellToolTip', field:'cotyId', displayName:'getALString("Land Cover Name", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <select name='GrpCultivation_itm__cotyId' data-ng-model='row.entity.cotyId' data-np-ui-model='row.entity._cotyId' data-ng-change='markEntityAsUpdated(row.entity,&quot;cotyId&quot;)' data-np-select='dummy' data-ng-options='x.value as x.label for x in getDropDownItems_GrpCultivation_itm__cotyId(row.entity) track by x.cotyId' data-ng-disabled='GrpCultivation_itm__cotyId_disabled(row.entity)' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpCultivation():ModelGrpCultivation {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "Cultivation";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = undefined;

        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.Cultivation.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.Cultivation {
            var self = this;
            return <Entities.Cultivation>self.$scope.modelGrpCultivation.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.Cultivation, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  false;

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_name)) {
                    paramData['fsch_name'] = self.modelGrpCultivation.fsch_name;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_code)) {
                    paramData['fsch_code'] = self.modelGrpCultivation.fsch_code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_cotyId) && !isVoid(self.modelGrpCultivation.fsch_cotyId.cotyId)) {
                    paramData['fsch_cotyId_cotyId'] = self.modelGrpCultivation.fsch_cotyId.cotyId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "Cultivation/findAllByCriteriaRange_CultivationGrpCultivation_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_name)) {
                    paramData['fsch_name'] = self.modelGrpCultivation.fsch_name;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_code)) {
                    paramData['fsch_code'] = self.modelGrpCultivation.fsch_code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_cotyId) && !isVoid(self.modelGrpCultivation.fsch_cotyId.cotyId)) {
                    paramData['fsch_cotyId_cotyId'] = self.modelGrpCultivation.fsch_cotyId.cotyId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "Cultivation/findAllByCriteriaRange_CultivationGrpCultivation";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_name)) {
                    paramData['fsch_name'] = self.modelGrpCultivation.fsch_name;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_code)) {
                    paramData['fsch_code'] = self.modelGrpCultivation.fsch_code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_cotyId) && !isVoid(self.modelGrpCultivation.fsch_cotyId.cotyId)) {
                    paramData['fsch_cotyId_cotyId'] = self.modelGrpCultivation.fsch_cotyId.cotyId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "Cultivation/findAllByCriteriaRange_CultivationGrpCultivation_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_name)) {
                    paramData['fsch_name'] = self.modelGrpCultivation.fsch_name;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_code)) {
                    paramData['fsch_code'] = self.modelGrpCultivation.fsch_code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_cotyId) && !isVoid(self.modelGrpCultivation.fsch_cotyId.cotyId)) {
                    paramData['fsch_cotyId_cotyId'] = self.modelGrpCultivation.fsch_cotyId.cotyId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: Tuple2<boolean, Entities.Cultivation[] > = undefined;
        private bNonContinuousCallersFuncs: Array<(items: Entities.Cultivation[]) => void> = [];


        public getMergedItems(func: (items: Entities.Cultivation[]) => void, bContinuousCaller:boolean=true, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.Cultivation[]):void {
                var mergedEntities = <Entities.Cultivation[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        map(e => <Entities.Cultivation>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }



                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache !== undefined &&
                        self.getMergedItems_cache.a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache = new Tuple2(true, undefined);

                    var wsPath = "Cultivation/findAllByCriteriaRange_CultivationGrpCultivation";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);


                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.Cultivation[]>self.getEntitiesFromJSON(response);
                            self.getMergedItems_cache.a = false;
                            self.getMergedItems_cache.b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache.a = false;
                            self.getMergedItems_cache.b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache.a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache.b;
                        processDBItems(dbEntities);
                    }
                }
        }

        public _expToExcelIsDisabled():boolean  {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                return true;
            }
            return false;
        }
        public expToExcelBtnAction_fileName() {
            var self = this;
            return "Crops List" + "_" + Utils.convertDateToString(new Date(), "yyyy_MM_dd_HHmm") + ".xls";
        }
        public exportToExcelBtnAction() {
            var self = this;
            var wsPath = "Cultivation/findAllByCriteriaRange_CultivationGrpCultivation_toExcel";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['__fields'] = self.getExcelFieldDefinitions();
            if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_name)) {
                paramData['fsch_name'] = self.modelGrpCultivation.fsch_name;
            }
            if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_code)) {
                paramData['fsch_code'] = self.modelGrpCultivation.fsch_code;
            }
            if (!isVoid(self) && !isVoid(self.modelGrpCultivation) && !isVoid(self.modelGrpCultivation.fsch_cotyId) && !isVoid(self.modelGrpCultivation.fsch_cotyId.cotyId)) {
                paramData['fsch_cotyId_cotyId'] = self.modelGrpCultivation.fsch_cotyId.cotyId;
            }

            NpTypes.AlertMessage.clearAlerts(self.PageModel);
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                    var data = base64DecToArr(response.data);
                    var blob = new Blob([data], { type: "application/octet-stream" });
                    saveAs(blob, self.expToExcelBtnAction_fileName());
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                });

        }









        public constructEntity(): Entities.Cultivation {
            var self = this;
            var ret = new Entities.Cultivation(
                /*cultId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*name:string*/ null,
                /*code:number*/ null,
                /*rowVersion:number*/ null,
                /*cotyId:Entities.CoverType*/ null,
                /*exfiId:Entities.ExcelFile*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.Cultivation = <Entities.Cultivation>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.Cultivation): Entities.Cultivation {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.Cultivation, calledByParent: boolean= false): Entities.Cultivation {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.Cultivation.Create();
            ret.updateInstance(src);
            ret.cultId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
            },1);

            return ret;
        }



        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.Cultivation, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        public _disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpCultivation_srchr__fsch_name_disabled(cultivation:Entities.Cultivation):boolean {
            var self = this;


            return false;
        }
        public GrpCultivation_srchr__fsch_code_disabled(cultivation:Entities.Cultivation):boolean {
            var self = this;


            return false;
        }
        public GrpCultivation_srchr__fsch_cotyId_disabled(cultivation:Entities.Cultivation):boolean {
            var self = this;


            return false;
        }
        GrpCultivation_srchr__fsch_cotyIdCache : ExpiredArrayResource<Entities.CoverType> = new ExpiredArrayResource<Entities.CoverType>();

        public getDropDownItems_GrpCultivation_srchr__fsch_cotyId():Array<SelectItem<Entities.CoverType>> {
            var self = this;
            var key:string = '#';
            return self.GrpCultivation_srchr__fsch_cotyIdCache.getArrayResource(
                key,
                function(newArray:Array<SelectItem<Entities.CoverType>>) {
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['toRowIndex'] = 100;
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url, paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                    success(function (result, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                        if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                        var nullObject:SelectItem<Entities.CoverType> = new SelectItem(null, self.$scope.getALString("DropDownList_ChooseOne"));
                        (<any>nullObject).cotyId = null;
                        newArray.push(nullObject);
                        var entities = Entities.CoverType.fromJSONComplete((<any>result).data);
                        _.each(entities, function(coverType:Entities.CoverType) {
                            var newItem:SelectItem<Entities.CoverType> = new SelectItem(coverType,coverType.name);
                            (<any>newItem).cotyId = coverType.cotyId;
                            newArray.push(newItem);
                        });
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                        if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                        NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                    });
                }
            );
        }
        public GrpCultivation_itm__name_disabled(cultivation:Entities.Cultivation):boolean {
            var self = this;
            if (cultivation === undefined || cultivation === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Cultivation_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(cultivation, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpCultivation_itm__code_disabled(cultivation:Entities.Cultivation):boolean {
            var self = this;
            if (cultivation === undefined || cultivation === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Cultivation_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(cultivation, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpCultivation_itm__cotyId_disabled(cultivation:Entities.Cultivation):boolean {
            var self = this;
            if (cultivation === undefined || cultivation === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_Cultivation_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(cultivation, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        GrpCultivation_itm__cotyIdCache : ExpiredArrayResource<Entities.CoverType> = new ExpiredArrayResource<Entities.CoverType>();

        public getDropDownItems_GrpCultivation_itm__cotyId(cultivation:Entities.Cultivation):Array<SelectItem<Entities.CoverType>> {
            var self = this;
            var key:string = '#';
            return self.GrpCultivation_itm__cotyIdCache.getArrayResource(
                key,
                function(newArray:Array<SelectItem<Entities.CoverType>>) {
                    var wsPath = "CoverType/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['toRowIndex'] = 100;
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url, paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                    success(function (result, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                        if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                        var nullObject:SelectItem<Entities.CoverType> = new SelectItem(null, self.$scope.getALString("DropDownList_ChooseOne"));
                        (<any>nullObject).cotyId = null;
                        newArray.push(nullObject);
                        var entities = Entities.CoverType.fromJSONComplete((<any>result).data);
                        _.each(entities, function(coverType:Entities.CoverType) {
                            var newItem:SelectItem<Entities.CoverType> = new SelectItem(coverType,coverType.name);
                            (<any>newItem).cotyId = coverType.cotyId;
                            newArray.push(newItem);
                        });
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                        if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                        NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                    });
                }
            );
        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            var self = this;
            if (!self.$scope.globals.hasPrivilege("Niva_Cultivation_W"))
                return true; // no write privilege

            

            var parEntityIsLocked   = false;
            if (parEntityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public _deleteIsDisabled(cultivation:Entities.Cultivation):boolean  {
            var self = this;
            if (cultivation === null || cultivation === undefined || cultivation.getEntityName() !== "Cultivation")
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_Cultivation_D"))
                return true; // no delete privilege;




            var entityIsLocked   = self.isEntityLocked(cultivation, LockKind.DeleteLock);
            if (entityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }


    }

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
