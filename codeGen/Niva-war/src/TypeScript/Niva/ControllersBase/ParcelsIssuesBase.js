var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/ParcelsIssuesBase.ts" />
/// <reference path="../EntitiesBase/GpDecisionBase.ts" />
/// <reference path="../EntitiesBase/GpRequestsContextsBase.ts" />
/// <reference path="../EntitiesBase/GpUploadBase.ts" />
/// <reference path="../EntitiesBase/ParcelsIssuesActivitiesBase.ts" />
/// <reference path="../EntitiesBase/ParcelClasBase.ts" />
/// <reference path="LovParcelClasBase.ts" />
/// <reference path="../EntitiesBase/GpRequestsProducersBase.ts" />
/// <reference path="LovGpRequestsProducersBase.ts" />
/// <reference path="../Controllers/ParcelsIssues.ts" />
var Controllers;
(function (Controllers) {
    var ParcelsIssues;
    (function (ParcelsIssues) {
        var PageModelBase = (function (_super) {
            __extends(PageModelBase, _super);
            function PageModelBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(PageModelBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return PageModelBase;
        })(Controllers.AbstractPageModel);
        ParcelsIssues.PageModelBase = PageModelBase;
        var PageControllerBase = (function (_super) {
            __extends(PageControllerBase, _super);
            function PageControllerBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelGroupControllers = undefined;
                this._allGroupControllers = undefined;
                var self = this;
                model.controller = self;
                $scope.pageModel = self.model;
                $scope._saveIsDisabled =
                    function () {
                        return self._saveIsDisabled();
                    };
                $scope._cancelIsDisabled =
                    function () {
                        return self._cancelIsDisabled();
                    };
                $scope.onSaveBtnAction = function () {
                    self.onSaveBtnAction(function (response) { self.onSuccesfullSaveFromButton(response); });
                };
                $scope.onNewBtnAction = function () {
                    self.onNewBtnAction();
                };
                $scope.onDeleteBtnAction = function () {
                    self.onDeleteBtnAction();
                };
                $timeout(function () {
                    $('#ParcelsIssues').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                    $('#NpMainContent').scrollTop(0);
                }, 0);
            }
            Object.defineProperty(PageControllerBase.prototype, "ControllerClassName", {
                get: function () {
                    return "PageController";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "HtmlDivId", {
                get: function () {
                    return "ParcelsIssues";
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._getPageTitle = function () {
                return "ParcelsIssues";
            };
            Object.defineProperty(PageControllerBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._saveIsDisabled = function () {
                var self = this;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // 
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            PageControllerBase.prototype._cancelIsDisabled = function () {
                var self = this;
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            Object.defineProperty(PageControllerBase.prototype, "pageModel", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.update = function () {
                var self = this;
                self.model.modelGrpParcelsIssues.controller.updateUI();
            };
            PageControllerBase.prototype.refreshVisibleEntities = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpParcelsIssues.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpGpDecision.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpGpRequestsContexts.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpGpUpload.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpParcelsIssuesActivities.controller.refreshVisibleEntities(newEntitiesIds);
            };
            PageControllerBase.prototype.refreshGroups = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpParcelsIssues.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpGpDecision.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpGpRequestsContexts.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpGpUpload.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpParcelsIssuesActivities.controller.refreshVisibleEntities(newEntitiesIds);
            };
            Object.defineProperty(PageControllerBase.prototype, "FirstLevelGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._firstLevelGroupControllers === undefined) {
                        self._firstLevelGroupControllers = [
                            self.model.modelGrpParcelsIssues.controller
                        ];
                    }
                    return this._firstLevelGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "AllGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._allGroupControllers === undefined) {
                        self._allGroupControllers = [
                            self.model.modelGrpParcelsIssues.controller,
                            self.model.modelGrpGpDecision.controller,
                            self.model.modelGrpGpRequestsContexts.controller,
                            self.model.modelGrpGpUpload.controller,
                            self.model.modelGrpParcelsIssuesActivities.controller
                        ];
                    }
                    return this._allGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.getPageChanges = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var pageChanges = [];
                if (bForDelete) {
                    pageChanges = pageChanges.concat(self.model.modelGrpParcelsIssues.controller.getChangesToCommitForDeletion());
                    pageChanges = pageChanges.concat(self.model.modelGrpParcelsIssues.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpGpDecision.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpGpRequestsContexts.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpGpUpload.controller.getDeleteChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpParcelsIssuesActivities.controller.getDeleteChangesToCommit());
                }
                else {
                    pageChanges = pageChanges.concat(self.model.modelGrpParcelsIssues.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpGpDecision.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpGpRequestsContexts.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpGpUpload.controller.getChangesToCommit());
                    pageChanges = pageChanges.concat(self.model.modelGrpParcelsIssuesActivities.controller.getChangesToCommit());
                }
                var hasParcelsIssues = pageChanges.some(function (change) { return change.entityName === "ParcelsIssues"; });
                if (!hasParcelsIssues) {
                    var validateEntity = new Controllers.ChangeToCommit(Controllers.ChangeStatus.UPDATE, 0, "ParcelsIssues", self.model.modelGrpParcelsIssues.controller.Current);
                    pageChanges = pageChanges.concat(validateEntity);
                }
                return pageChanges;
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbUrl = function () {
                return "/Niva/rest/MainService/synchronizeChangesWithDb_ParcelsIssues";
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbData = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var paramData = {};
                paramData.data = self.getPageChanges(bForDelete);
                return paramData;
            };
            PageControllerBase.prototype.onSuccesfullSaveFromButton = function (response) {
                var self = this;
                _super.prototype.onSuccesfullSaveFromButton.call(this, response);
                if (!isVoid(response.warningMessages)) {
                    NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
                }
                if (!isVoid(response.infoMessages)) {
                    NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
                }
                self.model.modelGrpParcelsIssues.controller.cleanUpAfterSave();
                self.model.modelGrpGpDecision.controller.cleanUpAfterSave();
                self.model.modelGrpGpRequestsContexts.controller.cleanUpAfterSave();
                self.model.modelGrpGpUpload.controller.cleanUpAfterSave();
                self.model.modelGrpParcelsIssuesActivities.controller.cleanUpAfterSave();
                self.$scope.globals.isCurrentTransactionDirty = false;
                self.refreshGroups(response.newEntitiesIds);
            };
            PageControllerBase.prototype.onFailuredSave = function (data, status) {
                var self = this;
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                var errors = Messages.dynamicMessage(data);
                NpTypes.AlertMessage.addDanger(self.model, errors);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
            };
            PageControllerBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            PageControllerBase.prototype.onSaveBtnAction = function (onSuccess) {
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                var errors = self.validatePage();
                if (errors.length > 0) {
                    var errMessage = errors.join('<br>');
                    NpTypes.AlertMessage.addDanger(self.model, errMessage);
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
                    return;
                }
                if (self.$scope.globals.isCurrentTransactionDirty === false) {
                    var jqSaveBtn = self.SaveBtnJQueryHandler;
                    self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                    return;
                }
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData();
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    if (!self.shownAsDialog()) {
                        var breadcrumbStepModel = self.$scope.getCurrentPageModel();
                        var newParcelsIssuesId = response.newEntitiesIds.firstOrNull(function (x) { return x.entityName === 'ParcelsIssues'; });
                        if (!isVoid(newParcelsIssuesId)) {
                            if (!isVoid(breadcrumbStepModel)) {
                                breadcrumbStepModel.selectedEntity = Entities.ParcelsIssues.CreateById(newParcelsIssuesId.databaseId);
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        else {
                            if (!isVoid(breadcrumbStepModel) && !(isVoid(breadcrumbStepModel.selectedEntity))) {
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        self.$scope.setCurrentPageModel(breadcrumbStepModel);
                    }
                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status);
                });
            };
            PageControllerBase.prototype.getInitialEntity = function () {
                if (this.shownAsDialog()) {
                    return this.dialogSelectedEntity();
                }
                else {
                    var breadCrumbStepModel = this.$scope.getPageModelByURL('/ParcelsIssues');
                    if (isVoid(breadCrumbStepModel)) {
                        return null;
                    }
                    else {
                        return isVoid(breadCrumbStepModel.selectedEntity) ? null : breadCrumbStepModel.selectedEntity;
                    }
                }
            };
            PageControllerBase.prototype.onPageUnload = function (actualNavigation) {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                actualNavigation(self.$scope);
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    actualNavigation(self.$scope);
                }
            };
            PageControllerBase.prototype.onNewBtnAction = function () {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                self.addNewRecord();
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () { self.addNewRecord(); }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    self.addNewRecord();
                }
            };
            PageControllerBase.prototype.addNewRecord = function () {
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                self.model.modelGrpParcelsIssues.controller.cleanUpAfterSave();
                self.model.modelGrpGpDecision.controller.cleanUpAfterSave();
                self.model.modelGrpGpRequestsContexts.controller.cleanUpAfterSave();
                self.model.modelGrpGpUpload.controller.cleanUpAfterSave();
                self.model.modelGrpParcelsIssuesActivities.controller.cleanUpAfterSave();
                self.model.modelGrpParcelsIssues.controller.createNewEntityAndAddToUI();
            };
            PageControllerBase.prototype.onDeleteBtnAction = function () {
                var self = this;
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("EntityDeletionWarningMsg"), IconKind.QUESTION, [
                    new Tuple2("MessageBox_Button_Yes", function () { self.deleteRecord(); }),
                    new Tuple2("MessageBox_Button_No", function () { })
                ], 1, 1);
            };
            PageControllerBase.prototype.deleteRecord = function () {
                var self = this;
                if (self.Mode === Controllers.EditMode.NEW) {
                    console.log("Delete pressed in a form while being in new mode!");
                    return;
                }
                NpTypes.AlertMessage.clearAlerts(self.model);
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData(true);
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.$scope.navigateBackward();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.model, Messages.dynamicMessage(data));
                });
            };
            Object.defineProperty(PageControllerBase.prototype, "Mode", {
                get: function () {
                    var self = this;
                    if (isVoid(self.model.modelGrpParcelsIssues) || isVoid(self.model.modelGrpParcelsIssues.controller))
                        return Controllers.EditMode.NEW;
                    return self.model.modelGrpParcelsIssues.controller.Mode;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._newIsDisabled = function () {
                var self = this;
                if (isVoid(self.model.modelGrpParcelsIssues) || isVoid(self.model.modelGrpParcelsIssues.controller))
                    return true;
                return self.model.modelGrpParcelsIssues.controller._newIsDisabled();
            };
            PageControllerBase.prototype._deleteIsDisabled = function () {
                var self = this;
                if (self.Mode === Controllers.EditMode.NEW)
                    return true;
                if (isVoid(self.model.modelGrpParcelsIssues) || isVoid(self.model.modelGrpParcelsIssues.controller))
                    return true;
                return self.model.modelGrpParcelsIssues.controller._deleteIsDisabled(self.model.modelGrpParcelsIssues.controller.Current);
            };
            return PageControllerBase;
        })(Controllers.AbstractPageController);
        ParcelsIssues.PageControllerBase = PageControllerBase;
        // GROUP GrpParcelsIssues
        var ModelGrpParcelsIssuesBase = (function (_super) {
            __extends(ModelGrpParcelsIssuesBase, _super);
            function ModelGrpParcelsIssuesBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "parcelsIssuesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].parcelsIssuesId;
                },
                set: function (parcelsIssuesId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].parcelsIssuesId = parcelsIssuesId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_parcelsIssuesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._parcelsIssuesId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "dteCreated", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].dteCreated;
                },
                set: function (dteCreated_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].dteCreated = dteCreated_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_dteCreated", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._dteCreated;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "status", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].status;
                },
                set: function (status_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].status = status_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_status", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._status;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "dteStatusUpdate", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].dteStatusUpdate;
                },
                set: function (dteStatusUpdate_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].dteStatusUpdate = dteStatusUpdate_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_dteStatusUpdate", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._dteStatusUpdate;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "typeOfIssue", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].typeOfIssue;
                },
                set: function (typeOfIssue_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].typeOfIssue = typeOfIssue_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_typeOfIssue", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._typeOfIssue;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "active", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].active;
                },
                set: function (active_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].active = active_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_active", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._active;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "pclaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].pclaId;
                },
                set: function (pclaId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].pclaId = pclaId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_pclaId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._pclaId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpParcelsIssuesBase;
        })(Controllers.AbstractGroupFormModel);
        ParcelsIssues.ModelGrpParcelsIssuesBase = ModelGrpParcelsIssuesBase;
        var ControllerGrpParcelsIssuesBase = (function (_super) {
            __extends(ControllerGrpParcelsIssuesBase, _super);
            function ControllerGrpParcelsIssuesBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                $scope.modelGrpParcelsIssues = self.model;
                var selectedEntity = this.$scope.pageModel.controller.getInitialEntity();
                if (isVoid(selectedEntity)) {
                    self.createNewEntityAndAddToUI();
                }
                else {
                    var clonedEntity = Entities.ParcelsIssues.Create();
                    clonedEntity.updateInstance(selectedEntity);
                    $scope.modelGrpParcelsIssues.visibleEntities[0] = clonedEntity;
                    $scope.modelGrpParcelsIssues.selectedEntities[0] = clonedEntity;
                }
                $scope.GrpParcelsIssues_itm__dteCreated_disabled =
                    function (parcelsIssues) {
                        return self.GrpParcelsIssues_itm__dteCreated_disabled(parcelsIssues);
                    };
                $scope.GrpParcelsIssues_itm__status_disabled =
                    function (parcelsIssues) {
                        return self.GrpParcelsIssues_itm__status_disabled(parcelsIssues);
                    };
                $scope.GrpParcelsIssues_itm__dteStatusUpdate_disabled =
                    function (parcelsIssues) {
                        return self.GrpParcelsIssues_itm__dteStatusUpdate_disabled(parcelsIssues);
                    };
                $scope.GrpParcelsIssues_itm__pclaId_disabled =
                    function (parcelsIssues) {
                        return self.GrpParcelsIssues_itm__pclaId_disabled(parcelsIssues);
                    };
                $scope.showLov_GrpParcelsIssues_itm__pclaId =
                    function (parcelsIssues) {
                        $timeout(function () {
                            self.showLov_GrpParcelsIssues_itm__pclaId(parcelsIssues);
                        }, 0);
                    };
                $scope.GrpParcelsIssues_0_disabled =
                    function () {
                        return self.GrpParcelsIssues_0_disabled();
                    };
                $scope.GrpParcelsIssues_0_invisible =
                    function () {
                        return self.GrpParcelsIssues_0_invisible();
                    };
                $scope.child_group_GrpGpDecision_isInvisible =
                    function () {
                        return self.child_group_GrpGpDecision_isInvisible();
                    };
                $scope.child_group_GrpGpRequestsContexts_isInvisible =
                    function () {
                        return self.child_group_GrpGpRequestsContexts_isInvisible();
                    };
                $scope.child_group_GrpParcelsIssuesActivities_isInvisible =
                    function () {
                        return self.child_group_GrpParcelsIssuesActivities_isInvisible();
                    };
                $scope.pageModel.modelGrpParcelsIssues = $scope.modelGrpParcelsIssues;
                /*
                        $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                            $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.ParcelsIssues[] , oldVisible:Entities.ParcelsIssues[]) => {
                                console.log("visible entities changed",newVisible);
                                self.onVisibleItemsChange(newVisible, oldVisible);
                            } )  ));
                */
                //bind entity child collection with child controller merged items
                Entities.ParcelsIssues.gpDecisionCollection = function (parcelsIssues, func) {
                    _this.model.modelGrpGpDecision.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, parcelsIssues);
                };
                //bind entity child collection with child controller merged items
                Entities.ParcelsIssues.gpRequestsContextsCollection = function (parcelsIssues, func) {
                    _this.model.modelGrpGpRequestsContexts.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, parcelsIssues);
                };
                //bind entity child collection with child controller merged items
                Entities.ParcelsIssues.parcelsIssuesActivitiesCollection = function (parcelsIssues, func) {
                    _this.model.modelGrpParcelsIssuesActivities.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, parcelsIssues);
                };
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                    //unbind entity child collection with child controller merged items
                    Entities.ParcelsIssues.gpDecisionCollection = null; //(parcelsIssues, func) => { }
                    //unbind entity child collection with child controller merged items
                    Entities.ParcelsIssues.gpRequestsContextsCollection = null; //(parcelsIssues, func) => { }
                    //unbind entity child collection with child controller merged items
                    Entities.ParcelsIssues.parcelsIssuesActivitiesCollection = null; //(parcelsIssues, func) => { }
                });
            }
            ControllerGrpParcelsIssuesBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpParcelsIssues";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "HtmlDivId", {
                get: function () {
                    return "ParcelsIssues_ControllerGrpParcelsIssues";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.DateItem(function (ent) { return 'dteCreated'; }, false, function (ent) { return ent.dteCreated; }, function (ent) { return ent._dteCreated; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined),
                            new Controllers.NumberItem(function (ent) { return 'Κατάσταση'; }, false, function (ent) { return ent.status; }, function (ent) { return ent._status; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, function () { return -32768; }, function () { return 32767; }, 0),
                            new Controllers.DateItem(function (ent) { return 'Ημερομηνία'; }, false, function (ent) { return ent.dteStatusUpdate; }, function (ent) { return ent._dteStatusUpdate; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined),
                            new Controllers.LovItem(function (ent) { return 'pclaId'; }, false, function (ent) { return ent.pclaId; }, function (ent) { return ent._pclaId; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); })
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "modelGrpParcelsIssues", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelsIssuesBase.prototype.getEntityName = function () {
                return "ParcelsIssues";
            };
            ControllerGrpParcelsIssuesBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
            };
            ControllerGrpParcelsIssuesBase.prototype.setCurrentFormPageBreadcrumpStepModel = function () {
                var self = this;
                if (self.$scope.pageModel.controller.shownAsDialog())
                    return;
                var breadCrumbStepModel = { selectedEntity: null };
                if (!isVoid(self.Current)) {
                    var selectedEntity = Entities.ParcelsIssues.Create();
                    selectedEntity.updateInstance(self.Current);
                    breadCrumbStepModel.selectedEntity = selectedEntity;
                }
                else {
                    breadCrumbStepModel.selectedEntity = null;
                }
                self.$scope.setCurrentPageModel(breadCrumbStepModel);
            };
            ControllerGrpParcelsIssuesBase.prototype.getEntitiesFromJSON = function (webResponse) {
                var _this = this;
                var entlist = Entities.ParcelsIssues.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelsIssues.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelsIssuesBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return false;
            };
            ControllerGrpParcelsIssuesBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.ParcelsIssues(
                /*parcelsIssuesId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*dteCreated:Date*/ null, 
                /*status:number*/ null, 
                /*dteStatusUpdate:Date*/ null, 
                /*rowVersion:number*/ null, 
                /*typeOfIssue:number*/ null, 
                /*active:boolean*/ null, 
                /*pclaId:Entities.ParcelClas*/ null);
                return ret;
            };
            ControllerGrpParcelsIssuesBase.prototype.createNewEntityAndAddToUI = function () {
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.$scope.modelGrpParcelsIssues.visibleEntities[0] = newEnt;
                self.$scope.modelGrpParcelsIssues.selectedEntities[0] = newEnt;
                return newEnt;
            };
            ControllerGrpParcelsIssuesBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpParcelsIssuesBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                var _this = this;
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.ParcelsIssues.Create();
                ret.updateInstance(src);
                ret.parcelsIssuesId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.visibleEntities[0] = ret;
                this.model.selectedEntities[0] = ret;
                this.$timeout(function () {
                    _this.modelGrpParcelsIssues.modelGrpGpDecision.controller.cloneAllEntitiesUnderParent(src, ret);
                    _this.modelGrpParcelsIssues.modelGrpGpRequestsContexts.controller.cloneAllEntitiesUnderParent(src, ret);
                    _this.modelGrpParcelsIssues.modelGrpParcelsIssuesActivities.controller.cloneAllEntitiesUnderParent(src, ret);
                }, 1);
                return ret;
            };
            Object.defineProperty(ControllerGrpParcelsIssuesBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [
                            self.modelGrpParcelsIssues.modelGrpGpDecision.controller,
                            self.modelGrpParcelsIssues.modelGrpGpRequestsContexts.controller,
                            self.modelGrpParcelsIssues.modelGrpParcelsIssuesActivities.controller
                        ];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelsIssuesBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                var _this = this;
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    self.modelGrpParcelsIssues.modelGrpGpDecision.controller.deleteAllEntitiesUnderParent(ent, function () {
                        self.modelGrpParcelsIssues.modelGrpGpRequestsContexts.controller.deleteAllEntitiesUnderParent(ent, function () {
                            self.modelGrpParcelsIssues.modelGrpParcelsIssuesActivities.controller.deleteAllEntitiesUnderParent(ent, function () {
                                _super.prototype.deleteEntity.call(_this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                            });
                        });
                    });
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        self.modelGrpParcelsIssues.modelGrpGpDecision.controller.deleteNewEntitiesUnderParent(ent);
                        self.modelGrpParcelsIssues.modelGrpGpRequestsContexts.controller.deleteNewEntitiesUnderParent(ent);
                        self.modelGrpParcelsIssues.modelGrpParcelsIssuesActivities.controller.deleteNewEntitiesUnderParent(ent);
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpParcelsIssuesBase.prototype.GrpParcelsIssues_itm__dteCreated_disabled = function (parcelsIssues) {
                var self = this;
                if (parcelsIssues === undefined || parcelsIssues === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(parcelsIssues, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpParcelsIssuesBase.prototype.GrpParcelsIssues_itm__status_disabled = function (parcelsIssues) {
                var self = this;
                if (parcelsIssues === undefined || parcelsIssues === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(parcelsIssues, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpParcelsIssuesBase.prototype.GrpParcelsIssues_itm__dteStatusUpdate_disabled = function (parcelsIssues) {
                var self = this;
                if (parcelsIssues === undefined || parcelsIssues === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(parcelsIssues, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpParcelsIssuesBase.prototype.GrpParcelsIssues_itm__pclaId_disabled = function (parcelsIssues) {
                var self = this;
                if (parcelsIssues === undefined || parcelsIssues === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(parcelsIssues, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpParcelsIssuesBase.prototype.showLov_GrpParcelsIssues_itm__pclaId = function (parcelsIssues) {
                var self = this;
                if (parcelsIssues === undefined)
                    return;
                var uimodel = parcelsIssues._pclaId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'ParcelClas';
                dialogOptions.previousModel = self.cachedLovModel_GrpParcelsIssues_itm__pclaId;
                dialogOptions.className = "ControllerLovParcelClas";
                dialogOptions.onSelect = function (selectedEntity) {
                    var parcelClas1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (true && isVoid(parcelClas1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(parcelClas1) && parcelClas1.isEqual(parcelsIssues.pclaId))
                        return;
                    parcelsIssues.pclaId = parcelClas1;
                    self.markEntityAsUpdated(parcelsIssues, 'GrpParcelsIssues_itm__pclaId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpParcelsIssues_itm__pclaId = lovModel;
                    var wsPath = "ParcelClas/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred)) {
                        paramData['fsch_ParcelClasLov_probPred'] = lovModel.fsch_ParcelClasLov_probPred;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred2)) {
                        paramData['fsch_ParcelClasLov_probPred2'] = lovModel.fsch_ParcelClasLov_probPred2;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_prodCode)) {
                        paramData['fsch_ParcelClasLov_prodCode'] = lovModel.fsch_ParcelClasLov_prodCode;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_parcIdentifier)) {
                        paramData['fsch_ParcelClasLov_parcIdentifier'] = lovModel.fsch_ParcelClasLov_parcIdentifier;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "ParcelClas/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred)) {
                        paramData['fsch_ParcelClasLov_probPred'] = lovModel.fsch_ParcelClasLov_probPred;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred2)) {
                        paramData['fsch_ParcelClasLov_probPred2'] = lovModel.fsch_ParcelClasLov_probPred2;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_prodCode)) {
                        paramData['fsch_ParcelClasLov_prodCode'] = lovModel.fsch_ParcelClasLov_prodCode;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_parcIdentifier)) {
                        paramData['fsch_ParcelClasLov_parcIdentifier'] = lovModel.fsch_ParcelClasLov_parcIdentifier;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred)) {
                        paramData['fsch_ParcelClasLov_probPred'] = lovModel.fsch_ParcelClasLov_probPred;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_probPred2)) {
                        paramData['fsch_ParcelClasLov_probPred2'] = lovModel.fsch_ParcelClasLov_probPred2;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_prodCode)) {
                        paramData['fsch_ParcelClasLov_prodCode'] = lovModel.fsch_ParcelClasLov_prodCode;
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_ParcelClasLov_parcIdentifier)) {
                        paramData['fsch_ParcelClasLov_parcIdentifier'] = lovModel.fsch_ParcelClasLov_parcIdentifier;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['probPred'] = true;
                dialogOptions.shownCols['probPred2'] = true;
                dialogOptions.shownCols['prodCode'] = true;
                dialogOptions.shownCols['parcIdentifier'] = true;
                dialogOptions.shownCols['parcCode'] = true;
                dialogOptions.shownCols['geom4326'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/ParcelClas.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpParcelsIssuesBase.prototype.GrpParcelsIssues_0_disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelsIssuesBase.prototype.GrpParcelsIssues_0_invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelsIssuesBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelsIssuesBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelsIssuesBase.prototype._newIsDisabled = function () {
                var self = this;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var parEntityIsLocked = false;
                if (parEntityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpParcelsIssuesBase.prototype._deleteIsDisabled = function (parcelsIssues) {
                var self = this;
                if (parcelsIssues === null || parcelsIssues === undefined || parcelsIssues.getEntityName() !== "ParcelsIssues")
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_D"))
                    return true; // no delete privilege;
                var entityIsLocked = self.isEntityLocked(parcelsIssues, Controllers.LockKind.DeleteLock);
                if (entityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpParcelsIssuesBase.prototype.child_group_GrpGpDecision_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpGpDecision === undefined) || (self.model.modelGrpGpDecision.controller === undefined))
                    return false;
                return self.model.modelGrpGpDecision.controller._isInvisible();
            };
            ControllerGrpParcelsIssuesBase.prototype.child_group_GrpGpRequestsContexts_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpGpRequestsContexts === undefined) || (self.model.modelGrpGpRequestsContexts.controller === undefined))
                    return false;
                return self.model.modelGrpGpRequestsContexts.controller._isInvisible();
            };
            ControllerGrpParcelsIssuesBase.prototype.child_group_GrpParcelsIssuesActivities_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpParcelsIssuesActivities === undefined) || (self.model.modelGrpParcelsIssuesActivities.controller === undefined))
                    return false;
                return self.model.modelGrpParcelsIssuesActivities.controller._isInvisible();
            };
            return ControllerGrpParcelsIssuesBase;
        })(Controllers.AbstractGroupFormController);
        ParcelsIssues.ControllerGrpParcelsIssuesBase = ControllerGrpParcelsIssuesBase;
        // GROUP GrpGpDecision
        var ModelGrpGpDecisionBase = (function (_super) {
            __extends(ModelGrpGpDecisionBase, _super);
            function ModelGrpGpDecisionBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
                this._fsch_cropOk = new NpTypes.UINumberModel(undefined);
                this._fsch_landcoverOk = new NpTypes.UINumberModel(undefined);
            }
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "gpDecisionsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].gpDecisionsId;
                },
                set: function (gpDecisionsId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].gpDecisionsId = gpDecisionsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_gpDecisionsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._gpDecisionsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "cropOk", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cropOk;
                },
                set: function (cropOk_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cropOk = cropOk_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_cropOk", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cropOk;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "landcoverOk", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].landcoverOk;
                },
                set: function (landcoverOk_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].landcoverOk = landcoverOk_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_landcoverOk", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._landcoverOk;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "dteInsert", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].dteInsert;
                },
                set: function (dteInsert_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].dteInsert = dteInsert_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_dteInsert", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._dteInsert;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "usrInsert", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].usrInsert;
                },
                set: function (usrInsert_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].usrInsert = usrInsert_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_usrInsert", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._usrInsert;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "parcelsIssuesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].parcelsIssuesId;
                },
                set: function (parcelsIssuesId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].parcelsIssuesId = parcelsIssuesId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_parcelsIssuesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._parcelsIssuesId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cultId;
                },
                set: function (cultId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cultId = cultId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_cultId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cultId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].cotyId;
                },
                set: function (cotyId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].cotyId = cotyId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_cotyId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._cotyId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "fsch_cropOk", {
                get: function () {
                    return this._fsch_cropOk.value;
                },
                set: function (vl) {
                    this._fsch_cropOk.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "fsch_landcoverOk", {
                get: function () {
                    return this._fsch_landcoverOk.value;
                },
                set: function (vl) {
                    this._fsch_landcoverOk.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpDecisionBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpGpDecisionBase;
        })(Controllers.AbstractGroupTableModel);
        ParcelsIssues.ModelGrpGpDecisionBase = ModelGrpGpDecisionBase;
        var ControllerGrpGpDecisionBase = (function (_super) {
            __extends(ControllerGrpGpDecisionBase, _super);
            function ControllerGrpGpDecisionBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpGpDecision = self.model;
                $scope._disabled =
                    function () {
                        return self._disabled();
                    };
                $scope._invisible =
                    function () {
                        return self._invisible();
                    };
                $scope.GrpGpDecision_srchr__fsch_cropOk_disabled =
                    function (gpDecision) {
                        return self.GrpGpDecision_srchr__fsch_cropOk_disabled(gpDecision);
                    };
                $scope.GrpGpDecision_srchr__fsch_landcoverOk_disabled =
                    function (gpDecision) {
                        return self.GrpGpDecision_srchr__fsch_landcoverOk_disabled(gpDecision);
                    };
                $scope.GrpGpDecision_itm__cropOk_disabled =
                    function (gpDecision) {
                        return self.GrpGpDecision_itm__cropOk_disabled(gpDecision);
                    };
                $scope.GrpGpDecision_itm__landcoverOk_disabled =
                    function (gpDecision) {
                        return self.GrpGpDecision_itm__landcoverOk_disabled(gpDecision);
                    };
                $scope.pageModel.modelGrpGpDecision = $scope.modelGrpGpDecision;
                $scope.clearBtnAction = function () {
                    self.modelGrpGpDecision.fsch_cropOk = undefined;
                    self.modelGrpGpDecision.fsch_landcoverOk = undefined;
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpParcelsIssues.modelGrpGpDecision = $scope.modelGrpGpDecision;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpParcelsIssues.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.GpDecision[] , oldVisible:Entities.GpDecision[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            ControllerGrpGpDecisionBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpGpDecision";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "HtmlDivId", {
                get: function () {
                    return "ParcelsIssues_ControllerGrpGpDecision";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.NumberItem(function (ent) { return 'cropOk'; }, true, function (ent) { return self.model.fsch_cropOk; }, function (ent) { return self.model._fsch_cropOk; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, function () { return -32768; }, function () { return 32767; }, 0),
                            new Controllers.NumberItem(function (ent) { return 'landcoverOk'; }, true, function (ent) { return self.model.fsch_landcoverOk; }, function (ent) { return self.model._fsch_landcoverOk; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, function () { return -32768; }, function () { return 32767; }, 0),
                            new Controllers.NumberItem(function (ent) { return 'cropOk'; }, false, function (ent) { return ent.cropOk; }, function (ent) { return ent._cropOk; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, function () { return -32768; }, function () { return 32767; }, 0),
                            new Controllers.NumberItem(function (ent) { return 'landcoverOk'; }, false, function (ent) { return ent.landcoverOk; }, function (ent) { return ent._landcoverOk; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, function () { return -32768; }, function () { return 32767; }, 0)
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpDecisionBase.prototype.gridTitle = function () {
                return "GpDecision";
            };
            ControllerGrpGpDecisionBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpGpDecisionBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'cropOk', displayName: 'getALString("cropOk", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpDecision_itm__cropOk' data-ng-model='row.entity.cropOk' data-np-ui-model='row.entity._cropOk' data-ng-change='markEntityAsUpdated(row.entity,&quot;cropOk&quot;)' data-ng-readonly='GrpGpDecision_itm__cropOk_disabled(row.entity)' data-np-number='dummy' data-np-min='-32768' data-np-max='32767' data-np-decimals='0' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'landcoverOk', displayName: 'getALString("landcoverOk", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpDecision_itm__landcoverOk' data-ng-model='row.entity.landcoverOk' data-np-ui-model='row.entity._landcoverOk' data-ng-change='markEntityAsUpdated(row.entity,&quot;landcoverOk&quot;)' data-ng-readonly='GrpGpDecision_itm__landcoverOk_disabled(row.entity)' data-np-number='dummy' data-np-min='-32768' data-np-max='32767' data-np-decimals='0' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "modelGrpGpDecision", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpDecisionBase.prototype.getEntityName = function () {
                return "GpDecision";
            };
            ControllerGrpGpDecisionBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerGrpGpDecisionBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.GpDecision.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.parcelsIssuesId = _this.Parent;
                    }
                    else {
                        x.parcelsIssuesId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpGpDecision.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpDecisionBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpParcelsIssues.controller.isEntityLocked(cur.parcelsIssuesId, lockKind);
            };
            ControllerGrpGpDecisionBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_cropOk)) {
                    paramData['fsch_cropOk'] = self.modelGrpGpDecision.fsch_cropOk;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_landcoverOk)) {
                    paramData['fsch_landcoverOk'] = self.modelGrpGpDecision.fsch_landcoverOk;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpGpDecisionBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "GpDecision/findAllByCriteriaRange_ParcelsIssuesGrpGpDecision_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_cropOk)) {
                    paramData['fsch_cropOk'] = self.modelGrpGpDecision.fsch_cropOk;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_landcoverOk)) {
                    paramData['fsch_landcoverOk'] = self.modelGrpGpDecision.fsch_landcoverOk;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpGpDecisionBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "GpDecision/findAllByCriteriaRange_ParcelsIssuesGrpGpDecision";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_cropOk)) {
                    paramData['fsch_cropOk'] = self.modelGrpGpDecision.fsch_cropOk;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_landcoverOk)) {
                    paramData['fsch_landcoverOk'] = self.modelGrpGpDecision.fsch_landcoverOk;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpGpDecisionBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "GpDecision/findAllByCriteriaRange_ParcelsIssuesGrpGpDecision_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_cropOk)) {
                    paramData['fsch_cropOk'] = self.modelGrpGpDecision.fsch_cropOk;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpDecision) && !isVoid(self.modelGrpGpDecision.fsch_landcoverOk)) {
                    paramData['fsch_landcoverOk'] = self.modelGrpGpDecision.fsch_landcoverOk;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpGpDecisionBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.parcelsIssuesId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "GpDecision/findAllByCriteriaRange_ParcelsIssuesGrpGpDecision";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['parcelsIssuesId_parcelsIssuesId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpGpDecisionBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.parcelsIssuesId === undefined)
                    return false;
                return x.parcelsIssuesId.getKey() === this.Parent.getKey();
            };
            ControllerGrpGpDecisionBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.parcelsIssuesId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelsIssues.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "parcelsIssuesId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelsIssues.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpDecisionBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.GpDecision(
                /*gpDecisionsId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*cropOk:number*/ null, 
                /*landcoverOk:number*/ null, 
                /*dteInsert:Date*/ null, 
                /*usrInsert:string*/ null, 
                /*rowVersion:number*/ null, 
                /*parcelsIssuesId:Entities.ParcelsIssues*/ null, 
                /*cultId:Entities.Cultivation*/ null, 
                /*cotyId:Entities.CoverType*/ null);
                return ret;
            };
            ControllerGrpGpDecisionBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.parcelsIssuesId = self.parcelsIssuesId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpGpDecisionBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpGpDecisionBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.GpDecision.Create();
                ret.updateInstance(src);
                ret.gpDecisionsId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            ControllerGrpGpDecisionBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (gpDecisionList) {
                    gpDecisionList.forEach(function (gpDecision) {
                        var gpDecisionCloned = _this.cloneEntity_internal(gpDecision, true);
                        gpDecisionCloned.parcelsIssuesId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpGpDecisionBase.prototype.deleteNewEntitiesUnderParent = function (parcelsIssues) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var gpDecision = change.a;
                    if (parcelsIssues.getKey() === gpDecision.parcelsIssuesId.getKey()) {
                        toBeDeleted.push(gpDecision);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                });
            };
            ControllerGrpGpDecisionBase.prototype.deleteAllEntitiesUnderParent = function (parcelsIssues, afterDeleteAction) {
                var self = this;
                function deleteGpDecisionList(gpDecisionList) {
                    if (gpDecisionList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = gpDecisionList[0];
                        var tail = gpDecisionList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteGpDecisionList(tail);
                        });
                    }
                }
                this.getMergedItems(function (gpDecisionList) {
                    deleteGpDecisionList(gpDecisionList);
                }, false, parcelsIssues);
            };
            Object.defineProperty(ControllerGrpGpDecisionBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpDecisionBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpGpDecisionBase.prototype._disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpDecisionBase.prototype._invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpDecisionBase.prototype.GrpGpDecision_srchr__fsch_cropOk_disabled = function (gpDecision) {
                var self = this;
                return false;
            };
            ControllerGrpGpDecisionBase.prototype.GrpGpDecision_srchr__fsch_landcoverOk_disabled = function (gpDecision) {
                var self = this;
                return false;
            };
            ControllerGrpGpDecisionBase.prototype.GrpGpDecision_itm__cropOk_disabled = function (gpDecision) {
                var self = this;
                if (gpDecision === undefined || gpDecision === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(gpDecision, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpGpDecisionBase.prototype.GrpGpDecision_itm__landcoverOk_disabled = function (gpDecision) {
                var self = this;
                if (gpDecision === undefined || gpDecision === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(gpDecision, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpGpDecisionBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController.GrpParcelsIssues_0_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpDecisionBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController.GrpParcelsIssues_0_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpDecisionBase.prototype._newIsDisabled = function () {
                var self = this;
                if (self.Parent === null || self.Parent === undefined)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var parEntityIsLocked = self.ParentController.isEntityLocked(self.Parent, Controllers.LockKind.UpdateLock);
                if (parEntityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpGpDecisionBase.prototype._deleteIsDisabled = function (gpDecision) {
                var self = this;
                if (gpDecision === null || gpDecision === undefined || gpDecision.getEntityName() !== "GpDecision")
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_D"))
                    return true; // no delete privilege;
                var entityIsLocked = self.isEntityLocked(gpDecision, Controllers.LockKind.DeleteLock);
                if (entityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            return ControllerGrpGpDecisionBase;
        })(Controllers.AbstractGroupTableController);
        ParcelsIssues.ControllerGrpGpDecisionBase = ControllerGrpGpDecisionBase;
        // GROUP GrpGpRequestsContexts
        var ModelGrpGpRequestsContextsBase = (function (_super) {
            __extends(ModelGrpGpRequestsContextsBase, _super);
            function ModelGrpGpRequestsContextsBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
                this._fsch_type = new NpTypes.UIStringModel(undefined);
                this._fsch_label = new NpTypes.UIStringModel(undefined);
                this._fsch_comment = new NpTypes.UIStringModel(undefined);
                this._fsch_geomHexewkb = new NpTypes.UIStringModel(undefined);
            }
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "gpRequestsContextsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].gpRequestsContextsId;
                },
                set: function (gpRequestsContextsId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].gpRequestsContextsId = gpRequestsContextsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_gpRequestsContextsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._gpRequestsContextsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "type", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].type;
                },
                set: function (type_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].type = type_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_type", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._type;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "label", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].label;
                },
                set: function (label_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].label = label_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_label", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._label;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "comment", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].comment;
                },
                set: function (comment_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].comment = comment_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_comment", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._comment;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "geomHexewkb", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].geomHexewkb;
                },
                set: function (geomHexewkb_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].geomHexewkb = geomHexewkb_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_geomHexewkb", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._geomHexewkb;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "referencepoint", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].referencepoint;
                },
                set: function (referencepoint_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].referencepoint = referencepoint_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_referencepoint", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._referencepoint;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "hash", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].hash;
                },
                set: function (hash_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].hash = hash_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_hash", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._hash;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "gpRequestsProducersId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].gpRequestsProducersId;
                },
                set: function (gpRequestsProducersId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].gpRequestsProducersId = gpRequestsProducersId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_gpRequestsProducersId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._gpRequestsProducersId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "parcelsIssuesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].parcelsIssuesId;
                },
                set: function (parcelsIssuesId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].parcelsIssuesId = parcelsIssuesId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_parcelsIssuesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._parcelsIssuesId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "fsch_type", {
                get: function () {
                    return this._fsch_type.value;
                },
                set: function (vl) {
                    this._fsch_type.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "fsch_label", {
                get: function () {
                    return this._fsch_label.value;
                },
                set: function (vl) {
                    this._fsch_label.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "fsch_comment", {
                get: function () {
                    return this._fsch_comment.value;
                },
                set: function (vl) {
                    this._fsch_comment.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "fsch_geomHexewkb", {
                get: function () {
                    return this._fsch_geomHexewkb.value;
                },
                set: function (vl) {
                    this._fsch_geomHexewkb.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpRequestsContextsBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpGpRequestsContextsBase;
        })(Controllers.AbstractGroupTableModel);
        ParcelsIssues.ModelGrpGpRequestsContextsBase = ModelGrpGpRequestsContextsBase;
        var ControllerGrpGpRequestsContextsBase = (function (_super) {
            __extends(ControllerGrpGpRequestsContextsBase, _super);
            function ControllerGrpGpRequestsContextsBase($scope, $http, $timeout, Plato, model) {
                var _this = this;
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpGpRequestsContexts = self.model;
                $scope._disabled =
                    function () {
                        return self._disabled();
                    };
                $scope._invisible =
                    function () {
                        return self._invisible();
                    };
                $scope.GrpGpRequestsContexts_srchr__fsch_type_disabled =
                    function (gpRequestsContexts) {
                        return self.GrpGpRequestsContexts_srchr__fsch_type_disabled(gpRequestsContexts);
                    };
                $scope.GrpGpRequestsContexts_srchr__fsch_label_disabled =
                    function (gpRequestsContexts) {
                        return self.GrpGpRequestsContexts_srchr__fsch_label_disabled(gpRequestsContexts);
                    };
                $scope.GrpGpRequestsContexts_srchr__fsch_comment_disabled =
                    function (gpRequestsContexts) {
                        return self.GrpGpRequestsContexts_srchr__fsch_comment_disabled(gpRequestsContexts);
                    };
                $scope.GrpGpRequestsContexts_srchr__fsch_geomHexewkb_disabled =
                    function (gpRequestsContexts) {
                        return self.GrpGpRequestsContexts_srchr__fsch_geomHexewkb_disabled(gpRequestsContexts);
                    };
                $scope.GrpGpRequestsContexts_itm__type_disabled =
                    function (gpRequestsContexts) {
                        return self.GrpGpRequestsContexts_itm__type_disabled(gpRequestsContexts);
                    };
                $scope.GrpGpRequestsContexts_itm__label_disabled =
                    function (gpRequestsContexts) {
                        return self.GrpGpRequestsContexts_itm__label_disabled(gpRequestsContexts);
                    };
                $scope.GrpGpRequestsContexts_itm__comment_disabled =
                    function (gpRequestsContexts) {
                        return self.GrpGpRequestsContexts_itm__comment_disabled(gpRequestsContexts);
                    };
                $scope.GrpGpRequestsContexts_itm__geomHexewkb_disabled =
                    function (gpRequestsContexts) {
                        return self.GrpGpRequestsContexts_itm__geomHexewkb_disabled(gpRequestsContexts);
                    };
                $scope.GrpGpRequestsContexts_itm__referencepoint_disabled =
                    function (gpRequestsContexts) {
                        return self.GrpGpRequestsContexts_itm__referencepoint_disabled(gpRequestsContexts);
                    };
                $scope.GrpGpRequestsContexts_itm__hash_disabled =
                    function (gpRequestsContexts) {
                        return self.GrpGpRequestsContexts_itm__hash_disabled(gpRequestsContexts);
                    };
                $scope.GrpGpRequestsContexts_itm__gpRequestsProducersId_disabled =
                    function (gpRequestsContexts) {
                        return self.GrpGpRequestsContexts_itm__gpRequestsProducersId_disabled(gpRequestsContexts);
                    };
                $scope.showLov_GrpGpRequestsContexts_itm__gpRequestsProducersId =
                    function (gpRequestsContexts) {
                        $timeout(function () {
                            self.showLov_GrpGpRequestsContexts_itm__gpRequestsProducersId(gpRequestsContexts);
                        }, 0);
                    };
                $scope.child_group_GrpGpUpload_isInvisible =
                    function () {
                        return self.child_group_GrpGpUpload_isInvisible();
                    };
                $scope.pageModel.modelGrpGpRequestsContexts = $scope.modelGrpGpRequestsContexts;
                $scope.clearBtnAction = function () {
                    self.modelGrpGpRequestsContexts.fsch_type = undefined;
                    self.modelGrpGpRequestsContexts.fsch_label = undefined;
                    self.modelGrpGpRequestsContexts.fsch_comment = undefined;
                    self.modelGrpGpRequestsContexts.fsch_geomHexewkb = undefined;
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpParcelsIssues.modelGrpGpRequestsContexts = $scope.modelGrpGpRequestsContexts;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpParcelsIssues.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.GpRequestsContexts[] , oldVisible:Entities.GpRequestsContexts[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                //bind entity child collection with child controller merged items
                Entities.GpRequestsContexts.gpUploadCollection = function (gpRequestsContexts, func) {
                    _this.model.modelGrpGpUpload.controller.getMergedItems(function (childEntities) {
                        func(childEntities);
                    }, true, gpRequestsContexts);
                };
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                    //unbind entity child collection with child controller merged items
                    Entities.GpRequestsContexts.gpUploadCollection = null; //(gpRequestsContexts, func) => { }
                });
            }
            ControllerGrpGpRequestsContextsBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpGpRequestsContexts";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "HtmlDivId", {
                get: function () {
                    return "ParcelsIssues_ControllerGrpGpRequestsContexts";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.TextItem(function (ent) { return 'type'; }, true, function (ent) { return self.model.fsch_type; }, function (ent) { return self.model._fsch_type; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'label'; }, true, function (ent) { return self.model.fsch_label; }, function (ent) { return self.model._fsch_label; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'comment'; }, true, function (ent) { return self.model.fsch_comment; }, function (ent) { return self.model._fsch_comment; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'geomHexewkb'; }, true, function (ent) { return self.model.fsch_geomHexewkb; }, function (ent) { return self.model._fsch_geomHexewkb; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'type'; }, false, function (ent) { return ent.type; }, function (ent) { return ent._type; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'label'; }, false, function (ent) { return ent.label; }, function (ent) { return ent._label; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'comment'; }, false, function (ent) { return ent.comment; }, function (ent) { return ent._comment; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'geomHexewkb'; }, false, function (ent) { return ent.geomHexewkb; }, function (ent) { return ent._geomHexewkb; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'referencepoint'; }, false, function (ent) { return ent.referencepoint; }, function (ent) { return ent._referencepoint; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'hash'; }, false, function (ent) { return ent.hash; }, function (ent) { return ent._hash; }, function (ent) { return true; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.LovItem(function (ent) { return 'gpRequestsProducersId'; }, false, function (ent) { return ent.gpRequestsProducersId; }, function (ent) { return ent._gpRequestsProducersId; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); })
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpRequestsContextsBase.prototype.gridTitle = function () {
                return "GpRequestsContexts";
            };
            ControllerGrpGpRequestsContextsBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpGpRequestsContextsBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'type', displayName: 'getALString("type", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpRequestsContexts_itm__type' data-ng-model='row.entity.type' data-np-ui-model='row.entity._type' data-ng-change='markEntityAsUpdated(row.entity,&quot;type&quot;)' data-ng-readonly='GrpGpRequestsContexts_itm__type_disabled(row.entity)' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'label', displayName: 'getALString("label", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpRequestsContexts_itm__label' data-ng-model='row.entity.label' data-np-ui-model='row.entity._label' data-ng-change='markEntityAsUpdated(row.entity,&quot;label&quot;)' data-ng-readonly='GrpGpRequestsContexts_itm__label_disabled(row.entity)' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'comment', displayName: 'getALString("comment", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpRequestsContexts_itm__comment' data-ng-model='row.entity.comment' data-np-ui-model='row.entity._comment' data-ng-change='markEntityAsUpdated(row.entity,&quot;comment&quot;)' data-ng-readonly='GrpGpRequestsContexts_itm__comment_disabled(row.entity)' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'geomHexewkb', displayName: 'getALString("geomHexewkb", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpRequestsContexts_itm__geomHexewkb' data-ng-model='row.entity.geomHexewkb' data-np-ui-model='row.entity._geomHexewkb' data-ng-change='markEntityAsUpdated(row.entity,&quot;geomHexewkb&quot;)' data-ng-readonly='GrpGpRequestsContexts_itm__geomHexewkb_disabled(row.entity)' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'referencepoint', displayName: 'getALString("referencepoint", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpRequestsContexts_itm__referencepoint' data-ng-model='row.entity.referencepoint' data-np-ui-model='row.entity._referencepoint' data-ng-change='markEntityAsUpdated(row.entity,&quot;referencepoint&quot;)' data-ng-readonly='GrpGpRequestsContexts_itm__referencepoint_disabled(row.entity)' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'hash', displayName: 'getALString("hash", true)', requiredAsterisk: self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"), resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpRequestsContexts_itm__hash' data-ng-model='row.entity.hash' data-np-ui-model='row.entity._hash' data-ng-change='markEntityAsUpdated(row.entity,&quot;hash&quot;)' data-np-required='true' data-ng-readonly='GrpGpRequestsContexts_itm__hash_disabled(row.entity)' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'gpRequestsProducersId.notes', displayName: 'getALString("gpRequestsProducersId", true)', requiredAsterisk: self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"), resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <div data-on-key-down='!GrpGpRequestsContexts_itm__gpRequestsProducersId_disabled(row.entity) && showLov_GrpGpRequestsContexts_itm__gpRequestsProducersId(row.entity)' data-keys='[123]' > \
                    <input data-np-source='row.entity.gpRequestsProducersId.notes' data-np-ui-model='row.entity._gpRequestsProducersId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='GrpGpRequestsContexts_itm__gpRequestsProducersId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_GrpGpRequestsContexts_itm__gpRequestsProducersId(row.entity)'   data-ng-disabled=\"GrpGpRequestsContexts_itm__gpRequestsProducersId_disabled(row.entity)\"   />  \
                    </div> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "modelGrpGpRequestsContexts", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpRequestsContextsBase.prototype.getEntityName = function () {
                return "GpRequestsContexts";
            };
            ControllerGrpGpRequestsContextsBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerGrpGpRequestsContextsBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.GpRequestsContexts.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.parcelsIssuesId = _this.Parent;
                    }
                    else {
                        x.parcelsIssuesId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpGpRequestsContexts.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpRequestsContextsBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpParcelsIssues.controller.isEntityLocked(cur.parcelsIssuesId, lockKind);
            };
            ControllerGrpGpRequestsContextsBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_type)) {
                    paramData['fsch_type'] = self.modelGrpGpRequestsContexts.fsch_type;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_label)) {
                    paramData['fsch_label'] = self.modelGrpGpRequestsContexts.fsch_label;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_comment)) {
                    paramData['fsch_comment'] = self.modelGrpGpRequestsContexts.fsch_comment;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_geomHexewkb)) {
                    paramData['fsch_geomHexewkb'] = self.modelGrpGpRequestsContexts.fsch_geomHexewkb;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpGpRequestsContextsBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "GpRequestsContexts/findAllByCriteriaRange_ParcelsIssuesGrpGpRequestsContexts_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_type)) {
                    paramData['fsch_type'] = self.modelGrpGpRequestsContexts.fsch_type;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_label)) {
                    paramData['fsch_label'] = self.modelGrpGpRequestsContexts.fsch_label;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_comment)) {
                    paramData['fsch_comment'] = self.modelGrpGpRequestsContexts.fsch_comment;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_geomHexewkb)) {
                    paramData['fsch_geomHexewkb'] = self.modelGrpGpRequestsContexts.fsch_geomHexewkb;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpGpRequestsContextsBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "GpRequestsContexts/findAllByCriteriaRange_ParcelsIssuesGrpGpRequestsContexts";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_type)) {
                    paramData['fsch_type'] = self.modelGrpGpRequestsContexts.fsch_type;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_label)) {
                    paramData['fsch_label'] = self.modelGrpGpRequestsContexts.fsch_label;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_comment)) {
                    paramData['fsch_comment'] = self.modelGrpGpRequestsContexts.fsch_comment;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_geomHexewkb)) {
                    paramData['fsch_geomHexewkb'] = self.modelGrpGpRequestsContexts.fsch_geomHexewkb;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpGpRequestsContextsBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "GpRequestsContexts/findAllByCriteriaRange_ParcelsIssuesGrpGpRequestsContexts_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_type)) {
                    paramData['fsch_type'] = self.modelGrpGpRequestsContexts.fsch_type;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_label)) {
                    paramData['fsch_label'] = self.modelGrpGpRequestsContexts.fsch_label;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_comment)) {
                    paramData['fsch_comment'] = self.modelGrpGpRequestsContexts.fsch_comment;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpRequestsContexts) && !isVoid(self.modelGrpGpRequestsContexts.fsch_geomHexewkb)) {
                    paramData['fsch_geomHexewkb'] = self.modelGrpGpRequestsContexts.fsch_geomHexewkb;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpGpRequestsContextsBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.parcelsIssuesId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "GpRequestsContexts/findAllByCriteriaRange_ParcelsIssuesGrpGpRequestsContexts";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['parcelsIssuesId_parcelsIssuesId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpGpRequestsContextsBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.parcelsIssuesId === undefined)
                    return false;
                return x.parcelsIssuesId.getKey() === this.Parent.getKey();
            };
            ControllerGrpGpRequestsContextsBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.parcelsIssuesId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelsIssues.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "parcelsIssuesId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelsIssues.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpRequestsContextsBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.GpRequestsContexts(
                /*gpRequestsContextsId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*type:string*/ null, 
                /*label:string*/ null, 
                /*comment:string*/ null, 
                /*geomHexewkb:string*/ null, 
                /*referencepoint:string*/ null, 
                /*rowVersion:number*/ null, 
                /*hash:string*/ null, 
                /*gpRequestsProducersId:Entities.GpRequestsProducers*/ null, 
                /*parcelsIssuesId:Entities.ParcelsIssues*/ null);
                return ret;
            };
            ControllerGrpGpRequestsContextsBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.parcelsIssuesId = self.parcelsIssuesId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpGpRequestsContextsBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpGpRequestsContextsBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                var _this = this;
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.GpRequestsContexts.Create();
                ret.updateInstance(src);
                ret.gpRequestsContextsId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                    _this.modelGrpGpRequestsContexts.modelGrpGpUpload.controller.cloneAllEntitiesUnderParent(src, ret);
                }, 1);
                return ret;
            };
            ControllerGrpGpRequestsContextsBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (gpRequestsContextsList) {
                    gpRequestsContextsList.forEach(function (gpRequestsContexts) {
                        var gpRequestsContextsCloned = _this.cloneEntity_internal(gpRequestsContexts, true);
                        gpRequestsContextsCloned.parcelsIssuesId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpGpRequestsContextsBase.prototype.deleteNewEntitiesUnderParent = function (parcelsIssues) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var gpRequestsContexts = change.a;
                    if (parcelsIssues.getKey() === gpRequestsContexts.parcelsIssuesId.getKey()) {
                        toBeDeleted.push(gpRequestsContexts);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                    self.modelGrpGpRequestsContexts.modelGrpGpUpload.controller.deleteNewEntitiesUnderParent(x);
                });
            };
            ControllerGrpGpRequestsContextsBase.prototype.deleteAllEntitiesUnderParent = function (parcelsIssues, afterDeleteAction) {
                var self = this;
                function deleteGpRequestsContextsList(gpRequestsContextsList) {
                    if (gpRequestsContextsList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = gpRequestsContextsList[0];
                        var tail = gpRequestsContextsList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteGpRequestsContextsList(tail);
                        });
                    }
                }
                this.getMergedItems(function (gpRequestsContextsList) {
                    deleteGpRequestsContextsList(gpRequestsContextsList);
                }, false, parcelsIssues);
            };
            Object.defineProperty(ControllerGrpGpRequestsContextsBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [
                            self.modelGrpGpRequestsContexts.modelGrpGpUpload.controller
                        ];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpRequestsContextsBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                var _this = this;
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    self.modelGrpGpRequestsContexts.modelGrpGpUpload.controller.deleteAllEntitiesUnderParent(ent, function () {
                        _super.prototype.deleteEntity.call(_this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                    });
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        self.modelGrpGpRequestsContexts.modelGrpGpUpload.controller.deleteNewEntitiesUnderParent(ent);
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpGpRequestsContextsBase.prototype._disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpRequestsContextsBase.prototype._invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpRequestsContextsBase.prototype.GrpGpRequestsContexts_srchr__fsch_type_disabled = function (gpRequestsContexts) {
                var self = this;
                return false;
            };
            ControllerGrpGpRequestsContextsBase.prototype.GrpGpRequestsContexts_srchr__fsch_label_disabled = function (gpRequestsContexts) {
                var self = this;
                return false;
            };
            ControllerGrpGpRequestsContextsBase.prototype.GrpGpRequestsContexts_srchr__fsch_comment_disabled = function (gpRequestsContexts) {
                var self = this;
                return false;
            };
            ControllerGrpGpRequestsContextsBase.prototype.GrpGpRequestsContexts_srchr__fsch_geomHexewkb_disabled = function (gpRequestsContexts) {
                var self = this;
                return false;
            };
            ControllerGrpGpRequestsContextsBase.prototype.GrpGpRequestsContexts_itm__type_disabled = function (gpRequestsContexts) {
                var self = this;
                if (gpRequestsContexts === undefined || gpRequestsContexts === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(gpRequestsContexts, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpGpRequestsContextsBase.prototype.GrpGpRequestsContexts_itm__label_disabled = function (gpRequestsContexts) {
                var self = this;
                if (gpRequestsContexts === undefined || gpRequestsContexts === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(gpRequestsContexts, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpGpRequestsContextsBase.prototype.GrpGpRequestsContexts_itm__comment_disabled = function (gpRequestsContexts) {
                var self = this;
                if (gpRequestsContexts === undefined || gpRequestsContexts === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(gpRequestsContexts, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpGpRequestsContextsBase.prototype.GrpGpRequestsContexts_itm__geomHexewkb_disabled = function (gpRequestsContexts) {
                var self = this;
                if (gpRequestsContexts === undefined || gpRequestsContexts === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(gpRequestsContexts, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpGpRequestsContextsBase.prototype.GrpGpRequestsContexts_itm__referencepoint_disabled = function (gpRequestsContexts) {
                var self = this;
                if (gpRequestsContexts === undefined || gpRequestsContexts === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(gpRequestsContexts, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpGpRequestsContextsBase.prototype.GrpGpRequestsContexts_itm__hash_disabled = function (gpRequestsContexts) {
                var self = this;
                if (gpRequestsContexts === undefined || gpRequestsContexts === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(gpRequestsContexts, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpGpRequestsContextsBase.prototype.GrpGpRequestsContexts_itm__gpRequestsProducersId_disabled = function (gpRequestsContexts) {
                var self = this;
                if (gpRequestsContexts === undefined || gpRequestsContexts === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(gpRequestsContexts, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpGpRequestsContextsBase.prototype.showLov_GrpGpRequestsContexts_itm__gpRequestsProducersId = function (gpRequestsContexts) {
                var self = this;
                if (gpRequestsContexts === undefined)
                    return;
                var uimodel = gpRequestsContexts._gpRequestsProducersId;
                var dialogOptions = new NpTypes.LovDialogOptions();
                dialogOptions.title = 'GpRequestsProducers';
                dialogOptions.previousModel = self.cachedLovModel_GrpGpRequestsContexts_itm__gpRequestsProducersId;
                dialogOptions.className = "ControllerLovGpRequestsProducers";
                dialogOptions.onSelect = function (selectedEntity) {
                    var gpRequestsProducers1 = selectedEntity;
                    uimodel.clearAllErrors();
                    if (true && isVoid(gpRequestsProducers1)) {
                        uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                        self.$timeout(function () {
                            uimodel.clearAllErrors();
                        }, 3000);
                        return;
                    }
                    if (!isVoid(gpRequestsProducers1) && gpRequestsProducers1.isEqual(gpRequestsContexts.gpRequestsProducersId))
                        return;
                    gpRequestsContexts.gpRequestsProducersId = gpRequestsProducers1;
                    self.markEntityAsUpdated(gpRequestsContexts, 'GrpGpRequestsContexts_itm__gpRequestsProducersId');
                };
                dialogOptions.openNewEntityDialog = function () {
                };
                dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    self.cachedLovModel_GrpGpRequestsContexts_itm__gpRequestsProducersId = lovModel;
                    var wsPath = "GpRequestsProducers/findAllByCriteriaRange_forLov";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_GpRequestsProducersLov_notes)) {
                        paramData['fsch_GpRequestsProducersLov_notes'] = lovModel.fsch_GpRequestsProducersLov_notes;
                    }
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.showTotalItems = true;
                dialogOptions.updateTotalOnDemand = false;
                dialogOptions.makeWebRequest_count = function (lovModel, excludedIds) {
                    if (excludedIds === void 0) { excludedIds = []; }
                    var wsPath = "GpRequestsProducers/findAllByCriteriaRange_forLov_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_GpRequestsProducersLov_notes)) {
                        paramData['fsch_GpRequestsProducersLov_notes'] = lovModel.fsch_GpRequestsProducersLov_notes;
                    }
                    var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                    var wsStartTs = (new Date).getTime();
                    return promise.success(function () {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
                };
                dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, lovModel, excludedIds) {
                    var paramData = {};
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                        paramData['sortField'] = model.sortField;
                        paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_GpRequestsProducersLov_notes)) {
                        paramData['fsch_GpRequestsProducersLov_notes'] = lovModel.fsch_GpRequestsProducersLov_notes;
                    }
                    var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                    return res;
                };
                dialogOptions.shownCols['notes'] = true;
                self.Plato.showDialog(self.$scope, dialogOptions.title, "/" + self.$scope.globals.appName + '/partials/lovs/GpRequestsProducers.html?rev=' + self.$scope.globals.version, dialogOptions);
            };
            ControllerGrpGpRequestsContextsBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController.GrpParcelsIssues_0_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpRequestsContextsBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController.GrpParcelsIssues_0_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpRequestsContextsBase.prototype._newIsDisabled = function () {
                var self = this;
                if (self.Parent === null || self.Parent === undefined)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var parEntityIsLocked = self.ParentController.isEntityLocked(self.Parent, Controllers.LockKind.UpdateLock);
                if (parEntityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpGpRequestsContextsBase.prototype._deleteIsDisabled = function (gpRequestsContexts) {
                var self = this;
                if (gpRequestsContexts === null || gpRequestsContexts === undefined || gpRequestsContexts.getEntityName() !== "GpRequestsContexts")
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_D"))
                    return true; // no delete privilege;
                var entityIsLocked = self.isEntityLocked(gpRequestsContexts, Controllers.LockKind.DeleteLock);
                if (entityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpGpRequestsContextsBase.prototype.child_group_GrpGpUpload_isInvisible = function () {
                var self = this;
                if ((self.model.modelGrpGpUpload === undefined) || (self.model.modelGrpGpUpload.controller === undefined))
                    return false;
                return self.model.modelGrpGpUpload.controller._isInvisible();
            };
            return ControllerGrpGpRequestsContextsBase;
        })(Controllers.AbstractGroupTableController);
        ParcelsIssues.ControllerGrpGpRequestsContextsBase = ControllerGrpGpRequestsContextsBase;
        // GROUP GrpGpUpload
        var ModelGrpGpUploadBase = (function (_super) {
            __extends(ModelGrpGpUploadBase, _super);
            function ModelGrpGpUploadBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
                this._fsch_data = new NpTypes.UIStringModel(undefined);
                this._fsch_environment = new NpTypes.UIStringModel(undefined);
                this._fsch_dteUpload = new NpTypes.UIDateModel(undefined);
                this._fsch_hash = new NpTypes.UIStringModel(undefined);
            }
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "gpUploadsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].gpUploadsId;
                },
                set: function (gpUploadsId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].gpUploadsId = gpUploadsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_gpUploadsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._gpUploadsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "data", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].data;
                },
                set: function (data_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].data = data_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_data", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._data;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "environment", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].environment;
                },
                set: function (environment_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].environment = environment_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_environment", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._environment;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "dteUpload", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].dteUpload;
                },
                set: function (dteUpload_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].dteUpload = dteUpload_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_dteUpload", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._dteUpload;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "hash", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].hash;
                },
                set: function (hash_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].hash = hash_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_hash", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._hash;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "image", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].image;
                },
                set: function (image_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].image = image_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_image", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._image;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "gpRequestsContextsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].gpRequestsContextsId;
                },
                set: function (gpRequestsContextsId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].gpRequestsContextsId = gpRequestsContextsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_gpRequestsContextsId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._gpRequestsContextsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "fsch_data", {
                get: function () {
                    return this._fsch_data.value;
                },
                set: function (vl) {
                    this._fsch_data.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "fsch_environment", {
                get: function () {
                    return this._fsch_environment.value;
                },
                set: function (vl) {
                    this._fsch_environment.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "fsch_dteUpload", {
                get: function () {
                    return this._fsch_dteUpload.value;
                },
                set: function (vl) {
                    this._fsch_dteUpload.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "fsch_hash", {
                get: function () {
                    return this._fsch_hash.value;
                },
                set: function (vl) {
                    this._fsch_hash.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpGpUploadBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpGpUploadBase;
        })(Controllers.AbstractGroupTableModel);
        ParcelsIssues.ModelGrpGpUploadBase = ModelGrpGpUploadBase;
        var ControllerGrpGpUploadBase = (function (_super) {
            __extends(ControllerGrpGpUploadBase, _super);
            function ControllerGrpGpUploadBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpGpUpload = self.model;
                $scope._disabled =
                    function () {
                        return self._disabled();
                    };
                $scope._invisible =
                    function () {
                        return self._invisible();
                    };
                $scope.GrpGpUpload_srchr__fsch_data_disabled =
                    function (gpUpload) {
                        return self.GrpGpUpload_srchr__fsch_data_disabled(gpUpload);
                    };
                $scope.GrpGpUpload_srchr__fsch_environment_disabled =
                    function (gpUpload) {
                        return self.GrpGpUpload_srchr__fsch_environment_disabled(gpUpload);
                    };
                $scope.GrpGpUpload_srchr__fsch_dteUpload_disabled =
                    function (gpUpload) {
                        return self.GrpGpUpload_srchr__fsch_dteUpload_disabled(gpUpload);
                    };
                $scope.GrpGpUpload_srchr__fsch_hash_disabled =
                    function (gpUpload) {
                        return self.GrpGpUpload_srchr__fsch_hash_disabled(gpUpload);
                    };
                $scope.GrpGpUpload_itm__data_disabled =
                    function (gpUpload) {
                        return self.GrpGpUpload_itm__data_disabled(gpUpload);
                    };
                $scope.GrpGpUpload_itm__environment_disabled =
                    function (gpUpload) {
                        return self.GrpGpUpload_itm__environment_disabled(gpUpload);
                    };
                $scope.GrpGpUpload_itm__dteUpload_disabled =
                    function (gpUpload) {
                        return self.GrpGpUpload_itm__dteUpload_disabled(gpUpload);
                    };
                $scope.GrpGpUpload_itm__hash_disabled =
                    function (gpUpload) {
                        return self.GrpGpUpload_itm__hash_disabled(gpUpload);
                    };
                $scope.GrpGpUpload_itm__image_disabled =
                    function (gpUpload) {
                        return self.GrpGpUpload_itm__image_disabled(gpUpload);
                    };
                $scope.createBlobModel_GrpGpUpload_itm__image =
                    function () {
                        var tmp = self.createBlobModel_GrpGpUpload_itm__image();
                        return tmp;
                    };
                $scope.pageModel.modelGrpGpUpload = $scope.modelGrpGpUpload;
                $scope.clearBtnAction = function () {
                    self.modelGrpGpUpload.fsch_data = undefined;
                    self.modelGrpGpUpload.fsch_environment = undefined;
                    self.modelGrpGpUpload.fsch_dteUpload = undefined;
                    self.modelGrpGpUpload.fsch_hash = undefined;
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpGpRequestsContexts.modelGrpGpUpload = $scope.modelGrpGpUpload;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpGpRequestsContexts.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.GpUpload[] , oldVisible:Entities.GpUpload[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            ControllerGrpGpUploadBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpGpUpload";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "HtmlDivId", {
                get: function () {
                    return "ParcelsIssues_ControllerGrpGpUpload";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.TextItem(function (ent) { return 'data'; }, true, function (ent) { return self.model.fsch_data; }, function (ent) { return self.model._fsch_data; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'environment'; }, true, function (ent) { return self.model.fsch_environment; }, function (ent) { return self.model._fsch_environment; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.DateItem(function (ent) { return 'dteUpload'; }, true, function (ent) { return self.model.fsch_dteUpload; }, function (ent) { return self.model._fsch_dteUpload; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined),
                            new Controllers.TextItem(function (ent) { return 'hash'; }, true, function (ent) { return self.model.fsch_hash; }, function (ent) { return self.model._fsch_hash; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'data'; }, false, function (ent) { return ent.data; }, function (ent) { return ent._data; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'environment'; }, false, function (ent) { return ent.environment; }, function (ent) { return ent._environment; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.DateItem(function (ent) { return 'dteUpload'; }, false, function (ent) { return ent.dteUpload; }, function (ent) { return ent._dteUpload; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined),
                            new Controllers.TextItem(function (ent) { return 'hash'; }, false, function (ent) { return ent.hash; }, function (ent) { return ent._hash; }, function (ent) { return true; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.BlobItem(function (ent) { return 'image'; }, false, function (ent) { return ent.image; }, function (ent) { return ent._image; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); })
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpUploadBase.prototype.gridTitle = function () {
                return "GpUpload";
            };
            ControllerGrpGpUploadBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpGpUploadBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'data', displayName: 'getALString("data", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpUpload_itm__data' data-ng-model='row.entity.data' data-np-ui-model='row.entity._data' data-ng-change='markEntityAsUpdated(row.entity,&quot;data&quot;)' data-ng-readonly='GrpGpUpload_itm__data_disabled(row.entity)' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'environment', displayName: 'getALString("environment", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpUpload_itm__environment' data-ng-model='row.entity.environment' data-np-ui-model='row.entity._environment' data-ng-change='markEntityAsUpdated(row.entity,&quot;environment&quot;)' data-ng-readonly='GrpGpUpload_itm__environment_disabled(row.entity)' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'dteUpload', displayName: 'getALString("dteUpload", true)', requiredAsterisk: self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"), resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpUpload_itm__dteUpload' data-ng-model='row.entity.dteUpload' data-np-ui-model='row.entity._dteUpload' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteUpload&quot;)' data-np-required='true' data-ng-readonly='GrpGpUpload_itm__dteUpload_disabled(row.entity)' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'hash', displayName: 'getALString("hash", true)', requiredAsterisk: self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"), resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpGpUpload_itm__hash' data-ng-model='row.entity.hash' data-np-ui-model='row.entity._hash' data-ng-change='markEntityAsUpdated(row.entity,&quot;hash&quot;)' data-np-required='true' data-ng-readonly='GrpGpUpload_itm__hash_disabled(row.entity)' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'image', displayName: 'getALString("image", true)', requiredAsterisk: false, resizable: true, sortable: false, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <span data-ng-model='row.entity._image' data-np-factory='createBlobModel_GrpGpUpload_itm__image()' data-np-blob='dummy' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "modelGrpGpUpload", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpUploadBase.prototype.getEntityName = function () {
                return "GpUpload";
            };
            ControllerGrpGpUploadBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerGrpGpUploadBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.GpUpload.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.gpRequestsContextsId = _this.Parent;
                    }
                    else {
                        x.gpRequestsContextsId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpGpUpload.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpUploadBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpGpRequestsContexts.controller.isEntityLocked(cur.gpRequestsContextsId, lockKind);
            };
            ControllerGrpGpUploadBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.gpRequestsContextsId)) {
                    paramData['gpRequestsContextsId_gpRequestsContextsId'] = self.Parent.gpRequestsContextsId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_data)) {
                    paramData['fsch_data'] = self.modelGrpGpUpload.fsch_data;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_environment)) {
                    paramData['fsch_environment'] = self.modelGrpGpUpload.fsch_environment;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_dteUpload)) {
                    paramData['fsch_dteUpload'] = self.modelGrpGpUpload.fsch_dteUpload;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_hash)) {
                    paramData['fsch_hash'] = self.modelGrpGpUpload.fsch_hash;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpGpUploadBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "GpUpload/findAllByCriteriaRange_ParcelsIssuesGrpGpUpload_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.gpRequestsContextsId)) {
                    paramData['gpRequestsContextsId_gpRequestsContextsId'] = self.Parent.gpRequestsContextsId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_data)) {
                    paramData['fsch_data'] = self.modelGrpGpUpload.fsch_data;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_environment)) {
                    paramData['fsch_environment'] = self.modelGrpGpUpload.fsch_environment;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_dteUpload)) {
                    paramData['fsch_dteUpload'] = self.modelGrpGpUpload.fsch_dteUpload;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_hash)) {
                    paramData['fsch_hash'] = self.modelGrpGpUpload.fsch_hash;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpGpUploadBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "GpUpload/findAllByCriteriaRange_ParcelsIssuesGrpGpUpload";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.gpRequestsContextsId)) {
                    paramData['gpRequestsContextsId_gpRequestsContextsId'] = self.Parent.gpRequestsContextsId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_data)) {
                    paramData['fsch_data'] = self.modelGrpGpUpload.fsch_data;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_environment)) {
                    paramData['fsch_environment'] = self.modelGrpGpUpload.fsch_environment;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_dteUpload)) {
                    paramData['fsch_dteUpload'] = self.modelGrpGpUpload.fsch_dteUpload;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_hash)) {
                    paramData['fsch_hash'] = self.modelGrpGpUpload.fsch_hash;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpGpUploadBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "GpUpload/findAllByCriteriaRange_ParcelsIssuesGrpGpUpload_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.gpRequestsContextsId)) {
                    paramData['gpRequestsContextsId_gpRequestsContextsId'] = self.Parent.gpRequestsContextsId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_data)) {
                    paramData['fsch_data'] = self.modelGrpGpUpload.fsch_data;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_environment)) {
                    paramData['fsch_environment'] = self.modelGrpGpUpload.fsch_environment;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_dteUpload)) {
                    paramData['fsch_dteUpload'] = self.modelGrpGpUpload.fsch_dteUpload;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpGpUpload) && !isVoid(self.modelGrpGpUpload.fsch_hash)) {
                    paramData['fsch_hash'] = self.modelGrpGpUpload.fsch_hash;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpGpUploadBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.gpRequestsContextsId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "GpUpload/findAllByCriteriaRange_ParcelsIssuesGrpGpUpload";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['gpRequestsContextsId_gpRequestsContextsId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpGpUploadBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.gpRequestsContextsId === undefined)
                    return false;
                return x.gpRequestsContextsId.getKey() === this.Parent.getKey();
            };
            ControllerGrpGpUploadBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.gpRequestsContextsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpGpRequestsContexts.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "gpRequestsContextsId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpGpRequestsContexts.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpUploadBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.GpUpload(
                /*gpUploadsId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*data:string*/ null, 
                /*environment:string*/ null, 
                /*dteUpload:Date*/ null, 
                /*hash:string*/ null, 
                /*image:string*/ null, 
                /*gpRequestsContextsId:Entities.GpRequestsContexts*/ null);
                return ret;
            };
            ControllerGrpGpUploadBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.gpRequestsContextsId = self.gpRequestsContextsId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpGpUploadBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpGpUploadBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.GpUpload.Create();
                ret.updateInstance(src);
                ret.gpUploadsId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            ControllerGrpGpUploadBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (gpUploadList) {
                    gpUploadList.forEach(function (gpUpload) {
                        var gpUploadCloned = _this.cloneEntity_internal(gpUpload, true);
                        gpUploadCloned.gpRequestsContextsId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpGpUploadBase.prototype.deleteNewEntitiesUnderParent = function (gpRequestsContexts) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var gpUpload = change.a;
                    if (gpRequestsContexts.getKey() === gpUpload.gpRequestsContextsId.getKey()) {
                        toBeDeleted.push(gpUpload);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                });
            };
            ControllerGrpGpUploadBase.prototype.deleteAllEntitiesUnderParent = function (gpRequestsContexts, afterDeleteAction) {
                var self = this;
                function deleteGpUploadList(gpUploadList) {
                    if (gpUploadList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = gpUploadList[0];
                        var tail = gpUploadList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteGpUploadList(tail);
                        });
                    }
                }
                this.getMergedItems(function (gpUploadList) {
                    deleteGpUploadList(gpUploadList);
                }, false, gpRequestsContexts);
            };
            Object.defineProperty(ControllerGrpGpUploadBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpGpUploadBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpGpUploadBase.prototype._disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpUploadBase.prototype._invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpUploadBase.prototype.GrpGpUpload_srchr__fsch_data_disabled = function (gpUpload) {
                var self = this;
                return false;
            };
            ControllerGrpGpUploadBase.prototype.GrpGpUpload_srchr__fsch_environment_disabled = function (gpUpload) {
                var self = this;
                return false;
            };
            ControllerGrpGpUploadBase.prototype.GrpGpUpload_srchr__fsch_dteUpload_disabled = function (gpUpload) {
                var self = this;
                return false;
            };
            ControllerGrpGpUploadBase.prototype.GrpGpUpload_srchr__fsch_hash_disabled = function (gpUpload) {
                var self = this;
                return false;
            };
            ControllerGrpGpUploadBase.prototype.GrpGpUpload_itm__data_disabled = function (gpUpload) {
                var self = this;
                if (gpUpload === undefined || gpUpload === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(gpUpload, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpGpUploadBase.prototype.GrpGpUpload_itm__environment_disabled = function (gpUpload) {
                var self = this;
                if (gpUpload === undefined || gpUpload === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(gpUpload, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpGpUploadBase.prototype.GrpGpUpload_itm__dteUpload_disabled = function (gpUpload) {
                var self = this;
                if (gpUpload === undefined || gpUpload === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(gpUpload, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpGpUploadBase.prototype.GrpGpUpload_itm__hash_disabled = function (gpUpload) {
                var self = this;
                if (gpUpload === undefined || gpUpload === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(gpUpload, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpGpUploadBase.prototype.GrpGpUpload_itm__image_disabled = function (gpUpload) {
                var self = this;
                if (gpUpload === undefined || gpUpload === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(gpUpload, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpGpUploadBase.prototype.createBlobModel_GrpGpUpload_itm__image = function () {
                var self = this;
                var ret = new NpTypes.NpBlob(function (e) { self.markEntityAsUpdated(e, "image"); }, // on value change
                function (e) { return self.getDownloadUrl_createBlobModel_GrpGpUpload_itm__image(e); }, // download url
                function (e) { return self.GrpGpUpload_itm__image_disabled(e); }, // is disabled
                function (e) { return false; }, // is invisible
                "/Niva/rest/GpUpload/setImage", // post url
                true, // add is enabled
                true, // del is enabled
                "", // valid extensions
                function (e) { return 195; } //size in KB
                 //size in KB
                );
                return ret;
            };
            ControllerGrpGpUploadBase.prototype.getDownloadUrl_createBlobModel_GrpGpUpload_itm__image = function (gpUpload) {
                var self = this;
                if (isVoid(gpUpload))
                    return 'javascript:void(0)';
                if (gpUpload.isNew() && isVoid(gpUpload.image))
                    return 'javascript:void(0)';
                var url = "/Niva/rest/GpUpload/getImage?";
                if (!gpUpload.isNew()) {
                    url += "&id=" + encodeURIComponent(gpUpload.gpUploadsId);
                }
                if (!isVoid(gpUpload.image)) {
                    url += "&temp_id=" + encodeURIComponent(gpUpload.image);
                }
                return url;
            };
            ControllerGrpGpUploadBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpUploadBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpGpUploadBase.prototype._newIsDisabled = function () {
                var self = this;
                if (self.Parent === null || self.Parent === undefined)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var parEntityIsLocked = self.ParentController.isEntityLocked(self.Parent, Controllers.LockKind.UpdateLock);
                if (parEntityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpGpUploadBase.prototype._deleteIsDisabled = function (gpUpload) {
                var self = this;
                if (gpUpload === null || gpUpload === undefined || gpUpload.getEntityName() !== "GpUpload")
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_D"))
                    return true; // no delete privilege;
                var entityIsLocked = self.isEntityLocked(gpUpload, Controllers.LockKind.DeleteLock);
                if (entityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            return ControllerGrpGpUploadBase;
        })(Controllers.AbstractGroupTableController);
        ParcelsIssues.ControllerGrpGpUploadBase = ControllerGrpGpUploadBase;
        // GROUP GrpParcelsIssuesActivities
        var ModelGrpParcelsIssuesActivitiesBase = (function (_super) {
            __extends(ModelGrpParcelsIssuesActivitiesBase, _super);
            function ModelGrpParcelsIssuesActivitiesBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
                this._fsch_type = new NpTypes.UINumberModel(undefined);
                this._fsch_dteTimestamp = new NpTypes.UIDateModel(undefined);
            }
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "parcelsIssuesActivitiesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].parcelsIssuesActivitiesId;
                },
                set: function (parcelsIssuesActivitiesId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].parcelsIssuesActivitiesId = parcelsIssuesActivitiesId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "_parcelsIssuesActivitiesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._parcelsIssuesActivitiesId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "type", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].type;
                },
                set: function (type_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].type = type_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "_type", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._type;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "dteTimestamp", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].dteTimestamp;
                },
                set: function (dteTimestamp_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].dteTimestamp = dteTimestamp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "_dteTimestamp", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._dteTimestamp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "typeExtrainfo", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].typeExtrainfo;
                },
                set: function (typeExtrainfo_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].typeExtrainfo = typeExtrainfo_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "_typeExtrainfo", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._typeExtrainfo;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "parcelsIssuesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].parcelsIssuesId;
                },
                set: function (parcelsIssuesId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].parcelsIssuesId = parcelsIssuesId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "_parcelsIssuesId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._parcelsIssuesId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "fsch_type", {
                get: function () {
                    return this._fsch_type.value;
                },
                set: function (vl) {
                    this._fsch_type.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "fsch_dteTimestamp", {
                get: function () {
                    return this._fsch_dteTimestamp.value;
                },
                set: function (vl) {
                    this._fsch_dteTimestamp.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpParcelsIssuesActivitiesBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpParcelsIssuesActivitiesBase;
        })(Controllers.AbstractGroupTableModel);
        ParcelsIssues.ModelGrpParcelsIssuesActivitiesBase = ModelGrpParcelsIssuesActivitiesBase;
        var ControllerGrpParcelsIssuesActivitiesBase = (function (_super) {
            __extends(ControllerGrpParcelsIssuesActivitiesBase, _super);
            function ControllerGrpParcelsIssuesActivitiesBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpParcelsIssuesActivities = self.model;
                $scope._disabled =
                    function () {
                        return self._disabled();
                    };
                $scope._invisible =
                    function () {
                        return self._invisible();
                    };
                $scope.GrpParcelsIssuesActivities_srchr__fsch_type_disabled =
                    function (parcelsIssuesActivities) {
                        return self.GrpParcelsIssuesActivities_srchr__fsch_type_disabled(parcelsIssuesActivities);
                    };
                $scope.GrpParcelsIssuesActivities_srchr__fsch_dteTimestamp_disabled =
                    function (parcelsIssuesActivities) {
                        return self.GrpParcelsIssuesActivities_srchr__fsch_dteTimestamp_disabled(parcelsIssuesActivities);
                    };
                $scope.GrpParcelsIssuesActivities_itm__type_disabled =
                    function (parcelsIssuesActivities) {
                        return self.GrpParcelsIssuesActivities_itm__type_disabled(parcelsIssuesActivities);
                    };
                $scope.GrpParcelsIssuesActivities_itm__dteTimestamp_disabled =
                    function (parcelsIssuesActivities) {
                        return self.GrpParcelsIssuesActivities_itm__dteTimestamp_disabled(parcelsIssuesActivities);
                    };
                $scope.pageModel.modelGrpParcelsIssuesActivities = $scope.modelGrpParcelsIssuesActivities;
                $scope.clearBtnAction = function () {
                    self.modelGrpParcelsIssuesActivities.fsch_type = undefined;
                    self.modelGrpParcelsIssuesActivities.fsch_dteTimestamp = undefined;
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpParcelsIssues.modelGrpParcelsIssuesActivities = $scope.modelGrpParcelsIssuesActivities;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpParcelsIssues.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50); /*
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.ParcelsIssuesActivities[] , oldVisible:Entities.ParcelsIssuesActivities[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            ControllerGrpParcelsIssuesActivitiesBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpParcelsIssuesActivitiesBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpParcelsIssuesActivities";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesActivitiesBase.prototype, "HtmlDivId", {
                get: function () {
                    return "ParcelsIssues_ControllerGrpParcelsIssuesActivities";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesActivitiesBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.NumberItem(function (ent) { return 'type'; }, true, function (ent) { return self.model.fsch_type; }, function (ent) { return self.model._fsch_type; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, function () { return -32768; }, function () { return 32767; }, 0),
                            new Controllers.DateItem(function (ent) { return 'dteTimestamp'; }, true, function (ent) { return self.model.fsch_dteTimestamp; }, function (ent) { return self.model._fsch_dteTimestamp; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined),
                            new Controllers.NumberItem(function (ent) { return 'type'; }, false, function (ent) { return ent.type; }, function (ent) { return ent._type; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, function () { return -32768; }, function () { return 32767; }, 0),
                            new Controllers.DateItem(function (ent) { return 'dteTimestamp'; }, false, function (ent) { return ent.dteTimestamp; }, function (ent) { return ent._dteTimestamp; }, function (ent) { return true; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined)
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelsIssuesActivitiesBase.prototype.gridTitle = function () {
                return "ParcelsIssuesActivities";
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'type', displayName: 'getALString("type", true)', requiredAsterisk: self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"), resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelsIssuesActivities_itm__type' data-ng-model='row.entity.type' data-np-ui-model='row.entity._type' data-ng-change='markEntityAsUpdated(row.entity,&quot;type&quot;)' data-np-required='true' data-ng-readonly='GrpParcelsIssuesActivities_itm__type_disabled(row.entity)' data-np-number='dummy' data-np-min='-32768' data-np-max='32767' data-np-decimals='0' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'dteTimestamp', displayName: 'getALString("dteTimestamp", true)', requiredAsterisk: self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"), resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpParcelsIssuesActivities_itm__dteTimestamp' data-ng-model='row.entity.dteTimestamp' data-np-ui-model='row.entity._dteTimestamp' data-ng-change='markEntityAsUpdated(row.entity,&quot;dteTimestamp&quot;)' data-np-required='true' data-ng-readonly='GrpParcelsIssuesActivities_itm__dteTimestamp_disabled(row.entity)' data-np-date='dummy' data-np-format='dd-MM-yyyy' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpParcelsIssuesActivitiesBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesActivitiesBase.prototype, "modelGrpParcelsIssuesActivities", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelsIssuesActivitiesBase.prototype.getEntityName = function () {
                return "ParcelsIssuesActivities";
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = {};
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var entlist = Entities.ParcelsIssuesActivities.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.parcelsIssuesId = _this.Parent;
                    }
                    else {
                        x.parcelsIssuesId = parent;
                    }
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpParcelsIssuesActivitiesBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelsIssuesActivities.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelsIssuesActivitiesBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return self.$scope.modelGrpParcelsIssues.controller.isEntityLocked(cur.parcelsIssuesId, lockKind);
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssuesActivities) && !isVoid(self.modelGrpParcelsIssuesActivities.fsch_type)) {
                    paramData['fsch_type'] = self.modelGrpParcelsIssuesActivities.fsch_type;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssuesActivities) && !isVoid(self.modelGrpParcelsIssuesActivities.fsch_dteTimestamp)) {
                    paramData['fsch_dteTimestamp'] = self.modelGrpParcelsIssuesActivities.fsch_dteTimestamp;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "ParcelsIssuesActivities/findAllByCriteriaRange_ParcelsIssuesGrpParcelsIssuesActivities_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssuesActivities) && !isVoid(self.modelGrpParcelsIssuesActivities.fsch_type)) {
                    paramData['fsch_type'] = self.modelGrpParcelsIssuesActivities.fsch_type;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssuesActivities) && !isVoid(self.modelGrpParcelsIssuesActivities.fsch_dteTimestamp)) {
                    paramData['fsch_dteTimestamp'] = self.modelGrpParcelsIssuesActivities.fsch_dteTimestamp;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "ParcelsIssuesActivities/findAllByCriteriaRange_ParcelsIssuesGrpParcelsIssuesActivities";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssuesActivities) && !isVoid(self.modelGrpParcelsIssuesActivities.fsch_type)) {
                    paramData['fsch_type'] = self.modelGrpParcelsIssuesActivities.fsch_type;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssuesActivities) && !isVoid(self.modelGrpParcelsIssuesActivities.fsch_dteTimestamp)) {
                    paramData['fsch_dteTimestamp'] = self.modelGrpParcelsIssuesActivities.fsch_dteTimestamp;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "ParcelsIssuesActivities/findAllByCriteriaRange_ParcelsIssuesGrpParcelsIssuesActivities_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.parcelsIssuesId)) {
                    paramData['parcelsIssuesId_parcelsIssuesId'] = self.Parent.parcelsIssuesId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssuesActivities) && !isVoid(self.modelGrpParcelsIssuesActivities.fsch_type)) {
                    paramData['fsch_type'] = self.modelGrpParcelsIssuesActivities.fsch_type;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpParcelsIssuesActivities) && !isVoid(self.modelGrpParcelsIssuesActivities.fsch_dteTimestamp)) {
                    paramData['fsch_dteTimestamp'] = self.modelGrpParcelsIssuesActivities.fsch_dteTimestamp;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.parcelsIssuesId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.getKey()] !== undefined &&
                            self.getMergedItems_cache[parent.getKey()].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);
                        var wsPath = "ParcelsIssuesActivities/findAllByCriteriaRange_ParcelsIssuesGrpParcelsIssuesActivities";
                        var url = "/Niva/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['parcelsIssuesId_parcelsIssuesId'] = parent.getKey();
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.parcelsIssuesId === undefined)
                    return false;
                return x.parcelsIssuesId.getKey() === this.Parent.getKey();
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpParcelsIssuesActivitiesBase.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.parcelsIssuesId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesActivitiesBase.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelsIssues.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesActivitiesBase.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpParcelsIssuesActivitiesBase.prototype, "parcelsIssuesId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpParcelsIssues.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelsIssuesActivitiesBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.ParcelsIssuesActivities(
                /*parcelsIssuesActivitiesId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*rowVersion:number*/ null, 
                /*type:number*/ null, 
                /*dteTimestamp:Date*/ null, 
                /*typeExtrainfo:string*/ null, 
                /*parcelsIssuesId:Entities.ParcelsIssues*/ null);
                return ret;
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                newEnt.parcelsIssuesId = self.parcelsIssuesId;
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.ParcelsIssuesActivities.Create();
                ret.updateInstance(src);
                ret.parcelsIssuesActivitiesId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype.cloneAllEntitiesUnderParent = function (oldParent, newParent) {
                var _this = this;
                this.model.totalItems = 0;
                this.getMergedItems(function (parcelsIssuesActivitiesList) {
                    parcelsIssuesActivitiesList.forEach(function (parcelsIssuesActivities) {
                        var parcelsIssuesActivitiesCloned = _this.cloneEntity_internal(parcelsIssuesActivities, true);
                        parcelsIssuesActivitiesCloned.parcelsIssuesId = newParent;
                    });
                    _this.updateUI();
                }, false, oldParent);
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype.deleteNewEntitiesUnderParent = function (parcelsIssues) {
                var self = this;
                var toBeDeleted = [];
                for (var i = 0; i < self.model.newEntities.length; i++) {
                    var change = self.model.newEntities[i];
                    var parcelsIssuesActivities = change.a;
                    if (parcelsIssues.getKey() === parcelsIssuesActivities.parcelsIssuesId.getKey()) {
                        toBeDeleted.push(parcelsIssuesActivities);
                    }
                }
                _.each(toBeDeleted, function (x) {
                    self.deleteEntity(x, false, -1);
                    // for each child group
                    // call deleteNewEntitiesUnderParent(ent)
                });
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype.deleteAllEntitiesUnderParent = function (parcelsIssues, afterDeleteAction) {
                var self = this;
                function deleteParcelsIssuesActivitiesList(parcelsIssuesActivitiesList) {
                    if (parcelsIssuesActivitiesList.length === 0) {
                        self.$timeout(function () {
                            afterDeleteAction();
                        }, 1); //make sure that parents are marked for deletion after 1 ms
                    }
                    else {
                        var head = parcelsIssuesActivitiesList[0];
                        var tail = parcelsIssuesActivitiesList.slice(1);
                        self.deleteEntity(head, false, -1, true, function () {
                            deleteParcelsIssuesActivitiesList(tail);
                        });
                    }
                }
                this.getMergedItems(function (parcelsIssuesActivitiesList) {
                    deleteParcelsIssuesActivitiesList(parcelsIssuesActivitiesList);
                }, false, parcelsIssues);
            };
            Object.defineProperty(ControllerGrpParcelsIssuesActivitiesBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpParcelsIssuesActivitiesBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype._disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype._invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype.GrpParcelsIssuesActivities_srchr__fsch_type_disabled = function (parcelsIssuesActivities) {
                var self = this;
                return false;
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype.GrpParcelsIssuesActivities_srchr__fsch_dteTimestamp_disabled = function (parcelsIssuesActivities) {
                var self = this;
                return false;
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype.GrpParcelsIssuesActivities_itm__type_disabled = function (parcelsIssuesActivities) {
                var self = this;
                if (parcelsIssuesActivities === undefined || parcelsIssuesActivities === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(parcelsIssuesActivities, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype.GrpParcelsIssuesActivities_itm__dteTimestamp_disabled = function (parcelsIssuesActivities) {
                var self = this;
                if (parcelsIssuesActivities === undefined || parcelsIssuesActivities === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(parcelsIssuesActivities, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = this.ParentController.GrpParcelsIssues_0_disabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = this.ParentController.GrpParcelsIssues_0_invisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype._newIsDisabled = function () {
                var self = this;
                if (self.Parent === null || self.Parent === undefined)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_W"))
                    return true; // no write privilege
                var parEntityIsLocked = self.ParentController.isEntityLocked(self.Parent, Controllers.LockKind.UpdateLock);
                if (parEntityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpParcelsIssuesActivitiesBase.prototype._deleteIsDisabled = function (parcelsIssuesActivities) {
                var self = this;
                if (parcelsIssuesActivities === null || parcelsIssuesActivities === undefined || parcelsIssuesActivities.getEntityName() !== "ParcelsIssuesActivities")
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_ParcelsIssues_D"))
                    return true; // no delete privilege;
                var entityIsLocked = self.isEntityLocked(parcelsIssuesActivities, Controllers.LockKind.DeleteLock);
                if (entityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            return ControllerGrpParcelsIssuesActivitiesBase;
        })(Controllers.AbstractGroupTableController);
        ParcelsIssues.ControllerGrpParcelsIssuesActivitiesBase = ControllerGrpParcelsIssuesActivitiesBase;
    })(ParcelsIssues = Controllers.ParcelsIssues || (Controllers.ParcelsIssues = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ParcelsIssuesBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
