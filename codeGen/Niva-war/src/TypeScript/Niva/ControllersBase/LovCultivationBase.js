/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/CultivationBase.ts" />
/// <reference path="../Controllers/LovCultivation.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovCultivationBase = (function (_super) {
        __extends(ModelLovCultivationBase, _super);
        function ModelLovCultivationBase($scope) {
            _super.call(this, $scope);
            this.$scope = $scope;
            this._fsch_CultivationLov_name = new NpTypes.UIStringModel(undefined);
            this._fsch_CultivationLov_code = new NpTypes.UINumberModel(undefined);
        }
        Object.defineProperty(ModelLovCultivationBase.prototype, "fsch_CultivationLov_name", {
            get: function () {
                return this._fsch_CultivationLov_name.value;
            },
            set: function (vl) {
                this._fsch_CultivationLov_name.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "fsch_CultivationLov_code", {
            get: function () {
                return this._fsch_CultivationLov_code.value;
            },
            set: function (vl) {
                this._fsch_CultivationLov_code.value = vl;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "appName", {
            get: function () {
                return this.$scope.globals.appName;
            },
            set: function (appName_newVal) {
                this.$scope.globals.appName = appName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "_appName", {
            get: function () {
                return this.$scope.globals._appName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "appTitle", {
            get: function () {
                return this.$scope.globals.appTitle;
            },
            set: function (appTitle_newVal) {
                this.$scope.globals.appTitle = appTitle_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "_appTitle", {
            get: function () {
                return this.$scope.globals._appTitle;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "globalUserEmail", {
            get: function () {
                return this.$scope.globals.globalUserEmail;
            },
            set: function (globalUserEmail_newVal) {
                this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "_globalUserEmail", {
            get: function () {
                return this.$scope.globals._globalUserEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals.globalUserActiveEmail;
            },
            set: function (globalUserActiveEmail_newVal) {
                this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "_globalUserActiveEmail", {
            get: function () {
                return this.$scope.globals._globalUserActiveEmail;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "globalUserLoginName", {
            get: function () {
                return this.$scope.globals.globalUserLoginName;
            },
            set: function (globalUserLoginName_newVal) {
                this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "_globalUserLoginName", {
            get: function () {
                return this.$scope.globals._globalUserLoginName;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "globalUserVat", {
            get: function () {
                return this.$scope.globals.globalUserVat;
            },
            set: function (globalUserVat_newVal) {
                this.$scope.globals.globalUserVat = globalUserVat_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "_globalUserVat", {
            get: function () {
                return this.$scope.globals._globalUserVat;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "globalUserId", {
            get: function () {
                return this.$scope.globals.globalUserId;
            },
            set: function (globalUserId_newVal) {
                this.$scope.globals.globalUserId = globalUserId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "_globalUserId", {
            get: function () {
                return this.$scope.globals._globalUserId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "globalSubsId", {
            get: function () {
                return this.$scope.globals.globalSubsId;
            },
            set: function (globalSubsId_newVal) {
                this.$scope.globals.globalSubsId = globalSubsId_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "_globalSubsId", {
            get: function () {
                return this.$scope.globals._globalSubsId;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "globalSubsDescription", {
            get: function () {
                return this.$scope.globals.globalSubsDescription;
            },
            set: function (globalSubsDescription_newVal) {
                this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "_globalSubsDescription", {
            get: function () {
                return this.$scope.globals._globalSubsDescription;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "globalSubsSecClasses", {
            get: function () {
                return this.$scope.globals.globalSubsSecClasses;
            },
            set: function (globalSubsSecClasses_newVal) {
                this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "globalSubsCode", {
            get: function () {
                return this.$scope.globals.globalSubsCode;
            },
            set: function (globalSubsCode_newVal) {
                this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "_globalSubsCode", {
            get: function () {
                return this.$scope.globals._globalSubsCode;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "globalLang", {
            get: function () {
                return this.$scope.globals.globalLang;
            },
            set: function (globalLang_newVal) {
                this.$scope.globals.globalLang = globalLang_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "_globalLang", {
            get: function () {
                return this.$scope.globals._globalLang;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "sessionClientIp", {
            get: function () {
                return this.$scope.globals.sessionClientIp;
            },
            set: function (sessionClientIp_newVal) {
                this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "_sessionClientIp", {
            get: function () {
                return this.$scope.globals._sessionClientIp;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "globalDema", {
            get: function () {
                return this.$scope.globals.globalDema;
            },
            set: function (globalDema_newVal) {
                this.$scope.globals.globalDema = globalDema_newVal;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ModelLovCultivationBase.prototype, "_globalDema", {
            get: function () {
                return this.$scope.globals._globalDema;
            },
            enumerable: true,
            configurable: true
        });
        return ModelLovCultivationBase;
    })(Controllers.AbstractLovModel);
    Controllers.ModelLovCultivationBase = ModelLovCultivationBase;
    var ControllerLovCultivationBase = (function (_super) {
        __extends(ControllerLovCultivationBase, _super);
        function ControllerLovCultivationBase($scope, $http, $timeout, Plato, model) {
            _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 5, maxLinesInHeader: 1 });
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.model = model;
            var self = this;
            model.controller = self;
            $scope.clearBtnAction = function () {
                self.model.fsch_CultivationLov_name = undefined;
                self.model.fsch_CultivationLov_code = undefined;
                self.updateGrid();
            };
            $scope._disabled =
                function () {
                    return self._disabled();
                };
            $scope._invisible =
                function () {
                    return self._invisible();
                };
            $scope.LovCultivation_fsch_CultivationLov_name_disabled =
                function () {
                    return self.LovCultivation_fsch_CultivationLov_name_disabled();
                };
            $scope.LovCultivation_fsch_CultivationLov_code_disabled =
                function () {
                    return self.LovCultivation_fsch_CultivationLov_code_disabled();
                };
            $scope.modelLovCultivation = model;
            self.updateUI();
        }
        ControllerLovCultivationBase.prototype.dynamicMessage = function (sMsg) {
            return Messages.dynamicMessage(sMsg);
        };
        Object.defineProperty(ControllerLovCultivationBase.prototype, "ControllerClassName", {
            get: function () {
                return "ControllerLovCultivation";
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ControllerLovCultivationBase.prototype, "HtmlDivId", {
            get: function () {
                return "Cultivation_Id";
            },
            enumerable: true,
            configurable: true
        });
        ControllerLovCultivationBase.prototype.getGridColumnDefinitions = function () {
            var self = this;
            if (isVoid(self.model.dialogOptions)) {
                self.model.dialogOptions = self.$scope.globals.findAndRemoveDialogOptionByClassName(self.ControllerClassName);
            }
            var ret = [
                { cellClass: 'cellToolTip', field: 'name', displayName: 'getALString("Name", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '83%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.name' /> \
                </div>" },
                { cellClass: 'cellToolTip', field: 'code', displayName: 'getALString("Code", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '11%', cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()' > \
                    <label data-ng-bind='row.entity.code' /> \
                </div>" },
                {
                    width: '1',
                    cellTemplate: '<div></div>',
                    cellClass: undefined,
                    field: undefined,
                    displayName: undefined,
                    resizable: undefined,
                    sortable: undefined,
                    enableCellEdit: undefined
                }
            ].filter((function (cl) { return cl.field === undefined || self.model.dialogOptions.shownCols[cl.field] === true; }));
            if (self.isMultiSelect()) {
                ret.splice(0, 0, { width: '22', cellTemplate: "<div class=\"GridSpecialCheckBox\" ><input class=\"npGridSelectCheckBox\" type=\"checkbox\" data-ng-click=\"setSelectedRow(row.rowIndex);selectEntityCheckBoxClicked(row.entity)\" data-ng-checked=\"isEntitySelected(row.entity)\" > </input></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined });
            }
            return ret;
        };
        ControllerLovCultivationBase.prototype.getEntitiesFromJSON = function (webResponse) {
            return Entities.Cultivation.fromJSONComplete(webResponse.data);
        };
        Object.defineProperty(ControllerLovCultivationBase.prototype, "modelLovCultivation", {
            get: function () {
                var self = this;
                return self.model;
            },
            enumerable: true,
            configurable: true
        });
        ControllerLovCultivationBase.prototype.initializeModelWithPreviousValues = function () {
            var self = this;
            var prevModel = self.model.dialogOptions.previousModel;
            if (prevModel !== undefined) {
                self.modelLovCultivation.fsch_CultivationLov_name = prevModel.fsch_CultivationLov_name;
                self.modelLovCultivation.fsch_CultivationLov_code = prevModel.fsch_CultivationLov_code;
            }
        };
        ControllerLovCultivationBase.prototype._disabled = function () {
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal = false;
            return programmerVal;
        };
        ControllerLovCultivationBase.prototype._invisible = function () {
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal = false;
            return programmerVal;
        };
        ControllerLovCultivationBase.prototype.LovCultivation_fsch_CultivationLov_name_disabled = function () {
            var self = this;
            return false;
        };
        ControllerLovCultivationBase.prototype.LovCultivation_fsch_CultivationLov_code_disabled = function () {
            var self = this;
            return false;
        };
        return ControllerLovCultivationBase;
    })(Controllers.LovController);
    Controllers.ControllerLovCultivationBase = ControllerLovCultivationBase;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovCultivationBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
