var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/AgencyBase.ts" />
/// <reference path="../Controllers/Agency.ts" />
var Controllers;
(function (Controllers) {
    var Agency;
    (function (Agency) {
        var PageModelBase = (function (_super) {
            __extends(PageModelBase, _super);
            function PageModelBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(PageModelBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageModelBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return PageModelBase;
        })(Controllers.AbstractPageModel);
        Agency.PageModelBase = PageModelBase;
        var PageControllerBase = (function (_super) {
            __extends(PageControllerBase, _super);
            function PageControllerBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model);
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this._firstLevelGroupControllers = undefined;
                this._allGroupControllers = undefined;
                var self = this;
                model.controller = self;
                $scope.pageModel = self.model;
                $scope._saveIsDisabled =
                    function () {
                        return self._saveIsDisabled();
                    };
                $scope._cancelIsDisabled =
                    function () {
                        return self._cancelIsDisabled();
                    };
                $scope.onSaveBtnAction = function () {
                    self.onSaveBtnAction(function (response) { self.onSuccesfullSaveFromButton(response); });
                };
                $timeout(function () {
                    $('#Agency').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                    $('#NpMainContent').scrollTop(0);
                }, 0);
            }
            Object.defineProperty(PageControllerBase.prototype, "ControllerClassName", {
                get: function () {
                    return "PageController";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "HtmlDivId", {
                get: function () {
                    return "Agency";
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._getPageTitle = function () {
                return "Paying Agency";
            };
            Object.defineProperty(PageControllerBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype._saveIsDisabled = function () {
                var self = this;
                if (!self.$scope.globals.hasPrivilege("Niva_Agency_W"))
                    return true; // 
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            PageControllerBase.prototype._cancelIsDisabled = function () {
                var self = this;
                var isContainerControlDisabled = false;
                var disabledByProgrammerMethod = false;
                return disabledByProgrammerMethod || isContainerControlDisabled;
            };
            Object.defineProperty(PageControllerBase.prototype, "pageModel", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.update = function () {
                var self = this;
                self.model.modelGrpAgency.controller.updateUI();
            };
            PageControllerBase.prototype.refreshVisibleEntities = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpAgency.controller.refreshVisibleEntities(newEntitiesIds);
            };
            PageControllerBase.prototype.refreshGroups = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpAgency.controller.refreshVisibleEntities(newEntitiesIds);
            };
            Object.defineProperty(PageControllerBase.prototype, "FirstLevelGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._firstLevelGroupControllers === undefined) {
                        self._firstLevelGroupControllers = [
                            self.model.modelGrpAgency.controller
                        ];
                    }
                    return this._firstLevelGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageControllerBase.prototype, "AllGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._allGroupControllers === undefined) {
                        self._allGroupControllers = [
                            self.model.modelGrpAgency.controller
                        ];
                    }
                    return this._allGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            PageControllerBase.prototype.getPageChanges = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var pageChanges = [];
                pageChanges = pageChanges.concat(self.model.modelGrpAgency.controller.getChangesToCommit());
                return pageChanges;
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbUrl = function () {
                return "/Niva/rest/MainService/synchronizeChangesWithDb_Agency";
            };
            PageControllerBase.prototype.getSynchronizeChangesWithDbData = function (bForDelete) {
                if (bForDelete === void 0) { bForDelete = false; }
                var self = this;
                var paramData = {};
                paramData.data = self.getPageChanges(bForDelete);
                return paramData;
            };
            PageControllerBase.prototype.onSuccesfullSaveFromButton = function (response) {
                var self = this;
                _super.prototype.onSuccesfullSaveFromButton.call(this, response);
                if (!isVoid(response.warningMessages)) {
                    NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
                }
                if (!isVoid(response.infoMessages)) {
                    NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
                }
                self.model.modelGrpAgency.controller.cleanUpAfterSave();
                self.$scope.globals.isCurrentTransactionDirty = false;
                self.refreshGroups(response.newEntitiesIds);
            };
            PageControllerBase.prototype.onFailuredSave = function (data, status) {
                var self = this;
                if (self.$scope.globals.nInFlightRequests > 0)
                    self.$scope.globals.nInFlightRequests--;
                var errors = Messages.dynamicMessage(data);
                NpTypes.AlertMessage.addDanger(self.model, errors);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
            };
            PageControllerBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            PageControllerBase.prototype.onSaveBtnAction = function (onSuccess) {
                var self = this;
                NpTypes.AlertMessage.clearAlerts(self.model);
                var errors = self.validatePage();
                if (errors.length > 0) {
                    var errMessage = errors.join('<br>');
                    NpTypes.AlertMessage.addDanger(self.model, errMessage);
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>", IconKind.ERROR, [new Tuple2("OK", function () { }),], 0, 0, '50em');
                    return;
                }
                if (self.$scope.globals.isCurrentTransactionDirty === false) {
                    var jqSaveBtn = self.SaveBtnJQueryHandler;
                    self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                    return;
                }
                var url = self.getSynchronizeChangesWithDbUrl();
                var wsPath = this.getWsPathFromUrl(url);
                var paramData = self.getSynchronizeChangesWithDbData();
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, { params: paramData }, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status);
                });
            };
            PageControllerBase.prototype.onPageUnload = function (actualNavigation) {
                var self = this;
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, [
                        new Tuple2("MessageBox_Button_Yes", function () {
                            self.onSaveBtnAction(function (saveResponse) {
                                actualNavigation(self.$scope);
                            });
                        }),
                        new Tuple2("MessageBox_Button_No", function () {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", function () { })
                    ], 0, 2);
                }
                else {
                    actualNavigation(self.$scope);
                }
            };
            return PageControllerBase;
        })(Controllers.AbstractPageController);
        Agency.PageControllerBase = PageControllerBase;
        // GROUP GrpAgency
        var ModelGrpAgencyBase = (function (_super) {
            __extends(ModelGrpAgencyBase, _super);
            function ModelGrpAgencyBase($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
                this._fsch_name = new NpTypes.UIStringModel(undefined);
                this._fsch_country = new NpTypes.UIStringModel(undefined);
            }
            Object.defineProperty(ModelGrpAgencyBase.prototype, "agenId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].agenId;
                },
                set: function (agenId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].agenId = agenId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "_agenId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._agenId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "name", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].name;
                },
                set: function (name_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].name = name_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "_name", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._name;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "country", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].country;
                },
                set: function (country_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].country = country_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "_country", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._country;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].rowVersion;
                },
                set: function (rowVersion_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].rowVersion = rowVersion_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "_rowVersion", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._rowVersion;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "srid", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].srid;
                },
                set: function (srid_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].srid = srid_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "_srid", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._srid;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "fsch_name", {
                get: function () {
                    return this._fsch_name.value;
                },
                set: function (vl) {
                    this._fsch_name.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "fsch_country", {
                get: function () {
                    return this._fsch_country.value;
                },
                set: function (vl) {
                    this._fsch_country.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "appName", {
                get: function () {
                    return this.$scope.globals.appName;
                },
                set: function (appName_newVal) {
                    this.$scope.globals.appName = appName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "_appName", {
                get: function () {
                    return this.$scope.globals._appName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "appTitle", {
                get: function () {
                    return this.$scope.globals.appTitle;
                },
                set: function (appTitle_newVal) {
                    this.$scope.globals.appTitle = appTitle_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "_appTitle", {
                get: function () {
                    return this.$scope.globals._appTitle;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "globalUserEmail", {
                get: function () {
                    return this.$scope.globals.globalUserEmail;
                },
                set: function (globalUserEmail_newVal) {
                    this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "_globalUserEmail", {
                get: function () {
                    return this.$scope.globals._globalUserEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals.globalUserActiveEmail;
                },
                set: function (globalUserActiveEmail_newVal) {
                    this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "_globalUserActiveEmail", {
                get: function () {
                    return this.$scope.globals._globalUserActiveEmail;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "globalUserLoginName", {
                get: function () {
                    return this.$scope.globals.globalUserLoginName;
                },
                set: function (globalUserLoginName_newVal) {
                    this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "_globalUserLoginName", {
                get: function () {
                    return this.$scope.globals._globalUserLoginName;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "globalUserVat", {
                get: function () {
                    return this.$scope.globals.globalUserVat;
                },
                set: function (globalUserVat_newVal) {
                    this.$scope.globals.globalUserVat = globalUserVat_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "_globalUserVat", {
                get: function () {
                    return this.$scope.globals._globalUserVat;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "globalUserId", {
                get: function () {
                    return this.$scope.globals.globalUserId;
                },
                set: function (globalUserId_newVal) {
                    this.$scope.globals.globalUserId = globalUserId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "_globalUserId", {
                get: function () {
                    return this.$scope.globals._globalUserId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "globalSubsId", {
                get: function () {
                    return this.$scope.globals.globalSubsId;
                },
                set: function (globalSubsId_newVal) {
                    this.$scope.globals.globalSubsId = globalSubsId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "_globalSubsId", {
                get: function () {
                    return this.$scope.globals._globalSubsId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "globalSubsDescription", {
                get: function () {
                    return this.$scope.globals.globalSubsDescription;
                },
                set: function (globalSubsDescription_newVal) {
                    this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "_globalSubsDescription", {
                get: function () {
                    return this.$scope.globals._globalSubsDescription;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "globalSubsSecClasses", {
                get: function () {
                    return this.$scope.globals.globalSubsSecClasses;
                },
                set: function (globalSubsSecClasses_newVal) {
                    this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "globalSubsCode", {
                get: function () {
                    return this.$scope.globals.globalSubsCode;
                },
                set: function (globalSubsCode_newVal) {
                    this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "_globalSubsCode", {
                get: function () {
                    return this.$scope.globals._globalSubsCode;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "globalLang", {
                get: function () {
                    return this.$scope.globals.globalLang;
                },
                set: function (globalLang_newVal) {
                    this.$scope.globals.globalLang = globalLang_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "_globalLang", {
                get: function () {
                    return this.$scope.globals._globalLang;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "sessionClientIp", {
                get: function () {
                    return this.$scope.globals.sessionClientIp;
                },
                set: function (sessionClientIp_newVal) {
                    this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "_sessionClientIp", {
                get: function () {
                    return this.$scope.globals._sessionClientIp;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "globalDema", {
                get: function () {
                    return this.$scope.globals.globalDema;
                },
                set: function (globalDema_newVal) {
                    this.$scope.globals.globalDema = globalDema_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpAgencyBase.prototype, "_globalDema", {
                get: function () {
                    return this.$scope.globals._globalDema;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpAgencyBase;
        })(Controllers.AbstractGroupTableModel);
        Agency.ModelGrpAgencyBase = ModelGrpAgencyBase;
        var ControllerGrpAgencyBase = (function (_super) {
            __extends(ControllerGrpAgencyBase, _super);
            function ControllerGrpAgencyBase($scope, $http, $timeout, Plato, model) {
                _super.call(this, $scope, $http, $timeout, Plato, model, { pageSize: 10, maxLinesInHeader: 1 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.model = model;
                this._items = undefined;
                this.getMergedItems_cache = undefined;
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpAgency = self.model;
                $scope._disabled =
                    function () {
                        return self._disabled();
                    };
                $scope._invisible =
                    function () {
                        return self._invisible();
                    };
                $scope.GrpAgency_srchr__fsch_name_disabled =
                    function (agency) {
                        return self.GrpAgency_srchr__fsch_name_disabled(agency);
                    };
                $scope.GrpAgency_srchr__fsch_country_disabled =
                    function (agency) {
                        return self.GrpAgency_srchr__fsch_country_disabled(agency);
                    };
                $scope.GrpAgency_itm__name_disabled =
                    function (agency) {
                        return self.GrpAgency_itm__name_disabled(agency);
                    };
                $scope.GrpAgency_itm__country_disabled =
                    function (agency) {
                        return self.GrpAgency_itm__country_disabled(agency);
                    };
                $scope.GrpAgency_itm__srid_disabled =
                    function (agency) {
                        return self.GrpAgency_itm__srid_disabled(agency);
                    };
                $scope.pageModel.modelGrpAgency = $scope.modelGrpAgency;
                $scope.clearBtnAction = function () {
                    self.modelGrpAgency.fsch_name = undefined;
                    self.modelGrpAgency.fsch_country = undefined;
                    self.updateGrid(0, false, true);
                };
                self.$timeout(function () {
                    self.updateUI();
                }, 0);
                /*
                        $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                            $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.Agency[] , oldVisible:Entities.Agency[]) => {
                                console.log("visible entities changed",newVisible);
                                self.onVisibleItemsChange(newVisible, oldVisible);
                            } )  ));
                */
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            ControllerGrpAgencyBase.prototype.dynamicMessage = function (sMsg) {
                return Messages.dynamicMessage(sMsg);
            };
            Object.defineProperty(ControllerGrpAgencyBase.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpAgency";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpAgencyBase.prototype, "HtmlDivId", {
                get: function () {
                    return "Agency_ControllerGrpAgency";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpAgencyBase.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [
                            new Controllers.TextItem(function (ent) { return 'Name'; }, true, function (ent) { return self.model.fsch_name; }, function (ent) { return self.model._fsch_name; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'Country'; }, true, function (ent) { return self.model.fsch_country; }, function (ent) { return self.model._fsch_country; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'Name'; }, false, function (ent) { return ent.name; }, function (ent) { return ent._name; }, function (ent) { return true; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.TextItem(function (ent) { return 'Country'; }, false, function (ent) { return ent.country; }, function (ent) { return ent._country; }, function (ent) { return false; }, function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined),
                            new Controllers.NumberItem(function (ent) { return 'SRID'; }, false, function (ent) { return ent.srid; }, function (ent) { return ent._srid; }, function (ent) { return false; }, //isRequired
                            function (vl, ent) { return new NpTypes.ValidationResult(true, ""); }, undefined, undefined, 0)
                        ];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpAgencyBase.prototype.gridTitle = function () {
                return "Paying Agency";
            };
            ControllerGrpAgencyBase.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpAgencyBase.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    { width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },
                    { cellClass: 'cellToolTip', field: 'name', displayName: 'getALString("Name", true)', requiredAsterisk: self.$scope.globals.hasPrivilege("Niva_Agency_W"), resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpAgency_itm__name' data-ng-model='row.entity.name' data-np-ui-model='row.entity._name' data-ng-change='markEntityAsUpdated(row.entity,&quot;name&quot;)' data-np-required='true' data-ng-readonly='GrpAgency_itm__name_disabled(row.entity)' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'country', displayName: 'getALString("Country", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '19%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpAgency_itm__country' data-ng-model='row.entity.country' data-np-ui-model='row.entity._country' data-ng-change='markEntityAsUpdated(row.entity,&quot;country&quot;)' data-ng-readonly='GrpAgency_itm__country_disabled(row.entity)' data-np-text='dummy' /> \
                </div>" },
                    { cellClass: 'cellToolTip', field: 'srid', displayName: 'getALString("SRID", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '8%', cellTemplate: "<div data-ng-class='col.colIndex()'> \
                    <input name='GrpAgency_itm__srid' data-ng-model='row.entity.srid' data-np-ui-model='row.entity._srid' data-ng-change='markEntityAsUpdated(row.entity,&quot;srid&quot;)' data-ng-readonly='GrpAgency_itm__srid_disabled(row.entity)' data-np-number='dummy' data-np-decimals='0' data-np-decSep=',' data-np-thSep='' class='ngCellNumber' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpAgencyBase.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpAgencyBase.prototype, "modelGrpAgency", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpAgencyBase.prototype.getEntityName = function () {
                return "Agency";
            };
            ControllerGrpAgencyBase.prototype.cleanUpAfterSave = function () {
                _super.prototype.cleanUpAfterSave.call(this);
                this.getMergedItems_cache = undefined;
            };
            ControllerGrpAgencyBase.prototype.getEntitiesFromJSON = function (webResponse) {
                var _this = this;
                var entlist = Entities.Agency.fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    x.markAsDirty = function (x, itemId) {
                        _this.markEntityAsUpdated(x, itemId);
                    };
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpAgencyBase.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpAgency.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpAgencyBase.prototype.isEntityLocked = function (cur, lockKind) {
                var self = this;
                if (cur === null || cur === undefined)
                    return true;
                return false;
            };
            ControllerGrpAgencyBase.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgency) && !isVoid(self.modelGrpAgency.fsch_name)) {
                    paramData['fsch_name'] = self.modelGrpAgency.fsch_name;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgency) && !isVoid(self.modelGrpAgency.fsch_country)) {
                    paramData['fsch_country'] = self.modelGrpAgency.fsch_country;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpAgencyBase.prototype.makeWebRequestGetIds = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "Agency/findAllByCriteriaRange_AgencyGrpAgency_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.modelGrpAgency) && !isVoid(self.modelGrpAgency.fsch_name)) {
                    paramData['fsch_name'] = self.modelGrpAgency.fsch_name;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgency) && !isVoid(self.modelGrpAgency.fsch_country)) {
                    paramData['fsch_country'] = self.modelGrpAgency.fsch_country;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpAgencyBase.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "Agency/findAllByCriteriaRange_AgencyGrpAgency";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgency) && !isVoid(self.modelGrpAgency.fsch_name)) {
                    paramData['fsch_name'] = self.modelGrpAgency.fsch_name;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgency) && !isVoid(self.modelGrpAgency.fsch_country)) {
                    paramData['fsch_country'] = self.modelGrpAgency.fsch_country;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpAgencyBase.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = "Agency/findAllByCriteriaRange_AgencyGrpAgency_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.modelGrpAgency) && !isVoid(self.modelGrpAgency.fsch_name)) {
                    paramData['fsch_name'] = self.modelGrpAgency.fsch_name;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpAgency) && !isVoid(self.modelGrpAgency.fsch_country)) {
                    paramData['fsch_country'] = self.modelGrpAgency.fsch_country;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpAgencyBase.prototype.getMergedItems = function (func, bContinuousCaller, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache === undefined;
                if (bMakeWebRequest) {
                    var pendingRequest = self.getMergedItems_cache !== undefined &&
                        self.getMergedItems_cache.a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache = new Tuple2(true, undefined);
                    var wsPath = "Agency/findAllByCriteriaRange_AgencyGrpAgency";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                        success(function (response, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                        if (self.$scope.globals.nInFlightRequests > 0)
                            self.$scope.globals.nInFlightRequests--;
                        var dbEntities = self.getEntitiesFromJSON(response);
                        self.getMergedItems_cache.a = false;
                        self.getMergedItems_cache.b = dbEntities;
                        processDBItems(dbEntities);
                    }).error(function (data, status, header, config) {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                        if (self.$scope.globals.nInFlightRequests > 0)
                            self.$scope.globals.nInFlightRequests--;
                        self.getMergedItems_cache.a = false;
                        self.getMergedItems_cache.b = [];
                        NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                        console.error("Error received in getMergedItems:" + data);
                        console.dir(status);
                        console.dir(header);
                        console.dir(config);
                    });
                }
                else {
                    var bPendingRequest = self.getMergedItems_cache.a;
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        }
                        else {
                            processDBItems([]);
                        }
                    }
                    else {
                        var dbEntities = self.getMergedItems_cache.b;
                        processDBItems(dbEntities);
                    }
                }
            };
            ControllerGrpAgencyBase.prototype.constructEntity = function () {
                var self = this;
                var ret = new Entities.Agency(
                /*agenId:string*/ self.$scope.globals.getNextSequenceValue(), 
                /*name:string*/ null, 
                /*country:string*/ null, 
                /*rowVersion:number*/ null, 
                /*srid:number*/ null);
                return ret;
            };
            ControllerGrpAgencyBase.prototype.createNewEntityAndAddToUI = function (bContinuousCaller) {
                if (bContinuousCaller === void 0) { bContinuousCaller = false; }
                var self = this;
                var newEnt = self.constructEntity();
                self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
                self.$scope.globals.isCurrentTransactionDirty = true;
                self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
                self.focusFirstCellYouCan = true;
                if (!bContinuousCaller) {
                    self.model.totalItems++;
                    self.model.pagingOptions.currentPage = 1;
                    self.updateUI();
                }
                return newEnt;
            };
            ControllerGrpAgencyBase.prototype.cloneEntity = function (src) {
                this.$scope.globals.isCurrentTransactionDirty = true;
                this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
                var ret = this.cloneEntity_internal(src);
                return ret;
            };
            ControllerGrpAgencyBase.prototype.cloneEntity_internal = function (src, calledByParent) {
                if (calledByParent === void 0) { calledByParent = false; }
                var tmpId = this.$scope.globals.getNextSequenceValue();
                var ret = Entities.Agency.Create();
                ret.updateInstance(src);
                ret.agenId = tmpId;
                this.onCloneEntity(ret);
                this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
                this.model.totalItems++;
                if (!calledByParent)
                    this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();
                this.$timeout(function () {
                }, 1);
                return ret;
            };
            Object.defineProperty(ControllerGrpAgencyBase.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpAgencyBase.prototype.deleteEntity = function (ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction) {
                if (bCascade === void 0) { bCascade = false; }
                if (afterDeleteAction === void 0) { afterDeleteAction = function () { }; }
                var self = this;
                if (bCascade) {
                    //delete all entities under this parent
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                }
                else {
                    // delete only new entities under this parent,
                    _super.prototype.deleteEntity.call(this, ent, triggerUpdate, rowIndex, false, function () {
                        afterDeleteAction();
                    });
                }
            };
            ControllerGrpAgencyBase.prototype._disabled = function () {
                if (false)
                    return true;
                var parControl = this._isDisabled();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpAgencyBase.prototype._invisible = function () {
                if (false)
                    return true;
                var parControl = this._isInvisible();
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpAgencyBase.prototype.GrpAgency_srchr__fsch_name_disabled = function (agency) {
                var self = this;
                return false;
            };
            ControllerGrpAgencyBase.prototype.GrpAgency_srchr__fsch_country_disabled = function (agency) {
                var self = this;
                return false;
            };
            ControllerGrpAgencyBase.prototype.GrpAgency_itm__name_disabled = function (agency) {
                var self = this;
                if (agency === undefined || agency === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Agency_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(agency, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpAgencyBase.prototype.GrpAgency_itm__country_disabled = function (agency) {
                var self = this;
                if (agency === undefined || agency === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Agency_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(agency, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpAgencyBase.prototype.GrpAgency_itm__srid_disabled = function (agency) {
                var self = this;
                if (agency === undefined || agency === null)
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Agency_W"))
                    return true; // no write privilege
                var entityIsDisabled = self.isEntityLocked(agency, Controllers.LockKind.DisabledLock);
                var isContainerControlDisabled = self._isDisabled();
                var disabledByProgrammerMethod = false;
                return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod;
            };
            ControllerGrpAgencyBase.prototype._isDisabled = function () {
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpAgencyBase.prototype._isInvisible = function () {
                if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                    return false;
                if (false)
                    return true;
                var parControl = false;
                if (parControl)
                    return true;
                var programmerVal = false;
                return programmerVal;
            };
            ControllerGrpAgencyBase.prototype._newIsDisabled = function () {
                var self = this;
                if (!self.$scope.globals.hasPrivilege("Niva_Agency_W"))
                    return true; // no write privilege
                var parEntityIsLocked = false;
                if (parEntityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            ControllerGrpAgencyBase.prototype._deleteIsDisabled = function (agency) {
                var self = this;
                if (agency === null || agency === undefined || agency.getEntityName() !== "Agency")
                    return true;
                if (!self.$scope.globals.hasPrivilege("Niva_Agency_D"))
                    return true; // no delete privilege;
                var entityIsLocked = self.isEntityLocked(agency, Controllers.LockKind.DeleteLock);
                if (entityIsLocked)
                    return true;
                var isCtrlIsDisabled = self._isDisabled();
                if (isCtrlIsDisabled)
                    return true;
                return false;
            };
            return ControllerGrpAgencyBase;
        })(Controllers.AbstractGroupTableController);
        Agency.ControllerGrpAgencyBase = ControllerGrpAgencyBase;
    })(Agency = Controllers.Agency || (Controllers.Agency = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=AgencyBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
