/// <reference path="../../RTL/services.ts" />
/// <reference path="../../RTL/base64Utils.ts" />
/// <reference path="../../RTL/FileSaver.ts" />
/// <reference path="../../RTL/Controllers/BaseController.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../globals.ts" />
/// <reference path="../messages.ts" />
/// <reference path="../EntitiesBase/FileTemplateBase.ts" />
/// <reference path="../EntitiesBase/TemplateColumnBase.ts" />
/// <reference path="../EntitiesBase/PredefColBase.ts" />
/// <reference path="LovPredefColBase.ts" />
/// <reference path="../Controllers/FileTemplate.ts" />
module Controllers.FileTemplate {
    export class PageModelBase extends AbstractPageModel {
        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        modelGrpFileTemplate:ModelGrpFileTemplate;
        modelGrpTemplateColumn:ModelGrpTemplateColumn;
        controller: PageController;
        constructor(public $scope: IPageScope) { super($scope); }
    }


    export interface IPageScopeBase extends IAbstractPageScope {
        globals: Globals;
        _saveIsDisabled():boolean; 
        _cancelIsDisabled():boolean; 
        pageModel : PageModel;
        onSaveBtnAction(): void;
        onNewBtnAction(): void;
        onDeleteBtnAction():void;

    }

    export class PageControllerBase extends AbstractPageController {
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model:PageModel)
        {
            super($scope, $http, $timeout, Plato, model);
            var self: PageControllerBase = this;
            model.controller = <PageController>self;
            $scope.pageModel = self.model;

            $scope._saveIsDisabled = 
                () => {
                    return self._saveIsDisabled();
                };
            $scope._cancelIsDisabled = 
                () => {
                    return self._cancelIsDisabled();
                };

            $scope.onSaveBtnAction = () => { 
                self.onSaveBtnAction((response:NpTypes.SaveResponce) => {self.onSuccesfullSaveFromButton(response);});
            };
            

            $scope.onNewBtnAction = () => { 
                self.onNewBtnAction();
            };
            
            $scope.onDeleteBtnAction = () => { 
                self.onDeleteBtnAction();
            };


            $timeout(function() {
                $('#FileTemplate').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                $('#NpMainContent').scrollTop(0);
            }, 0);

        }
        public get ControllerClassName(): string {
            return "PageController";
        }
        public get HtmlDivId(): string {
            return "FileTemplate";
        }
    
        public _getPageTitle(): string {
            return "Data Import Template";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                ];
            }
            return this._items;
        }

        public _saveIsDisabled():boolean {
            var self = this;

            if (!self.$scope.globals.hasPrivilege("Niva_FileTemplate_W"))
                return true; // 


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }
        public _cancelIsDisabled():boolean {
            var self = this;


            

            var isContainerControlDisabled   = false;
            var disabledByProgrammerMethod = false;
            return disabledByProgrammerMethod || isContainerControlDisabled;

        }

        public get pageModel():PageModel {
            var self = this;
            return self.model;
        }

        
        public update():void {
            var self = this;
            self.model.modelGrpFileTemplate.controller.updateUI();
        }

        public refreshVisibleEntities(newEntitiesIds: NpTypes.NewEntityId[]):void {
            var self = this;
            self.model.modelGrpFileTemplate.controller.refreshVisibleEntities(newEntitiesIds);
            self.model.modelGrpTemplateColumn.controller.refreshVisibleEntities(newEntitiesIds);
        }

        public refreshGroups(newEntitiesIds: NpTypes.NewEntityId[]): void {
            var self = this;
            self.model.modelGrpFileTemplate.controller.refreshVisibleEntities(newEntitiesIds, () => {
                self.model.modelGrpTemplateColumn.controller.updateUI();
            });
        }

        _firstLevelGroupControllers: Array<AbstractGroupController>=undefined;
        public get FirstLevelGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelGroupControllers === undefined) {
                self._firstLevelGroupControllers = [
                    self.model.modelGrpFileTemplate.controller
                ];
            }
            return this._firstLevelGroupControllers;
        }

        _allGroupControllers: Array<AbstractGroupController>=undefined;
        public get AllGroupControllers(): Array<AbstractGroupController> {
            var self = this;
            if (self._allGroupControllers === undefined) {
                self._allGroupControllers = [
                    self.model.modelGrpFileTemplate.controller,
                    self.model.modelGrpTemplateColumn.controller
                ];
            }
            return this._allGroupControllers;
        }

        public getPageChanges(bForDelete:boolean = false):Array<ChangeToCommit> {
            var self = this;
            var pageChanges: Array<ChangeToCommit> = [];
            if (bForDelete) {
                pageChanges = pageChanges.concat(self.model.modelGrpFileTemplate.controller.getChangesToCommitForDeletion());
                pageChanges = pageChanges.concat(self.model.modelGrpFileTemplate.controller.getDeleteChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpTemplateColumn.controller.getDeleteChangesToCommit());
            } else {

                pageChanges = pageChanges.concat(self.model.modelGrpFileTemplate.controller.getChangesToCommit());
                pageChanges = pageChanges.concat(self.model.modelGrpTemplateColumn.controller.getChangesToCommit());
            }
            
            var hasFileTemplate = pageChanges.some(change => change.entityName === "FileTemplate")
            if (!hasFileTemplate) {
                var validateEntity = new ChangeToCommit(ChangeStatus.UPDATE, 0, "FileTemplate", self.model.modelGrpFileTemplate.controller.Current);
                pageChanges = pageChanges.concat(validateEntity);
            }
            return pageChanges;
        }

        private getSynchronizeChangesWithDbUrl():string { 
            return "/Niva/rest/MainService/synchronizeChangesWithDb_FileTemplate"; 
        }
        
        public getSynchronizeChangesWithDbData(bForDelete:boolean = false):any {
            var self = this;
            var paramData:any = {}
            paramData.data = self.getPageChanges(bForDelete);
            return paramData;
        }


        public onSuccesfullSaveFromButton(response:NpTypes.SaveResponce) {
            var self = this;
            super.onSuccesfullSaveFromButton(response);
            if (!isVoid(response.warningMessages)) {
                NpTypes.AlertMessage.addWarning(self.model, Messages.dynamicMessage((response.warningMessages)));
            }
            if (!isVoid(response.infoMessages)) {
                NpTypes.AlertMessage.addInfo(self.model, Messages.dynamicMessage((response.infoMessages)));
            }

            self.model.modelGrpFileTemplate.controller.cleanUpAfterSave();
            self.model.modelGrpTemplateColumn.controller.cleanUpAfterSave();
            self.$scope.globals.isCurrentTransactionDirty = false;
            self.refreshGroups(response.newEntitiesIds);
        }

        public onFailuredSave(data:any, status:number) {
            var self = this;
            if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
            var errors = Messages.dynamicMessage(data);
            NpTypes.AlertMessage.addDanger(self.model, errors);
            messageBox( self.$scope, self.Plato,"MessageBox_Attention_Title",
                "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errors + "<p>&nbsp;<p>",
                IconKind.ERROR, [new Tuple2("OK", () => {}),], 0, 0,'50em');
        }
        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }
        
        public onSaveBtnAction(onSuccess:(response:NpTypes.SaveResponce)=>void): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            var errors = self.validatePage();
            if (errors.length > 0) {
                var errMessage = errors.join('<br>');
                NpTypes.AlertMessage.addDanger(self.model, errMessage);
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title",
                    "<b>" + Messages.dynamicMessage("OnFailuredSaveMsg") + ":</b><p>&nbsp;<br>" + errMessage + "<p>&nbsp;<p>",
                    IconKind.ERROR,[new Tuple2("OK", () => {}),], 0, 0,'50em');
                return;
            }

            if (self.$scope.globals.isCurrentTransactionDirty === false) {
                var jqSaveBtn = self.SaveBtnJQueryHandler;
                self.showPopover(jqSaveBtn, Messages.dynamicMessage("NoChangesToSaveMsg"), true);
                return;
            }

            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData();
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success((response:NpTypes.SaveResponce, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.refresh(self);
                    self.$scope.globals.isCurrentTransactionDirty = false;

                    if (!self.shownAsDialog()) {
                        var breadcrumbStepModel = <IFormPageBreadcrumbStepModel<Entities.FileTemplate>>self.$scope.getCurrentPageModel();
                        var newFileTemplateId = response.newEntitiesIds.firstOrNull(x => x.entityName === 'FileTemplate');
                        if (!isVoid(newFileTemplateId)) {
                            if (!isVoid(breadcrumbStepModel)) {
                                breadcrumbStepModel.selectedEntity = Entities.FileTemplate.CreateById(newFileTemplateId.databaseId);
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        } else {
                            if (!isVoid(breadcrumbStepModel) && !(isVoid(breadcrumbStepModel.selectedEntity))) {
                                breadcrumbStepModel.selectedEntity.refresh(self);
                            }
                        }
                        self.$scope.setCurrentPageModel(breadcrumbStepModel);
                    }

                    self.onSuccesfullSave(response);
                    onSuccess(response);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    self.onFailuredSave(data, status)
            });
        }

        public getInitialEntity(): Entities.FileTemplate {
            if (this.shownAsDialog()) {
                return <Entities.FileTemplate>this.dialogSelectedEntity();
            } else {
                var breadCrumbStepModel = <IFormPageBreadcrumbStepModel<Entities.FileTemplate>>this.$scope.getPageModelByURL('/FileTemplate');
                if (isVoid(breadCrumbStepModel)) {
                    return null;
                } else {
                    return isVoid(breadCrumbStepModel.selectedEntity) ? null : breadCrumbStepModel.selectedEntity;
                }
            }
        }



        public onPageUnload(actualNavigation: (pageScopeYouAreLeavingFrom?: NpTypes.IApplicationScope) => void) {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => {
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                actualNavigation(self.$scope);
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {
                            self.$scope.globals.isCurrentTransactionDirty = false;
                            actualNavigation(self.$scope);
                        }),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                actualNavigation(self.$scope);
            }
        }


        public onNewBtnAction(): void {
            var self = this;
            if (self.$scope.globals.isCurrentTransactionDirty) {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("SaveChangesWarningMsg"), IconKind.QUESTION, 
                    [
                        new Tuple2("MessageBox_Button_Yes", () => { 
                            self.onSaveBtnAction((saveResponse:NpTypes.SaveResponce)=>{
                                self.addNewRecord();
                            }); 
                        }),
                        new Tuple2("MessageBox_Button_No", () => {self.addNewRecord();}),
                        new Tuple2("MessageBox_Button_Cancel", () => {})
                    ], 0,2);
            } else {
                self.addNewRecord();
            }
        }

        public addNewRecord(): void {
            var self = this;
            NpTypes.AlertMessage.clearAlerts(self.model);
            self.model.modelGrpFileTemplate.controller.cleanUpAfterSave();
            self.model.modelGrpTemplateColumn.controller.cleanUpAfterSave();
            self.model.modelGrpFileTemplate.controller.createNewEntityAndAddToUI();
        }

        public onDeleteBtnAction():void {
            var self = this;
            messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title", Messages.dynamicMessage("EntityDeletionWarningMsg"), IconKind.QUESTION, 
                [
                    new Tuple2("MessageBox_Button_Yes", () => {self.deleteRecord();}),
                    new Tuple2("MessageBox_Button_No", () => {})
                ], 1,1);
        }

        public deleteRecord(): void {
            var self = this;
            if (self.Mode === EditMode.NEW) {
                console.log("Delete pressed in a form while being in new mode!");
                return;
            }
            NpTypes.AlertMessage.clearAlerts(self.model);
            var url = self.getSynchronizeChangesWithDbUrl();
            var wsPath = this.getWsPathFromUrl(url);
            var paramData = self.getSynchronizeChangesWithDbData(true);
            Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
            var wsStartTs = (new Date).getTime();
            self.$http.post(url, {params:paramData}, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    self.$scope.globals.isCurrentTransactionDirty = false;
                    self.$scope.navigateBackward();
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests>0) self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.model, Messages.dynamicMessage(data));
            });
        }
        
        public get Mode(): EditMode {
            var self = this;
            if (isVoid(self.model.modelGrpFileTemplate) || isVoid(self.model.modelGrpFileTemplate.controller))
                return EditMode.NEW;
            return self.model.modelGrpFileTemplate.controller.Mode;
        }

        public _newIsDisabled(): boolean {
            var self = this;
            if (isVoid(self.model.modelGrpFileTemplate) || isVoid(self.model.modelGrpFileTemplate.controller))
                return true;
            return self.model.modelGrpFileTemplate.controller._newIsDisabled();
        }
        public _deleteIsDisabled(): boolean {
            var self = this;
            if (self.Mode === EditMode.NEW)
                return true;
            if (isVoid(self.model.modelGrpFileTemplate) || isVoid(self.model.modelGrpFileTemplate.controller))
                return true;
            return self.model.modelGrpFileTemplate.controller._deleteIsDisabled(self.model.modelGrpFileTemplate.controller.Current);
        }


    }


    

    // GROUP GrpFileTemplate

    export class ModelGrpFileTemplateBase extends Controllers.AbstractGroupFormModel {
        modelGrpTemplateColumn:ModelGrpTemplateColumn;
        controller: ControllerGrpFileTemplate;
        public get fiteId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.FileTemplate>this.selectedEntities[0]).fiteId;
        }

        public set fiteId(fiteId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.FileTemplate>this.selectedEntities[0]).fiteId = fiteId_newVal;
        }

        public get _fiteId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._fiteId;
        }

        public get name():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.FileTemplate>this.selectedEntities[0]).name;
        }

        public set name(name_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.FileTemplate>this.selectedEntities[0]).name = name_newVal;
        }

        public get _name():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._name;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.FileTemplate>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.FileTemplate>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpFileTemplate) { super($scope); }
    }



    export interface IScopeGrpFileTemplateBase extends Controllers.IAbstractFormGroupScope, IPageScope{
        globals: Globals;
        modelGrpFileTemplate : ModelGrpFileTemplate;
        GrpFileTemplate_itm__name_disabled(fileTemplate:Entities.FileTemplate):boolean; 
        GrpFileTemplate_0_disabled():boolean; 
        GrpFileTemplate_0_invisible():boolean; 
        child_group_GrpTemplateColumn_isInvisible():boolean; 

    }



    export class ControllerGrpFileTemplateBase extends Controllers.AbstractGroupFormController {
        constructor(
            public $scope: IScopeGrpFileTemplate,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpFileTemplate)
        {
            super($scope, $http, $timeout, Plato, model);
            var self:ControllerGrpFileTemplateBase = this;
            model.controller = <ControllerGrpFileTemplate>self;
            $scope.modelGrpFileTemplate = self.model;

            var selectedEntity: Entities.FileTemplate = this.$scope.pageModel.controller.getInitialEntity();
            if (isVoid(selectedEntity)) {
                self.createNewEntityAndAddToUI();
            } else {
                var clonedEntity = Entities.FileTemplate.Create();
                clonedEntity.updateInstance(selectedEntity);
                $scope.modelGrpFileTemplate.visibleEntities[0] = clonedEntity;
                $scope.modelGrpFileTemplate.selectedEntities[0] = clonedEntity;
            } 
            $scope.GrpFileTemplate_itm__name_disabled = 
                (fileTemplate:Entities.FileTemplate) => {
                    return self.GrpFileTemplate_itm__name_disabled(fileTemplate);
                };
            $scope.GrpFileTemplate_0_disabled = 
                () => {
                    return self.GrpFileTemplate_0_disabled();
                };
            $scope.GrpFileTemplate_0_invisible = 
                () => {
                    return self.GrpFileTemplate_0_invisible();
                };
            $scope.child_group_GrpTemplateColumn_isInvisible = 
                () => {
                    return self.child_group_GrpTemplateColumn_isInvisible();
                };


            $scope.pageModel.modelGrpFileTemplate = $scope.modelGrpFileTemplate;


    /*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.FileTemplate[] , oldVisible:Entities.FileTemplate[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */
            //bind entity child collection with child controller merged items
            Entities.FileTemplate.templateColumnCollection = (fileTemplate, func) => {
                this.model.modelGrpTemplateColumn.controller.getMergedItems(childEntities => {
                    func(childEntities);
                }, true, fileTemplate);
            }

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
                //unbind entity child collection with child controller merged items
                Entities.FileTemplate.templateColumnCollection = null;//(fileTemplate, func) => { }
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpFileTemplate";
        }
        public get HtmlDivId(): string {
            return "FileTemplate_ControllerGrpFileTemplate";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new TextItem (
                        (ent?: Entities.FileTemplate) => 'Name',
                        false ,
                        (ent?: Entities.FileTemplate) => ent.name,  
                        (ent?: Entities.FileTemplate) => ent._name,  
                        (ent?: Entities.FileTemplate) => true, 
                        (vl: string, ent?: Entities.FileTemplate) => new NpTypes.ValidationResult(true, ""), 
                        undefined)
                ];
            }
            return this._items;
        }

        

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpFileTemplate():ModelGrpFileTemplate {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "FileTemplate";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
        }

        public setCurrentFormPageBreadcrumpStepModel() {
            var self = this;
            if (self.$scope.pageModel.controller.shownAsDialog())
                return;

            var breadCrumbStepModel: IFormPageBreadcrumbStepModel<Entities.FileTemplate> = { selectedEntity: null };
            if (!isVoid(self.Current)) {
                var selectedEntity: Entities.FileTemplate = Entities.FileTemplate.Create();
                selectedEntity.updateInstance(self.Current);
                breadCrumbStepModel.selectedEntity = selectedEntity;
            } else {
                breadCrumbStepModel.selectedEntity = null;
            }
            self.$scope.setCurrentPageModel(breadCrumbStepModel);
        }

        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.FileTemplate.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.FileTemplate {
            var self = this;
            return <Entities.FileTemplate>self.$scope.modelGrpFileTemplate.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.FileTemplate, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  false;

        }







        public constructEntity(): Entities.FileTemplate {
            var self = this;
            var ret = new Entities.FileTemplate(
                /*fiteId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*name:string*/ null,
                /*rowVersion:number*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.FileTemplate = <Entities.FileTemplate>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.$scope.modelGrpFileTemplate.visibleEntities[0] = newEnt;
            self.$scope.modelGrpFileTemplate.selectedEntities[0] = newEnt;
            return newEnt;

        }

        public cloneEntity(src: Entities.FileTemplate): Entities.FileTemplate {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.FileTemplate, calledByParent: boolean= false): Entities.FileTemplate {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.FileTemplate.Create();
            ret.updateInstance(src);
            ret.fiteId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.visibleEntities[0] = ret;
            this.model.selectedEntities[0] = ret;

            this.$timeout( () => {
                this.modelGrpFileTemplate.modelGrpTemplateColumn.controller.cloneAllEntitiesUnderParent(src, ret);
            },1);

            return ret;
        }



        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                    self.modelGrpFileTemplate.modelGrpTemplateColumn.controller
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.FileTemplate, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                self.modelGrpFileTemplate.modelGrpTemplateColumn.controller.deleteAllEntitiesUnderParent(ent, () => {
                    super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
                });
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    self.modelGrpFileTemplate.modelGrpTemplateColumn.controller.deleteNewEntitiesUnderParent(ent);
                    afterDeleteAction();
                });
            }
        }


        public GrpFileTemplate_itm__name_disabled(fileTemplate:Entities.FileTemplate):boolean {
            var self = this;
            if (fileTemplate === undefined || fileTemplate === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_FileTemplate_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(fileTemplate, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpFileTemplate_0_disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpFileTemplate_0_invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = false;
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            var self = this;
            if (!self.$scope.globals.hasPrivilege("Niva_FileTemplate_W"))
                return true; // no write privilege

            

            var parEntityIsLocked   = false;
            if (parEntityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public _deleteIsDisabled(fileTemplate:Entities.FileTemplate):boolean  {
            var self = this;
            if (fileTemplate === null || fileTemplate === undefined || fileTemplate.getEntityName() !== "FileTemplate")
                return true;
            if (!self.$scope.globals.hasPrivilege("Niva_FileTemplate_D"))
                return true; // no delete privilege;




            var entityIsLocked   = self.isEntityLocked(fileTemplate, LockKind.DeleteLock);
            if (entityIsLocked)
                return true;
            var isCtrlIsDisabled  = self._isDisabled();
            if (isCtrlIsDisabled)
                return true;
            return false;

        }

        public child_group_GrpTemplateColumn_isInvisible():boolean {
            var self = this;
            if ((self.model.modelGrpTemplateColumn === undefined) || (self.model.modelGrpTemplateColumn.controller === undefined))
                return false;
            return self.model.modelGrpTemplateColumn.controller._isInvisible()
        }

    }


    // GROUP GrpTemplateColumn

    export class ModelGrpTemplateColumnBase extends Controllers.AbstractGroupTableModel {
        controller: ControllerGrpTemplateColumn;
        public get tecoId():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.TemplateColumn>this.selectedEntities[0]).tecoId;
        }

        public set tecoId(tecoId_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.TemplateColumn>this.selectedEntities[0]).tecoId = tecoId_newVal;
        }

        public get _tecoId():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._tecoId;
        }

        public get clfierName():string {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.TemplateColumn>this.selectedEntities[0]).clfierName;
        }

        public set clfierName(clfierName_newVal:string) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.TemplateColumn>this.selectedEntities[0]).clfierName = clfierName_newVal;
        }

        public get _clfierName():NpTypes.UIStringModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._clfierName;
        }

        public get rowVersion():number {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.TemplateColumn>this.selectedEntities[0]).rowVersion;
        }

        public set rowVersion(rowVersion_newVal:number) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.TemplateColumn>this.selectedEntities[0]).rowVersion = rowVersion_newVal;
        }

        public get _rowVersion():NpTypes.UINumberModel {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._rowVersion;
        }

        public get fiteId():Entities.FileTemplate {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.TemplateColumn>this.selectedEntities[0]).fiteId;
        }

        public set fiteId(fiteId_newVal:Entities.FileTemplate) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.TemplateColumn>this.selectedEntities[0]).fiteId = fiteId_newVal;
        }

        public get _fiteId():NpTypes.UIManyToOneModel<Entities.FileTemplate> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._fiteId;
        }

        public get prcoId():Entities.PredefCol {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<Entities.TemplateColumn>this.selectedEntities[0]).prcoId;
        }

        public set prcoId(prcoId_newVal:Entities.PredefCol) {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return;
            (<Entities.TemplateColumn>this.selectedEntities[0]).prcoId = prcoId_newVal;
        }

        public get _prcoId():NpTypes.UIManyToOneModel<Entities.PredefCol> {
            if ( (this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                return undefined;
            return (<any>this.selectedEntities[0])._prcoId;
        }

        public get appName():string {
            return this.$scope.globals.appName;
        }

        public set appName(appName_newVal:string) {
            this.$scope.globals.appName = appName_newVal;
        }
        public get _appName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appName;
        }
        public get appTitle():string {
            return this.$scope.globals.appTitle;
        }

        public set appTitle(appTitle_newVal:string) {
            this.$scope.globals.appTitle = appTitle_newVal;
        }
        public get _appTitle():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._appTitle;
        }
        public get globalUserEmail():string {
            return this.$scope.globals.globalUserEmail;
        }

        public set globalUserEmail(globalUserEmail_newVal:string) {
            this.$scope.globals.globalUserEmail = globalUserEmail_newVal;
        }
        public get _globalUserEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserEmail;
        }
        public get globalUserActiveEmail():string {
            return this.$scope.globals.globalUserActiveEmail;
        }

        public set globalUserActiveEmail(globalUserActiveEmail_newVal:string) {
            this.$scope.globals.globalUserActiveEmail = globalUserActiveEmail_newVal;
        }
        public get _globalUserActiveEmail():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserActiveEmail;
        }
        public get globalUserLoginName():string {
            return this.$scope.globals.globalUserLoginName;
        }

        public set globalUserLoginName(globalUserLoginName_newVal:string) {
            this.$scope.globals.globalUserLoginName = globalUserLoginName_newVal;
        }
        public get _globalUserLoginName():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserLoginName;
        }
        public get globalUserVat():string {
            return this.$scope.globals.globalUserVat;
        }

        public set globalUserVat(globalUserVat_newVal:string) {
            this.$scope.globals.globalUserVat = globalUserVat_newVal;
        }
        public get _globalUserVat():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalUserVat;
        }
        public get globalUserId():number {
            return this.$scope.globals.globalUserId;
        }

        public set globalUserId(globalUserId_newVal:number) {
            this.$scope.globals.globalUserId = globalUserId_newVal;
        }
        public get _globalUserId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalUserId;
        }
        public get globalSubsId():number {
            return this.$scope.globals.globalSubsId;
        }

        public set globalSubsId(globalSubsId_newVal:number) {
            this.$scope.globals.globalSubsId = globalSubsId_newVal;
        }
        public get _globalSubsId():NpTypes.UINumberModel {
            return (<any>this.$scope.globals)._globalSubsId;
        }
        public get globalSubsDescription():string {
            return this.$scope.globals.globalSubsDescription;
        }

        public set globalSubsDescription(globalSubsDescription_newVal:string) {
            this.$scope.globals.globalSubsDescription = globalSubsDescription_newVal;
        }
        public get _globalSubsDescription():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsDescription;
        }
        public get globalSubsSecClasses():number[] {
            return this.$scope.globals.globalSubsSecClasses;
        }

        public set globalSubsSecClasses(globalSubsSecClasses_newVal:number[]) {
            this.$scope.globals.globalSubsSecClasses = globalSubsSecClasses_newVal;
        }

        public get globalSubsCode():string {
            return this.$scope.globals.globalSubsCode;
        }

        public set globalSubsCode(globalSubsCode_newVal:string) {
            this.$scope.globals.globalSubsCode = globalSubsCode_newVal;
        }
        public get _globalSubsCode():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalSubsCode;
        }
        public get globalLang():string {
            return this.$scope.globals.globalLang;
        }

        public set globalLang(globalLang_newVal:string) {
            this.$scope.globals.globalLang = globalLang_newVal;
        }
        public get _globalLang():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._globalLang;
        }
        public get sessionClientIp():string {
            return this.$scope.globals.sessionClientIp;
        }

        public set sessionClientIp(sessionClientIp_newVal:string) {
            this.$scope.globals.sessionClientIp = sessionClientIp_newVal;
        }
        public get _sessionClientIp():NpTypes.UIStringModel {
            return (<any>this.$scope.globals)._sessionClientIp;
        }
        public get globalDema():Entities.DecisionMaking {
            return this.$scope.globals.globalDema;
        }

        public set globalDema(globalDema_newVal:Entities.DecisionMaking) {
            this.$scope.globals.globalDema = globalDema_newVal;
        }
        public get _globalDema():NpTypes.UIManyToOneModel<Entities.DecisionMaking> {
            return (<any>this.$scope.globals)._globalDema;
        }
        constructor(public $scope: IScopeGrpTemplateColumn) { super($scope); }
    }



    export interface IScopeGrpTemplateColumnBase extends Controllers.IAbstractTableGroupScope, IScopeGrpFileTemplateBase{
        globals: Globals;
        modelGrpTemplateColumn : ModelGrpTemplateColumn;
        showLov_GrpTemplateColumn_itm__prcoId(templateColumn:Entities.TemplateColumn):void; 
        GrpTemplateColumn_itm__clfierName_disabled(templateColumn:Entities.TemplateColumn):boolean; 
        GrpTemplateColumn_0_disabled():boolean; 
        GrpTemplateColumn_0_invisible():boolean; 

    }



    export class ControllerGrpTemplateColumnBase extends Controllers.AbstractGroupTableController {
        constructor(
            public $scope: IScopeGrpTemplateColumn,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker,
            public model: ModelGrpTemplateColumn)
        {
            super($scope, $http, $timeout, Plato, model, {pageSize: 10, maxLinesInHeader: 1});
            var self:ControllerGrpTemplateColumnBase = this;
            model.controller = <ControllerGrpTemplateColumn>self;
            model.grid.showTotalItems = true;
            model.grid.updateTotalOnDemand = false;

            $scope.modelGrpTemplateColumn = self.model;

            $scope.showLov_GrpTemplateColumn_itm__prcoId = 
                (templateColumn:Entities.TemplateColumn) => {
                    $timeout( () => {        
                        self.showLov_GrpTemplateColumn_itm__prcoId(templateColumn);
                    }, 0);
                };
            $scope.GrpTemplateColumn_itm__clfierName_disabled = 
                (templateColumn:Entities.TemplateColumn) => {
                    return self.GrpTemplateColumn_itm__clfierName_disabled(templateColumn);
                };
            $scope.GrpTemplateColumn_0_disabled = 
                () => {
                    return self.GrpTemplateColumn_0_disabled();
                };
            $scope.GrpTemplateColumn_0_invisible = 
                () => {
                    return self.GrpTemplateColumn_0_invisible();
                };


            $scope.pageModel.modelGrpTemplateColumn = $scope.modelGrpTemplateColumn;


            $scope.clearBtnAction = () => { 
                self.updateGrid(0, false, true);
            };



            $scope.modelGrpFileTemplate.modelGrpTemplateColumn = $scope.modelGrpTemplateColumn;
            self.$timeout( 
                () => {
                    $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                        $scope.$watch('modelGrpFileTemplate.selectedEntities[0]', () => {self.onParentChange();}, false)));
                },
                50);/*        
            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watchCollection(() => model.visibleEntities, (newVisible:Entities.TemplateColumn[] , oldVisible:Entities.TemplateColumn[]) => {
                    console.log("visible entities changed",newVisible);
                    self.onVisibleItemsChange(newVisible, oldVisible);
                } )  ));
    */

            $scope.$on('$destroy', (evt:ng.IAngularEvent, ...cols:any[]):void => {
            });

            
        }


        public dynamicMessage(sMsg: string):string {
            return Messages.dynamicMessage(sMsg);
        }

        public get ControllerClassName(): string {
            return "ControllerGrpTemplateColumn";
        }
        public get HtmlDivId(): string {
            return "FileTemplate_ControllerGrpTemplateColumn";
        }

        _items: Array<Item<any>> = undefined;
        public get Items(): Array<Item<any>> {
            var self = this;
            if (self._items === undefined) {
                self._items = [
                    new LovItem (
                        (ent?: Entities.TemplateColumn) => 'System Column',
                        false ,
                        (ent?: Entities.TemplateColumn) => ent.prcoId,  
                        (ent?: Entities.TemplateColumn) => ent._prcoId,  
                        (ent?: Entities.TemplateColumn) => true, //isRequired
                        (vl: Entities.PredefCol, ent?: Entities.TemplateColumn) => new NpTypes.ValidationResult(true, "")),
                    new TextItem (
                        (ent?: Entities.TemplateColumn) => 'Import Column',
                        false ,
                        (ent?: Entities.TemplateColumn) => ent.clfierName,  
                        (ent?: Entities.TemplateColumn) => ent._clfierName,  
                        (ent?: Entities.TemplateColumn) => true, 
                        (vl: string, ent?: Entities.TemplateColumn) => new NpTypes.ValidationResult(true, ""), 
                        undefined)
                ];
            }
            return this._items;
        }

        

        public gridTitle(): string {
            return "Columns Assignments";
        }

        public getColumnTooltip(fieldName: string): string {
            if (fieldName === 'clfierName') return "You Can Update the Import Columns To Your File Schema";
            return "";
        }

        public gridColumnFilter(field: string): boolean {
            return true;
        }
        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            return [
                { width:'22', cellTemplate:"<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass:undefined, field:undefined, displayName:undefined, resizable:undefined, sortable:undefined, enableCellEdit:undefined },
                { cellClass:'cellToolTip', field:'prcoId.columnName', displayName:'getALString("System Column", true)', requiredAsterisk:false, resizable:true, sortable:true, enableCellEdit:false, width:'19%', cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <div  > \
                    <input data-np-source='row.entity.prcoId.columnName' data-np-ui-model='row.entity._prcoId'  data-np-context='row.entity'  data-np-lookup=\"\" class='npLovInput' name='GrpTemplateColumn_itm__prcoId' readonly='true'  >  \
                    <button class='npLovButton' type='button' data-np-click='showLov_GrpTemplateColumn_itm__prcoId(row.entity)'   data-ng-disabled=\"true\"   />  \
                    </div> \
                </div>"},
                { cellClass:'cellToolTip', field:'clfierName', displayName:'getALString("Import Column", true)', requiredAsterisk:self.$scope.globals.hasPrivilege("Niva_FileTemplate_W"), resizable:true, sortable:true, enableCellEdit:false, width:'19%', headerCellTemplate: this.getGridHeaderCellTemplate({ tooltip: "{{getColumnTooltip('clfierName')}}" }), cellTemplate:"<div data-ng-class='col.colIndex()'> \
                    <input name='GrpTemplateColumn_itm__clfierName' data-ng-model='row.entity.clfierName' data-np-ui-model='row.entity._clfierName' data-ng-change='markEntityAsUpdated(row.entity,&quot;clfierName&quot;)' data-np-required='true' data-ng-readonly='GrpTemplateColumn_itm__clfierName_disabled(row.entity)' data-np-text='dummy' /> \
                </div>"},
                {
                    width:'1',
                    cellTemplate: '<div></div>',
                    cellClass:undefined,
                    field:undefined,
                    displayName:undefined,
                    resizable:undefined,
                    sortable:undefined,
                    enableCellEdit:undefined
                }
            ].filter(col => col.field === undefined || self.gridColumnFilter(col.field));
        }

        public get PageModel(): NpTypes.IPageModel {
            var self = this;
            return self.$scope.pageModel;
        }

        public get modelGrpTemplateColumn():ModelGrpTemplateColumn {
            var self = this;
            return self.model;
        }

        public getEntityName(): string {
            return "TemplateColumn";
        }


        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.getMergedItems_cache = {};

        }

        public getEntitiesFromJSON(webResponse: any, parent?: Entities.FileTemplate): Array<NpTypes.IBaseEntity> {
            var entlist = Entities.TemplateColumn.fromJSONComplete(webResponse.data);
        
            entlist.forEach(x => {
                if (isVoid(parent)) {
                    x.fiteId = this.Parent;
                } else {
                    x.fiteId = parent;
                }

                x.markAsDirty = (x: NpTypes.IBaseEntity, itemId: string) => {
                    this.markEntityAsUpdated(x, itemId);
                }
            });

            return entlist;
        }

        public get Current():Entities.TemplateColumn {
            var self = this;
            return <Entities.TemplateColumn>self.$scope.modelGrpTemplateColumn.selectedEntities[0];
        }



        public isEntityLocked(cur:Entities.TemplateColumn, lockKind:LockKind):boolean  {
            var self = this;
            if (cur === null || cur === undefined)
                return true;

            return  self.$scope.modelGrpFileTemplate.controller.isEntityLocked(<Entities.FileTemplate>cur.fiteId, lockKind);

        }





        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
                var paramData = {};
                    
                var model = self.model;
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.fiteId)) {
                    paramData['fiteId_fiteId'] = self.Parent.fiteId;
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;

        }
        public makeWebRequestGetIds(excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "TemplateColumn/findAllByCriteriaRange_FileTemplateGrpTemplateColumn_getIds";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.fiteId)) {
                    paramData['fiteId_fiteId'] = self.Parent.fiteId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "TemplateColumn/findAllByCriteriaRange_FileTemplateGrpTemplateColumn";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                    
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                
                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                    
                if (model.sortField !== undefined) {
                     paramData['sortField'] = model.sortField;
                     paramData['sortOrder'] = model.sortOrder == 'asc';
                }

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.fiteId)) {
                    paramData['fiteId_fiteId'] = self.Parent.fiteId;
                }

                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
                var wsPath = "TemplateColumn/findAllByCriteriaRange_FileTemplateGrpTemplateColumn_count";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};

                paramData['exc_Id'] = excludedIds;
                var model = self.model;

                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.fiteId)) {
                    paramData['fiteId_fiteId'] = self.Parent.fiteId;
                }

                var promise = self.$http.post(
                    url,
                    paramData,
                    {timeout:self.$scope.globals.timeoutInMS, cache:false});
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });

        }




        private getMergedItems_cache: { [parentId: string]:  Tuple2<boolean, Entities.TemplateColumn[]>; } = {};
        private bNonContinuousCallersFuncs: Array<(items: Entities.TemplateColumn[]) => void> = [];


        public getMergedItems(func: (items: Entities.TemplateColumn[]) => void, bContinuousCaller:boolean=true, parent?: Entities.FileTemplate, bForceUpdate:boolean=false): void {
            var self = this;
            var model = self.model;

            function processDBItems(dbEntities: Entities.TemplateColumn[]):void {
                var mergedEntities = <Entities.TemplateColumn[]>self.processEntities(dbEntities, true);
                var newEntities =
                    model.newEntities.
                        filter(e => parent.isEqual((<Entities.TemplateColumn>e.a).fiteId)).
                        map(e => <Entities.TemplateColumn>e.a);
                var allItems = newEntities.concat(mergedEntities);
                func(allItems);
                self.bNonContinuousCallersFuncs.forEach(f => { f(allItems); });
                self.bNonContinuousCallersFuncs.splice(0);
            }
            if (parent === undefined)
                parent = self.Parent;

            if (isVoid(parent)) {
                console.warn('calling getMergedItems and parent is undefined ...');
                func([]);
                return;
            }




            if (parent.isNew()) {
                processDBItems([]);
            } else {
                var bMakeWebRequest:boolean = bForceUpdate || self.getMergedItems_cache[parent.getKey()] === undefined;

                if (bMakeWebRequest) {
                    var pendingRequest =
                        self.getMergedItems_cache[parent.getKey()] !== undefined &&
                        self.getMergedItems_cache[parent.getKey()].a === true;
                    if (pendingRequest)
                        return;
                    self.getMergedItems_cache[parent.getKey()] = new Tuple2(true, undefined);

                    var wsPath = "TemplateColumn/findAllByCriteriaRange_FileTemplateGrpTemplateColumn";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};
                    paramData['fromRowIndex'] = 0;
                    paramData['toRowIndex'] = 2000;
                    paramData['exc_Id'] = Object.keys(model.deletedEntities);
                    paramData['fiteId_fiteId'] = parent.getKey();



                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                    var wsStartTs = (new Date).getTime();
                    self.$http.post(url,paramData, {timeout:self.$scope.globals.timeoutInMS, cache:false}).
                        success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var dbEntities = <Entities.TemplateColumn[]>self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.getKey()].a = false;
                            self.getMergedItems_cache[parent.getKey()].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                } else {
                    var bPendingRequest = self.getMergedItems_cache[parent.getKey()].a
                    if (bPendingRequest) {
                        if (!bContinuousCaller) {
                            self.bNonContinuousCallersFuncs.push(func);
                        } else {
                            processDBItems([]);
                        }
                    } else {
                        var dbEntities = self.getMergedItems_cache[parent.getKey()].b;
                        processDBItems(dbEntities);
                    }
                }
            }
        }



        public belongsToCurrentParent(x:Entities.TemplateColumn):boolean {
            if (this.Parent === undefined || x.fiteId === undefined)
                return false;
            return x.fiteId.getKey() === this.Parent.getKey();

        }

        public onParentChange():void {
            var self = this;
            var newParent = self.Parent;
            self.model.pagingOptions.currentPage = 1;
            if (newParent !== undefined) {
                if (newParent.isNew()) 
                    self.getMergedItems(items => { self.model.totalItems = items.length; }, false);
                self.updateGrid();
            } else {
                self.model.visibleEntities.splice(0);
                self.model.selectedEntities.splice(0);
                self.model.totalItems = 0;
            }
        }

        public get Parent():Entities.FileTemplate {
            var self = this;
            return self.fiteId;
        }

        public get ParentController() {
            var self = this;
            return self.$scope.modelGrpFileTemplate.controller;
        }

        public get ParentIsNewOrUndefined(): boolean {
            var self = this;
            return self.Parent === undefined || (self.Parent.getKey().indexOf('TEMP_ID') === 0);
        }


        public get fiteId():Entities.FileTemplate {
            var self = this;
            return <Entities.FileTemplate>self.$scope.modelGrpFileTemplate.selectedEntities[0];
        }









        public constructEntity(): Entities.TemplateColumn {
            var self = this;
            var ret = new Entities.TemplateColumn(
                /*tecoId:string*/ self.$scope.globals.getNextSequenceValue(),
                /*clfierName:string*/ null,
                /*rowVersion:number*/ null,
                /*fiteId:Entities.FileTemplate*/ null,
                /*prcoId:Entities.PredefCol*/ null);
            return ret;
        }


        public createNewEntityAndAddToUI(bContinuousCaller:boolean=false): NpTypes.IBaseEntity {
        
            var self = this;
            var newEnt : Entities.TemplateColumn = <Entities.TemplateColumn>self.constructEntity();
            self.model.newEntities.unshift(new Tuple2(newEnt, Utils.getTimeInMS()));
            newEnt.fiteId = self.fiteId;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('insert of ' + newEnt.getEntityName());
            self.focusFirstCellYouCan = true;
            if(!bContinuousCaller){
                self.model.totalItems++;
                self.model.pagingOptions.currentPage = 1;
                self.updateUI();
            }

            return newEnt;

        }

        public cloneEntity(src: Entities.TemplateColumn): Entities.TemplateColumn {
            this.$scope.globals.isCurrentTransactionDirty = true;
            this.markParentEntityAsUpdated('clone of ' + src.getEntityName());
            var ret = this.cloneEntity_internal(src);
            return ret;
        }

        public cloneEntity_internal(src: Entities.TemplateColumn, calledByParent: boolean= false): Entities.TemplateColumn {
            var tmpId = this.$scope.globals.getNextSequenceValue();
            var ret = Entities.TemplateColumn.Create();
            ret.updateInstance(src);
            ret.tecoId = tmpId;
            this.onCloneEntity(ret);
            this.model.newEntities.unshift(new Tuple2(ret, Utils.getTimeInMS()));
            this.model.totalItems++;
            if (!calledByParent)
                this.focusFirstCellYouCan = true;
                this.model.pagingOptions.currentPage = 1;
                this.updateUI();

            this.$timeout( () => {
            },1);

            return ret;
        }



        public cloneAllEntitiesUnderParent(oldParent:Entities.FileTemplate, newParent:Entities.FileTemplate) {
        
            this.model.totalItems = 0;

            this.getMergedItems(templateColumnList => {
                templateColumnList.forEach(templateColumn => {
                    var templateColumnCloned = this.cloneEntity_internal(templateColumn, true);
                    templateColumnCloned.fiteId = newParent;
                });
                this.updateUI();
            },false, oldParent);
        }


        public deleteNewEntitiesUnderParent(fileTemplate: Entities.FileTemplate) {
        

            var self = this;
            var toBeDeleted:Array<Entities.TemplateColumn> = [];
            for (var i = 0; i < self.model.newEntities.length; i++) {
                var change = self.model.newEntities[i];
                var templateColumn = <Entities.TemplateColumn>change.a
                if (fileTemplate.getKey() === templateColumn.fiteId.getKey()) {
                    toBeDeleted.push(templateColumn);
                }
            }

            _.each(toBeDeleted, (x:Entities.TemplateColumn) => {
                self.deleteEntity(x, false, -1);
                // for each child group
                // call deleteNewEntitiesUnderParent(ent)
            });

        }

        public deleteAllEntitiesUnderParent(fileTemplate: Entities.FileTemplate, afterDeleteAction: () => void) {
        
            var self = this;

            function deleteTemplateColumnList(templateColumnList: Entities.TemplateColumn[]) {
                if (templateColumnList.length === 0) {
                    self.$timeout( () => {
                        afterDeleteAction();
                    },1);   //make sure that parents are marked for deletion after 1 ms
                } else {
                    var head = templateColumnList[0];
                    var tail = templateColumnList.slice(1);
                    self.deleteEntity(head, false, -1, true, () => {
                        deleteTemplateColumnList(tail);
                    });
                }
            }


            this.getMergedItems(templateColumnList => {
                deleteTemplateColumnList(templateColumnList);
            }, false, fileTemplate);

        }

        _firstLevelChildGroups: Array<AbstractGroupController>=undefined;
        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            var self = this;
            if (self._firstLevelChildGroups === undefined) {
                self._firstLevelChildGroups = [
                ];
            }
            return this._firstLevelChildGroups;
        }

        public deleteEntity(ent: Entities.TemplateColumn, triggerUpdate:boolean, rowIndex:number, bCascade:boolean=false, afterDeleteAction: () => void = () => { }) {
        
            var self = this;
            if (bCascade) {
                //delete all entities under this parent
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, afterDeleteAction);
            } else {
                // delete only new entities under this parent,
                super.deleteEntity(ent, triggerUpdate, rowIndex, false, () => {
                    afterDeleteAction();
                });
            }
        }


        cachedLovModel_GrpTemplateColumn_itm__prcoId:ModelLovPredefColBase;
        public showLov_GrpTemplateColumn_itm__prcoId(templateColumn:Entities.TemplateColumn) {
            var self = this;
            if (templateColumn === undefined)
                return;
            var uimodel:NpTypes.IUIModel = templateColumn._prcoId;

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = 'Predefined Columns';
            dialogOptions.previousModel= self.cachedLovModel_GrpTemplateColumn_itm__prcoId;
            dialogOptions.className = "ControllerLovPredefCol";
            dialogOptions.onSelect = (selectedEntity: NpTypes.IBaseEntity) => {
                var predefCol1:Entities.PredefCol =   <Entities.PredefCol>selectedEntity;
                uimodel.clearAllErrors();
                if(true && isVoid(predefCol1)) {
                    uimodel.addNewErrorMessage(self.dynamicMessage("FieldValidation_RequiredMsg2"), true);
                    self.$timeout( () => {
                        uimodel.clearAllErrors();
                        },3000);
                    return;
                }
                if (!isVoid(predefCol1) && predefCol1.isEqual(templateColumn.prcoId))
                    return;
                templateColumn.prcoId = predefCol1;
                self.markEntityAsUpdated(templateColumn, 'GrpTemplateColumn_itm__prcoId');

            };
            dialogOptions.openNewEntityDialog =  () => { 
            };

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel:ModelLovPredefColBase, excludedIds:Array<string>=[]) =>  {
                self.cachedLovModel_GrpTemplateColumn_itm__prcoId = lovModel;
                    var wsPath = "PredefCol/findAllByCriteriaRange_FileTemplate_GrpTemplateColumn_itm__prcoId";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                        
                    paramData['fromRowIndex'] = paramIndexFrom;
                    paramData['toRowIndex'] = paramIndexTo;
                    
                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                        
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_PredefColLov_columnName)) {
                        paramData['fsch_PredefColLov_columnName'] = lovModel.fsch_PredefColLov_columnName;
                    }

                    Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };
            dialogOptions.showTotalItems = true;
            dialogOptions.updateTotalOnDemand = false;
            dialogOptions.makeWebRequest_count = (lovModel:ModelLovPredefColBase, excludedIds:Array<string>=[]) =>  {
                    var wsPath = "PredefCol/findAllByCriteriaRange_FileTemplate_GrpTemplateColumn_itm__prcoId_count";
                    var url = "/Niva/rest/" + wsPath + "?";
                    var paramData = {};

                    paramData['exc_Id'] = excludedIds;
                    var model = lovModel;

                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_PredefColLov_columnName)) {
                        paramData['fsch_PredefColLov_columnName'] = lovModel.fsch_PredefColLov_columnName;
                    }

                    var promise = self.$http.post(
                        url,
                        paramData,
                        {timeout:self.$scope.globals.timeoutInMS, cache:false});
                    var wsStartTs = (new Date).getTime();
                    return promise.success(() => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    }).error((data, status, header, config) => {
                        self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: any, excludedIds: Array<string>):string => {
                    var paramData = {};
                        
                    var model = lovModel;
                    if (model.sortField !== undefined) {
                         paramData['sortField'] = model.sortField;
                         paramData['sortOrder'] = model.sortOrder == 'asc';
                    }
                    if (!isVoid(lovModel) && !isVoid(lovModel.fsch_PredefColLov_columnName)) {
                        paramData['fsch_PredefColLov_columnName'] = lovModel.fsch_PredefColLov_columnName;
                    }
                    var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                    return res;

            };


            dialogOptions.shownCols['columnName'] = true;

            self.Plato.showDialog(self.$scope,
                dialogOptions.title,
                "/"+self.$scope.globals.appName+'/partials/lovs/PredefCol.html?rev=' + self.$scope.globals.version,
                dialogOptions);

        }

        public GrpTemplateColumn_itm__clfierName_disabled(templateColumn:Entities.TemplateColumn):boolean {
            var self = this;
            if (templateColumn === undefined || templateColumn === null)
                return true;

            if (!self.$scope.globals.hasPrivilege("Niva_FileTemplate_W"))
                return true; // no write privilege


            var entityIsDisabled   = self.isEntityLocked(templateColumn, LockKind.DisabledLock);
            var isContainerControlDisabled   = self._isDisabled();
            var disabledByProgrammerMethod = false;
            return entityIsDisabled || isContainerControlDisabled || disabledByProgrammerMethod; 
        }
        public GrpTemplateColumn_0_disabled():boolean { 
            if (false)
                return true;
            var parControl = this._isDisabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public GrpTemplateColumn_0_invisible():boolean { 
            if (false)
                return true;
            var parControl = this._isInvisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isDisabled():boolean { 
            if (false)
                return true;
            var parControl = this.ParentController.GrpFileTemplate_0_disabled();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _isInvisible():boolean { 
            
            if (!isVoid(this.HtmlDivId) && this.isParentTabUnderConstruction(this.HtmlDivId))
                return false;
            if (false)
                return true;
            var parControl = this.ParentController.GrpFileTemplate_0_invisible();
            if (parControl)
                return true;
            var programmerVal =  false;
            return programmerVal;
        }
        public _newIsDisabled():boolean  {
            return true;
        }
        public _deleteIsDisabled(templateColumn:Entities.TemplateColumn):boolean  {
            return true;
        }

    }

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
