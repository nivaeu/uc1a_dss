/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/ParcelsIssues.ts" />
/// <reference path="../Entities/Cultivation.ts" />
/// <reference path="../Entities/CoverType.ts" />
/// <reference path="../Entities/GpDecision.ts" />

module Entities {

    export class GpDecisionBase extends NpTypes.BaseEntity {
        constructor(
            gpDecisionsId: string,
            cropOk: number,
            landcoverOk: number,
            dteInsert: Date,
            usrInsert: string,
            rowVersion: number,
            parcelsIssuesId: ParcelsIssues,
            cultId: Cultivation,
            cotyId: CoverType) 
        {
            super();
            var self = this;
            this._gpDecisionsId = new NpTypes.UIStringModel(gpDecisionsId, this);
            this._cropOk = new NpTypes.UINumberModel(cropOk, this);
            this._landcoverOk = new NpTypes.UINumberModel(landcoverOk, this);
            this._dteInsert = new NpTypes.UIDateModel(dteInsert, this);
            this._usrInsert = new NpTypes.UIStringModel(usrInsert, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._parcelsIssuesId = new NpTypes.UIManyToOneModel<Entities.ParcelsIssues>(parcelsIssuesId, this);
            this._cultId = new NpTypes.UIManyToOneModel<Entities.Cultivation>(cultId, this);
            this._cotyId = new NpTypes.UIManyToOneModel<Entities.CoverType>(cotyId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.gpDecisionsId; 
        }
        
        public getKeyName(): string { 
            return "gpDecisionsId"; 
        }

        public getEntityName(): string { 
            return 'GpDecision'; 
        }
        
        public fromJSON(data: any): GpDecision[] { 
            return GpDecision.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.gpDecisionsId === null || this.gpDecisionsId === undefined)
                return true;
            if (this.gpDecisionsId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: GpDecisionBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //gpDecisionsId property
        _gpDecisionsId: NpTypes.UIStringModel;
        public get gpDecisionsId():string {
            return this._gpDecisionsId.value;
        }
        public set gpDecisionsId(gpDecisionsId:string) {
            var self = this;
            self._gpDecisionsId.value = gpDecisionsId;
        //    self.gpDecisionsId_listeners.forEach(cb => { cb(<GpDecision>self); });
        }
        //public gpDecisionsId_listeners: Array<(c:GpDecision) => void> = [];
        //cropOk property
        _cropOk: NpTypes.UINumberModel;
        public get cropOk():number {
            return this._cropOk.value;
        }
        public set cropOk(cropOk:number) {
            var self = this;
            self._cropOk.value = cropOk;
        //    self.cropOk_listeners.forEach(cb => { cb(<GpDecision>self); });
        }
        //public cropOk_listeners: Array<(c:GpDecision) => void> = [];
        //landcoverOk property
        _landcoverOk: NpTypes.UINumberModel;
        public get landcoverOk():number {
            return this._landcoverOk.value;
        }
        public set landcoverOk(landcoverOk:number) {
            var self = this;
            self._landcoverOk.value = landcoverOk;
        //    self.landcoverOk_listeners.forEach(cb => { cb(<GpDecision>self); });
        }
        //public landcoverOk_listeners: Array<(c:GpDecision) => void> = [];
        //dteInsert property
        _dteInsert: NpTypes.UIDateModel;
        public get dteInsert():Date {
            return this._dteInsert.value;
        }
        public set dteInsert(dteInsert:Date) {
            var self = this;
            self._dteInsert.value = dteInsert;
        //    self.dteInsert_listeners.forEach(cb => { cb(<GpDecision>self); });
        }
        //public dteInsert_listeners: Array<(c:GpDecision) => void> = [];
        //usrInsert property
        _usrInsert: NpTypes.UIStringModel;
        public get usrInsert():string {
            return this._usrInsert.value;
        }
        public set usrInsert(usrInsert:string) {
            var self = this;
            self._usrInsert.value = usrInsert;
        //    self.usrInsert_listeners.forEach(cb => { cb(<GpDecision>self); });
        }
        //public usrInsert_listeners: Array<(c:GpDecision) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<GpDecision>self); });
        }
        //public rowVersion_listeners: Array<(c:GpDecision) => void> = [];
        //parcelsIssuesId property
        _parcelsIssuesId: NpTypes.UIManyToOneModel<Entities.ParcelsIssues>;
        public get parcelsIssuesId():ParcelsIssues {
            return this._parcelsIssuesId.value;
        }
        public set parcelsIssuesId(parcelsIssuesId:ParcelsIssues) {
            var self = this;
            self._parcelsIssuesId.value = parcelsIssuesId;
        //    self.parcelsIssuesId_listeners.forEach(cb => { cb(<GpDecision>self); });
        }
        //public parcelsIssuesId_listeners: Array<(c:GpDecision) => void> = [];
        //cultId property
        _cultId: NpTypes.UIManyToOneModel<Entities.Cultivation>;
        public get cultId():Cultivation {
            return this._cultId.value;
        }
        public set cultId(cultId:Cultivation) {
            var self = this;
            self._cultId.value = cultId;
        //    self.cultId_listeners.forEach(cb => { cb(<GpDecision>self); });
        }
        //public cultId_listeners: Array<(c:GpDecision) => void> = [];
        //cotyId property
        _cotyId: NpTypes.UIManyToOneModel<Entities.CoverType>;
        public get cotyId():CoverType {
            return this._cotyId.value;
        }
        public set cotyId(cotyId:CoverType) {
            var self = this;
            self._cotyId.value = cotyId;
        //    self.cotyId_listeners.forEach(cb => { cb(<GpDecision>self); });
        }
        //public cotyId_listeners: Array<(c:GpDecision) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.gpDecisionsId_listeners.splice(0);
            self.cropOk_listeners.splice(0);
            self.landcoverOk_listeners.splice(0);
            self.dteInsert_listeners.splice(0);
            self.usrInsert_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.parcelsIssuesId_listeners.splice(0);
            self.cultId_listeners.splice(0);
            self.cotyId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : GpDecision {
            var key = "GpDecision:"+x.gpDecisionsId;
            var ret =  new GpDecision(
                x.gpDecisionsId,
                x.cropOk,
                x.landcoverOk,
                isVoid(x.dteInsert) ? null : new Date(x.dteInsert),
                x.usrInsert,
                x.rowVersion,
                x.parcelsIssuesId,
                x.cultId,
                x.cotyId
            );
            deserializedEntities[key] = ret;
            ret.parcelsIssuesId = (x.parcelsIssuesId !== undefined && x.parcelsIssuesId !== null) ? <ParcelsIssues>NpTypes.BaseEntity.entitiesFactory[x.parcelsIssuesId.$entityName].fromJSONPartial(x.parcelsIssuesId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.cultId = (x.cultId !== undefined && x.cultId !== null) ? <Cultivation>NpTypes.BaseEntity.entitiesFactory[x.cultId.$entityName].fromJSONPartial(x.cultId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.cotyId = (x.cotyId !== undefined && x.cotyId !== null) ? <CoverType>NpTypes.BaseEntity.entitiesFactory[x.cotyId.$entityName].fromJSONPartial(x.cotyId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): GpDecision {
            
            return <GpDecision>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : GpDecision {
            var self = this;
            var key="";
            var ret:GpDecision = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "GpDecision:"+x.$refId;
                ret = <GpDecision>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "GpDecisionBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "GpDecision:"+x.gpDecisionsId;
                    var cachedCopy = <GpDecision>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = GpDecision.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = GpDecision.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = GpDecision.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<GpDecision> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<GpDecision> = _.map(data, (x:any):GpDecision => {
                return GpDecision.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:GpDecision[])=>void)  {
            var url = "/Niva/rest/GpDecision/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = GpDecision.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.gpDecisionsId = this.gpDecisionsId;
                ret.cropOk = this.cropOk;
                ret.landcoverOk = this.landcoverOk;
                ret.dteInsert = this.dteInsert;
                ret.usrInsert = this.usrInsert;
                ret.rowVersion = this.rowVersion;
                ret.parcelsIssuesId = 
                    (this.parcelsIssuesId !== undefined && this.parcelsIssuesId !== null) ? 
                        { parcelsIssuesId :  this.parcelsIssuesId.parcelsIssuesId} :
                        (this.parcelsIssuesId === undefined ? undefined : null);
                ret.cultId = 
                    (this.cultId !== undefined && this.cultId !== null) ? 
                        { cultId :  this.cultId.cultId} :
                        (this.cultId === undefined ? undefined : null);
                ret.cotyId = 
                    (this.cotyId !== undefined && this.cotyId !== null) ? 
                        { cotyId :  this.cotyId.cotyId} :
                        (this.cotyId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:GpDecision):void {
            var self = this;
            self.gpDecisionsId = other.gpDecisionsId
            self.cropOk = other.cropOk
            self.landcoverOk = other.landcoverOk
            self.dteInsert = other.dteInsert
            self.usrInsert = other.usrInsert
            self.rowVersion = other.rowVersion
            self.parcelsIssuesId = other.parcelsIssuesId
            self.cultId = other.cultId
            self.cotyId = other.cotyId
        }

        public static Create() : GpDecision {
            return new GpDecision(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(gpDecisionsId:string) : GpDecision {
            var ret = GpDecision.Create();
            ret.gpDecisionsId = gpDecisionsId;
            return ret;
        }

        public static cashedEntities: { [id: string]: GpDecision; } = {};
        public static findById_unecrypted(gpDecisionsId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : GpDecision {
            if (Entities.GpDecision.cashedEntities[gpDecisionsId.toString()] !== undefined)
                return Entities.GpDecision.cashedEntities[gpDecisionsId.toString()];

            var wsPath = "GpDecision/findByGpDecisionsId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['gpDecisionsId'] = gpDecisionsId;
            var ret = GpDecision.Create();
            Entities.GpDecision.cashedEntities[gpDecisionsId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.GpDecision.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + gpDecisionsId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['GpDecision'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.GpDecision.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.GpDecision.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
