/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/SuperClas.ts" />
/// <reference path="../Entities/Cultivation.ts" />
/// <reference path="../Entities/SuperClassDetail.ts" />

module Entities {

    export class SuperClassDetailBase extends NpTypes.BaseEntity {
        constructor(
            sucdId: string,
            rowVersion: number,
            sucaId: SuperClas,
            cultId: Cultivation) 
        {
            super();
            var self = this;
            this._sucdId = new NpTypes.UIStringModel(sucdId, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._sucaId = new NpTypes.UIManyToOneModel<Entities.SuperClas>(sucaId, this);
            this._cultId = new NpTypes.UIManyToOneModel<Entities.Cultivation>(cultId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.sucdId; 
        }
        
        public getKeyName(): string { 
            return "sucdId"; 
        }

        public getEntityName(): string { 
            return 'SuperClassDetail'; 
        }
        
        public fromJSON(data: any): SuperClassDetail[] { 
            return SuperClassDetail.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.sucdId === null || this.sucdId === undefined)
                return true;
            if (this.sucdId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: SuperClassDetailBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //sucdId property
        _sucdId: NpTypes.UIStringModel;
        public get sucdId():string {
            return this._sucdId.value;
        }
        public set sucdId(sucdId:string) {
            var self = this;
            self._sucdId.value = sucdId;
        //    self.sucdId_listeners.forEach(cb => { cb(<SuperClassDetail>self); });
        }
        //public sucdId_listeners: Array<(c:SuperClassDetail) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<SuperClassDetail>self); });
        }
        //public rowVersion_listeners: Array<(c:SuperClassDetail) => void> = [];
        //sucaId property
        _sucaId: NpTypes.UIManyToOneModel<Entities.SuperClas>;
        public get sucaId():SuperClas {
            return this._sucaId.value;
        }
        public set sucaId(sucaId:SuperClas) {
            var self = this;
            self._sucaId.value = sucaId;
        //    self.sucaId_listeners.forEach(cb => { cb(<SuperClassDetail>self); });
        }
        //public sucaId_listeners: Array<(c:SuperClassDetail) => void> = [];
        //cultId property
        _cultId: NpTypes.UIManyToOneModel<Entities.Cultivation>;
        public get cultId():Cultivation {
            return this._cultId.value;
        }
        public set cultId(cultId:Cultivation) {
            var self = this;
            self._cultId.value = cultId;
        //    self.cultId_listeners.forEach(cb => { cb(<SuperClassDetail>self); });
        }
        //public cultId_listeners: Array<(c:SuperClassDetail) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.sucdId_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.sucaId_listeners.splice(0);
            self.cultId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : SuperClassDetail {
            var key = "SuperClassDetail:"+x.sucdId;
            var ret =  new SuperClassDetail(
                x.sucdId,
                x.rowVersion,
                x.sucaId,
                x.cultId
            );
            deserializedEntities[key] = ret;
            ret.sucaId = (x.sucaId !== undefined && x.sucaId !== null) ? <SuperClas>NpTypes.BaseEntity.entitiesFactory[x.sucaId.$entityName].fromJSONPartial(x.sucaId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.cultId = (x.cultId !== undefined && x.cultId !== null) ? <Cultivation>NpTypes.BaseEntity.entitiesFactory[x.cultId.$entityName].fromJSONPartial(x.cultId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): SuperClassDetail {
            
            return <SuperClassDetail>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : SuperClassDetail {
            var self = this;
            var key="";
            var ret:SuperClassDetail = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "SuperClassDetail:"+x.$refId;
                ret = <SuperClassDetail>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "SuperClassDetailBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "SuperClassDetail:"+x.sucdId;
                    var cachedCopy = <SuperClassDetail>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = SuperClassDetail.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = SuperClassDetail.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = SuperClassDetail.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<SuperClassDetail> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<SuperClassDetail> = _.map(data, (x:any):SuperClassDetail => {
                return SuperClassDetail.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:SuperClassDetail[])=>void)  {
            var url = "/Niva/rest/SuperClassDetail/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = SuperClassDetail.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.sucdId = this.sucdId;
                ret.rowVersion = this.rowVersion;
                ret.sucaId = 
                    (this.sucaId !== undefined && this.sucaId !== null) ? 
                        { sucaId :  this.sucaId.sucaId} :
                        (this.sucaId === undefined ? undefined : null);
                ret.cultId = 
                    (this.cultId !== undefined && this.cultId !== null) ? 
                        { cultId :  this.cultId.cultId} :
                        (this.cultId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:SuperClassDetail):void {
            var self = this;
            self.sucdId = other.sucdId
            self.rowVersion = other.rowVersion
            self.sucaId = other.sucaId
            self.cultId = other.cultId
        }

        public static Create() : SuperClassDetail {
            return new SuperClassDetail(
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(sucdId:string) : SuperClassDetail {
            var ret = SuperClassDetail.Create();
            ret.sucdId = sucdId;
            return ret;
        }

        public static cashedEntities: { [id: string]: SuperClassDetail; } = {};
        public static findById_unecrypted(sucdId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : SuperClassDetail {
            if (Entities.SuperClassDetail.cashedEntities[sucdId.toString()] !== undefined)
                return Entities.SuperClassDetail.cashedEntities[sucdId.toString()];

            var wsPath = "SuperClassDetail/findBySucdId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['sucdId'] = sucdId;
            var ret = SuperClassDetail.Create();
            Entities.SuperClassDetail.cashedEntities[sucdId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.SuperClassDetail.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + sucdId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['SuperClassDetail'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.SuperClassDetail.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.SuperClassDetail.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
