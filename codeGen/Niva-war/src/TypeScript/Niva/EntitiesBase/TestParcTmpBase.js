/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/TestParcTmp.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var TestParcTmpBase = (function (_super) {
        __extends(TestParcTmpBase, _super);
        function TestParcTmpBase(parcId, cultDesc, cultId, geom) {
            _super.call(this);
            var self = this;
            this._parcId = new NpTypes.UIStringModel(parcId, this);
            this._cultDesc = new NpTypes.UIStringModel(cultDesc, this);
            this._cultId = new NpTypes.UINumberModel(cultId, this);
            this._geom = new NpTypes.UIGeoModel(geom, this);
            self.postConstruct();
        }
        TestParcTmpBase.prototype.getKey = function () {
            return this.parcId;
        };
        TestParcTmpBase.prototype.getKeyName = function () {
            return "parcId";
        };
        TestParcTmpBase.prototype.getEntityName = function () {
            return 'TestParcTmp';
        };
        TestParcTmpBase.prototype.fromJSON = function (data) {
            return Entities.TestParcTmp.fromJSONComplete(data);
        };
        Object.defineProperty(TestParcTmpBase.prototype, "parcId", {
            get: function () {
                return this._parcId.value;
            },
            set: function (parcId) {
                var self = this;
                self._parcId.value = parcId;
                //    self.parcId_listeners.forEach(cb => { cb(<TestParcTmp>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TestParcTmpBase.prototype, "cultDesc", {
            get: function () {
                return this._cultDesc.value;
            },
            set: function (cultDesc) {
                var self = this;
                self._cultDesc.value = cultDesc;
                //    self.cultDesc_listeners.forEach(cb => { cb(<TestParcTmp>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TestParcTmpBase.prototype, "cultId", {
            get: function () {
                return this._cultId.value;
            },
            set: function (cultId) {
                var self = this;
                self._cultId.value = cultId;
                //    self.cultId_listeners.forEach(cb => { cb(<TestParcTmp>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(TestParcTmpBase.prototype, "geom", {
            get: function () {
                return this._geom.value;
            },
            set: function (geom) {
                var self = this;
                self._geom.value = geom;
                //    self.geom_listeners.forEach(cb => { cb(<TestParcTmp>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public geom_listeners: Array<(c:TestParcTmp) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.parcId_listeners.splice(0);
            self.cultDesc_listeners.splice(0);
            self.cultId_listeners.splice(0);
            self.geom_listeners.splice(0);
        }
        */
        TestParcTmpBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "TestParcTmp:" + x.parcId;
            var ret = new Entities.TestParcTmp(x.parcId, x.cultDesc, x.cultId, x.geom);
            deserializedEntities[key] = ret;
            return ret;
        };
        TestParcTmpBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        TestParcTmpBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "TestParcTmp:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "TestParcTmpBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "TestParcTmp:"+x.parcId;
                    var cachedCopy = <TestParcTmp>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = TestParcTmp.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = TestParcTmp.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.TestParcTmp.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        TestParcTmpBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.TestParcTmp.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        TestParcTmpBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/TestParcTmp/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.TestParcTmp.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        TestParcTmpBase.prototype.toJSON = function () {
            var ret = {};
            ret.parcId = this.parcId;
            ret.cultDesc = this.cultDesc;
            ret.cultId = this.cultId;
            ret.geom = this.geom;
            return ret;
        };
        TestParcTmpBase.prototype.updateInstance = function (other) {
            var self = this;
            self.parcId = other.parcId;
            self.cultDesc = other.cultDesc;
            self.cultId = other.cultId;
            self.geom = other.geom;
        };
        TestParcTmpBase.Create = function () {
            return new Entities.TestParcTmp(undefined, undefined, undefined, undefined);
        };
        TestParcTmpBase.CreateById = function (parcId) {
            var ret = Entities.TestParcTmp.Create();
            ret.parcId = parcId;
            return ret;
        };
        TestParcTmpBase.findById_unecrypted = function (parcId, $scope, $http, errFunc) {
            if (Entities.TestParcTmp.cashedEntities[parcId.toString()] !== undefined)
                return Entities.TestParcTmp.cashedEntities[parcId.toString()];
            var wsPath = "TestParcTmp/findByParcId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['parcId'] = parcId;
            var ret = Entities.TestParcTmp.Create();
            Entities.TestParcTmp.cashedEntities[parcId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.TestParcTmp.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + parcId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        TestParcTmpBase.prototype.getRowVersion = function () {
            return 0;
        };
        TestParcTmpBase.cashedEntities = {};
        return TestParcTmpBase;
    })(NpTypes.BaseEntity);
    Entities.TestParcTmpBase = TestParcTmpBase;
    NpTypes.BaseEntity.entitiesFactory['TestParcTmp'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.TestParcTmp.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.TestParcTmp.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=TestParcTmpBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
