/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/CoverType.ts" />
/// <reference path="../Entities/Cultivation.ts" />
/// <reference path="../Entities/ExcelError.ts" />
/// <reference path="../Entities/ExcelFile.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var ExcelFileBase = (function (_super) {
        __extends(ExcelFileBase, _super);
        function ExcelFileBase(id, filename, data, totalRows, totalErrorRows, rowVersion) {
            _super.call(this);
            var self = this;
            this._id = new NpTypes.UIStringModel(id, this);
            this._filename = new NpTypes.UIStringModel(filename, this);
            this._data = new NpTypes.UIBlobModel(data, function () { return '...'; }, function (fileName) { }, this);
            this._totalRows = new NpTypes.UINumberModel(totalRows, this);
            this._totalErrorRows = new NpTypes.UINumberModel(totalErrorRows, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            self.postConstruct();
        }
        ExcelFileBase.prototype.getKey = function () {
            return this.id;
        };
        ExcelFileBase.prototype.getKeyName = function () {
            return "id";
        };
        ExcelFileBase.prototype.getEntityName = function () {
            return 'ExcelFile';
        };
        ExcelFileBase.prototype.fromJSON = function (data) {
            return Entities.ExcelFile.fromJSONComplete(data);
        };
        Object.defineProperty(ExcelFileBase.prototype, "id", {
            get: function () {
                return this._id.value;
            },
            set: function (id) {
                var self = this;
                self._id.value = id;
                //    self.id_listeners.forEach(cb => { cb(<ExcelFile>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ExcelFileBase.prototype, "filename", {
            get: function () {
                return this._filename.value;
            },
            set: function (filename) {
                var self = this;
                self._filename.value = filename;
                //    self.filename_listeners.forEach(cb => { cb(<ExcelFile>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ExcelFileBase.prototype, "data", {
            get: function () {
                return this._data.value;
            },
            set: function (data) {
                var self = this;
                self._data.value = data;
                //    self.data_listeners.forEach(cb => { cb(<ExcelFile>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ExcelFileBase.prototype, "totalRows", {
            get: function () {
                return this._totalRows.value;
            },
            set: function (totalRows) {
                var self = this;
                self._totalRows.value = totalRows;
                //    self.totalRows_listeners.forEach(cb => { cb(<ExcelFile>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ExcelFileBase.prototype, "totalErrorRows", {
            get: function () {
                return this._totalErrorRows.value;
            },
            set: function (totalErrorRows) {
                var self = this;
                self._totalErrorRows.value = totalErrorRows;
                //    self.totalErrorRows_listeners.forEach(cb => { cb(<ExcelFile>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ExcelFileBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<ExcelFile>self); });
            },
            enumerable: true,
            configurable: true
        });
        ExcelFileBase.prototype.coverTypeCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.ExcelFile.coverTypeCollection)) {
                if (self instanceof Entities.ExcelFile) {
                    Entities.ExcelFile.coverTypeCollection(self, func);
                }
            }
        };
        ExcelFileBase.prototype.cultivationCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.ExcelFile.cultivationCollection)) {
                if (self instanceof Entities.ExcelFile) {
                    Entities.ExcelFile.cultivationCollection(self, func);
                }
            }
        };
        ExcelFileBase.prototype.excelErrorCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.ExcelFile.excelErrorCollection)) {
                if (self instanceof Entities.ExcelFile) {
                    Entities.ExcelFile.excelErrorCollection(self, func);
                }
            }
        };
        /*
        public removeAllListeners() {
            var self = this;
            self.id_listeners.splice(0);
            self.filename_listeners.splice(0);
            self.data_listeners.splice(0);
            self.totalRows_listeners.splice(0);
            self.totalErrorRows_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
        }
        */
        ExcelFileBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "ExcelFile:" + x.id;
            var ret = new Entities.ExcelFile(x.id, x.filename, x.data, x.totalRows, x.totalErrorRows, x.rowVersion);
            deserializedEntities[key] = ret;
            return ret;
        };
        ExcelFileBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        ExcelFileBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "ExcelFile:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "ExcelFileBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "ExcelFile:"+x.id;
                    var cachedCopy = <ExcelFile>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = ExcelFile.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = ExcelFile.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.ExcelFile.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        ExcelFileBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.ExcelFile.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        ExcelFileBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/ExcelFile/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.ExcelFile.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        ExcelFileBase.prototype.toJSON = function () {
            var ret = {};
            ret.id = this.id;
            ret.filename = this.filename;
            ret.data = this.data;
            ret.totalRows = this.totalRows;
            ret.totalErrorRows = this.totalErrorRows;
            ret.rowVersion = this.rowVersion;
            return ret;
        };
        ExcelFileBase.prototype.updateInstance = function (other) {
            var self = this;
            self.id = other.id;
            self.filename = other.filename;
            self.data = other.data;
            self.totalRows = other.totalRows;
            self.totalErrorRows = other.totalErrorRows;
            self.rowVersion = other.rowVersion;
        };
        ExcelFileBase.Create = function () {
            return new Entities.ExcelFile(undefined, undefined, undefined, undefined, undefined, undefined);
        };
        ExcelFileBase.CreateById = function (id) {
            var ret = Entities.ExcelFile.Create();
            ret.id = id;
            return ret;
        };
        ExcelFileBase.findById_unecrypted = function (id, $scope, $http, errFunc) {
            if (Entities.ExcelFile.cashedEntities[id.toString()] !== undefined)
                return Entities.ExcelFile.cashedEntities[id.toString()];
            var wsPath = "ExcelFile/findById_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['id'] = id;
            var ret = Entities.ExcelFile.Create();
            Entities.ExcelFile.cashedEntities[id.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.ExcelFile.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + id + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        ExcelFileBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        //public rowVersion_listeners: Array<(c:ExcelFile) => void> = [];
        ExcelFileBase.coverTypeCollection = null; //(excelFile, list) => { };
        ExcelFileBase.cultivationCollection = null; //(excelFile, list) => { };
        ExcelFileBase.excelErrorCollection = null; //(excelFile, list) => { };
        ExcelFileBase.cashedEntities = {};
        return ExcelFileBase;
    })(NpTypes.BaseEntity);
    Entities.ExcelFileBase = ExcelFileBase;
    NpTypes.BaseEntity.entitiesFactory['ExcelFile'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.ExcelFile.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.ExcelFile.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=ExcelFileBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
