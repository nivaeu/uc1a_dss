/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Dbmanagementlog.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var DbmanagementlogBase = (function (_super) {
        __extends(DbmanagementlogBase, _super);
        function DbmanagementlogBase(dblid, dteDone, version, notes, type) {
            _super.call(this);
            var self = this;
            this._dblid = new NpTypes.UIStringModel(dblid, this);
            this._dteDone = new NpTypes.UIDateModel(dteDone, this);
            this._version = new NpTypes.UIStringModel(version, this);
            this._notes = new NpTypes.UIStringModel(notes, this);
            this._type = new NpTypes.UINumberModel(type, this);
            self.postConstruct();
        }
        DbmanagementlogBase.prototype.getKey = function () {
            return this.dblid;
        };
        DbmanagementlogBase.prototype.getKeyName = function () {
            return "dblid";
        };
        DbmanagementlogBase.prototype.getEntityName = function () {
            return 'Dbmanagementlog';
        };
        DbmanagementlogBase.prototype.fromJSON = function (data) {
            return Entities.Dbmanagementlog.fromJSONComplete(data);
        };
        Object.defineProperty(DbmanagementlogBase.prototype, "dblid", {
            get: function () {
                return this._dblid.value;
            },
            set: function (dblid) {
                var self = this;
                self._dblid.value = dblid;
                //    self.dblid_listeners.forEach(cb => { cb(<Dbmanagementlog>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DbmanagementlogBase.prototype, "dteDone", {
            get: function () {
                return this._dteDone.value;
            },
            set: function (dteDone) {
                var self = this;
                self._dteDone.value = dteDone;
                //    self.dteDone_listeners.forEach(cb => { cb(<Dbmanagementlog>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DbmanagementlogBase.prototype, "version", {
            get: function () {
                return this._version.value;
            },
            set: function (version) {
                var self = this;
                self._version.value = version;
                //    self.version_listeners.forEach(cb => { cb(<Dbmanagementlog>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DbmanagementlogBase.prototype, "notes", {
            get: function () {
                return this._notes.value;
            },
            set: function (notes) {
                var self = this;
                self._notes.value = notes;
                //    self.notes_listeners.forEach(cb => { cb(<Dbmanagementlog>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DbmanagementlogBase.prototype, "type", {
            get: function () {
                return this._type.value;
            },
            set: function (type) {
                var self = this;
                self._type.value = type;
                //    self.type_listeners.forEach(cb => { cb(<Dbmanagementlog>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public type_listeners: Array<(c:Dbmanagementlog) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.dblid_listeners.splice(0);
            self.dteDone_listeners.splice(0);
            self.version_listeners.splice(0);
            self.notes_listeners.splice(0);
            self.type_listeners.splice(0);
        }
        */
        DbmanagementlogBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "Dbmanagementlog:" + x.dblid;
            var ret = new Entities.Dbmanagementlog(x.dblid, isVoid(x.dteDone) ? null : new Date(x.dteDone), x.version, x.notes, x.type);
            deserializedEntities[key] = ret;
            return ret;
        };
        DbmanagementlogBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        DbmanagementlogBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "Dbmanagementlog:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "DbmanagementlogBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "Dbmanagementlog:"+x.dblid;
                    var cachedCopy = <Dbmanagementlog>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = Dbmanagementlog.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = Dbmanagementlog.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.Dbmanagementlog.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        DbmanagementlogBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.Dbmanagementlog.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        DbmanagementlogBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/Dbmanagementlog/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.Dbmanagementlog.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        DbmanagementlogBase.prototype.toJSON = function () {
            var ret = {};
            ret.dblid = this.dblid;
            ret.dteDone = this.dteDone;
            ret.version = this.version;
            ret.notes = this.notes;
            ret.type = this.type;
            return ret;
        };
        DbmanagementlogBase.prototype.updateInstance = function (other) {
            var self = this;
            self.dblid = other.dblid;
            self.dteDone = other.dteDone;
            self.version = other.version;
            self.notes = other.notes;
            self.type = other.type;
        };
        DbmanagementlogBase.Create = function () {
            return new Entities.Dbmanagementlog(undefined, undefined, undefined, undefined, undefined);
        };
        DbmanagementlogBase.CreateById = function (dblid) {
            var ret = Entities.Dbmanagementlog.Create();
            ret.dblid = dblid;
            return ret;
        };
        DbmanagementlogBase.findById_unecrypted = function (dblid, $scope, $http, errFunc) {
            if (Entities.Dbmanagementlog.cashedEntities[dblid.toString()] !== undefined)
                return Entities.Dbmanagementlog.cashedEntities[dblid.toString()];
            var wsPath = "Dbmanagementlog/findByDblid_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['dblid'] = dblid;
            var ret = Entities.Dbmanagementlog.Create();
            Entities.Dbmanagementlog.cashedEntities[dblid.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.Dbmanagementlog.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + dblid + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        DbmanagementlogBase.prototype.getRowVersion = function () {
            return 0;
        };
        DbmanagementlogBase.cashedEntities = {};
        return DbmanagementlogBase;
    })(NpTypes.BaseEntity);
    Entities.DbmanagementlogBase = DbmanagementlogBase;
    NpTypes.BaseEntity.entitiesFactory['Dbmanagementlog'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.Dbmanagementlog.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.Dbmanagementlog.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=DbmanagementlogBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
