/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Classification.ts" />
/// <reference path="../Entities/Statistic.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var StatisticBase = (function (_super) {
        __extends(StatisticBase, _super);
        function StatisticBase(statId, name, description, value, rowVersion, clasId) {
            _super.call(this);
            var self = this;
            this._statId = new NpTypes.UIStringModel(statId, this);
            this._name = new NpTypes.UIStringModel(name, this);
            this._description = new NpTypes.UIStringModel(description, this);
            this._value = new NpTypes.UINumberModel(value, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._clasId = new NpTypes.UIManyToOneModel(clasId, this);
            self.postConstruct();
        }
        StatisticBase.prototype.getKey = function () {
            return this.statId;
        };
        StatisticBase.prototype.getKeyName = function () {
            return "statId";
        };
        StatisticBase.prototype.getEntityName = function () {
            return 'Statistic';
        };
        StatisticBase.prototype.fromJSON = function (data) {
            return Entities.Statistic.fromJSONComplete(data);
        };
        Object.defineProperty(StatisticBase.prototype, "statId", {
            get: function () {
                return this._statId.value;
            },
            set: function (statId) {
                var self = this;
                self._statId.value = statId;
                //    self.statId_listeners.forEach(cb => { cb(<Statistic>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StatisticBase.prototype, "name", {
            get: function () {
                return this._name.value;
            },
            set: function (name) {
                var self = this;
                self._name.value = name;
                //    self.name_listeners.forEach(cb => { cb(<Statistic>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StatisticBase.prototype, "description", {
            get: function () {
                return this._description.value;
            },
            set: function (description) {
                var self = this;
                self._description.value = description;
                //    self.description_listeners.forEach(cb => { cb(<Statistic>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StatisticBase.prototype, "value", {
            get: function () {
                return this._value.value;
            },
            set: function (value) {
                var self = this;
                self._value.value = value;
                //    self.value_listeners.forEach(cb => { cb(<Statistic>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StatisticBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<Statistic>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(StatisticBase.prototype, "clasId", {
            get: function () {
                return this._clasId.value;
            },
            set: function (clasId) {
                var self = this;
                self._clasId.value = clasId;
                //    self.clasId_listeners.forEach(cb => { cb(<Statistic>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public clasId_listeners: Array<(c:Statistic) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.statId_listeners.splice(0);
            self.name_listeners.splice(0);
            self.description_listeners.splice(0);
            self.value_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.clasId_listeners.splice(0);
        }
        */
        StatisticBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "Statistic:" + x.statId;
            var ret = new Entities.Statistic(x.statId, x.name, x.description, x.value, x.rowVersion, x.clasId);
            deserializedEntities[key] = ret;
            ret.clasId = (x.clasId !== undefined && x.clasId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.clasId.$entityName].fromJSONPartial(x.clasId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        StatisticBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        StatisticBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "Statistic:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "StatisticBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "Statistic:"+x.statId;
                    var cachedCopy = <Statistic>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = Statistic.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = Statistic.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.Statistic.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        StatisticBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.Statistic.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        StatisticBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/Statistic/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.Statistic.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        StatisticBase.prototype.toJSON = function () {
            var ret = {};
            ret.statId = this.statId;
            ret.name = this.name;
            ret.description = this.description;
            ret.value = this.value;
            ret.rowVersion = this.rowVersion;
            ret.clasId =
                (this.clasId !== undefined && this.clasId !== null) ?
                    { clasId: this.clasId.clasId } :
                    (this.clasId === undefined ? undefined : null);
            return ret;
        };
        StatisticBase.prototype.updateInstance = function (other) {
            var self = this;
            self.statId = other.statId;
            self.name = other.name;
            self.description = other.description;
            self.value = other.value;
            self.rowVersion = other.rowVersion;
            self.clasId = other.clasId;
        };
        StatisticBase.Create = function () {
            return new Entities.Statistic(undefined, undefined, undefined, undefined, undefined, undefined);
        };
        StatisticBase.CreateById = function (statId) {
            var ret = Entities.Statistic.Create();
            ret.statId = statId;
            return ret;
        };
        StatisticBase.findById_unecrypted = function (statId, $scope, $http, errFunc) {
            if (Entities.Statistic.cashedEntities[statId.toString()] !== undefined)
                return Entities.Statistic.cashedEntities[statId.toString()];
            var wsPath = "Statistic/findByStatId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['statId'] = statId;
            var ret = Entities.Statistic.Create();
            Entities.Statistic.cashedEntities[statId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.Statistic.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + statId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        StatisticBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        StatisticBase.cashedEntities = {};
        return StatisticBase;
    })(NpTypes.BaseEntity);
    Entities.StatisticBase = StatisticBase;
    NpTypes.BaseEntity.entitiesFactory['Statistic'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.Statistic.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.Statistic.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=StatisticBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
