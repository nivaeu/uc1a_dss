/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Integrateddecision.ts" />
/// <reference path="../Entities/EcGroupCopy.ts" />
/// <reference path="../Entities/ParcelDecision.ts" />
/// <reference path="../Entities/Classification.ts" />
/// <reference path="../Entities/EcGroup.ts" />
/// <reference path="../Entities/DecisionMaking.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var DecisionMakingBase = (function (_super) {
        __extends(DecisionMakingBase, _super);
        function DecisionMakingBase(demaId, description, dateTime, rowVersion, recordtype, hasBeenRun, hasPopulatedIntegratedDecisionsAndIssues, clasId, ecgrId) {
            _super.call(this);
            var self = this;
            this._demaId = new NpTypes.UIStringModel(demaId, this);
            this._description = new NpTypes.UIStringModel(description, this);
            this._dateTime = new NpTypes.UIDateModel(dateTime, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._recordtype = new NpTypes.UINumberModel(recordtype, this);
            this._hasBeenRun = new NpTypes.UIBoolModel(hasBeenRun, this);
            this._hasPopulatedIntegratedDecisionsAndIssues = new NpTypes.UIBoolModel(hasPopulatedIntegratedDecisionsAndIssues, this);
            this._clasId = new NpTypes.UIManyToOneModel(clasId, this);
            this._ecgrId = new NpTypes.UIManyToOneModel(ecgrId, this);
            self.postConstruct();
        }
        DecisionMakingBase.prototype.getKey = function () {
            return this.demaId;
        };
        DecisionMakingBase.prototype.getKeyName = function () {
            return "demaId";
        };
        DecisionMakingBase.prototype.getEntityName = function () {
            return 'DecisionMaking';
        };
        DecisionMakingBase.prototype.fromJSON = function (data) {
            return Entities.DecisionMaking.fromJSONComplete(data);
        };
        Object.defineProperty(DecisionMakingBase.prototype, "demaId", {
            get: function () {
                return this._demaId.value;
            },
            set: function (demaId) {
                var self = this;
                self._demaId.value = demaId;
                //    self.demaId_listeners.forEach(cb => { cb(<DecisionMaking>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DecisionMakingBase.prototype, "description", {
            get: function () {
                return this._description.value;
            },
            set: function (description) {
                var self = this;
                self._description.value = description;
                //    self.description_listeners.forEach(cb => { cb(<DecisionMaking>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DecisionMakingBase.prototype, "dateTime", {
            get: function () {
                return this._dateTime.value;
            },
            set: function (dateTime) {
                var self = this;
                self._dateTime.value = dateTime;
                //    self.dateTime_listeners.forEach(cb => { cb(<DecisionMaking>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DecisionMakingBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<DecisionMaking>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DecisionMakingBase.prototype, "recordtype", {
            get: function () {
                return this._recordtype.value;
            },
            set: function (recordtype) {
                var self = this;
                self._recordtype.value = recordtype;
                //    self.recordtype_listeners.forEach(cb => { cb(<DecisionMaking>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DecisionMakingBase.prototype, "hasBeenRun", {
            get: function () {
                return this._hasBeenRun.value;
            },
            set: function (hasBeenRun) {
                var self = this;
                self._hasBeenRun.value = hasBeenRun;
                //    self.hasBeenRun_listeners.forEach(cb => { cb(<DecisionMaking>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DecisionMakingBase.prototype, "hasPopulatedIntegratedDecisionsAndIssues", {
            get: function () {
                return this._hasPopulatedIntegratedDecisionsAndIssues.value;
            },
            set: function (hasPopulatedIntegratedDecisionsAndIssues) {
                var self = this;
                self._hasPopulatedIntegratedDecisionsAndIssues.value = hasPopulatedIntegratedDecisionsAndIssues;
                //    self.hasPopulatedIntegratedDecisionsAndIssues_listeners.forEach(cb => { cb(<DecisionMaking>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DecisionMakingBase.prototype, "clasId", {
            get: function () {
                return this._clasId.value;
            },
            set: function (clasId) {
                var self = this;
                self._clasId.value = clasId;
                //    self.clasId_listeners.forEach(cb => { cb(<DecisionMaking>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DecisionMakingBase.prototype, "ecgrId", {
            get: function () {
                return this._ecgrId.value;
            },
            set: function (ecgrId) {
                var self = this;
                self._ecgrId.value = ecgrId;
                //    self.ecgrId_listeners.forEach(cb => { cb(<DecisionMaking>self); });
            },
            enumerable: true,
            configurable: true
        });
        DecisionMakingBase.prototype.integrateddecisionCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.DecisionMaking.integrateddecisionCollection)) {
                if (self instanceof Entities.DecisionMaking) {
                    Entities.DecisionMaking.integrateddecisionCollection(self, func);
                }
            }
        };
        DecisionMakingBase.prototype.ecGroupCopyCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.DecisionMaking.ecGroupCopyCollection)) {
                if (self instanceof Entities.DecisionMaking) {
                    Entities.DecisionMaking.ecGroupCopyCollection(self, func);
                }
            }
        };
        DecisionMakingBase.prototype.parcelDecisionCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.DecisionMaking.parcelDecisionCollection)) {
                if (self instanceof Entities.DecisionMaking) {
                    Entities.DecisionMaking.parcelDecisionCollection(self, func);
                }
            }
        };
        /*
        public removeAllListeners() {
            var self = this;
            self.demaId_listeners.splice(0);
            self.description_listeners.splice(0);
            self.dateTime_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.recordtype_listeners.splice(0);
            self.hasBeenRun_listeners.splice(0);
            self.hasPopulatedIntegratedDecisionsAndIssues_listeners.splice(0);
            self.clasId_listeners.splice(0);
            self.ecgrId_listeners.splice(0);
        }
        */
        DecisionMakingBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "DecisionMaking:" + x.demaId;
            var ret = new Entities.DecisionMaking(x.demaId, x.description, isVoid(x.dateTime) ? null : new Date(x.dateTime), x.rowVersion, x.recordtype, x.hasBeenRun, x.hasPopulatedIntegratedDecisionsAndIssues, x.clasId, x.ecgrId);
            deserializedEntities[key] = ret;
            ret.clasId = (x.clasId !== undefined && x.clasId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.clasId.$entityName].fromJSONPartial(x.clasId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.ecgrId = (x.ecgrId !== undefined && x.ecgrId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.ecgrId.$entityName].fromJSONPartial(x.ecgrId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        DecisionMakingBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        DecisionMakingBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "DecisionMaking:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "DecisionMakingBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "DecisionMaking:"+x.demaId;
                    var cachedCopy = <DecisionMaking>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = DecisionMaking.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = DecisionMaking.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.DecisionMaking.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        DecisionMakingBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.DecisionMaking.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        DecisionMakingBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/DecisionMaking/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.DecisionMaking.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        DecisionMakingBase.prototype.toJSON = function () {
            var ret = {};
            ret.demaId = this.demaId;
            ret.description = this.description;
            ret.dateTime = this.dateTime;
            ret.rowVersion = this.rowVersion;
            ret.recordtype = this.recordtype;
            ret.hasBeenRun = this.hasBeenRun;
            ret.hasPopulatedIntegratedDecisionsAndIssues = this.hasPopulatedIntegratedDecisionsAndIssues;
            ret.clasId =
                (this.clasId !== undefined && this.clasId !== null) ?
                    { clasId: this.clasId.clasId } :
                    (this.clasId === undefined ? undefined : null);
            ret.ecgrId =
                (this.ecgrId !== undefined && this.ecgrId !== null) ?
                    { ecgrId: this.ecgrId.ecgrId } :
                    (this.ecgrId === undefined ? undefined : null);
            return ret;
        };
        DecisionMakingBase.prototype.updateInstance = function (other) {
            var self = this;
            self.demaId = other.demaId;
            self.description = other.description;
            self.dateTime = other.dateTime;
            self.rowVersion = other.rowVersion;
            self.recordtype = other.recordtype;
            self.hasBeenRun = other.hasBeenRun;
            self.hasPopulatedIntegratedDecisionsAndIssues = other.hasPopulatedIntegratedDecisionsAndIssues;
            self.clasId = other.clasId;
            self.ecgrId = other.ecgrId;
        };
        DecisionMakingBase.Create = function () {
            return new Entities.DecisionMaking(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined);
        };
        DecisionMakingBase.CreateById = function (demaId) {
            var ret = Entities.DecisionMaking.Create();
            ret.demaId = demaId;
            return ret;
        };
        DecisionMakingBase.findById_unecrypted = function (demaId, $scope, $http, errFunc) {
            if (Entities.DecisionMaking.cashedEntities[demaId.toString()] !== undefined)
                return Entities.DecisionMaking.cashedEntities[demaId.toString()];
            var wsPath = "DecisionMaking/findByDemaId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['demaId'] = demaId;
            var ret = Entities.DecisionMaking.Create();
            Entities.DecisionMaking.cashedEntities[demaId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.DecisionMaking.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + demaId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        DecisionMakingBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        //public ecgrId_listeners: Array<(c:DecisionMaking) => void> = [];
        DecisionMakingBase.integrateddecisionCollection = null; //(decisionMaking, list) => { };
        DecisionMakingBase.ecGroupCopyCollection = null; //(decisionMaking, list) => { };
        DecisionMakingBase.parcelDecisionCollection = null; //(decisionMaking, list) => { };
        DecisionMakingBase.cashedEntities = {};
        return DecisionMakingBase;
    })(NpTypes.BaseEntity);
    Entities.DecisionMakingBase = DecisionMakingBase;
    NpTypes.BaseEntity.entitiesFactory['DecisionMaking'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.DecisionMaking.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.DecisionMaking.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=DecisionMakingBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
