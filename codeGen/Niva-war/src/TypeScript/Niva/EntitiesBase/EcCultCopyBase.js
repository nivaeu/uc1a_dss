/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/EcCultDetailCopy.ts" />
/// <reference path="../Entities/Cultivation.ts" />
/// <reference path="../Entities/EcGroupCopy.ts" />
/// <reference path="../Entities/CoverType.ts" />
/// <reference path="../Entities/SuperClas.ts" />
/// <reference path="../Entities/EcCultCopy.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var EcCultCopyBase = (function (_super) {
        __extends(EcCultCopyBase, _super);
        function EcCultCopyBase(ecccId, noneMatchDecision, rowVersion, cultId, ecgcId, cotyId, sucaId) {
            _super.call(this);
            var self = this;
            this._ecccId = new NpTypes.UIStringModel(ecccId, this);
            this._noneMatchDecision = new NpTypes.UINumberModel(noneMatchDecision, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._cultId = new NpTypes.UIManyToOneModel(cultId, this);
            this._ecgcId = new NpTypes.UIManyToOneModel(ecgcId, this);
            this._cotyId = new NpTypes.UIManyToOneModel(cotyId, this);
            this._sucaId = new NpTypes.UIManyToOneModel(sucaId, this);
            self.postConstruct();
        }
        EcCultCopyBase.prototype.getKey = function () {
            return this.ecccId;
        };
        EcCultCopyBase.prototype.getKeyName = function () {
            return "ecccId";
        };
        EcCultCopyBase.prototype.getEntityName = function () {
            return 'EcCultCopy';
        };
        EcCultCopyBase.prototype.fromJSON = function (data) {
            return Entities.EcCultCopy.fromJSONComplete(data);
        };
        Object.defineProperty(EcCultCopyBase.prototype, "ecccId", {
            get: function () {
                return this._ecccId.value;
            },
            set: function (ecccId) {
                var self = this;
                self._ecccId.value = ecccId;
                //    self.ecccId_listeners.forEach(cb => { cb(<EcCultCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultCopyBase.prototype, "noneMatchDecision", {
            get: function () {
                return this._noneMatchDecision.value;
            },
            set: function (noneMatchDecision) {
                var self = this;
                self._noneMatchDecision.value = noneMatchDecision;
                //    self.noneMatchDecision_listeners.forEach(cb => { cb(<EcCultCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultCopyBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<EcCultCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultCopyBase.prototype, "cultId", {
            get: function () {
                return this._cultId.value;
            },
            set: function (cultId) {
                var self = this;
                self._cultId.value = cultId;
                //    self.cultId_listeners.forEach(cb => { cb(<EcCultCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultCopyBase.prototype, "ecgcId", {
            get: function () {
                return this._ecgcId.value;
            },
            set: function (ecgcId) {
                var self = this;
                self._ecgcId.value = ecgcId;
                //    self.ecgcId_listeners.forEach(cb => { cb(<EcCultCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultCopyBase.prototype, "cotyId", {
            get: function () {
                return this._cotyId.value;
            },
            set: function (cotyId) {
                var self = this;
                self._cotyId.value = cotyId;
                //    self.cotyId_listeners.forEach(cb => { cb(<EcCultCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcCultCopyBase.prototype, "sucaId", {
            get: function () {
                return this._sucaId.value;
            },
            set: function (sucaId) {
                var self = this;
                self._sucaId.value = sucaId;
                //    self.sucaId_listeners.forEach(cb => { cb(<EcCultCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        EcCultCopyBase.prototype.ecCultDetailCopyCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.EcCultCopy.ecCultDetailCopyCollection)) {
                if (self instanceof Entities.EcCultCopy) {
                    Entities.EcCultCopy.ecCultDetailCopyCollection(self, func);
                }
            }
        };
        /*
        public removeAllListeners() {
            var self = this;
            self.ecccId_listeners.splice(0);
            self.noneMatchDecision_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.cultId_listeners.splice(0);
            self.ecgcId_listeners.splice(0);
            self.cotyId_listeners.splice(0);
            self.sucaId_listeners.splice(0);
        }
        */
        EcCultCopyBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "EcCultCopy:" + x.ecccId;
            var ret = new Entities.EcCultCopy(x.ecccId, x.noneMatchDecision, x.rowVersion, x.cultId, x.ecgcId, x.cotyId, x.sucaId);
            deserializedEntities[key] = ret;
            ret.cultId = (x.cultId !== undefined && x.cultId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.cultId.$entityName].fromJSONPartial(x.cultId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.ecgcId = (x.ecgcId !== undefined && x.ecgcId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.ecgcId.$entityName].fromJSONPartial(x.ecgcId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.cotyId = (x.cotyId !== undefined && x.cotyId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.cotyId.$entityName].fromJSONPartial(x.cotyId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.sucaId = (x.sucaId !== undefined && x.sucaId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.sucaId.$entityName].fromJSONPartial(x.sucaId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        EcCultCopyBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        EcCultCopyBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "EcCultCopy:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "EcCultCopyBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "EcCultCopy:"+x.ecccId;
                    var cachedCopy = <EcCultCopy>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = EcCultCopy.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = EcCultCopy.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.EcCultCopy.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        EcCultCopyBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.EcCultCopy.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        EcCultCopyBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/EcCultCopy/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.EcCultCopy.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        EcCultCopyBase.prototype.toJSON = function () {
            var ret = {};
            ret.ecccId = this.ecccId;
            ret.noneMatchDecision = this.noneMatchDecision;
            ret.rowVersion = this.rowVersion;
            ret.cultId =
                (this.cultId !== undefined && this.cultId !== null) ?
                    { cultId: this.cultId.cultId } :
                    (this.cultId === undefined ? undefined : null);
            ret.ecgcId =
                (this.ecgcId !== undefined && this.ecgcId !== null) ?
                    { ecgcId: this.ecgcId.ecgcId } :
                    (this.ecgcId === undefined ? undefined : null);
            ret.cotyId =
                (this.cotyId !== undefined && this.cotyId !== null) ?
                    { cotyId: this.cotyId.cotyId } :
                    (this.cotyId === undefined ? undefined : null);
            ret.sucaId =
                (this.sucaId !== undefined && this.sucaId !== null) ?
                    { sucaId: this.sucaId.sucaId } :
                    (this.sucaId === undefined ? undefined : null);
            return ret;
        };
        EcCultCopyBase.prototype.updateInstance = function (other) {
            var self = this;
            self.ecccId = other.ecccId;
            self.noneMatchDecision = other.noneMatchDecision;
            self.rowVersion = other.rowVersion;
            self.cultId = other.cultId;
            self.ecgcId = other.ecgcId;
            self.cotyId = other.cotyId;
            self.sucaId = other.sucaId;
        };
        EcCultCopyBase.Create = function () {
            return new Entities.EcCultCopy(undefined, undefined, undefined, undefined, undefined, undefined, undefined);
        };
        EcCultCopyBase.CreateById = function (ecccId) {
            var ret = Entities.EcCultCopy.Create();
            ret.ecccId = ecccId;
            return ret;
        };
        EcCultCopyBase.findById_unecrypted = function (ecccId, $scope, $http, errFunc) {
            if (Entities.EcCultCopy.cashedEntities[ecccId.toString()] !== undefined)
                return Entities.EcCultCopy.cashedEntities[ecccId.toString()];
            var wsPath = "EcCultCopy/findByEcccId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['ecccId'] = ecccId;
            var ret = Entities.EcCultCopy.Create();
            Entities.EcCultCopy.cashedEntities[ecccId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.EcCultCopy.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + ecccId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        EcCultCopyBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        //public sucaId_listeners: Array<(c:EcCultCopy) => void> = [];
        EcCultCopyBase.ecCultDetailCopyCollection = null; //(ecCultCopy, list) => { };
        EcCultCopyBase.cashedEntities = {};
        return EcCultCopyBase;
    })(NpTypes.BaseEntity);
    Entities.EcCultCopyBase = EcCultCopyBase;
    NpTypes.BaseEntity.entitiesFactory['EcCultCopy'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.EcCultCopy.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.EcCultCopy.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=EcCultCopyBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
