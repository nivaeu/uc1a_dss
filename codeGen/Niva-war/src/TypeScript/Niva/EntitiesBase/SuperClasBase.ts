/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/EcCult.ts" />
/// <reference path="../Entities/SuperClassDetail.ts" />
/// <reference path="../Entities/EcCultCopy.ts" />
/// <reference path="../Entities/SuperClas.ts" />

module Entities {

    export class SuperClasBase extends NpTypes.BaseEntity {
        constructor(
            sucaId: string,
            code: string,
            name: string,
            rowVersion: number) 
        {
            super();
            var self = this;
            this._sucaId = new NpTypes.UIStringModel(sucaId, this);
            this._code = new NpTypes.UIStringModel(code, this);
            this._name = new NpTypes.UIStringModel(name, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.sucaId; 
        }
        
        public getKeyName(): string { 
            return "sucaId"; 
        }

        public getEntityName(): string { 
            return 'SuperClas'; 
        }
        
        public fromJSON(data: any): SuperClas[] { 
            return SuperClas.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.sucaId === null || this.sucaId === undefined)
                return true;
            if (this.sucaId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: SuperClasBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //sucaId property
        _sucaId: NpTypes.UIStringModel;
        public get sucaId():string {
            return this._sucaId.value;
        }
        public set sucaId(sucaId:string) {
            var self = this;
            self._sucaId.value = sucaId;
        //    self.sucaId_listeners.forEach(cb => { cb(<SuperClas>self); });
        }
        //public sucaId_listeners: Array<(c:SuperClas) => void> = [];
        //code property
        _code: NpTypes.UIStringModel;
        public get code():string {
            return this._code.value;
        }
        public set code(code:string) {
            var self = this;
            self._code.value = code;
        //    self.code_listeners.forEach(cb => { cb(<SuperClas>self); });
        }
        //public code_listeners: Array<(c:SuperClas) => void> = [];
        //name property
        _name: NpTypes.UIStringModel;
        public get name():string {
            return this._name.value;
        }
        public set name(name:string) {
            var self = this;
            self._name.value = name;
        //    self.name_listeners.forEach(cb => { cb(<SuperClas>self); });
        }
        //public name_listeners: Array<(c:SuperClas) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<SuperClas>self); });
        }
        //public rowVersion_listeners: Array<(c:SuperClas) => void> = [];
        public static ecCultCollection: (self:Entities.SuperClas, func: (ecCultList: Entities.EcCult[]) => void) => void = null; //(superClas, list) => { };
        public ecCultCollection(func: (ecCultList: Entities.EcCult[]) => void) {
            var self: Entities.SuperClasBase = this;
        	if (!isVoid(SuperClas.ecCultCollection)) {
        		if (self instanceof Entities.SuperClas) {
        			SuperClas.ecCultCollection(<Entities.SuperClas>self, func);
                }
            }
        }
        public static superClassDetailCollection: (self:Entities.SuperClas, func: (superClassDetailList: Entities.SuperClassDetail[]) => void) => void = null; //(superClas, list) => { };
        public superClassDetailCollection(func: (superClassDetailList: Entities.SuperClassDetail[]) => void) {
            var self: Entities.SuperClasBase = this;
        	if (!isVoid(SuperClas.superClassDetailCollection)) {
        		if (self instanceof Entities.SuperClas) {
        			SuperClas.superClassDetailCollection(<Entities.SuperClas>self, func);
                }
            }
        }
        public static ecCultCopyCollection: (self:Entities.SuperClas, func: (ecCultCopyList: Entities.EcCultCopy[]) => void) => void = null; //(superClas, list) => { };
        public ecCultCopyCollection(func: (ecCultCopyList: Entities.EcCultCopy[]) => void) {
            var self: Entities.SuperClasBase = this;
        	if (!isVoid(SuperClas.ecCultCopyCollection)) {
        		if (self instanceof Entities.SuperClas) {
        			SuperClas.ecCultCopyCollection(<Entities.SuperClas>self, func);
                }
            }
        }
        /*
        public removeAllListeners() {
            var self = this;
            self.sucaId_listeners.splice(0);
            self.code_listeners.splice(0);
            self.name_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : SuperClas {
            var key = "SuperClas:"+x.sucaId;
            var ret =  new SuperClas(
                x.sucaId,
                x.code,
                x.name,
                x.rowVersion
            );
            deserializedEntities[key] = ret;
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): SuperClas {
            
            return <SuperClas>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : SuperClas {
            var self = this;
            var key="";
            var ret:SuperClas = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "SuperClas:"+x.$refId;
                ret = <SuperClas>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "SuperClasBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "SuperClas:"+x.sucaId;
                    var cachedCopy = <SuperClas>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = SuperClas.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = SuperClas.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = SuperClas.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<SuperClas> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<SuperClas> = _.map(data, (x:any):SuperClas => {
                return SuperClas.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:SuperClas[])=>void)  {
            var url = "/Niva/rest/SuperClas/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = SuperClas.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.sucaId = this.sucaId;
                ret.code = this.code;
                ret.name = this.name;
                ret.rowVersion = this.rowVersion;
            return ret;
        }

        public updateInstance(other:SuperClas):void {
            var self = this;
            self.sucaId = other.sucaId
            self.code = other.code
            self.name = other.name
            self.rowVersion = other.rowVersion
        }

        public static Create() : SuperClas {
            return new SuperClas(
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(sucaId:string) : SuperClas {
            var ret = SuperClas.Create();
            ret.sucaId = sucaId;
            return ret;
        }

        public static cashedEntities: { [id: string]: SuperClas; } = {};
        public static findById_unecrypted(sucaId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : SuperClas {
            if (Entities.SuperClas.cashedEntities[sucaId.toString()] !== undefined)
                return Entities.SuperClas.cashedEntities[sucaId.toString()];

            var wsPath = "SuperClas/findBySucaId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['sucaId'] = sucaId;
            var ret = SuperClas.Create();
            Entities.SuperClas.cashedEntities[sucaId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.SuperClas.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + sucaId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['SuperClas'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.SuperClas.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.SuperClas.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
