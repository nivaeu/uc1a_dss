/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/GpRequestsProducers.ts" />
/// <reference path="../Entities/AgrisnapUsersProducers.ts" />
/// <reference path="../Entities/Producers.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var ProducersBase = (function (_super) {
        __extends(ProducersBase, _super);
        function ProducersBase(prodName, rowVersion, prodCode, producersId) {
            _super.call(this);
            var self = this;
            this._prodName = new NpTypes.UIStringModel(prodName, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._prodCode = new NpTypes.UINumberModel(prodCode, this);
            this._producersId = new NpTypes.UIStringModel(producersId, this);
            self.postConstruct();
        }
        ProducersBase.prototype.getKey = function () {
            return this.producersId;
        };
        ProducersBase.prototype.getKeyName = function () {
            return "producersId";
        };
        ProducersBase.prototype.getEntityName = function () {
            return 'Producers';
        };
        ProducersBase.prototype.fromJSON = function (data) {
            return Entities.Producers.fromJSONComplete(data);
        };
        Object.defineProperty(ProducersBase.prototype, "prodName", {
            get: function () {
                return this._prodName.value;
            },
            set: function (prodName) {
                var self = this;
                self._prodName.value = prodName;
                //    self.prodName_listeners.forEach(cb => { cb(<Producers>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ProducersBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<Producers>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ProducersBase.prototype, "prodCode", {
            get: function () {
                return this._prodCode.value;
            },
            set: function (prodCode) {
                var self = this;
                self._prodCode.value = prodCode;
                //    self.prodCode_listeners.forEach(cb => { cb(<Producers>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ProducersBase.prototype, "producersId", {
            get: function () {
                return this._producersId.value;
            },
            set: function (producersId) {
                var self = this;
                self._producersId.value = producersId;
                //    self.producersId_listeners.forEach(cb => { cb(<Producers>self); });
            },
            enumerable: true,
            configurable: true
        });
        ProducersBase.prototype.gpRequestsProducersCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.Producers.gpRequestsProducersCollection)) {
                if (self instanceof Entities.Producers) {
                    Entities.Producers.gpRequestsProducersCollection(self, func);
                }
            }
        };
        ProducersBase.prototype.agrisnapUsersProducersCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.Producers.agrisnapUsersProducersCollection)) {
                if (self instanceof Entities.Producers) {
                    Entities.Producers.agrisnapUsersProducersCollection(self, func);
                }
            }
        };
        /*
        public removeAllListeners() {
            var self = this;
            self.prodName_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.prodCode_listeners.splice(0);
            self.producersId_listeners.splice(0);
        }
        */
        ProducersBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "Producers:" + x.producersId;
            var ret = new Entities.Producers(x.prodName, x.rowVersion, x.prodCode, x.producersId);
            deserializedEntities[key] = ret;
            return ret;
        };
        ProducersBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        ProducersBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "Producers:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "ProducersBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "Producers:"+x.producersId;
                    var cachedCopy = <Producers>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = Producers.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = Producers.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.Producers.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        ProducersBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.Producers.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        ProducersBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/Producers/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.Producers.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        ProducersBase.prototype.toJSON = function () {
            var ret = {};
            ret.prodName = this.prodName;
            ret.rowVersion = this.rowVersion;
            ret.prodCode = this.prodCode;
            ret.producersId = this.producersId;
            return ret;
        };
        ProducersBase.prototype.updateInstance = function (other) {
            var self = this;
            self.prodName = other.prodName;
            self.rowVersion = other.rowVersion;
            self.prodCode = other.prodCode;
            self.producersId = other.producersId;
        };
        ProducersBase.Create = function () {
            return new Entities.Producers(undefined, undefined, undefined, undefined);
        };
        ProducersBase.CreateById = function (producersId) {
            var ret = Entities.Producers.Create();
            ret.producersId = producersId;
            return ret;
        };
        ProducersBase.findById_unecrypted = function (producersId, $scope, $http, errFunc) {
            if (Entities.Producers.cashedEntities[producersId.toString()] !== undefined)
                return Entities.Producers.cashedEntities[producersId.toString()];
            var wsPath = "Producers/findByProducersId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['producersId'] = producersId;
            var ret = Entities.Producers.Create();
            Entities.Producers.cashedEntities[producersId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.Producers.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + producersId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        ProducersBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        //public producersId_listeners: Array<(c:Producers) => void> = [];
        ProducersBase.gpRequestsProducersCollection = null; //(producers, list) => { };
        ProducersBase.agrisnapUsersProducersCollection = null; //(producers, list) => { };
        ProducersBase.cashedEntities = {};
        return ProducersBase;
    })(NpTypes.BaseEntity);
    Entities.ProducersBase = ProducersBase;
    NpTypes.BaseEntity.entitiesFactory['Producers'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.Producers.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.Producers.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=ProducersBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
