/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/Integrateddecision.ts" />
/// <reference path="../Entities/EcGroupCopy.ts" />
/// <reference path="../Entities/ParcelDecision.ts" />
/// <reference path="../Entities/Classification.ts" />
/// <reference path="../Entities/EcGroup.ts" />
/// <reference path="../Entities/DecisionMaking.ts" />

module Entities {

    export class DecisionMakingBase extends NpTypes.BaseEntity {
        constructor(
            demaId: string,
            description: string,
            dateTime: Date,
            rowVersion: number,
            recordtype: number,
            hasBeenRun: boolean,
            hasPopulatedIntegratedDecisionsAndIssues: boolean,
            clasId: Classification,
            ecgrId: EcGroup) 
        {
            super();
            var self = this;
            this._demaId = new NpTypes.UIStringModel(demaId, this);
            this._description = new NpTypes.UIStringModel(description, this);
            this._dateTime = new NpTypes.UIDateModel(dateTime, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._recordtype = new NpTypes.UINumberModel(recordtype, this);
            this._hasBeenRun = new NpTypes.UIBoolModel(hasBeenRun, this);
            this._hasPopulatedIntegratedDecisionsAndIssues = new NpTypes.UIBoolModel(hasPopulatedIntegratedDecisionsAndIssues, this);
            this._clasId = new NpTypes.UIManyToOneModel<Entities.Classification>(clasId, this);
            this._ecgrId = new NpTypes.UIManyToOneModel<Entities.EcGroup>(ecgrId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.demaId; 
        }
        
        public getKeyName(): string { 
            return "demaId"; 
        }

        public getEntityName(): string { 
            return 'DecisionMaking'; 
        }
        
        public fromJSON(data: any): DecisionMaking[] { 
            return DecisionMaking.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.demaId === null || this.demaId === undefined)
                return true;
            if (this.demaId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: DecisionMakingBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //demaId property
        _demaId: NpTypes.UIStringModel;
        public get demaId():string {
            return this._demaId.value;
        }
        public set demaId(demaId:string) {
            var self = this;
            self._demaId.value = demaId;
        //    self.demaId_listeners.forEach(cb => { cb(<DecisionMaking>self); });
        }
        //public demaId_listeners: Array<(c:DecisionMaking) => void> = [];
        //description property
        _description: NpTypes.UIStringModel;
        public get description():string {
            return this._description.value;
        }
        public set description(description:string) {
            var self = this;
            self._description.value = description;
        //    self.description_listeners.forEach(cb => { cb(<DecisionMaking>self); });
        }
        //public description_listeners: Array<(c:DecisionMaking) => void> = [];
        //dateTime property
        _dateTime: NpTypes.UIDateModel;
        public get dateTime():Date {
            return this._dateTime.value;
        }
        public set dateTime(dateTime:Date) {
            var self = this;
            self._dateTime.value = dateTime;
        //    self.dateTime_listeners.forEach(cb => { cb(<DecisionMaking>self); });
        }
        //public dateTime_listeners: Array<(c:DecisionMaking) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<DecisionMaking>self); });
        }
        //public rowVersion_listeners: Array<(c:DecisionMaking) => void> = [];
        //recordtype property
        _recordtype: NpTypes.UINumberModel;
        public get recordtype():number {
            return this._recordtype.value;
        }
        public set recordtype(recordtype:number) {
            var self = this;
            self._recordtype.value = recordtype;
        //    self.recordtype_listeners.forEach(cb => { cb(<DecisionMaking>self); });
        }
        //public recordtype_listeners: Array<(c:DecisionMaking) => void> = [];
        //hasBeenRun property
        _hasBeenRun: NpTypes.UIBoolModel;
        public get hasBeenRun():boolean {
            return this._hasBeenRun.value;
        }
        public set hasBeenRun(hasBeenRun:boolean) {
            var self = this;
            self._hasBeenRun.value = hasBeenRun;
        //    self.hasBeenRun_listeners.forEach(cb => { cb(<DecisionMaking>self); });
        }
        //public hasBeenRun_listeners: Array<(c:DecisionMaking) => void> = [];
        //hasPopulatedIntegratedDecisionsAndIssues property
        _hasPopulatedIntegratedDecisionsAndIssues: NpTypes.UIBoolModel;
        public get hasPopulatedIntegratedDecisionsAndIssues():boolean {
            return this._hasPopulatedIntegratedDecisionsAndIssues.value;
        }
        public set hasPopulatedIntegratedDecisionsAndIssues(hasPopulatedIntegratedDecisionsAndIssues:boolean) {
            var self = this;
            self._hasPopulatedIntegratedDecisionsAndIssues.value = hasPopulatedIntegratedDecisionsAndIssues;
        //    self.hasPopulatedIntegratedDecisionsAndIssues_listeners.forEach(cb => { cb(<DecisionMaking>self); });
        }
        //public hasPopulatedIntegratedDecisionsAndIssues_listeners: Array<(c:DecisionMaking) => void> = [];
        //clasId property
        _clasId: NpTypes.UIManyToOneModel<Entities.Classification>;
        public get clasId():Classification {
            return this._clasId.value;
        }
        public set clasId(clasId:Classification) {
            var self = this;
            self._clasId.value = clasId;
        //    self.clasId_listeners.forEach(cb => { cb(<DecisionMaking>self); });
        }
        //public clasId_listeners: Array<(c:DecisionMaking) => void> = [];
        //ecgrId property
        _ecgrId: NpTypes.UIManyToOneModel<Entities.EcGroup>;
        public get ecgrId():EcGroup {
            return this._ecgrId.value;
        }
        public set ecgrId(ecgrId:EcGroup) {
            var self = this;
            self._ecgrId.value = ecgrId;
        //    self.ecgrId_listeners.forEach(cb => { cb(<DecisionMaking>self); });
        }
        //public ecgrId_listeners: Array<(c:DecisionMaking) => void> = [];
        public static integrateddecisionCollection: (self:Entities.DecisionMaking, func: (integrateddecisionList: Entities.Integrateddecision[]) => void) => void = null; //(decisionMaking, list) => { };
        public integrateddecisionCollection(func: (integrateddecisionList: Entities.Integrateddecision[]) => void) {
            var self: Entities.DecisionMakingBase = this;
        	if (!isVoid(DecisionMaking.integrateddecisionCollection)) {
        		if (self instanceof Entities.DecisionMaking) {
        			DecisionMaking.integrateddecisionCollection(<Entities.DecisionMaking>self, func);
                }
            }
        }
        public static ecGroupCopyCollection: (self:Entities.DecisionMaking, func: (ecGroupCopyList: Entities.EcGroupCopy[]) => void) => void = null; //(decisionMaking, list) => { };
        public ecGroupCopyCollection(func: (ecGroupCopyList: Entities.EcGroupCopy[]) => void) {
            var self: Entities.DecisionMakingBase = this;
        	if (!isVoid(DecisionMaking.ecGroupCopyCollection)) {
        		if (self instanceof Entities.DecisionMaking) {
        			DecisionMaking.ecGroupCopyCollection(<Entities.DecisionMaking>self, func);
                }
            }
        }
        public static parcelDecisionCollection: (self:Entities.DecisionMaking, func: (parcelDecisionList: Entities.ParcelDecision[]) => void) => void = null; //(decisionMaking, list) => { };
        public parcelDecisionCollection(func: (parcelDecisionList: Entities.ParcelDecision[]) => void) {
            var self: Entities.DecisionMakingBase = this;
        	if (!isVoid(DecisionMaking.parcelDecisionCollection)) {
        		if (self instanceof Entities.DecisionMaking) {
        			DecisionMaking.parcelDecisionCollection(<Entities.DecisionMaking>self, func);
                }
            }
        }
        /*
        public removeAllListeners() {
            var self = this;
            self.demaId_listeners.splice(0);
            self.description_listeners.splice(0);
            self.dateTime_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.recordtype_listeners.splice(0);
            self.hasBeenRun_listeners.splice(0);
            self.hasPopulatedIntegratedDecisionsAndIssues_listeners.splice(0);
            self.clasId_listeners.splice(0);
            self.ecgrId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : DecisionMaking {
            var key = "DecisionMaking:"+x.demaId;
            var ret =  new DecisionMaking(
                x.demaId,
                x.description,
                isVoid(x.dateTime) ? null : new Date(x.dateTime),
                x.rowVersion,
                x.recordtype,
                x.hasBeenRun,
                x.hasPopulatedIntegratedDecisionsAndIssues,
                x.clasId,
                x.ecgrId
            );
            deserializedEntities[key] = ret;
            ret.clasId = (x.clasId !== undefined && x.clasId !== null) ? <Classification>NpTypes.BaseEntity.entitiesFactory[x.clasId.$entityName].fromJSONPartial(x.clasId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.ecgrId = (x.ecgrId !== undefined && x.ecgrId !== null) ? <EcGroup>NpTypes.BaseEntity.entitiesFactory[x.ecgrId.$entityName].fromJSONPartial(x.ecgrId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): DecisionMaking {
            
            return <DecisionMaking>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : DecisionMaking {
            var self = this;
            var key="";
            var ret:DecisionMaking = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "DecisionMaking:"+x.$refId;
                ret = <DecisionMaking>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "DecisionMakingBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "DecisionMaking:"+x.demaId;
                    var cachedCopy = <DecisionMaking>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = DecisionMaking.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = DecisionMaking.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = DecisionMaking.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<DecisionMaking> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<DecisionMaking> = _.map(data, (x:any):DecisionMaking => {
                return DecisionMaking.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:DecisionMaking[])=>void)  {
            var url = "/Niva/rest/DecisionMaking/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = DecisionMaking.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.demaId = this.demaId;
                ret.description = this.description;
                ret.dateTime = this.dateTime;
                ret.rowVersion = this.rowVersion;
                ret.recordtype = this.recordtype;
                ret.hasBeenRun = this.hasBeenRun;
                ret.hasPopulatedIntegratedDecisionsAndIssues = this.hasPopulatedIntegratedDecisionsAndIssues;
                ret.clasId = 
                    (this.clasId !== undefined && this.clasId !== null) ? 
                        { clasId :  this.clasId.clasId} :
                        (this.clasId === undefined ? undefined : null);
                ret.ecgrId = 
                    (this.ecgrId !== undefined && this.ecgrId !== null) ? 
                        { ecgrId :  this.ecgrId.ecgrId} :
                        (this.ecgrId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:DecisionMaking):void {
            var self = this;
            self.demaId = other.demaId
            self.description = other.description
            self.dateTime = other.dateTime
            self.rowVersion = other.rowVersion
            self.recordtype = other.recordtype
            self.hasBeenRun = other.hasBeenRun
            self.hasPopulatedIntegratedDecisionsAndIssues = other.hasPopulatedIntegratedDecisionsAndIssues
            self.clasId = other.clasId
            self.ecgrId = other.ecgrId
        }

        public static Create() : DecisionMaking {
            return new DecisionMaking(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(demaId:string) : DecisionMaking {
            var ret = DecisionMaking.Create();
            ret.demaId = demaId;
            return ret;
        }

        public static cashedEntities: { [id: string]: DecisionMaking; } = {};
        public static findById_unecrypted(demaId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : DecisionMaking {
            if (Entities.DecisionMaking.cashedEntities[demaId.toString()] !== undefined)
                return Entities.DecisionMaking.cashedEntities[demaId.toString()];

            var wsPath = "DecisionMaking/findByDemaId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['demaId'] = demaId;
            var ret = DecisionMaking.Create();
            Entities.DecisionMaking.cashedEntities[demaId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.DecisionMaking.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + demaId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['DecisionMaking'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.DecisionMaking.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.DecisionMaking.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
