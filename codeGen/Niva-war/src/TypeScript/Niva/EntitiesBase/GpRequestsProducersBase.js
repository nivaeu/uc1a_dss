/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/GpRequestsContexts.ts" />
/// <reference path="../Entities/GpRequests.ts" />
/// <reference path="../Entities/Producers.ts" />
/// <reference path="../Entities/GpRequestsProducers.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var GpRequestsProducersBase = (function (_super) {
        __extends(GpRequestsProducersBase, _super);
        function GpRequestsProducersBase(gpRequestsProducersId, rowVersion, notes, gpRequestsId, producersId) {
            _super.call(this);
            var self = this;
            this._gpRequestsProducersId = new NpTypes.UIStringModel(gpRequestsProducersId, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._notes = new NpTypes.UIStringModel(notes, this);
            this._gpRequestsId = new NpTypes.UIManyToOneModel(gpRequestsId, this);
            this._producersId = new NpTypes.UIManyToOneModel(producersId, this);
            self.postConstruct();
        }
        GpRequestsProducersBase.prototype.getKey = function () {
            return this.gpRequestsProducersId;
        };
        GpRequestsProducersBase.prototype.getKeyName = function () {
            return "gpRequestsProducersId";
        };
        GpRequestsProducersBase.prototype.getEntityName = function () {
            return 'GpRequestsProducers';
        };
        GpRequestsProducersBase.prototype.fromJSON = function (data) {
            return Entities.GpRequestsProducers.fromJSONComplete(data);
        };
        Object.defineProperty(GpRequestsProducersBase.prototype, "gpRequestsProducersId", {
            get: function () {
                return this._gpRequestsProducersId.value;
            },
            set: function (gpRequestsProducersId) {
                var self = this;
                self._gpRequestsProducersId.value = gpRequestsProducersId;
                //    self.gpRequestsProducersId_listeners.forEach(cb => { cb(<GpRequestsProducers>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsProducersBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<GpRequestsProducers>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsProducersBase.prototype, "notes", {
            get: function () {
                return this._notes.value;
            },
            set: function (notes) {
                var self = this;
                self._notes.value = notes;
                //    self.notes_listeners.forEach(cb => { cb(<GpRequestsProducers>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsProducersBase.prototype, "gpRequestsId", {
            get: function () {
                return this._gpRequestsId.value;
            },
            set: function (gpRequestsId) {
                var self = this;
                self._gpRequestsId.value = gpRequestsId;
                //    self.gpRequestsId_listeners.forEach(cb => { cb(<GpRequestsProducers>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsProducersBase.prototype, "producersId", {
            get: function () {
                return this._producersId.value;
            },
            set: function (producersId) {
                var self = this;
                self._producersId.value = producersId;
                //    self.producersId_listeners.forEach(cb => { cb(<GpRequestsProducers>self); });
            },
            enumerable: true,
            configurable: true
        });
        GpRequestsProducersBase.prototype.gpRequestsContextsCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.GpRequestsProducers.gpRequestsContextsCollection)) {
                if (self instanceof Entities.GpRequestsProducers) {
                    Entities.GpRequestsProducers.gpRequestsContextsCollection(self, func);
                }
            }
        };
        /*
        public removeAllListeners() {
            var self = this;
            self.gpRequestsProducersId_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.notes_listeners.splice(0);
            self.gpRequestsId_listeners.splice(0);
            self.producersId_listeners.splice(0);
        }
        */
        GpRequestsProducersBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "GpRequestsProducers:" + x.gpRequestsProducersId;
            var ret = new Entities.GpRequestsProducers(x.gpRequestsProducersId, x.rowVersion, x.notes, x.gpRequestsId, x.producersId);
            deserializedEntities[key] = ret;
            ret.gpRequestsId = (x.gpRequestsId !== undefined && x.gpRequestsId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.gpRequestsId.$entityName].fromJSONPartial(x.gpRequestsId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.producersId = (x.producersId !== undefined && x.producersId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.producersId.$entityName].fromJSONPartial(x.producersId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        GpRequestsProducersBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        GpRequestsProducersBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "GpRequestsProducers:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "GpRequestsProducersBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "GpRequestsProducers:"+x.gpRequestsProducersId;
                    var cachedCopy = <GpRequestsProducers>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = GpRequestsProducers.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = GpRequestsProducers.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.GpRequestsProducers.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        GpRequestsProducersBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.GpRequestsProducers.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        GpRequestsProducersBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/GpRequestsProducers/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.GpRequestsProducers.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        GpRequestsProducersBase.prototype.toJSON = function () {
            var ret = {};
            ret.gpRequestsProducersId = this.gpRequestsProducersId;
            ret.rowVersion = this.rowVersion;
            ret.notes = this.notes;
            ret.gpRequestsId =
                (this.gpRequestsId !== undefined && this.gpRequestsId !== null) ?
                    { gpRequestsId: this.gpRequestsId.gpRequestsId } :
                    (this.gpRequestsId === undefined ? undefined : null);
            ret.producersId =
                (this.producersId !== undefined && this.producersId !== null) ?
                    { producersId: this.producersId.producersId } :
                    (this.producersId === undefined ? undefined : null);
            return ret;
        };
        GpRequestsProducersBase.prototype.updateInstance = function (other) {
            var self = this;
            self.gpRequestsProducersId = other.gpRequestsProducersId;
            self.rowVersion = other.rowVersion;
            self.notes = other.notes;
            self.gpRequestsId = other.gpRequestsId;
            self.producersId = other.producersId;
        };
        GpRequestsProducersBase.Create = function () {
            return new Entities.GpRequestsProducers(undefined, undefined, undefined, undefined, undefined);
        };
        GpRequestsProducersBase.CreateById = function (gpRequestsProducersId) {
            var ret = Entities.GpRequestsProducers.Create();
            ret.gpRequestsProducersId = gpRequestsProducersId;
            return ret;
        };
        GpRequestsProducersBase.findById_unecrypted = function (gpRequestsProducersId, $scope, $http, errFunc) {
            if (Entities.GpRequestsProducers.cashedEntities[gpRequestsProducersId.toString()] !== undefined)
                return Entities.GpRequestsProducers.cashedEntities[gpRequestsProducersId.toString()];
            var wsPath = "GpRequestsProducers/findByGpRequestsProducersId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['gpRequestsProducersId'] = gpRequestsProducersId;
            var ret = Entities.GpRequestsProducers.Create();
            Entities.GpRequestsProducers.cashedEntities[gpRequestsProducersId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.GpRequestsProducers.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + gpRequestsProducersId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        GpRequestsProducersBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        //public producersId_listeners: Array<(c:GpRequestsProducers) => void> = [];
        GpRequestsProducersBase.gpRequestsContextsCollection = null; //(gpRequestsProducers, list) => { };
        GpRequestsProducersBase.cashedEntities = {};
        return GpRequestsProducersBase;
    })(NpTypes.BaseEntity);
    Entities.GpRequestsProducersBase = GpRequestsProducersBase;
    NpTypes.BaseEntity.entitiesFactory['GpRequestsProducers'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.GpRequestsProducers.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.GpRequestsProducers.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=GpRequestsProducersBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
