/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/ParcelsIssues.ts" />
/// <reference path="../Entities/FmisUpload.ts" />

module Entities {

    export class FmisUploadBase extends NpTypes.BaseEntity {
        constructor(
            fmisUploadsId: string,
            metadata: string,
            dteUpload: Date,
            docfile: string,
            usrUpload: string,
            parcelsIssuesId: ParcelsIssues) 
        {
            super();
            var self = this;
            this._fmisUploadsId = new NpTypes.UIStringModel(fmisUploadsId, this);
            this._metadata = new NpTypes.UIStringModel(metadata, this);
            this._dteUpload = new NpTypes.UIDateModel(dteUpload, this);
            this._docfile = new NpTypes.UIBlobModel(
                docfile, 
                ()  => 'FMIS Document',
                (fileName:string)   => {},
                this);
            this._usrUpload = new NpTypes.UIStringModel(usrUpload, this);
            this._parcelsIssuesId = new NpTypes.UIManyToOneModel<Entities.ParcelsIssues>(parcelsIssuesId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.fmisUploadsId; 
        }
        
        public getKeyName(): string { 
            return "fmisUploadsId"; 
        }

        public getEntityName(): string { 
            return 'FmisUpload'; 
        }
        
        public fromJSON(data: any): FmisUpload[] { 
            return FmisUpload.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.fmisUploadsId === null || this.fmisUploadsId === undefined)
                return true;
            if (this.fmisUploadsId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: FmisUploadBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //fmisUploadsId property
        _fmisUploadsId: NpTypes.UIStringModel;
        public get fmisUploadsId():string {
            return this._fmisUploadsId.value;
        }
        public set fmisUploadsId(fmisUploadsId:string) {
            var self = this;
            self._fmisUploadsId.value = fmisUploadsId;
        //    self.fmisUploadsId_listeners.forEach(cb => { cb(<FmisUpload>self); });
        }
        //public fmisUploadsId_listeners: Array<(c:FmisUpload) => void> = [];
        //metadata property
        _metadata: NpTypes.UIStringModel;
        public get metadata():string {
            return this._metadata.value;
        }
        public set metadata(metadata:string) {
            var self = this;
            self._metadata.value = metadata;
        //    self.metadata_listeners.forEach(cb => { cb(<FmisUpload>self); });
        }
        //public metadata_listeners: Array<(c:FmisUpload) => void> = [];
        //dteUpload property
        _dteUpload: NpTypes.UIDateModel;
        public get dteUpload():Date {
            return this._dteUpload.value;
        }
        public set dteUpload(dteUpload:Date) {
            var self = this;
            self._dteUpload.value = dteUpload;
        //    self.dteUpload_listeners.forEach(cb => { cb(<FmisUpload>self); });
        }
        //public dteUpload_listeners: Array<(c:FmisUpload) => void> = [];
        //docfile property
        _docfile: NpTypes.UIBlobModel;
        public get docfile():string {
            return this._docfile.value;
        }
        public set docfile(docfile:string) {
            var self = this;
            self._docfile.value = docfile;
        //    self.docfile_listeners.forEach(cb => { cb(<FmisUpload>self); });
        }
        //public docfile_listeners: Array<(c:FmisUpload) => void> = [];
        //usrUpload property
        _usrUpload: NpTypes.UIStringModel;
        public get usrUpload():string {
            return this._usrUpload.value;
        }
        public set usrUpload(usrUpload:string) {
            var self = this;
            self._usrUpload.value = usrUpload;
        //    self.usrUpload_listeners.forEach(cb => { cb(<FmisUpload>self); });
        }
        //public usrUpload_listeners: Array<(c:FmisUpload) => void> = [];
        //parcelsIssuesId property
        _parcelsIssuesId: NpTypes.UIManyToOneModel<Entities.ParcelsIssues>;
        public get parcelsIssuesId():ParcelsIssues {
            return this._parcelsIssuesId.value;
        }
        public set parcelsIssuesId(parcelsIssuesId:ParcelsIssues) {
            var self = this;
            self._parcelsIssuesId.value = parcelsIssuesId;
        //    self.parcelsIssuesId_listeners.forEach(cb => { cb(<FmisUpload>self); });
        }
        //public parcelsIssuesId_listeners: Array<(c:FmisUpload) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.fmisUploadsId_listeners.splice(0);
            self.metadata_listeners.splice(0);
            self.dteUpload_listeners.splice(0);
            self.docfile_listeners.splice(0);
            self.usrUpload_listeners.splice(0);
            self.parcelsIssuesId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : FmisUpload {
            var key = "FmisUpload:"+x.fmisUploadsId;
            var ret =  new FmisUpload(
                x.fmisUploadsId,
                x.metadata,
                isVoid(x.dteUpload) ? null : new Date(x.dteUpload),
                x.docfile,
                x.usrUpload,
                x.parcelsIssuesId
            );
            deserializedEntities[key] = ret;
            ret.parcelsIssuesId = (x.parcelsIssuesId !== undefined && x.parcelsIssuesId !== null) ? <ParcelsIssues>NpTypes.BaseEntity.entitiesFactory[x.parcelsIssuesId.$entityName].fromJSONPartial(x.parcelsIssuesId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): FmisUpload {
            
            return <FmisUpload>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : FmisUpload {
            var self = this;
            var key="";
            var ret:FmisUpload = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "FmisUpload:"+x.$refId;
                ret = <FmisUpload>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "FmisUploadBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "FmisUpload:"+x.fmisUploadsId;
                    var cachedCopy = <FmisUpload>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = FmisUpload.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = FmisUpload.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = FmisUpload.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<FmisUpload> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<FmisUpload> = _.map(data, (x:any):FmisUpload => {
                return FmisUpload.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:FmisUpload[])=>void)  {
            var url = "/Niva/rest/FmisUpload/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = FmisUpload.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.fmisUploadsId = this.fmisUploadsId;
                ret.metadata = this.metadata;
                ret.dteUpload = this.dteUpload;
                ret.docfile = this.docfile;
                ret.usrUpload = this.usrUpload;
                ret.parcelsIssuesId = 
                    (this.parcelsIssuesId !== undefined && this.parcelsIssuesId !== null) ? 
                        { parcelsIssuesId :  this.parcelsIssuesId.parcelsIssuesId} :
                        (this.parcelsIssuesId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:FmisUpload):void {
            var self = this;
            self.fmisUploadsId = other.fmisUploadsId
            self.metadata = other.metadata
            self.dteUpload = other.dteUpload
            self.docfile = other.docfile
            self.usrUpload = other.usrUpload
            self.parcelsIssuesId = other.parcelsIssuesId
        }

        public static Create() : FmisUpload {
            return new FmisUpload(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(fmisUploadsId:string) : FmisUpload {
            var ret = FmisUpload.Create();
            ret.fmisUploadsId = fmisUploadsId;
            return ret;
        }

        public static cashedEntities: { [id: string]: FmisUpload; } = {};
        public static findById_unecrypted(fmisUploadsId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : FmisUpload {
            if (Entities.FmisUpload.cashedEntities[fmisUploadsId.toString()] !== undefined)
                return Entities.FmisUpload.cashedEntities[fmisUploadsId.toString()];

            var wsPath = "FmisUpload/findByFmisUploadsId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['fmisUploadsId'] = fmisUploadsId;
            var ret = FmisUpload.Create();
            Entities.FmisUpload.cashedEntities[fmisUploadsId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.FmisUpload.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + fmisUploadsId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return 0;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['FmisUpload'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.FmisUpload.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.FmisUpload.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
