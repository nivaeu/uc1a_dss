/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/ParcelsIssues.ts" />
/// <reference path="../Entities/ParcelsIssuesStatusPerDema.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var ParcelsIssuesStatusPerDemaBase = (function (_super) {
        __extends(ParcelsIssuesStatusPerDemaBase, _super);
        function ParcelsIssuesStatusPerDemaBase(status, dteStatusUpdate, rowVersion, parcelsIssuesStatusPerDemaId, parcelsIssuesId) {
            _super.call(this);
            var self = this;
            this._status = new NpTypes.UINumberModel(status, this);
            this._dteStatusUpdate = new NpTypes.UIDateModel(dteStatusUpdate, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._parcelsIssuesStatusPerDemaId = new NpTypes.UIStringModel(parcelsIssuesStatusPerDemaId, this);
            this._parcelsIssuesId = new NpTypes.UIManyToOneModel(parcelsIssuesId, this);
            self.postConstruct();
        }
        ParcelsIssuesStatusPerDemaBase.prototype.getKey = function () {
            return this.parcelsIssuesStatusPerDemaId;
        };
        ParcelsIssuesStatusPerDemaBase.prototype.getKeyName = function () {
            return "parcelsIssuesStatusPerDemaId";
        };
        ParcelsIssuesStatusPerDemaBase.prototype.getEntityName = function () {
            return 'ParcelsIssuesStatusPerDema';
        };
        ParcelsIssuesStatusPerDemaBase.prototype.fromJSON = function (data) {
            return Entities.ParcelsIssuesStatusPerDema.fromJSONComplete(data);
        };
        Object.defineProperty(ParcelsIssuesStatusPerDemaBase.prototype, "status", {
            get: function () {
                return this._status.value;
            },
            set: function (status) {
                var self = this;
                self._status.value = status;
                //    self.status_listeners.forEach(cb => { cb(<ParcelsIssuesStatusPerDema>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelsIssuesStatusPerDemaBase.prototype, "dteStatusUpdate", {
            get: function () {
                return this._dteStatusUpdate.value;
            },
            set: function (dteStatusUpdate) {
                var self = this;
                self._dteStatusUpdate.value = dteStatusUpdate;
                //    self.dteStatusUpdate_listeners.forEach(cb => { cb(<ParcelsIssuesStatusPerDema>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelsIssuesStatusPerDemaBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<ParcelsIssuesStatusPerDema>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelsIssuesStatusPerDemaBase.prototype, "parcelsIssuesStatusPerDemaId", {
            get: function () {
                return this._parcelsIssuesStatusPerDemaId.value;
            },
            set: function (parcelsIssuesStatusPerDemaId) {
                var self = this;
                self._parcelsIssuesStatusPerDemaId.value = parcelsIssuesStatusPerDemaId;
                //    self.parcelsIssuesStatusPerDemaId_listeners.forEach(cb => { cb(<ParcelsIssuesStatusPerDema>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ParcelsIssuesStatusPerDemaBase.prototype, "parcelsIssuesId", {
            get: function () {
                return this._parcelsIssuesId.value;
            },
            set: function (parcelsIssuesId) {
                var self = this;
                self._parcelsIssuesId.value = parcelsIssuesId;
                //    self.parcelsIssuesId_listeners.forEach(cb => { cb(<ParcelsIssuesStatusPerDema>self); });
            },
            enumerable: true,
            configurable: true
        });
        //public parcelsIssuesId_listeners: Array<(c:ParcelsIssuesStatusPerDema) => void> = [];
        /*
        public removeAllListeners() {
            var self = this;
            self.status_listeners.splice(0);
            self.dteStatusUpdate_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.parcelsIssuesStatusPerDemaId_listeners.splice(0);
            self.parcelsIssuesId_listeners.splice(0);
        }
        */
        ParcelsIssuesStatusPerDemaBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "ParcelsIssuesStatusPerDema:" + x.parcelsIssuesStatusPerDemaId;
            var ret = new Entities.ParcelsIssuesStatusPerDema(x.status, isVoid(x.dteStatusUpdate) ? null : new Date(x.dteStatusUpdate), x.rowVersion, x.parcelsIssuesStatusPerDemaId, x.parcelsIssuesId);
            deserializedEntities[key] = ret;
            ret.parcelsIssuesId = (x.parcelsIssuesId !== undefined && x.parcelsIssuesId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.parcelsIssuesId.$entityName].fromJSONPartial(x.parcelsIssuesId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        ParcelsIssuesStatusPerDemaBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        ParcelsIssuesStatusPerDemaBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "ParcelsIssuesStatusPerDema:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "ParcelsIssuesStatusPerDemaBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "ParcelsIssuesStatusPerDema:"+x.parcelsIssuesStatusPerDemaId;
                    var cachedCopy = <ParcelsIssuesStatusPerDema>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = ParcelsIssuesStatusPerDema.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = ParcelsIssuesStatusPerDema.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.ParcelsIssuesStatusPerDema.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        ParcelsIssuesStatusPerDemaBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.ParcelsIssuesStatusPerDema.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        ParcelsIssuesStatusPerDemaBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/ParcelsIssuesStatusPerDema/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.ParcelsIssuesStatusPerDema.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        ParcelsIssuesStatusPerDemaBase.prototype.toJSON = function () {
            var ret = {};
            ret.status = this.status;
            ret.dteStatusUpdate = this.dteStatusUpdate;
            ret.rowVersion = this.rowVersion;
            ret.parcelsIssuesStatusPerDemaId = this.parcelsIssuesStatusPerDemaId;
            ret.parcelsIssuesId =
                (this.parcelsIssuesId !== undefined && this.parcelsIssuesId !== null) ?
                    { parcelsIssuesId: this.parcelsIssuesId.parcelsIssuesId } :
                    (this.parcelsIssuesId === undefined ? undefined : null);
            return ret;
        };
        ParcelsIssuesStatusPerDemaBase.prototype.updateInstance = function (other) {
            var self = this;
            self.status = other.status;
            self.dteStatusUpdate = other.dteStatusUpdate;
            self.rowVersion = other.rowVersion;
            self.parcelsIssuesStatusPerDemaId = other.parcelsIssuesStatusPerDemaId;
            self.parcelsIssuesId = other.parcelsIssuesId;
        };
        ParcelsIssuesStatusPerDemaBase.Create = function () {
            return new Entities.ParcelsIssuesStatusPerDema(undefined, undefined, undefined, undefined, undefined);
        };
        ParcelsIssuesStatusPerDemaBase.CreateById = function (parcelsIssuesStatusPerDemaId) {
            var ret = Entities.ParcelsIssuesStatusPerDema.Create();
            ret.parcelsIssuesStatusPerDemaId = parcelsIssuesStatusPerDemaId;
            return ret;
        };
        ParcelsIssuesStatusPerDemaBase.findById_unecrypted = function (parcelsIssuesStatusPerDemaId, $scope, $http, errFunc) {
            if (Entities.ParcelsIssuesStatusPerDema.cashedEntities[parcelsIssuesStatusPerDemaId.toString()] !== undefined)
                return Entities.ParcelsIssuesStatusPerDema.cashedEntities[parcelsIssuesStatusPerDemaId.toString()];
            var wsPath = "ParcelsIssuesStatusPerDema/findByParcelsIssuesStatusPerDemaId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['parcelsIssuesStatusPerDemaId'] = parcelsIssuesStatusPerDemaId;
            var ret = Entities.ParcelsIssuesStatusPerDema.Create();
            Entities.ParcelsIssuesStatusPerDema.cashedEntities[parcelsIssuesStatusPerDemaId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.ParcelsIssuesStatusPerDema.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + parcelsIssuesStatusPerDemaId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        ParcelsIssuesStatusPerDemaBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        ParcelsIssuesStatusPerDemaBase.cashedEntities = {};
        return ParcelsIssuesStatusPerDemaBase;
    })(NpTypes.BaseEntity);
    Entities.ParcelsIssuesStatusPerDemaBase = ParcelsIssuesStatusPerDemaBase;
    NpTypes.BaseEntity.entitiesFactory['ParcelsIssuesStatusPerDema'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.ParcelsIssuesStatusPerDema.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.ParcelsIssuesStatusPerDema.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=ParcelsIssuesStatusPerDemaBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
