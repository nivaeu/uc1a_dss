/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/EcCult.ts" />
/// <reference path="../Entities/FmisDecision.ts" />
/// <reference path="../Entities/SuperClassDetail.ts" />
/// <reference path="../Entities/GpDecision.ts" />
/// <reference path="../Entities/EcCultCopy.ts" />
/// <reference path="../Entities/ParcelClas.ts" />
/// <reference path="../Entities/ParcelClas.ts" />
/// <reference path="../Entities/ParcelClas.ts" />
/// <reference path="../Entities/CoverType.ts" />
/// <reference path="../Entities/ExcelFile.ts" />
/// <reference path="../Entities/Cultivation.ts" />

module Entities {

    export class CultivationBase extends NpTypes.BaseEntity {
        constructor(
            cultId: string,
            name: string,
            code: number,
            rowVersion: number,
            cotyId: CoverType,
            exfiId: ExcelFile) 
        {
            super();
            var self = this;
            this._cultId = new NpTypes.UIStringModel(cultId, this);
            this._name = new NpTypes.UIStringModel(name, this);
            this._code = new NpTypes.UINumberModel(code, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._cotyId = new NpTypes.UIManyToOneModel<Entities.CoverType>(cotyId, this);
            this._exfiId = new NpTypes.UIManyToOneModel<Entities.ExcelFile>(exfiId, this);
            self.postConstruct();
        }

        public getKey(): string { 
            return this.cultId; 
        }
        
        public getKeyName(): string { 
            return "cultId"; 
        }

        public getEntityName(): string { 
            return 'Cultivation'; 
        }
        
        public fromJSON(data: any): Cultivation[] { 
            return Cultivation.fromJSONComplete(data);
        }

        /*
        the following two functions are now implemented in the
        BaseEntity so they can be removed

        public isNew(): boolean { 
            if (this.cultId === null || this.cultId === undefined)
                return true;
            if (this.cultId.indexOf('TEMP_ID') === 0)
                return true;
            return false; 
        }

        public isEqual(other: CultivationBase): boolean {
            if (isVoid(other))
                return false;
            return this.getKey() === other.getKey();
        }
        */

        //cultId property
        _cultId: NpTypes.UIStringModel;
        public get cultId():string {
            return this._cultId.value;
        }
        public set cultId(cultId:string) {
            var self = this;
            self._cultId.value = cultId;
        //    self.cultId_listeners.forEach(cb => { cb(<Cultivation>self); });
        }
        //public cultId_listeners: Array<(c:Cultivation) => void> = [];
        //name property
        _name: NpTypes.UIStringModel;
        public get name():string {
            return this._name.value;
        }
        public set name(name:string) {
            var self = this;
            self._name.value = name;
        //    self.name_listeners.forEach(cb => { cb(<Cultivation>self); });
        }
        //public name_listeners: Array<(c:Cultivation) => void> = [];
        //code property
        _code: NpTypes.UINumberModel;
        public get code():number {
            return this._code.value;
        }
        public set code(code:number) {
            var self = this;
            self._code.value = code;
        //    self.code_listeners.forEach(cb => { cb(<Cultivation>self); });
        }
        //public code_listeners: Array<(c:Cultivation) => void> = [];
        //rowVersion property
        _rowVersion: NpTypes.UINumberModel;
        public get rowVersion():number {
            return this._rowVersion.value;
        }
        public set rowVersion(rowVersion:number) {
            var self = this;
            self._rowVersion.value = rowVersion;
        //    self.rowVersion_listeners.forEach(cb => { cb(<Cultivation>self); });
        }
        //public rowVersion_listeners: Array<(c:Cultivation) => void> = [];
        //cotyId property
        _cotyId: NpTypes.UIManyToOneModel<Entities.CoverType>;
        public get cotyId():CoverType {
            return this._cotyId.value;
        }
        public set cotyId(cotyId:CoverType) {
            var self = this;
            self._cotyId.value = cotyId;
        //    self.cotyId_listeners.forEach(cb => { cb(<Cultivation>self); });
        }
        //public cotyId_listeners: Array<(c:Cultivation) => void> = [];
        //exfiId property
        _exfiId: NpTypes.UIManyToOneModel<Entities.ExcelFile>;
        public get exfiId():ExcelFile {
            return this._exfiId.value;
        }
        public set exfiId(exfiId:ExcelFile) {
            var self = this;
            self._exfiId.value = exfiId;
        //    self.exfiId_listeners.forEach(cb => { cb(<Cultivation>self); });
        }
        //public exfiId_listeners: Array<(c:Cultivation) => void> = [];
        public static ecCultCollection: (self:Entities.Cultivation, func: (ecCultList: Entities.EcCult[]) => void) => void = null; //(cultivation, list) => { };
        public ecCultCollection(func: (ecCultList: Entities.EcCult[]) => void) {
            var self: Entities.CultivationBase = this;
        	if (!isVoid(Cultivation.ecCultCollection)) {
        		if (self instanceof Entities.Cultivation) {
        			Cultivation.ecCultCollection(<Entities.Cultivation>self, func);
                }
            }
        }
        public static fmisDecisionCollection: (self:Entities.Cultivation, func: (fmisDecisionList: Entities.FmisDecision[]) => void) => void = null; //(cultivation, list) => { };
        public fmisDecisionCollection(func: (fmisDecisionList: Entities.FmisDecision[]) => void) {
            var self: Entities.CultivationBase = this;
        	if (!isVoid(Cultivation.fmisDecisionCollection)) {
        		if (self instanceof Entities.Cultivation) {
        			Cultivation.fmisDecisionCollection(<Entities.Cultivation>self, func);
                }
            }
        }
        public static superClassDetailCollection: (self:Entities.Cultivation, func: (superClassDetailList: Entities.SuperClassDetail[]) => void) => void = null; //(cultivation, list) => { };
        public superClassDetailCollection(func: (superClassDetailList: Entities.SuperClassDetail[]) => void) {
            var self: Entities.CultivationBase = this;
        	if (!isVoid(Cultivation.superClassDetailCollection)) {
        		if (self instanceof Entities.Cultivation) {
        			Cultivation.superClassDetailCollection(<Entities.Cultivation>self, func);
                }
            }
        }
        public static gpDecisionCollection: (self:Entities.Cultivation, func: (gpDecisionList: Entities.GpDecision[]) => void) => void = null; //(cultivation, list) => { };
        public gpDecisionCollection(func: (gpDecisionList: Entities.GpDecision[]) => void) {
            var self: Entities.CultivationBase = this;
        	if (!isVoid(Cultivation.gpDecisionCollection)) {
        		if (self instanceof Entities.Cultivation) {
        			Cultivation.gpDecisionCollection(<Entities.Cultivation>self, func);
                }
            }
        }
        public static ecCultCopyCollection: (self:Entities.Cultivation, func: (ecCultCopyList: Entities.EcCultCopy[]) => void) => void = null; //(cultivation, list) => { };
        public ecCultCopyCollection(func: (ecCultCopyList: Entities.EcCultCopy[]) => void) {
            var self: Entities.CultivationBase = this;
        	if (!isVoid(Cultivation.ecCultCopyCollection)) {
        		if (self instanceof Entities.Cultivation) {
        			Cultivation.ecCultCopyCollection(<Entities.Cultivation>self, func);
                }
            }
        }
        public static parcelClascult_id_declCollection: (self:Entities.Cultivation, func: (parcelClasList: Entities.ParcelClas[]) => void) => void = null; //(cultivation, list) => { };
        public parcelClascult_id_declCollection(func: (parcelClasList: Entities.ParcelClas[]) => void) {
            var self: Entities.CultivationBase = this;
        	if (!isVoid(Cultivation.parcelClascult_id_declCollection)) {
        		if (self instanceof Entities.Cultivation) {
        			Cultivation.parcelClascult_id_declCollection(<Entities.Cultivation>self, func);
                }
            }
        }
        public static parcelClascult_id_predCollection: (self:Entities.Cultivation, func: (parcelClasList: Entities.ParcelClas[]) => void) => void = null; //(cultivation, list) => { };
        public parcelClascult_id_predCollection(func: (parcelClasList: Entities.ParcelClas[]) => void) {
            var self: Entities.CultivationBase = this;
        	if (!isVoid(Cultivation.parcelClascult_id_predCollection)) {
        		if (self instanceof Entities.Cultivation) {
        			Cultivation.parcelClascult_id_predCollection(<Entities.Cultivation>self, func);
                }
            }
        }
        public static parcelClascult_id_pred2Collection: (self:Entities.Cultivation, func: (parcelClasList: Entities.ParcelClas[]) => void) => void = null; //(cultivation, list) => { };
        public parcelClascult_id_pred2Collection(func: (parcelClasList: Entities.ParcelClas[]) => void) {
            var self: Entities.CultivationBase = this;
        	if (!isVoid(Cultivation.parcelClascult_id_pred2Collection)) {
        		if (self instanceof Entities.Cultivation) {
        			Cultivation.parcelClascult_id_pred2Collection(<Entities.Cultivation>self, func);
                }
            }
        }
        /*
        public removeAllListeners() {
            var self = this;
            self.cultId_listeners.splice(0);
            self.name_listeners.splice(0);
            self.code_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.cotyId_listeners.splice(0);
            self.exfiId_listeners.splice(0);
        }
        */
        public static fromJSONPartial_actualdecode_private(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) : Cultivation {
            var key = "Cultivation:"+x.cultId;
            var ret =  new Cultivation(
                x.cultId,
                x.name,
                x.code,
                x.rowVersion,
                x.cotyId,
                x.exfiId
            );
            deserializedEntities[key] = ret;
            ret.cotyId = (x.cotyId !== undefined && x.cotyId !== null) ? <CoverType>NpTypes.BaseEntity.entitiesFactory[x.cotyId.$entityName].fromJSONPartial(x.cotyId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined,
            ret.exfiId = (x.exfiId !== undefined && x.exfiId !== null) ? <ExcelFile>NpTypes.BaseEntity.entitiesFactory[x.exfiId.$entityName].fromJSONPartial(x.exfiId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) :  undefined
            return ret;
        }
        public static fromJSONPartial_actualdecode(x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }): Cultivation {
            
            return <Cultivation>NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        static fromJSONPartial(x:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind:NpTypes.EntityCachedKind) : Cultivation {
            var self = this;
            var key="";
            var ret:Cultivation = undefined;
              
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "Cultivation:"+x.$refId;
                ret = <Cultivation>deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "CultivationBase::fromJSONPartial Received $refIf="+x.$refId+"but entity not in deserializedEntities map");
                }

            } else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "Cultivation:"+x.cultId;
                    var cachedCopy = <Cultivation>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = Cultivation.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = Cultivation.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Cultivation.fromJSONPartial_actualdecode(x, deserializedEntities);
            }

            return ret;
        }

        static fromJSONComplete(data:any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {}) : Array<Cultivation> {
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret:Array<Cultivation> = _.map(data, (x:any):Cultivation => {
                return Cultivation.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });

            return ret;
        }

        
        public static getEntitiesFromIdsList(ctrl:NpTypes.IAbstractController, ids:string[], func:(list:Cultivation[])=>void)  {
            var url = "/Niva/rest/Cultivation/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, rsp => {
                var dbEntities = Cultivation.fromJSONComplete(rsp.data); 
                func(dbEntities);
            });


        }

        public toJSON():any {
            var ret:any = {};
                ret.cultId = this.cultId;
                ret.name = this.name;
                ret.code = this.code;
                ret.rowVersion = this.rowVersion;
                ret.cotyId = 
                    (this.cotyId !== undefined && this.cotyId !== null) ? 
                        { cotyId :  this.cotyId.cotyId} :
                        (this.cotyId === undefined ? undefined : null);
                ret.exfiId = 
                    (this.exfiId !== undefined && this.exfiId !== null) ? 
                        { id :  this.exfiId.id} :
                        (this.exfiId === undefined ? undefined : null);
            return ret;
        }

        public updateInstance(other:Cultivation):void {
            var self = this;
            self.cultId = other.cultId
            self.name = other.name
            self.code = other.code
            self.rowVersion = other.rowVersion
            self.cotyId = other.cotyId
            self.exfiId = other.exfiId
        }

        public static Create() : Cultivation {
            return new Cultivation(
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined,
                    undefined
                );
        }
        public static CreateById(cultId:string) : Cultivation {
            var ret = Cultivation.Create();
            ret.cultId = cultId;
            return ret;
        }

        public static cashedEntities: { [id: string]: Cultivation; } = {};
        public static findById_unecrypted(cultId:string, $scope: NpTypes.IApplicationScope, $http: ng.IHttpService, errFunc:(msg:string)=>void) : Cultivation {
            if (Entities.Cultivation.cashedEntities[cultId.toString()] !== undefined)
                return Entities.Cultivation.cashedEntities[cultId.toString()];

            var wsPath = "Cultivation/findByCultId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['cultId'] = cultId;
            var ret = Cultivation.Create();
            Entities.Cultivation.cashedEntities[cultId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, {timeout:$scope.globals.timeoutInMS, cache:false}).
                success(function (response, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    var dbEntities  = Entities.Cultivation.fromJSONComplete(response.data);
                    if (dbEntities.length === 1) {
                        ret.updateInstance(dbEntities[0]);
                    } else {
                        errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + cultId + "\")"));
                    }
                }).error(function (data, status, header, config) {
                    $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    errFunc(data);
                });


            return ret;
        }




        public getRowVersion(): number {
            return this.rowVersion;
        }


        

    }

    NpTypes.BaseEntity.entitiesFactory['Cultivation'] = {
        fromJSONPartial_actualdecode: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }) => {
            return Entities.Cultivation.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: (x: any, deserializedEntities: { [id: string]: NpTypes.IBaseEntity; }, entityKind: NpTypes.EntityCachedKind) => {
            return Entities.Cultivation.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    }


} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
