/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/EcCultCopy.ts" />
/// <reference path="../Entities/DecisionMaking.ts" />
/// <reference path="../Entities/EcGroupCopy.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var EcGroupCopyBase = (function (_super) {
        __extends(EcGroupCopyBase, _super);
        function EcGroupCopyBase(ecgcId, description, name, rowVersion, recordtype, cropLevel, demaId) {
            _super.call(this);
            var self = this;
            this._ecgcId = new NpTypes.UIStringModel(ecgcId, this);
            this._description = new NpTypes.UIStringModel(description, this);
            this._name = new NpTypes.UIStringModel(name, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._recordtype = new NpTypes.UINumberModel(recordtype, this);
            this._cropLevel = new NpTypes.UINumberModel(cropLevel, this);
            this._demaId = new NpTypes.UIManyToOneModel(demaId, this);
            self.postConstruct();
        }
        EcGroupCopyBase.prototype.getKey = function () {
            return this.ecgcId;
        };
        EcGroupCopyBase.prototype.getKeyName = function () {
            return "ecgcId";
        };
        EcGroupCopyBase.prototype.getEntityName = function () {
            return 'EcGroupCopy';
        };
        EcGroupCopyBase.prototype.fromJSON = function (data) {
            return Entities.EcGroupCopy.fromJSONComplete(data);
        };
        Object.defineProperty(EcGroupCopyBase.prototype, "ecgcId", {
            get: function () {
                return this._ecgcId.value;
            },
            set: function (ecgcId) {
                var self = this;
                self._ecgcId.value = ecgcId;
                //    self.ecgcId_listeners.forEach(cb => { cb(<EcGroupCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcGroupCopyBase.prototype, "description", {
            get: function () {
                return this._description.value;
            },
            set: function (description) {
                var self = this;
                self._description.value = description;
                //    self.description_listeners.forEach(cb => { cb(<EcGroupCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcGroupCopyBase.prototype, "name", {
            get: function () {
                return this._name.value;
            },
            set: function (name) {
                var self = this;
                self._name.value = name;
                //    self.name_listeners.forEach(cb => { cb(<EcGroupCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcGroupCopyBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<EcGroupCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcGroupCopyBase.prototype, "recordtype", {
            get: function () {
                return this._recordtype.value;
            },
            set: function (recordtype) {
                var self = this;
                self._recordtype.value = recordtype;
                //    self.recordtype_listeners.forEach(cb => { cb(<EcGroupCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcGroupCopyBase.prototype, "cropLevel", {
            get: function () {
                return this._cropLevel.value;
            },
            set: function (cropLevel) {
                var self = this;
                self._cropLevel.value = cropLevel;
                //    self.cropLevel_listeners.forEach(cb => { cb(<EcGroupCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EcGroupCopyBase.prototype, "demaId", {
            get: function () {
                return this._demaId.value;
            },
            set: function (demaId) {
                var self = this;
                self._demaId.value = demaId;
                //    self.demaId_listeners.forEach(cb => { cb(<EcGroupCopy>self); });
            },
            enumerable: true,
            configurable: true
        });
        EcGroupCopyBase.prototype.ecCultCopyCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.EcGroupCopy.ecCultCopyCollection)) {
                if (self instanceof Entities.EcGroupCopy) {
                    Entities.EcGroupCopy.ecCultCopyCollection(self, func);
                }
            }
        };
        /*
        public removeAllListeners() {
            var self = this;
            self.ecgcId_listeners.splice(0);
            self.description_listeners.splice(0);
            self.name_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.recordtype_listeners.splice(0);
            self.cropLevel_listeners.splice(0);
            self.demaId_listeners.splice(0);
        }
        */
        EcGroupCopyBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "EcGroupCopy:" + x.ecgcId;
            var ret = new Entities.EcGroupCopy(x.ecgcId, x.description, x.name, x.rowVersion, x.recordtype, x.cropLevel, x.demaId);
            deserializedEntities[key] = ret;
            ret.demaId = (x.demaId !== undefined && x.demaId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.demaId.$entityName].fromJSONPartial(x.demaId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        EcGroupCopyBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        EcGroupCopyBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "EcGroupCopy:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "EcGroupCopyBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "EcGroupCopy:"+x.ecgcId;
                    var cachedCopy = <EcGroupCopy>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = EcGroupCopy.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = EcGroupCopy.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.EcGroupCopy.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        EcGroupCopyBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.EcGroupCopy.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        EcGroupCopyBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/EcGroupCopy/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.EcGroupCopy.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        EcGroupCopyBase.prototype.toJSON = function () {
            var ret = {};
            ret.ecgcId = this.ecgcId;
            ret.description = this.description;
            ret.name = this.name;
            ret.rowVersion = this.rowVersion;
            ret.recordtype = this.recordtype;
            ret.cropLevel = this.cropLevel;
            ret.demaId =
                (this.demaId !== undefined && this.demaId !== null) ?
                    { demaId: this.demaId.demaId } :
                    (this.demaId === undefined ? undefined : null);
            return ret;
        };
        EcGroupCopyBase.prototype.updateInstance = function (other) {
            var self = this;
            self.ecgcId = other.ecgcId;
            self.description = other.description;
            self.name = other.name;
            self.rowVersion = other.rowVersion;
            self.recordtype = other.recordtype;
            self.cropLevel = other.cropLevel;
            self.demaId = other.demaId;
        };
        EcGroupCopyBase.Create = function () {
            return new Entities.EcGroupCopy(undefined, undefined, undefined, undefined, undefined, undefined, undefined);
        };
        EcGroupCopyBase.CreateById = function (ecgcId) {
            var ret = Entities.EcGroupCopy.Create();
            ret.ecgcId = ecgcId;
            return ret;
        };
        EcGroupCopyBase.findById_unecrypted = function (ecgcId, $scope, $http, errFunc) {
            if (Entities.EcGroupCopy.cashedEntities[ecgcId.toString()] !== undefined)
                return Entities.EcGroupCopy.cashedEntities[ecgcId.toString()];
            var wsPath = "EcGroupCopy/findByEcgcId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['ecgcId'] = ecgcId;
            var ret = Entities.EcGroupCopy.Create();
            Entities.EcGroupCopy.cashedEntities[ecgcId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.EcGroupCopy.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + ecgcId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        EcGroupCopyBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        //public demaId_listeners: Array<(c:EcGroupCopy) => void> = [];
        EcGroupCopyBase.ecCultCopyCollection = null; //(ecGroupCopy, list) => { };
        EcGroupCopyBase.cashedEntities = {};
        return EcGroupCopyBase;
    })(NpTypes.BaseEntity);
    Entities.EcGroupCopyBase = EcGroupCopyBase;
    NpTypes.BaseEntity.entitiesFactory['EcGroupCopy'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.EcGroupCopy.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.EcGroupCopy.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=EcGroupCopyBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
