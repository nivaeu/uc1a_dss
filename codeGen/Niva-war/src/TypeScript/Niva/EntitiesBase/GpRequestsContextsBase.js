/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../RTL/npTypes.ts" />
/// <reference path="../../RTL/utils.ts" />
/// <reference path="../../RTL/geoLib.ts" />
/// <reference path="../Entities/GpUpload.ts" />
/// <reference path="../Entities/GpRequestsProducers.ts" />
/// <reference path="../Entities/ParcelsIssues.ts" />
/// <reference path="../Entities/GpRequestsContexts.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Entities;
(function (Entities) {
    var GpRequestsContextsBase = (function (_super) {
        __extends(GpRequestsContextsBase, _super);
        function GpRequestsContextsBase(gpRequestsContextsId, type, label, comment, geomHexewkb, referencepoint, rowVersion, hash, gpRequestsProducersId, parcelsIssuesId) {
            _super.call(this);
            var self = this;
            this._gpRequestsContextsId = new NpTypes.UIStringModel(gpRequestsContextsId, this);
            this._type = new NpTypes.UIStringModel(type, this);
            this._label = new NpTypes.UIStringModel(label, this);
            this._comment = new NpTypes.UIStringModel(comment, this);
            this._geomHexewkb = new NpTypes.UIStringModel(geomHexewkb, this);
            this._referencepoint = new NpTypes.UIStringModel(referencepoint, this);
            this._rowVersion = new NpTypes.UINumberModel(rowVersion, this);
            this._hash = new NpTypes.UIStringModel(hash, this);
            this._gpRequestsProducersId = new NpTypes.UIManyToOneModel(gpRequestsProducersId, this);
            this._parcelsIssuesId = new NpTypes.UIManyToOneModel(parcelsIssuesId, this);
            self.postConstruct();
        }
        GpRequestsContextsBase.prototype.getKey = function () {
            return this.gpRequestsContextsId;
        };
        GpRequestsContextsBase.prototype.getKeyName = function () {
            return "gpRequestsContextsId";
        };
        GpRequestsContextsBase.prototype.getEntityName = function () {
            return 'GpRequestsContexts';
        };
        GpRequestsContextsBase.prototype.fromJSON = function (data) {
            return Entities.GpRequestsContexts.fromJSONComplete(data);
        };
        Object.defineProperty(GpRequestsContextsBase.prototype, "gpRequestsContextsId", {
            get: function () {
                return this._gpRequestsContextsId.value;
            },
            set: function (gpRequestsContextsId) {
                var self = this;
                self._gpRequestsContextsId.value = gpRequestsContextsId;
                //    self.gpRequestsContextsId_listeners.forEach(cb => { cb(<GpRequestsContexts>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsContextsBase.prototype, "type", {
            get: function () {
                return this._type.value;
            },
            set: function (type) {
                var self = this;
                self._type.value = type;
                //    self.type_listeners.forEach(cb => { cb(<GpRequestsContexts>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsContextsBase.prototype, "label", {
            get: function () {
                return this._label.value;
            },
            set: function (label) {
                var self = this;
                self._label.value = label;
                //    self.label_listeners.forEach(cb => { cb(<GpRequestsContexts>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsContextsBase.prototype, "comment", {
            get: function () {
                return this._comment.value;
            },
            set: function (comment) {
                var self = this;
                self._comment.value = comment;
                //    self.comment_listeners.forEach(cb => { cb(<GpRequestsContexts>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsContextsBase.prototype, "geomHexewkb", {
            get: function () {
                return this._geomHexewkb.value;
            },
            set: function (geomHexewkb) {
                var self = this;
                self._geomHexewkb.value = geomHexewkb;
                //    self.geomHexewkb_listeners.forEach(cb => { cb(<GpRequestsContexts>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsContextsBase.prototype, "referencepoint", {
            get: function () {
                return this._referencepoint.value;
            },
            set: function (referencepoint) {
                var self = this;
                self._referencepoint.value = referencepoint;
                //    self.referencepoint_listeners.forEach(cb => { cb(<GpRequestsContexts>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsContextsBase.prototype, "rowVersion", {
            get: function () {
                return this._rowVersion.value;
            },
            set: function (rowVersion) {
                var self = this;
                self._rowVersion.value = rowVersion;
                //    self.rowVersion_listeners.forEach(cb => { cb(<GpRequestsContexts>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsContextsBase.prototype, "hash", {
            get: function () {
                return this._hash.value;
            },
            set: function (hash) {
                var self = this;
                self._hash.value = hash;
                //    self.hash_listeners.forEach(cb => { cb(<GpRequestsContexts>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsContextsBase.prototype, "gpRequestsProducersId", {
            get: function () {
                return this._gpRequestsProducersId.value;
            },
            set: function (gpRequestsProducersId) {
                var self = this;
                self._gpRequestsProducersId.value = gpRequestsProducersId;
                //    self.gpRequestsProducersId_listeners.forEach(cb => { cb(<GpRequestsContexts>self); });
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(GpRequestsContextsBase.prototype, "parcelsIssuesId", {
            get: function () {
                return this._parcelsIssuesId.value;
            },
            set: function (parcelsIssuesId) {
                var self = this;
                self._parcelsIssuesId.value = parcelsIssuesId;
                //    self.parcelsIssuesId_listeners.forEach(cb => { cb(<GpRequestsContexts>self); });
            },
            enumerable: true,
            configurable: true
        });
        GpRequestsContextsBase.prototype.gpUploadCollection = function (func) {
            var self = this;
            if (!isVoid(Entities.GpRequestsContexts.gpUploadCollection)) {
                if (self instanceof Entities.GpRequestsContexts) {
                    Entities.GpRequestsContexts.gpUploadCollection(self, func);
                }
            }
        };
        /*
        public removeAllListeners() {
            var self = this;
            self.gpRequestsContextsId_listeners.splice(0);
            self.type_listeners.splice(0);
            self.label_listeners.splice(0);
            self.comment_listeners.splice(0);
            self.geomHexewkb_listeners.splice(0);
            self.referencepoint_listeners.splice(0);
            self.rowVersion_listeners.splice(0);
            self.hash_listeners.splice(0);
            self.gpRequestsProducersId_listeners.splice(0);
            self.parcelsIssuesId_listeners.splice(0);
        }
        */
        GpRequestsContextsBase.fromJSONPartial_actualdecode_private = function (x, deserializedEntities) {
            var key = "GpRequestsContexts:" + x.gpRequestsContextsId;
            var ret = new Entities.GpRequestsContexts(x.gpRequestsContextsId, x.type, x.label, x.comment, x.geomHexewkb, x.referencepoint, x.rowVersion, x.hash, x.gpRequestsProducersId, x.parcelsIssuesId);
            deserializedEntities[key] = ret;
            ret.gpRequestsProducersId = (x.gpRequestsProducersId !== undefined && x.gpRequestsProducersId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.gpRequestsProducersId.$entityName].fromJSONPartial(x.gpRequestsProducersId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined,
                ret.parcelsIssuesId = (x.parcelsIssuesId !== undefined && x.parcelsIssuesId !== null) ? NpTypes.BaseEntity.entitiesFactory[x.parcelsIssuesId.$entityName].fromJSONPartial(x.parcelsIssuesId, deserializedEntities, NpTypes.EntityCachedKind.MANY_TO_ONE) : undefined;
            return ret;
        };
        GpRequestsContextsBase.fromJSONPartial_actualdecode = function (x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        };
        GpRequestsContextsBase.fromJSONPartial = function (x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = "GpRequestsContexts:" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "GpRequestsContextsBase::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                /*
                if (entityKind === NpTypes.EntityCachedKind.MANY_TO_ONE) {
                    key = "GpRequestsContexts:"+x.gpRequestsContextsId;
                    var cachedCopy = <GpRequestsContexts>deserializedEntities[key];
                    if (cachedCopy !== undefined) {
                        ret = cachedCopy;
                    } else {
                        ret = GpRequestsContexts.fromJSONPartial_actualdecode(x, deserializedEntities);
                    }
                } else {
                    ret = GpRequestsContexts.fromJSONPartial_actualdecode(x, deserializedEntities);
                }
                */
                ret = Entities.GpRequestsContexts.fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        };
        GpRequestsContextsBase.fromJSONComplete = function (data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            //var deserializedEntities: { [id: string]: NpTypes.IBaseEntity; } = {};
            var ret = _.map(data, function (x) {
                return Entities.GpRequestsContexts.fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        };
        GpRequestsContextsBase.getEntitiesFromIdsList = function (ctrl, ids, func) {
            var url = "/Niva/rest/GpRequestsContexts/findAllByIds?";
            ctrl.httpPost(url, { "ids": ids }, function (rsp) {
                var dbEntities = Entities.GpRequestsContexts.fromJSONComplete(rsp.data);
                func(dbEntities);
            });
        };
        GpRequestsContextsBase.prototype.toJSON = function () {
            var ret = {};
            ret.gpRequestsContextsId = this.gpRequestsContextsId;
            ret.type = this.type;
            ret.label = this.label;
            ret.comment = this.comment;
            ret.geomHexewkb = this.geomHexewkb;
            ret.referencepoint = this.referencepoint;
            ret.rowVersion = this.rowVersion;
            ret.hash = this.hash;
            ret.gpRequestsProducersId =
                (this.gpRequestsProducersId !== undefined && this.gpRequestsProducersId !== null) ?
                    { gpRequestsProducersId: this.gpRequestsProducersId.gpRequestsProducersId } :
                    (this.gpRequestsProducersId === undefined ? undefined : null);
            ret.parcelsIssuesId =
                (this.parcelsIssuesId !== undefined && this.parcelsIssuesId !== null) ?
                    { parcelsIssuesId: this.parcelsIssuesId.parcelsIssuesId } :
                    (this.parcelsIssuesId === undefined ? undefined : null);
            return ret;
        };
        GpRequestsContextsBase.prototype.updateInstance = function (other) {
            var self = this;
            self.gpRequestsContextsId = other.gpRequestsContextsId;
            self.type = other.type;
            self.label = other.label;
            self.comment = other.comment;
            self.geomHexewkb = other.geomHexewkb;
            self.referencepoint = other.referencepoint;
            self.rowVersion = other.rowVersion;
            self.hash = other.hash;
            self.gpRequestsProducersId = other.gpRequestsProducersId;
            self.parcelsIssuesId = other.parcelsIssuesId;
        };
        GpRequestsContextsBase.Create = function () {
            return new Entities.GpRequestsContexts(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined);
        };
        GpRequestsContextsBase.CreateById = function (gpRequestsContextsId) {
            var ret = Entities.GpRequestsContexts.Create();
            ret.gpRequestsContextsId = gpRequestsContextsId;
            return ret;
        };
        GpRequestsContextsBase.findById_unecrypted = function (gpRequestsContextsId, $scope, $http, errFunc) {
            if (Entities.GpRequestsContexts.cashedEntities[gpRequestsContextsId.toString()] !== undefined)
                return Entities.GpRequestsContexts.cashedEntities[gpRequestsContextsId.toString()];
            var wsPath = "GpRequestsContexts/findByGpRequestsContextsId_unecrypted";
            var url = "/Niva/rest/" + wsPath + "?";
            var paramData = {};
            paramData['gpRequestsContextsId'] = gpRequestsContextsId;
            var ret = Entities.GpRequestsContexts.Create();
            Entities.GpRequestsContexts.cashedEntities[gpRequestsContextsId.toString()] = ret;
            var wsStartTs = (new Date).getTime();
            $http.post(url, paramData, { timeout: $scope.globals.timeoutInMS, cache: false }).
                success(function (response, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                var dbEntities = Entities.GpRequestsContexts.fromJSONComplete(response.data);
                if (dbEntities.length === 1) {
                    ret.updateInstance(dbEntities[0]);
                }
                else {
                    errFunc($scope.globals.getDynamicMessage("EntityById_NotFoundMsg(\"" + gpRequestsContextsId + "\")"));
                }
            }).error(function (data, status, header, config) {
                $scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                errFunc(data);
            });
            return ret;
        };
        GpRequestsContextsBase.prototype.getRowVersion = function () {
            return this.rowVersion;
        };
        //public parcelsIssuesId_listeners: Array<(c:GpRequestsContexts) => void> = [];
        GpRequestsContextsBase.gpUploadCollection = null; //(gpRequestsContexts, list) => { };
        GpRequestsContextsBase.cashedEntities = {};
        return GpRequestsContextsBase;
    })(NpTypes.BaseEntity);
    Entities.GpRequestsContextsBase = GpRequestsContextsBase;
    NpTypes.BaseEntity.entitiesFactory['GpRequestsContexts'] = {
        fromJSONPartial_actualdecode: function (x, deserializedEntities) {
            return Entities.GpRequestsContexts.fromJSONPartial_actualdecode_private(x, deserializedEntities);
        },
        fromJSONPartial: function (x, deserializedEntities, entityKind) {
            return Entities.GpRequestsContexts.fromJSONPartial(x, deserializedEntities, entityKind);
        }
    };
})(Entities || (Entities = {}));
//# sourceMappingURL=GpRequestsContextsBase.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
