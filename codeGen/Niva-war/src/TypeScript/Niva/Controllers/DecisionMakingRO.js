//E8C07693F14ADAB0018CD310888546C5
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/DecisionMakingROBase.ts" />
/// <reference path="../Services/MainService.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var DecisionMakingRO;
    (function (DecisionMakingRO) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(DecisionMakingRO.PageModelBase);
        DecisionMakingRO.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(DecisionMakingRO.PageControllerBase);
        DecisionMakingRO.PageController = PageController;
        g_controllers['DecisionMakingRO.PageController'] = Controllers.DecisionMakingRO.PageController;
        var ModelGrpDema = (function (_super) {
            __extends(ModelGrpDema, _super);
            function ModelGrpDema() {
                _super.apply(this, arguments);
            }
            return ModelGrpDema;
        })(DecisionMakingRO.ModelGrpDemaBase);
        DecisionMakingRO.ModelGrpDema = ModelGrpDema;
        var ControllerGrpDema = (function (_super) {
            __extends(ControllerGrpDema, _super);
            function ControllerGrpDema($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpDema($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpDema;
        })(DecisionMakingRO.ControllerGrpDemaBase);
        DecisionMakingRO.ControllerGrpDema = ControllerGrpDema;
        g_controllers['DecisionMakingRO.ControllerGrpDema'] = Controllers.DecisionMakingRO.ControllerGrpDema;
        var ModelGrpIntegrateddecision = (function (_super) {
            __extends(ModelGrpIntegrateddecision, _super);
            function ModelGrpIntegrateddecision() {
                _super.apply(this, arguments);
            }
            return ModelGrpIntegrateddecision;
        })(DecisionMakingRO.ModelGrpIntegrateddecisionBase);
        DecisionMakingRO.ModelGrpIntegrateddecision = ModelGrpIntegrateddecision;
        var ControllerGrpIntegrateddecision = (function (_super) {
            __extends(ControllerGrpIntegrateddecision, _super);
            function ControllerGrpIntegrateddecision($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpIntegrateddecision($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            ControllerGrpIntegrateddecision.prototype.exportIntegratedDecisionsToCSV_fileName = function () {
                var self = this;
                return "IntegratedParcelDecisions" + "_" + Utils.convertDateToString(new Date(), "yyyy_MM_dd_HHmm") + ".csv";
            };
            ControllerGrpIntegrateddecision.prototype.exportIntegratedDecisionsToCSV = function () {
                var self = this;
                var wsPath = "Integrateddecision/findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision_toCSV";
                var url = "/Niva/rest/" + wsPath + "?";
                var paramData = {};
                paramData['__fields'] = self.getExcelFieldDefinitions();
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.demaId)) {
                    paramData['demaId_demaId'] = self.Parent.demaId;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_decisionCode)) {
                    paramData['fsch_decisionCode'] = self.modelGrpIntegrateddecision.fsch_decisionCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_prodCode)) {
                    paramData['fsch_prodCode'] = self.modelGrpIntegrateddecision.fsch_prodCode;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_Code)) {
                    paramData['fsch_Code'] = self.modelGrpIntegrateddecision.fsch_Code;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.fsch_Parcel_Identifier)) {
                    paramData['fsch_Parcel_Identifier'] = self.modelGrpIntegrateddecision.fsch_Parcel_Identifier;
                }
                NpTypes.AlertMessage.clearAlerts(self.PageModel);
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    var data = response;
                    var blob = new Blob([data], { type: "text/csv;charset=utf-8" });
                    saveAs(blob, self.exportIntegratedDecisionsToCSV_fileName());
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.PageModel, Messages.dynamicMessage(data));
                });
            };
            ControllerGrpIntegrateddecision.prototype.actionPushToCommonsAPI = function (integrateddecision) {
                var self = this;
                // Checking Modifications
                if (self.$scope.globals.isCurrentTransactionDirty) {
                    //Error was detected
                    NpTypes.AlertMessage.clearAlerts(this.$scope.pageModel);
                    NpTypes.AlertMessage.addDanger(this.$scope.pageModel, "Please save before Push");
                    return;
                }
                //Ask Confirmatiom 
                messageBox(self.$scope, self.Plato, "Info", "Do you want to Push Traffic Lights to WP4 CMDS API?", IconKind.QUESTION, [
                    new Tuple2("Yes", function () {
                        NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
                        //call web service
                        var paramData = new Services.MainService_actionPushToCommonsAPI_req();
                        paramData.demaId = integrateddecision.demaId.demaId;
                        Services.MainService.actionPushToCommonsAPI(self.$scope.pageModel.controller, paramData, function (respDataList) {
                            self.updateUI;
                            NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
                            NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, "Push was executed succesfully!");
                            respDataList.forEach(function (item) { return NpTypes.AlertMessage.addWarning(self.$scope.pageModel, item); });
                        });
                    }),
                    new Tuple2("No", function () { })
                ], 1, 1, '18em');
            };
            ControllerGrpIntegrateddecision.prototype.createPclaId_geom4326Map = function (defaultOptions) {
                var self = this;
                defaultOptions.layers = [
                    new NpGeoLayers.OpenStreetTileLayer(true),
                    new NpGeoLayers.SqlVectorLayer({
                        layerId: 'INPMP_ParcelGreen',
                        label: 'Green Parcels',
                        isVisible: true,
                        getQueryUrl: function () { return '/Niva/rest/Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelGreen?'; },
                        getQueryInputs: function (Integrateddecision) { return { globalDema_demaId: (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.globalDema) && !isVoid(self.modelGrpIntegrateddecision.globalDema.demaId)) ? self.modelGrpIntegrateddecision.globalDema.demaId : null }; },
                        fillColor: 'rgba(0, 255, 0, 0.8)',
                        borderColor: 'rgba(0,0,0,1.0)',
                        penWidth: 1,
                        selectable: true,
                        showSpatialSearchFilters: false
                    }),
                    new NpGeoLayers.SqlVectorLayer({
                        layerId: 'INPMP_ParcelYellow',
                        label: 'Yellow Parcels',
                        isVisible: true,
                        getQueryUrl: function () { return '/Niva/rest/Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelYellow?'; },
                        getQueryInputs: function (Integrateddecision) { return { globalDema_demaId: (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.globalDema) && !isVoid(self.modelGrpIntegrateddecision.globalDema.demaId)) ? self.modelGrpIntegrateddecision.globalDema.demaId : null }; },
                        fillColor: 'rgba(255, 255, 0, 0.8)',
                        borderColor: 'rgba(0,0,0,1.0)',
                        penWidth: 1,
                        selectable: true,
                        showSpatialSearchFilters: false
                    }),
                    new NpGeoLayers.SqlVectorLayer({
                        layerId: 'INPMP_ParcelRed',
                        label: 'Red Parcels',
                        isVisible: true,
                        getQueryUrl: function () { return '/Niva/rest/Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelRed?'; },
                        getQueryInputs: function (Integrateddecision) { return { globalDema_demaId: (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.globalDema) && !isVoid(self.modelGrpIntegrateddecision.globalDema.demaId)) ? self.modelGrpIntegrateddecision.globalDema.demaId : null }; },
                        fillColor: 'rgba(255, 0, 0, 0.8)',
                        borderColor: 'rgba(0,0,0,1.0)',
                        penWidth: 1,
                        selectable: true,
                        showSpatialSearchFilters: false
                    }),
                    new NpGeoLayers.SqlVectorLayer({
                        layerId: 'INPMP_ParcelUndefined',
                        label: 'Undefined Parcels',
                        isVisible: true,
                        getQueryUrl: function () { return '/Niva/rest/Integrateddecision/getSqlLayer_DecisionMakingRO_INPMPDMRO_ParcelUndefined?'; },
                        getQueryInputs: function (Integrateddecision) { return { globalDema_demaId: (!isVoid(self) && !isVoid(self.modelGrpIntegrateddecision) && !isVoid(self.modelGrpIntegrateddecision.globalDema) && !isVoid(self.modelGrpIntegrateddecision.globalDema.demaId)) ? self.modelGrpIntegrateddecision.globalDema.demaId : null }; },
                        fillColor: 'rgba(255,255,255, 0.5)',
                        borderColor: 'rgba(0,0,0,0.5)',
                        penWidth: 1,
                        selectable: true,
                        showSpatialSearchFilters: false
                    })
                ];
                defaultOptions.zoomOnEntityChange = true;
                defaultOptions.showCoordinates = true;
                defaultOptions.showRuler = true;
                defaultOptions.centerPoint = [49.843056, 9.901944];
                defaultOptions.zoomLevel = 10;
                defaultOptions.fillColor = "rgba(50, 0,255, 0.1)";
                defaultOptions.borderColor = "blue";
                //  defaultOptions.labelColor?: string;
                defaultOptions.penWidth = 4;
                defaultOptions.opacity = 0.5;
                defaultOptions.layerSelectorWidthInPx = 170;
                defaultOptions.coordinateSystem = "EPSG:4326";
                // mapId?: string;
                defaultOptions.showJumpButton = true;
                defaultOptions.showEditButtons = false;
                defaultOptions.showMeasureButton = false;
                defaultOptions.showLegendButton = false;
                // defaultOptions.getEntityLabel?: (currentEntity: any) => string;
                // defaultOptions.getFeatureLabel?: (feature: ol.Feature, lightweight: boolean) => string; // it should be property of vectory layer
                //  defaultOptions.topologyType?: number; //0|undfined = none, 1=client topology, 2:serverside topology
                //topologyFunc?: (feature: ol.geom.Geometry, resp : ) => void
                defaultOptions.fullScreenOnlyMap = false; //true or false. If true only mapdiv shall be used for full screen control. If false or ommited use map+datagrid
                this.pclaId_geom4326Map = new NpGeo.NpMap(defaultOptions);
            };
            return ControllerGrpIntegrateddecision;
        })(DecisionMakingRO.ControllerGrpIntegrateddecisionBase);
        DecisionMakingRO.ControllerGrpIntegrateddecision = ControllerGrpIntegrateddecision;
        g_controllers['DecisionMakingRO.ControllerGrpIntegrateddecision'] = Controllers.DecisionMakingRO.ControllerGrpIntegrateddecision;
    })(DecisionMakingRO = Controllers.DecisionMakingRO || (Controllers.DecisionMakingRO = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=DecisionMakingRO.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
