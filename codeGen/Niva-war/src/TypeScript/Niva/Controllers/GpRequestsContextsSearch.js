//A2B4FBF5561027824A125803A04F4FC6
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/GpRequestsContextsSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelGpRequestsContextsSearch = (function (_super) {
        __extends(ModelGpRequestsContextsSearch, _super);
        function ModelGpRequestsContextsSearch() {
            _super.apply(this, arguments);
        }
        return ModelGpRequestsContextsSearch;
    })(Controllers.ModelGpRequestsContextsSearchBase);
    Controllers.ModelGpRequestsContextsSearch = ModelGpRequestsContextsSearch;
    var ControllerGpRequestsContextsSearch = (function (_super) {
        __extends(ControllerGpRequestsContextsSearch, _super);
        function ControllerGpRequestsContextsSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelGpRequestsContextsSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerGpRequestsContextsSearch;
    })(Controllers.ControllerGpRequestsContextsSearchBase);
    Controllers.ControllerGpRequestsContextsSearch = ControllerGpRequestsContextsSearch;
    g_controllers['ControllerGpRequestsContextsSearch'] = Controllers.ControllerGpRequestsContextsSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=GpRequestsContextsSearch.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
