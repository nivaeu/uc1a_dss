//D6B4F596CA78BF9F4A3907FC92D4C74C
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelFMISBase.ts" />
/// <reference path="../Services/MainService.ts" />

module Controllers.ParcelFMIS {

    export class PageModel extends PageModelBase {
    }

    export interface IPageScope extends IPageScopeBase {
    }

    export class PageController extends PageControllerBase {

        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new PageModel($scope));
        }

        public getPageChanges(bForDelete: boolean = false): Array<ChangeToCommit> {
            var self = this;
            var pageChanges: Array<ChangeToCommit> = [];

            if (bForDelete) {
            } else {
                pageChanges = pageChanges.concat(self.model.modelGrpFmisDecision.controller.getChangesToCommit());
            }

            var hasParcelClas = pageChanges.some(change => change.entityName === "ParcelClas")
            return pageChanges;
        }


        public onSuccesfullSaveFromButton(response: NpTypes.SaveResponce) {
            var self = this;
            super.onSuccesfullSaveFromButton(response);


            NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);

            //call web service
            var paramData: Services.MainService_updateIntegratedDecisionsNIssues_req = new Services.MainService_updateIntegratedDecisionsNIssues_req();
            paramData.pclaId = self.$scope.pageModel.modelGrpParcelClas.pclaId;
            console.log("Calling Update Integrated Decisions Service...." + self.$scope.pageModel.modelGrpParcelClas.pclaId);

            Services.MainService.updateIntegratedDecisionsNIssues(self.$scope.pageModel.controller, paramData, respDataList => {

                //self.updateUI();

                NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
                NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, "Update was Completed Succesfully.");

                respDataList.forEach(item => NpTypes.AlertMessage.addWarning(self.$scope.pageModel, item));
            }
            );



        }



    }

    g_controllers['ParcelFMIS.PageController'] = Controllers.ParcelFMIS.PageController;



    export class ModelGrpParcelClas extends ModelGrpParcelClasBase {
    }


    export interface IScopeGrpParcelClas extends IScopeGrpParcelClasBase {
    }

    export class ControllerGrpParcelClas extends ControllerGrpParcelClasBase {
        constructor(
            public $scope: IScopeGrpParcelClas,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelGrpParcelClas($scope));
        }
    }
    g_controllers['ParcelFMIS.ControllerGrpParcelClas'] = Controllers.ParcelFMIS.ControllerGrpParcelClas;

    export class ModelGrpParcelsIssues extends ModelGrpParcelsIssuesBase {
    }


    export interface IScopeGrpParcelsIssues extends IScopeGrpParcelsIssuesBase {
    }

    export class ControllerGrpParcelsIssues extends ControllerGrpParcelsIssuesBase {
        constructor(
            public $scope: IScopeGrpParcelsIssues,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelGrpParcelsIssues($scope));
        }
    }
    g_controllers['ParcelFMIS.ControllerGrpParcelsIssues'] = Controllers.ParcelFMIS.ControllerGrpParcelsIssues;

    export class ModelGrpFmisDecision extends ModelGrpFmisDecisionBase {
    }


    export interface IScopeGrpFmisDecision extends IScopeGrpFmisDecisionBase {
    }

    export class ControllerGrpFmisDecision extends ControllerGrpFmisDecisionBase {
        constructor(
            public $scope: IScopeGrpFmisDecision,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelGrpFmisDecision($scope));

        }
        public isCheckMade(fmisDecision: Entities.FmisDecision): boolean {
            var self = this;
            return !fmisDecision.isNew();

        }
    }
    g_controllers['ParcelFMIS.ControllerGrpFmisDecision'] = Controllers.ParcelFMIS.ControllerGrpFmisDecision;

    export class ModelGrpFmisUpload extends ModelGrpFmisUploadBase {
    }


    export interface IScopeGrpFmisUpload extends IScopeGrpFmisUploadBase {
    }

    export class ControllerGrpFmisUpload extends ControllerGrpFmisUploadBase {
        constructor(
            public $scope: IScopeGrpFmisUpload,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelGrpFmisUpload($scope));
        }
    }
    g_controllers['ParcelFMIS.ControllerGrpFmisUpload'] = Controllers.ParcelFMIS.ControllerGrpFmisUpload;

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
