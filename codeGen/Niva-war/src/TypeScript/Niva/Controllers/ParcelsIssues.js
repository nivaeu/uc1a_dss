//C205B88873999D9571E04DF8FC4FFC7B
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelsIssuesBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ParcelsIssues;
    (function (ParcelsIssues) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(ParcelsIssues.PageModelBase);
        ParcelsIssues.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(ParcelsIssues.PageControllerBase);
        ParcelsIssues.PageController = PageController;
        g_controllers['ParcelsIssues.PageController'] = Controllers.ParcelsIssues.PageController;
        var ModelGrpParcelsIssues = (function (_super) {
            __extends(ModelGrpParcelsIssues, _super);
            function ModelGrpParcelsIssues() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcelsIssues;
        })(ParcelsIssues.ModelGrpParcelsIssuesBase);
        ParcelsIssues.ModelGrpParcelsIssues = ModelGrpParcelsIssues;
        var ControllerGrpParcelsIssues = (function (_super) {
            __extends(ControllerGrpParcelsIssues, _super);
            function ControllerGrpParcelsIssues($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcelsIssues($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpParcelsIssues;
        })(ParcelsIssues.ControllerGrpParcelsIssuesBase);
        ParcelsIssues.ControllerGrpParcelsIssues = ControllerGrpParcelsIssues;
        g_controllers['ParcelsIssues.ControllerGrpParcelsIssues'] = Controllers.ParcelsIssues.ControllerGrpParcelsIssues;
        var ModelGrpGpDecision = (function (_super) {
            __extends(ModelGrpGpDecision, _super);
            function ModelGrpGpDecision() {
                _super.apply(this, arguments);
            }
            return ModelGrpGpDecision;
        })(ParcelsIssues.ModelGrpGpDecisionBase);
        ParcelsIssues.ModelGrpGpDecision = ModelGrpGpDecision;
        var ControllerGrpGpDecision = (function (_super) {
            __extends(ControllerGrpGpDecision, _super);
            function ControllerGrpGpDecision($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpGpDecision($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpGpDecision;
        })(ParcelsIssues.ControllerGrpGpDecisionBase);
        ParcelsIssues.ControllerGrpGpDecision = ControllerGrpGpDecision;
        g_controllers['ParcelsIssues.ControllerGrpGpDecision'] = Controllers.ParcelsIssues.ControllerGrpGpDecision;
        var ModelGrpGpRequestsContexts = (function (_super) {
            __extends(ModelGrpGpRequestsContexts, _super);
            function ModelGrpGpRequestsContexts() {
                _super.apply(this, arguments);
            }
            return ModelGrpGpRequestsContexts;
        })(ParcelsIssues.ModelGrpGpRequestsContextsBase);
        ParcelsIssues.ModelGrpGpRequestsContexts = ModelGrpGpRequestsContexts;
        var ControllerGrpGpRequestsContexts = (function (_super) {
            __extends(ControllerGrpGpRequestsContexts, _super);
            function ControllerGrpGpRequestsContexts($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpGpRequestsContexts($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpGpRequestsContexts;
        })(ParcelsIssues.ControllerGrpGpRequestsContextsBase);
        ParcelsIssues.ControllerGrpGpRequestsContexts = ControllerGrpGpRequestsContexts;
        g_controllers['ParcelsIssues.ControllerGrpGpRequestsContexts'] = Controllers.ParcelsIssues.ControllerGrpGpRequestsContexts;
        var ModelGrpGpUpload = (function (_super) {
            __extends(ModelGrpGpUpload, _super);
            function ModelGrpGpUpload() {
                _super.apply(this, arguments);
            }
            return ModelGrpGpUpload;
        })(ParcelsIssues.ModelGrpGpUploadBase);
        ParcelsIssues.ModelGrpGpUpload = ModelGrpGpUpload;
        var ControllerGrpGpUpload = (function (_super) {
            __extends(ControllerGrpGpUpload, _super);
            function ControllerGrpGpUpload($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpGpUpload($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpGpUpload;
        })(ParcelsIssues.ControllerGrpGpUploadBase);
        ParcelsIssues.ControllerGrpGpUpload = ControllerGrpGpUpload;
        g_controllers['ParcelsIssues.ControllerGrpGpUpload'] = Controllers.ParcelsIssues.ControllerGrpGpUpload;
        var ModelGrpParcelsIssuesActivities = (function (_super) {
            __extends(ModelGrpParcelsIssuesActivities, _super);
            function ModelGrpParcelsIssuesActivities() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcelsIssuesActivities;
        })(ParcelsIssues.ModelGrpParcelsIssuesActivitiesBase);
        ParcelsIssues.ModelGrpParcelsIssuesActivities = ModelGrpParcelsIssuesActivities;
        var ControllerGrpParcelsIssuesActivities = (function (_super) {
            __extends(ControllerGrpParcelsIssuesActivities, _super);
            function ControllerGrpParcelsIssuesActivities($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcelsIssuesActivities($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpParcelsIssuesActivities;
        })(ParcelsIssues.ControllerGrpParcelsIssuesActivitiesBase);
        ParcelsIssues.ControllerGrpParcelsIssuesActivities = ControllerGrpParcelsIssuesActivities;
        g_controllers['ParcelsIssues.ControllerGrpParcelsIssuesActivities'] = Controllers.ParcelsIssues.ControllerGrpParcelsIssuesActivities;
    })(ParcelsIssues = Controllers.ParcelsIssues || (Controllers.ParcelsIssues = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ParcelsIssues.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
