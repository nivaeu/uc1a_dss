//E693384896C15F57ACC03790133BCAC8
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/GpRequestsContextsBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var GpRequestsContexts;
    (function (GpRequestsContexts) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(GpRequestsContexts.PageModelBase);
        GpRequestsContexts.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(GpRequestsContexts.PageControllerBase);
        GpRequestsContexts.PageController = PageController;
        g_controllers['GpRequestsContexts.PageController'] = Controllers.GpRequestsContexts.PageController;
        var ModelGrpGpRequestsContexts = (function (_super) {
            __extends(ModelGrpGpRequestsContexts, _super);
            function ModelGrpGpRequestsContexts() {
                _super.apply(this, arguments);
            }
            return ModelGrpGpRequestsContexts;
        })(GpRequestsContexts.ModelGrpGpRequestsContextsBase);
        GpRequestsContexts.ModelGrpGpRequestsContexts = ModelGrpGpRequestsContexts;
        var ControllerGrpGpRequestsContexts = (function (_super) {
            __extends(ControllerGrpGpRequestsContexts, _super);
            function ControllerGrpGpRequestsContexts($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpGpRequestsContexts($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpGpRequestsContexts;
        })(GpRequestsContexts.ControllerGrpGpRequestsContextsBase);
        GpRequestsContexts.ControllerGrpGpRequestsContexts = ControllerGrpGpRequestsContexts;
        g_controllers['GpRequestsContexts.ControllerGrpGpRequestsContexts'] = Controllers.GpRequestsContexts.ControllerGrpGpRequestsContexts;
    })(GpRequestsContexts = Controllers.GpRequestsContexts || (Controllers.GpRequestsContexts = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=GpRequestsContexts.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
