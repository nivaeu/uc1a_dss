//99D0F0E6499CC118B8B4141803CD43AA
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ClassificationBase.ts" />
/// <reference path="../Services/MainService.ts" />

module Controllers.Classification {

    export class PageModel extends PageModelBase {
    }

    export interface IPageScope extends IPageScopeBase {
    }

    export class PageController extends PageControllerBase {

        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new PageModel($scope));
        }
    }

    g_controllers['Classification.PageController'] = Controllers.Classification.PageController;



    export class ModelGrpClas extends ModelGrpClasBase {
    }


    export interface IScopeGrpClas extends IScopeGrpClasBase {
    }

    export class ControllerGrpClas extends ControllerGrpClasBase {
        constructor(
            public $scope: IScopeGrpClas,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelGrpClas($scope));
        }

        public isImportDisabled(classification: Entities.Classification): boolean {
            if (
                (isVoid(classification)) || (classification.filePath === null)
                || (classification.name === null) || (classification.dateTime === null)
                || (classification.clfrId === null) || (classification.fiteId === null)
                || (classification.isNew())
            ) {
                return true;
            }
            return false;
        }

        public isFieldsDisabled(classification: Entities.Classification): boolean {
            if ((!isVoid(classification)) && ((classification.recordtype === 1))) {
                return true;
            }
            return false;
        }

        /**/
        public importAction(classification: Entities.Classification) {

            var self = this;
            console.log("Method: importAction() called");

            // Checking Modifications
            if (self.$scope.globals.isCurrentTransactionDirty) {

                //Error was detected
                NpTypes.AlertMessage.clearAlerts(this.$scope.pageModel);
                NpTypes.AlertMessage.addDanger(this.$scope.pageModel, "Please save before Importing");
                return;
            }

            //Ask Confirmatiom 

            messageBox(self.$scope, self.Plato, "Info", "Do you want to Import?",
                IconKind.QUESTION,
                [
                    new Tuple2("Yes", () => {

                        NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);

                        //call web service
                        var paramData: Services.MainService_importingClassification_req = new Services.MainService_importingClassification_req();
                        paramData.classId = classification.clasId;
                        Services.MainService.importingClassification(self.$scope.pageModel.controller, paramData, respDataList => {

                            classification.isImported = true;
                            self.model.modelGrpParcelClas.controller.updateUI();
                            //self.updateUI();

                            NpTypes.AlertMessage.clearAlerts(self.$scope.pageModel);
                            NpTypes.AlertMessage.addSuccess(self.$scope.pageModel, "Import done succesfully.");

                            respDataList.forEach(item => NpTypes.AlertMessage.addWarning(self.$scope.pageModel, item));
                        });
                    }),
                    new Tuple2("No", () => { })
                ], 1, 1, '18em');

        }

        public isEditClassificationDisabled(classification: Entities.Classification): boolean {
            var self = this;
            if (classification.isImported === null) {
                classification.isImported = false;
            }



            if (classification.isImported === false) {
                return false;
            } else {
                return true;
            }

        }

        public isEditClassificationNotImported():boolean { 
            
            var self=this;
            return !self.modelGrpClas.isImported;
 
        }


    }
    g_controllers['Classification.ControllerGrpClas'] = Controllers.Classification.ControllerGrpClas;

















    export class ModelGrpParcelClas extends ModelGrpParcelClasBase {
    }


    export interface IScopeGrpParcelClas extends IScopeGrpParcelClasBase {
    }

    export class ControllerGrpParcelClas extends ControllerGrpParcelClasBase {
        constructor(
            public $scope: IScopeGrpParcelClas,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker) {
            super($scope, $http, $timeout, Plato, new ModelGrpParcelClas($scope));


        }





        public createGeom4326Map(defaultOptions: NpGeo.NpMapConstructionOptions) {
            defaultOptions.layers = [new NpGeoLayers.OpenStreetTileLayer(true)];
            defaultOptions.zoomOnEntityChange = true;
            defaultOptions.showCoordinates = true;
            defaultOptions.showRuler = true;
            defaultOptions.centerPoint = [49.843056, 9.901944];
            defaultOptions.zoomLevel = 10;
            defaultOptions.fillColor = "rgba(50, 0,255, 0.1)";
            defaultOptions.borderColor = "blue";
            //  defaultOptions.labelColor?: string;
            defaultOptions.penWidth = 4;
            defaultOptions.opacity = 0.5;
            defaultOptions.layerSelectorWidthInPx = 170;
            defaultOptions.coordinateSystem = "EPSG:4326";
            // mapId?: string;
            defaultOptions.showJumpButton = true;
            defaultOptions.showEditButtons = false;
            defaultOptions.showMeasureButton = false;
            defaultOptions.showLegendButton = false;
            // defaultOptions.getEntityLabel?: (currentEntity: any) => string;

            // defaultOptions.getFeatureLabel?: (feature: ol.Feature, lightweight: boolean) => string; // it should be property of vectory layer
            //  defaultOptions.topologyType?: number; //0|undfined = none, 1=client topology, 2:serverside topology
            //topologyFunc?: (feature: ol.geom.Geometry, resp : ) => void

            defaultOptions.fullScreenOnlyMap = false;//true or false. If true only mapdiv shall be used for full screen control. If false or ommited use map+datagrid

            this.geom4326Map = new NpGeo.NpMap(defaultOptions);





        }


    }
    g_controllers['Classification.ControllerGrpParcelClas'] = Controllers.Classification.ControllerGrpParcelClas;

    /*    export class ModelGrpParcelDecision extends ModelGrpParcelDecisionBase {
        }
    */
    /*    export interface IScopeGrpParcelDecision extends IScopeGrpParcelDecisionBase {
        }
    */
    /*    export class ControllerGrpParcelDecision extends ControllerGrpParcelDecisionBase {
            constructor(
                public $scope: IScopeGrpParcelDecision,
                public $http: ng.IHttpService,
                public $timeout: ng.ITimeoutService,
                public Plato:Services.INpDialogMaker)
            {
                super($scope, $http, $timeout, Plato, new ModelGrpParcelDecision($scope) );
            }
        }
        g_controllers['Classification.ControllerGrpParcelDecision'] = Controllers.Classification.ControllerGrpParcelDecision;
    */




    /*    export class ModelGrpDecisionMaking extends ModelGrpDecisionMakingBase {
        }
    */

    /*    export interface IScopeGrpDecisionMaking extends IScopeGrpDecisionMakingBase {
        }
    */
    /*    export class ControllerGrpDecisionMaking extends ControllerGrpDecisionMakingBase {
            constructor(
                public $scope: IScopeGrpDecisionMaking,
                public $http: ng.IHttpService,
                public $timeout: ng.ITimeoutService,
                public Plato:Services.INpDialogMaker)
            {
                super($scope, $http, $timeout, Plato, new ModelGrpDecisionMaking($scope) );
            }
        }
        g_controllers['Classification.ControllerGrpDecisionMaking'] = Controllers.Classification.ControllerGrpDecisionMaking;
    */
    /*    export class ModelGrpParcelDecision1 extends ModelGrpParcelDecision1Base {
        }
    */

    /*    export interface IScopeGrpParcelDecision1 extends IScopeGrpParcelDecision1Base {
        }
    */
    /*    export class ControllerGrpParcelDecision1 extends ControllerGrpParcelDecision1Base {
            constructor(
                public $scope: IScopeGrpParcelDecision1,
                public $http: ng.IHttpService,
                public $timeout: ng.ITimeoutService,
                public Plato:Services.INpDialogMaker)
            {
                super($scope, $http, $timeout, Plato, new ModelGrpParcelDecision1($scope) );
            }
        }
        g_controllers['Classification.ControllerGrpParcelDecision1'] = Controllers.Classification.ControllerGrpParcelDecision1;
    */
    /*    export class ModelGrpStatistic extends ModelGrpStatisticBase {
        }
    */

    /*   export interface IScopeGrpStatistic extends IScopeGrpStatisticBase {
       }
   */
    /*   export class ControllerGrpStatistic extends ControllerGrpStatisticBase {
           constructor(
               public $scope: IScopeGrpStatistic,
               public $http: ng.IHttpService,
               public $timeout: ng.ITimeoutService,
               public Plato:Services.INpDialogMaker)
           {
               super($scope, $http, $timeout, Plato, new ModelGrpStatistic($scope) );
           }
       }
       g_controllers['Classification.ControllerGrpStatistic'] = Controllers.Classification.ControllerGrpStatistic;
    */
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
