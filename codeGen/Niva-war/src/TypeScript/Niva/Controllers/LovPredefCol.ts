//B0318D8B8F3E90006E3DB9705DDB0F00
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovPredefColBase.ts" />

module Controllers {

    export class ModelLovPredefCol extends ModelLovPredefColBase {
    }

    export interface IScopeLovPredefCol extends IScopeLovPredefColBase {
    }

    export class ControllerLovPredefCol extends ControllerLovPredefColBase {
        
        constructor(
            public $scope: IScopeLovPredefCol,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelLovPredefCol($scope));
        }
    }

    g_controllers['ControllerLovPredefCol'] = Controllers.ControllerLovPredefCol;


    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
