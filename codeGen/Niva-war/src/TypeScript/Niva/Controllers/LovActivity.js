//00BC89CB489D1DBA0A260AE034EFF2E8
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovActivityBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovActivity = (function (_super) {
        __extends(ModelLovActivity, _super);
        function ModelLovActivity() {
            _super.apply(this, arguments);
        }
        return ModelLovActivity;
    })(Controllers.ModelLovActivityBase);
    Controllers.ModelLovActivity = ModelLovActivity;
    var ControllerLovActivity = (function (_super) {
        __extends(ControllerLovActivity, _super);
        function ControllerLovActivity($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovActivity($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovActivity;
    })(Controllers.ControllerLovActivityBase);
    Controllers.ControllerLovActivity = ControllerLovActivity;
    g_controllers['ControllerLovActivity'] = Controllers.ControllerLovActivity;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovActivity.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
