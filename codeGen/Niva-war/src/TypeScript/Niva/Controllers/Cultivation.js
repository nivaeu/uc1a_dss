//E2C5A98641EBEDD2CD9CD5A528AED9F6
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/CultivationBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var Cultivation;
    (function (Cultivation) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(Cultivation.PageModelBase);
        Cultivation.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            PageController.prototype.successUpdate = function (fileName, jsonResponse) {
                var self = this;
                self.model.modelGrpCultivation.controller.updateUI();
            };
            PageController.prototype.isDeleteSelectedCropsDisabled = function () {
                // MT - Enables or disables the Delete Selected Crops button according to the existence of checked crops.
                var self = this;
                var nrof_checkedEntities = self.model.modelGrpCultivation.grid.selectedRowsCount();
                var isDisabled = false;
                if (nrof_checkedEntities > 0) {
                    isDisabled = false;
                }
                else {
                    isDisabled = true;
                }
                return isDisabled;
            };
            PageController.prototype.deleteSelectedCrops = function () {
                // MT - Deletes multiple Crop records, selected by checkboxes.
                var self = this;
                var rowIndex = 0;
                var func = function (entList) {
                    //console.log(entList);
                    entList.forEach(function (ent) {
                        //console.log(ent);
                        self.model.modelGrpCultivation.controller.deleteEntity(ent, false, rowIndex, false);
                    });
                    self.model.modelGrpCultivation.controller.checkedEntities = {};
                    self.model.modelGrpCultivation.controller.updateUI();
                };
                self.model.modelGrpCultivation.controller.getCheckedEntities(func);
            };
            return PageController;
        })(Cultivation.PageControllerBase);
        Cultivation.PageController = PageController;
        g_controllers['Cultivation.PageController'] = Controllers.Cultivation.PageController;
        var ModelGrpCultivation = (function (_super) {
            __extends(ModelGrpCultivation, _super);
            function ModelGrpCultivation() {
                _super.apply(this, arguments);
            }
            return ModelGrpCultivation;
        })(Cultivation.ModelGrpCultivationBase);
        Cultivation.ModelGrpCultivation = ModelGrpCultivation;
        var ControllerGrpCultivation = (function (_super) {
            __extends(ControllerGrpCultivation, _super);
            function ControllerGrpCultivation($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpCultivation($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpCultivation;
        })(Cultivation.ControllerGrpCultivationBase);
        Cultivation.ControllerGrpCultivation = ControllerGrpCultivation;
        g_controllers['Cultivation.ControllerGrpCultivation'] = Controllers.Cultivation.ControllerGrpCultivation;
    })(Cultivation = Controllers.Cultivation || (Controllers.Cultivation = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=Cultivation.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
