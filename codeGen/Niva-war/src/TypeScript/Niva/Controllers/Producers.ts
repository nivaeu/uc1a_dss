//9C72B560B20DE2FD236D2F0A270AD3B8
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ProducersBase.ts" />

module Controllers.Producers {

    export class PageModel extends PageModelBase {
    }

    export interface IPageScope extends IPageScopeBase {
    }

    export class PageController extends PageControllerBase {
        
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new PageModel($scope));
        }
    }

    g_controllers['Producers.PageController'] = Controllers.Producers.PageController;



    export class ModelGrpProducers extends ModelGrpProducersBase {
    }


    export interface IScopeGrpProducers extends IScopeGrpProducersBase {
    }

    export class ControllerGrpProducers extends ControllerGrpProducersBase {
        constructor(
            public $scope: IScopeGrpProducers,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpProducers($scope) );
        }
    }
    g_controllers['Producers.ControllerGrpProducers'] = Controllers.Producers.ControllerGrpProducers;
    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
