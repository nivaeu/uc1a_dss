//58257965629C2950E2BF5DE52FBBE151
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovAgencyBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovAgency = (function (_super) {
        __extends(ModelLovAgency, _super);
        function ModelLovAgency() {
            _super.apply(this, arguments);
        }
        return ModelLovAgency;
    })(Controllers.ModelLovAgencyBase);
    Controllers.ModelLovAgency = ModelLovAgency;
    var ControllerLovAgency = (function (_super) {
        __extends(ControllerLovAgency, _super);
        function ControllerLovAgency($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovAgency($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovAgency;
    })(Controllers.ControllerLovAgencyBase);
    Controllers.ControllerLovAgency = ControllerLovAgency;
    g_controllers['ControllerLovAgency'] = Controllers.ControllerLovAgency;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovAgency.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
