//C72B2B6A53F619E076F2D32724457888
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovParcelClasBase.ts" />

module Controllers {

    export class ModelLovParcelClas extends ModelLovParcelClasBase {
    }

    export interface IScopeLovParcelClas extends IScopeLovParcelClasBase {
    }

    export class ControllerLovParcelClas extends ControllerLovParcelClasBase {
        
        constructor(
            public $scope: IScopeLovParcelClas,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelLovParcelClas($scope));
        }
    }

    g_controllers['ControllerLovParcelClas'] = Controllers.ControllerLovParcelClas;


    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
