//C72B2B6A53F619E076F2D32724457888
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovParcelClasBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovParcelClas = (function (_super) {
        __extends(ModelLovParcelClas, _super);
        function ModelLovParcelClas() {
            _super.apply(this, arguments);
        }
        return ModelLovParcelClas;
    })(Controllers.ModelLovParcelClasBase);
    Controllers.ModelLovParcelClas = ModelLovParcelClas;
    var ControllerLovParcelClas = (function (_super) {
        __extends(ControllerLovParcelClas, _super);
        function ControllerLovParcelClas($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovParcelClas($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovParcelClas;
    })(Controllers.ControllerLovParcelClasBase);
    Controllers.ControllerLovParcelClas = ControllerLovParcelClas;
    g_controllers['ControllerLovParcelClas'] = Controllers.ControllerLovParcelClas;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovParcelClas.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
