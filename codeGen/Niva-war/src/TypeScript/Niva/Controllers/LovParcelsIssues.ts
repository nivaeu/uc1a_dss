//D671BB7F1359250D9349E9ACD5BA1455
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovParcelsIssuesBase.ts" />

module Controllers {

    export class ModelLovParcelsIssues extends ModelLovParcelsIssuesBase {
    }

    export interface IScopeLovParcelsIssues extends IScopeLovParcelsIssuesBase {
    }

    export class ControllerLovParcelsIssues extends ControllerLovParcelsIssuesBase {
        
        constructor(
            public $scope: IScopeLovParcelsIssues,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelLovParcelsIssues($scope));
        }
    }

    g_controllers['ControllerLovParcelsIssues'] = Controllers.ControllerLovParcelsIssues;


    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
