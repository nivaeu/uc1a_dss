//8207BE9FDE4B08A10026662CE691B432
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovEcCultBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovEcCult = (function (_super) {
        __extends(ModelLovEcCult, _super);
        function ModelLovEcCult() {
            _super.apply(this, arguments);
        }
        return ModelLovEcCult;
    })(Controllers.ModelLovEcCultBase);
    Controllers.ModelLovEcCult = ModelLovEcCult;
    var ControllerLovEcCult = (function (_super) {
        __extends(ControllerLovEcCult, _super);
        function ControllerLovEcCult($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovEcCult($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovEcCult;
    })(Controllers.ControllerLovEcCultBase);
    Controllers.ControllerLovEcCult = ControllerLovEcCult;
    g_controllers['ControllerLovEcCult'] = Controllers.ControllerLovEcCult;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovEcCult.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
