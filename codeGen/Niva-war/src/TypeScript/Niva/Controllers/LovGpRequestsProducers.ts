//C2EB80AA0707F54FD8A4501C61A86151
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovGpRequestsProducersBase.ts" />

module Controllers {

    export class ModelLovGpRequestsProducers extends ModelLovGpRequestsProducersBase {
    }

    export interface IScopeLovGpRequestsProducers extends IScopeLovGpRequestsProducersBase {
    }

    export class ControllerLovGpRequestsProducers extends ControllerLovGpRequestsProducersBase {
        
        constructor(
            public $scope: IScopeLovGpRequestsProducers,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelLovGpRequestsProducers($scope));
        }
    }

    g_controllers['ControllerLovGpRequestsProducers'] = Controllers.ControllerLovGpRequestsProducers;


    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
