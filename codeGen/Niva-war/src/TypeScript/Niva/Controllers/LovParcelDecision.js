//1BE93802D94C25454A04DB4F145E174E
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovParcelDecisionBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovParcelDecision = (function (_super) {
        __extends(ModelLovParcelDecision, _super);
        function ModelLovParcelDecision() {
            _super.apply(this, arguments);
        }
        return ModelLovParcelDecision;
    })(Controllers.ModelLovParcelDecisionBase);
    Controllers.ModelLovParcelDecision = ModelLovParcelDecision;
    var ControllerLovParcelDecision = (function (_super) {
        __extends(ControllerLovParcelDecision, _super);
        function ControllerLovParcelDecision($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovParcelDecision($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovParcelDecision;
    })(Controllers.ControllerLovParcelDecisionBase);
    Controllers.ControllerLovParcelDecision = ControllerLovParcelDecision;
    g_controllers['ControllerLovParcelDecision'] = Controllers.ControllerLovParcelDecision;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovParcelDecision.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
