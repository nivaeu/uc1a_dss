//5D4FB23F27C2E6B0433E26B4ED5999AD
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovParcelsIssuesActivitiesBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovParcelsIssuesActivities = (function (_super) {
        __extends(ModelLovParcelsIssuesActivities, _super);
        function ModelLovParcelsIssuesActivities() {
            _super.apply(this, arguments);
        }
        return ModelLovParcelsIssuesActivities;
    })(Controllers.ModelLovParcelsIssuesActivitiesBase);
    Controllers.ModelLovParcelsIssuesActivities = ModelLovParcelsIssuesActivities;
    var ControllerLovParcelsIssuesActivities = (function (_super) {
        __extends(ControllerLovParcelsIssuesActivities, _super);
        function ControllerLovParcelsIssuesActivities($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovParcelsIssuesActivities($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovParcelsIssuesActivities;
    })(Controllers.ControllerLovParcelsIssuesActivitiesBase);
    Controllers.ControllerLovParcelsIssuesActivities = ControllerLovParcelsIssuesActivities;
    g_controllers['ControllerLovParcelsIssuesActivities'] = Controllers.ControllerLovParcelsIssuesActivities;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovParcelsIssuesActivities.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
