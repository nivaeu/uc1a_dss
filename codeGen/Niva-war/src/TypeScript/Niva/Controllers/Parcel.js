//55445C5E09C70FF47E662785181C0B92
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var Parcel;
    (function (Parcel) {
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel() {
                _super.apply(this, arguments);
            }
            return PageModel;
        })(Parcel.PageModelBase);
        Parcel.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return PageController;
        })(Parcel.PageControllerBase);
        Parcel.PageController = PageController;
        g_controllers['Parcel.PageController'] = Controllers.Parcel.PageController;
        var ModelGrpParcel = (function (_super) {
            __extends(ModelGrpParcel, _super);
            function ModelGrpParcel() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcel;
        })(Parcel.ModelGrpParcelBase);
        Parcel.ModelGrpParcel = ModelGrpParcel;
        var ControllerGrpParcel = (function (_super) {
            __extends(ControllerGrpParcel, _super);
            function ControllerGrpParcel($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpParcel;
        })(Parcel.ControllerGrpParcelBase);
        Parcel.ControllerGrpParcel = ControllerGrpParcel;
        g_controllers['Parcel.ControllerGrpParcel'] = Controllers.Parcel.ControllerGrpParcel;
        var ModelGrpGeotagphoto = (function (_super) {
            __extends(ModelGrpGeotagphoto, _super);
            function ModelGrpGeotagphoto() {
                _super.apply(this, arguments);
            }
            return ModelGrpGeotagphoto;
        })(Parcel.ModelGrpGeotagphotoBase);
        Parcel.ModelGrpGeotagphoto = ModelGrpGeotagphoto;
        var ControllerGrpGeotagphoto = (function (_super) {
            __extends(ControllerGrpGeotagphoto, _super);
            function ControllerGrpGeotagphoto($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpGeotagphoto($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpGeotagphoto;
        })(Parcel.ControllerGrpGeotagphotoBase);
        Parcel.ControllerGrpGeotagphoto = ControllerGrpGeotagphoto;
        g_controllers['Parcel.ControllerGrpGeotagphoto'] = Controllers.Parcel.ControllerGrpGeotagphoto;
        var ModelGrpParcActivity = (function (_super) {
            __extends(ModelGrpParcActivity, _super);
            function ModelGrpParcActivity() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcActivity;
        })(Parcel.ModelGrpParcActivityBase);
        Parcel.ModelGrpParcActivity = ModelGrpParcActivity;
        var ControllerGrpParcActivity = (function (_super) {
            __extends(ControllerGrpParcActivity, _super);
            function ControllerGrpParcActivity($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcActivity($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpParcActivity;
        })(Parcel.ControllerGrpParcActivityBase);
        Parcel.ControllerGrpParcActivity = ControllerGrpParcActivity;
        g_controllers['Parcel.ControllerGrpParcActivity'] = Controllers.Parcel.ControllerGrpParcActivity;
        var ModelGrpParcelClas = (function (_super) {
            __extends(ModelGrpParcelClas, _super);
            function ModelGrpParcelClas() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcelClas;
        })(Parcel.ModelGrpParcelClasBase);
        Parcel.ModelGrpParcelClas = ModelGrpParcelClas;
        var ControllerGrpParcelClas = (function (_super) {
            __extends(ControllerGrpParcelClas, _super);
            function ControllerGrpParcelClas($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcelClas($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpParcelClas;
        })(Parcel.ControllerGrpParcelClasBase);
        Parcel.ControllerGrpParcelClas = ControllerGrpParcelClas;
        g_controllers['Parcel.ControllerGrpParcelClas'] = Controllers.Parcel.ControllerGrpParcelClas;
        var ModelGrpParcelDecision = (function (_super) {
            __extends(ModelGrpParcelDecision, _super);
            function ModelGrpParcelDecision() {
                _super.apply(this, arguments);
            }
            return ModelGrpParcelDecision;
        })(Parcel.ModelGrpParcelDecisionBase);
        Parcel.ModelGrpParcelDecision = ModelGrpParcelDecision;
        var ControllerGrpParcelDecision = (function (_super) {
            __extends(ControllerGrpParcelDecision, _super);
            function ControllerGrpParcelDecision($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpParcelDecision($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
            }
            return ControllerGrpParcelDecision;
        })(Parcel.ControllerGrpParcelDecisionBase);
        Parcel.ControllerGrpParcelDecision = ControllerGrpParcelDecision;
        g_controllers['Parcel.ControllerGrpParcelDecision'] = Controllers.Parcel.ControllerGrpParcelDecision;
    })(Parcel = Controllers.Parcel || (Controllers.Parcel = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=Parcel.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
