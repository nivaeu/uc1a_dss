//8C6AFACF61D8EDC6C0A120CA3CFEFC5E
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/IntegrateddecisionBase.ts" />

module Controllers.Integrateddecision {

    export class PageModel extends PageModelBase {
    }

    export interface IPageScope extends IPageScopeBase {
    }

    export class PageController extends PageControllerBase {
        
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new PageModel($scope));
        }
    }

    g_controllers['Integrateddecision.PageController'] = Controllers.Integrateddecision.PageController;



    export class ModelGrpIntegrateddecision extends ModelGrpIntegrateddecisionBase {
    }


    export interface IScopeGrpIntegrateddecision extends IScopeGrpIntegrateddecisionBase {
    }

    export class ControllerGrpIntegrateddecision extends ControllerGrpIntegrateddecisionBase {
        constructor(
            public $scope: IScopeGrpIntegrateddecision,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpIntegrateddecision($scope) );
        }
    }
    g_controllers['Integrateddecision.ControllerGrpIntegrateddecision'] = Controllers.Integrateddecision.ControllerGrpIntegrateddecision;
    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
