//6959DBA8D89023075EA00CB568731DC4
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelFMISSearchBase.ts" />

module Controllers {

    export class ModelParcelFMISSearch extends ModelParcelFMISSearchBase {
    }

    export interface IScopeParcelFMISSearch extends IScopeParcelFMISSearchBase {
    }

    export class ControllerParcelFMISSearch extends ControllerParcelFMISSearchBase {
        
        constructor(
            public $scope: IScopeParcelFMISSearch,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelParcelFMISSearch($scope));
        }
    }

    g_controllers['ControllerParcelFMISSearch'] = Controllers.ControllerParcelFMISSearch;


    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
