//6D9F67FDE2B44F5F983C67AEA8FF1DE9
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/AgrisnapUsersProducersSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelAgrisnapUsersProducersSearch = (function (_super) {
        __extends(ModelAgrisnapUsersProducersSearch, _super);
        function ModelAgrisnapUsersProducersSearch() {
            _super.apply(this, arguments);
        }
        return ModelAgrisnapUsersProducersSearch;
    })(Controllers.ModelAgrisnapUsersProducersSearchBase);
    Controllers.ModelAgrisnapUsersProducersSearch = ModelAgrisnapUsersProducersSearch;
    var ControllerAgrisnapUsersProducersSearch = (function (_super) {
        __extends(ControllerAgrisnapUsersProducersSearch, _super);
        function ControllerAgrisnapUsersProducersSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelAgrisnapUsersProducersSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerAgrisnapUsersProducersSearch;
    })(Controllers.ControllerAgrisnapUsersProducersSearchBase);
    Controllers.ControllerAgrisnapUsersProducersSearch = ControllerAgrisnapUsersProducersSearch;
    g_controllers['ControllerAgrisnapUsersProducersSearch'] = Controllers.ControllerAgrisnapUsersProducersSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=AgrisnapUsersProducersSearch.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
