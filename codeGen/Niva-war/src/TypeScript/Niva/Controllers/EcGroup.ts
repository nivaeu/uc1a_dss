//F316B70AE21AD3C836AEA43557A277AB
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/EcGroupBase.ts" />

module Controllers.EcGroup {

    export class PageModel extends PageModelBase {
    }

    export interface IPageScope extends IPageScopeBase {
    }

    export class PageController extends PageControllerBase {
        
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new PageModel($scope));
        }
    }

    g_controllers['EcGroup.PageController'] = Controllers.EcGroup.PageController;



    export class ModelGrpEcGroup extends ModelGrpEcGroupBase {
    }


    export interface IScopeGrpEcGroup extends IScopeGrpEcGroupBase {
    }

    export class ControllerGrpEcGroup extends ControllerGrpEcGroupBase {
        constructor(
            public $scope: IScopeGrpEcGroup,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpEcGroup($scope) );
        }

        public isEcGroupDisabled(): boolean {

            return this.Current.recordtype !== 0;

        }


        public isCropLevelDisabled(ecGroup: Entities.EcGroup): boolean {
            return (isVoid(ecGroup)) || (!ecGroup.isNew())
        }


        // The three methods bellow controls which of the three groups (for Crop Type or Land Cover or Super Class) will be visible

        // show groups bellow  when crop level is Crop Type
        public isEcGroupChildInv(): boolean {
            if (this.Current.cropLevel === 0) {
                return false;
            }
            return true;
        }


        // show groups bellow when crop level is Land Cover
        public isEcGroupCoverChildInv(): boolean {
            if (this.Current.cropLevel === 1) {
                return false;
            }
            return true;
        }

        // show groups bellow when crop level is  Crop -> Land Cover
        public isEcGroupSuperChildInv(): boolean {
            if (this.Current.cropLevel === 2) {
                return false;
            }
            return true;
        }


    }
    g_controllers['EcGroup.ControllerGrpEcGroup'] = Controllers.EcGroup.ControllerGrpEcGroup;

    export class ModelGrpEcCult extends ModelGrpEcCultBase {
    }


    export interface IScopeGrpEcCult extends IScopeGrpEcCultBase {
    }

    export class ControllerGrpEcCult extends ControllerGrpEcCultBase {
        constructor(
            public $scope: IScopeGrpEcCult,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpEcCult($scope) );
        }
    }
    g_controllers['EcGroup.ControllerGrpEcCult'] = Controllers.EcGroup.ControllerGrpEcCult;

    export class ModelGrpEcCultDetail extends ModelGrpEcCultDetailBase {
    }


    export interface IScopeGrpEcCultDetail extends IScopeGrpEcCultDetailBase {
    }

    export class ControllerGrpEcCultDetail extends ControllerGrpEcCultDetailBase {
        constructor(
            public $scope: IScopeGrpEcCultDetail,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpEcCultDetail($scope) );
        }
    }
    g_controllers['EcGroup.ControllerGrpEcCultDetail'] = Controllers.EcGroup.ControllerGrpEcCultDetail;

    export class ModelEcCultCover extends ModelEcCultCoverBase {
    }


    export interface IScopeEcCultCover extends IScopeEcCultCoverBase {
    }

    export class ControllerEcCultCover extends ControllerEcCultCoverBase {
        constructor(
            public $scope: IScopeEcCultCover,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelEcCultCover($scope) );
        }
    }
    g_controllers['EcGroup.ControllerEcCultCover'] = Controllers.EcGroup.ControllerEcCultCover;

    export class ModelEcCultDetailCover extends ModelEcCultDetailCoverBase {
    }


    export interface IScopeEcCultDetailCover extends IScopeEcCultDetailCoverBase {
    }

    export class ControllerEcCultDetailCover extends ControllerEcCultDetailCoverBase {
        constructor(
            public $scope: IScopeEcCultDetailCover,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelEcCultDetailCover($scope) );
        }
    }
    g_controllers['EcGroup.ControllerEcCultDetailCover'] = Controllers.EcGroup.ControllerEcCultDetailCover;

    export class ModelEcCultSuper extends ModelEcCultSuperBase {
    }


    export interface IScopeEcCultSuper extends IScopeEcCultSuperBase {
    }

    export class ControllerEcCultSuper extends ControllerEcCultSuperBase {
        constructor(
            public $scope: IScopeEcCultSuper,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelEcCultSuper($scope) );
        }
    }
    g_controllers['EcGroup.ControllerEcCultSuper'] = Controllers.EcGroup.ControllerEcCultSuper;

    export class ModelEcCultDetailSuper extends ModelEcCultDetailSuperBase {
    }


    export interface IScopeEcCultDetailSuper extends IScopeEcCultDetailSuperBase {
    }

    export class ControllerEcCultDetailSuper extends ControllerEcCultDetailSuperBase {
        constructor(
            public $scope: IScopeEcCultDetailSuper,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelEcCultDetailSuper($scope) );
        }
    }
    g_controllers['EcGroup.ControllerEcCultDetailSuper'] = Controllers.EcGroup.ControllerEcCultDetailSuper;
    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
