//A9C1F45377925F3FF0B5F52E8CCD6DEC
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/GpRequestsProducersSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelGpRequestsProducersSearch = (function (_super) {
        __extends(ModelGpRequestsProducersSearch, _super);
        function ModelGpRequestsProducersSearch() {
            _super.apply(this, arguments);
        }
        return ModelGpRequestsProducersSearch;
    })(Controllers.ModelGpRequestsProducersSearchBase);
    Controllers.ModelGpRequestsProducersSearch = ModelGpRequestsProducersSearch;
    var ControllerGpRequestsProducersSearch = (function (_super) {
        __extends(ControllerGpRequestsProducersSearch, _super);
        function ControllerGpRequestsProducersSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelGpRequestsProducersSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerGpRequestsProducersSearch;
    })(Controllers.ControllerGpRequestsProducersSearchBase);
    Controllers.ControllerGpRequestsProducersSearch = ControllerGpRequestsProducersSearch;
    g_controllers['ControllerGpRequestsProducersSearch'] = Controllers.ControllerGpRequestsProducersSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=GpRequestsProducersSearch.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
