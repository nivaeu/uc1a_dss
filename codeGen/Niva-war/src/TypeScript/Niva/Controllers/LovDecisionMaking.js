//E2DEC7B5D5EB1B50DAC472DE03D72A2E
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovDecisionMakingBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovDecisionMaking = (function (_super) {
        __extends(ModelLovDecisionMaking, _super);
        function ModelLovDecisionMaking() {
            _super.apply(this, arguments);
        }
        return ModelLovDecisionMaking;
    })(Controllers.ModelLovDecisionMakingBase);
    Controllers.ModelLovDecisionMaking = ModelLovDecisionMaking;
    var ControllerLovDecisionMaking = (function (_super) {
        __extends(ControllerLovDecisionMaking, _super);
        function ControllerLovDecisionMaking($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovDecisionMaking($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovDecisionMaking;
    })(Controllers.ControllerLovDecisionMakingBase);
    Controllers.ControllerLovDecisionMaking = ControllerLovDecisionMaking;
    g_controllers['ControllerLovDecisionMaking'] = Controllers.ControllerLovDecisionMaking;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovDecisionMaking.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
