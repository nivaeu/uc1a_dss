//301E24331D280989AB1DDB6FE497B85D
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/GpUploadSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelGpUploadSearch = (function (_super) {
        __extends(ModelGpUploadSearch, _super);
        function ModelGpUploadSearch() {
            _super.apply(this, arguments);
        }
        return ModelGpUploadSearch;
    })(Controllers.ModelGpUploadSearchBase);
    Controllers.ModelGpUploadSearch = ModelGpUploadSearch;
    var ControllerGpUploadSearch = (function (_super) {
        __extends(ControllerGpUploadSearch, _super);
        function ControllerGpUploadSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelGpUploadSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerGpUploadSearch;
    })(Controllers.ControllerGpUploadSearchBase);
    Controllers.ControllerGpUploadSearch = ControllerGpUploadSearch;
    g_controllers['ControllerGpUploadSearch'] = Controllers.ControllerGpUploadSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=GpUploadSearch.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
