//B1CA32A3DA7DC612DECF2FA90DF2453D
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovCultivationBase.ts" />

module Controllers {

    export class ModelLovCultivation extends ModelLovCultivationBase {
    }

    export interface IScopeLovCultivation extends IScopeLovCultivationBase {
    }

    export class ControllerLovCultivation extends ControllerLovCultivationBase {
        
        constructor(
            public $scope: IScopeLovCultivation,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelLovCultivation($scope));
        }
    }

    g_controllers['ControllerLovCultivation'] = Controllers.ControllerLovCultivation;


    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
