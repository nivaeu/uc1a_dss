//85A7A104974C01CD5EA26826A63D9F99
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelGPSearchBase.ts" />

module Controllers {

    export class ModelParcelGPSearch extends ModelParcelGPSearchBase {
    }

    export interface IScopeParcelGPSearch extends IScopeParcelGPSearchBase {
    }

    export class ControllerParcelGPSearch extends ControllerParcelGPSearchBase {
        
        constructor(
            public $scope: IScopeParcelGPSearch,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelParcelGPSearch($scope));
        }
    }

    g_controllers['ControllerParcelGPSearch'] = Controllers.ControllerParcelGPSearch;


    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
