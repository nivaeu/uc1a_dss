//29B68864B7D7F07476A1DBE0BC5E59DA
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/ParcelDecisionSearchBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelParcelDecisionSearch = (function (_super) {
        __extends(ModelParcelDecisionSearch, _super);
        function ModelParcelDecisionSearch() {
            _super.apply(this, arguments);
        }
        return ModelParcelDecisionSearch;
    })(Controllers.ModelParcelDecisionSearchBase);
    Controllers.ModelParcelDecisionSearch = ModelParcelDecisionSearch;
    var ControllerParcelDecisionSearch = (function (_super) {
        __extends(ControllerParcelDecisionSearch, _super);
        function ControllerParcelDecisionSearch($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelParcelDecisionSearch($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerParcelDecisionSearch;
    })(Controllers.ControllerParcelDecisionSearchBase);
    Controllers.ControllerParcelDecisionSearch = ControllerParcelDecisionSearch;
    g_controllers['ControllerParcelDecisionSearch'] = Controllers.ControllerParcelDecisionSearch;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=ParcelDecisionSearch.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
