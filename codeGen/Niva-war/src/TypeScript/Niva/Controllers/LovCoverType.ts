//96CB564F9A66B8C8236FE26F5902F7D9
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovCoverTypeBase.ts" />

module Controllers {

    export class ModelLovCoverType extends ModelLovCoverTypeBase {
    }

    export interface IScopeLovCoverType extends IScopeLovCoverTypeBase {
    }

    export class ControllerLovCoverType extends ControllerLovCoverTypeBase {
        
        constructor(
            public $scope: IScopeLovCoverType,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelLovCoverType($scope));
        }
    }

    g_controllers['ControllerLovCoverType'] = Controllers.ControllerLovCoverType;


    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
