//659AC2115BD34666B98B854E5B01FEBE
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovEcGroupBase.ts" />

module Controllers {

    export class ModelLovEcGroup extends ModelLovEcGroupBase {
    }

    export interface IScopeLovEcGroup extends IScopeLovEcGroupBase {
    }

    export class ControllerLovEcGroup extends ControllerLovEcGroupBase {
        
        constructor(
            public $scope: IScopeLovEcGroup,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelLovEcGroup($scope));
        }
    }

    g_controllers['ControllerLovEcGroup'] = Controllers.ControllerLovEcGroup;


    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
