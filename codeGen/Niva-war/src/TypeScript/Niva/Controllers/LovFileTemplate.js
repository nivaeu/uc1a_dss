//931F954669BB6A042C0EDB85B9574AC7
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovFileTemplateBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovFileTemplate = (function (_super) {
        __extends(ModelLovFileTemplate, _super);
        function ModelLovFileTemplate() {
            _super.apply(this, arguments);
        }
        return ModelLovFileTemplate;
    })(Controllers.ModelLovFileTemplateBase);
    Controllers.ModelLovFileTemplate = ModelLovFileTemplate;
    var ControllerLovFileTemplate = (function (_super) {
        __extends(ControllerLovFileTemplate, _super);
        function ControllerLovFileTemplate($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovFileTemplate($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovFileTemplate;
    })(Controllers.ControllerLovFileTemplateBase);
    Controllers.ControllerLovFileTemplate = ControllerLovFileTemplate;
    g_controllers['ControllerLovFileTemplate'] = Controllers.ControllerLovFileTemplate;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovFileTemplate.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
