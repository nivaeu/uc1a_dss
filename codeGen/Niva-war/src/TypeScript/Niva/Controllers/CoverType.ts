//055EEC7E6C19214787645A24A71C73EF
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/CoverTypeBase.ts" />

module Controllers.CoverType {

    export class PageModel extends PageModelBase {
    }

    export interface IPageScope extends IPageScopeBase {
    }

    export class PageController extends PageControllerBase {
        
        constructor(
            public $scope: IPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new PageModel($scope));
        }
        public successUpdate(fileName: string, jsonResponse: any): void {
            var self = this;

            self.model.modelGrpCoverType.controller.updateUI();
        }
    }

    g_controllers['CoverType.PageController'] = Controllers.CoverType.PageController;



    export class ModelGrpCoverType extends ModelGrpCoverTypeBase {
    }


    export interface IScopeGrpCoverType extends IScopeGrpCoverTypeBase {
    }

    export class ControllerGrpCoverType extends ControllerGrpCoverTypeBase {
        constructor(
            public $scope: IScopeGrpCoverType,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato:Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new ModelGrpCoverType($scope) );
        }
    }
    g_controllers['CoverType.ControllerGrpCoverType'] = Controllers.CoverType.ControllerGrpCoverType;
    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
