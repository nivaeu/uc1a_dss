//C2EB80AA0707F54FD8A4501C61A86151
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovGpRequestsProducersBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovGpRequestsProducers = (function (_super) {
        __extends(ModelLovGpRequestsProducers, _super);
        function ModelLovGpRequestsProducers() {
            _super.apply(this, arguments);
        }
        return ModelLovGpRequestsProducers;
    })(Controllers.ModelLovGpRequestsProducersBase);
    Controllers.ModelLovGpRequestsProducers = ModelLovGpRequestsProducers;
    var ControllerLovGpRequestsProducers = (function (_super) {
        __extends(ControllerLovGpRequestsProducers, _super);
        function ControllerLovGpRequestsProducers($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovGpRequestsProducers($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovGpRequestsProducers;
    })(Controllers.ControllerLovGpRequestsProducersBase);
    Controllers.ControllerLovGpRequestsProducers = ControllerLovGpRequestsProducers;
    g_controllers['ControllerLovGpRequestsProducers'] = Controllers.ControllerLovGpRequestsProducers;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovGpRequestsProducers.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
