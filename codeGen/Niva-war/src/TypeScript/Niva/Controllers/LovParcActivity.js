//5B0E926FBB328D8805131D71F3B36E02
//Εxtended classes
/// <reference path="../../RTL/services.ts" />
// /// <reference path="../common.ts" />
/// <reference path="../version.ts" />
/// <reference path="../ControllersBase/LovParcActivityBase.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ModelLovParcActivity = (function (_super) {
        __extends(ModelLovParcActivity, _super);
        function ModelLovParcActivity() {
            _super.apply(this, arguments);
        }
        return ModelLovParcActivity;
    })(Controllers.ModelLovParcActivityBase);
    Controllers.ModelLovParcActivity = ModelLovParcActivity;
    var ControllerLovParcActivity = (function (_super) {
        __extends(ControllerLovParcActivity, _super);
        function ControllerLovParcActivity($scope, $http, $timeout, Plato) {
            _super.call(this, $scope, $http, $timeout, Plato, new ModelLovParcActivity($scope));
            this.$scope = $scope;
            this.$http = $http;
            this.$timeout = $timeout;
            this.Plato = Plato;
        }
        return ControllerLovParcActivity;
    })(Controllers.ControllerLovParcActivityBase);
    Controllers.ControllerLovParcActivity = ControllerLovParcActivity;
    g_controllers['ControllerLovParcActivity'] = Controllers.ControllerLovParcActivity;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=LovParcActivity.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
