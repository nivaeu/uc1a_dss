#!/usr/bin/env python
import os
import re
import sys
import hashlib
from sys import stdout

# Colored message ANSI constants
g_red = chr(27) + "[31m" if stdout.isatty() else ""
g_cyan = chr(27) + "[36m" if stdout.isatty() else ""
g_green = chr(27) + "[32m" if stdout.isatty() else ""
g_yellow = chr(27) + "[33m" if stdout.isatty() else ""
g_normal = chr(27) + "[0m" if stdout.isatty() else ""


def DetectGlobalSettings():
    if not os.path.exists("project.xml"):
        print "Please run me from the folder containing project.xml."
        sys.exit(1)

    for line in open("project.xml"):
        m = re.match(r'^<WebApplication.*Name="([^"]+)"', line)
        if m:
            appName = m.group(1)
        m = re.match(r'^.*<CodeGenerationFolder>([^<]+)</CodeGe', line)
        if m:
            codeGenFolder = m.group(1)

    if appName == '':
        print "Could not find application name in project.xml ..."
        sys.exit(1)

    print "Detected application " + g_cyan + appName + g_normal + \
        " in folder " + g_yellow + codeGenFolder + g_normal + "..."
    staticPath = codeGenFolder + "/" + appName + '-war/web/'
    return appName, codeGenFolder, staticPath


def IsModifiedFile(filename):
    a = hashlib.md5()
    f = open(filename)
    md5line = f.readline()
    if len(md5line) != 35 or not md5line.startswith('//'):
        return True
    md5line = md5line[2:].strip().lower()
    for line in f.readlines():
        a.update(line)
    return a.hexdigest() != md5line


def main():
    unused_appName, codeGenFolder, unused_staticPath = DetectGlobalSettings()

    if not os.path.exists("project.xml"):
        print "Please run the script from the folder containing 'project.xml'"
        sys.exit(1)

    extensions = ['.ts', '.java']
    for root, unused_dirs, files in os.walk(codeGenFolder):
        for f in files:
            if any(f.endswith(ext) for ext in extensions):
                fullPath = os.path.join(root, f)
                if not IsModifiedFile(fullPath):
                    os.unlink(fullPath)
                    print "Removing", fullPath


if __name__ == "__main__":
    main()
