#!/usr/bin/env python
import os
import re
import sys
import glob
import time
import shutil
import hashlib

g_THRESHOLD = 1024*1024

g_ignoreListTS = [
    'TriggerSourceMap/dummy.ts',
    'sprintf.d.ts',
    'openlayers3.d.ts',
    'version.ts']
g_ignoreListJS = [x.replace('.ts', '.js') for x in g_ignoreListTS]
g_webFolder = os.getenv("WEBFOLDER")
g_groupIndex = os.getenv("GROUP")

# Colored message ANSI constants
g_red = chr(27) + "[31m" if sys.stdout.isatty() else ""
g_cyan = chr(27) + "[36m" if sys.stdout.isatty() else ""
g_green = chr(27) + "[32m" if sys.stdout.isatty() else ""
g_yellow = chr(27) + "[33m" if sys.stdout.isatty() else ""
g_normal = chr(27) + "[0m" if sys.stdout.isatty() else ""


def green(msg):
    sys.stdout.write(g_green + msg + g_normal)


def red(msg):
    sys.stdout.write(g_red + msg + g_normal)


def cyan(msg):
    sys.stdout.write(g_cyan + msg + g_normal)


def yellow(msg):
    sys.stdout.write(g_yellow + msg + g_normal)


def make():
    green(
        "Gathering all .ts files into groups, and generating a Makefile...\n")
    groups = [[]]
    alreadyProcessed = {}
    totalSoFar = 0
    for root, _, files in os.walk("."):
        for filename in [_ for _ in files if _.lower().endswith('.ts')]:
            if root.endswith('Base') and 'Base.ts' in filename:
                # The base .ts files will be compiled together with their
                # extended classes - avoids memory issues
                continue
            fullpath = os.path.join(root, filename)[2:]
            if fullpath in alreadyProcessed:
                continue
            alreadyProcessed[fullpath] = 1
            if fullpath.startswith('DefinitelyTyped'):
                continue
            if any(fullpath.endswith(ignore) for ignore in g_ignoreListTS):
                continue
            groups[-1].append(fullpath)
            totalSoFar += os.stat(fullpath).st_size
            baseFile = filename.replace(".ts", "Base.ts")
            # Gather the base class of any extended classes
            for parent in ["Controllers", "Entities", "Services"]:
                if parent in root:
                    candidate = os.path.join(
                        os.path.dirname(root), parent+"Base", baseFile)[2:]
                    if os.path.exists(candidate):
                        groups[-1].append(candidate)
                        totalSoFar += os.stat(candidate).st_size
                        alreadyProcessed[candidate] = 1
                    break
            if totalSoFar > g_THRESHOLD or len(groups[-1]) > 100:
                totalSoFar = 0
                groups.append([])

    os.chdir(os.path.abspath(os.path.dirname(sys.argv[0]) + "/.."))
    speedyMakefile = open("Makefile", "w")
    with open("Makefile.ParallelTSC") as f:
        for line in f:
            if line.startswith("# @@PYINDEXMARKER@@"):
                appName = line.split()[-1]
                next(f)
                speedyMakefile.write(
                    '${WEBFOLDER}/index.html:	' +
                    '${WEBFOLDER}/index.html.template ' +
                    appName + '/templates/index.html.parallel.footer\n')
            elif line.startswith("# @@PYGROUPMARKER@@"):
                appName = line.split()[-1]
                next(f)
                for idx, grp in enumerate(groups):
                    speedyMakefile.write(
                        "Group" + str(idx+1) + ":\t" +
                        " ".join(grp) + "| " + appName + "/version.js\n")
                    speedyMakefile.write(
                        '	@WEBFOLDER=${WEBFOLDER} GROUP=' + str(idx+1) +
                        ' ./TriggerSourceMap/pythonMaker.py -buildTS $^\n\n')
                allGroups = " ".join(
                    "Group"+str(i+1) for i in xrange(len(groups)))
                speedyMakefile.write(".PHONY:\t" + allGroups + "\n")
                speedyMakefile.write(
                    'common:\t' + allGroups + " ${WEBFOLDER}/index.html " +
                    "${WEBFOLDER}/partials/login.html revision\n")
            else:
                speedyMakefile.write(line)


def getAppName():
    f = open("Makefile.ParallelTSC")
    for line in f:
        if line.startswith("# @@PYINDEXMARKER@@"):
            return line.split()[-1]
    red("Failed to locate PYINDEXMARKER in Makefile.ParallelTSC... " +
        "Aborting...\n")
    sys.exit(-1)


def createMissingTargetFolders(filelist):
    for tsFile in filelist:
        dirName = os.path.dirname(tsFile)
        if not os.path.exists(g_webFolder + "/js/" + dirName):
            try:
                os.makedirs(g_webFolder + "/js/" + dirName, 0755)
            except Exception, e:
                if 'File exists' not in str(e):
                    red("Failed to create" + g_webFolder + "/js/" +
                        dirName + "\n")
                    red(str(e) + "\n")


def copyfileWhileHandlingWindowsLunacy(src, trgt):
    attempt = 0
    while True:
        try:
            attempt += 1
            shutil.copyfile(src, trgt)
            break
        except:
            if attempt == 10:
                red("Your OS is crap. Install Linux, maybe?\n")
                sys.exit(1)
            yellow(
                "Your OS failed to copy:\n    " + src +
                "\nto:\n    " + trgt + "\n")
            print "Retrying in 1 sec..."
            time.sleep(1)


def buildTS(filelist):

    def md5(filename):
        return hashlib.md5(open(filename).read()).hexdigest()

    def checkForRebuild():
        modifiedOrNewFiles = []
        for tsFile in filelist:
            jsName = re.sub(r'\.ts$', '.js', tsFile)
            if os.path.exists(tsFile+".md5") and os.path.exists(jsName):
                if os.stat(tsFile).st_mtime > os.stat(tsFile+".md5").st_mtime:
                    oldMD5 = open(tsFile+".md5").read()
                    newMD5 = md5(tsFile)
                    if oldMD5 != newMD5:
                        modifiedOrNewFiles.append(tsFile)
            else:
                modifiedOrNewFiles.append(tsFile)
        if not modifiedOrNewFiles:
            green("All .js files of Group" + g_groupIndex + " up-to-date.\n")
            sys.exit(0)
        return modifiedOrNewFiles

    def rebuild(listOfFilenames):
        appName = getAppName()
        cyan(
            "Rebuilding:\n\t" + "\n\t".join(listOfFilenames) + "\n...\n")
        if 0 != os.system(
                'tsc --sourcemap --sourceRoot ' +
                '/' + appName + '/src/TypeScript ' +
                '--target ES5 "TriggerSourceMap/dummy.ts" "' +
                '" "'.join(listOfFilenames) + '"'):
            for tsFile in listOfFilenames:
                jsName = re.sub(r'\.ts$', '.js', tsFile)
                try:
                    os.unlink(jsName)
                except:
                    pass
                try:
                    os.unlink(tsFile+".md5")
                except:
                    pass
            sys.exit(-1)

        for tsFile in filelist:
            jsName = re.sub(r'\.ts$', '.js', tsFile)
            open(tsFile+".md5", "w").write(md5(tsFile))
            for f in [jsName, jsName+".map"]:
                os.chmod(f, 0644)
            copyfileWhileHandlingWindowsLunacy(
                jsName, g_webFolder+"/js/"+jsName)
            copyfileWhileHandlingWindowsLunacy(
                jsName+".map", g_webFolder+"/js/"+jsName+".map")

    createMissingTargetFolders(filelist)
    listOfFilenames = checkForRebuild()
    rebuild(listOfFilenames)


def copyJSDEBUG():
    os.chdir(os.path.abspath(os.path.dirname(sys.argv[0]) + "/.."))
    for root, _, files in os.walk("."):
        for jsName in [_ for _ in files if _.lower().endswith('.js')]:
            fullpath = os.path.join(root, jsName)[2:]
            if any(fullpath.endswith(ignore) for ignore in g_ignoreListJS):
                continue
            if fullpath.startswith('DefinitelyTyped'):
                continue
            dirName = os.path.dirname(fullpath)
            try:
                os.mkdir(g_webFolder + "/js/" + dirName)
            except:
                pass
            targetFilename = g_webFolder+"/js/"+dirName+"/"+jsName
            if not os.path.exists(targetFilename):
                copyfileWhileHandlingWindowsLunacy(
                    fullpath, targetFilename)
            if not os.path.exists(targetFilename+".map"):
                copyfileWhileHandlingWindowsLunacy(
                    fullpath+".map", targetFilename+".map")

def which(program):
    '''Check if program exists in PATH'''

    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)
    fpath, _ = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None

def uglifyJS(scriptJSfilename):
    '''Invoke yui-compressor --nomunge --type js on the target'''
    if which('yui-compressor') is None:
        red("No 'yui-compressor' in the PATH...\n")
        sys.exit(-1)
    os.system("echo Minifying " + scriptJSfilename + " ...\n")
    if 0 != os.system(
            "yui-compressor --nomunge --type js -o " +
            scriptJSfilename + ".new " + scriptJSfilename):
        red("'yui-compressor' invocation failed...\n")
        sys.exit(-1)
    copyfileWhileHandlingWindowsLunacy(
        scriptJSfilename+".new", scriptJSfilename)
    os.unlink(scriptJSfilename+".new")

def uglifyJSinDir(dirName):
    '''Invoke yui-compressor --nomunge --type js on the target'''
    if which('yui-compressor') is None:
        red("No 'yui-compressor' in the PATH...\n")
        sys.exit(-1)
    os.system("echo Minifying Js files in directory " + dirName + " ...\n")
    if 0 != os.system(
            "yui-compressor --nomunge --type js -o " +
            "'.js$:.js' " + dirName + "/*.js"):
        red("'yui-compressor' invocation failed...\n")
        sys.exit(-1)

def copyJSRELEASE():
    os.chdir(os.path.abspath(os.path.dirname(sys.argv[0]) + "/.."))
    appName = getAppName()
    shutil.rmtree(g_webFolder+"/js/RTL", ignore_errors=True)
    shutil.rmtree(g_webFolder+"/js/"+appName, ignore_errors=True)
    scriptJSfilename = g_webFolder+"/js/script.js"
    scriptJS = open(scriptJSfilename, 'w')

    def readFile(jsFilename):
        '''Read a tsc-compiled .js file and append to final script.js'''
        if not os.path.exists(jsFilename):
            # No such file - Abort, removing the script.js
            red("Failed to locate "+jsFilename+"... Aborting.\n")
            scriptJS.close()
            try:
                os.unlink(scriptJSfilename)
            except:
                pass
            sys.exit(-1)
        # This is a release build - make sure no such thing exists in the .war
        # ...and also remove any .map, too!
        for ext in ['', '.map']:
            try:
                os.unlink(g_webFolder + "/js/" + jsFilename + ext)
            except:
                pass
        return open(jsFilename).read() + "\n"

    def emitGlobExcludingSuffixes(wildcard, exclusionList):
        for i in glob.glob(wildcard):
            if any(i.endswith(x) for x in exclusionList):
                continue
            scriptJS.write(readFile(i))

    scriptJS.write(readFile(appName + "/version.js"))
    scriptJS.write(readFile("RTL/npTypes.js"))
    emitGlobExcludingSuffixes('RTL/*.js', ["RTL/npTypes.js"])
    scriptJS.write(readFile("RTL/Controllers/BaseController.js"))
    emitGlobExcludingSuffixes('RTL/Controllers/*.js', ["RTL/Controllers/BaseController.js"])
    for line in open ("./Js_Sources.FileList").readlines():
        emitGlobExcludingSuffixes(appName + line.strip(), [])

    scriptJS.close()
    uglifyJS(scriptJSfilename)

    targetDirNames = []
    for line in open ("./Js_SourcesLazy.FileList").readlines():
        jsFile = appName + line.strip()
        if not jsFile.lower().endswith('.js'):
            continue
        targetFilename = g_webFolder + "/js/" + jsFile
        targetDirName = os.path.dirname(targetFilename)
        if targetDirName not in targetDirNames:
            targetDirNames.append(targetDirName)
        try:
            os.makedirs(targetDirName)
        except:
            pass
        if not os.path.exists(targetFilename):
            copyfileWhileHandlingWindowsLunacy(
                jsFile, targetFilename)
    for dir in targetDirNames:
        uglifyJSinDir(dir)

        
if __name__ == "__main__":
    if len(sys.argv) == 1 or sys.argv[1] == "-make":
        make()
    elif sys.argv[1] == "-buildTS":
        buildTS(sys.argv[2:])
    elif sys.argv[1] == "-copyJSDEBUG":
        copyJSDEBUG()
    elif sys.argv[1] == "-copyJSRELEASE":
        copyJSRELEASE()
