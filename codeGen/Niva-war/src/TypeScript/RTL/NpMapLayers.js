/// <reference path="../DefinitelyTyped/jquery/jquery.d.ts"/>
/// <reference path="../DefinitelyTyped/angularjs/angular.d.ts"/>
/// <reference path="openlayers3.d.ts"/>
/// <reference path="FileSaver.ts"/>
/// <reference path="utils.ts"/>
/// <reference path="sprintf.d.ts"/>
/// <reference path="Controllers/BaseController.ts"/>
/// <reference path="base64Utils.ts"/>
/// <reference path="NpMapGlobals.ts"/>
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var NpGeoLayers;
(function (NpGeoLayers) {
    // ==================================
    //  _
    // | |
    // | |     __ _ _   _  ___ _ __ ___
    // | |    / _` | | | |/ _ \ '__/ __|
    // | |___| (_| | |_| |  __/ |  \__ \
    // |______\__,_|\__, |\___|_|  |___/
    //               __/ |
    //              |___/
    //
    // =================================
    ////////////////////
    // Types and enums
    var BaseLayer = (function () {
        function BaseLayer(label, layer, isVector, isVisible, orderIndex, opacity) {
            this.label = label;
            this.layer = layer;
            this.isVector = isVector;
            this.isVisible = isVisible;
            this.orderIndex = orderIndex;
            this.opacity = opacity;
        }
        Object.defineProperty(BaseLayer.prototype, "Map", {
            get: function () {
                return this._map;
            },
            set: function (map) {
                this._map = map;
                this.onAttachMap();
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(BaseLayer.prototype, "source", {
            get: function () {
                return undefined;
            },
            enumerable: true,
            configurable: true
        });
        BaseLayer.prototype.onAttachMap = function () { };
        Object.defineProperty(BaseLayer.prototype, "Current", {
            get: function () {
                var grp = this.Map.grp;
                return isVoid(grp.model.selectedEntities) ?
                    undefined :
                    grp.model.selectedEntities[0];
            },
            enumerable: true,
            configurable: true
        });
        return BaseLayer;
    })();
    NpGeoLayers.BaseLayer = BaseLayer;
    var TileLayer = (function (_super) {
        __extends(TileLayer, _super);
        function TileLayer(label, layer, isVisible) {
            _super.call(this, label, layer, false, isVisible);
        }
        return TileLayer;
    })(BaseLayer);
    NpGeoLayers.TileLayer = TileLayer;
    // Why no GoogleMapsTileLayer class?
    //
    // Because it will never be supported - see here:
    //
    //    https://github.com/openlayers/ol3/issues/2205
    //
    // "There will be no ol.source.GoogleMaps, unless Google changes
    //  their policy and allow direct access to their map tiles."
    var BingTileLayer = (function (_super) {
        __extends(BingTileLayer, _super);
        function BingTileLayer(isVisible, 
            // key: string = 'Ak-dzM4wZjSqTlzveKz5u0d4IQ4bRzVI309GxmkgSVr1ewS6iPSrOvOKhA-CJlm3') {   pavlos:AuxBBMjAlE3BNG6BEaiNVni3ic3XH1uFPrS_ctXzHKohBObdCOBN_rP-n4hkAo9h
            //key: string = 'AqZQy5kWnnlla4IuJL6cw2iI8sIPF98p4vm4f0F60Qrl2I9kDBH7paEarhfs-Bfj') {
            key) {
            if (isVisible === void 0) { isVisible = true; }
            if (key === void 0) { key = 'AuxBBMjAlE3BNG6BEaiNVni3ic3XH1uFPrS_ctXzHKohBObdCOBN_rP-n4hkAo9h'; }
            _super.call(this, "Bing Maps", new ol.layer.Tile({
                source: new ol.source.BingMaps({
                    imagerySet: 'Aerial',
                    key: key,
                    crossOrigin: 'anonymous',
                    maxZoom: 19
                }),
                visible: isVisible
            }), isVisible);
        }
        return BingTileLayer;
    })(TileLayer);
    NpGeoLayers.BingTileLayer = BingTileLayer;
    var ESRITileLayer = (function (_super) {
        __extends(ESRITileLayer, _super);
        function ESRITileLayer(isVisible) {
            if (isVisible === void 0) { isVisible = true; }
            _super.call(this, "ESRI Maps", new ol.layer.Tile({
                source: new ol.source.XYZ({
                    url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
                    crossOrigin: 'null'
                }),
                visible: isVisible
            }), isVisible);
        }
        return ESRITileLayer;
    })(TileLayer);
    NpGeoLayers.ESRITileLayer = ESRITileLayer;
    var NokiaTileLayer = (function (_super) {
        __extends(NokiaTileLayer, _super);
        function NokiaTileLayer(isVisible, appName, key) {
            if (isVisible === void 0) { isVisible = true; }
            if (appName === void 0) { appName = 'Z95pSEJ7EWsD8efJzkDP'; }
            if (key === void 0) { key = 'xzSRD_mZlnfpBRe8grrP9w'; }
            _super.call(this, "NOKIA Maps", new ol.layer.Tile({
                source: new ol.source.XYZ({
                    url: 'https://1.aerial.maps.cit.api.here.com/maptile/2.1/maptile/newest/satellite.day/{z}/{x}/{y}' +
                        '/256/png8?app_id=' + appName + '&app_code=' + key,
                    crossOrigin: 'null'
                }),
                visible: isVisible
            }), isVisible);
        }
        return NokiaTileLayer;
    })(TileLayer);
    NpGeoLayers.NokiaTileLayer = NokiaTileLayer;
    var QuestTileLayer = (function (_super) {
        __extends(QuestTileLayer, _super);
        function QuestTileLayer(isVisible) {
            if (isVisible === void 0) { isVisible = false; }
            _super.call(this, "Map Quest", new ol.layer.Tile({
                source: new ol.source.MapQuest({
                    layer: 'sat' /*,
                    crossOrigin: 'null' */
                }),
                visible: isVisible
            }), isVisible);
        }
        return QuestTileLayer;
    })(TileLayer);
    NpGeoLayers.QuestTileLayer = QuestTileLayer;
    var OpenStreetTileLayer = (function (_super) {
        __extends(OpenStreetTileLayer, _super);
        function OpenStreetTileLayer(isVisible) {
            if (isVisible === void 0) { isVisible = false; }
            _super.call(this, "Open Street Map", new ol.layer.Tile({
                source: new ol.source.OSM({}),
                visible: isVisible
            }), isVisible);
        }
        return OpenStreetTileLayer;
    })(TileLayer);
    NpGeoLayers.OpenStreetTileLayer = OpenStreetTileLayer;
    var OpenTopoTileLayer = (function (_super) {
        __extends(OpenTopoTileLayer, _super);
        function OpenTopoTileLayer(isVisible) {
            if (isVisible === void 0) { isVisible = false; }
            _super.call(this, "Open Topo Map", new ol.layer.Tile({
                source: new ol.source.XYZ({
                    title: 'OSM',
                    type: 'base',
                    url: '//{a-c}.tile.opentopomap.org/{z}/{x}/{y}.png',
                    maxZoom: 17
                }),
                visible: isVisible
            }), isVisible);
        }
        return OpenTopoTileLayer;
    })(TileLayer);
    NpGeoLayers.OpenTopoTileLayer = OpenTopoTileLayer;
    var OpenStamenTileLayer1 = (function (_super) {
        __extends(OpenStamenTileLayer1, _super);
        function OpenStamenTileLayer1(isVisible) {
            if (isVisible === void 0) { isVisible = false; }
            _super.call(this, "OSM Watercolor", new ol.layer.Tile({
                source: new ol.source.Stamen({
                    layer: 'watercolor',
                    maxZoom: 15
                }),
                visible: isVisible
            }), isVisible);
        }
        return OpenStamenTileLayer1;
    })(TileLayer);
    NpGeoLayers.OpenStamenTileLayer1 = OpenStamenTileLayer1;
    var OpenStamenTileLayer2 = (function (_super) {
        __extends(OpenStamenTileLayer2, _super);
        function OpenStamenTileLayer2(isVisible) {
            if (isVisible === void 0) { isVisible = false; }
            _super.call(this, "OSM Toner", new ol.layer.Tile({
                source: new ol.source.Stamen({
                    layer: 'toner',
                    maxZoom: 15
                }),
                visible: isVisible
            }), isVisible);
        }
        return OpenStamenTileLayer2;
    })(TileLayer);
    NpGeoLayers.OpenStamenTileLayer2 = OpenStamenTileLayer2;
    var DigitalGlobeTileLayer = (function (_super) {
        __extends(DigitalGlobeTileLayer, _super);
        function DigitalGlobeTileLayer(isVisible) {
            if (isVisible === void 0) { isVisible = false; }
            _super.call(this, "DigitalGlobe Maps", new ol.layer.Tile({
                source: new ol.source.XYZ({
                    url: 'https://api.tiles.mapbox.com/v4/digitalglobe.nmmhkk79/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZGlnaXRhbGdsb2JlIiwiYSI6ImNpcGg5dHkzYTAxM290bG1kemJraHU5bmoifQ.CHhq1DFgZPSQQC-DYWpzaQ'
                }),
                visible: isVisible
            }), isVisible);
        }
        return DigitalGlobeTileLayer;
    })(TileLayer);
    NpGeoLayers.DigitalGlobeTileLayer = DigitalGlobeTileLayer;
    var GoogleTileLayer = (function (_super) {
        __extends(GoogleTileLayer, _super);
        function GoogleTileLayer(isVisible) {
            if (isVisible === void 0) { isVisible = false; }
            _super.call(this, "Google Satellite", new ol.layer.Tile({
                source: new ol.source.OSM({
                    url: 'https://mt{0-3}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',
                }),
                visible: isVisible
            }), isVisible);
        }
        return GoogleTileLayer;
    })(TileLayer);
    NpGeoLayers.GoogleTileLayer = GoogleTileLayer;
    var GeoserverTileLayer = (function (_super) {
        __extends(GeoserverTileLayer, _super);
        function GeoserverTileLayer(label, url, layer, format, isVisible, username, password) {
            if (format === void 0) { format = 'image/jpeg'; }
            if (isVisible === void 0) { isVisible = false; }
            if (username === void 0) { username = null; }
            if (password === void 0) { password = null; }
            _super.call(this, label, new ol.layer.Tile({
                source: new ol.source.TileWMS({
                    url: url,
                    params: {
                        'LAYERS': layer,
                        'FORMAT': format,
                        'VERSION': '1.1.1',
                        'SRS': 'EPSG:2100',
                        'TILED': true
                    },
                    extent: [103987, 3849000, 1008000, 4626000],
                    projection: 'EPSG:2100',
                    tileGrid: new ol.tilegrid.WMTS({
                        origin: [103987, 3849000],
                        tileSize: [256, 256],
                        resolutions: NpGeoGlobals.getTileResolutions(),
                        maxExtent: [103987, 3849000, 1008000, 4626000],
                        matrixIds: NpGeoGlobals.getTileMatrixIds()
                    }),
                    serverType: 'geoserver'
                }),
                visible: isVisible
            }), isVisible);
            //if (!isVoid(username) && !isVoid(password)) {
            //    $.ajax({
            //        url:
            //        url + '?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=' +
            //        (<any>window).escape(format) + '&TRANSPARENT=true&LAYERS=' +
            //        (<any>window).escape(layer) +
            //        '&WIDTH=256&HEIGHT=256&CRS=EPSG%3A3857&STYLES=&BBOX=' +
            //        '2629433.773010064%2C4237668.84813017%2C2630656.7654626267%2C4238891.840582733',
            //        type: 'GET',
            //        dataType: 'jsonp',
            //        async: false,
            //        beforeSend: function (xhr) {
            //            xhr.setRequestHeader("Authorization", "Basic " + btoa(username + ":" + password));
            //        }
            //    });
            //}
        }
        return GeoserverTileLayer;
    })(TileLayer);
    NpGeoLayers.GeoserverTileLayer = GeoserverTileLayer;
    /**
     * @p.tsagkis 10/4/2016
     * this is test class to support ktimatologio tile layer
     * it is not working.needs unvestigation as origin coordinates
     * seems to be passed incorrectly
     * We can always call it as wms get map request but it will fairly slower
     */
    var KtimaNetTileLayer = (function (_super) {
        __extends(KtimaNetTileLayer, _super);
        function KtimaNetTileLayer(label, url, layer, format, isVisible, username, password) {
            if (format === void 0) { format = 'image/png'; }
            if (isVisible === void 0) { isVisible = false; }
            if (username === void 0) { username = null; }
            if (password === void 0) { password = null; }
            _super.call(this, label, new ol.layer.Tile({
                source: new ol.source.TileWMS({
                    url: url,
                    params: {
                        'LAYERS': layer,
                        'FORMAT': format,
                        'VERSION': '1.1.0',
                        'SRS': 'EPSG:4326',
                        'TILED': true,
                        'TRANSPARENT': false
                    },
                    extent: [19.153, 32.4, 30.962, 41.625],
                    projection: ol.proj.get('EPSG:4326'),
                    tileGrid: new ol.tilegrid.WMTS({
                        origin: [19.153, 32.4],
                        tileSize: [256, 256],
                        resolutions: NpGeoGlobals.getTileResolutions(),
                        maxExtent: [19.153, 32.4, 30.962, 41.625],
                        matrixIds: NpGeoGlobals.getTileMatrixIds()
                    }),
                    //tileLoadFunction: function (imageTile, src) {
                    //    console.log("imageTile", imageTile);
                    //    console.log("src", src);
                    //},
                    serverType: 'geoserver'
                }),
                visible: isVisible
            }), isVisible);
            //if (!isVoid(username) && !isVoid(password)) {
            //    $.ajax({
            //        url:
            //        url + '?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=' +
            //        (<any>window).escape(format) + '&TRANSPARENT=true&LAYERS=' +
            //        (<any>window).escape(layer) +
            //        '&WIDTH=256&HEIGHT=256&CRS=EPSG%3A3857&STYLES=&BBOX=' +
            //        '2629433.773010064%2C4237668.84813017%2C2630656.7654626267%2C4238891.840582733',
            //        type: 'GET',
            //        dataType: 'jsonp',
            //        async: false,
            //        beforeSend: function (xhr) {
            //            xhr.setRequestHeader("Authorization", "Basic " + btoa(username + ":" + password));
            //        }
            //    });
            //}
        }
        return KtimaNetTileLayer;
    })(TileLayer);
    NpGeoLayers.KtimaNetTileLayer = KtimaNetTileLayer;
    var VectorLayer = (function (_super) {
        __extends(VectorLayer, _super);
        function VectorLayer(options) {
            _super.call(this, options.label, options.layer, true, options.isVisible);
            options.layer.layerId = this.layerId;
            options.layer.npName = options.label;
            this.borderColor = options.borderColor;
            this.fillColor = fallback(options.fillColor, 'blue');
            this.labelColor = options.labelColor;
            this.penWidth = options.penWidth;
            //@p.tsagkis
            this.orderIndex = options.orderIndex;
            this.opacity = options.opacity;
            //this.opacity = fallback(options.opacity, NpGeoGlobals.DEFAULT_OPACITY);
            if (!isVoid(this.layer))
                this.layer.setOpacity(this.opacity);
        }
        Object.defineProperty(VectorLayer.prototype, "source", {
            get: function () {
                return this.layer.getSource();
            },
            enumerable: true,
            configurable: true
        });
        VectorLayer.prototype.onInfoFeature = function (feature) {
            console.log("onInfoFeature:", feature);
        };
        return VectorLayer;
    })(BaseLayer);
    NpGeoLayers.VectorLayer = VectorLayer;
    var SqlLayerEntity = (function (_super) {
        __extends(SqlLayerEntity, _super);
        function SqlLayerEntity(data, coordinateSystem) {
            _super.call(this);
            this.data = data;
            this.coordinateSystem = coordinateSystem;
        }
        SqlLayerEntity.prototype.getId = function () {
            return this.data.id;
        };
        SqlLayerEntity.prototype.getGeom = function () {
            return this.data.geom;
        };
        SqlLayerEntity.prototype.getShortDescription = function () {
            return this.data.shortdesc;
        };
        SqlLayerEntity.prototype.getFeature = function () {
            var self = this;
            var ret = NpGeoGlobals.geoJSON_to_OL3feature({
                obj: this.getGeom(),
                coordinateSystem: self.coordinateSystem,
                name: this.getShortDescription()
            });
            ret.entity = this;
            return ret;
        };
        SqlLayerEntity.prototype.getKey = function () { return this.getId(); };
        //        public toJSON(): any { throw "Not Impemented function" }
        SqlLayerEntity.prototype.getEntityName = function () { return "SqlLayerEntity"; };
        //        public updateInstance(other: IBaseEntity): void { throw "Not Impemented function" }
        SqlLayerEntity.prototype.getKeyName = function () { return "id"; };
        return SqlLayerEntity;
    })(NpTypes.BaseEntity);
    NpGeoLayers.SqlLayerEntity = SqlLayerEntity;
    function getAppName() {
        var appName = "";
        try {
            var scope = (angular.element('html').scope());
            appName = scope.globals.appName;
        }
        catch (err) { }
        return appName;
    }
    function getSetting(uniqId, variable, defaultValue) {
        if (!supports_html5_storage())
            return defaultValue;
        var uniqueId = getAppName() + uniqId;
        if (localStorage[uniqueId] === undefined) {
            localStorage[uniqueId] = JSON.stringify({});
        }
        var sessionData = JSON.parse(localStorage[uniqueId]);
        var ret = sessionData[variable];
        if (isVoid(ret)) {
            sessionData[variable] = defaultValue;
            localStorage[uniqueId] = JSON.stringify(sessionData);
            return defaultValue;
        }
        else
            return ret;
    }
    NpGeoLayers.getSetting = getSetting;
    function setSetting(uniqId, variable, newValue) {
        if (!supports_html5_storage())
            return;
        var uniqueId = getAppName() + uniqId;
        if (localStorage[uniqueId] === undefined) {
            localStorage[uniqueId] = JSON.stringify({});
        }
        var sessionData = JSON.parse(localStorage[uniqueId]);
        sessionData[variable] = newValue;
        localStorage[uniqueId] = JSON.stringify(sessionData);
    }
    var SqlVectorLayer = (function (_super) {
        __extends(SqlVectorLayer, _super);
        function SqlVectorLayer(options) {
            _super.call(this, {
                label: options.label,
                layerId: options.layerId,
                layer: new ol.layer.Vector({
                    source: new ol.source.Vector(),
                    visible: getSetting(options.layerId + options.label, "isVisible", options.isVisible || true),
                    layerId: options.layerId
                }),
                isVisible: getSetting(options.layerId + options.label, "isVisible", options.isVisible || true),
                fillColor: options.fillColor || 'blue',
                borderColor: options.borderColor,
                penWidth: options.penWidth,
                //@p.tsagkis
                orderIndex: getSetting(options.layerId + options.label, "orderIndex", options.orderIndex || 0),
                opacity: getSetting(options.layerId + options.label, "opacity", options.opacity || NpGeoGlobals.DEFAULT_OPACITY)
            });
            this.timerBrakes = null;
            //console.log("options.orderIndex", options.orderIndex);
            var self = this;
            this.getQueryUrl = options.getQueryUrl;
            this.layerId = options.layerId;
            this.getQueryInputs = fallback(options.getQueryInputs, function (x) {
                return {};
            });
            this.getSearchFilterQueryUrl = options.getSearchFilterQueryUrl;
            this.getFilterInputParams = fallback(options.getFilterInputParams, function (lovModel) {
                return {};
            });
            this.getDynamicInfo = fallback(options.getDynamicInfo, function () { return undefined; });
            this.isSearch = fallback(options.isSearch, false);
            this.searchDialogPartialName = options.searchDialogPartialName;
            this.cacheLimit = fallback(options.cacheLimit, 1000);
            this.labelVisible = getSetting(options.layerId + options.label, "labelVisible", options.labelVisible || true);
            this.orderIndex = getSetting(options.layerId + options.label, "orderIndex", options.orderIndex || 0);
            this.opacity = getSetting(options.layerId + options.label, "opacity", options.opacity || 1);
            this.verticesVisible =
                isVoid(options.verticesVisible) ?
                    getSetting(options.layerId + options.label, "verticesVisible", options.verticesVisible || true) :
                    options.verticesVisible;
            this.lSnapEnable =
                isVoid(options.lSnapEnable) ?
                    getSetting(options.layerId + options.label, "lSnapEnable", options.lSnapEnable || true) :
                    options.lSnapEnable;
            this.inclusive = options.inclusive;
            this.exclusive = options.exclusive;
            this.selectable =
                isVoid(options.selectable) ?
                    getSetting(options.layerId + options.label, "selectable", options.selectable || true) :
                    options.selectable;
            this.layer.getSqlVectorLayer = function () {
                return self;
            };
            this.searchControllerClassName = options.searchControllerClassName;
            if (isVoid(this.styleFunc)) {
                self.styleFunc = function (feature, resolution) {
                    return self.defaultStyleFunc(feature, resolution);
                };
            }
            this.showSpatialSearchFilters = fallback(options.showSpatialSearchFilters, true);
        }
        SqlVectorLayer.prototype.showSearchDialog = function (otherLayer) {
            if (!this.isSearch) {
                throw "This method can be called only in search layers";
            }
            if (otherLayer.isSearch) {
                throw "The 'other' layer cannot be a search layer";
            }
            var self = this;
            var grp = this.Map.grp;
            var curEntity = self.Current;
            otherLayer.showSearchDialogPrivate(this, curEntity);
        };
        SqlVectorLayer.prototype.onClearFilters = function () {
            this.getQueryUrl = undefined;
            this.getQueryInputs = undefined;
            this.getDynamicInfo = function () { return undefined; };
            this.source.clear();
        };
        Object.defineProperty(SqlVectorLayer.prototype, "persistedSearchModel", {
            //        public getDynamicInfo(): NpTypes.DynamicSqlVectorInfo {
            //            return undefined;
            //        }
            get: function () {
                return getSetting(this.layerId + this.label, "persistedSearchModel", undefined);
            },
            set: function (newVal) {
                setSetting(this.layerId + this.label, "persistedSearchModel", newVal);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SqlVectorLayer.prototype, "persistedSearchFilterQueryUrl", {
            get: function () {
                return getSetting(this.layerId + this.label, "persistedSearchFilterQueryUrl", undefined);
            },
            set: function (newVal) {
                setSetting(this.layerId + this.label, "persistedSearchFilterQueryUrl", newVal);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SqlVectorLayer.prototype, "persistedMasterLayerCode", {
            get: function () {
                return getSetting(this.layerId + this.label, "persistedMasterLayerCode", undefined);
            },
            set: function (newVal) {
                setSetting(this.layerId + this.label, "persistedMasterLayerCode", newVal);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(SqlVectorLayer.prototype, "persistedDynamicSqlVectorInfo", {
            get: function () {
                return getSetting(this.layerId + this.label, "persistedDynamicSqlVectorInfo", undefined);
            },
            set: function (newVal) {
                setSetting(this.layerId + this.label, "persistedDynamicSqlVectorInfo", newVal);
            },
            enumerable: true,
            configurable: true
        });
        SqlVectorLayer.prototype.onApplyNewFilter = function (newGetQueryUrl, newGetQueryInputs, newDynamicInfo) {
            this.getQueryUrl = newGetQueryUrl;
            this.getQueryInputs = newGetQueryInputs;
            this.getDynamicInfo = newDynamicInfo;
        };
        SqlVectorLayer.prototype.recoverFromLocalStorage = function () {
            var _this = this;
            var self = this;
            var masterLayer = this.Map.layers.firstOrNull(function (l) { return (l instanceof SqlVectorLayer) && l.layerId === _this.persistedMasterLayerCode; });
            if (!isVoid(masterLayer) && !isVoid(this.persistedSearchModel)) {
                this.onApplyNewFilter(function () { return _this.persistedSearchFilterQueryUrl; }, function (grpEnt) {
                    var searchModel = self.persistedSearchModel;
                    var ret = masterLayer.getFilterInputParams(searchModel, grpEnt);
                    ret['geoFunc'] = searchModel.geoFunc;
                    ret['geoFuncParam'] = searchModel.geoFuncParam;
                    return ret;
                }, function () { return self.persistedDynamicSqlVectorInfo; });
            }
        };
        SqlVectorLayer.prototype.showSearchDialogPrivate = function (searchLayer, grpEntity) {
            var _this = this;
            var self = this;
            var grp = this.Map.grp;
            var dialogOptions = new NpTypes.SFLDialogOptions;
            dialogOptions.title = "Search " + self.label; //self.LovFrmTitle;
            dialogOptions.previousModel = self._previousSearchModel;
            dialogOptions.className = this.searchControllerClassName;
            dialogOptions.dynInfo = self.getDynamicInfo();
            dialogOptions.onApplyNewFilter = function (searchModel) {
                searchLayer.persistedSearchModel = searchModel.getPersistedModel();
                searchLayer.persistedSearchFilterQueryUrl = self.getSearchFilterQueryUrl();
                searchLayer.persistedDynamicSqlVectorInfo = self.getDynamicInfo();
                searchLayer.persistedMasterLayerCode = self.layerId;
                searchLayer.onApplyNewFilter(self.getSearchFilterQueryUrl, function (grpEnt) {
                    var ret = self.getFilterInputParams(searchModel, grpEnt);
                    if (!isVoid(searchModel) && !isVoid(searchModel.dialogOptions) && !isVoid(searchModel.dialogOptions.selectedGeometry)) {
                        ret['selectedGeometry'] = JSON.stringify(searchModel.dialogOptions.selectedGeometry);
                        ret['geoFunc'] = searchModel.geoFunc;
                        ret['geoFuncParam'] = searchModel.geoFuncParam;
                    }
                    return ret;
                }, self.getDynamicInfo);
            };
            dialogOptions.onClearFilters = function () {
                searchLayer.onClearFilters();
            };
            var selectedFeaures = this.Map.selectInteraction.getFeatures();
            var length = selectedFeaures.getLength();
            var selectedFeature = length > 0 ? selectedFeaures.item(0) : undefined;
            dialogOptions.selectedGeometry = NpGeoGlobals.OL3feature_to_geoJSON(selectedFeature, this.Map.coordinateSystem);
            dialogOptions.showSpatialSearchParams = this.showSpatialSearchFilters;
            /*
                        dialogOptions.onPageDataReceived = (entities: NpGeo.SqlLayerEntity[]) => {
                            var newFeatures = entities.map(ent => ent.getFeature());
                            searchLayer.source.clear();
                            newFeatures.forEach((f: ol.Feature) => {
                                searchLayer.source.addFeature(f);
                            });
            
                        }
            */
            dialogOptions.makeWebRequest = function (paramIndexFrom, paramIndexTo, searchModel, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                //self._previousSearchModel = searchModel;
                var paramData = self.getFilterInputParams(searchModel, grpEntity);
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                //paramData['exc_Id'] = excludedIds;
                var model = searchModel;
                var url = self.getSearchFilterQueryUrl().replace("?", "");
                if (_this.getDynamicInfo() !== undefined) {
                    paramData['layerCode'] = _this.getDynamicInfo().code;
                    paramData['mode'] = 'GRID';
                }
                else {
                    url = url + "_grid?";
                }
                if (!isVoid(dialogOptions.selectedGeometry)) {
                    paramData['selectedGeometry'] = JSON.stringify(dialogOptions.selectedGeometry);
                    paramData['geoFunc'] = searchModel.geoFunc;
                    paramData['geoFuncParam'] = searchModel.geoFuncParam;
                }
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                Utils.showWaitWindow(grp.$scope, grp.Plato, grp.$timeout);
                var wsPath = url.split('/').reverse().slice(0, 2).reverse().join('/').replace('?', '');
                var wsStartTs = (new Date).getTime();
                var promise = grp.$http.post(url, paramData, { timeout: grp.$scope.globals.timeoutInMS, cache: false });
                return promise.success(function () {
                    grp.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    grp.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            var exportDataSet = function (suffix, fileName, searchModel) {
                var grp = _this.Map.grp;
                var paramData = self.getFilterInputParams(searchModel, grpEntity);
                var model = searchModel;
                var url = self.getSearchFilterQueryUrl().replace("?", suffix);
                if (_this.getDynamicInfo() !== undefined) {
                    paramData['layerCode'] = _this.getDynamicInfo().code;
                }
                if (!isVoid(dialogOptions.selectedGeometry)) {
                    paramData['selectedGeometry'] = JSON.stringify(dialogOptions.selectedGeometry);
                    paramData['geoFunc'] = searchModel.geoFunc;
                    paramData['geoFuncParam'] = searchModel.geoFuncParam;
                }
                NpTypes.AlertMessage.clearAlerts(grp.PageModel);
                Utils.showWaitWindow(grp.$scope, grp.Plato, grp.$timeout);
                var wsPath = url.split('/').reverse().slice(0, 2).reverse().join('/').replace('?', '');
                var wsStartTs = (new Date).getTime();
                grp.$http.post(url, paramData, { timeout: grp.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    grp.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (grp.$scope.globals.nInFlightRequests > 0)
                        grp.$scope.globals.nInFlightRequests--;
                    var data = base64DecToArr(response.data);
                    var blob = new Blob([data], { type: "application/octet-stream" });
                    saveAs(blob, fileName);
                }).error(function (data, status, header, config) {
                    grp.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (grp.$scope.globals.nInFlightRequests > 0)
                        grp.$scope.globals.nInFlightRequests--;
                    //                        NpTypes.AlertMessage.addDanger(grp.PageModel, Messages.dynamicMessage(data));
                    NpTypes.AlertMessage.addDanger(grp.PageModel, data);
                });
            };
            dialogOptions.ExportGeoJsonFile = function (searchModel) {
                exportDataSet("_geoJsonFile", "aafile.json", searchModel);
            };
            dialogOptions.ExportExcel = function (searchModel) {
                exportDataSet("_excelFile", "file.xls", searchModel);
            };
            dialogOptions.onRowSelect = function (r) {
                if (isVoid(r) || isVoid(r.getGeom())) {
                    return;
                }
                var feature = r.getFeature();
                if (isVoid(feature)) {
                    return;
                }
                searchLayer.source.clear();
                searchLayer.source.addFeature(feature);
                var extent = feature.getGeometry().getExtent();
                if (isVoid(extent))
                    return;
                var checkForPoint = Math.abs(extent[2] - extent[0]) + Math.abs(extent[3] - extent[1]);
                var view = _this.Map.map.getView();
                if (checkForPoint < 10) {
                    view.setCenter([extent[0], extent[1]]);
                    var activeLayer = _this.Map.tileSelect.getActiveTileLayer();
                    if (!isVoid(activeLayer))
                        view.setZoom(activeLayer.getSource().getTileGrid().getMaxZoom() - 4);
                }
                else
                    view.fit(extent, _this.Map.map.getSize());
            };
            dialogOptions.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, model, excludedIds) {
                var paramData = self.getFilterInputParams(model, grpEntity);
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return res;
            };
            var slf = this.getDynamicInfo() == undefined ? 'slf/' : '';
            grp.Plato.showDialog(grp.$scope, dialogOptions.title, "/" + grp.$scope.globals.appName + '/partials/' + slf + self.searchDialogPartialName + '?rev=' + grp.$scope.globals.version, dialogOptions);
        };
        SqlVectorLayer.prototype.setLabelVisible = function (v) {
            this.labelVisible = v;
            setSetting(this.layerId + this.label, "labelVisible", v);
        };
        SqlVectorLayer.prototype.setLSnapEnable = function (v) {
            //console.log("setting layer snapping=====", v);
            this.lSnapEnable = v;
            setSetting(this.layerId + this.label, "lSnapEnable", v);
        };
        SqlVectorLayer.prototype.setVerticesVisible = function (v) {
            this.verticesVisible = v;
            setSetting(this.layerId + this.label, "verticesVisible", v);
        };
        SqlVectorLayer.prototype.setIsVisible = function (v) {
            this.isVisible = v;
            setSetting(this.layerId + this.label, "isVisible", v);
        };
        SqlVectorLayer.prototype.setSelectable = function (v) {
            this.selectable = v;
            setSetting(this.layerId + this.label, "selectable", v);
        };
        SqlVectorLayer.prototype.setOpacity = function (n) {
            this.opacity = n;
            setSetting(this.layerId + this.label, "opacity", n);
        };
        SqlVectorLayer.prototype.setOrderIndex = function (n) {
            this.orderIndex = n;
            setSetting(this.layerId + this.label, "orderIndex", n);
        };
        SqlVectorLayer.prototype.setUsedForExclusions = function (v) {
            this.exclusiveFilter = v;
            setSetting(this.layerId + this.label, "isUsedForExclusions", v);
        };
        SqlVectorLayer.prototype.getLabelVisible = function () {
            return this.labelVisible;
        };
        SqlVectorLayer.prototype.getVerticesVisible = function () {
            return this.verticesVisible;
        };
        SqlVectorLayer.prototype.getlSnapEnable = function () {
            return this.lSnapEnable;
        };
        SqlVectorLayer.prototype.getSelectable = function () {
            return this.selectable;
        };
        SqlVectorLayer.prototype.getOpacity = function () {
            return this.opacity;
        };
        SqlVectorLayer.prototype.getOrderIndex = function () {
            return this.orderIndex;
        };
        SqlVectorLayer.prototype.addCurrentWindowToParams = function (params) {
            var width = this.Map.map.getViewport().clientWidth;
            var height = this.Map.map.getViewport().clientHeight;
            var blFromMap = this.Map.map.getCoordinateFromPixel([0, height - 1]);
            var urFromMap = this.Map.map.getCoordinateFromPixel([width - 1, 0]);
            var srcProj = 'EPSG:3857';
            var dstProj = NpGeoGlobals.commonCoordinateSystems(this.Map.coordinateSystem);
            if (isVoid(dstProj)) {
                console.error("Can't send SqlVectorLayer query - missing map projection...");
                return;
            }
            var blNew = proj4(srcProj, dstProj, blFromMap);
            var urNew = proj4(srcProj, dstProj, urFromMap);
            params['xmin'] = blNew[0];
            params['ymin'] = blNew[1];
            params['xmax'] = urNew[0];
            params['ymax'] = urNew[1];
            if (this.getDynamicInfo() !== undefined) {
                params['layerCode'] = this.getDynamicInfo().code;
                params['mode'] = 'MAP';
            }
        };
        SqlVectorLayer.prototype.removeSqlLayerFromLocalCache = function (sqlLayerId) {
            var self = this;
            var cacheKey = self.Map.mapId + "#@#" + self.layerId;
            var key = cacheKey + '#@#' + sqlLayerId;
            delete NpGeo.NpMap.cachedVectorsOfMap[key];
        };
        SqlVectorLayer.prototype.fetchLayerGeometries = function () {
            var _this = this;
            var grp = this.Map.grp;
            var self = this;
            var curEntity = self.Current;
            if (!isVoid(this.getQueryUrl) && !isVoid(curEntity) && this.layer.getVisible()) {
                var cacheKey = self.Map.mapId + "#@#" + self.layerId;
                var doTheWork = function () {
                    // console.log("Sending request:" + self.layerId+":"+self.timerBrakes);
                    self.timerBrakes = null;
                    var params = self.getQueryInputs(curEntity);
                    // Closured cache containers
                    var idsInCache = [];
                    var cachedIdsData = {};
                    // Keep this layer's ids (that are cached in the global cache) into the closured containers
                    // Why? Because in the time it takes for the web service call below to come back,
                    // some of these ids may have been lost (evicted from the cache, see eviction code below)
                    for (var key in NpGeo.NpMap.cachedVectorsOfMap) {
                        if (key.startsWith(cacheKey)) {
                            var pieces = key.split(/#@#/);
                            idsInCache.push(pieces[pieces.length - 1]);
                            cachedIdsData[key] = NpGeo.NpMap.cachedVectorsOfMap[key];
                        }
                    }
                    params['idsInCache'] = idsInCache;
                    self.addCurrentWindowToParams(params);
                    //clear all snap features
                    //this.Map.snapFeats.clear();
                    grp.httpPost(self.getQueryUrl(), params, function (response) {
                        // Load all the new sqlEntities using the cache if possible
                        // console.log("Received: " + response.data.length + " entries");
                        var entities = _.map(response.data, function (sqlEntityData) {
                            var se = new SqlLayerEntity(sqlEntityData, self.Map.coordinateSystem);
                            var currentTimeInMS = new Date().getTime();
                            if (isVoid(se.getGeom())) {
                                var key = cacheKey + '#@#' + se.getId();
                                var cacheEntry = NpGeo.NpMap.cachedVectorsOfMap[key];
                                if (!isVoid(cacheEntry)) {
                                    // Update the usage timestamp of this cache entry
                                    cacheEntry.b = currentTimeInMS;
                                    // Cached value returned.
                                    se = cacheEntry.a;
                                }
                                else {
                                    // In the interim, this key has been evicted from the cache.
                                    // Re-insert it, and timestamp it.
                                    se = cachedIdsData[key].a; // Use the closured cache copy!
                                    NpGeo.NpMap.cachedVectorsOfMap[key] = new Tuple2(se, currentTimeInMS);
                                }
                            }
                            else
                                NpGeo.NpMap.cachedVectorsOfMap[cacheKey + '#@#' + se.getId()] =
                                    new Tuple2(se, currentTimeInMS);
                            return se;
                        });
                        // Garbage collect the closured IDs cache
                        cachedIdsData = null;
                        // Garbage collect the oldest globally cached vectors
                        var thisLayerKeys = Object.keys(NpGeo.NpMap.cachedVectorsOfMap).
                            filter(function (x) { return x.startsWith(cacheKey); });
                        if (thisLayerKeys.length > self.cacheLimit) {
                            thisLayerKeys.
                                map(function (x) { return new Tuple2(x, NpGeo.NpMap.cachedVectorsOfMap[x].b); }).
                                sort(function (a1, a2) { return a1.b - a2.b; }).
                                slice(0, thisLayerKeys.length - self.cacheLimit).
                                forEach(function (k) { delete NpGeo.NpMap.cachedVectorsOfMap[k.a]; });
                        }
                        var newFeatures = entities.map(function (ent) { return ent.getFeature(); });
                        self.source.clear();
                        //@p.tsagkis handle the snapping features 
                        //first need to remove any features of current looping layer
                        var featsArray = _this.Map.snapFeats.getArray();
                        var clonedFeats = $.extend(true, [], featsArray); //deep clone it. Necessary step to hanldle collection array.
                        for (var f = 0; f < clonedFeats.length; f++) {
                            if (clonedFeats[f].get('layerId') === self.layerId) {
                                _this.Map.snapFeats.remove(clonedFeats[f]);
                            }
                        }
                        clonedFeats = []; //release it
                        //and now we need to add those new features
                        newFeatures.forEach(function (f) {
                            self.source.addFeature(f);
                            if (self.isVisible && self.lSnapEnable === true) {
                                f.set('layerId', self.layer.get('layerId'));
                                //console.log("pushing features. setting layer id", (<any>self.layer).get('layerId'));
                                _this.Map.snapFeats.push(f);
                            }
                        });
                        _this.Map.snapInteraction.setActive(_this.Map.snapEnabled);
                    });
                };
                if (!isVoid(self.timerBrakes)) {
                    // console.log("Clearing pending request..." + self.layerId+":"+self.timerBrakes);
                    clearTimeout(self.timerBrakes);
                }
                self.timerBrakes = setTimeout(doTheWork, 500);
            }
        };
        SqlVectorLayer.prototype.onAttachMap = function () {
            var _this = this;
            var grp = this.Map.grp;
            var self = this;
            this.Map.snapFeats = new ol.Collection([]);
            this.Map.snapFeats.clear();
            if (this.isSearch) {
                this.recoverFromLocalStorage();
            }
            grp.$scope.$on('$destroy', (grp.$scope.$watch(function () {
                var curEntity = self.Current;
                var params = self.getQueryInputs(curEntity);
                self.addCurrentWindowToParams(params);
                var visible = self.Map.isVisible();
                return isVoid(curEntity) ? "" : JSON.stringify(params) + _this.layer.getVisible().toString() + visible;
            }, function () {
                if (self.Map.isVisible()) {
                    _this.layer.setStyle(NpGeoGlobals.createPolygonWithVerticesStyleFunction(_this.Map, _this.styleFunc, _this.fillColor, _this.borderColor, _this.labelColor, _this.penWidth, undefined, _this.verticesVisible, _this.labelVisible));
                    self.fetchLayerGeometries();
                }
            })));
        };
        /**
         * @P.tsagkis
         * optimization has taken place for smart label placement
         * get the current mbr, intersect it with each feature is about to be labeled
         * and then place the text within the intersected geometry
         * The point returned should always be inside the polygon. THIS IS A RULE
         * Also handling the multipolygons case.
         * Find the larger one and label only this.
         * @16/2/2017 adding the poitn support
         *
         * @param feature
         * @param resolution
         */
        SqlVectorLayer.prototype.defaultStyleFunc = function (feature, resolution) {
            var fillColorMapped;
            var borderColorMapped;
            var labelColorMapped;
            var penWidth;
            if (typeof this.fillColor !== 'undefined') {
                fillColorMapped = (this.fillColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[this.fillColor] : this.fillColor;
            }
            else {
                fillColorMapped = 'rgba(255, 165, 0, 0.8)'; //default orange
            }
            if (typeof this.borderColor !== 'undefined') {
                borderColorMapped = (this.borderColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[this.borderColor] : this.borderColor;
            }
            else {
                borderColorMapped = 'rgba(0, 0, 0, 0.8)'; //default black
            }
            if (typeof this.labelColor !== 'undefined') {
                labelColorMapped = (this.labelColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[this.labelColor] : this.labelColor;
            }
            else {
                labelColorMapped = 'black'; //use black for default 
            }
            penWidth = (typeof this.penWidth !== 'undefined') ? this.penWidth : 7; //set a point size of 7 for deafault
            //console.log("fillColorMapped",fillColorMapped)
            var config = {};
            if (feature.getGeometry().getType() === 'Point') {
                config = {
                    image: new ol.style.Circle({
                        radius: penWidth,
                        fill: new ol.style.Fill({
                            color: fillColorMapped
                        }),
                        stroke: new ol.style.Stroke({
                            color: borderColorMapped,
                            width: 2
                        })
                    })
                };
            }
            else {
                config = {
                    stroke: new ol.style.Stroke({
                        color: borderColorMapped,
                        width: this.penWidth
                    }),
                    fill: new ol.style.Fill({
                        color: fillColorMapped
                    })
                };
            }
            var style = new ol.style.Style(config);
            var textStyleConfig = {
                text: NpGeoGlobals.createTextStyle(feature, resolution, this.labelVisible, labelColorMapped),
                geometry: function (feature) {
                    var mapExtent = NpGeoGlobals.globalMapThis.map.getView().calculateExtent(NpGeoGlobals.globalMapThis.map.getSize());
                    var extentGeom = new ol.geom.Polygon([[
                            [mapExtent[0], mapExtent[1]],
                            [mapExtent[0], mapExtent[3]],
                            [mapExtent[2], mapExtent[3]],
                            [mapExtent[2], mapExtent[1]],
                            [mapExtent[0], mapExtent[1]]
                        ]]);
                    if (feature.getGeometry().getType() === 'MultiPolygon') {
                        var intGeom = getJSTSIntersection(NpGeoGlobals.getMaxPoly(feature.getGeometry().getPolygons()), extentGeom);
                        if (intGeom.getType() === 'MultiPolygon') {
                            return NpGeoGlobals.getMaxPoly(intGeom.getPolygons()).getInteriorPoint();
                        }
                        else {
                            return intGeom.getInteriorPoint();
                        }
                    }
                    else if (feature.getGeometry().getType() === 'Polygon') {
                        return feature.getGeometry().getInteriorPoint();
                    }
                    else if (feature.getGeometry().getType() === 'Point' || feature.getGeometry().getType() == 'MultiPoint') {
                        return feature.getGeometry();
                    }
                    else if (feature.getGeometry().getType() === 'LineString') {
                        return new ol.geom.Point(NpGeoGlobals.lineMidpoint(feature.getGeometry().getCoordinates()));
                    }
                    else if (feature.getGeometry().getType() === 'MultiLineString') {
                        return new ol.geom.Point(NpGeoGlobals.lineMidpoint(NpGeoGlobals.getMaxLine(feature.getGeometry().getLineStrings()).getCoordinates()));
                    }
                    else {
                        console.error("wrong geometry type", feature.getGeometry());
                    }
                }
            };
            var textStyle = new ol.style.Style(textStyleConfig);
            var style = new ol.style.Style(config);
            return [style, textStyle];
        };
        SqlVectorLayer.prototype.onSelectFeature = function (feature, lyr) {
            var html = this.Map.getFeatureLabel(feature, false);
            var infoString = '';
            if (!isVoid(lyr.npName)) {
                infoString = " (" + lyr.npName + ")";
            }
            if (!isVoid(html)) {
                messageBox(this.Map.grp.$scope, this.Map.Plato, "Info " + infoString, html, IconKind.INFO, [
                    new Tuple2("OK", function () { })
                ], 0, 0, 'auto');
            }
        };
        SqlVectorLayer.prototype.onInfoFeature = function (feature) {
            console.log("feature data ios====", feature.entity.data);
        };
        return SqlVectorLayer;
    })(VectorLayer);
    NpGeoLayers.SqlVectorLayer = SqlVectorLayer;
    var DynamicSqlVectorLayer = (function (_super) {
        __extends(DynamicSqlVectorLayer, _super);
        function DynamicSqlVectorLayer(appName, inf) {
            _super.call(this, {
                layerId: inf.code,
                label: inf.label,
                isVisible: true,
                getQueryUrl: function () { return '/' + appName + '/rest/MainService/dynamicLayer_request?'; },
                getQueryInputs: function (x) { return {}; },
                getSearchFilterQueryUrl: function () { return '/' + appName + '/rest/MainService/dynamicLayer_request?'; },
                getFilterInputParams: function (srhModel, naturalDisaster) { return {}; },
                searchDialogPartialName: 'DynamicSqlLayerFilter.html',
                searchControllerClassName: 'DynamicSqlLayerFilterController',
                fillColor: inf.fillColor,
                borderColor: inf.borderColor,
                penWidth: inf.penWidth,
                orderIndex: inf.orderIndex,
                opacity: inf.opacity,
                getDynamicInfo: function () { return inf; }
            });
            this.appName = appName;
            this.inf = inf;
        }
        return DynamicSqlVectorLayer;
    })(SqlVectorLayer);
    NpGeoLayers.DynamicSqlVectorLayer = DynamicSqlVectorLayer;
    var WfsVectorLayer = (function (_super) {
        __extends(WfsVectorLayer, _super);
        function WfsVectorLayer(options) {
            _super.call(this, {
                label: options.label,
                layer: null,
                layerId: null,
                isVisible: options.isVisible,
                fillColor: options.fillColor || 'blue',
                borderColor: options.borderColor,
                penWidth: options.penWidth,
                opacity: options.opacity || NpGeoGlobals.DEFAULT_OPACITY
            });
            this.timerForLoader = null;
            var self = this;
            this.urlWFS = options.urlWFS;
            this.callbackName = options.callbackName;
            this.limit = fallback(options.limit, undefined);
            this.coordinateSystem = fallback(this.coordinateSystem, 'EPSG:3857');
            this.layer = new ol.layer.Vector({
                source: new ol.source.ServerVector({
                    format: new ol.format.GeoJSON({
                        defaultDataProjection: ol.proj.get(self.coordinateSystem)
                    }),
                    loader: function (extent, resolution, projection) {
                        self.onLoad(extent, resolution, projection);
                    },
                    strategy: ol.loadingstrategy.createTile(new ol.tilegrid.XYZ({
                        maxZoom: 19
                    }))
                }),
                style: new ol.style.Style({
                    stroke: new ol.style.Stroke({
                        color: self.fillColor,
                        width: 2
                    })
                }),
            });
            //@p.tsagkis
            this.layer.setOpacity(this.opacity);
            this.layer.setZIndex(this.orderIndex);
            this.layer.setStyle(NpGeoGlobals.createPolygonStyleFunction(this.fillColor, this.borderColor, this.labelColor, this.penWidth, undefined));
        }
        WfsVectorLayer.prototype.onLoad = function (extent, resolution, projection) {
            var npWfs = this;
            var url = npWfs.urlWFS;
            if (!isVoid(npWfs.limit)) {
                url += '&count=' + npWfs.limit;
            }
            url += '&outputFormat=text/javascript&format_options=callback:';
            url += npWfs.callbackName;
            // The extent coming in from OpenLayers is wrong - use the current map viewport
            var width = npWfs.Map.map.getViewport().clientWidth;
            var height = npWfs.Map.map.getViewport().clientHeight;
            var blFromMap = npWfs.Map.map.getCoordinateFromPixel([0, height - 1]);
            var urFromMap = npWfs.Map.map.getCoordinateFromPixel([width - 1, 0]);
            var srcProj = projection.code_;
            if (isVoid(srcProj))
                srcProj = 'EPSG:3857';
            var dstProj = NpGeoGlobals.commonCoordinateSystems(npWfs.Map.coordinateSystem);
            if (isVoid(dstProj))
                return;
            //var ul = [extent[0], extent[1]];
            //var br = [extent[2], extent[3]];
            var fixedExtent = [blFromMap[0], blFromMap[1], urFromMap[0], urFromMap[1]];
            var blNew = proj4(srcProj, dstProj, blFromMap);
            var urNew = proj4(srcProj, dstProj, urFromMap);
            var newExtent = [blNew[0], blNew[1], urNew[0], urNew[1]];
            url += '&srsName=' + projection.code_ + '&bbox=' + newExtent.join(',') + ',' + npWfs.Map.coordinateSystem;
            var doTheWork = function () {
                // console.log("From " + fixedExtent.join(' ### '));
                // console.log("To " + newExtent.join(' ### '));
                // console.log(url);
                $.ajax({
                    url: url,
                    dataType: 'jsonp'
                });
            };
            if (!isVoid(npWfs.timerForLoader))
                clearTimeout(npWfs.timerForLoader);
            npWfs.timerForLoader = setTimeout(doTheWork, 1000);
        };
        Object.defineProperty(WfsVectorLayer.prototype, "source", {
            get: function () {
                return this.layer.getSource();
            },
            enumerable: true,
            configurable: true
        });
        return WfsVectorLayer;
    })(VectorLayer);
    NpGeoLayers.WfsVectorLayer = WfsVectorLayer;
})(NpGeoLayers || (NpGeoLayers = {}));
//# sourceMappingURL=NpMapLayers.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
