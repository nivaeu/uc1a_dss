// https://npmjs.org/package/sprintf

declare module Sprintf {
	export function sprintf(fmt: string, ...args: any[]): string;
	export function vsprintf(fmt: string, args: any[]): string;
}
