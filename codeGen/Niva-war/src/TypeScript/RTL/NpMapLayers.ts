﻿/// <reference path="../DefinitelyTyped/jquery/jquery.d.ts"/>
/// <reference path="../DefinitelyTyped/angularjs/angular.d.ts"/>
/// <reference path="openlayers3.d.ts"/>
/// <reference path="FileSaver.ts"/>
/// <reference path="utils.ts"/>
/// <reference path="sprintf.d.ts"/>
/// <reference path="Controllers/BaseController.ts"/>
/// <reference path="base64Utils.ts"/>
/// <reference path="NpMapGlobals.ts"/>

module NpGeoLayers {

    // ==================================
    //  _
    // | |
    // | |     __ _ _   _  ___ _ __ ___
    // | |    / _` | | | |/ _ \ '__/ __|
    // | |___| (_| | |_| |  __/ |  \__ \
    // |______\__,_|\__, |\___|_|  |___/
    //               __/ |
    //              |___/
    //
    // =================================

    ////////////////////
    // Types and enums

    export class BaseLayer {
        constructor(
            public label: string,
            public layer: ol.layer.Layer,
            public isVector: boolean,
            public isVisible: boolean,
            public orderIndex?: number,
            public opacity?: number
        )
        { }

        private _map: NpGeo.NpMap;
        public get Map(): NpGeo.NpMap {
            return this._map;
        }

        public set Map(map: NpGeo.NpMap) {
            this._map = map;
            this.onAttachMap();
        }

        public get source(): ol.source.Vector {
            return undefined;
        }

        public onAttachMap() { }
        public get Current(): NpTypes.IBaseEntity {
            var grp = this.Map.grp;
            return isVoid(grp.model.selectedEntities) ?
                undefined :
                grp.model.selectedEntities[0];
        }

    }

    export class TileLayer extends BaseLayer {
        constructor(
            label: string,
            layer: ol.layer.Tile,
            isVisible: boolean) {
            super(label, layer, false, isVisible);
        }
    }

    // Why no GoogleMapsTileLayer class?
    //
    // Because it will never be supported - see here:
    //
    //    https://github.com/openlayers/ol3/issues/2205
    //
    // "There will be no ol.source.GoogleMaps, unless Google changes
    //  their policy and allow direct access to their map tiles."

    export class BingTileLayer extends TileLayer {
        constructor(isVisible: boolean = true,
            // key: string = 'Ak-dzM4wZjSqTlzveKz5u0d4IQ4bRzVI309GxmkgSVr1ewS6iPSrOvOKhA-CJlm3') {   pavlos:AuxBBMjAlE3BNG6BEaiNVni3ic3XH1uFPrS_ctXzHKohBObdCOBN_rP-n4hkAo9h
            //key: string = 'AqZQy5kWnnlla4IuJL6cw2iI8sIPF98p4vm4f0F60Qrl2I9kDBH7paEarhfs-Bfj') {
            key: string = 'AuxBBMjAlE3BNG6BEaiNVni3ic3XH1uFPrS_ctXzHKohBObdCOBN_rP-n4hkAo9h') {
            super(
                "Bing Maps",
                new ol.layer.Tile({
                    source: new ol.source.BingMaps({
                        imagerySet: 'Aerial',
                        key: key,
                        crossOrigin: 'anonymous',
                        maxZoom: 19
                    }),
                    visible: isVisible
                }),
                isVisible);
        }
    }

    export class ESRITileLayer extends TileLayer {
        constructor(isVisible: boolean = true) {
            super(
                "ESRI Maps",
                new ol.layer.Tile({
                    source: new ol.source.XYZ({
                        url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
                        crossOrigin: 'null'
                    }),
                    visible: isVisible
                }),
                isVisible);
        }
    }

    export class NokiaTileLayer extends TileLayer {
        constructor(
            isVisible: boolean = true,
            appName: string = 'Z95pSEJ7EWsD8efJzkDP',
            key: string = 'xzSRD_mZlnfpBRe8grrP9w') {
            super(
                "NOKIA Maps",
                new ol.layer.Tile({
                    source: new ol.source.XYZ({
                        url: 'https://1.aerial.maps.cit.api.here.com/maptile/2.1/maptile/newest/satellite.day/{z}/{x}/{y}' +
                        '/256/png8?app_id=' + appName + '&app_code=' + key,
                        crossOrigin: 'null'
                    }),
                    visible: isVisible
                }),
                isVisible);
        }
    }

    export class QuestTileLayer extends TileLayer {
        constructor(isVisible: boolean = false) {
            super(
                "Map Quest",
                new ol.layer.Tile({
                    source: new ol.source.MapQuest({
                        layer: 'sat'/*,
                        crossOrigin: 'null' */
                    }),
                    visible: isVisible
                }),
                isVisible);
        }
    }

    export class OpenStreetTileLayer extends TileLayer {
        constructor(isVisible: boolean = false) {
            super(
                "Open Street Map",
                new ol.layer.Tile({
                    source: new ol.source.OSM({
                        // crossOrigin: 'null'
                    }),
                    visible: isVisible
                }),
                isVisible);
        }
    }

    export class OpenTopoTileLayer extends TileLayer {
        constructor(isVisible: boolean = false) {
            super(
                "Open Topo Map",
                new ol.layer.Tile({
                    source: new ol.source.XYZ({
                        title    : 'OSM',
                        type     : 'base',
                        url      : '//{a-c}.tile.opentopomap.org/{z}/{x}/{y}.png',
                        maxZoom  : 17
                        
                    }),
                    visible: isVisible
                }),
                isVisible);
        }
    }

    export class OpenStamenTileLayer1 extends TileLayer {
        constructor(isVisible: boolean = false) {
            super(
                "OSM Watercolor",
                new ol.layer.Tile({
                    source: new ol.source.Stamen({
                        layer: 'watercolor',//toner
                        maxZoom: 15
                    }),
                    visible: isVisible
                }),

                isVisible);
        }
    }

    export class OpenStamenTileLayer2 extends TileLayer {
        constructor(isVisible: boolean = false) {
            super(
                "OSM Toner",
                new ol.layer.Tile({
                    source: new ol.source.Stamen({
                        layer: 'toner',//toner
                        maxZoom: 15
                    }),
                    visible: isVisible
                }),

                isVisible);
        }
    }

    export class DigitalGlobeTileLayer extends TileLayer {
        constructor(isVisible: boolean = false) {
            super(
                "DigitalGlobe Maps",
                new ol.layer.Tile({
                    source: new ol.source.XYZ({
                        url: 'https://api.tiles.mapbox.com/v4/digitalglobe.nmmhkk79/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiZGlnaXRhbGdsb2JlIiwiYSI6ImNpcGg5dHkzYTAxM290bG1kemJraHU5bmoifQ.CHhq1DFgZPSQQC-DYWpzaQ'
                    }),
                    visible: isVisible
                }),
                isVisible);
        }
    }


    export class GoogleTileLayer extends TileLayer {
        constructor(isVisible: boolean = false) {
            super(
                "Google Satellite",
                new ol.layer.Tile({
                    source: new ol.source.OSM({
                        url: 'https://mt{0-3}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',
                    }),
                    visible: isVisible
                }),
                isVisible);
        }
    }



    export class GeoserverTileLayer extends TileLayer {
        constructor(
            label: string,
            url: string,
            layer: string,
            format: string = 'image/jpeg',
            isVisible: boolean = false,
            username: string = null,
            password: string = null
        ) {
            super(
                label,
                new ol.layer.Tile({
                    source: new ol.source.TileWMS({
                        url: url,
                        params: {
                            'LAYERS'  : layer,
                            'FORMAT'  : format,
                            'VERSION' : '1.1.1', 
                            'SRS'     : 'EPSG:2100',
                            'TILED'   : true
                        },
                        extent         : [103987, 3849000, 1008000, 4626000],
                        projection     : 'EPSG:2100',
                        tileGrid       : new ol.tilegrid.WMTS({
                            origin       : [103987, 3849000],
                            tileSize     : [256, 256],
                            resolutions  : NpGeoGlobals.getTileResolutions(),
                            maxExtent    : [103987, 3849000, 1008000, 4626000],
                            matrixIds    : NpGeoGlobals.getTileMatrixIds()
                        }),
                        serverType     : 'geoserver'
                        //crossOrigin: 'anonymous'
                    }),
                    visible: isVisible
                }),
                isVisible);
            //if (!isVoid(username) && !isVoid(password)) {
            //    $.ajax({
            //        url:
            //        url + '?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=' +
            //        (<any>window).escape(format) + '&TRANSPARENT=true&LAYERS=' +
            //        (<any>window).escape(layer) +
            //        '&WIDTH=256&HEIGHT=256&CRS=EPSG%3A3857&STYLES=&BBOX=' +
            //        '2629433.773010064%2C4237668.84813017%2C2630656.7654626267%2C4238891.840582733',
            //        type: 'GET',
            //        dataType: 'jsonp',
            //        async: false,
            //        beforeSend: function (xhr) {
            //            xhr.setRequestHeader("Authorization", "Basic " + btoa(username + ":" + password));
            //        }
            //    });
            //}
        }
    }
    /**
     * @p.tsagkis 10/4/2016
     * this is test class to support ktimatologio tile layer
     * it is not working.needs unvestigation as origin coordinates
     * seems to be passed incorrectly
     * We can always call it as wms get map request but it will fairly slower
     */
    export class KtimaNetTileLayer extends TileLayer {
        constructor(
            label: string,
            url: string,
            layer: string,
            format: string = 'image/png',
            isVisible: boolean = false,
            username: string = null,
            password: string = null
        ) {
            super(
                label,
                new ol.layer.Tile({
                    source: new ol.source.TileWMS({
                        url: url,
                        params: {
                            'LAYERS'        : layer,
                            'FORMAT'        : format,
                            'VERSION'       : '1.1.0',
                            'SRS'           : 'EPSG:4326',
                            'TILED'         : true,
                            'TRANSPARENT'   : false
                        },
                        extent: [19.153, 32.4, 30.962, 41.625],
                        projection: ol.proj.get('EPSG:4326'),
                        tileGrid: new ol.tilegrid.WMTS({
                            origin: [19.153, 32.4],
                            tileSize: [256, 256],
                            resolutions: NpGeoGlobals.getTileResolutions(),
                            maxExtent: [19.153, 32.4, 30.962, 41.625],
                            matrixIds: NpGeoGlobals.getTileMatrixIds()
                        }),
                        //tileLoadFunction: function (imageTile, src) {
                        //    console.log("imageTile", imageTile);
                        //    console.log("src", src);
                        //},
                        serverType: 'geoserver'
                        //crossOrigin: 'anonymous'
                    }),
                    visible: isVisible
                }),
                isVisible);
            //if (!isVoid(username) && !isVoid(password)) {
            //    $.ajax({
            //        url:
            //        url + '?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetMap&FORMAT=' +
            //        (<any>window).escape(format) + '&TRANSPARENT=true&LAYERS=' +
            //        (<any>window).escape(layer) +
            //        '&WIDTH=256&HEIGHT=256&CRS=EPSG%3A3857&STYLES=&BBOX=' +
            //        '2629433.773010064%2C4237668.84813017%2C2630656.7654626267%2C4238891.840582733',
            //        type: 'GET',
            //        dataType: 'jsonp',
            //        async: false,
            //        beforeSend: function (xhr) {
            //            xhr.setRequestHeader("Authorization", "Basic " + btoa(username + ":" + password));
            //        }
            //    });
            //}
        }
    }


    export class VectorLayer extends BaseLayer {
        fillColor: string;
        borderColor: string;
        labelColor: string;
        penWidth: number;
        orderIndex: number;
        opacity: number;
        layerId: string;
        

        constructor(
            options: {
                label: string;
                layer: ol.layer.Vector;
                layerId: string;
                isVisible: boolean;
                fillColor?: string;
                borderColor?: string;
                labelColor?: string;
                penWidth?: number;
                orderIndex?: number;
                opacity?: number;
                
            }) {
            super(options.label, options.layer, true, options.isVisible);
            (<any>options.layer).layerId = this.layerId;
            (<any>options.layer).npName = options.label;
            this.borderColor = options.borderColor;
            this.fillColor = fallback(options.fillColor, 'blue');
            this.labelColor = options.labelColor;
            this.penWidth = options.penWidth;
            //@p.tsagkis
            this.orderIndex = options.orderIndex;
            this.opacity = options.opacity;
            
            
            
            //this.opacity = fallback(options.opacity, NpGeoGlobals.DEFAULT_OPACITY);
            if (!isVoid(this.layer))
                this.layer.setOpacity(this.opacity);
        }

        public get source(): ol.source.Vector {
            return this.layer.getSource();
        }


        public onInfoFeature(feature: ol.Feature) {
            console.log("onInfoFeature:", feature);
        }
    }

    export class SqlLayerEntity extends NpTypes.BaseEntity {
        constructor(public data: any, public coordinateSystem: string) {
            super();
        }

        public getId(): string {
            return this.data.id;
        }
        public getGeom(): any {
            return this.data.geom;
        }
        public getShortDescription(): string {
            return this.data.shortdesc;
        }
        public getFeature(): ol.Feature {
            var self = this;
            var ret = NpGeoGlobals.geoJSON_to_OL3feature({
                obj: this.getGeom(),
                coordinateSystem: self.coordinateSystem,
                name: this.getShortDescription()
            });
            ret.entity = this;
            return ret;
        }

        public getKey(): string { return this.getId(); }
        //        public toJSON(): any { throw "Not Impemented function" }
        public getEntityName(): string { return "SqlLayerEntity"; }
        //        public updateInstance(other: IBaseEntity): void { throw "Not Impemented function" }
        public getKeyName(): string { return "id"; }
        //        public fromJSON(data: any): IBaseEntity[] { throw "Not Impemented function" }

    }

    function getAppName() {
        var appName = "";
        try {
            var scope: NpTypes.IApplicationScope = <NpTypes.IApplicationScope>(angular.element('html').scope());
            appName = scope.globals.appName;
        }
        catch (err) { }
        return appName;
    }

    export function getSetting<T>(uniqId: string, variable: string, defaultValue: T): T {
        if (!supports_html5_storage())
            return defaultValue;
        var uniqueId = getAppName() + uniqId;
        if (localStorage[uniqueId] === undefined) {
            localStorage[uniqueId] = JSON.stringify({});
        }
        var sessionData = JSON.parse(localStorage[uniqueId]);
        var ret = sessionData[variable];
        if (isVoid(ret)) {
            sessionData[variable] = defaultValue;
            localStorage[uniqueId] = JSON.stringify(sessionData);
            return defaultValue;
        } else
            return ret;
    }

    function setSetting<T>(uniqId: string, variable: string, newValue: T): T {
        if (!supports_html5_storage())
            return;
        var uniqueId = getAppName() + uniqId;
        if (localStorage[uniqueId] === undefined) {
            localStorage[uniqueId] = JSON.stringify({});
        }
        var sessionData = JSON.parse(localStorage[uniqueId]);
        sessionData[variable] = newValue;
        localStorage[uniqueId] = JSON.stringify(sessionData);
    }


    export class SqlVectorLayer<
        TGrpEntity extends NpTypes.IBaseEntity,
        TSearchDialogModel extends Controllers.AbstractSqlLayerFilterModel> extends VectorLayer {
        //public entities: SqlLayerEntity[] = [];
        public getQueryUrl: () => string;
        public getSearchFilterQueryUrl: () => string;
        public getQueryInputs: (x: TGrpEntity) => any;
        public getFilterInputParams: (lovModel: TSearchDialogModel, grpEnt?: TGrpEntity) => any;
        public getDynamicInfo: () => NpTypes.DynamicSqlVectorInfo;
        private timerBrakes: number = null;
        public layerId: string;
        private cacheLimit: number;
        private labelVisible: boolean;
        public orderIndex: number;
        public opacity: number;
        private verticesVisible: boolean;
        public lSnapEnable: boolean;
        private selectable: boolean;
        public inclusive: boolean;
        public exclusive: boolean;
        public exclusiveFilter: boolean;
        public searchDialogPartialName: string;
        public isSearch: boolean;
        public searchControllerClassName: string;
        public styleFunc: (feature: ol.Feature, resolution: any) => ol.style.Style[];
        public showSpatialSearchFilters: boolean;


        constructor(options: {
            layerId: string;
            label: string;
            isVisible?: boolean;
            getQueryUrl?: () => string;
            getQueryInputs?: (x: TGrpEntity) => any;
            getSearchFilterQueryUrl?: () => string;
            getFilterInputParams?: (lovModel: TSearchDialogModel, grpEnt?: TGrpEntity) => any;
            searchDialogPartialName?: string;
            fillColor?: string;
            borderColor?: string;
            penWidth?: number;
            orderIndex?: number;
            opacity?: number;
            cacheLimit?: number;
            labelVisible?: boolean;
            verticesVisible?: boolean;
            lSnapEnable?: boolean;
            inclusive?: boolean;
            exclusive?: boolean;
            selectable?: boolean;
            isSearch?: boolean;
            searchControllerClassName?: string;
            getDynamicInfo?: () => NpTypes.DynamicSqlVectorInfo;
            showSpatialSearchFilters?: boolean;
        }) {
            super({
                label: options.label,
                layerId: options.layerId,
                layer: new ol.layer.Vector({
                    source: new ol.source.Vector(),
                    visible: getSetting(options.layerId + options.label, "isVisible", options.isVisible || true),
                    layerId: options.layerId
                }),
                isVisible: getSetting(options.layerId + options.label, "isVisible", options.isVisible || true),
                fillColor: options.fillColor || 'blue',
                borderColor: options.borderColor,
                penWidth: options.penWidth,
                //@p.tsagkis
                orderIndex: getSetting(options.layerId + options.label, "orderIndex", options.orderIndex || 0),
                opacity: getSetting(options.layerId + options.label, "opacity", options.opacity || NpGeoGlobals.DEFAULT_OPACITY)
            });
            //console.log("options.orderIndex", options.orderIndex);
            var self = this;
            this.getQueryUrl = options.getQueryUrl;
            this.layerId = options.layerId;
            this.getQueryInputs = fallback(options.getQueryInputs, (x: TGrpEntity) => {
                return {};
            });
            this.getSearchFilterQueryUrl = options.getSearchFilterQueryUrl;
            this.getFilterInputParams = fallback(options.getFilterInputParams, (lovModel: TSearchDialogModel) => {
                return {};
            });
            this.getDynamicInfo = fallback(options.getDynamicInfo, () => undefined);
            this.isSearch = fallback(options.isSearch, false);
            this.searchDialogPartialName = options.searchDialogPartialName;
            this.cacheLimit = fallback(options.cacheLimit, 1000);
            this.labelVisible = getSetting(options.layerId + options.label, "labelVisible", options.labelVisible || true);
            this.orderIndex = getSetting(options.layerId + options.label, "orderIndex", options.orderIndex || 0);
            this.opacity = getSetting(options.layerId + options.label, "opacity", options.opacity || 1);
            this.verticesVisible =
                isVoid(options.verticesVisible) ?
                    getSetting(options.layerId + options.label, "verticesVisible", options.verticesVisible || true) :
                    options.verticesVisible;
            this.lSnapEnable =
                isVoid(options.lSnapEnable) ?
                    getSetting(options.layerId + options.label, "lSnapEnable", options.lSnapEnable || true) :
                    options.lSnapEnable;
            this.inclusive = options.inclusive;
            this.exclusive = options.exclusive;
            this.selectable =
                isVoid(options.selectable) ?
                    getSetting(options.layerId + options.label, "selectable", options.selectable || true) :
                    options.selectable;

            (<any>this.layer).getSqlVectorLayer = () => {
                return self;
            };
            this.searchControllerClassName = options.searchControllerClassName;
            if (isVoid(this.styleFunc)) {
                self.styleFunc = (feature: ol.Feature, resolution: any) => {
                    return self.defaultStyleFunc(feature, resolution);
                }
            }
            this.showSpatialSearchFilters = fallback(options.showSpatialSearchFilters, true);
        }






        private _previousSearchModel: TSearchDialogModel;


        public showSearchDialog(otherLayer: SqlVectorLayer<TGrpEntity, TSearchDialogModel>) {
            if (!this.isSearch) {
                throw "This method can be called only in search layers";
            }
            if (otherLayer.isSearch) {
                throw "The 'other' layer cannot be a search layer";
            }
            var self = this;
            var grp = this.Map.grp;
            var curEntity = <TGrpEntity>self.Current

            otherLayer.showSearchDialogPrivate(this, curEntity);
        }



        public onClearFilters() {
            this.getQueryUrl = undefined;
            this.getQueryInputs = undefined;
            this.getDynamicInfo = () => undefined;
            this.source.clear();
        }
        //        public getDynamicInfo(): NpTypes.DynamicSqlVectorInfo {
        //            return undefined;
        //        }

        public get persistedSearchModel(): TSearchDialogModel {
            return getSetting(this.layerId + this.label, "persistedSearchModel", undefined)
        }
        public set persistedSearchModel(newVal: TSearchDialogModel) {
            setSetting(this.layerId + this.label, "persistedSearchModel", newVal);
        }

        public get persistedSearchFilterQueryUrl(): string {
            return getSetting(this.layerId + this.label, "persistedSearchFilterQueryUrl", undefined)
        }
        public set persistedSearchFilterQueryUrl(newVal: string) {
            setSetting(this.layerId + this.label, "persistedSearchFilterQueryUrl", newVal);
        }

        public get persistedMasterLayerCode(): string {
            return getSetting(this.layerId + this.label, "persistedMasterLayerCode", undefined)
        }
        public set persistedMasterLayerCode(newVal: string) {
            setSetting(this.layerId + this.label, "persistedMasterLayerCode", newVal);
        }


        public get persistedDynamicSqlVectorInfo(): NpTypes.DynamicSqlVectorInfo {
            return getSetting(this.layerId + this.label, "persistedDynamicSqlVectorInfo", undefined)
        }
        public set persistedDynamicSqlVectorInfo(newVal: NpTypes.DynamicSqlVectorInfo) {
            setSetting(this.layerId + this.label, "persistedDynamicSqlVectorInfo", newVal);
        }

        public onApplyNewFilter(newGetQueryUrl: () => string, newGetQueryInputs: (x: TGrpEntity) => any, newDynamicInfo: () => NpTypes.DynamicSqlVectorInfo) {
            this.getQueryUrl = newGetQueryUrl
            this.getQueryInputs = newGetQueryInputs;
            this.getDynamicInfo = newDynamicInfo;
        }

        public recoverFromLocalStorage() {
            var self = this;
            var masterLayer = <SqlVectorLayer<NpTypes.IBaseEntity, Controllers.AbstractSqlLayerFilterModel>>this.Map.layers.firstOrNull(l => (l instanceof SqlVectorLayer) && (<SqlVectorLayer<NpTypes.IBaseEntity, Controllers.AbstractSqlLayerFilterModel>>l).layerId === this.persistedMasterLayerCode);
            if (!isVoid(masterLayer) && !isVoid(this.persistedSearchModel)) {
                this.onApplyNewFilter(
                    () => this.persistedSearchFilterQueryUrl,
                    (grpEnt: TGrpEntity) => {
                        var searchModel = self.persistedSearchModel
                        var ret = masterLayer.getFilterInputParams(searchModel, grpEnt);
                        ret['geoFunc'] = searchModel.geoFunc;
                        ret['geoFuncParam'] = searchModel.geoFuncParam;
                        return ret;
                    },
                    () => self.persistedDynamicSqlVectorInfo
                );
            }
        }


        public showSearchDialogPrivate(searchLayer: SqlVectorLayer<TGrpEntity, TSearchDialogModel>, grpEntity?: TGrpEntity) {
            var self = this;
            var grp = this.Map.grp;

            var dialogOptions = new NpTypes.SFLDialogOptions;
            dialogOptions.title = "Search " + self.label //self.LovFrmTitle;
            dialogOptions.previousModel = self._previousSearchModel;
            dialogOptions.className = this.searchControllerClassName;
            dialogOptions.dynInfo = self.getDynamicInfo();
            dialogOptions.onApplyNewFilter = (searchModel: TSearchDialogModel) => {
                searchLayer.persistedSearchModel = searchModel.getPersistedModel();
                searchLayer.persistedSearchFilterQueryUrl = self.getSearchFilterQueryUrl();
                searchLayer.persistedDynamicSqlVectorInfo = self.getDynamicInfo();
                searchLayer.persistedMasterLayerCode = self.layerId
                searchLayer.onApplyNewFilter(
                    self.getSearchFilterQueryUrl,
                    (grpEnt: TGrpEntity) => {
                        var ret = self.getFilterInputParams(searchModel, grpEnt);
                        if (!isVoid(searchModel) && !isVoid(searchModel.dialogOptions) && !isVoid(searchModel.dialogOptions.selectedGeometry)) {
                            ret['selectedGeometry'] = JSON.stringify(searchModel.dialogOptions.selectedGeometry);
                            ret['geoFunc'] = searchModel.geoFunc;
                            ret['geoFuncParam'] = searchModel.geoFuncParam;
                        }
                        return ret;
                    },
                    self.getDynamicInfo);
            }

            dialogOptions.onClearFilters = () => {
                searchLayer.onClearFilters();
            }


            var selectedFeaures = this.Map.selectInteraction.getFeatures();
            var length = selectedFeaures.getLength();
            var selectedFeature = length > 0 ? selectedFeaures.item(0) : undefined;
            dialogOptions.selectedGeometry = NpGeoGlobals.OL3feature_to_geoJSON(selectedFeature, this.Map.coordinateSystem);
            dialogOptions.showSpatialSearchParams = this.showSpatialSearchFilters;

            /*
                        dialogOptions.onPageDataReceived = (entities: NpGeo.SqlLayerEntity[]) => {
                            var newFeatures = entities.map(ent => ent.getFeature());
                            searchLayer.source.clear();
                            newFeatures.forEach((f: ol.Feature) => {
                                searchLayer.source.addFeature(f);
                            });
            
                        }
            */
            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, searchModel: TSearchDialogModel, excludedIds: Array<string> = []) => {
                //self._previousSearchModel = searchModel;
                var paramData = self.getFilterInputParams(searchModel, grpEntity);
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;

                //paramData['exc_Id'] = excludedIds;
                var model = searchModel;
                var url = self.getSearchFilterQueryUrl().replace("?", "");
                if (this.getDynamicInfo() !== undefined) {
                    paramData['layerCode'] = this.getDynamicInfo().code;
                    paramData['mode'] = 'GRID';
                } else {
                    url = url + "_grid?";
                }

                if (!isVoid(dialogOptions.selectedGeometry)) {
                    paramData['selectedGeometry'] = JSON.stringify(dialogOptions.selectedGeometry);
                    paramData['geoFunc'] = searchModel.geoFunc;
                    paramData['geoFuncParam'] = searchModel.geoFuncParam;
                }

                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }


                Utils.showWaitWindow(grp.$scope, grp.Plato, grp.$timeout);
                var wsPath = url.split('/').reverse().slice(0, 2).reverse().join('/').replace('?', '');
                var wsStartTs = (new Date).getTime();
                var promise = grp.$http.post(url, paramData, { timeout: grp.$scope.globals.timeoutInMS, cache: false });
                return promise.success(() => {
                    grp.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                    grp.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                })
            };



            var exportDataSet = (suffix: string, fileName: string, searchModel: TSearchDialogModel) => {
                var grp = this.Map.grp;
                var paramData = self.getFilterInputParams(searchModel, grpEntity);
                var model = searchModel;
                var url = self.getSearchFilterQueryUrl().replace("?", suffix);
                if (this.getDynamicInfo() !== undefined) {
                    paramData['layerCode'] = this.getDynamicInfo().code;
                }
                if (!isVoid(dialogOptions.selectedGeometry)) {
                    paramData['selectedGeometry'] = JSON.stringify(dialogOptions.selectedGeometry);
                    paramData['geoFunc'] = searchModel.geoFunc;
                    paramData['geoFuncParam'] = searchModel.geoFuncParam;
                }


                NpTypes.AlertMessage.clearAlerts(grp.PageModel);
                Utils.showWaitWindow(grp.$scope, grp.Plato, grp.$timeout);
                var wsPath = url.split('/').reverse().slice(0, 2).reverse().join('/').replace('?', '');
                var wsStartTs = (new Date).getTime();
                grp.$http.post(url, paramData, { timeout: grp.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                        grp.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                        if (grp.$scope.globals.nInFlightRequests > 0) grp.$scope.globals.nInFlightRequests--;
                        var data = base64DecToArr(response.data);
                        var blob = new Blob([data], { type: "application/octet-stream" });
                        saveAs(blob, fileName);
                    }).error(function (data, status, header, config) {
                        grp.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                        if (grp.$scope.globals.nInFlightRequests > 0) grp.$scope.globals.nInFlightRequests--;
                        //                        NpTypes.AlertMessage.addDanger(grp.PageModel, Messages.dynamicMessage(data));
                        NpTypes.AlertMessage.addDanger(grp.PageModel, data);
                    });

            }


            dialogOptions.ExportGeoJsonFile = (searchModel: TSearchDialogModel) => {
                exportDataSet("_geoJsonFile", "aafile.json", searchModel);
            }

            dialogOptions.ExportExcel = (searchModel: TSearchDialogModel) => {
                exportDataSet("_excelFile", "file.xls", searchModel);
            }


            dialogOptions.onRowSelect = (r: SqlLayerEntity) => {
                if (isVoid(r) || isVoid(r.getGeom())) {
                    return;
                }

                var feature = r.getFeature();
                if (isVoid(feature)) {
                    return;
                }

                searchLayer.source.clear();
                searchLayer.source.addFeature(feature);
                var extent = feature.getGeometry().getExtent();
                if (isVoid(extent))
                    return;

                var checkForPoint = Math.abs(extent[2] - extent[0]) + Math.abs(extent[3] - extent[1]);
                var view = this.Map.map.getView();
                if (checkForPoint < 10) {
                    view.setCenter([extent[0], extent[1]]);
                    var activeLayer = this.Map.tileSelect.getActiveTileLayer();
                    if (!isVoid(activeLayer))
                        view.setZoom(activeLayer.getSource().getTileGrid().getMaxZoom() - 4);
                } else
                    view.fit(extent, this.Map.map.getSize());

            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, model: TSearchDialogModel, excludedIds: Array<string>): string => {
                var paramData = self.getFilterInputParams(model, grpEntity);

                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return res;
            };

            var slf = this.getDynamicInfo() == undefined ? 'slf/' : '';

            grp.Plato.showDialog(grp.$scope,
                dialogOptions.title,
                "/" + grp.$scope.globals.appName + '/partials/' + slf + self.searchDialogPartialName + '?rev=' + grp.$scope.globals.version,
                dialogOptions);
        }




        public setLabelVisible(v: boolean) {
            this.labelVisible = v;
            setSetting(this.layerId + this.label, "labelVisible", v);
        }

        public setLSnapEnable(v: boolean) {
            //console.log("setting layer snapping=====", v);
            this.lSnapEnable = v;
            setSetting(this.layerId + this.label, "lSnapEnable", v);
        }

        public setVerticesVisible(v: boolean) {
            this.verticesVisible = v;
            setSetting(this.layerId + this.label, "verticesVisible", v);
        }

        public setIsVisible(v: boolean) {
            this.isVisible = v;
            setSetting(this.layerId + this.label, "isVisible", v);
        }

        public setSelectable(v: boolean) {
            this.selectable = v;
            setSetting(this.layerId + this.label, "selectable", v);
        }

        public setOpacity(n:number) {
            this.opacity = n;
            setSetting(this.layerId + this.label, "opacity", n);
        }

        public setOrderIndex(n: number) {
            this.orderIndex = n;
            setSetting(this.layerId + this.label, "orderIndex", n);
        }

        public setUsedForExclusions(v: boolean) {
            this.exclusiveFilter = v;
            setSetting(this.layerId + this.label, "isUsedForExclusions", v);
        }

        public getLabelVisible() {
            return this.labelVisible;
        }

        public getVerticesVisible() {
            return this.verticesVisible;
        }

        public getlSnapEnable() {
            return this.lSnapEnable;
        }

        public getSelectable() {
            return this.selectable;
        }

        public getOpacity() {
            return this.opacity;
        }

        public getOrderIndex() {
            return this.orderIndex;
        }

        private addCurrentWindowToParams(params: any) {
            var width = this.Map.map.getViewport().clientWidth;
            var height = this.Map.map.getViewport().clientHeight;
            var blFromMap = this.Map.map.getCoordinateFromPixel([0, height - 1]);
            var urFromMap = this.Map.map.getCoordinateFromPixel([width - 1, 0]);
            var srcProj = 'EPSG:3857';
            var dstProj = NpGeoGlobals.commonCoordinateSystems(this.Map.coordinateSystem);
            if (isVoid(dstProj)) {
                console.error("Can't send SqlVectorLayer query - missing map projection...");
                return;
            }
            var blNew = proj4(srcProj, dstProj, blFromMap);
            var urNew = proj4(srcProj, dstProj, urFromMap);
            params['xmin'] = blNew[0];
            params['ymin'] = blNew[1];
            params['xmax'] = urNew[0];
            params['ymax'] = urNew[1];
            if (this.getDynamicInfo() !== undefined) {
                params['layerCode'] = this.getDynamicInfo().code;
                params['mode'] = 'MAP';
            }
        }

        public removeSqlLayerFromLocalCache(sqlLayerId: string) {
            var self = this;
            var cacheKey = self.Map.mapId + "#@#" + self.layerId;
            var key = cacheKey + '#@#' + sqlLayerId;
            delete NpGeo.NpMap.cachedVectorsOfMap[key];
        }
        public fetchLayerGeometries() { 
            var grp = this.Map.grp;
            var self = this;
            var curEntity = <TGrpEntity>self.Current
            if (!isVoid(this.getQueryUrl) && !isVoid(curEntity) && this.layer.getVisible()) {
                var cacheKey = self.Map.mapId + "#@#" + self.layerId;
                var doTheWork = () => {
                    // console.log("Sending request:" + self.layerId+":"+self.timerBrakes);
                    self.timerBrakes = null;
                    var params = self.getQueryInputs(curEntity)

                    // Closured cache containers
                    var idsInCache = [];
                    var cachedIdsData: { [cacheKey: string]: Tuple2<SqlLayerEntity, number>; } = {};

                    // Keep this layer's ids (that are cached in the global cache) into the closured containers
                    // Why? Because in the time it takes for the web service call below to come back,
                    // some of these ids may have been lost (evicted from the cache, see eviction code below)
                    for (var key in NpGeo.NpMap.cachedVectorsOfMap) {
                        if (key.startsWith(cacheKey)) {
                            var pieces = key.split(/#@#/);
                            idsInCache.push(pieces[pieces.length - 1]);
                            cachedIdsData[key] = NpGeo.NpMap.cachedVectorsOfMap[key];
                        }
                    }
                    params['idsInCache'] = idsInCache;
                    self.addCurrentWindowToParams(params);
                    //clear all snap features
                    //this.Map.snapFeats.clear();
                    grp.httpPost(
                        self.getQueryUrl(),
                        params,
                        response => {
                            // Load all the new sqlEntities using the cache if possible

                            // console.log("Received: " + response.data.length + " entries");
                            var entities = _.map(
                                response.data,
                                sqlEntityData => {
                                    var se = new SqlLayerEntity(sqlEntityData, self.Map.coordinateSystem);
                                    var currentTimeInMS = new Date().getTime();
                                    if (isVoid(se.getGeom())) {
                                        var key = cacheKey + '#@#' + se.getId();
                                        var cacheEntry = NpGeo.NpMap.cachedVectorsOfMap[key];
                                        if (!isVoid(cacheEntry)) {
                                            // Update the usage timestamp of this cache entry
                                            cacheEntry.b = currentTimeInMS;
                                            // Cached value returned.
                                            se = cacheEntry.a;
                                        } else {
                                            // In the interim, this key has been evicted from the cache.
                                            // Re-insert it, and timestamp it.
                                            se = cachedIdsData[key].a; // Use the closured cache copy!
                                            NpGeo.NpMap.cachedVectorsOfMap[key] = new Tuple2(se, currentTimeInMS);
                                        }
                                    } else
                                        NpGeo.NpMap.cachedVectorsOfMap[cacheKey + '#@#' + se.getId()] =
                                            new Tuple2(se, currentTimeInMS);
                                    return se;
                                });

                            // Garbage collect the closured IDs cache
                            cachedIdsData = null;

                            // Garbage collect the oldest globally cached vectors
                            var thisLayerKeys =
                                Object.keys(NpGeo.NpMap.cachedVectorsOfMap).
                                    filter(x => x.startsWith(cacheKey));
                            if (thisLayerKeys.length > self.cacheLimit) {
                                thisLayerKeys.
                                    map(x => new Tuple2(x, NpGeo.NpMap.cachedVectorsOfMap[x].b)).
                                    sort((a1, a2) => a1.b - a2.b).
                                    slice(0, thisLayerKeys.length - self.cacheLimit).
                                    forEach(k => { delete NpGeo.NpMap.cachedVectorsOfMap[k.a]; })
                            }
                            var newFeatures = entities.map(ent => ent.getFeature());
                            self.source.clear(); 
                            //@p.tsagkis handle the snapping features 
                            //first need to remove any features of current looping layer
                            var featsArray = this.Map.snapFeats.getArray();
                            var clonedFeats = $.extend(true, [], featsArray);//deep clone it. Necessary step to hanldle collection array.
                            for (var f = 0; f < clonedFeats.length; f++) {                               
                                if (clonedFeats[f].get('layerId') === self.layerId) {
                                    this.Map.snapFeats.remove(clonedFeats[f]);
                                }
                            }
                            clonedFeats = [];//release it
                            //and now we need to add those new features
                            newFeatures.forEach((f: ol.Feature) => {
                                self.source.addFeature(f);
                                if (self.isVisible && self.lSnapEnable===true) {
                                    f.set('layerId', (<any>self.layer).get('layerId'));
                                   //console.log("pushing features. setting layer id", (<any>self.layer).get('layerId'));
                                    this.Map.snapFeats.push(f);
                                    
                                }
                            });
                            this.Map.snapInteraction.setActive(this.Map.snapEnabled);
                            
                        }); 
                };
                if (!isVoid(self.timerBrakes)) {
                    // console.log("Clearing pending request..." + self.layerId+":"+self.timerBrakes);
                    clearTimeout(self.timerBrakes);
                }
                self.timerBrakes = setTimeout(doTheWork, 500);     
            }
            
        }

        public onAttachMap() {
            var grp = this.Map.grp;
            var self = this;
            
            this.Map.snapFeats = new ol.Collection([]);
            this.Map.snapFeats.clear();
            

            if (this.isSearch) {
                this.recoverFromLocalStorage();
            }
            grp.$scope.$on('$destroy', <(evt: ng.IAngularEvent, ...cols: any[]) => any>(
                grp.$scope.$watch(
                    () => {
                        var curEntity = <TGrpEntity>self.Current
                        var params = self.getQueryInputs(curEntity)
                        self.addCurrentWindowToParams(params);
                        var visible = self.Map.isVisible()
                        return isVoid(curEntity) ? "" : JSON.stringify(params) + this.layer.getVisible().toString() + visible;
                    },
                    () => {
                        if (self.Map.isVisible()) {
                            (<ol.layer.Vector>this.layer).setStyle(
                                NpGeoGlobals.createPolygonWithVerticesStyleFunction(
                                    this.Map,
                                    this.styleFunc,
                                    this.fillColor, this.borderColor, this.labelColor, this.penWidth,
                                    undefined, this.verticesVisible, this.labelVisible));
                            self.fetchLayerGeometries();
                        }
                    }))); 
        }

        /**
         * @P.tsagkis
         * optimization has taken place for smart label placement
         * get the current mbr, intersect it with each feature is about to be labeled
         * and then place the text within the intersected geometry
         * The point returned should always be inside the polygon. THIS IS A RULE
         * Also handling the multipolygons case.
         * Find the larger one and label only this.
         * @16/2/2017 adding the poitn support 
         *
         * @param feature
         * @param resolution
         */
        public defaultStyleFunc(feature: ol.Feature, resolution: any) {  
            var fillColorMapped;
            var borderColorMapped;
            var labelColorMapped;
            var penWidth;
            if (typeof this.fillColor !== 'undefined') {
                fillColorMapped = (this.fillColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[this.fillColor] : this.fillColor;
            } else {
                fillColorMapped = 'rgba(255, 165, 0, 0.8)'; //default orange
            }

            if (typeof this.borderColor !== 'undefined') {
                borderColorMapped = (this.borderColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[this.borderColor] : this.borderColor;
            } else {
                borderColorMapped = 'rgba(0, 0, 0, 0.8)'; //default black
            }

            if (typeof this.labelColor !== 'undefined') {
                labelColorMapped = (this.labelColor in NpGeoGlobals.colorMap) ? NpGeoGlobals.colorMap[this.labelColor] : this.labelColor;
            } else {
                labelColorMapped = 'black'; //use black for default 
            }
            penWidth = (typeof this.penWidth !== 'undefined') ? this.penWidth : 7; //set a point size of 7 for deafault
            //console.log("fillColorMapped",fillColorMapped)
            var config = {};
            if (feature.getGeometry().getType() === 'Point') {
                config = {
                    image: new ol.style.Circle({
                        radius: penWidth,
                        fill: new ol.style.Fill({
                            color: fillColorMapped
                        }),
                        stroke: new ol.style.Stroke({
                            color: borderColorMapped,
                            width: 2
                        })
                    })
                }
            }
            else {
                config = {
                    stroke: new ol.style.Stroke({
                        color: borderColorMapped,
                        width: this.penWidth
                    }),
                    fill: new ol.style.Fill({
                        color: fillColorMapped
                    })
                }
            }

            
            var style = new ol.style.Style(config);
            
            var textStyleConfig = {
                text: NpGeoGlobals.createTextStyle(feature, resolution, this.labelVisible, labelColorMapped),
                geometry: function (feature) {
                    var mapExtent = NpGeoGlobals.globalMapThis.map.getView().calculateExtent(NpGeoGlobals.globalMapThis.map.getSize());
                    var extentGeom = new ol.geom.Polygon([[
                        [mapExtent[0], mapExtent[1]],
                        [mapExtent[0], mapExtent[3]],
                        [mapExtent[2], mapExtent[3]],
                        [mapExtent[2], mapExtent[1]],
                        [mapExtent[0], mapExtent[1]]
                    ]]);
                    
                    if (feature.getGeometry().getType() === 'MultiPolygon') {
                       
                        var intGeom = getJSTSIntersection(NpGeoGlobals.getMaxPoly(feature.getGeometry().getPolygons()), extentGeom);
                        if (intGeom.getType() === 'MultiPolygon') {
                            return NpGeoGlobals.getMaxPoly(intGeom.getPolygons()).getInteriorPoint();
                        } else {
                            return intGeom.getInteriorPoint();
                        }
                       
                    } else if (feature.getGeometry().getType() === 'Polygon') {
                        return feature.getGeometry().getInteriorPoint();
                    } else if (feature.getGeometry().getType() === 'Point' || feature.getGeometry().getType() == 'MultiPoint') {
                        return feature.getGeometry();
                    } else if (feature.getGeometry().getType() === 'LineString') {
                        return new ol.geom.Point(NpGeoGlobals.lineMidpoint(feature.getGeometry().getCoordinates()));
                    } else if (feature.getGeometry().getType() === 'MultiLineString') {
                        return new ol.geom.Point(NpGeoGlobals.lineMidpoint(NpGeoGlobals.getMaxLine(feature.getGeometry().getLineStrings()).getCoordinates()));
                    } else {//linestring (handled) need further manipulation for multilinestring,point,multipoint
                        console.error("wrong geometry type", feature.getGeometry());
                    }               
                    
                    
                }
            };
            var textStyle = new ol.style.Style(textStyleConfig);
            var style = new ol.style.Style(config);
            return [style,textStyle];
        }

        public onSelectFeature(feature: ol.Feature,lyr: ol.layer.Layer) {
            var html = this.Map.getFeatureLabel(feature, false);
            var infoString = '';
            if (!isVoid((<any>lyr).npName)) {
                infoString = " (" + (<any>lyr).npName +")";
            }
 
            if (!isVoid(html)) {
                messageBox(
                    this.Map.grp.$scope,
                    this.Map.Plato,
                    "Info " + infoString,
                    html,
                    IconKind.INFO,
                    [
                        new Tuple2("OK", () => { })
                    ], 0, 0, 'auto');
            }
        }

        public onInfoFeature(feature: ol.Feature) {
            console.log("feature data ios====", feature.entity.data);

        }


    }




    export class DynamicSqlVectorLayer extends SqlVectorLayer<NpTypes.IBaseEntity, Controllers.AbstractSqlLayerFilterModel>  {
        constructor(
            public appName: string,
            public inf: NpTypes.DynamicSqlVectorInfo) {
            super({
                layerId: inf.code,
                label: inf.label,
                isVisible: true,
                getQueryUrl: () => '/' + appName + '/rest/MainService/dynamicLayer_request?',
                getQueryInputs: (x: NpTypes.IBaseEntity) => { return {} },
                getSearchFilterQueryUrl: () => '/' + appName + '/rest/MainService/dynamicLayer_request?',
                getFilterInputParams: (srhModel: Controllers.AbstractSqlLayerFilterModel, naturalDisaster?: NpTypes.IBaseEntity) => { return {} },
                searchDialogPartialName: 'DynamicSqlLayerFilter.html',
                searchControllerClassName: 'DynamicSqlLayerFilterController',
                fillColor: inf.fillColor,
                borderColor: inf.borderColor,
                penWidth: inf.penWidth,
                orderIndex: inf.orderIndex,
                opacity: inf.opacity,
                getDynamicInfo: () => inf
            });
        }


    }

    
    export class WfsVectorLayer extends VectorLayer {
        timerForLoader: number = null;
        urlWFS: string;
        callbackName: string;
        limit: number;
        coordinateSystem: string;
        constructor(options: {
            label: string;
            isVisible: boolean;
            urlWFS: string;
            callbackName: string;
            limit?: number;
            fillColor?: string;
            borderColor?: string;
            penWidth?: number;
            opacity?: number;
            coordinateSystem?: string;
        }) {
            super({
                label: options.label,
                layer: null,
                layerId:null,
                isVisible: options.isVisible,
                fillColor: options.fillColor || 'blue',
                borderColor: options.borderColor,
                penWidth: options.penWidth,
                opacity: options.opacity || NpGeoGlobals.DEFAULT_OPACITY
            });
            var self = this;
            this.urlWFS = options.urlWFS;
            this.callbackName = options.callbackName;
            this.limit = fallback(options.limit, undefined);
            this.coordinateSystem = fallback(this.coordinateSystem, 'EPSG:3857');

            this.layer = new ol.layer.Vector({
                source: new ol.source.ServerVector({
                    format: new ol.format.GeoJSON({
                        defaultDataProjection: ol.proj.get(self.coordinateSystem)
                    }),
                    loader: (extent, resolution, projection) => {
                        self.onLoad(extent, resolution, projection);
                    },
                    strategy: ol.loadingstrategy.createTile(new ol.tilegrid.XYZ({
                        maxZoom: 19
                    }))
                }),
                style: new ol.style.Style({
                    stroke: new ol.style.Stroke({
                        color: self.fillColor, // 'rgba(0, 255, 255, 1.0)',
                        width: 2
                    })
                }),
            });
            //@p.tsagkis
            this.layer.setOpacity(this.opacity);
            this.layer.setZIndex(this.orderIndex);
            
            
            (<ol.layer.Vector>this.layer).setStyle(
                NpGeoGlobals.createPolygonStyleFunction(this.fillColor, this.borderColor, this.labelColor, this.penWidth, undefined));
        }

        onLoad(extent, resolution, projection) {
            var npWfs = this;
            var url = npWfs.urlWFS;
            if (!isVoid(npWfs.limit)) {
                url += '&count=' + npWfs.limit;
            }
            url += '&outputFormat=text/javascript&format_options=callback:';
            url += npWfs.callbackName;

            // The extent coming in from OpenLayers is wrong - use the current map viewport
            var width = npWfs.Map.map.getViewport().clientWidth;
            var height = npWfs.Map.map.getViewport().clientHeight;
            var blFromMap = npWfs.Map.map.getCoordinateFromPixel([0, height - 1]);
            var urFromMap = npWfs.Map.map.getCoordinateFromPixel([width - 1, 0]);
            var srcProj = projection.code_;
            if (isVoid(srcProj))
                srcProj = 'EPSG:3857';
            var dstProj = NpGeoGlobals.commonCoordinateSystems(npWfs.Map.coordinateSystem);
            if (isVoid(dstProj)) return;

            //var ul = [extent[0], extent[1]];
            //var br = [extent[2], extent[3]];
            var fixedExtent = [blFromMap[0], blFromMap[1], urFromMap[0], urFromMap[1]];
            var blNew = proj4(srcProj, dstProj, blFromMap);
            var urNew = proj4(srcProj, dstProj, urFromMap);
            var newExtent = [blNew[0], blNew[1], urNew[0], urNew[1]];
            url += '&srsName=' + projection.code_ + '&bbox=' + newExtent.join(',') + ',' + npWfs.Map.coordinateSystem;

            var doTheWork = () => {
                // console.log("From " + fixedExtent.join(' ### '));
                // console.log("To " + newExtent.join(' ### '));
                // console.log(url);
                $.ajax({
                    url: url,
                    dataType: 'jsonp'
                });
            };
            if (!isVoid(npWfs.timerForLoader))
                clearTimeout(npWfs.timerForLoader);
            npWfs.timerForLoader = setTimeout(doTheWork, 1000);
        }
        public get source(): ol.source.Vector {
            return this.layer.getSource();
        }
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
