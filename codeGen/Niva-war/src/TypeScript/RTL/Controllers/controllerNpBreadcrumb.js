/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../npTypes.ts" />
/// <reference path="../services.ts" />
var Controllers;
(function (Controllers) {
    var controllerNpBreadcrumb = (function () {
        function controllerNpBreadcrumb($scope, $timeout, Plato, LoginService, NavigationService) {
            this.$scope = $scope;
            this.$timeout = $timeout;
            this.Plato = Plato;
            this.LoginService = LoginService;
            this.NavigationService = NavigationService;
            var self = this;
            $scope.modelNavigation = NavigationService.modelNavigation;
            $scope.toggleMenu = function () { self.toggleMenu(); };
            $scope.logout = function () {
                LoginService.logout();
            };
        }
        controllerNpBreadcrumb.prototype.toggleMenu = function () {
            var self = this;
            function resize(target, new_width) {
                // http://stackoverflow.com/questions/7506104/jquery-ui-resizable-set-size-programmatically
                var $wrapper = $(target).resizable('widget'), $element = $wrapper.find('.ui-resizable'), dx = $element.width() - new_width;
                $element.width(new_width);
                $wrapper.width($wrapper.width() - dx);
                $('.TopFormArea').css('width', 'calc( 100% - ' + (new_width + 17) + 'px )');
                $('.fixedContainer').css('width', (parseInt($('.TopFormArea').css('width')) - 52) + 'px'); // subtract 52px for scrollbar buttons
            }
            if (self.$scope.modelNavigation.menuLabel === 'Hide menu') {
                self.$scope.modelNavigation.menuLabel = 'Show menu';
                self.$scope.modelNavigation.oldMenuAreaWidth = parseInt($('.resizable1').css('width'));
                self.$scope.modelNavigation.oldWorkAreWidth = parseInt($('.resizable2').css('width'));
                resize('resizable1', 0);
                $('.resizable2').css('left', '0%');
                $('.resizable2').css('width', '100%');
            }
            else {
                self.$scope.modelNavigation.menuLabel = 'Hide menu';
                resize('resizable1', self.$scope.modelNavigation.oldMenuAreaWidth);
                $('.resizable2').css('left', self.$scope.modelNavigation.oldMenuAreaWidth);
                $('.resizable2').css('width', self.$scope.modelNavigation.oldWorkAreWidth);
            }
            // Trigger resizes of all grids
            // We don't need this trigger since we added watcher of vissibility of 'resizable1' in rootscope (in controllerMain)
            //self.$scope.modelNavigation.epochOfLastResize = Utils.getTimeInMS();
        };
        return controllerNpBreadcrumb;
    })();
    Controllers.controllerNpBreadcrumb = controllerNpBreadcrumb;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerNpBreadcrumb.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
