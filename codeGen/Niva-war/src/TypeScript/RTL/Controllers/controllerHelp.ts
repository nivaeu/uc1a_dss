/// <reference path="../npTypes.ts" />

module Controllers {

    export interface IHelpScope extends NpTypes.IApplicationScope {
        closeHelp():void;
    }

    export class controllerHelp {

        constructor(public $scope: IHelpScope) {
            var dialogOptions: NpTypes.DialogOptions = <NpTypes.DialogOptions>$scope.globals.findAndRemoveDialogOptionByClassName("HelpClass");

            $scope.closeHelp = function() {
                dialogOptions.jquiDialog.dialog("close");
            }
        }
    }
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
