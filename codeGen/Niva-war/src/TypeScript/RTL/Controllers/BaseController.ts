﻿/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../DefinitelyTyped/jqueryui/jqueryui.d.ts" />
/// <reference path="../../DefinitelyTyped/jquery/jquery.d.ts" />
/// <reference path="../ng-grid-layout.ts" />
/// <reference path="../ng-grid-flexible-height.ts" />
/// <reference path="../sprintf.d.ts" />
/// <reference path="../npTypes.ts" />
/// <reference path="../utils.ts" />
/// <reference path="../services.ts" />

//DELETED
// /// <reference path="controllerNavigation.ts" />

/// <reference path="../geoLib.ts" />


module Controllers {

/*
    Items
*/

    export class Item<T> {
        constructor(
            public label: (ent?: NpTypes.IBaseEntity) => string,
            public unbounded: boolean,
            public itemValue: (ent?: NpTypes.IBaseEntity) => T,
            public uiModel: (ent?: NpTypes.IBaseEntity) => NpTypes.IUIModel,
            public isRequired: (ent?: NpTypes.IBaseEntity) => boolean,
            public isValid: (vl: T, ent?: NpTypes.IBaseEntity) => NpTypes.ValidationResult) {
        }
        public getLabelForMessage(scope: NpTypes.IApplicationScope, ent?: NpTypes.IBaseEntity): string {
            var self = this;
            var lbl: string = self.label(ent);
            if (isVoid(lbl)) return lbl;
            var translatedLabel: string = lbl
            if (!isVoid(scope))
                translatedLabel = scope.getALString(lbl, true);
            return translatedLabel.replace("<br/>", "&#160;").replace("&#160;&#160;", "&#160;");
        }
        public validateItem(scope: NpTypes.IApplicationScope, ent?: NpTypes.IBaseEntity): Array<string> {
            var self = this;
            var ret:Array<string> = [];
            var itmValue = self.itemValue(ent);
            var uiModelError = self.uiModel(ent).getErrors();
            for (var i = 0; i < uiModelError.length; i++) {
                ret.push(scope.globals.getDynamicMessage("FieldValidationErrorMsg(\"" + self.getLabelForMessage(scope, ent) + "\")") + ": " + uiModelError[i]);
            }

            //check item for errors only if there are not registered errors
            if (uiModelError.length == 0) {
                if (isVoid(itmValue) && self.isRequired(ent)) {
                    var errMsg1 = scope.globals.getDynamicMessage("FieldValidation_RequiredMsg1(\"" + self.getLabelForMessage(scope, ent) + "\")");
                    ret.push(errMsg1);
                    var errMsg2 = scope.globals.getDynamicMessage("FieldValidation_RequiredMsg2()");
                    self.uiModel(ent).addNewErrorMessage(errMsg2, false);
                }
                var custValidation = self.isValid(itmValue, ent);
                if (!custValidation.isValid)
                    ret.push(custValidation.errorMessage);
            }
            return ret;
        }
    }

    export class TextItem extends Item<string> {
        constructor(
            public label: (ent?: NpTypes.IBaseEntity) => string,
            public unbounded: boolean,
            public itemValue: (ent?: NpTypes.IBaseEntity) => string,
            public uiModel: (ent?: NpTypes.IBaseEntity) => NpTypes.IUIModel,
            public isRequired: (ent?: NpTypes.IBaseEntity) => boolean,
            public isValid: (vl: string, ent?: NpTypes.IBaseEntity) => NpTypes.ValidationResult,
            public maxLength?: number) {
            super(label, unbounded, itemValue, uiModel, isRequired, isValid);
        }

        public validateItem(scope: NpTypes.IApplicationScope, ent?: NpTypes.IBaseEntity): Array<string> {
            var self = this;
            var ret = super.validateItem(scope, ent);
            var itmValue = self.itemValue(ent);
            if (!isVoid(itmValue)) {
                if (!isVoid(self.maxLength) && itmValue.length > self.maxLength) {
                    ret.push(scope.globals.getDynamicMessage("FieldValidation_MaxLengthMsg(\"" + self.getLabelForMessage(scope, ent) + "\", \"" + itmValue + "\", " + self.maxLength + ")"));
                }
            }
            return ret;
        }
    }

    export class MapItem extends Item<ol.Feature> {
        constructor(
            public label: (ent?: NpTypes.IBaseEntity) => string,
            public unbounded: boolean,
            public itemValue: (ent?: NpTypes.IBaseEntity) => ol.Feature,
            public uiModel: (ent?: NpTypes.IBaseEntity) => NpTypes.IUIModel,
            public isRequired: (ent?: NpTypes.IBaseEntity) => boolean,
            public isValid: (vl: ol.Feature, ent?: NpTypes.IBaseEntity) => NpTypes.ValidationResult,
            public maxLength?: number) {
            super(label, unbounded, itemValue, uiModel, isRequired, isValid);
        }
    }

    export class DateOrNumberItem<T> extends Item<T> {
        constructor(
            public label: (ent?: NpTypes.IBaseEntity) => string,
            public unbounded: boolean,
            public itemValue: (ent?: NpTypes.IBaseEntity) => T,
            public uiModel: (ent?: NpTypes.IBaseEntity) => NpTypes.IUIModel,
            public isRequired: (ent?: NpTypes.IBaseEntity) => boolean,
            public isValid: (vl: T, ent?: NpTypes.IBaseEntity) => NpTypes.ValidationResult,
            public minVal?: () => T,
            public maxVal?: () => T) {
            super(label, unbounded, itemValue, uiModel, isRequired, isValid);
        }

        public validateItem(scope: NpTypes.IApplicationScope, ent?: NpTypes.IBaseEntity): Array<string> {
            var self = this;
            var ret = super.validateItem(scope, ent);
            var itmValue = self.itemValue(ent);
            var minVal : T = (isVoid(self.minVal) || isVoid(self.minVal())) ? undefined : self.minVal();
            var maxVal : T = (isVoid(self.maxVal) || isVoid(self.maxVal())) ? undefined : self.maxVal();
            if (!isVoid(itmValue)) {
                if (!isVoid(minVal) && !isVoid(maxVal) && (itmValue < minVal || itmValue > maxVal)) {
                    ret.push(scope.globals.getDynamicMessage("FieldValidation_MinMaxValueMsg(\"" + self.getLabelForMessage(scope, ent) + "\", \"" + itmValue + "\", \"" + maxVal + "\", \"" + minVal + "\")"));
                } else if (!isVoid(minVal) && itmValue < minVal) {
                    ret.push(scope.globals.getDynamicMessage("FieldValidation_MinValueMsg(\"" + self.getLabelForMessage(scope, ent) + "\", \"" + itmValue + "\", \"" + minVal + "\")"));
                } else if (!isVoid(maxVal) && itmValue > maxVal) {
                    ret.push(scope.globals.getDynamicMessage("FieldValidation_MaxValueMsg(\"" + self.getLabelForMessage(scope, ent) + "\", \"" + itmValue + "\", \"" + maxVal + "\")"));
                }
            }
            return ret;
        }
    }


    export class NumberItem extends DateOrNumberItem<number> {
        constructor(
            public label: (ent?: NpTypes.IBaseEntity) => string,
            public unbounded: boolean,
            public itemValue: (ent?: NpTypes.IBaseEntity) => number,
            public uiModel: (ent?: NpTypes.IBaseEntity) => NpTypes.IUIModel,
            public isRequired: (ent?: NpTypes.IBaseEntity) => boolean,
            public isValid: (vl: number, ent?: NpTypes.IBaseEntity) => NpTypes.ValidationResult,
            public minVal?: () => number,
            public maxVal?: () => number,
            public maxDecimals?: number) {
            super(label, unbounded, itemValue, uiModel, isRequired, isValid);
        }
        public validateItem(scope: NpTypes.IApplicationScope, ent?: NpTypes.IBaseEntity): Array<string> {
            
            var self = this;
            var ret = super.validateItem(scope, ent);
            var itmValue = self.itemValue(ent);
            if (!isVoid(self.maxDecimals) && !isVoid(itmValue)) {
                var delta = parseFloat(itmValue.toFixed(self.maxDecimals + 1)) - parseFloat(itmValue.toFixed(self.maxDecimals))
                if (delta !== 0) {
                    ret.push(scope.globals.getDynamicMessage("FieldValidation_MaxDecimalsMsg(\"" + self.getLabelForMessage(scope, ent) + "\", \"" + itmValue + "\", \"" + self.maxDecimals + "\")"));
                }
            }
            return ret;
        }
    }

    export class DateItem extends DateOrNumberItem<Date> {
    }
    export class LovItem extends Item<NpTypes.IBaseEntity> {
    }

    export class LovItem2<
        TGrpEntity extends NpTypes.IBaseEntity,
        TLovEntity extends NpTypes.IBaseEntity,
        TLovModel extends AbstractLovModel> extends Item<TLovEntity> {

        constructor(
            public label: (ent?: TGrpEntity) => string,
            public unbounded: boolean,
            public itemValue: (ent?: TGrpEntity) => TLovEntity,
            public uiModel: (ent?: TGrpEntity) => NpTypes.IUIModel,
            public isRequired: (ent?: TGrpEntity) => boolean,
            public isValid: (vl: TLovEntity, grpEnt?: TGrpEntity) => NpTypes.ValidationResult,

            public grp: AbstractController,
            public LovFrmTitle: string,
            public sLovClassName: string,
            public bMultiSelect: boolean,
            public setLovValue: (lovEnt: TLovEntity, grpEnt?: TGrpEntity) => void,
            public onValueChange: (grpEnt?: TGrpEntity) => void,
            public partialName: string,

            public bHasNewButton: boolean,
            public sEditPageTitle: string,
            public sEditFrmWidth: string,
            public soEditPageClassName: string,
            public sEditPagePartial: string,
            public sLovEntityName: string,
            public sMainQueryID : string,
            public sGetIdsQueryID : string,
            public setExtraReturnValues: (grpEnt: TGrpEntity, lovEnt: TLovEntity) => void = (grpEnt, lovEnt) =>{},
            public getInputParams: (lovModel: TLovModel, grpEnt?: TGrpEntity) => any = (lovModel: TLovModel) => { return {}; },
            public shownCols: { [id: string]: boolean; } = {}
            )

        {
            super(label, unbounded, itemValue, uiModel, isRequired, isValid);
        }

        private _previousLovModel: TLovModel;

        public showBoundedLov(grpEntity?: TGrpEntity)
        {
            var self = this;

            function httpPost(url: string, paramData: any, wsPath: string): ng.IHttpPromise<any> {
                var promise = self.grp.$http.post(
                    url,
                    paramData,
                    { timeout: self.grp.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(() => {
                    self.grp.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error((data, status, header, config) => {
                        self.grp.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    });
            }

            var dialogOptions = new NpTypes.LovDialogOptions();
            dialogOptions.title = self.LovFrmTitle;
            dialogOptions.previousModel = self._previousLovModel;
            dialogOptions.className = self.sLovClassName;
            dialogOptions.bMultiSelect = self.bMultiSelect;
            dialogOptions.makeWebRequestGetIds = (lovModel: TLovModel, excludedIds: Array<string>= []) => {
                var wsPath = self.sLovEntityName + '/' + self.sGetIdsQueryID;
                var url = "/" + self.grp.$scope.globals.appName + '/rest/' + wsPath + '?';
                var paramData = self.getInputParams(lovModel, grpEntity);

                paramData['exc_Id'] = excludedIds;


                Utils.showWaitWindow(self.grp.$scope, self.grp.Plato, self.grp.$timeout);
                return httpPost(url, paramData, wsPath);
            }

            var uimodel = self.uiModel(grpEntity);
            dialogOptions.onSelect = (selectedEntity: TLovEntity) => {
                uimodel.clearAllErrors();
                if (self.isRequired(grpEntity) && isVoid(selectedEntity)) {
                    uimodel.addNewErrorMessage(self.grp.$scope.globals.getDynamicMessage("FieldValidation_RequiredMsg2()"), true);
                    self.grp.$timeout(() => {
                        uimodel.clearAllErrors();
                    }, 3000);
                    return;
                }
                var validationResult = self.isValid(selectedEntity, grpEntity);
                if (!validationResult.isValid) {
                    uimodel.addNewErrorMessage(validationResult.errorMessage, true);
                    self.grp.$timeout(() => {
                        uimodel.clearAllErrors();
                    }, 3000);
                    return;
                }

                if (!isVoid(selectedEntity) && selectedEntity.isEqual(self.itemValue(grpEntity)))
                    return;
                self.setLovValue(selectedEntity, grpEntity);
                //self.grp.markEntityAsUpdated(grpEntity, '<sItemId>');
                self.onValueChange(grpEntity);
                self.setExtraReturnValues(grpEntity, selectedEntity);
            }

            dialogOptions.hasNewButton = self.bHasNewButton;

            dialogOptions.openNewEntityDialog = () => {
                var dlgOptions = new NpTypes.DialogOptions();
                dlgOptions.title = self.sEditPageTitle;
                dlgOptions.width = self.sEditFrmWidth;
                dlgOptions.className = self.soEditPageClassName;
                self.grp.Plato.showDialog(self.grp.$scope,
                    dlgOptions.title,
                    "/" + self.grp.$scope.globals.appName + '/partials/' + self.sEditPagePartial + '?rev=' + self.grp.$scope.globals.version,
                    dlgOptions);
            }

            dialogOptions.makeWebRequest = (paramIndexFrom: number, paramIndexTo: number, lovModel: TLovModel, excludedIds:Array<string>=[]) => {
                self._previousLovModel = lovModel;
                var paramData = self.getInputParams(lovModel, grpEntity);
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;

                paramData['exc_Id'] = excludedIds;
                var model = lovModel;


                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }


                Utils.showWaitWindow(self.grp.$scope, self.grp.Plato, self.grp.$timeout);
                var wsPath = self.sLovEntityName + '/' + self.sMainQueryID;
                var url = "/" + self.grp.$scope.globals.appName + '/rest/' + wsPath + '?';

                return httpPost(url, paramData, wsPath);
            };

            dialogOptions.getWebRequestParamsAsString = (paramIndexFrom: number, paramIndexTo: number, lovModel: TLovModel, excludedIds: Array<string>): string => {
                var paramData = self.getInputParams(lovModel, grpEntity);

                var model = lovModel;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                var res = Object.keys(paramData).map(key => key + ":" + paramData[key]).join("#");
                return res;
            };
            dialogOptions.shownCols = self.shownCols;
            self.grp.Plato.showDialog(self.grp.$scope,
                dialogOptions.title,
                "/" + self.grp.$scope.globals.appName + '/partials/lovs/' + self.partialName+ '?rev=' + self.grp.$scope.globals.version,
                dialogOptions);
        }



        public findByCode(newCodeValue: string, component: NpTypes.NpLookup, grpEntity?: TGrpEntity) {
            var self = this;
            var uimodel = self.uiModel(grpEntity);
            var onSuccess = (selectedEntity: TLovEntity) => {
                uimodel.clearAllErrors();
                if (self.isRequired(grpEntity) && isVoid(selectedEntity)) {
                    uimodel.addNewErrorMessage(self.grp.$scope.globals.getDynamicMessage("FieldValidation_RequiredMsg2()"), true);
                    //component.element.val(<if (bBounded)> <soGroupEntityName.L1>.<else>self.<sModelVarName>.< endif > <sLovFieldName>.< sCodeField>);
                    self.grp.$timeout(() => {
                        uimodel.clearAllErrors();
                    }, 3000);
                    return;
                }
                var validationResult = self.isValid(selectedEntity, grpEntity);
                if (!validationResult.isValid) {
                    uimodel.addNewErrorMessage(validationResult.errorMessage, true);
                    self.grp.$timeout(() => {
                        uimodel.clearAllErrors();
                    }, 3000);
                    return;
                }

                if (!isVoid(selectedEntity) && selectedEntity.isEqual(self.itemValue(grpEntity)))
                    return;
                self.setLovValue(selectedEntity, grpEntity);
                //self.grp.markEntityAsUpdated(grpEntity, '<sItemId>');
                self.onValueChange(grpEntity);
                self.setExtraReturnValues(grpEntity, selectedEntity);
            }

        }
        /*
    public findByCode(newCodeValue:string, component:NpTypes.NpLookup, grpEntity?: TGrpEntity) {
        var self = this;
        var uimodel: NpTypes.IUIModel = <if (bBounded)> <soGroupEntityName.L1>.<else>self.<sModelVarName>.< endif > _ < sLovFieldName>;
        var onSuccess = (<sLovEntityName.L1>1:Entities.<sLovEntityName>) => {
            uimodel.clearAllErrors();

        // if oldModelValue != null and newValue == null check for required
        if (!isVoid(<if (bBounded)> <soGroupEntityName.L1>.<else>self.<sModelVarName>.< endif ><sLovFieldName>) && isVoid(<sLovEntityName.L1>1)) {
                if (<sRequiredExpression>) {
                    uimodel.addNewErrorMessage('Υποχρεωτικό πεδίο', true);
                    component.element.val(<if (bBounded)> <soGroupEntityName.L1>.<else>self.<sModelVarName>.< endif > <sLovFieldName>.< sCodeField>);
                    self.$timeout(() => {
                        uimodel.clearAllErrors();
                    }, 3000);

                    return;
                }
            }
        <if (soIsValidMethod)>
        var validationResult = self.<soIsValidMethod>(<sLovEntityName.L1>1<if (soGroupEntityName)>, <soGroupEntityName.L1><endif>);
            if (!validationResult.isValid) {
                uimodel.addNewErrorMessage(validationResult.errorMessage, true);
                self.$timeout(() => {
                    uimodel.clearAllErrors();
                }, 3000);
            if (!isVoid(<if (bBounded)> <soGroupEntityName.L1>.<else>self.<sModelVarName>.< endif ><sLovFieldName>)) {
                    component.element.val(<if (bBounded)> <soGroupEntityName.L1>.<else>self.<sModelVarName>.< endif > <sLovFieldName>.< sCodeField>);
                }
                return;
            }

        <endif>
        if (!isVoid(<sLovEntityName.L1>1) && <sLovEntityName.L1>1.isEqual(<if (bBounded)> <soGroupEntityName.L1>.<else>self.<sModelVarName>.< endif ><sLovFieldName>))
            return;


        <if (bBounded)> <soGroupEntityName.L1>.<else>self.<sModelVarName>.< endif ><sLovFieldName> = <sLovEntityName.L1>1;

        <if (bBounded)>
                self.markEntityAsUpdated(<soGroupEntityName.L1>, '<sItemId>');

            <endif>
        <arrsExtraReturns; separator = "\n" >
        <if (sOnValueChangeMethod)>
        self.<sOnValueChangeMethod>(<if (soGroupEntityName)> <soGroupEntityName.L1><endif>);

        <endif>

    };

        var onError = (err: string) => {
            uimodel.addNewErrorMessage("Δεν βρέθηκε εγγραφή για το '" + newCodeValue + "'", false);
        if (!isVoid(<if (bBounded)> <soGroupEntityName.L1>.<else>self.<sModelVarName>.< endif ><sLovFieldName>)) {
                component.element.val(<if (bBounded)> <soGroupEntityName.L1>.<else>self.<sModelVarName>.< endif > <sLovFieldName>.< sCodeField>);
            }
            self.$timeout(() => {
                uimodel.clearAllErrors();
            }, 3000);
        }

    if (isVoid(newCodeValue) || (newCodeValue === "")) {
            onSuccess(null);
        } else {
        Entities.<sLovEntityName>.<sFindByCodeFuncName>(<arrsFindByCodeInputArgs; separator =", ">, self.$scope, self.$http, onSuccess, onError);
        }
    }



    */

    }

    export class CheckBoxItem extends Item<boolean> {
    }
    export class BlobItem extends Item<string> {
    }
    export class StaticListItem<T> extends Item<T> {
    }

    export interface IRegion {
        pageController: AbstractPageController;
        groupController?: AbstractGroupController;
        parentRegion: IRegion;
        label: string;
        isDisanled(ent: NpTypes.IBaseEntity): boolean;
        isInvisible(ent: NpTypes.IBaseEntity): boolean;
    }

    export class ItemRegion /*implements IRegion*/ {
        items: Item<any>[];
    }

    export class RegionContainer /*implements IRegion*/ {
        regions: IRegion[];
    }
    
/*
    Groups
*/

    export enum LockKind {
        DisabledLock, ReadOnlyLock, InvisibleLock, DeleteLock, UpdateLock
    }

    export enum ChangeStatus {
        NEW,
        UPDATE,
        DELETE
    }

    export class ChangeToCommit {
        constructor(public status: ChangeStatus, public when:number, public entityName:string, public entity:any) {
        }
    }

    export class ChangedEntity extends Tuple2<NpTypes.IBaseEntity, number> {
    }




    // Group Models
    export class AbstractGroupModel {
        public visibleEntities: Array<NpTypes.IBaseEntity> = [];
        public selectedEntities: Array<NpTypes.IBaseEntity> = [];
        public lastRequestId: number = 0;
        newEntities: Array<ChangedEntity> = [];
        updatedEntities: { [id: string]: ChangedEntity; } = {};
        deletedEntities: { [id: string]: ChangedEntity; } = {};
        constructor(public $scope: IAbstractGroupScope) { }
        public getPersistedModel(): any {
            return {};
        }
        public setPersistedModel(vl: any) {
        }
        
    }

    export class AbstractGroupTableModel extends AbstractGroupModel {
        showSearchParams: boolean = false;
        grid: any;
        pagingOptions: any;
        updateTotalCompleted: boolean = false;
        nInProgressCounts: number = 0;
        totalItems: number = 0;
        flexibleHeightPlugin: any;
        flexibleLayoutPlugin: any;
        sortField:string;
        sortOrder:string;


        constructor(public $scope: IAbstractTableGroupScope) { super($scope);}
    }

    export class AbstractGroupFormModel extends AbstractGroupModel {
        constructor(public $scope: IAbstractFormGroupScope) { super($scope); }
    }


    // Group Scopes
    export interface IAbstractControllerScope extends NpTypes.IApplicationScope {
        getLabelById(itmId: string): string;
    }

    export interface IAbstractGroupScope extends IAbstractControllerScope {
        model: any;
        focusHack(number): void;
        isItemDisabled(x: NpTypes.IBaseEntity): boolean;
        isItemInvisible(x: NpTypes.IBaseEntity): boolean;
        createNewEntityAndAddToUI(): NpTypes.IBaseEntity;
        showAuditInfo(): void;
        multipleInsertEntities(): void;
        markEntityAsUpdated(ent: NpTypes.IBaseEntity, itemId: string): void;
        isDisabled(): boolean;
        isInvisible(): boolean;
        isTabActive(tabRegionId: string): boolean;
        newIsDisabled(): boolean;
        multInsertIsDisabled(): boolean;
        expToExcelIsDisabled(): boolean;
        deleteIsDisabled(cur: NpTypes.IBaseEntity): boolean;
        deleteEntity(x: NpTypes.IBaseEntity, triggerUpdate: boolean, rowIndex: number, bCascade?: boolean): void;
        cloneEntity(src: NpTypes.IBaseEntity): NpTypes.IBaseEntity;
    }

    export interface IAbstractFormGroupScope extends IAbstractGroupScope {
    }


    export interface IAbstractTableGroupScope extends IAbstractGroupScope {
        gridTitle(): string;
        getColumnTooltip(fieldName: string): string
        updateGrid(): void;
        setSelectedRow(number): void;
        gridPageDown(): void;
        gridPageUp(): void;
        clearBtnAction(): void;
        selectEntityCheckBoxClicked(x: NpTypes.IBaseEntity): void;
        isEntitySelected(x: NpTypes.IBaseEntity): boolean;
        unUselectActiveSetEntities();
        selectActiveSetEntities();
        isMultiSelect(): boolean;
        exportToExcelBtnAction();
    }




    export class AbstractController implements NpTypes.IAbstractController {
        constructor(
            public $scope: IAbstractControllerScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker)
        {
            $scope.getLabelById = itmId => this.getLabelById(itmId);
        }

        public $q: ng.IQService;
        public NavigationService: Services.NavigationService;

        public initialize(navigationService: Services.NavigationService, qService: ng.IQService): void {
            var self = this;
            self.NavigationService = navigationService;
            self.$q = qService;
        }

        public dynamicMessage(sMsg: string):string {
            throw "not implemented function"
        }

        getALString = this.$scope.getALString;

        public getLabelById(itmId: string): string {
            switch (itmId) {
                case 'saveBtn_id':
                    return 'Save'
                case 'searchBtn_id':
                    return 'Search';
                case 'clearBtn_id':
                    return 'Clear';
                case 'newBtn_id':
                case 'newPageBtn_id':
                    return 'New Record';
                case 'cancelBtn_id':
                    return 'Back';
                case 'deleteBtn_id':
                    return 'Delete';
                case 'auditBtn_id':
                    return '?';
            }
            return itmId;
        }

        
        public get AppName(): string {
            return this.$scope.globals.appName;
        }
        public get ControllerClassName(): string {
            throw "Not implemented function:PageModel";
        }

        public get PageModel(): NpTypes.IPageModel {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:PageModel");
        }

        public httpPost(url: string, data: any, successFunc: (response: any) => void, errFunc?: (response: any) => void, requestConfig?: any) {
            var timeout: number = !isVoid(requestConfig) && !isVoid(requestConfig.timeout) ? requestConfig.timeout : this.$scope.globals.timeoutInMS;
            this.httpMethod(url, data, successFunc,
                () => this.$http.post(url, data, { timeout: timeout, cache: false }), errFunc);
        }

        public httpGet(url: string, data: any, successFunc: (response: any) => void, errFunc?: (response: any) => void, requestConfig?: any) {
            var timeout: number = !isVoid(requestConfig) && !isVoid(requestConfig.timeout) ? requestConfig.timeout : this.$scope.globals.timeoutInMS;
            this.httpMethod(url, data, successFunc,
                () => this.$http.get(url, { timeout: timeout, cache: false, params: data }), errFunc);
        }

        public getWsPathFromUrl(url: string): string { return url.split('/').reverse().slice(0, 2).reverse().join('/').replace('?', ''); }

        private ongoingRequests: { [id: string]: boolean; } = {};
        private httpMethod(url: string, data: any, successFunc: (response: any) => void, httpMethod: () => ng.IHttpPromise<any>, errFunc?: (response: any) => void) {
            var requestKey = url + JSON.stringify(data);
            var makeWebRequest =
                this.ongoingRequests[requestKey] === undefined;
            if (makeWebRequest) {
                this.ongoingRequests[requestKey] === true;
                Utils.showWaitWindow(this.$scope, this.Plato, this.$timeout);
                var wsPath = this.getWsPathFromUrl(url);
                var wsStartTs = (new Date).getTime();
                httpMethod().
                    success((response, status, header, config) => {
                        this.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                        if (this.$scope.globals.nInFlightRequests > 0) this.$scope.globals.nInFlightRequests--;
                        delete this.ongoingRequests[requestKey];
                        successFunc(response);
                    }).
                    error((data, status, header, config) => {
                        this.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                        if (this.$scope.globals.nInFlightRequests > 0) this.$scope.globals.nInFlightRequests--;
                        delete this.ongoingRequests[requestKey];
                        var errMsg: string = data;
                        if (status === 0 && (isVoid(errMsg) || errMsg === "")) {
                            errMsg = "HttpRequestTimeOutMessage";
                        }
                        if (errFunc === undefined) {
                            NpTypes.AlertMessage.addDanger(this.PageModel, this.dynamicMessage(errMsg));
                        } else {
                            errFunc(errMsg);
                        }
                        console.error("Error received in makeWebRequest:" + data);
                        console.dir(status);
                        console.dir(header);
                        console.dir(config);
                    });
            }

        }

    }



    
    // Group Controllers
    export class AbstractGroupController extends AbstractController {
        constructor(
            public $scope: IAbstractGroupScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker,
            public model: AbstractGroupModel)
        {
            super($scope, $http, $timeout, Plato);
            var self = this;
            $scope.model = model;
            model.selectedEntities = [];
            model.visibleEntities = [];

            $scope.newIsDisabled = () => { return self._newIsDisabled(); };
            $scope.multInsertIsDisabled = () => { return self._multInsertIsDisabled();};
            $scope.expToExcelIsDisabled = () => { return self._expToExcelIsDisabled(); };
            $scope.deleteIsDisabled = (ent: NpTypes.IBaseEntity) => { return self._deleteIsDisabled(ent); };
            $scope.isDisabled = () => { return self._isDisabled(); };
            $scope.isInvisible = () => { return self._isInvisible(); };
            $scope.isTabActive = (tabRegionId: string): boolean => {
                return self.isTabActive(tabRegionId);
            }

            $scope.isItemDisabled = (x: NpTypes.IBaseEntity) => {
                return false;
            };
            $scope.isItemInvisible = (x: NpTypes.IBaseEntity) => {
                return false;
            };
            $scope.createNewEntityAndAddToUI = () => {
                return self.createNewEntityAndAddToUI();
            };
            $scope.showAuditInfo = () => {
                self.showAuditInfo();
            }
            $scope.multipleInsertEntities = () => {
                self.multipleInsertEntities();
            };
            $scope.markEntityAsUpdated = (ent: NpTypes.IBaseEntity, itemId: string) => {
                self.markEntityAsUpdated(ent, itemId);
            };
            $scope.deleteEntity = (c: NpTypes.IBaseEntity, triggerUpdate: boolean, rowIndex: number, bCascade?:boolean) => {
                self.deleteEntity(c, triggerUpdate, rowIndex, bCascade);
            };

            $scope.$on('$destroy', (evt: ng.IAngularEvent, ...cols: any[]): void => {
                $scope.model.visibleEntities.splice(0);
                $scope.model.visibleEntities = null;
                $scope.model.selectedEntities.splice(0);
                $scope.model.selectedEntities = null;
                $scope.model = null;
            });
            $scope.cloneEntity = (src: NpTypes.IBaseEntity) => {
                return self.cloneEntity(src);
            }


        }
        public cloneEntity(src: NpTypes.IBaseEntity): NpTypes.IBaseEntity {
            throw "unimplemented function";
        }
        public onCloneEntity(src: NpTypes.IBaseEntity): void {
        }

        public _newIsDisabled(): boolean {
            return false;
        }
        public _multInsertIsDisabled(): boolean {
            return false;
        }
        public _expToExcelIsDisabled(): boolean {
            return false;
        }
        public _deleteIsDisabled(ent: NpTypes.IBaseEntity): boolean {
            return false;
        }
        public _isDisabled(): boolean {
            return false;
        }
        public _isInvisible(): boolean {
            return false;
        }
        public isParentTabUnderConstruction(divId: string): boolean {
            var tabRegionDiv = $('#' + divId);
            var parentTabControls = tabRegionDiv.closest('div.ui-tabs');

            if (parentTabControls.length === 0)
                return false;    // not tab control

            //every surrounding tab control must have the .ui-widget class
            var bSomeTabAreUnderConstruction = parentTabControls.not('.ui-widget').length > 0;
            if (bSomeTabAreUnderConstruction)
                return true;

            return false;
        }
        public isTabActive(tabRegionId: string): boolean {
            var tabRegionDiv = $('#' + tabRegionId);
            var parentTabControls = tabRegionDiv.closest('div.ui-tabs');

            if (parentTabControls.length === 0)
                return true;    // not tab control

            //every surrounding tab control must have the .ui-widget class
            var bSomeTabAreUnderConstruction = parentTabControls.not('.ui-widget').length > 0;

            if (bSomeTabAreUnderConstruction)
                return true;

            var ulTabsNav = parentTabControls.tabs().children('ul.ui-tabs-nav');
            if (isVoid(ulTabsNav))
                return true;
            var activeTab = ulTabsNav.find('li.ui-state-active');
            if (isVoid(activeTab))
                return true;
            var activeRegionId = activeTab.attr('aria-controls');
            if (isVoid(activeRegionId))
                return true;
            return activeRegionId === tabRegionId;
        }


        public cleanUpAfterSave() {
            var self = this;
            self.model.deletedEntities = {};
            self.model.updatedEntities = {};
            self.model.newEntities = [];
        }

        public constructEntity(): NpTypes.IBaseEntity {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:constructEntity");
        }

        public lockParent(): boolean {
            return false;
        }

        public get Parent(): NpTypes.IBaseEntity {
            return undefined;
        }

        public get ParentController(): AbstractGroupController {
            return undefined;
        }
        
        public markParentEntityAsUpdated(itemId: string) {
            var self = this;
            if (!self.lockParent())
                return;
            if (isVoid(self.ParentController) || isVoid(self.Parent) || self.Parent.isNew())
                return;
            self.ParentController.markEntityAsUpdated(self.Parent, itemId);
        }

        // called by buttons Add
        // groups must override this method in 
        public createNewEntityAndAddToUI(): NpTypes.IBaseEntity {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:createNewEntityAndAddToUI");
        }

        public get Current(): NpTypes.IBaseEntity {
            return this.model.selectedEntities[0];
        }

        public get CurrentRowIndex(): number {
            var rowIndex: number = 0;
            if (!isVoid(this.Current) && !isVoid(this.model.visibleEntities) && this.model.visibleEntities.length > 0) {
                this.model.visibleEntities.some((ent, indx) => {
                    if (ent.getKey() === this.Current.getKey()) {
                        rowIndex = indx;
                        return true;
                    }
                });
            }
            
            return rowIndex;
        }

        public showAuditInfo(): void {
            var self = this;

            if (!self.$scope.globals.hasPrivilege(self.$scope.globals.appName + "_" + "ShowAuditInfo"))
                return

            function showAuditInfoMessage(usrIns: string, dteIns: string, usrUpd: string, dteUpd: string): void {
                messageBox(self.$scope, self.Plato, self.getALString("Audit Info of Record"),
                        '<table>' +
                            '<tbody>' +
                                '<tr>' +
                                    '<td colspan="1" style="width:40%"><label class="text-right" style="font-size:13px !important; font-weight:bold !important; min-width:10em" >' + self.getALString("Insert User") + ':</label></td>' +
                                    '<td colspan="1" style="width:60%"><label class="text-left" style="font-size:13px !important">' + usrIns + '</label></td>' +
                                '</tr>' +
                                '<tr>' +
                        '<td colspan="1" style="width:40%"><label class="text-right" style="font-size:13px !important; font-weight:bold !important; min-width:10em">' + self.getALString("Insert Timestamp") + ':</label></td>' +
                                    '<td colspan="1" style="width:60%"><label class="text-left" style="font-size:13px !important">' + dteIns + '</label></td>' +
                                '</tr>' +
                                '<tr>' +
                            '<td colspan="1" style="width:40%"><label class="text-right" style="font-size:13px !important; font-weight:bold !important; min-width:10em">' + self.getALString("Last Update User") + ':</label></td>' +
                                    '<td colspan="1" style="width:60%"><label class="text-left" style="font-size:13px !important">' + usrUpd + '</label></td>' +
                                '</tr>' +
                                '<tr>' +
                                '<td colspan="1" style="width:40%"><label class="text-right" style="font-size:13px !important; font-weight:bold !important; min-width:10em">' + self.getALString("Last Update Timestamp") + ':</label></td>' +
                                    '<td colspan="1" style="width:60%"><label class="text-left" style="font-size:13px !important">' + dteUpd + '</label></td>' +
                                '</tr>' +
                            '</tbody>' +
                        '</table>',
                    IconKind.INFO,
                    [new Tuple2("OK", () => { })], 0, 0, '30em');
            }

            var cr = this.Current;
            if (!isVoid(cr)) {
                var ai = cr.getAuditInfo();
                var dteIns = !isVoid(ai) && !isVoid(ai.dateInsert) ? Utils.convertDateToString(ai.dateInsert, 'dd/MM/yyyy HH:mm:ss') : '';
                var dteUpd = !isVoid(ai) && !isVoid(ai.dateUpdate) ? Utils.convertDateToString(ai.dateUpdate, 'dd/MM/yyyy HH:mm:ss') : '';
                var userInsert = !isVoid(ai) && !isVoid(ai.userInsert) ? ai.userInsert : '';
                var usrIns = '';
                var userUpdate = !isVoid(ai) && !isVoid(ai.userUpdate) ? ai.userUpdate : '';
                var usrUpd = '';

                if (userInsert !== '' || userUpdate !== '') {
                    var url = "/" + this.AppName + "/rest/Menu/getAuditInfoUsers?";
                    self.httpPost(url, { "userInsert": userInsert , "userUpdate": userUpdate }, rsp => {
                        usrIns =
                            ((isVoid(rsp.userinsertlastname) ? '' : rsp.userinsertlastname) + ' ' +
                             (isVoid(rsp.userinsertfirstname) ? '' : rsp.userinsertfirstname)).trim();
                        if (!isVoid(rsp.userinsertusername) && rsp.userinsertusername !== '')
                            usrIns = usrIns + ' (' + rsp.userinsertusername + ')'
                        usrUpd =
                            ((isVoid(rsp.userupdatelastname) ? '' : rsp.userupdatelastname) + ' ' +
                             (isVoid(rsp.userupdatefirstname) ? '' : rsp.userupdatefirstname)).trim();
                        if (!isVoid(rsp.userupdateusername) && rsp.userupdateusername !== '')
                            usrUpd = usrUpd + ' (' + rsp.userupdateusername + ')'

                        showAuditInfoMessage(usrIns, dteIns, usrUpd, dteUpd);
                    });
                } else {
                    showAuditInfoMessage(usrIns, dteIns, usrUpd, dteUpd);
                }
            }
            
        }

        // groups must override this method in 
        public multipleInsertEntities(): void {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:multipleInsertEntities");
        }


        public deleteNewEntitiesUnderParent(parentEntity: NpTypes.IBaseEntity) {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:deleteNewEntitiesUnderParent");
        }

        public deleteEntity(ent: NpTypes.IBaseEntity, triggerUpdate: boolean, rowIndex: number, bCascade: boolean= false, afterDeleteAction: () => void = () => { }) {
            var self = this;
            if (ent === undefined) {
                console.warn('deleteEntity called for an undefined entity')
                return;
            }
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated('delete of ' + ent.getEntityName());
            if (!ent.isNew()) {
                if (self.model.updatedEntities[ent.getKey()] !== undefined) {
                    delete self.model.updatedEntities[ent.getKey()]
                }
                self.model.deletedEntities[ent.getKey()] = new Tuple2(ent, Utils.getTimeInMS());
            } else {
                var i = 0;
                for (i = 0; i < self.model.newEntities.length; i++) {
                    var e = self.model.newEntities[i];
                    if (ent.getKey() === e.a.getKey()) {
                        self.model.newEntities.splice(i, 1);
                        break;
                    }
                }
            }

            afterDeleteAction();

            // for each child group
            // TODO: call deleteNewEntitiesUnderParent(ent)
        }
        private updateVisibleEntities(ent: NpTypes.IBaseEntity) {
            var visInstance = this.model.visibleEntities.firstOrNull(x => x.getKey() === ent.getKey())
            if (!isVoid(visInstance)) {
                visInstance.updateInstance(ent);
            }
        }
        public markEntityAsUpdated(ent: NpTypes.IBaseEntity, itemId: string) {
            if (ent === undefined) {
                console.warn('markEntityAsUpdated called for an undefined entity')
                return;
            }
            var self = this;
            self.$scope.globals.isCurrentTransactionDirty = true;
            self.markParentEntityAsUpdated(itemId);
            var isNew = _.any(self.model.newEntities, (e: ChangedEntity): boolean => {
                return ent.getKey() === e.a.getKey();
            });
            this.updateVisibleEntities(ent);
            if (isNew)
                return;
            if (self.model.updatedEntities[ent.getKey()] === undefined) {
                var changeEntity: ChangedEntity = new Tuple2(ent, Utils.getTimeInMS());
                self.model.updatedEntities[ent.getKey()] = changeEntity;
            }
        }


        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:getEntitiesFromJSON");
        }

        public updateUI() {
            if (!isVoid(this.model.selectedEntities[0])) {
                this.model.selectedEntities[0].refresh(this);
            }

        }

        public isVisible(): boolean { throw "not implemented function"}

        public refreshVisibleEntities(newEntitiesIds: NpTypes.NewEntityId[], afterRefreshFunc?: ()=>void) {
            if (!this.isVisible())
                return;
            if (this.model.selectedEntities.length === 0)
                return;
            var url = "/" + this.AppName + "/rest/" + this.getEntityName() + "/findAllByIds?";
            function getVisibleEntityDbId(vis: NpTypes.IBaseEntity) {
                return vis.isNew() ?
                        newEntitiesIds.first(y => y.tempId === vis.getKey()).databaseId :
                        vis.getKey();
            }
            var ids = this.model.visibleEntities.map(x => getVisibleEntityDbId(x));

            this.httpPost(url, { "ids": ids }, rsp => {
                var refresedEntities = this.getEntitiesFromJSON(rsp);
                var deletedEntities: NpTypes.IBaseEntity[] = []
                this.model.visibleEntities.forEach(vis => {
                    var visId = getVisibleEntityDbId(vis);
                    var refreshedEntity = refresedEntities.firstOrNull(x => x.getKey() === visId);
                    if (!isVoid(refreshedEntity)) {
                        vis.updateInstance(refreshedEntity);
                    } else {
                        deletedEntities.push(vis);
                    }
                });
                deletedEntities.forEach(x => {
                    this.model.visibleEntities.removeFirstElement( v => v.getKey() === x.getKey());
                    this.model.selectedEntities.removeFirstElement(v => v.getKey() === x.getKey());
                });

                if ( deletedEntities.length >0 && this.model.selectedEntities.length === 0 && this.model.visibleEntities.length > 0) {
                    this.model.selectedEntities.push(this.model.visibleEntities[0]);
                }
                if (afterRefreshFunc !== undefined)
                    afterRefreshFunc();
            });
        }

        public getEntityName(): string {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:getEntityName");
        }

        public getChangesToCommit(): Array<ChangeToCommit> {
            var self = this;
            var ret: Array<ChangeToCommit> = [];
            _.each(self.model.newEntities, (ch: ChangedEntity) => {
                var changeToCommit = new ChangeToCommit(ChangeStatus.NEW, ch.b, self.getEntityName(), ch.a.toJSON());
                ret.push(changeToCommit);
            });
            _.each(self.model.updatedEntities, (ch: ChangedEntity) => {
                var changeToCommit = new ChangeToCommit(ChangeStatus.UPDATE, ch.b, self.getEntityName(), ch.a.toJSON());
                ret.push(changeToCommit);
            });
            _.each(self.model.deletedEntities, (ch: ChangedEntity) => {
                var changeToCommit = new ChangeToCommit(ChangeStatus.DELETE, ch.b, self.getEntityName(), ch.a.toJSON());
                ret.push(changeToCommit);
            });

            return ret;
        }

        public getDeleteChangesToCommit(): Array<ChangeToCommit> {
            var self = this;
            var ret: Array<ChangeToCommit> = [];
            _.each(self.model.deletedEntities, (ch: ChangedEntity) => {
                var changeToCommit = new ChangeToCommit(ChangeStatus.DELETE, ch.b, self.getEntityName(), ch.a.toJSON());
                ret.push(changeToCommit);
            });

            return ret;
        }
        public get PageModel(): NpTypes.IPageModel {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:PageModel");
        }
        public get ControllerClassName(): string {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:ControllerClassName");
        }
        public get HtmlDivId(): string {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:HtmlDivId");
        }

        public get Items(): Array<Item<any>> {
            return undefined;
        }

        public get FirstLevelChildGroups(): Array<AbstractGroupController> {
            return undefined;
        }

        public validateGroup(): Array<string> {
            var self = this;
            var ret = [];
            self.Items.filter(itm => itm.unbounded).forEach(itm => {
                ret.push.apply(ret, itm.validateItem(self.$scope));
            });
            var updatedEntities = Object.keys(self.model.updatedEntities).map(key => self.model.updatedEntities[key]);
            var validatedEntities =
                updatedEntities.concat(self.model.newEntities).map(x => x.a);

           /* var visibleEntities =
                self.model.visibleEntities.filter(vis => !validatedEntities.some(vl => vl.isEqual(vis)));

            validatedEntities = validatedEntities.concat(visibleEntities); */
            
            validatedEntities.forEach(ent => {
                ret.push.apply(ret, self.validateEntity(ent));
            });

            self.FirstLevelChildGroups.forEach(grp => {
                ret.push.apply(ret, grp.validateGroup());
            });
            return ret;
        }

        public validateEntity(ent: NpTypes.IBaseEntity): Array<string> {
            var self = this;
            var ret = [];
            self.Items.filter(itm => !itm.unbounded).forEach(itm => {
                ret.push.apply(ret, itm.validateItem(self.$scope, ent));
            });
            return ret;
        }



    }


    export enum EditMode {
        NEW,
        UPDATE
    }

    export class AbstractGroupFormController extends AbstractGroupController {
        constructor(
            public $scope: IAbstractGroupScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public plato: Services.INpDialogMaker,
            public model: AbstractGroupFormModel)
        {
            super($scope, $http, $timeout, plato, model)
        }

        public get Mode(): EditMode {
            var self = this;
            
            return self.model.newEntities.length === 1 ? EditMode.NEW : EditMode.UPDATE;
        }

        public getChangesToCommitForDeletion(): Array<ChangeToCommit> {
            var self = this;
            var ret: Array<ChangeToCommit> = [];
            if (self.Mode === EditMode.UPDATE) {
                var changeToCommit = new ChangeToCommit(ChangeStatus.DELETE, Utils.getTimeInMS(), self.getEntityName(), self.model.selectedEntities[0]);
                ret.push(changeToCommit);
            }
            return ret;
        }
        public isVisible(): boolean { return true; }

        public setCurrentFormPageBreadcrumpStepModel() {
        }

        public updateUI() {
            var self = this;
            if (!isVoid(this.model.selectedEntities[0])) {
                self.model.selectedEntities[0].refresh(self, () => { self.setCurrentFormPageBreadcrumpStepModel() });
            }
        }

        public refreshVisibleEntities(newEntitiesIds: NpTypes.NewEntityId[], afterRefreshFunc?: () => void) {
            var self = this;
            super.refreshVisibleEntities(newEntitiesIds, () => {
                self.setCurrentFormPageBreadcrumpStepModel();
                if (afterRefreshFunc !== undefined)
                    afterRefreshFunc();
            });
        }

    }


    export interface GridHeaderCellTemplateParams {
        tooltip: string;
    }

    export interface GridAttrs {
        pageSize: number;
        maxLinesInHeader: number;
        maxPageVisibleRows?: number // the max number of rows to be shown in One Page without Scroll
    }
    export class AbstractGroupTableController extends AbstractGroupController {
        checkedEntities: { [id: string]: boolean; } = {};

        public focusFirstCellYouCan: boolean = false;

        constructor(
            public $scope: IAbstractTableGroupScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public plato: Services.INpDialogMaker,
            public model: AbstractGroupTableModel,
            gridAttrs: GridAttrs,
            isLov : boolean = false)
        {
            super($scope, $http, $timeout, plato, model)
            var self = this;

            var _rowHeight = 21;
            model.pagingOptions = {
                pageSizes: (gridAttrs.pageSize === 5 || gridAttrs.pageSize === 10 || gridAttrs.pageSize === 15) ? [5, 10, 15] : [5, 10, 15, gridAttrs.pageSize],
                pageSize: gridAttrs.pageSize,
                currentPage: 1
            };
            model.flexibleHeightPlugin = new ngGridFlexibleHeightPlugin({
                minHeight: 58 + 20 * gridAttrs.maxLinesInHeader,
                maxViewPortHeight: isVoid(gridAttrs.maxPageVisibleRows) ? undefined : _rowHeight * gridAttrs.maxPageVisibleRows
            });
            model.flexibleLayoutPlugin = new ngGridLayoutPlugin();
            $scope.focusHack = function (idx: number) {
                model.grid.selectRow(idx, true);
            }

            model.grid = {
                data: 'model.visibleEntities',

                enablePaging: true,
                pagingOptions: model.pagingOptions,
                maxPageVisibleRows:gridAttrs.maxPageVisibleRows,

                totalServerItems: 'model.totalItems',
                multiSelect: false,
                selectedItems:        model.selectedEntities,
                showFooter: true,
                selectedRowsCount: () => {
                    return self.getCheckedEntitiesIds().length;
                },

                enableColumnResize: true,
                enableCellSelection: true,
                enableSorting:        true,
                useExternalSorting:   true,
                keepLastSelected: true,

                headerRowHeight: gridAttrs.maxLinesInHeader*20 + 4,
                footerRowHeight: 31,
                rowHeight: _rowHeight,

                maintainColumnRatios: false,
                plugins: [model.flexibleHeightPlugin, model.flexibleLayoutPlugin],

                localeCodeParam: 'globals.globalLang',
                columnDefs: self.getGridColumnDefinitions(),
            
                totalItemsLabel: self.getTotalItemsLabel(),
                showTotalItems: true,
                updateTotalOnDemand: false,
                nInProgressCounts: () => {
                    return model.nInProgressCounts;
                },
                updateTotalCompleted: () => {
                    return model.updateTotalCompleted;
                },
                updateTotalItems: (forceUpdate?) => {
                    return self.updateTotalItems(forceUpdate);
                }
            };

            self.inProgress = false;

            $scope.selectEntityCheckBoxClicked = (x: NpTypes.IBaseEntity) => {
                self.selectEntityCheckBoxClicked(x);
            };

            $scope.isEntitySelected = (x: NpTypes.IBaseEntity) => {
                return self.isEntitySelected(x);
            };

            $scope.unUselectActiveSetEntities = () => {
                self.unUselectActiveSetEntities();
            }
            $scope.selectActiveSetEntities = () => {
                self.selectActiveSetEntities();
            }

            $scope.isMultiSelect = () => {
                return self.isMultiSelect();
            }
            $scope.exportToExcelBtnAction = () => {
                self.exportToExcelBtnAction();
            }

            $scope.updateGrid = (): void => {
                return self.updateGrid(0, false, true);
            }

            $scope.gridTitle = (): string => {
                return self.gridTitle();
            }

            $scope.getColumnTooltip = (fieldName: string): string => {
                return self.getColumnTooltip(fieldName);
            }

            $scope.setSelectedRow = function (idx: number) {
                $scope.model.grid.selectRow(idx, true);
                //$("#controllerCustomersRoot .ngViewport input")[0].focus();
            }

            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watch('model.pagingOptions', function (newVal, oldVal) {
                    var bChangedCurrentPage: boolean = (newVal.currentPage !== oldVal.currentPage);
                    var bChangedPageSize: boolean = (newVal.pageSize !== oldVal.pageSize);
                    var prevCurrentPage = self.model.pagingOptions.currentPage;
                    if (bChangedPageSize) {
                        self.model.pagingOptions.currentPage = 1;
                    }
                    if (newVal !== oldVal && (bChangedCurrentPage || (bChangedPageSize && prevCurrentPage === self.model.pagingOptions.currentPage))) {
                        self.updateGrid();
                    }
                    $timeout(function () {
                        self.model.flexibleLayoutPlugin.updateGridLayout();
                    }, 0);

                }, true)));

            if (isLov === false) {
                $scope.$on('$destroy', <(evt: ng.IAngularEvent, ...cols: any[]) => any>(
                    $scope.$watch(
                        () => {
                            return self.isVisible();
                        },
                        (newVal, oldVal) => {
                            if (oldVal === false && newVal === true) {
                                self.updateGrid(0, true);
                            }
                        },
                        false)));
            }

            $scope.$on('ngGridEventSorted', function(evt:ng.IAngularEvent, ...cols:any[]) {
                var col = cols[0];
                evt.preventDefault();
                evt.stopPropagation();
                if (($scope.model.sortField !== col.fields[0]) || ($scope.model.sortOrder !== col.directions[0])) {
                    $scope.model.sortField = col.fields[0];
                    $scope.model.sortOrder = col.directions[0];
                    $timeout(function() { self.updateUI(); }, 0);
                }
            });

            $scope.$on('$destroy', <(evt:ng.IAngularEvent, ...cols:any[]) => any>(
                $scope.$watch('modelNavigation.epochOfLastResize', function (newVal, oldVal) {
                    self.updateLayout();
                })));

            $scope.$on('$destroy', (evt: ng.IAngularEvent, ...cols: any[]): void => {
                model.grid.data = null;
                model.grid = null;
            });

            $scope.gridPageDown = function () {
                var canPageDown = false;
                if ($scope.model.totalItems >= $scope.model.visibleEntities.length) {
                    if ($scope.model.pagingOptions.currentPage <
                        Math.ceil(
                            $scope.model.totalItems /
                            $scope.model.pagingOptions.pageSize)) {
                        canPageDown = true;
                    }
                } else {
                    if ($scope.model.visibleEntities.length >= $scope.model.pagingOptions.pageSize)
                        canPageDown = true;
                }

                if (canPageDown)
                    $scope.model.pagingOptions.currentPage += 1;
            }
            $scope.gridPageUp = function () {
                if ($scope.model.pagingOptions.currentPage > 1) {
                    $scope.model.pagingOptions.currentPage -= 1;
                }
            }
        }

        public getGridHeaderCellTemplate(attrs: GridHeaderCellTemplateParams): string {
            /*********************************************/
            /* This is the Default Header Cell Template of ng-grid (https://github.com/angular-ui/ng-grid/wiki/Templating) */
            /*
            <div class="ngHeaderSortColumn {{col.headerClass}}" ng-style="{'cursor': col.cursor}" ng-class="{ 'ngSorted': !noSortVisible }">
                <div ng-click="col.sort($event)" ng-class="'colt' + col.index" class="ngHeaderText">{{col.displayName}}</div>
                <div class="ngSortButtonDown" ng-show="col.showSortButtonDown()"></div>
                <div class="ngSortButtonUp" ng-show="col.showSortButtonUp()"></div>
                <div class="ngSortPriority">{{col.sortPriority}}</div>
                <div ng-class="{ ngPinnedIcon: col.pinned, ngUnPinnedIcon: !col.pinned }" ng-click="togglePin(col)" ng-show="col.pinnable"></div>
            </div>
            <div ng-show="col.resizable" class="ngHeaderGrip" ng-click="col.gripClick($event)" ng-mousedown="col.gripOnMouseDown($event)"></div>
            */
            /**********************************************/
            var colTitle = isVoid(attrs.tooltip) ? '' : 'title="' + attrs.tooltip + '"';
            var headerCellTemplate =
                '<div ' + colTitle + ' class="ngHeaderSortColumn {{col.headerClass}}" ng-style="{\'cursor\': col.cursor}" ng-class="{ \'ngSorted\': !noSortVisible }">' +
                '    <div ng-click="col.sort($event)" ng-class="col.requiredAsterisk ? \'colt\'+col.index + \' requiredAsterisk\' : \'colt\'+col.index" class="ngHeaderText" ng-bind-html-unsafe="{{col.displayName}}"></div>' +
                '    <div class="ngSortButtonDown" ng-show="col.showSortButtonDown()"></div>' +
                '    <div class="ngSortButtonUp" ng-show="col.showSortButtonUp()"></div>' +
                '    <div class="ngSortPriority">{{col.sortPriority}}</div>' +
                '    <div ng-class="{ ngPinnedIcon: col.pinned, ngUnPinnedIcon: !col.pinned }" ng-click="togglePin(col)" ng-show="col.pinnable"></div>' +
                '</div>' +
                '<div ng-show="col.resizable" class="ngHeaderGrip" ng-click="col.gripClick($event)" ng-mousedown="col.gripOnMouseDown($event)"></div>';

            return headerCellTemplate;
        }

        public getTotalItemsLabel(): string {
            return 'Records:';
        }

        public isMultiSelect(): boolean {
            return false;
        }
        public getCheckedEntitiesIds():string[] {
            return Object.keys(this.checkedEntities);
        }
        public selectEntityCheckBoxClicked(x: NpTypes.IBaseEntity): void {
            if (isVoid(x.getKey()))
                return;
            if (this.checkedEntities[x.getKey()] === undefined) {
                this.checkedEntities[x.getKey()] = true;
            } else {
                delete this.checkedEntities[x.getKey()];
            }
        }

        public isEntitySelected(x: NpTypes.IBaseEntity): boolean {
            if (isVoid(x.getKey()))
                return false;
            return this.checkedEntities[x.getKey()] === true;
        }

        public unUselectActiveSetEntities() {
            this.getAllIds(false);
        }
        public selectActiveSetEntities() {
            this.getAllIds(true);
        }

        private getAllIds(bSelectAll:boolean) {
            var self = this;
            function doRequest() {
                var excludedIds = Object.keys(self.model.deletedEntities);
                self.makeWebRequestGetIds(excludedIds).
                    success(function (response, status, header, config) {
                        if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                        var allIds: string[] = response.data;
                        allIds.forEach(id => {
                            if (bSelectAll) {
                                self.checkedEntities[id] = true;
                            } else {
                                delete self.checkedEntities[id];
                            }

                        });
                    }).error(function (data, status, header, config) {
                        if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                        NpTypes.AlertMessage.addDanger(self.PageModel, data);
                        console.error("Error received in makeWebRequest:" + data);
                        console.dir(status);
                        console.dir(header);
                        console.dir(config);
                    });
            }

            if (self.model.totalItems <= 10000) {
                doRequest();
            } else {
                messageBox(self.$scope, self.Plato, "MessageBox_Attention_Title",
                    self.dynamicMessage("GetAllIds_TotalItemsMsg(" + self.model.totalItems + ")"),
                    IconKind.INFO,
                    [new Tuple2("OK", () => { })], 0, 0); 
            }
        }

        public getExcelFields() {
            var self = this;
            return self.getGridColumnDefinitions()
                .filter(col => col.field !== undefined)
                .map(col => col.field);
        }
        public getExcelFieldDefinitions() {
            var self = this;
            return self.getGridColumnDefinitions()
                .filter(col => col.field !== undefined)
                .map(col => { return { 'field': col.field, 'displayName': Utils.replaceHtmlChars(self.$scope.$eval(col.displayName)) }; });
        }
        public exportToExcelBtnAction() {
        }

        public deleteEntity(ent: NpTypes.IBaseEntity, triggerUpdate: boolean, rowIndex: number, bCascade: boolean= false, afterDeleteAction: () => void = () => { }) {
            super.deleteEntity(ent, triggerUpdate, rowIndex, bCascade, afterDeleteAction);
            if (triggerUpdate) {
                this.updateGrid(rowIndex);
            } else {
                this.model.visibleEntities.removeFirstElement(x => x.isEqual(ent));
                this.model.selectedEntities.removeFirstElement(x => x.isEqual(ent));
            }
            this.model.totalItems--;
        }

        public updateLayout() {
            var self = this;
            if (!self.isVisible())
                return;
            self.$timeout(function () {
                if (!self.isVisible())
                    return;
                // George investigated, and claims this call is superfluous:
                //self.$scope.model.flexibleLayoutPlugin.updateGridLayout();
                self.$scope.model.grid.ngGrid.refreshDomSizes();

                // The call to buildColumns() below resets the column widths to their
                // percentage-driven values.  If the user has resized a column,
                // this means he loses his resizing with any "+ New" he presses!
                // 
                // We don't want that. But even worse, the login in ng-grid
                // in line 1725 says this:
                //
                //      if (isNaN(t) && !$scope.hasUserChangedGridColumnWidths)
                //
                // ...which skips the percentage-driven column width calculation
                // altogether, if the user has resized! This means that ANY resize
                // causes a catastrophic collapse the next time updateLayout 
                // is called (because the percentage-driven columns are not processed!)
                //
                // Disabling the call to buildColumns, and hoping the change
                // doesn't break anything else (God willing).
                //
                //self.$scope.model.grid.ngGrid.buildColumns();

                self.$scope.model.grid.ngGrid.configureColumnWidths();
                self.$scope.model.flexibleLayoutPlugin.updateGridLayout();
                // $('#CustomerTable_ControllerGrpCustomer').find('input')[15].focus();

                self.gridRepainted();
            }, 0);
        }

        public selectAndFocusTheFirstCellYouCan(row: JQuery) {
            if (isVoid(row) || row.length === 0)
                return;

            var columns = angular.element(row[0].children).filter(function () { return this.nodeType !== 8; }); //Remove html comments for IE8
            var i = 0;
            if (this.$scope.model.grid.ngGrid.config.showSelectionCheckbox && angular.element(columns[i]).scope() && (<any>angular.element(columns[i])).scope().col.index === 0) {
                i = 1; //don't want to focus on checkbox
            }

            // Skip over disabled/readonly columns
            var ngCellElement, domElemToWorkOn;
            var isDisabledOrReadOnly = function(elem) {
                return $(elem).is(":disabled") || $(elem).is("[readonly]");
            };
            while (i>=0 && i<columns.length && columns[i]) {
                ngCellElement = (<any>columns[i]).children[1].children[0];
                var candidateDomElements = $(ngCellElement).find('input,button,select,a');
                for(var j=0; j<candidateDomElements.length; j++) {
                    var domElem = candidateDomElements[j];
                    if (!isDisabledOrReadOnly(domElem) && domElem.className !== "npGridDelete") {
                        domElemToWorkOn = domElem;
                        break;
                    }
                }
                if (domElemToWorkOn !== undefined)
                    break;
                else
                    i++;
            }
            if (domElemToWorkOn !== undefined) {
                $(domElemToWorkOn).focus();
                var isInput = (<any>$(domElemToWorkOn)).tagName == "INPUT";
                if (isInput) {
                    $(domElemToWorkOn).select();
                }
            }
        }

        public gridRepainted() {
            if (this.focusFirstCellYouCan) {
                this.$scope.setSelectedRow(0);
                var controllerVisibleDiv = $('#' + this.HtmlDivId + ':visible');
                var row = controllerVisibleDiv.find('.ngRow').first();
                this.selectAndFocusTheFirstCellYouCan(row);
                this.focusFirstCellYouCan = false;
            }
        }

        public inProgress: boolean;

        public isVisible(): boolean {
            var controllerVisibleDiv = $('#' + this.HtmlDivId + ':visible');
            if (controllerVisibleDiv.length !== 1)
                return false;
            //controller is visible but it may be within  a tab which is under construction

            var parentTabControls = controllerVisibleDiv.parents('div.ui-tabs');

            if (parentTabControls.length === 0)
                return true;    // not tab control, so controller is visible

            //every surrounding tab control must have the .ui-widget class
            var bSomeTabAreUnderConstruction = parentTabControls.not('.ui-widget').length > 0;

            if (bSomeTabAreUnderConstruction)
                return false;

            return true;
        } 

        public getGridColumnDefinitions(): Array<any> {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:getGridColumnDefinitions");
        }

        public updateUI(rowIndexToSelect: number = 0) {
            this.updateGrid(rowIndexToSelect);
            this.lastRequestParams = "";
        }

        private lastRequestParams: string = "";
        public isLov: boolean = false;
        gridTitle(): string {
            return "";
        }
        getColumnTooltip(fieldName: string): string {
            return "";
        }
        public updateTotalItems(forceUpdate?:boolean) {
            var self = this;
            if (!self.model.grid.showTotalItems)
                return;

            var myNewEntities = self.model.newEntities.filter(e => self.belongsToCurrentParent(e.a));

            if (self.model.grid.updateTotalOnDemand && (isVoid(forceUpdate) || !forceUpdate)) {
                self.model.totalItems = myNewEntities.length;
                return;
            }

            var excludedIds = Object.keys(self.model.deletedEntities);
            self.model.nInProgressCounts++;
            self.makeWebRequest_count(excludedIds).
                success(function (response, status, header, config) {
                    if (self.model.nInProgressCounts > 0) self.model.nInProgressCounts--;
                    self.model.totalItems = response.count + myNewEntities.length;
                    self.model.updateTotalCompleted = true;
                }).error(function (data, status, header, config) {
                    if (self.model.nInProgressCounts > 0) self.model.nInProgressCounts--;
                    console.error("Error received in makeWebRequest_count:" + data);
                });
        }
        updateGrid(rowIndexToSelect: number= 0, bUpdateOnlyIfWsParamsAreDifferent: boolean= false, goToFirstPage: boolean = false): void {
            var self = this;
            //if (self.inProgress)
              //  return;
            if (!self.isLov && !self.isVisible())
                return;
            self.inProgress = true;

            if (goToFirstPage) {
                self.model.pagingOptions.currentPage = 1;
            }

            var fromRowIndex: number = self.model.pagingOptions.pageSize * (self.model.pagingOptions.currentPage - 1);
            var toRowIndex: number = self.model.pagingOptions.pageSize * self.model.pagingOptions.currentPage;
            var myNewEntities = self.model.newEntities.filter(e => self.belongsToCurrentParent(e.a));
            var newRowsOnTop: number = myNewEntities.length;

            // Start the filling-in from the local myNewEntities container
            var newData: Array<NpTypes.IBaseEntity> = [];
            var idx = fromRowIndex;
            for (; idx < toRowIndex; idx++) {
                if (idx < newRowsOnTop) {
                    var ent = myNewEntities[idx].a
                    newData.push(ent);
                } else {
                    break;
                }
            }
            
            if ((idx !== toRowIndex) && !self.ParentIsNewOrUndefined) {
                var excludedIds = Object.keys(self.model.deletedEntities);
                var requestParams = self.getWebRequestParamsAsString(idx - newRowsOnTop, toRowIndex - newRowsOnTop, excludedIds);
                var makeWebRequest =
                    bUpdateOnlyIfWsParamsAreDifferent === false ? true : self.lastRequestParams !== requestParams;
                self.lastRequestParams = requestParams;
                if (makeWebRequest) {
                    self.model.updateTotalCompleted = false;
                    self.makeWebRequest(idx - newRowsOnTop, toRowIndex - newRowsOnTop, excludedIds).
                        success(function (response, status, header, config) {
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            var wrappedFetchedEntities = self.processEntities(self.getEntitiesFromJSON(response), false);
                            self.model.visibleEntities = newData.concat(wrappedFetchedEntities);

                            if (response.count != undefined && response.count == -1) {
                                self.updateTotalItems();
                            } else {
                                self.model.totalItems = response.count + myNewEntities.length;
                                self.model.updateTotalCompleted = true;
                            }

                            self.updateGridFinalSteps(rowIndexToSelect);
                            self.onDataReceived();
                        }).error(function (data, status, header, config) {
                            if (self.$scope.globals.nInFlightRequests > 0) self.$scope.globals.nInFlightRequests--;
                            NpTypes.AlertMessage.addDanger(self.PageModel, data);
                            console.error("Error received in makeWebRequest:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                            if (data === "NO_READ_ACCESS") {
                                self.$scope.navigateBackward();
                            }
                        });
                } else {
                    //self.updateGridFinalSteps(rowIndexToSelect);
                    self.updateLayout();
                }

            } else {
                // All data are from recently added entities!
                self.model.visibleEntities = newData;
                self.updateGridFinalSteps(rowIndexToSelect);
            }

        }

        public onDataReceived() {
        }

        //return true only in child groups and when the parent entity is new (i.e. not yet submitted to database)
        public get ParentIsNewOrUndefined(): boolean {
            return false;
        }

        public belongsToCurrentParent(x:NpTypes.IBaseEntity):boolean {
            return true;
        }

        private updateGridFinalSteps(rowIndexToSelect): void {
            var self = this;
            self.inProgress = false;

            self.model.selectedEntities.splice(0);
            var targetRowIndex = -1;
            if (rowIndexToSelect < self.model.visibleEntities.length) {
                targetRowIndex = rowIndexToSelect;
            } else {
                if (rowIndexToSelect - 1 < self.model.visibleEntities.length && (rowIndexToSelect - 1) >= 0) {
                    targetRowIndex = rowIndexToSelect - 1;
                }
            }
            if (targetRowIndex !== -1) {
                self.model.selectedEntities.push(self.model.visibleEntities[targetRowIndex]);
            }

            // https://github.com/angular-ui/ng-grid/issues/539
            self.$timeout(() => {
                if (!self.isVisible())
                    return;

                if (targetRowIndex !== -1) {
                    var grid = self.$scope.model.grid.ngGrid;
                    grid.$viewport.scrollTop(grid.rowMap[Math.max(targetRowIndex - 3, 0)] * grid.config.rowHeight);
                }
                self.updateLayout();
            }, 0);

        }

        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            return "#" + paramIndexFrom + "#" + paramIndexTo + "#" + excludedIds.join("#");
        }

        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds:Array<string>=[]): ng.IHttpPromise<any> {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:makeWebRequest");
        }
        public makeWebRequest_count(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:makeWebRequest_count");
        }

        public makeWebRequestGetIds(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            throw "Not implemented function:makeWebRequest";
        }


        public processEntities(dbEntities: Array<NpTypes.IBaseEntity>, bMergeVisibleEntities: boolean): Array<NpTypes.IBaseEntity> {
            // remove deleted
            var self = this;
            var filteredEntities =
                _.filter(dbEntities, (e: NpTypes.IBaseEntity):boolean => {
                    // Remove the ones we have already deleted locally
                    return undefined === self.model.deletedEntities[e.getKey()];
                });

            

            // merge updated
            var mergedEntities =
                _.map(filteredEntities, (e: NpTypes.IBaseEntity): NpTypes.IBaseEntity  => {
                    // Use the ones we have already updated locally
                    if (undefined !== self.model.updatedEntities[e.getKey()])
                        return self.model.updatedEntities[e.getKey()].a;
                    else
                        return e;
                });
            //merge also with visible
            var mergedEntities2 =
                bMergeVisibleEntities ?
                _.map(mergedEntities, (e: NpTypes.IBaseEntity): NpTypes.IBaseEntity  => {
                    var visible = self.model.visibleEntities.firstOrNull(x => x.isEqual(e));
                    if (isVoid(visible))
                        return e;
                    else
                        return visible;
                })
                : mergedEntities;
            
            return mergedEntities2;
        }
        public cleanUpAfterSave() {
            super.cleanUpAfterSave();
            this.lastRequestParams = "";
        }

    }


/*
    Lov Forms
*/


    export interface IAbstractLovScope extends IAbstractTableGroupScope {
        closeDialog(): void;
        openNewEntityDialog(): void;
    }
    export class AbstractLovModel extends AbstractGroupTableModel implements NpTypes.IPageModel {
        alertData: Array<NpTypes.AlertData> = [];
        alerts: Array<NpTypes.AlertMessage> = [];
        private _dialogOptions: NpTypes.LovDialogOptions;
        public get dialogOptions(): NpTypes.LovDialogOptions {
            return this._dialogOptions;
        }
        public set dialogOptions(vl: NpTypes.LovDialogOptions) {
            this._dialogOptions = vl;
        }
        showSearchParams: boolean = true;
        constructor(public $scope: IAbstractLovScope) { super($scope); }
    }

    export class LovController extends AbstractGroupTableController {
        constructor(
            public $scope: IAbstractLovScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker,
            public model: AbstractLovModel,
            gridAttrs: GridAttrs)
        {
            super($scope, $http, $timeout, Plato, model, gridAttrs, true);
            var self = this;
            self.isLov = true;
            if (isVoid(self.model.dialogOptions)) {
                self.model.dialogOptions = <NpTypes.LovDialogOptions>$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }

            self.initializeModelWithPreviousValues();

            if (self.model.dialogOptions !== undefined) {
                self.model.grid.showTotalItems = self.model.dialogOptions.showTotalItems;
                self.model.grid.updateTotalOnDemand = self.model.dialogOptions.updateTotalOnDemand;
            }

            if (self.model.dialogOptions !== undefined && self.model.dialogOptions.onConstructLovController !== undefined) {
                self.model.dialogOptions.onConstructLovController(self);
            }

            $scope.closeDialog = () => {
                self.closeDialog();
            }

            $scope.openNewEntityDialog = () => {
                self.model.dialogOptions.openNewEntityDialog();
            }
        }

        
        public onDataReceived() {
            this.$timeout(() => {
//                $('#' + this.HtmlDivId).find('.ngRow').first().find('input').first().click();
//                $('#' + this.HtmlDivId + ' .ngViewport').focus();
            }, 200);
        }

        public get PageModel(): NpTypes.IPageModel {
            return this.model;
        }

        public isMultiSelect(): boolean {
            var self = this;
            if (self.model.dialogOptions === undefined) {
                self.model.dialogOptions = <NpTypes.LovDialogOptions>this.$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            return self.model.dialogOptions.bMultiSelect;
        }


        public initializeModelWithPreviousValues() {
        }

        public closeDialog(): void {
            var self = this;
            var selectedEntity = self.model.selectedEntities[0];
            self.model.dialogOptions.jquiDialog.dialog("close");
            if (!self.isMultiSelect()) {
                self.model.dialogOptions.onSelect(selectedEntity);
            } else {
                if (isVoid(selectedEntity)) { //'Clear button called'
                    self.model.dialogOptions.onMultipleSelect([]);
                } else {
                    self.model.dialogOptions.onMultipleSelect(self.getCheckedEntitiesIds());
                }
            }
        }

        public makeWebRequestGetIds(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
            if (self.model.dialogOptions === undefined) {
                self.model.dialogOptions = <NpTypes.LovDialogOptions>this.$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            return self.model.dialogOptions.makeWebRequestGetIds(self.model, []);
        }


        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
            if (self.model.dialogOptions === undefined) {
                self.model.dialogOptions = <NpTypes.LovDialogOptions>this.$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            return self.model.dialogOptions.makeWebRequest(paramIndexFrom, paramIndexTo, self.model, []);
        }

        public makeWebRequest_count(excludedIds: Array<string> = []): ng.IHttpPromise<any> {
            var self = this;
            if (self.model.dialogOptions === undefined) {
                self.model.dialogOptions = <NpTypes.LovDialogOptions>this.$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            return self.model.dialogOptions.makeWebRequest_count(self.model, []);
        }

        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
            if (self.model.dialogOptions === undefined) {
                self.model.dialogOptions = <NpTypes.LovDialogOptions>this.$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            var lovRes = self.model.dialogOptions.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, self.model, []);
            return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + lovRes;
        }


        public getGridColumnDefinitions(): Array<any> {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:getGridColumnDefinitions");
        }


    }

/*
    Page and Unbounded Pages
*/

    export class AbstractPageModel implements NpTypes.IPageModel {
        alertData: Array<NpTypes.AlertData> = [];
        alerts: Array<NpTypes.AlertMessage> = [];
        pageInfoIsVisible: boolean = false;
        dialogOptions: NpTypes.DialogOptions;
        pageTitle: string;
//        formatReport: number = 1; //used by reports
        constructor(public $scope: IAbstractPageScope) { }
    }

    export interface IAbstractPageScope extends IAbstractControllerScope {
        pageModel: AbstractPageModel;
        onCancelBtnAction(): void;
        onPageInfoBtnAction(): void;
        closeAlert(idx: number): void;
        closePageInfo(): void;
        onEscWhileInDialog(): void;
        newIsDisabled(): boolean;
        saveIsDisabled(): boolean;
        deleteIsDisabled(cur: NpTypes.IBaseEntity): boolean; 
        cancelIsDisabled(): boolean;
        showAuditInfoIsVisible(): boolean;
        getPageTitle(): string;
    }

    export class AbstractPageController extends AbstractController implements NpTypes.IPage {
        constructor(
            public $scope: IAbstractPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker,
            public model: AbstractPageModel)
        {
            super($scope, $http, $timeout, Plato);

            // Enable periodic (1-second) calls to $scope.$digest()
            $scope.globals.bDigestHackEnabled = true;

            var self = this;
            $scope.pageModel = model;

            $scope.saveIsDisabled = () => { return self._saveIsDisabled(); };
            $scope.newIsDisabled = () => { return self._newIsDisabled(); };
            $scope.deleteIsDisabled = (cur: NpTypes.IBaseEntity) => { return self._deleteIsDisabled(); };
            $scope.cancelIsDisabled = () => { return self._cancelIsDisabled(); };
            $scope.showAuditInfoIsVisible = () => {
                return $scope.globals.hasPrivilege($scope.globals.appName + "_" + "ShowAuditInfo");
            }
            $scope.getPageTitle = () => { return self._getPageTitle(); };

            if (isVoid(self.model.dialogOptions)) {
                self.model.dialogOptions = $scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }

            if (self.shownAsDialog()) {
                $scope.globals.createNewTransactionLevel();
            } else {
                self.setupMainAppTemplate();
                Utils.UpdateMainWindowWidthAndMainButtonPlacement($scope);
                $scope.modelNavigation.onNavigation = (actualNavigation: (pageScopeYouAreLeavingFrom?: NpTypes.IApplicationScope) => void) => {
                    self.onPageUnload(actualNavigation);
                }
            }

            $scope.onCancelBtnAction = () => {
                self.onCancelBtnAction();
            };
            $scope.closeAlert = (idx: number) => {
                $scope.pageModel.alerts.splice(idx, 1);
            }

            $scope.onPageInfoBtnAction = () => {
                if (self.model.pageInfoIsVisible) {
                    self.model.pageInfoIsVisible = false;
                } else {
                    self.model.pageInfoIsVisible = true;
                }
            }
            $scope.closePageInfo = () => {
                self.model.pageInfoIsVisible = false;
            }

            $scope.onEscWhileInDialog = () => {
                if (!self.model.dialogOptions.showCloseButton) 
                    return;
                self.onPageUnload(() => { self.closeDialog() });
            }
            if (!self.shownAsDialog()) {
                $scope.modelNavigation.onLogout = (realLogout: () => void) => {
                    self.onPageUnload(() => { realLogout() });
                }
                $scope.$on('$destroy', (evt: ng.IAngularEvent, ...cols: any[]): void => {
                    $scope.modelNavigation.onLogout = null;
                });
            }

            NpTypes.BaseEntity.activePageController = this;
            $scope.$on('$destroy', (evt: ng.IAngularEvent, ...cols: any[]): void => {
                NpTypes.BaseEntity.activePageController = null;
            });


            $scope.$on('$destroy', <(evt: ng.IAngularEvent, ...cols: any[]) => any>(
                $scope.$watch(
                    () => {
                        return $scope.globals.globalLang;
                    },
                    (newVal, oldVal) => {
                        if (isVoid(oldVal) || isVoid(newVal)) {
                            return;
                        }
                        if (oldVal !== newVal) {
                            $scope.globals.onGlobalLangChange(self);
                        }
                    },
                    false)));



        }

        public setupMainAppTemplate(): void {
            this.$scope.globals.mainAppTemplate.showAll(true);
        }

        public onPageUnload(actualNavigation: (pageScopeYouAreLeavingFrom?: NpTypes.IApplicationScope) => void) {
            var self = this;
            if (self.shownAsDialog()) {
                var errors = self.validatePage();
                if (errors.length > 0) {
                    NpTypes.AlertMessage.clearAlerts(self.model);
                    NpTypes.AlertMessage.addDanger(self.model, errors.join('<br>'));
                } else {
                    actualNavigation(self.$scope)
                }
            } else {
                actualNavigation(self.$scope)
            }
        }


        public _saveIsDisabled(): boolean {
            return false;
        }
        public _newIsDisabled(): boolean {
            return false;
        }
        public _deleteIsDisabled(): boolean {
            return false;
        }
        public _cancelIsDisabled(): boolean {
            return false;
        }
        public _getPageTitle(): string {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:_getPageTitle");
        }
        public onCancelBtnAction(): void {
               
            var self = this;
            if (self.shownAsDialog()) {
                self.onPageUnload(() => { self.closeDialog() });
            } else {
                self.$scope.navigateBackward();
            }
        }

        public closeDialog(): void {
            var self = this;
            self.model.dialogOptions.jquiDialog.dialog("close");
            self.$scope.globals.dropLastTransactionLevel();
        }

        public shownAsDialog(): boolean {
            return !isVoid(this.model.dialogOptions);
        }

        public dialogSelectedEntity(): NpTypes.BaseEntity {
            if (this.shownAsDialog() && !isVoid(this.model.dialogOptions.customParams)) {
                return this.model.dialogOptions.customParams.selectedEntity;
            }
            return null;
        }

        /*
        works only if the dirty = false
        */
        public refresh() {
            if (!this.$scope.globals.isCurrentTransactionDirty) {
                this.refreshGroups([]);
            }
        }

        public refreshGroups(newEntitiesIds: NpTypes.NewEntityId[]): void {

        }
        public refreshVisibleEntities(newEntitiesIds: NpTypes.NewEntityId[]): void {

        }

        public get SaveBtnJQueryHandler():JQuery {
            return $('#saveBtn_id');
        }

        public onSuccesfullSaveFromButton(response: NpTypes.SaveResponce) {
            var self = this;
            var msg = self.dynamicMessage("EntitySuccessSave");
            var jqSaveBtn = self.SaveBtnJQueryHandler;
            self.showPopover(jqSaveBtn, msg);
        }

        public onSuccesfullSave(response: NpTypes.SaveResponce) {
        }

        // The timeout promise that destroys the error popovers after X milliseconds
        public promise: ng.IPromise<any> = undefined;

        public showPopover(element: JQuery, msg: string, warning: boolean= false): void {
            var self = this;
            var popOverClass = warning ? "warningPopover" : "infoPopover";
            var htmlMsg = Sprintf.sprintf("<div class='%s'>%s</div>", popOverClass, msg);
            var container = "body";//append on body by default
            if (NpGeoGlobals.fullScreenState === true) {//if fullscreenmode then append it within div to be 
                container = '#NpMainContent';
            }
            // Is there an already visible popover?
            if (self.promise === undefined) {
                // No, so go ahead and show this new one then!
                (<any>element).popover({
                    animation: true, html: true, placement: 'bottom', trigger: 'manual', content: htmlMsg, container: container
                }).popover('show');
                // Schedule a popover destroying after 3000ms, and store it in .promise
                self.promise = self.$timeout(function () {
                    (<any>element).popover('destroy');
                    self.promise = undefined;
                }, 3000);
            }
        }


        public static showFormAsDialog(className: string, Plato: Services.INpDialogMaker, callerScope: NpTypes.IApplicationScope, partialFileName: string, title: string, showCloseButton: boolean = true, width: string = '45em', customParams?: any): void {
            var dialogOptions = new NpTypes.DialogOptions();
            dialogOptions.title = title;
            dialogOptions.width = width;
            dialogOptions.showCloseButton = showCloseButton;
            dialogOptions.className = className;
            dialogOptions.customParams = customParams;
            Plato.showDialog(callerScope,
                dialogOptions.title,
                "/" + callerScope.globals.appName + '/partials/' + partialFileName + '?rev=' + callerScope.globals.version,
                dialogOptions);
        }
        public get PageModel(): NpTypes.IPageModel {
            return this.model;
        }

        public get FirstLevelGroupControllers(): Array<AbstractGroupController> {
            return [];
        }

        public get AllGroupControllers(): Array<AbstractGroupController> {
            return [];
        }

        public get Items(): Array<Item<any>> {
            return [];
        }

        public validatePage(): Array<string> {
            var self = this;
            var ret = [];
            self.Items.forEach(itm => {
                ret.push.apply(ret, itm.validateItem(self.$scope));
            });

            self.FirstLevelGroupControllers.forEach(grpCtr => {
                ret.push.apply(ret, grpCtr.validateGroup());
            });
            return ret;
        }

        public getLabelById(itmId: string): string {
            if (this.shownAsDialog() && itmId === 'cancelBtn_id') {
                return "OK"
            }
            return super.getLabelById(itmId);
        }


    }


    /*
        Search Pages
    */
    export interface IFormPageBreadcrumbStepModel<T extends NpTypes.IBaseEntity> extends NpTypes.IBreadcrumbStepModel {
        selectedEntity: T;
    }

    export class AbstractSearchPageModel extends AbstractGroupTableModel implements NpTypes.IPageModel{
        alertData: Array<NpTypes.AlertData> = [];
        alerts: Array<NpTypes.AlertMessage> = [];
        newBtnPressed: boolean = false;
        pageTitle: string;
        constructor(public $scope: IAbstractSearchPageScope) { super($scope); }
    }

    export interface IAbstractSearchPageScope extends IAbstractTableGroupScope {
        pageModel: AbstractSearchPageModel;
        searchBtnAction(): void;
        newBtnAction(): void;
        onEdit(x: NpTypes.IBaseEntity): void;
        onCancelBtnAction(): void;
        closeAlert(idx: number): void;

        newIsDisabled(): boolean;
        deleteIsDisabled(cur: NpTypes.IBaseEntity): boolean; 
        searchIsDisabled(): boolean; 
        editBtnIsDisabled(cur: NpTypes.IBaseEntity): boolean;
        getPageTitle(): string;
    }


    export class AbstractSearchPageController extends AbstractGroupTableController implements NpTypes.IPage {
        constructor(
            public $scope: IAbstractSearchPageScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker,
            public model: AbstractSearchPageModel,
            gridAttrs: GridAttrs)
        {
            super($scope, $http, $timeout, Plato, model, gridAttrs);
            var self = this;

            $scope.newIsDisabled = () => { return self._newIsDisabled(); };
            $scope.deleteIsDisabled = (ent: NpTypes.IBaseEntity) => { return self._deleteIsDisabled(ent); };
            $scope.searchIsDisabled = () => { return self._searchIsDisabled(); };
            $scope.editBtnIsDisabled = (ent: NpTypes.IBaseEntity) => { return self._editBtnIsDisabled(ent); };
            $scope.getPageTitle = () => { return self._getPageTitle(); };

            $scope.modelNavigation.onNavigation = undefined;

            $scope.pageModel = model;
            $scope.onCancelBtnAction = () => {
                self.onCancelBtnAction();
            };
            $scope.closeAlert = (idx: number) => {
                $scope.pageModel.alerts.splice(idx, 1);
            }
            $scope.searchBtnAction = () => {
                self.updateGrid(0, false, true);
            };
            $scope.modelNavigation.onNavigation = (actualNavigation: (pageScopeYouAreLeavingFrom?: NpTypes.IApplicationScope) => void) => {
                self.onPageUnload(actualNavigation);
            }

            self.setupMainAppTemplate();
            Utils.UpdateMainWindowWidthAndMainButtonPlacement($scope);

            NpTypes.BaseEntity.activePageController = this;
            $scope.$on('$destroy', (evt: ng.IAngularEvent, ...cols: any[]): void => {
                NpTypes.BaseEntity.activePageController = null;
            });

            $scope.$on('$destroy', <(evt: ng.IAngularEvent, ...cols: any[]) => any>(
                $scope.$watch(
                    () => {
                        return $scope.globals.globalLang;
                    },
                    (newVal, oldVal) => {
                        if (isVoid(oldVal) || isVoid(newVal)) {
                            return;
                        }
                        if (oldVal !== newVal) {
                            $scope.globals.onGlobalLangChange(self);
                        }
                    },
                    false)));

        }

        public _searchIsDisabled(): boolean {
            return false;
        }
        public _editBtnIsDisabled(ent: NpTypes.IBaseEntity): boolean {
            return false;
        }
        public _getPageTitle(): string {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:_getPageTitle");
        }

        public setupMainAppTemplate(): void {
            this.$scope.globals.mainAppTemplate.showAll(true);
        }

        public onPageUnload(actualNavigation: (pageScopeYouAreLeavingFrom?: NpTypes.IApplicationScope) => void) {
            var self = this;
            actualNavigation(self.$scope)
        }

        public onCancelBtnAction(): void {
            var self = this;
            self.$scope.navigateBackward();
        }
        public deleteEntity(ent: NpTypes.IBaseEntity, triggerUpdate: boolean, rowIndex: number, bCascade: boolean= false, afterDeleteAction: () => void = () => { }) {
            var self = this;
            messageBox(self.$scope, self.Plato,
                "MessageBox_Attention_Title",
                self.dynamicMessage("EntityDeletionWarningMsg"),
                IconKind.QUESTION,
                [
                    new Tuple2("MessageBox_Button_Yes", () => { self.deleteRecord(ent); }),
                    new Tuple2("MessageBox_Button_No", () => { })
                ], 1, 1);
        }
        public deleteRecord(x: NpTypes.IBaseEntity): void {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:deleteRecord");
        }
        public get PageModel(): NpTypes.IPageModel {
            return this.model;
        }

        public refresh() {
            this.updateUI();
        }
    }

    /*
    SQL LAYER FILTER
    */

    export interface IAbstractSqlLayerFilterScope extends IAbstractTableGroupScope {
        closeDialog(): void;
        onApplyNewFilter(): void;
        onClearFilters(): void;

        onGeoFuncChanged(): void;
        GEO_FUNC_disabled(): boolean;
        isGeoFuncInvisible(): boolean;
        GEO_FUNC_PRM_disabled(): boolean;
        isGeoFuncParamInvisible(): boolean;
        showSpatialSearchParams(): boolean;
        getGeoFunList(): Array<LabelValue>
        onExportGeoJsonFile(): void;
        onExportExcel(): void;

    }
    export class AbstractSqlLayerFilterModel extends AbstractGroupTableModel implements NpTypes.IPageModel {
        _geoFunc: NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get geoFunc(): string {
            return this._geoFunc.value;
        }
        public set geoFunc(vl: string) {
            this._geoFunc.value = vl;
        }
        _geoFuncParam: NpTypes.UIStringModel = new NpTypes.UIStringModel(undefined);
        public get geoFuncParam(): string {
            return this._geoFuncParam.value;
        }
        public set geoFuncParam(vl: string) {
            this._geoFuncParam.value = vl;
        }


        alertData: Array<NpTypes.AlertData> = [];
        alerts: Array<NpTypes.AlertMessage> = [];
        private _dialogOptions: NpTypes.SFLDialogOptions;
        public get dialogOptions(): NpTypes.SFLDialogOptions {
            return this._dialogOptions;
        }
        public set dialogOptions(vl: NpTypes.SFLDialogOptions) {
            this._dialogOptions = vl;
        }
        showSearchParams: boolean = true;
        constructor(public $scope: IAbstractSqlLayerFilterScope) { super($scope); }

        public getPersistedModel(): any {
            var ret = super.getPersistedModel();
            var a: NpTypes.IBaseEntity = undefined;
            ret['geoFunc'] = this.geoFunc;
            ret['geoFuncParam'] = this.geoFuncParam;
            return ret;
        }

        public setPersistedModel(vl: any) {
            super.setPersistedModel(vl);
            this.geoFunc = vl['geoFunc'];
            this.geoFuncParam = vl['geoFuncParam'];
        }


    }

    export class LabelValue {
        constructor(
            public label: string,
            public value: string,
            public isPrmDisabled : () => boolean) {
        }
    }

    export class SqlLayerFilterController extends AbstractGroupTableController {
        private spatialFuncs: LabelValue[] = [
            new LabelValue("Επιλέξτε", null, () => true),
            new LabelValue("Απέχουν έως", "WITHIN_DISTANCE", () => false),
//            new LabelValue("Είναι πιο κοντά", "NN", () => true),
            new LabelValue("Εμπεριέχονται", "INSIDE", () => true),
//            new LabelValue("Εμπεριέχει", "CONTAINS", () => true),
            new LabelValue("Ακουμπάνε", "TOUCH", () => true)
        ];
        constructor(
            public $scope: IAbstractSqlLayerFilterScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker,
            public model: AbstractSqlLayerFilterModel,
            gridAttrs: GridAttrs) {
            super($scope, $http, $timeout, Plato, model, gridAttrs, true);
            var self = this;
            self.isLov = true;
            if (isVoid(self.model.dialogOptions)) {
                self.model.dialogOptions = <NpTypes.SFLDialogOptions>$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }

            self.initializeModelWithPreviousValues();

            $scope.closeDialog = () => {
                self.closeDialog();
            }
            $scope.onApplyNewFilter = () => {
                self.onApplyNewFilter();
            }
            $scope.onClearFilters = () => {
                self.onClearFilters();
            }

            $scope.onGeoFuncChanged = () => {
                self.onGeoFuncChanged();
            };

            $scope.GEO_FUNC_disabled = () => {
                return self.GEO_FUNC_disabled();
            };

            $scope.isGeoFuncInvisible = () => {
                return self.isGeoFuncInvisible();
            };

            $scope.GEO_FUNC_PRM_disabled = () => {
                return self.GEO_FUNC_PRM_disabled();
            };

            $scope.isGeoFuncParamInvisible = () => {
                return self.isGeoFuncParamInvisible();
            };

            $scope.getGeoFunList = () => {
                return this.spatialFuncs;
            }

            $scope.$on('$destroy', <(evt: ng.IAngularEvent, ...cols: any[]) => any>(
                $scope.$watch('model.selectedEntities[0]', () => {
                    self.onRowSelect();
                }, false)));



            $scope.showSpatialSearchParams = () => {
                if (isVoid(self.model.dialogOptions))
                    return false;
                return self.model.dialogOptions.showSpatialSearchParams === true;
            }


            $scope.onExportGeoJsonFile = () => {
                self.onExportGeoJsonFile();
            };

            $scope.onExportExcel = () => {
                self.onExportExcel();
            };


        }

        public coordinateSystem: string = 'EPSG:2100';
        public static _ControllerClassName: string = "SqlLayerFilterController"
        public get ControllerClassName(): string {
            return SqlLayerFilterController._ControllerClassName;
        }
        /*
        public onDataReceived() {
            var self = this;
            if (self.model.dialogOptions === undefined) {
                self.model.dialogOptions = <NpTypes.SFLDialogOptions>this.$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            this.model.dialogOptions.onPageDataReceived(<NpGeo.SqlLayerEntity[]>this.model.visibleEntities);
        }
        */
        public get PageModel(): NpTypes.IPageModel {
            return this.model;
        }

        public onRowSelect() {
            var self = this;
            if (self.model.dialogOptions === undefined) {
                self.model.dialogOptions = <NpTypes.SFLDialogOptions>this.$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            var row = <NpGeoLayers.SqlLayerEntity>(self.model.selectedEntities.length > 0 ? self.model.selectedEntities[0] : undefined);
            this.model.dialogOptions.onRowSelect(row);
        }

        public initializeModelWithPreviousValues() {
        }

        public closeDialog(): void {
            var self = this;
            var selectedEntity = self.model.selectedEntities[0];
            self.model.dialogOptions.jquiDialog.dialog("close");
        }

        public onApplyNewFilter(): void {
            var self = this;
            self.model.dialogOptions.jquiDialog.dialog("close");
            self.model.dialogOptions.onApplyNewFilter(self.model);
        }

        public onClearFilters(): void {
            var self = this;
            self.model.dialogOptions.jquiDialog.dialog("close");
            self.model.dialogOptions.onClearFilters();
        }


        public getEntitiesFromJSON(webResponse: any): Array<NpTypes.IBaseEntity> {
            var self = this;
            var ret =
                _.map(webResponse.data, sqlEntityData => {
                    return new NpGeoLayers.SqlLayerEntity(sqlEntityData, self.coordinateSystem);
                });

            return ret;
        }



        public makeWebRequest(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
            if (self.model.dialogOptions === undefined) {
                self.model.dialogOptions = <NpTypes.SFLDialogOptions>this.$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            return self.model.dialogOptions.makeWebRequest(paramIndexFrom, paramIndexTo, self.model, []);
        }

        public getWebRequestParamsAsString(paramIndexFrom: number, paramIndexTo: number, excludedIds: Array<string>= []): string {
            var self = this;
            if (self.model.dialogOptions === undefined) {
                self.model.dialogOptions = <NpTypes.SFLDialogOptions>this.$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            var lovRes = self.model.dialogOptions.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, self.model, []);
            return super.getWebRequestParamsAsString(paramIndexFrom, paramIndexTo, excludedIds) + "#" + lovRes;
        }


        /*
        public makeWebRequestGetIds(excludedIds: Array<string>= []): ng.IHttpPromise<any> {
            var self = this;
            if (self.model.dialogOptions === undefined) {
                self.model.dialogOptions = <NpTypes.DialogOptions>this.$scope.globals.findAndRemoveDialogOptionByClassName(this.ControllerClassName);
            }
            return self.model.dialogOptions.makeWebRequestGetIds(self.model, []);
        }
        */

        /*


        */
        public getGridColumnDefinitions(): Array<any> {
            throw new NpTypes.NpException(NpTypes.ExceptionSeverity.generatorError, "Not implemented function:getGridColumnDefinitions");
        }


        public onGeoFuncChanged() {
        }
        public GEO_FUNC_disabled(): boolean {
            var self = this;
            if (isVoid(self.model.dialogOptions.selectedGeometry))
                return true;
            return false;
        }
        public isGeoFuncInvisible(): boolean {
            return false;
        }
        public GEO_FUNC_PRM_disabled(): boolean {
            var self = this;
            if (isVoid(self.model.dialogOptions.selectedGeometry))
                return true;

            var lb = self.spatialFuncs.firstOrNull(x => x.value === self.model.geoFunc);
            if (isVoid(lb)) {
                return true;
            }

            return lb.isPrmDisabled();
        }
        public isGeoFuncParamInvisible(): boolean {
            return false;
        }
//                    <select name='GEO_FUNC' data-ng-model='modelLovSLF0.geoFunc' data-np-ui-model='modelLovSLF0._geoFunc' data-ng-change='onGeoFuncChanged()' data-ng-show='!isGeoFuncInvisible()' data-np-select='dummy' data-ng-options='x.value as x.label for x in [{label:&#39;Επιλέξτε&#39;, value:null}, {label:&#39;Είναι σε απόσταση έως&#39;, value:&#39;WITHIN_DISTANCE&#39;}, {label:&#39;Είναι πιο κοντά&#39;, value:&#39;NN&#39;}, {label:&#39;Εμπεριέχει&#39;, value:&#39;RELATE_INSIDE&#39;}, {label:&#39;Ακουμπά&#39;, value:&#39;RELATE_TOUCH&#39;}]' data-ng-disabled='GEO_FUNC_disabled()' />


        public onExportGeoJsonFile() {
            this.model.dialogOptions.ExportGeoJsonFile(this.model);
        }

        public onExportExcel()  {
            this.model.dialogOptions.ExportExcel(this.model);
        }


    }



    export class DynamicSqlLayerFilterController extends SqlLayerFilterController {
        constructor(
            public $scope: IAbstractSqlLayerFilterScope,
            public $http: ng.IHttpService,
            public $timeout: ng.ITimeoutService,
            public Plato: Services.INpDialogMaker)
        {
            super($scope, $http, $timeout, Plato, new AbstractSqlLayerFilterModel($scope), { pageSize: 10, maxLinesInHeader: 1})
            var self = this;
            self.updateUI();
        }

        public static _ControllerClassName: string = "DynamicSqlLayerFilterController"
        public get ControllerClassName(): string {
            return DynamicSqlLayerFilterController._ControllerClassName;
        }


        public get HtmlDivId(): string {
            return "DynamicSqlLayerFilterController_id";
        }

        public getGridColumnDefinitions(): Array<any> {
            var self = this;
            if (isVoid(self.model.dialogOptions)) {
                self.model.dialogOptions = <NpTypes.SFLDialogOptions>self.$scope.globals.findAndRemoveDialogOptionByClassName(self.ControllerClassName);
            }

            var ret =
                self.model.dialogOptions.dynInfo.fields.
                    filter(f => f.field.toLowerCase() !== "geom").
                    map(f => {
                        return {
                            cellClass: 'cellToolTip', field: f.field.toLowerCase(), displayName: 'getALString("' + f.label + '", true)', resizable: true, sortable: false, enableCellEdit: false, width: f.width == undefined ? '14%' : f.width, cellTemplate: "<div data-ng-class='col.colIndex()' data-ng-dblclick='closeDialog()'> <label data-ng-bind='row.entity.data." + f.field.toLowerCase() + "' /> </div>"
                        }
                    })

            ret.push({
                width: '1',
                cellTemplate: '<div></div>',
                cellClass: undefined,
                field: undefined,
                displayName: undefined,
                resizable: undefined,
                sortable: undefined,
                enableCellEdit: undefined
            });


            return ret;
        }


    }




}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
