/// <reference path="../npTypes.ts"/>
/// <reference path="../utils.ts"/>

module Controllers {

    export class ModelMsgBox {
        public opt:MsgBoxDialogOptions;
        public buttonWidth:any;
        public allButtonWidth:any;
    }

    export interface IMsgBoxScope extends NpTypes.IApplicationScope {
        model:ModelMsgBox;
        buttonPressed(btn:Tuple2<string, ()=>void>);
        triggerCancel():void;
    }

    export class controllerMsgBox {

        constructor(public $scope: IMsgBoxScope) {
            var dialogOptions: MsgBoxDialogOptions = <MsgBoxDialogOptions>$scope.globals.findAndRemoveDialogOptionByClassName("MsgBoxClass");
            var model:ModelMsgBox = new ModelMsgBox();
            model.opt = dialogOptions;
            model.buttonWidth = {}
            model.buttonWidth['width'] = Math.floor(100 / model.opt.buttons.length) + "%";
            var allButtonWidth = 33*model.opt.buttons.length;
            allButtonWidth = allButtonWidth>100?100:allButtonWidth;
            model.allButtonWidth = { margin:'auto', width:allButtonWidth + '%'}; 
            $scope.model = model;
            $scope.buttonPressed = function(btn:Tuple2<string, ()=>void>) {
                dialogOptions.jquiDialog.dialog("close");
                btn.b();
            }
            $scope.triggerCancel = function() {
                dialogOptions.jquiDialog.dialog("close");
                if (dialogOptions.indexCancel !== undefined)
                    dialogOptions.buttons[dialogOptions.indexCancel].b();
            }
        }
    }
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
