/// <reference path="BaseController.ts" />
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Controllers;
(function (Controllers) {
    var ExcelFileImportResultsDlg;
    (function (ExcelFileImportResultsDlg) {
        function fromJSONPartial_actualdecode(x, deserializedEntities) {
            return NpTypes.BaseEntity.entitiesFactory[x.$entityName].fromJSONPartial_actualdecode(x, deserializedEntities);
        }
        function fromJSONPartial(x, deserializedEntities, entityKind) {
            var self = this;
            var key = "";
            var ret = undefined;
            if (x.$refId !== undefined && x.$refId !== null) {
                key = x.$entityName + ":" + x.$refId;
                ret = deserializedEntities[key];
                if (ret === undefined) {
                    throw new NpTypes.NpException(NpTypes.ExceptionSeverity.programmerError, x.$entityName + "::fromJSONPartial Received $refIf=" + x.$refId + "but entity not in deserializedEntities map");
                }
            }
            else {
                ret = fromJSONPartial_actualdecode(x, deserializedEntities);
            }
            return ret;
        }
        function fromJSONComplete(data, deserializedEntities) {
            if (deserializedEntities === void 0) { deserializedEntities = {}; }
            var self = this;
            var ret = _.map(data, function (x) {
                return fromJSONPartial(x, deserializedEntities, NpTypes.EntityCachedKind.MASTER);
            });
            return ret;
        }
        /**
         * CONTROLLERS
         */
        var PageModel = (function (_super) {
            __extends(PageModel, _super);
            function PageModel($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            return PageModel;
        })(Controllers.AbstractPageModel);
        ExcelFileImportResultsDlg.PageModel = PageModel;
        var PageController = (function (_super) {
            __extends(PageController, _super);
            function PageController($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new PageModel($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this._items = undefined;
                this._firstLevelGroupControllers = undefined;
                this._allGroupControllers = undefined;
                var self = this;
                var model = self.model;
                model.controller = self;
                $scope.pageModel = model;
                $timeout(function () {
                    $('#ExcelFile').find('input:enabled:not([readonly]), select:enabled, button:enabled').first().focus();
                    $('#NpMainContent').scrollTop(0);
                }, 0);
            }
            Object.defineProperty(PageController.prototype, "ControllerClassName", {
                get: function () {
                    return "PageController";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageController.prototype, "HtmlDivId", {
                get: function () {
                    return "ExcelFileImportResultsDlg";
                },
                enumerable: true,
                configurable: true
            });
            PageController.prototype._getPageTitle = function () {
                return "Αποτελέσματα Εισαγωγής Αρχείου Excel";
            };
            Object.defineProperty(PageController.prototype, "Items", {
                get: function () {
                    var self = this;
                    if (self._items === undefined) {
                        self._items = [];
                    }
                    return this._items;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageController.prototype, "model", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageController.prototype, "pageModel", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            PageController.prototype.update = function () {
                var self = this;
                self.model.modelGrpExcelFile.controller.updateUI();
            };
            PageController.prototype.refreshVisibleEntities = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpExcelFile.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpExcelError.controller.refreshVisibleEntities(newEntitiesIds);
            };
            PageController.prototype.refreshGroups = function (newEntitiesIds) {
                var self = this;
                self.model.modelGrpExcelFile.controller.refreshVisibleEntities(newEntitiesIds);
                self.model.modelGrpExcelError.controller.refreshVisibleEntities(newEntitiesIds);
            };
            Object.defineProperty(PageController.prototype, "FirstLevelGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._firstLevelGroupControllers === undefined) {
                        self._firstLevelGroupControllers = [
                            self.model.modelGrpExcelFile.controller
                        ];
                    }
                    return this._firstLevelGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(PageController.prototype, "AllGroupControllers", {
                get: function () {
                    var self = this;
                    if (self._allGroupControllers === undefined) {
                        self._allGroupControllers = [
                            self.model.modelGrpExcelFile.controller,
                            self.model.modelGrpExcelError.controller
                        ];
                    }
                    return this._allGroupControllers;
                },
                enumerable: true,
                configurable: true
            });
            PageController.prototype.onPageUnload = function (actualNavigation) {
                var self = this;
                actualNavigation(self.$scope);
            };
            return PageController;
        })(Controllers.AbstractPageController);
        ExcelFileImportResultsDlg.PageController = PageController;
        var ModelGrpExcelFile = (function (_super) {
            __extends(ModelGrpExcelFile, _super);
            function ModelGrpExcelFile($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
            }
            Object.defineProperty(ModelGrpExcelFile.prototype, "id", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].id;
                },
                set: function (id_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].id = id_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpExcelFile.prototype, "_id", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._id;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpExcelFile.prototype, "filename", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].filename;
                },
                set: function (filename_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].filename = filename_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpExcelFile.prototype, "_filename", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._filename;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpExcelFile.prototype, "data", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].data;
                },
                set: function (data_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].data = data_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpExcelFile.prototype, "_data", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._data;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpExcelFile.prototype, "totalRows", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].totalRows;
                },
                set: function (totalRows_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].totalRows = totalRows_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpExcelFile.prototype, "_totalRows", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._totalRows;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpExcelFile.prototype, "totalErrorRows", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].totalErrorRows;
                },
                set: function (totalErrorRows_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].totalErrorRows = totalErrorRows_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpExcelFile.prototype, "_totalErrorRows", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._totalErrorRows;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpExcelFile.prototype, "totalImportedRows", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return (this.selectedEntities[0].totalImportedRows != undefined ? this.selectedEntities[0].totalImportedRows : 0);
                },
                set: function (totalImportedRows_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].totalImportedRows = totalImportedRows_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpExcelFile.prototype, "_totalImportedRows", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._totalImportedRows;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpExcelFile;
        })(Controllers.AbstractGroupFormModel);
        ExcelFileImportResultsDlg.ModelGrpExcelFile = ModelGrpExcelFile;
        var ControllerGrpExcelFile = (function (_super) {
            __extends(ControllerGrpExcelFile, _super);
            function ControllerGrpExcelFile($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpExcelFile($scope));
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this._firstLevelChildGroups = undefined;
                var self = this;
                var model = self.model;
                model.controller = self;
                $scope.modelGrpExcelFile = model;
                $scope.createBlobModel_GrpExcelFile_itm__data =
                    function () {
                        var tmp = self.createBlobModel_GrpExcelFile_itm__data();
                        return tmp;
                    };
                $scope.pageModel.modelGrpExcelFile = $scope.modelGrpExcelFile;
                model.ExcelFileEntityName = $scope.pageModel.dialogOptions.customParams.ExcelFileEntityName;
                model.ExcelErrorEntityName = $scope.pageModel.dialogOptions.customParams.ExcelErrorEntityName;
                self.getExcelFileById($scope.pageModel.dialogOptions.customParams.excelFileId, function (excelFile) {
                    excelFile._data.fileNameGetter = function () { return excelFile.filename; };
                    $scope.modelGrpExcelFile.visibleEntities[0] = excelFile;
                    $scope.modelGrpExcelFile.selectedEntities[0] = excelFile;
                });
            }
            ControllerGrpExcelFile.prototype.getExcelFileById = function (excelFileId, afterCallFunc) {
                var _this = this;
                var self = this;
                if (isVoid(self.getEntityName()) || self.model.selectedEntities.length !== 0)
                    return;
                var url = "/" + self.AppName + "/rest/" + self.getEntityName() + "/findById?";
                self.httpPost(url, { "id": excelFileId }, function (rsp) {
                    var excelFiles = _this.getEntitiesFromJSON(rsp);
                    if (excelFiles.length === 0)
                        return;
                    if (afterCallFunc !== undefined)
                        afterCallFunc((excelFiles[0]));
                });
            };
            Object.defineProperty(ControllerGrpExcelFile.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpExcelFile";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpExcelFile.prototype, "HtmlDivId", {
                get: function () {
                    return "ExcelFileImportResultsDlg_ControllerGrpExcelFile";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpExcelFile.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpExcelFile.prototype, "model", {
                get: function () {
                    var self = this;
                    return self.$scope.model;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpExcelFile.prototype, "modelGrpExcelFile", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpExcelFile.prototype.getEntityName = function () {
                var self = this;
                return self.model.ExcelFileEntityName;
            };
            ControllerGrpExcelFile.prototype.getEntitiesFromJSON = function (webResponse) {
                var self = this;
                var entlist = fromJSONComplete(webResponse.data);
                return entlist;
            };
            Object.defineProperty(ControllerGrpExcelFile.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpExcelFile.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpExcelFile.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [
                            self.modelGrpExcelFile.modelGrpExcelError.controller
                        ];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpExcelFile.prototype.createBlobModel_GrpExcelFile_itm__data = function () {
                var self = this;
                var ret = new NpTypes.NpBlob(function (e) { }, // on value change
                function (e) { return self.getDownloadUrl_createBlobModel_GrpExcelFile_itm__data(e); }, // download url
                function (e) { return true; }, // is disabled
                function (e) { return false; }, // is invisible
                "", // post url
                false, // add is enabled
                false, // del is enabled
                "", // valid extensions
                function (e) { return 0; } //size in KB
                 //size in KB
                );
                return ret;
            };
            ControllerGrpExcelFile.prototype.getDownloadUrl_createBlobModel_GrpExcelFile_itm__data = function (excelFile) {
                var self = this;
                if (isVoid(self.getEntityName()))
                    return 'javascript:void(0)';
                if (isVoid(excelFile))
                    return 'javascript:void(0)';
                if (isVoid(excelFile.id))
                    return 'javascript:void(0)';
                var url = "/" + self.AppName + "/rest/" + self.getEntityName() + "/getExcelFileImportResultsGrpExcelFile_Data?";
                url += "&id=" + encodeURIComponent(excelFile.id);
                return url;
            };
            return ControllerGrpExcelFile;
        })(Controllers.AbstractGroupFormController);
        ExcelFileImportResultsDlg.ControllerGrpExcelFile = ControllerGrpExcelFile;
        var ModelGrpExcelError = (function (_super) {
            __extends(ModelGrpExcelError, _super);
            function ModelGrpExcelError($scope) {
                _super.call(this, $scope);
                this.$scope = $scope;
                this._fsch_row = new NpTypes.UINumberModel(undefined);
                this._fsch_errMessage = new NpTypes.UIStringModel(undefined);
            }
            Object.defineProperty(ModelGrpExcelError.prototype, "id", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].id;
                },
                set: function (id_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].id = id_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpExcelError.prototype, "_id", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._id;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpExcelError.prototype, "excelRowNum", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].excelRowNum;
                },
                set: function (excelRowNum_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].excelRowNum = excelRowNum_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpExcelError.prototype, "_excelRowNum", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._excelRowNum;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpExcelError.prototype, "errMessage", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].errMessage;
                },
                set: function (errMessage_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].errMessage = errMessage_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpExcelError.prototype, "_errMessage", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._errMessage;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpExcelError.prototype, "exfiId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0].exfiId;
                },
                set: function (exfiId_newVal) {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return;
                    this.selectedEntities[0].exfiId = exfiId_newVal;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpExcelError.prototype, "_exfiId", {
                get: function () {
                    if ((this.selectedEntities === undefined) || (this.selectedEntities[0] === undefined))
                        return undefined;
                    return this.selectedEntities[0]._exfiId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpExcelError.prototype, "fsch_row", {
                get: function () {
                    return this._fsch_row.value;
                },
                set: function (vl) {
                    this._fsch_row.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ModelGrpExcelError.prototype, "fsch_errMessage", {
                get: function () {
                    return this._fsch_errMessage.value;
                },
                set: function (vl) {
                    this._fsch_errMessage.value = vl;
                },
                enumerable: true,
                configurable: true
            });
            return ModelGrpExcelError;
        })(Controllers.AbstractGroupTableModel);
        ExcelFileImportResultsDlg.ModelGrpExcelError = ModelGrpExcelError;
        var ControllerGrpExcelError = (function (_super) {
            __extends(ControllerGrpExcelError, _super);
            function ControllerGrpExcelError($scope, $http, $timeout, Plato) {
                _super.call(this, $scope, $http, $timeout, Plato, new ModelGrpExcelError($scope), { pageSize: 10, maxLinesInHeader: 2 });
                this.$scope = $scope;
                this.$http = $http;
                this.$timeout = $timeout;
                this.Plato = Plato;
                this.getMergedItems_cache = {};
                this.bNonContinuousCallersFuncs = [];
                this._firstLevelChildGroups = undefined;
                var self = this;
                var model = self.model;
                model.controller = self;
                model.grid.showTotalItems = true;
                model.grid.updateTotalOnDemand = false;
                $scope.modelGrpExcelError = self.model;
                $scope.pageModel.modelGrpExcelError = $scope.modelGrpExcelError;
                $scope.clearBtnAction = function () {
                    self.modelGrpExcelError.fsch_row = undefined;
                    self.modelGrpExcelError.fsch_errMessage = undefined;
                    self.updateGrid(0, false, true);
                };
                $scope.modelGrpExcelFile.modelGrpExcelError = $scope.modelGrpExcelError;
                self.$timeout(function () {
                    $scope.$on('$destroy', ($scope.$watch('modelGrpExcelFile.selectedEntities[0]', function () { self.onParentChange(); }, false)));
                }, 50);
                $scope.$on('$destroy', function (evt) {
                    var cols = [];
                    for (var _i = 1; _i < arguments.length; _i++) {
                        cols[_i - 1] = arguments[_i];
                    }
                });
            }
            Object.defineProperty(ControllerGrpExcelError.prototype, "model", {
                get: function () {
                    var self = this;
                    return self.$scope.model;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpExcelError.prototype, "ControllerClassName", {
                get: function () {
                    return "ControllerGrpExcelError";
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpExcelError.prototype, "HtmlDivId", {
                get: function () {
                    return "ExcelFile_ControllerGrpExcelError";
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpExcelError.prototype.gridTitle = function () {
                return "ImportExvel_ResultsDlg_GridTitle";
            };
            ControllerGrpExcelError.prototype.gridColumnFilter = function (field) {
                return true;
            };
            ControllerGrpExcelError.prototype.getGridColumnDefinitions = function () {
                var self = this;
                return [
                    /*{ width: '22', cellTemplate: "<div class=\"GridSpecialButton\" ><button class=\"npGridDelete\" type=\"button\" data-np-click=\"setSelectedRow(row.rowIndex);deleteEntity(row.entity, true, row.rowIndex, false)\" data-ng-disabled=\"deleteIsDisabled(row.entity)\" > </button></div>", cellClass: undefined, field: undefined, displayName: undefined, resizable: undefined, sortable: undefined, enableCellEdit: undefined },*/
                    {
                        cellClass: 'cellToolTip', field: 'excelRowNum', displayName: 'getALString("Row\nNum", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '10%', cellTemplate: "<div data-ng-class='col.colIndex()' > \
                    <input name='GrpExcelError_itm__excelRowNum' data-ng-model='row.entity.excelRowNum' data-np-ui-model='row.entity._excelRowNum' data-ng-readonly='true' data-np-number='dummy' data-np-decimals='0' data-np-decSep=',' data-np-thSep='.' class='ngCellNumber' /> \
                </div>" },
                    {
                        cellClass: 'cellToolTip', field: 'errMessage', displayName: 'getALString("Error\nMessage", true)', requiredAsterisk: false, resizable: true, sortable: true, enableCellEdit: false, width: '80%', cellTemplate: "<div data-ng-class='col.colIndex()' > \
                    <input name='GrpExcelError_itm__errMessage' data-ng-model='row.entity.errMessage' data-np-ui-model='row.entity._errMessage' data-ng-readonly='true' data-np-text='dummy' /> \
                </div>" },
                    {
                        width: '1',
                        cellTemplate: '<div></div>',
                        cellClass: undefined,
                        field: undefined,
                        displayName: undefined,
                        resizable: undefined,
                        sortable: undefined,
                        enableCellEdit: undefined
                    }
                ].filter(function (col) { return col.field === undefined || self.gridColumnFilter(col.field); });
            };
            Object.defineProperty(ControllerGrpExcelError.prototype, "PageModel", {
                get: function () {
                    var self = this;
                    return self.$scope.pageModel;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpExcelError.prototype, "modelGrpExcelError", {
                get: function () {
                    var self = this;
                    return self.model;
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpExcelError.prototype.getEntityName = function () {
                var self = this;
                return self.$scope.modelGrpExcelFile.ExcelErrorEntityName;
            };
            ControllerGrpExcelError.prototype.getEntitiesFromJSON = function (webResponse, parent) {
                var _this = this;
                var self = this;
                var entlist = fromJSONComplete(webResponse.data);
                entlist.forEach(function (x) {
                    if (isVoid(parent)) {
                        x.exfiId = _this.Parent;
                    }
                    else {
                        x.exfiId = parent;
                    }
                });
                return entlist;
            };
            Object.defineProperty(ControllerGrpExcelError.prototype, "Current", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpExcelError.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            ControllerGrpExcelError.prototype.getWebRequestParamsAsString = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var paramData = {};
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.id)) {
                    paramData['exfiId_id'] = self.Parent.id;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpExcelError) && !isVoid(self.modelGrpExcelError.fsch_row)) {
                    paramData['fsch_row'] = self.modelGrpExcelError.fsch_row;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpExcelError) && !isVoid(self.modelGrpExcelError.fsch_errMessage)) {
                    paramData['fsch_errMessage'] = self.modelGrpExcelError.fsch_errMessage;
                }
                var res = Object.keys(paramData).map(function (key) { return key + ":" + paramData[key]; }).join("#");
                return _super.prototype.getWebRequestParamsAsString.call(this, paramIndexFrom, paramIndexTo, excludedIds) + "#" + res;
            };
            ControllerGrpExcelError.prototype.makeWebRequest = function (paramIndexFrom, paramIndexTo, excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = self.getEntityName() + "/findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError";
                var url = "/" + self.AppName + "/rest/" + wsPath + "?";
                var paramData = {};
                paramData['fromRowIndex'] = paramIndexFrom;
                paramData['toRowIndex'] = paramIndexTo;
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (model.sortField !== undefined) {
                    paramData['sortField'] = model.sortField;
                    paramData['sortOrder'] = model.sortOrder == 'asc';
                }
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.id)) {
                    paramData['exfiId_id'] = self.Parent.id;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpExcelError) && !isVoid(self.modelGrpExcelError.fsch_row)) {
                    paramData['fsch_row'] = self.modelGrpExcelError.fsch_row;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpExcelError) && !isVoid(self.modelGrpExcelError.fsch_errMessage)) {
                    paramData['fsch_errMessage'] = self.modelGrpExcelError.fsch_errMessage;
                }
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpExcelError.prototype.makeWebRequest_count = function (excludedIds) {
                if (excludedIds === void 0) { excludedIds = []; }
                var self = this;
                var wsPath = self.getEntityName() + "/findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError_count";
                var url = "/" + self.AppName + "/rest/" + wsPath + "?";
                var paramData = {};
                paramData['exc_Id'] = excludedIds;
                var model = self.model;
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.id)) {
                    paramData['exfiId_id'] = self.Parent.id;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpExcelError) && !isVoid(self.modelGrpExcelError.fsch_row)) {
                    paramData['fsch_row'] = self.modelGrpExcelError.fsch_row;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpExcelError) && !isVoid(self.modelGrpExcelError.fsch_errMessage)) {
                    paramData['fsch_errMessage'] = self.modelGrpExcelError.fsch_errMessage;
                }
                var promise = self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false });
                var wsStartTs = (new Date).getTime();
                return promise.success(function () {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                });
            };
            ControllerGrpExcelError.prototype.getMergedItems = function (func, bContinuousCaller, parent, bForceUpdate) {
                if (bContinuousCaller === void 0) { bContinuousCaller = true; }
                if (bForceUpdate === void 0) { bForceUpdate = false; }
                var self = this;
                var model = self.model;
                function processDBItems(dbEntities) {
                    var mergedEntities = self.processEntities(dbEntities, true);
                    var newEntities = model.newEntities.
                        filter(function (e) { return parent.isEqual(e.a.exfiId); }).
                        map(function (e) { return e.a; });
                    var allItems = newEntities.concat(mergedEntities);
                    func(allItems);
                    self.bNonContinuousCallersFuncs.forEach(function (f) { f(allItems); });
                    self.bNonContinuousCallersFuncs.splice(0);
                }
                if (parent === undefined)
                    parent = self.Parent;
                if (isVoid(parent)) {
                    console.warn('calling getMergedItems and parent is undefined ...');
                    func([]);
                    return;
                }
                if (parent.isNew()) {
                    processDBItems([]);
                }
                else {
                    var bMakeWebRequest = bForceUpdate || self.getMergedItems_cache[parent.id] === undefined;
                    if (bMakeWebRequest) {
                        var pendingRequest = self.getMergedItems_cache[parent.id] !== undefined &&
                            self.getMergedItems_cache[parent.id].a === true;
                        if (pendingRequest)
                            return;
                        self.getMergedItems_cache[parent.id] = new Tuple2(true, undefined);
                        var wsPath = self.getEntityName() + "/findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError";
                        var url = "/" + self.AppName + "/rest/" + wsPath + "?";
                        var paramData = {};
                        paramData['fromRowIndex'] = 0;
                        paramData['toRowIndex'] = 2000;
                        paramData['exc_Id'] = Object.keys(model.deletedEntities);
                        paramData['exfiId_id'] = parent.id;
                        Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                        var wsStartTs = (new Date).getTime();
                        self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                            success(function (response, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            var dbEntities = self.getEntitiesFromJSON(response, parent);
                            self.getMergedItems_cache[parent.id].a = false;
                            self.getMergedItems_cache[parent.id].b = dbEntities;
                            processDBItems(dbEntities);
                        }).error(function (data, status, header, config) {
                            self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                            if (self.$scope.globals.nInFlightRequests > 0)
                                self.$scope.globals.nInFlightRequests--;
                            self.getMergedItems_cache[parent.id].a = false;
                            self.getMergedItems_cache[parent.id].b = [];
                            NpTypes.AlertMessage.addDanger(self.PageModel, data);
                            console.error("Error received in getMergedItems:" + data);
                            console.dir(status);
                            console.dir(header);
                            console.dir(config);
                        });
                    }
                    else {
                        var bPendingRequest = self.getMergedItems_cache[parent.id].a;
                        if (bPendingRequest) {
                            if (!bContinuousCaller) {
                                self.bNonContinuousCallersFuncs.push(func);
                            }
                            else {
                                processDBItems([]);
                            }
                        }
                        else {
                            var dbEntities = self.getMergedItems_cache[parent.id].b;
                            processDBItems(dbEntities);
                        }
                    }
                }
            };
            ControllerGrpExcelError.prototype.expToExcelBtnAction_fileName = function () {
                var self = this;
                var excelFileName = self.$scope.modelGrpExcelFile.filename;
                var parts = excelFileName.split('.');
                parts.pop();
                var excelFileNameWithoutExt = parts.join('.');
                return excelFileNameWithoutExt + "_ImportErrors_" + Utils.convertDateToString(new Date(), "yyyy_MM_dd_HHmm") + ".xls";
            };
            ControllerGrpExcelError.prototype.exportToExcelBtnAction = function () {
                var self = this;
                var wsPath = self.getEntityName() + "/findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError_toExcel";
                var url = "/" + self.AppName + "/rest/" + wsPath + "?";
                var paramData = {};
                paramData['__fields'] = self.getExcelFields();
                if (!isVoid(self) && !isVoid(self.Parent) && !isVoid(self.Parent.id)) {
                    paramData['exfiId_id'] = self.Parent.id;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpExcelError) && !isVoid(self.modelGrpExcelError.fsch_row)) {
                    paramData['fsch_row'] = self.modelGrpExcelError.fsch_row;
                }
                if (!isVoid(self) && !isVoid(self.modelGrpExcelError) && !isVoid(self.modelGrpExcelError.fsch_errMessage)) {
                    paramData['fsch_errMessage'] = self.modelGrpExcelError.fsch_errMessage;
                }
                NpTypes.AlertMessage.clearAlerts(self.PageModel);
                Utils.showWaitWindow(self.$scope, self.Plato, self.$timeout);
                var wsStartTs = (new Date).getTime();
                self.$http.post(url, paramData, { timeout: self.$scope.globals.timeoutInMS, cache: false }).
                    success(function (response, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), true);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    var data = base64DecToArr(response.data);
                    var blob = new Blob([data], { type: "application/octet-stream" });
                    saveAs(blob, self.expToExcelBtnAction_fileName());
                }).error(function (data, status, header, config) {
                    self.$scope.globals.addWsResponseInfo(wsPath, wsStartTs, (new Date).getTime(), false, status);
                    if (self.$scope.globals.nInFlightRequests > 0)
                        self.$scope.globals.nInFlightRequests--;
                    NpTypes.AlertMessage.addDanger(self.PageModel, data);
                });
            };
            ControllerGrpExcelError.prototype.belongsToCurrentParent = function (x) {
                if (this.Parent === undefined || x.exfiId === undefined)
                    return false;
                return x.exfiId.id === this.Parent.id;
            };
            ControllerGrpExcelError.prototype.onParentChange = function () {
                var self = this;
                var newParent = self.Parent;
                self.model.pagingOptions.currentPage = 1;
                if (newParent !== undefined) {
                    if (newParent.isNew())
                        self.getMergedItems(function (items) { self.model.totalItems = items.length; }, false);
                    self.updateGrid();
                }
                else {
                    self.model.visibleEntities.splice(0);
                    self.model.selectedEntities.splice(0);
                    self.model.totalItems = 0;
                }
            };
            Object.defineProperty(ControllerGrpExcelError.prototype, "Parent", {
                get: function () {
                    var self = this;
                    return self.exfiId;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpExcelError.prototype, "ParentController", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpExcelFile.controller;
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpExcelError.prototype, "ParentIsNewOrUndefined", {
                get: function () {
                    var self = this;
                    return self.Parent === undefined || (self.Parent.id.indexOf('TEMP_ID') === 0);
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpExcelError.prototype, "exfiId", {
                get: function () {
                    var self = this;
                    return self.$scope.modelGrpExcelFile.selectedEntities[0];
                },
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(ControllerGrpExcelError.prototype, "FirstLevelChildGroups", {
                get: function () {
                    var self = this;
                    if (self._firstLevelChildGroups === undefined) {
                        self._firstLevelChildGroups = [];
                    }
                    return this._firstLevelChildGroups;
                },
                enumerable: true,
                configurable: true
            });
            return ControllerGrpExcelError;
        })(Controllers.AbstractGroupTableController);
        ExcelFileImportResultsDlg.ControllerGrpExcelError = ControllerGrpExcelError;
    })(ExcelFileImportResultsDlg = Controllers.ExcelFileImportResultsDlg || (Controllers.ExcelFileImportResultsDlg = {}));
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerExcelFileImportResultsDlg.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
