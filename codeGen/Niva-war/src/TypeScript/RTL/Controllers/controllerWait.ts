/// <reference path="../../DefinitelyTyped/angularjs/angular.d.ts" />
/// <reference path="../../DefinitelyTyped/angularjs/angular-cookies.d.ts" />
/// <reference path="../../DefinitelyTyped/underscore/underscore.d.ts" />
/// <reference path="../../DefinitelyTyped/jqueryui/jqueryui.d.ts" />

/// <reference path="../npTypes.ts" />

module Controllers {

    export interface IWaitScope extends NpTypes.IApplicationScope {
        //globals: RTLGlobals;
    }

    export class controllerWait {

        constructor(public $scope: IWaitScope) {
            var dialogOptions = $scope.globals.findAndRemoveDialogOptionByClassName("plzWaitDialog");
            //$scope.globals = globals;

            // This doesn't work deterministically - so we use
            // a $('.plzWaitDialog').remove() in the main periodic 
            // $digest loop (inside main.ts)

            //var timerId = window.setInterval(
            //    () => {
            //        if ($scope.globals.nInFlightRequests === 0) {
            //            window.clearInterval(timerId);
            //            dialogOptions.closeDialog();
            //        }
            //    },
            //    1000);
        }
    }
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
