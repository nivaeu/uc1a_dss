/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />

module Controllers {

    export class JumpModel {
        _XCoord: NpTypes.UINumberModel = new NpTypes.UINumberModel(undefined);
        _YCoord: NpTypes.UINumberModel = new NpTypes.UINumberModel(undefined);

        public get XCoord():number { return this._XCoord.value; }
        public set XCoord(xcoord:number) { this._XCoord.value = xcoord; }

        public get YCoord():number { return this._YCoord.value; }
        public set YCoord(ycoord:number) { this._YCoord.value = ycoord; }

        coordinateSystem: string;
    }

    export interface IJumpScope extends NpTypes.IApplicationScope {
        model: JumpModel;
        nevermind():void;
        navigateToCoords():void;
        allGood():boolean;
    }

    export class controllerJump {

        constructor(public $scope: IJumpScope) {
            var dialogOptions: NpTypes.DialogOptions =
                <NpTypes.DialogOptions>$scope.globals.findAndRemoveDialogOptionByClassName("JumpToCoordDialog");

            var self = this;
            var npMapThis:NpGeo.NpMap = (<any>dialogOptions).npMapThis;

            $scope.model = new JumpModel();
            $scope.model.XCoord = parseFloat(Sprintf.sprintf('%10.2f', (<any>dialogOptions).coords[0]));
            $scope.model.YCoord = parseFloat(Sprintf.sprintf('%10.2f', (<any>dialogOptions).coords[1]));
            $scope.model.coordinateSystem = npMapThis.coordinateSystem;

            $scope.nevermind = function() {
                dialogOptions.jquiDialog.dialog("close");
            }
            $scope.navigateToCoords = function() {
                if (! $scope.model._XCoord.isValid) return;
                if (! $scope.model._YCoord.isValid) return;
                var srcProj = NpGeoGlobals.commonCoordinateSystems(npMapThis.coordinateSystem);
                var dstProj = 'EPSG:3857';
                var coords = proj4(srcProj, dstProj, [$scope.model.XCoord, $scope.model.YCoord]);
                $scope.model.XCoord = coords[0];
                $scope.model.YCoord = coords[1];
                npMapThis.jumpToCoordinates_EPSG3857($scope.model.XCoord, $scope.model.YCoord);
                dialogOptions.jquiDialog.dialog("close");
            }
            $scope.allGood = function () {
                return !isVoid(self.$scope.model.XCoord) && !isVoid(self.$scope.model.YCoord);
            };
        }
    }
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
