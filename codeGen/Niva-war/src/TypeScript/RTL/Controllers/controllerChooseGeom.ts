/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />

module Controllers {

    export class GeometryRow {
        constructor(
            public area:string,
            public areaValue:number,
            public origIdx:number)
        {}
    }

    export class ChooseGeomModel {
        rows: Array<GeometryRow> = [];
        dataSetIdx: number = -1;
    }

    export interface IChooseGeomScope extends NpTypes.IApplicationScope {
        model: ChooseGeomModel;
        nevermind: ()=>void;
        preview: (idx:number)=>void;
        select: (idx:number)=>void;
    }

    export class controllerChooseGeom {

        constructor(
            public $scope: IChooseGeomScope,
            public Plato: Services.INpDialogMaker,
            $timeout:ng.ITimeoutService)
        {
            var dialogOptions: NpTypes.DialogOptions =
                <NpTypes.DialogOptions>$scope.globals.findAndRemoveDialogOptionByClassName("ChooseGeomDialog");
            var npMapThis:NpGeo.NpMap = (<any>dialogOptions).npMapThis;
            var polygonsRemaining:any = (<any>dialogOptions).polygonsRemaining;

            var self = this;
            $scope.model = new ChooseGeomModel();
            polygonsRemaining.forEach((x, origIdx) => {
                if (x.length > 0 && x[0].length > 0) {
                    var coordinateSystem = fallback(npMapThis.coordinateSystem, 'EPSG:2100');
                    var transGeom = NpGeoGlobals.coordinates_to_OL3feature(x).getGeometry().clone().transform(
                        ol.proj.get('EPSG:3857'),
                        ol.proj.get(coordinateSystem)
                    );
                    var area = transGeom.getArea();
                    var output;
                    //output = (Math.round(area / 1000000 * 100) / 100) + '&nbsp;' + 'km<sup>2</sup>';
                    //@p.tsagkis venizelou & nantin asked to get all areas as HA and not m2. 
                    // Stupid???? Maybe...... it is not my desicion anyway.
                    if (area > 10000) {
                        output = (Math.round(area / 1000000 * 100) / 100) + '&nbsp;' + 'km<sup>2</sup>';
                    } else {
                        output = (Math.round(area * 100) / 100) + '&nbsp;' + 'm<sup>2</sup>';
                    }
                    $scope.model.rows.push(new GeometryRow(output, area, origIdx));
                }
            });
            $scope.model.rows.sort((a:GeometryRow, b:GeometryRow) => {
                return b.areaValue - a.areaValue;
            });

            $scope.nevermind = function() {
                dialogOptions.jquiDialog.dialog("close");
            }

            function commonWork(f: ol.Collection<ol.Feature>, idx:number) {
                var croppedFeature = NpGeoGlobals.coordinates_to_OL3feature(polygonsRemaining[idx]);
                var g = croppedFeature.getGeometry();
                var newFeature = new ol.Feature();
                newFeature.setGeometryName("");
                newFeature.setGeometry(g);
                f.clear();
                f.push(newFeature);
                npMapThis.enterDrawMode();
            }

            $scope.preview = function(idx:number) {
                npMapThis.featureOverlayCrop.getSource().clear();
                var features = npMapThis.featureOverlayCrop.getSource().getFeaturesCollection();
                if (features === null) {
                    features = new ol.Collection<ol.Feature>();
                } 
                commonWork(features, idx);
                console.log("polygonsRemaining[idx]========",polygonsRemaining[idx]);
                var directionsClockwise = polygonsRemaining[idx].map(x => NpGeoGlobals.isClockWise(x)).filter(x => x === false);
                console.log("directionsClockwise", directionsClockwise);
                if (directionsClockwise.length > 1) {
                    messageBox(
                        $scope,
                        Plato,
                        "Μη επιτρεπτή γεωμετρία",
                        "Τα πολύγωνα δεν μπορούν να είναι σπασμένα σε κομμάτια.",
                        IconKind.ERROR, 
                        [
                            new Tuple2("OK", () => {})
                        ], 0, 0, '19em');
                    $scope.model.dataSetIdx = -1;
                } else
                    $scope.model.dataSetIdx = idx;
            };

            $scope.select = function() {
                npMapThis.featureOverlayCrop.getSource().clear();
                commonWork(npMapThis.featureOverlayDraw.getSource().getFeaturesCollection(), $scope.model.dataSetIdx);
                dialogOptions.jquiDialog.dialog("close");
            };
        }
    }
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
