/// <reference path="../npTypes.ts" />
var Controllers;
(function (Controllers) {
    var controllerHelp = (function () {
        function controllerHelp($scope) {
            this.$scope = $scope;
            var dialogOptions = $scope.globals.findAndRemoveDialogOptionByClassName("HelpClass");
            $scope.closeHelp = function () {
                dialogOptions.jquiDialog.dialog("close");
            };
        }
        return controllerHelp;
    })();
    Controllers.controllerHelp = controllerHelp;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerHelp.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
