/// <reference path="../npTypes.ts" />
/// <reference path="../geoLib.ts" />
var Controllers;
(function (Controllers) {
    var JumpModel = (function () {
        function JumpModel() {
            this._XCoord = new NpTypes.UINumberModel(undefined);
            this._YCoord = new NpTypes.UINumberModel(undefined);
        }
        Object.defineProperty(JumpModel.prototype, "XCoord", {
            get: function () { return this._XCoord.value; },
            set: function (xcoord) { this._XCoord.value = xcoord; },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(JumpModel.prototype, "YCoord", {
            get: function () { return this._YCoord.value; },
            set: function (ycoord) { this._YCoord.value = ycoord; },
            enumerable: true,
            configurable: true
        });
        return JumpModel;
    })();
    Controllers.JumpModel = JumpModel;
    var controllerJump = (function () {
        function controllerJump($scope) {
            this.$scope = $scope;
            var dialogOptions = $scope.globals.findAndRemoveDialogOptionByClassName("JumpToCoordDialog");
            var self = this;
            var npMapThis = dialogOptions.npMapThis;
            $scope.model = new JumpModel();
            $scope.model.XCoord = parseFloat(Sprintf.sprintf('%10.2f', dialogOptions.coords[0]));
            $scope.model.YCoord = parseFloat(Sprintf.sprintf('%10.2f', dialogOptions.coords[1]));
            $scope.model.coordinateSystem = npMapThis.coordinateSystem;
            $scope.nevermind = function () {
                dialogOptions.jquiDialog.dialog("close");
            };
            $scope.navigateToCoords = function () {
                if (!$scope.model._XCoord.isValid)
                    return;
                if (!$scope.model._YCoord.isValid)
                    return;
                var srcProj = NpGeoGlobals.commonCoordinateSystems(npMapThis.coordinateSystem);
                var dstProj = 'EPSG:3857';
                var coords = proj4(srcProj, dstProj, [$scope.model.XCoord, $scope.model.YCoord]);
                $scope.model.XCoord = coords[0];
                $scope.model.YCoord = coords[1];
                npMapThis.jumpToCoordinates_EPSG3857($scope.model.XCoord, $scope.model.YCoord);
                dialogOptions.jquiDialog.dialog("close");
            };
            $scope.allGood = function () {
                return !isVoid(self.$scope.model.XCoord) && !isVoid(self.$scope.model.YCoord);
            };
        }
        return controllerJump;
    })();
    Controllers.controllerJump = controllerJump;
})(Controllers || (Controllers = {}));
//# sourceMappingURL=controllerJump.js.map 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
