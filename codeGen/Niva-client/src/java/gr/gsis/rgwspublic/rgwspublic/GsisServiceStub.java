package gr.gsis.rgwspublic.rgwspublic;

//import com.sun.xml.internal.ws.api.message.Headers;
//import com.sun.xml.internal.ws.developer.WSBindingProvider;
import com.sun.xml.ws.developer.WSBindingProvider;
import com.sun.xml.ws.api.message.Headers;

import gr.neuropublic.base.Property;
import gr.neuropublic.utils.TwoTuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.ws.Holder;
import java.io.ByteArrayInputStream;
import java.math.BigDecimal;

public class GsisServiceStub {

    private static Logger logger = LoggerFactory.getLogger(GsisServiceStub.class);
    private static final String USERNAME; //= //"NEUROPUBLIC";
    private static final String PASSWORD; //= //"#NEURO1PUBLIC@";

    static {

        USERNAME = Property.getPropertyValue("web-services.properties", "gsisUserName", null);
        PASSWORD = Property.getPropertyValue("web-services.properties", "gsisPassword", null);
    }

    /**
     * Επιστρέφει πληροφορίες από το GSIS
     *
     * Το πρώτο στοιχείο του Tuple περιέχει τα βασικά πεδία του μητρώου στην Δομή RgWsPublicBasicRtUser
     * και το δεύτερο περιέχει στοιχεία για την δραστηριότητα της επιχείρησης RgWsPublicFirmActRtUserArray
     *
     * @param afm
     * @return
     */
    public static TwoTuple<Holder<RgWsPublicBasicRtUser>, Holder<RgWsPublicFirmActRtUserArray>> getAfmInfo(String afm) {

        logger.debug("getAfmInfo() called for "+afm);

        if (afm == null) {
            return null;
        }

        Holder<GenWsErrorRtUser> pErrorRecOut = null;
        RgWsPublic_Service service = null;
        RgWsPublic endPoint = null;
        Holder<RgWsPublicBasicRtUser> rgWsPublicBasicRtOut = null;
        Holder<RgWsPublicFirmActRtUserArray> arrayOfRgWsPublicFirmActRtOut = null;
        Holder<BigDecimal> pCallSeqIdOut = null;

        try {

            service = new RgWsPublic_Service();
            endPoint = service.getRgWsPublicPort();
            WSBindingProvider bp = (WSBindingProvider)endPoint;

            //Προσθέτουμε τον extra SOAP HEADER που ζητάνε
            Element node = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream((
                    "<Security> " +
                    "<UsernameToken> " +
                    "<Username>" + USERNAME + "</Username> " +
                    "<Password>" + PASSWORD + "</Password> " +
                    "</UsernameToken> " +
                    "</Security>").getBytes()))
                    .getDocumentElement();

            bp.setOutboundHeaders(
                Headers.create(node)
            );

            //Γεμίζουμε την XML δομή του SOAP
            RgWsPublicInputRtUser rgWsPublicInputRtIn = new RgWsPublicInputRtUser();
            rgWsPublicInputRtIn.setAfmCalledFor(afm);

            RgWsPublicBasicRtUser rgWsPublicBasicRtUser = new RgWsPublicBasicRtUser();
            rgWsPublicBasicRtOut = new Holder<>(rgWsPublicBasicRtUser);

            RgWsPublicFirmActRtUserArray rgWsPublicFirmActRtUserArray = new RgWsPublicFirmActRtUserArray();
            arrayOfRgWsPublicFirmActRtOut = new Holder<>(rgWsPublicFirmActRtUserArray);

            pCallSeqIdOut = new Holder<>(new BigDecimal("0"));

            GenWsErrorRtUser genWsErrorRtUser = new GenWsErrorRtUser();
            pErrorRecOut = new Holder<>(genWsErrorRtUser);

            //Κάνουμε την κλήση στο GSIS
            endPoint.rgWsPublicAfmMethod(rgWsPublicInputRtIn, rgWsPublicBasicRtOut, arrayOfRgWsPublicFirmActRtOut, pCallSeqIdOut, pErrorRecOut);

        } catch (Exception e) {

            e.printStackTrace();
            throw new RuntimeException("Exception in getAfmInfo() e ="+e.getMessage());
        }

        //Αν το web service επιστρέψει σφάλμα
        if ((pErrorRecOut != null) && (pErrorRecOut.value != null) && (pErrorRecOut.value.getErrorCode() != null) && (pErrorRecOut.value.getErrorCode().length() > 0)){

            logger.error("errorCode =" + pErrorRecOut.value.getErrorCode());
            logger.error("errorDescr =" + pErrorRecOut.value.getErrorDescr());

            throw new RuntimeException("GSIS returned ERROR ="+pErrorRecOut.value.getErrorCode()+" "+pErrorRecOut.value.getErrorDescr());
        }

        System.out.println("pCallSeqIdOut = "+pCallSeqIdOut.value);

        TwoTuple<Holder<RgWsPublicBasicRtUser>, Holder<RgWsPublicFirmActRtUserArray>> returnValue = new TwoTuple<>(rgWsPublicBasicRtOut, arrayOfRgWsPublicFirmActRtOut);
        return returnValue;
    }

    /**
     * Επιστέφει πληροφορίες για την έκδοση του GSIS
     *
     * @return
     */
    public static String getVersionInfo(){

        try {
            RgWsPublic_Service service = new RgWsPublic_Service();
            RgWsPublic endPoint = service.getRgWsPublicPort();

            String version = endPoint.rgWsPublicVersionInfo();
            logger.debug(version);

            return version;

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Exception in getAfmInfo() e ="+e.getMessage());
        }
    }
}
