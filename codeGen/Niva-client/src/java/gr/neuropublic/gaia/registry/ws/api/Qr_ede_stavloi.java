package gr.neuropublic.gaia.registry.ws.api;

import java.math.BigDecimal;
import java.util.List;
import java.util.ArrayList;
import java.sql.Timestamp;

public class Qr_ede_stavloi {

    public String shallowToString() {
        return String.format("%d, %d, %d, %d, %d, %s, %s, %s, %s, %s, %d, %s, %s, %s, %.4f, %d, %d, %s, %.2f, %10.0f, %d",
                             id                     ,
                             qr_ede_id              ,
                             est_id                ,
                             ede_year               ,
                             kodikos                ,
                             dnm_kodikos         ,
                             dnm_description     ,
                             municipality_id     ,
                             municipality_name   ,
                             topothesia          ,
                             iemtype                ,
                             iemtype_description ,
                             dteapometakinhsh ,
                             dteeosmetakinhsh ,
                             tm              ,
                             metakinoumenosflag     ,
                             est_est_id            ,
                             neoxartoypob        ,
                             area_ph         ,
                             dnm_zoa         ,
                             dnm_stavloi);
    }

    public Qr_ede_stavloi  (
                            int id                     ,
                            int qr_ede_id              ,
                            Long est_id                ,
                            int ede_year               ,
                            Integer kodikos            ,
                            String dnm_kodikos         ,
                            String dnm_description     ,
                            String municipality_id     ,
                            String municipality_name   ,
                            String topothesia          ,
                            Integer iemtype            ,
                            String iemtype_description ,
                            Timestamp dteapometakinhsh ,
                            Timestamp dteeosmetakinhsh ,
                            BigDecimal tm              ,
                            int metakinoumenosflag     ,
                            Long est_est_id            ,
                            String neoxartoypob        ,
                            BigDecimal area_ph         ,
                            BigDecimal dnm_zoa         ,
                            int        dnm_stavloi) {

        this.     id                  =      id                  ;
        this.     qr_ede_id           =      qr_ede_id           ;
        this.     est_id              =      est_id              ;
        this.     ede_year            =      ede_year            ;
        this.     kodikos             =      kodikos             ;
        this.     dnm_kodikos         =      dnm_kodikos         ;
        this.     dnm_description     =      dnm_description     ;
        this.     municipality_id     =      municipality_id     ;
        this.     municipality_name   =      municipality_name   ;
        this.     topothesia          =      topothesia          ;
        this.     iemtype             =      iemtype             ;
        this.     iemtype_description =      iemtype_description ;
        this.     dteapometakinhsh    =      dteapometakinhsh    ;
        this.     dteeosmetakinhsh    =      dteeosmetakinhsh    ;
        this.     tm                  =      tm                  ;
        this.     metakinoumenosflag  =      metakinoumenosflag  ;
        this.     est_est_id          =      est_est_id          ;
        this.     neoxartoypob        =      neoxartoypob        ;
        this.     area_ph             =      area_ph             ;

        this.     dnm_zoa             =      dnm_zoa             ;
        this.     dnm_stavloi         =      dnm_stavloi         ;


        this.Qr_ede_zoiko  =     new ArrayList<Qr_ede_zoiko>();
    }

    public    int id                     ;
    public    int qr_ede_id              ;
    public    long est_id                ;
    public    int ede_year               ;
    public    Integer kodikos            ;
    public    String dnm_kodikos         ;
    public    String dnm_description     ;
    public    String municipality_id     ;
    public    String municipality_name   ;
    public    String topothesia          ;
    public    Integer iemtype            ;
    public    String iemtype_description ;
    public    Timestamp dteapometakinhsh ;
    public    Timestamp dteeosmetakinhsh ;
    public    BigDecimal tm              ;
    public    int metakinoumenosflag     ;
    public    Long est_est_id            ;
    public    String neoxartoypob        ;
    public    BigDecimal area_ph         ;

    public BigDecimal  dnm_zoa           ;
    public int         dnm_stavloi       ;


    public List<Qr_ede_zoiko> Qr_ede_zoiko;

}