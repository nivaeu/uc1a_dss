package gr.neuropublic.gaia.registry.ws.api;

import java.math.BigDecimal;

public class Qr_ede_fytiko {

    public String shallowToString() {
        return String.format(
         "%d, %d, %d, %d, %d, %d, %s, %d, %s, %d, %d, %10.2f, %d, %d, %10.2f, %d, %s",
                           id,
                           qr_eda_id,
                           edf_id,
                           ede_year,
                           kodikos,
                           efy_id,
                           efy_description,
                           poi_id,
                           poi_description,
                           dentra,
                           kypseles,
                           epilektash,
                           episporhflag,
                           mastixodentra,
                           epilektash100,
                           elp_id,
                           elp_description);}

    public Qr_ede_fytiko(
                          int id,
                          int qr_eda_id,
                          long edf_id,
                          int ede_year,
                          Integer kodikos,
                          Long efy_id,
                          String efy_description,
                          Long poi_id,
                          String poi_description,
                          Integer dentra,
                          Integer kypseles,
                          BigDecimal epilektash,
                          int episporhflag,
                          int mastixodentra,
                          BigDecimal epilektash100) {
        this(
        id,
        qr_eda_id,
        edf_id,
        ede_year,
        kodikos,
        efy_id,
        efy_description,
        poi_id,
        poi_description,
        dentra,
        kypseles,
        epilektash,
        episporhflag,
        mastixodentra,
        epilektash100,
        null,null);
    }

    public Qr_ede_fytiko(
                          int id,
                          int qr_eda_id,
                          long edf_id,
                          int ede_year,
                          Integer kodikos,
                          Long efy_id,
                          String efy_description,
                          Long poi_id,
                          String poi_description,
                          Integer dentra,
                          Integer kypseles,
                          BigDecimal epilektash,
                          int episporhflag,
                          int mastixodentra,
                          BigDecimal epilektash100,
                          Integer elp_id,
                          String elp_description) {
    this.id = id;
    this.qr_eda_id = qr_eda_id;
    this.edf_id = edf_id;
    this.ede_year = ede_year;
    this.kodikos= kodikos;
    this.efy_id = efy_id;
    this.efy_description = efy_description;
    this.poi_id = poi_id;
    this.poi_description = poi_description;
    this.dentra = dentra;
    this.kypseles = kypseles;
    this.epilektash = epilektash;
    this.episporhflag = episporhflag;
    this.mastixodentra = mastixodentra;
    this.epilektash100 = epilektash100;
    this.elp_id        = elp_id;
    this.elp_description = elp_description;
                             }

    public int id;
    public int qr_eda_id;
    public long edf_id;
    public int ede_year;
    public Integer kodikos;
    public Long efy_id;
    public String efy_description;
    public Long poi_id;
    public String poi_description;
    public Integer dentra;
    public Integer kypseles;
    public BigDecimal epilektash;
    public int episporhflag;
    public int mastixodentra;
    public BigDecimal epilektash100;
    public Integer elp_id;
    public String elp_description;

}