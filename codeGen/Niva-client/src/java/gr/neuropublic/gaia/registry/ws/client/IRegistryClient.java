package gr.neuropublic.gaia.registry.ws.client;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import gr.neuropublic.gaia.registry.ws.api.*;
import gr.neuropublic.mutil.base.Pair;
import gr.neuropublic.mutil.base.Triad;
import gr.neuropublic.mutil.base.JsonProvider;


import java.lang.reflect.Type;

import com.google.gson.reflect.TypeToken;  

public interface IRegistryClient {
    public Pair<Boolean, List<Qr_ede_hd>> findFarmers(String afm, String dnm_kodikos, Long efy_id, Integer epilektashMin, Long poi_id, Long efo_id, Integer max) throws MalformedURLException, IOException;

    public Pair<Boolean, List<Qr_ede_hd>> findFarmersZoiko(String afm, String dnm_kodikos, Long ezo_id_eid, Long ezo_id_kat, Integer zoaMin, Long efo_id, Integer max) throws MalformedURLException, IOException;

    public Triad<Qr_ede_hd_only, List<Fytiko_aksia_det>, List<Zoiko_det>> find_fytiko_aksia (String afm, int ede_year) throws MalformedURLException, IOException;

    public Triad<Qr_ede_hd_only, List<Qr_ede_agroi>, List<Qr_ede_stavloi>> findCombined (String vat, int year) throws MalformedURLException, IOException;



}
