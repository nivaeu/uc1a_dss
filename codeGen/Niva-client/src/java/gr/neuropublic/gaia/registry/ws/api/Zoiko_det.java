package gr.neuropublic.gaia.registry.ws.api;

import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;
import java.text.SimpleDateFormat;
import java.math.BigDecimal;

public class Zoiko_det {

    public String toString() {
        return String.format("(%d, %s, %d, %s, %s, %s, %d)", ezo_id_kat, ezo_id_kat_description, elz_id, elz_kodikos, elz_description, elz_age, zoa);
    }

    public Zoiko_det(int     ezo_id_kat, String ezo_id_kat_description, int    elz_id,
                     String elz_kodikos, String elz_description       , String elz_age, int zoa) {
        this.ezo_id_kat = ezo_id_kat;
        this.ezo_id_kat_description = ezo_id_kat_description;
        this.elz_id = elz_id;
        this.elz_kodikos = elz_kodikos;
        this.elz_description = elz_description;
        this.elz_age = elz_age;
        this.zoa = zoa;
    }

    public int        ezo_id_kat;
    public String     ezo_id_kat_description;
    public int        elz_id;
    public String     elz_kodikos;
    public String     elz_description;
    public String     elz_age;
    public int        zoa;
}