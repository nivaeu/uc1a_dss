package gr.neuropublic.gaia.registry.ws.client;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.ArrayList;

import gr.neuropublic.gaia.registry.ws.api.*;
import gr.neuropublic.mutil.base.Pair;
import gr.neuropublic.mutil.base.Triad;
import gr.neuropublic.mutil.base.JsonProvider;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Type;

import com.google.gson.reflect.TypeToken;  

public class RegistryClient implements IRegistryClient {

    private String baseUrl; // /rest/actions/find?efy_id=456&amp;poi_id=135&amp;epilektashMin=1&amp;dhm_kodikos=16" />

    public RegistryClient(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    private String formEffectiveUrl(String afm, String dnm_kodikos, Long efy_id, Integer epilektashMin, Long poi_id, Long efo_id, Integer max) {
        String methodPoint =  this.baseUrl+"/rest/actions/find?";
        List<String> parameters = new ArrayList<String>();
        if (afm           != null) parameters.add(String.format("afm=%s"              , afm));
        if (dnm_kodikos   != null) parameters.add(String.format("dnm_kodikos=%s"      , dnm_kodikos));
        if (efy_id        != null) parameters.add(String.format("efy_id=%d"           , efy_id));
        if (epilektashMin != null) parameters.add(String.format("epilektashMin=%d"    , epilektashMin));
        if (poi_id        != null) parameters.add(String.format("poi_id=%d"           , poi_id));
        if (efo_id        != null) parameters.add(String.format("efo_id=%d"           , efo_id));
        if (max           != null) parameters.add(String.format("max=%d"              , max));
        return methodPoint+StringUtils.join(parameters, "&");
    }

    private String formEffectiveUrlZoiko(String afm, String dnm_kodikos, Long ezo_id_eid, Long ezo_id_kat, Integer zoaMin, Long efo_id, Integer max) {
        String methodPoint =  this.baseUrl+"/rest/actions/find_zoiko?";
        List<String> parameters = new ArrayList<String>();
        if (afm           != null) parameters.add(String.format("afm=%s"              , afm));
        if (dnm_kodikos   != null) parameters.add(String.format("dnm_kodikos=%s"      , dnm_kodikos));
        if (ezo_id_eid    != null) parameters.add(String.format("ezo_id_eid=%d"       , ezo_id_eid));
        if (ezo_id_kat    != null) parameters.add(String.format("ezo_id_kat=%d"       , ezo_id_kat));
        if (zoaMin        != null) parameters.add(String.format("zoaMin=%d"           , zoaMin));
        if (efo_id        != null) parameters.add(String.format("efo_id=%d"           , efo_id));
        if (max           != null) parameters.add(String.format("max=%d"              , max));
        return methodPoint+StringUtils.join(parameters, "&");
    }

    private String formEffectiveUrlFytikoAksia(String afm, int year) {
        return String.format(this.baseUrl+"/rest/actions/find_fytiko_aksia?afm=%s&ede_year=%d", afm, year);
    }

    private String formEffectiveUrlCombined(String vat, int year) {
        return String.format(this.baseUrl+"/rest/actions/findCombined?afm=%s&year=%d", vat, year);
    }

    @Override
    public Pair<Boolean, List<Qr_ede_hd>> findFarmers(String afm, String dnm_kodikos, Long efy_id, Integer epilektashMin, Long poi_id, Long efo_id, Integer max) throws MalformedURLException, IOException {
        String effectiveUrl = formEffectiveUrl(afm, dnm_kodikos, efy_id, epilektashMin, poi_id, efo_id, max);
        URL wsapp = new URL(effectiveUrl);
        URLConnection webconn = wsapp.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(webconn.getInputStream())); // maybe I should indicate "UTF8" as in line-95
        String line;
        StringBuilder sb = new StringBuilder();
        while ( (line = in.readLine()) != null)
            sb.append(line);
        String content = sb.toString();
        Pair<Boolean, List<Qr_ede_hd>> retValue = decode(content);
        in.close();
        return retValue;
    }

    @Override
    public Pair<Boolean, List<Qr_ede_hd>> findFarmersZoiko(String afm, String dnm_kodikos, Long ezo_id_eid, Long ezo_id_kat, Integer zoaMin, Long efo_id, Integer max) throws MalformedURLException, IOException {
        String effectiveUrl = formEffectiveUrlZoiko(afm, dnm_kodikos, ezo_id_eid, ezo_id_kat, zoaMin, efo_id, max);
        URL wsapp = new URL(effectiveUrl);
        URLConnection webconn = wsapp.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(webconn.getInputStream())); // maybe I should indicate "UTF8" as in line-95
        String line;
        StringBuilder sb = new StringBuilder();
        while ( (line = in.readLine()) != null)
            sb.append(line);
        String content = sb.toString();
        Pair<Boolean, List<Qr_ede_hd>> retValue = decode(content);
        in.close();
        return retValue;
    }



    @Override
    public Triad<Qr_ede_hd_only, List<Fytiko_aksia_det>, List<Zoiko_det>> find_fytiko_aksia (String afm, int ede_year) throws MalformedURLException, IOException {
        String effectiveUrl = formEffectiveUrlFytikoAksia(afm, ede_year);
        URL wsapp = new URL(effectiveUrl);
        URLConnection webconn = wsapp.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(webconn.getInputStream(),"UTF8")); // line-95
        String line;
        StringBuilder sb = new StringBuilder();
        while ( (line = in.readLine()) != null)
            sb.append(line);
        String content = sb.toString();
        Triad<Qr_ede_hd_only, List<Fytiko_aksia_det>, List<Zoiko_det>> retValue = decodeFytikoAksia(content);
        in.close();
        return retValue;    
    }


    private Pair<Boolean, List<Qr_ede_hd>> decode(String json) {
        final Type TYPE= new TypeToken<Pair<Boolean, List<Qr_ede_hd>>>() {}.getType();
        Pair<Boolean, List<Qr_ede_hd>> retValue = JsonProvider.fromJson(json, TYPE);
        return retValue;
    }

    private Triad<Qr_ede_hd_only, List<Fytiko_aksia_det>, List<Zoiko_det>> decodeFytikoAksia(String json) {
        final Type TYPE= new TypeToken<Triad<Qr_ede_hd_only, List<Fytiko_aksia_det>, List<Zoiko_det>>>() {}.getType();
        Triad<Qr_ede_hd_only, List<Fytiko_aksia_det>, List<Zoiko_det>> retValue = JsonProvider.fromJson(json, TYPE); retValue = JsonProvider.fromJson(json, TYPE);
        return retValue;
    }

    @Override
    public Triad<Qr_ede_hd_only, List<Qr_ede_agroi>, List<Qr_ede_stavloi>> findCombined (String vat, int year) throws MalformedURLException, IOException {
        String effectiveUrl = formEffectiveUrlCombined(vat, year);
        URL wsapp = new URL(effectiveUrl);
        URLConnection webconn = wsapp.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(webconn.getInputStream(),"UTF8")); // line-95
        String line;
        StringBuilder sb = new StringBuilder();
        while ( (line = in.readLine()) != null)
            sb.append(line);
        String content = sb.toString();
        Triad<Qr_ede_hd_only, List<Qr_ede_agroi>, List<Qr_ede_stavloi>> retValue = decodeFindCombined(content);
        in.close();
        return retValue;    
    }

    private Triad<Qr_ede_hd_only, List<Qr_ede_agroi>, List<Qr_ede_stavloi>> decodeFindCombined(String json) {
        final Type TYPE= new TypeToken< Triad<Qr_ede_hd_only, List<Qr_ede_agroi>, List<Qr_ede_stavloi>>  >() {}.getType();
        Triad<Qr_ede_hd_only, List<Qr_ede_agroi>, List<Qr_ede_stavloi>> retValue = JsonProvider.fromJson(json, TYPE);
        retValue = JsonProvider.fromJson(json, TYPE);
        return retValue;
    }


}
