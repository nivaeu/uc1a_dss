package gr.neuropublic.gaia.registry.ws.api;

import java.util.List;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.text.ParseException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.io.InputStream;
import java.sql.SQLException;
import java.math.BigDecimal;

import javax.ejb.Local;
import javax.ejb.Remote;

import gr.neuropublic.mutil.base.TreeNode;
import gr.neuropublic.mutil.base.Pair;
import gr.neuropublic.mutil.base.Triad;


public interface IRegistryService {

    public Pair<Boolean, List<Qr_ede_hd>> find(String afm, String dnm_kodikos, Long efy_id, Integer epilektashMin, Long poi_id, Long efo_id, int max) throws SQLException;


    public Pair<Boolean, List<Qr_ede_hd>> findZoiko(String afm, String dnm_kodikos, Long ezo_id_eid, Long ezo_id_kat, Integer zoaMin, Long efo_id, int max) throws SQLException;

    public Triad<Qr_ede_hd_only, List<Fytiko_aksia_det>, List<Zoiko_det>> find_fytiko_aksia (String afm, int ede_year) throws SQLException;

    public Triad<Qr_ede_hd_only, List<Qr_ede_agroi>, List<Qr_ede_stavloi>> findCombined (String vat, int year) throws SQLException;

    public int counter();
    @Local
    public interface ILocal extends IRegistryService {}

    @Remote
    public interface IRemote extends IRegistryService {}

}
