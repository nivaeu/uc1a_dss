package gr.neuropublic.gaia.registry.ws.api;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class Qr_ede_zoiko {

    public String shallowToString() {
        return String.format("%d   ,%d ,%d  ,   %d,   %d  ,%d"+
                             "%s   ,%d ,%s  ,   %s, %.2f,  %d"+
                             "%d   ,%d ,%.2f, %.2f, %.2f,%.2f"+
                             "%.2f ,%d ,%d  , %d  , %d  , %d" +
                             "%d   ,%d ,%d  , %d  , %s",
                         id                           ,
                         qr_est_id                    ,
                         edz_id                      ,
                         ede_year                     ,
                         kodikos                      ,
                         ezo_id_eid                  ,
                         ezo_id_eid_description    ,
                         ezo_id_kat                  ,
                         ezo_id_kat_description    ,
                         kekdb                     ,
                         zoa                   ,
                         zoikoflag                    ,
                         milkflag                     ,
                         zoiko1flag                   ,
                         timhmon               ,
                         syndelestis           ,
                         asfaxiavalue          ,
                         eisforavalue          ,
                         asfaxiavaluepro       ,
                         eniaiaflag                   ,
                         prosthethaigesflag           ,
                         symplprosthethaigesflag      ,
                         prosthethmetaigesflag        ,
                         prosthethmosxidonflag        ,
                         sympprosthethmosxidonflag    ,
                         prostheththilazousonflag     ,
                         sympprostheththilazousonflag ,
                         shrotrofiaflag               ,
                             dtedke                  );
    } 

    public Qr_ede_zoiko(
                        int id                           ,
                        int qr_est_id                    ,
                        long edz_id                      ,
                        int ede_year                     ,
                        int kodikos                      ,
                        long ezo_id_eid                  ,
                        String ezo_id_eid_description    ,
                        long ezo_id_kat                  ,
                        String ezo_id_kat_description    ,
                        String kekdb                     ,
                        BigDecimal zoa                   ,
                        Integer zoikoflag                ,
                        Integer milkflag                 ,
                        Integer zoiko1flag               ,
                        BigDecimal timhmon               ,
                        BigDecimal syndelestis           ,
                        BigDecimal asfaxiavalue          ,
                        BigDecimal eisforavalue          ,
                        BigDecimal asfaxiavaluepro       ,
                        Integer eniaiaflag                   ,
                        Integer prosthethaigesflag           ,
                        Integer symplprosthethaigesflag      ,
                        Integer prosthethmetaigesflag        ,
                        Integer prosthethmosxidonflag        ,
                        Integer sympprosthethmosxidonflag    ,
                        Integer prostheththilazousonflag     ,
                        Integer sympprostheththilazousonflag ,
                        Integer shrotrofiaflag               ,
                        Timestamp dtedke                   
                        ) {
        this(id, 
             qr_est_id                     ,
             edz_id                        ,
             ede_year                      ,
             kodikos                       ,
             ezo_id_eid                    ,
             ezo_id_eid_description        ,
             ezo_id_kat                    ,
             ezo_id_kat_description        ,
             kekdb                         ,
             zoa                           ,
             zoikoflag                     ,
             milkflag                      ,
             zoiko1flag                    ,
             timhmon                       ,
             syndelestis                   ,
             asfaxiavalue                  ,
             eisforavalue                  ,
             asfaxiavaluepro               ,
             eniaiaflag                    ,
             prosthethaigesflag            ,
             symplprosthethaigesflag       ,
             prosthethmetaigesflag         ,
             prosthethmosxidonflag         ,
             sympprosthethmosxidonflag     ,
             prostheththilazousonflag      ,
             sympprostheththilazousonflag  ,
             shrotrofiaflag                ,
             dtedke                        ,
             (Integer) null, (String) null, (String) null, (String) null);
    }

    public Qr_ede_zoiko(
                        int id                           ,
                        int qr_est_id                    ,
                        long edz_id                      ,
                        int ede_year                     ,
                        int kodikos                      ,
                        long ezo_id_eid                  ,
                        String ezo_id_eid_description    ,
                        long ezo_id_kat                  ,
                        String ezo_id_kat_description    ,
                        String kekdb                     ,
                        BigDecimal zoa                   ,
                        Integer zoikoflag                    ,
                        Integer milkflag                     ,
                        Integer zoiko1flag                   ,
                        BigDecimal timhmon               ,
                        BigDecimal syndelestis           ,
                        BigDecimal asfaxiavalue          ,
                        BigDecimal eisforavalue          ,
                        BigDecimal asfaxiavaluepro       ,
                        Integer eniaiaflag                   ,
                        Integer prosthethaigesflag           ,
                        Integer symplprosthethaigesflag      ,
                        Integer prosthethmetaigesflag        ,
                        Integer prosthethmosxidonflag        ,
                        Integer sympprosthethmosxidonflag    ,
                        Integer prostheththilazousonflag     ,
                        Integer sympprostheththilazousonflag ,
                        Integer shrotrofiaflag               ,
                        Timestamp dtedke                 ,
                        Integer elz_id                   ,
                        String  elz_kodikos              ,
                        String  elz_description          ,
                        String  elz_age
                        ) {
        this.id = id;
        this.qr_est_id                     = qr_est_id                    ;
        this.edz_id                        = edz_id                       ;
        this.ede_year                      = ede_year                     ;
        this.kodikos                       = kodikos                      ;
        this.ezo_id_eid                    = ezo_id_eid                   ;
        this.ezo_id_eid_description        = ezo_id_eid_description       ;
        this.ezo_id_kat                    = ezo_id_kat                   ;
        this.ezo_id_kat_description        = ezo_id_kat_description       ;
        this.kekdb                         = kekdb                        ;
        this.zoa                           = zoa                          ;
        this.zoikoflag                     = zoikoflag                    ;
        this.milkflag                      = milkflag                     ;
        this.zoiko1flag                    = zoiko1flag                   ;
        this.timhmon                       = timhmon                      ;
        this.syndelestis                   = syndelestis                  ;
        this.asfaxiavalue                  = asfaxiavalue                 ;
        this.eisforavalue                  = eisforavalue                 ;
        this.asfaxiavaluepro               = asfaxiavaluepro              ;
        this.eniaiaflag                    = eniaiaflag                   ;
        this.prosthethaigesflag            = prosthethaigesflag           ;
        this.symplprosthethaigesflag       = symplprosthethaigesflag      ;
        this.prosthethmetaigesflag         = prosthethmetaigesflag        ;
        this.prosthethmosxidonflag         = prosthethmosxidonflag        ;
        this.sympprosthethmosxidonflag     = sympprosthethmosxidonflag    ;
        this.prostheththilazousonflag      = prostheththilazousonflag     ;
        this.sympprostheththilazousonflag  = sympprostheththilazousonflag ;
        this.shrotrofiaflag                = shrotrofiaflag               ;
        this.dtedke                        = dtedke                       ;
        this.elz_id           = elz_id;
        this.elz_kodikos      = elz_kodikos;
        this.elz_description  = elz_description;
        this.elz_age          = elz_age;
    }

    public    int id                           ;// integer                        
    public    int qr_est_id                    ;// integer                        
    public    long edz_id                       ;// numeric(12,0)                  
    public    int ede_year                     ;// smallint                       
    public    int kodikos                      ;// numeric(5,0)                   
    public    long ezo_id_eid                   ;// numeric(12,0)                  
    public    String ezo_id_eid_description       ;// character varying(200)         
    public    long ezo_id_kat                   ;// numeric(12,0)                  
    public    String ezo_id_kat_description       ;// character varying(200)         
    public    String kekdb                        ;// character varying(10)          
    public    BigDecimal zoa                          ;// numeric                        
    public    Integer zoikoflag                    ;// numeric(1,0)                   
    public    Integer milkflag                     ;// numeric(1,0)                   
    public    Integer zoiko1flag                   ;// numeric(1,0)                   
    public    BigDecimal timhmon                      ;// numeric                        
    public    BigDecimal syndelestis                  ;// numeric                        
    public    BigDecimal asfaxiavalue                 ;// numeric                        
    public    BigDecimal eisforavalue                 ;// numeric                        
    public    BigDecimal asfaxiavaluepro              ;// numeric                        
    public    Integer eniaiaflag                   ;// numeric(1,0)                   
    public    Integer prosthethaigesflag           ;// numeric(1,0)                   
    public    Integer symplprosthethaigesflag      ;// numeric(1,0)                   
    public    Integer prosthethmetaigesflag        ;// numeric(1,0)                   
    public    Integer prosthethmosxidonflag        ;// numeric(1,0)                   
    public    Integer sympprosthethmosxidonflag    ;// numeric(1,0)                   
    public    Integer prostheththilazousonflag     ;// numeric(1,0)                   
    public    Integer sympprostheththilazousonflag ;// numeric(1,0)                   
    public    Integer shrotrofiaflag               ;// numeric(1,0)                   
    public    Timestamp dtedke                 ;// timestamp(0) without time zone 

    public Integer elz_id;
    public String elz_kodikos;
    public String elz_description;
    public String elz_age;


}
