package gr.neuropublic.gaia.registry.ws.api;

import java.math.BigDecimal;
import java.util.List;
import java.util.ArrayList;

public class Qr_ede_agroi {

    public String shallowToString() {
        return String.format("%d, %d, %d, %d, %d, %s, %s, %s, %s, %s, %d, %s, %d, %s, %10.2f, %10.2f, %10.2f, %d, %10.2f, %10.2f, %10.2f, %10.2f %s, %10.2f, %10.2f, %b, %10.2f, %d",
                          id,
                          qr_ede_id ,
                          eda_id,
                          ede_year,
                          kodikos,
                          dnm_kodikos,
                          dnm_description,
                          municipality_id,
                          municipality_name,
                          topothesia,
                          iemtype,
                          iemtype_description,
                          xptype,
                          xptype_description,
                          totalektash,
                          epilektash100,
                          kdektash,
                          synidioflag,
                          synidiopercent,
                          ektashap,
                          da_100,
                          d_a,
                          neoxartoypob,
                          area_ph,
                          perim_p,
                          isbiological,
                          dnm_epilektash,
                          dnm_plots);}

    public Qr_ede_agroi  (
                          int id,
                          int qr_ede_id ,
                          long eda_id,
                          int ede_year,
                          Integer kodikos,
                          String dnm_kodikos,
                          String dnm_description,
                          String municipality_id,
                          String municipality_name,
                          String topothesia,
                          Integer iemtype,
                          String iemtype_description,
                          Integer xptype,
                          String xptype_description,
                          BigDecimal totalektash,
                          BigDecimal epilektash100,
                          BigDecimal kdektash,
                          Integer synidioflag,
                          BigDecimal synidiopercent,
                          BigDecimal ektashap,
                          BigDecimal da_100,
                          BigDecimal d_a,
                          String  neoxartoypob,
                          BigDecimal area_ph,
                          BigDecimal perim_p, 
                          boolean isbiological,
                          BigDecimal dnm_epilektash,
                          int        dnm_plots
                         ) {
        this.id = id;
        this.qr_ede_id  = qr_ede_id;
        this.eda_id = eda_id;
        this.ede_year = ede_year;
        this.kodikos  =kodikos;
        this.dnm_kodikos =dnm_kodikos;
        this.dnm_description = dnm_description;
        this.municipality_id = municipality_id;
        this.municipality_name = municipality_name;
        this.topothesia = topothesia;
        this.iemtype = iemtype;
        this.iemtype_description = iemtype_description;
        this.xptype = xptype;
        this.xptype_description = xptype_description;
        this.totalektash = totalektash;
        this.epilektash100  =epilektash100;
        this.kdektash  =kdektash;
        this.synidioflag  =synidioflag;
        this.synidiopercent = synidiopercent;
        this.ektashap = ektashap;
        this.da_100  =da_100;
        this.d_a  =d_a;
        this.neoxartoypob = neoxartoypob;
        this.area_ph  =area_ph;
        this.perim_p = perim_p;
        this.isbiological = isbiological;
        this.dnm_epilektash = dnm_epilektash;
        this.dnm_plots = dnm_plots;
        this.Qr_ede_fytiko  =     new ArrayList<Qr_ede_fytiko>();
    }

    public int id;         //                  | integer                | not null default nextval('gr_query.eda_id_seq'::regclass) | plain    | 
    public int qr_ede_id ; //          | integer                | not null                                                  | plain    | 
    public long eda_id;     //              | numeric(12,0)          | not null                                                  | main     | 
    public int ede_year; //            | smallint               | not null                                                  | plain    | 
    public Integer kodikos; //             | numeric(5,0)           |                                                           | main     | 
    public String dnm_kodikos;         // | character varying(2)   |                                                           | extended | 
    public String dnm_description; //    | character varying(40)  |                                                           | extended | 
    public String municipality_id; //     | character varying(10)  |                                                           | extended | 
    public String municipality_name; //   | character varying(100) |                                                           | extended | 
    public String topothesia; //          | character varying(50)  |                                                           | extended | 
    public Integer iemtype; //             | numeric(2,0)           |                                                           | main     | 
    public String iemtype_description; // | character varying(50)  |                                                           | extended | 
    public Integer xptype; //              | numeric(2,0)           |                                                           | main     | 
    public String xptype_description; //  | character varying(50)  |                                                           | extended | 
    public BigDecimal totalektash; //         | numeric                |                                                           | main     | 
    public BigDecimal epilektash100; //       | numeric                |                                                           | main     | 
    public BigDecimal kdektash; //            | numeric                |                                                           | main     | 
    public Integer synidioflag; //         | numeric(1,0)           |                                                           | main     | 
    public BigDecimal synidiopercent; //      | numeric                |                                                           | main     | 
    public BigDecimal ektashap; //            | numeric                |                                                           | main     | 
    public BigDecimal da_100; //              | numeric                |                                                           | main     | 
    public BigDecimal d_a; //                 | numeric                |                                                           | main     | 
    public String  neoxartoypob; //        | character varying(20)  |                                                           | extended | 
    public BigDecimal area_ph;   //           | numeric                |                                                           | main     | 
    public BigDecimal perim_p; //             | numeric                |                                                           | main     | 
    public boolean isbiological;
    
    public BigDecimal  dnm_epilektash;
    public int         dnm_plots;

    public List<Qr_ede_fytiko> Qr_ede_fytiko;
}