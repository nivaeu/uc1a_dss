package gr.neuropublic.gaia.usermngmt.util;

import java.lang.reflect.InvocationTargetException;

public class StatusException extends UserMngmtException {
    private int status;
    public StatusException (int status) {
        this.status = status;
    }
    public int getStatus() {
        return status;
    }

    public static <T extends StatusException> void raiseIfNot(Class<T> klass, int code, int shouldBe) throws T{
        try {
            if (code != shouldBe) throw klass.getConstructor(Integer.TYPE).newInstance(code);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T extends StatusException> void raiseIfNotZero(Class<T> klass, int code) throws T{
        raiseIfNot(klass, code, 0);
    }


}