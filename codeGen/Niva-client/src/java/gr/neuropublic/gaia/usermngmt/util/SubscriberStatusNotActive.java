package gr.neuropublic.gaia.usermngmt.util;

public class SubscriberStatusNotActive extends StatusException {
    public SubscriberStatusNotActive(int status) {
        super(status);
    }
}