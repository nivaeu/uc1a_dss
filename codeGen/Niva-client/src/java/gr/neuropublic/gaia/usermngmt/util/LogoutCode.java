package gr.neuropublic.gaia.usermngmt.util;

public enum LogoutCode {
    USER(1), EXPIRY(2), CONCURRENT(3);

    private int code;
    private LogoutCode(int code) {
        this.code = code;
    }
    public int getCode() {
        return code;
    }


    public static LogoutCode fromCode(int code) {
        switch (code) {
        case 1:
            return LogoutCode.USER;
        case 2:
            return LogoutCode.EXPIRY;
        case 3:
            return LogoutCode.CONCURRENT;
        default:
            throw new RuntimeException("Unknown LogoutCode: "+code);
        }
    }
}