package gr.neuropublic.gaia.usermngmt.util;
public enum UpdateUserOption {
    BY_EMAIL("user_email"), BY_VAT("user_vat"), BY_PORTAL_ID("user_portal_id");
    private String columnName;
    private UpdateUserOption(String columnName) {
        this.columnName = columnName;
    }
    public String getColumnName() {
        return columnName;
    }
}