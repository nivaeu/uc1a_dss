package gr.neuropublic.gaia.usermngmt.util;

public class UnknownApp extends UserMngmtException {

    private String app;
    public UnknownApp(String app) {
        this.app = app;
    }
    public String getAppName() {
        return this.app;
    }

    public String toString() {
        return String.format("%s(%s)", this.getClass().getName(), this.app);
    }

}