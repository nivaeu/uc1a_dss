package gr.neuropublic.gaia.usermngmt.util;

public class SubscriberModuleGroupNotActive extends StatusException {
    public SubscriberModuleGroupNotActive (int status) {
        super(status);
    }

}