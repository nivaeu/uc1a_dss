package gr.neuropublic.gaia.usermngmt.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class SubscriberModuleGroupTemporalMismatch extends UserMngmtException {
    private String    subs_short_name;
    private String    appName;
    private Timestamp from;
    private Timestamp until;
    

    public SubscriberModuleGroupTemporalMismatch (String subs_short_name, String appName, Timestamp from, Timestamp until ) {
        this.subs_short_name = subs_short_name;
        this.appName         = appName;
        this.from            = from;
        this.until           = until;
    }

    public static void raiseIfNecessary(String subs_short_name, String appName, Timestamp from, Timestamp until, Timestamp t) throws SubscriberModuleGroupTemporalMismatch {
        if ((t.before(from)) || (until.before(t))) {
            throw new SubscriberModuleGroupTemporalMismatch(subs_short_name, appName, from, until);
        }
    }

    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return String.format("%s (%s, %s, %s, %s)", this.getClass().getName(), subs_short_name, appName, sdf.format(this.from), sdf.format(this.until));
    }

}