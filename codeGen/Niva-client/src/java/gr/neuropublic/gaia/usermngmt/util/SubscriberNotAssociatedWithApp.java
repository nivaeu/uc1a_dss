package gr.neuropublic.gaia.usermngmt.util;

public class SubscriberNotAssociatedWithApp extends UserMngmtException {

    public String email;
    public int subs_id;
    public String subs_short_name;
    public String app;

    public SubscriberNotAssociatedWithApp(String email, int subs_id, String subs_short_name, String app) {
        this.email = email;
        this.subs_id = subs_id;
        this.subs_short_name = subs_short_name;
        this.app = app;
    }

    public String toString() {
        return String.format("%s(%s, %d, %s, %s)", this.getClass().getName(), email, subs_id, subs_short_name, app);
    }

}