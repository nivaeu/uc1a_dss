package gr.neuropublic.validators;

import gr.neuropublic.validators.EntityValidationError;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

public interface ICheckedEntity {
    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException;
    
    public void setInsertUser(String userName, String clientIp);
    
    public void setUpdateUser(String userName, String clientIp);
	
    public ICheckedEntity clone(Map<String, ICheckedEntity> alreadyCloned);

    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto);
	
    public Object getPrimaryKeyValue();

    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto);
	
    public String getEntityName();
	
}
