/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.validators;

import java.util.ArrayList;
import java.util.List;


public class EntityValidationSubError {
    private String errorId;
    public String getErrorId() {return errorId;}
    public void setErrorId(String errorId) {this.errorId = errorId;}

    private List<String> args;
    public List<String> getArgs() {return args;}
    public void setArgs(List<String> args) {this.args = args;}
    
    public EntityValidationSubError() {
        errorId = null;
        args = new ArrayList<String>();
    }

    public EntityValidationSubError(String errId) {
        errorId = errId;
        args = new ArrayList<String>();
    }
    
    @Override
    public String toString() {
        String ret = errorId;
        for(String s : args) {
            ret += "@"+s;
        }

        return ret;
    }

    public String toStringJs() {
        String ret = errorId;
        
        for( int i =0; i < args.size(); i++) {
            String s = args.get(i);
            if (i == 0) {
                ret = ret + "(";
            }
            ret += "'" + s + "'";
            if (i < args.size() - 1) {
                ret += ",";
            }
            if (i == args.size() - 1) {
                ret = ret + ")";
            }
        }

        return ret;
    }
    
    
    public static EntityValidationSubError createFromString(String s) {
        EntityValidationSubError ret = new EntityValidationSubError();
        String[] parts = s.split("@");
        
        ret.setErrorId(parts[0]);
        
        for(int i=1; i<parts.length; i++) {
            ret.getArgs().add(parts[i]);
        }

        return ret;
    }

}
