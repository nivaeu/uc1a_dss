package gr.neuropublic.validators.impl;

import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.validators.annotations.ValidIBAN;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.validators.annotations.ValidTin;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigInteger;
import java.util.Arrays;

public class IBANValidator implements ConstraintValidator<ValidIBAN, String> {

    @Override
    public void initialize(ValidIBAN a) {
        //no initialization needed
    }

    @Override
    public boolean isValid(String iban, ConstraintValidatorContext context) {

        boolean returnValue = true;

        //Αν το ΙΒΑΝ είναι έγκυρο
        if(new IBAN(iban).isValid() ){
            return true;
        }
        else{

            //Set Error classes
            EntityValidationError entityValidationError = new EntityValidationError();
            EntityValidationSubError subError = new EntityValidationSubError("ibanValidator.ibanErrorMessage");
            subError.getArgs().add(iban);
            entityValidationError.getSubErrors().add(subError);

            //Set column names to message
            context.disableDefaultConstraintViolation();
            //context.buildConstraintViolationWithTemplate( entityValidationError.toString() ).addConstraintViolation();
            context.buildConstraintViolationWithTemplate( entityValidationError.toString() ).addConstraintViolation();

            return false;
        }

    }

    private class IBAN {

        private final BigInteger BD_97 = new BigInteger("97");
        private final BigInteger BD_98 = new BigInteger("98");
        private String invalidCause = null;
        private String ibanValue = null;

        /**
         * Get the IBAN
         * @return a string with the IBAN
         */
        public String getIbanValue() {
            return ibanValue;
        }

        /**
         * Set the IBAN
         * @param iban the IBAN to set
         */
        public void setIbanValue(String iban) {
            this.ibanValue = iban;
        }

        /**
         * Create an IBAN object with the given ibanValue code.
         * This constructor does not perform any validation on the ibanValue, only
         * @param iban
         */
        public IBAN(String iban) {
            this.ibanValue = iban;
        }

        /**
         * Completely validate an IBAN
         * Currently validation checks that the length is at least 5 chars:
         * (2 country code, 2 verifying digits, and 1 BBAN)
         * checks the country code to be valid an the BBAN to match the verifying digits
         *
         * @return <code>true</code> if the IBAN is found to be valid and <code>false</code> in other case
         */
        public boolean isValid() {

            if (this.ibanValue==null)
                return true;//Οταν το IBAN δεν έχει τιμή δεν τρέχει ο validator και δεν προκαλεί σφάλμα

            invalidCause = null;
            final String code = removeNonAlpha(this.ibanValue);
            final int len = code.length();
            if (len<4) {
                this.invalidCause="Too short (expected at least 4, got "+len+")";
                return false;
            }
            final String country = code.substring(0, 2);
            if (! Arrays.asList(countriesArray).contains( (country != null) ? country.toUpperCase() : null ) ) {
                this.invalidCause = "Invalid ISO country code: "+country;
                return false;
            }

            final StringBuffer bban = new StringBuffer(code.substring(4));
            if (bban.length()==0) {
                this.invalidCause="Empty Basic Bank Account Number";
                return false;
            }
            bban.append(code.substring(0, 4));

            String workString = translateChars(bban);
            int mod = modulo97(workString);
            if (mod!=1) {
                this.invalidCause = "Verification failed (expected 1 and obtained "+mod+")";
                return false;
            }

            return true;
        }


        /**
         * Translate letters to numbers, also ignoring non alphanumeric characters
         *
         * @param bban
         * @return the translated value
         */
        public String translateChars(final StringBuffer bban) {
            final StringBuffer result = new StringBuffer();
            for (int i=0;i<bban.length();i++) {
                char c = bban.charAt(i);
                if (Character.isLetter(c)) {
                    result.append(Character.getNumericValue(c));
                } else {
                    result.append((char)c);
                }
            }
            return result.toString();
        }

        /**
         *
         * @param iban
         * @return the resulting IBAN
         */
        public String removeNonAlpha(final String iban) {
            final StringBuffer result = new StringBuffer();
            for (int i=0;i<iban.length();i++) {
                char c = iban.charAt(i);
                if (Character.isLetter(c) || Character.isDigit(c) ) {
                    result.append((char)c);
                }
            }
            return result.toString();
        }

        private int modulo97(String bban) {
            BigInteger b = new BigInteger(bban);
            b = b.divideAndRemainder(BD_97)[1];
            b = BD_98.min(b);
            b = b.divideAndRemainder(BD_97)[1];
            return b.intValue();
            //return ((int)(98 - (Long.parseLong(bban) * 100) % 97L)) % 97;
        }

        /**
         * Get a string with information about why the IBAN was found invalid
         * @return a human readable (english) string
         */
        public String getInvalidCause() {
            return invalidCause;
        }

        public boolean equals(Object o)
        {
            if (o == this)
                return true;
            if (!(o instanceof IBAN))
                return false;
            IBAN iban = (IBAN) o;
            return iban.ibanValue == ibanValue;
        }

        public int hashCode() {
            return ibanValue.hashCode();
        }

        public String toString() {
            return ("IBAN Class with ibanValue = "+this.getIbanValue());
        }

    }

    private String[] countriesArray = new String[] {
            "AF",
            "AX",
            "AL" ,
            "DZ" ,
            "AS" ,
            "AD" ,
            "AO" ,
            "AI" ,
            "AQ" ,
            "AG" ,
            "AR" ,
            "AM" ,
            "AW" ,
            "AU" ,
            "AT" ,
            "AZ" ,
            "BS" ,
            "BH" ,
            "BD" ,
            "BB" ,
            "BY" ,
            "BE" ,
            "BZ" ,
            "BJ" ,
            "BM" ,
            "BT" ,
            "BO" ,
            "BA" ,
            "BW" ,
            "BV" ,
            "BR" ,
            "IO" ,
            "BN" ,
            "BG" ,
            "BF" ,
            "BI" ,
            "KH" ,
            "CM" ,
            "CA" ,
            "CV" ,
            "KY" ,
            "CF" ,
            "TD" ,
            "CL" ,
            "CN" ,
            "CX" ,
            "CC" ,
            "CO" ,
            "KM" ,
            "CG" ,
            "CD" ,
            "CK" ,
            "CR" ,
            "CI" ,
            "HR" ,
            "CU" ,
            "CY" ,
            "CZ" ,
            "DK" ,
            "DJ" ,
            "DM" ,
            "DO" ,
            "EC" ,
            "EG" ,
            "SV" ,
            "GQ" ,
            "ER" ,
            "EE" ,
            "ET" ,
            "FK" ,
            "FO" ,
            "FJ" ,
            "FI" ,
            "FR" ,
            "GF" ,
            "PF" ,
            "TF" ,
            "GA" ,
            "GM" ,
            "GE" ,
            "DE" ,
            "GH" ,
            "GI" ,
            "GR" ,
            "GL" ,
            "GD" ,
            "GP" ,
            "GU" ,
            "GT" ,
            "GG" ,
            "GN" ,
            "GW" ,
            "GY" ,
            "HT" ,
            "HM" ,
            "VA" ,
            "HN" ,
            "HK" ,
            "HU" ,
            "IS" ,
            "IN" ,
            "ID" ,
            "IR" ,
            "IQ" ,
            "IE" ,
            "IM" ,
            "IL" ,
            "IT" ,
            "JM" ,
            "JP" ,
            "JE" ,
            "JO" ,
            "KZ" ,
            "KE" ,
            "KI" ,
            "KP" ,
            "KR" ,
            "KW" ,
            "KG" ,
            "LA" ,
            "LV" ,
            "LB" ,
            "LS" ,
            "LR" ,
            "LY" ,
            "LI" ,
            "LT" ,
            "LU" ,
            "MO" ,
            "MK" ,
            "MG" ,
            "MW" ,
            "MY" ,
            "MV" ,
            "ML" ,
            "MT" ,
            "MH" ,
            "MQ" ,
            "MR" ,
            "MU" ,
            "YT" ,
            "MX" ,
            "FM" ,
            "MD" ,
            "MC" ,
            "MN" ,
            "ME" ,
            "MS" ,
            "MA" ,
            "MZ" ,
            "MM" ,
            "NA" ,
            "NR" ,
            "NP" ,
            "NL" ,
            "AN" ,
            "NC" ,
            "NZ" ,
            "NI" ,
            "NE" ,
            "NG" ,
            "NU" ,
            "NF" ,
            "MP" ,
            "NO" ,
            "OM" ,
            "PK" ,
            "PW" ,
            "PS" ,
            "PA" ,
            "PG" ,
            "PY" ,
            "PE" ,
            "PH" ,
            "PN" ,
            "PL" ,
            "PT" ,
            "PR" ,
            "QA" ,
            "RE" ,
            "RO" ,
            "RU" ,
            "RW" ,
            "BL" ,
            "SH" ,
            "KN" ,
            "LC" ,
            "MF" ,
            "PM" ,
            "VC" ,
            "WS" ,
            "SM" ,
            "ST" ,
            "SA" ,
            "SN" ,
            "RS" ,
            "SC" ,
            "SL" ,
            "SG" ,
            "SK" ,
            "SI" ,
            "SB" ,
            "SO" ,
            "ZA" ,
            "GS" ,
            "ES" ,
            "LK" ,
            "SD" ,
            "SR" ,
            "SJ" ,
            "SZ" ,
            "SE" ,
            "CH" ,
            "SY" ,
            "TW" ,
            "TJ" ,
            "TZ" ,
            "TH" ,
            "TL" ,
            "TG" ,
            "TK" ,
            "TO" ,
            "TT" ,
            "TN" ,
            "TR" ,
            "TM" ,
            "TC" ,
            "TV" ,
            "UG" ,
            "UA" ,
            "AE" ,
            "GB" ,
            "US" ,
            "UM" ,
            "UY" ,
            "UZ" ,
            "VU" ,
            "VE" ,
            "VN" ,
            "VG" ,
            "VI" ,
            "WF" ,
            "EH" ,
            "YE" ,
            "ZM" ,
            "ZW"};

}