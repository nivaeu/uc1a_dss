package gr.neuropublic.validators.impl;

import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.validators.annotations.ValidTin;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TinValidator implements ConstraintValidator<ValidTin, String> {

    @Override
    public void initialize(ValidTin a) {
        //no initialization needed
    }

    @Override
    public boolean isValid(String tin, ConstraintValidatorContext context) {


        boolean returnValue = (tin != null) ? validateTin(tin) : true;//Αν δεν έρθει ΑΦΜ ο validator δεν ενεργοποιείται

        if(returnValue){
            return true;
        }
        else{
            
            //Set Error classes
            EntityValidationError entityValidationError = new EntityValidationError();
            EntityValidationSubError subError = new EntityValidationSubError("tinValidator.tinErrorMessage");
            subError.getArgs().add(tin);
            entityValidationError.getSubErrors().add(subError);
            
            //Set column names to message
            context.disableDefaultConstraintViolation();
            //context.buildConstraintViolationWithTemplate( entityValidationError.toString() ).addConstraintViolation();
            context.buildConstraintViolationWithTemplate( entityValidationError.toString() ).addConstraintViolation();
            
            return false;
        }

    }

    public static boolean validateTin(String tin) {

        boolean returnValue = true;

        long iSum;
        long btRem;
        boolean CheckAFM = false;
        if (tin.equals("") || tin.length() != 9) {
            returnValue = false;
        }

        if(returnValue){

            iSum = 0;
            try {
                double value1 = Double.parseDouble(tin);
            } catch (NumberFormatException e) {
                returnValue = false;
            }

            if(returnValue){

                for (int i = 0; i < tin.length() - 1; i++) {
                    //System.out.println("\npower of 2=" + ((int) Math.pow(2, (this.afmValue.length() - i - 1))));
                    iSum = iSum + Integer.parseInt(tin.substring(i, i + 1)) * (int) Math.pow(2, (tin.length() - i - 1));
                }
                //System.out.println("\nisum=" + iSum);
                if (iSum == 0) {
                    returnValue = false;
                }

                if(returnValue){

                    btRem = iSum % 11;
                    int lastDigit = Integer.parseInt(tin.substring(tin.length() - 1, tin.length()));
                    if (lastDigit == btRem || (btRem == 10 && lastDigit == 0)) {
                        returnValue = true;
                    }
                    else{
                         returnValue = false;
                    }
                }
            }
        }
        return returnValue;
    }
}