package gr.neuropublic.validators.annotations;

import gr.neuropublic.validators.impl.ValidateUniqueKeysImpl;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
 
@Constraint(validatedBy = { ValidateUniqueKeysImpl.class })
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateUniqueKeys {
 
 
    String message() default " Values are not unique ";
 
    Class<?>[] groups() default {};
 
    Class<? extends Payload>[] payload() default {};
 
}