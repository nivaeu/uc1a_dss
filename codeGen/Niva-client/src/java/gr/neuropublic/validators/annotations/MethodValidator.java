
package gr.neuropublic.validators.annotations;

import gr.neuropublic.validators.impl.TinValidator;
import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.METHOD;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;


@Documented
@Constraint(validatedBy = TinValidator.class)
@Target({METHOD})
@Retention(RUNTIME)
public @interface MethodValidator {
    	String message() default "";
        String needDBConnection() default "false";
    	Class<?>[] groups() default {};
        Class<? extends Payload>[] payload() default {};
}
