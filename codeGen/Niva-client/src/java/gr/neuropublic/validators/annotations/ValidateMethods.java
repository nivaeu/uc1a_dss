package gr.neuropublic.validators.annotations;


import gr.neuropublic.validators.impl.ValidateMethodsImpl; 
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
 
import javax.validation.Constraint;
import javax.validation.Payload;
 
@Constraint(validatedBy = { ValidateMethodsImpl.class })
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidateMethods {
 
    String message() default " Entity is not valid";
 
    Class<?>[] groups() default {};
 
    Class<? extends Payload>[] payload() default {};
}