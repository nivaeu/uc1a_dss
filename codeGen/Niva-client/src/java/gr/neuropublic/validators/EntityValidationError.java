package gr.neuropublic.validators;

import java.util.ArrayList;
import java.util.List;



public class EntityValidationError 
{

    public Boolean hasErrors() {return subErrors.size()>0;}
    
    private List<EntityValidationSubError> subErrors =new ArrayList<EntityValidationSubError>();
    public List<EntityValidationSubError> getSubErrors() { return subErrors;}
    public void setSubErrors(List<EntityValidationSubError> subErrors) {this.subErrors = subErrors;}
    
    @Override
    public String toString() {
        String ret= "";

        for(int i=0; i< subErrors.size(); i++) {
            EntityValidationSubError cur = subErrors.get(i);
            if (i>0) {
                ret +="$";
            }
            ret+=cur.toString();
        }
        return ret;
    }

    public static EntityValidationError createFromString(String val) {
        EntityValidationError ret = new EntityValidationError();
        String[] parts = val.split("\\$");
        for(String s: parts) {
            ret.getSubErrors().add(EntityValidationSubError.createFromString(s));
        }
        
        return ret;
    }

    
    
}
