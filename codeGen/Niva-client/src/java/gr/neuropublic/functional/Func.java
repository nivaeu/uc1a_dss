/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.functional;

/**
 *
 * @author gmamais
 */
public interface Func<TRes> {
    public TRes lambda();
}
