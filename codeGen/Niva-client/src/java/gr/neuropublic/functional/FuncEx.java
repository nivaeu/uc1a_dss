/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.functional;

/**
 *
 * @author g_mamais
 */
public interface FuncEx<TRes> {
    public TRes lambda() throws Exception;
}

