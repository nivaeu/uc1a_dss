/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.functional;

/**
 *
 * @author gmamais
 */
public interface Func1<T1, TRes> {
    public TRes lambda(T1 x1);
}
