/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.functional;

/**
 *
 * @author gmamais
 */
public interface Func2<T1, T2, TRes> {
    public TRes lambda(T1 x1, T2 x2);
}
