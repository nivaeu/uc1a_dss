/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.functional;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author gmamais
 */
public class FList<T> {
    private ArrayList<T> _data = new ArrayList<T>();
    
    
    public static <T> FList<T> create(List<T> g) 
    {
        return new FList<T>(g);
    }
    
    public FList(List<T> data) {
        _data.addAll(data);
    }
    
    public List<T> getData() { return _data;}
    
    public <K> FList<K> map(Func1<T,K> f) {
        ArrayList<K> ret = new ArrayList<K>();
        for(T item : _data) {
            ret.add( f.lambda(item) );
        }
        return new FList<K>(ret);
    }
    
    public FList<T> filter(Func1<T,Boolean> f) {
        ArrayList<T> ret = new ArrayList<T>();
        for(T item : _data) {
            if (f.lambda(item))
                ret.add(item);
        }
        return new FList<T>(ret);
    }
    
    public <K> K fold(Func2<K,T,K> f, K initialState) {
        K ret = initialState;

        for(T item : _data) {
            ret = f.lambda(ret, item);
        }
        
        return ret;
    }
    

    public String join(String joinStr) {
        StringBuilder sb = new StringBuilder();
        if (_data.isEmpty())
            return "";
        sb.append(_data.get(0).toString());
        int length = _data.size();
        for(int i=1; i<length;i++) {
            sb.append(joinStr);
            sb.append(_data.get(i).toString());
        }
        return sb.toString();
    }
    
    
}
