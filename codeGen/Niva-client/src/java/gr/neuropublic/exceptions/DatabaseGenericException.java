
package gr.neuropublic.exceptions;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class DatabaseGenericException extends RuntimeException{
    
    public DatabaseGenericException(){
        super();
    }
    
    public DatabaseGenericException(String msg){
        super(msg);
    }
}
