package gr.neuropublic.exceptions;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class ValidationException extends RuntimeException{
    
    public ValidationException(){
        super();
    }
    
    public ValidationException(String msg){
        super(msg);
    }
}
