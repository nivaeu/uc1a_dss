package gr.neuropublic.utils;

/**
 * 
 * @author v_asloglou
 * @param <A>
 * @param <B>
 * @param <C>
 * @param <D>
 * @param <E>
 * @param <F> 
 */
public class SixTuple<A, B, C, D, E, F>
        extends FiveTuple<A, B, C, D, E>
{

    private final F sixth;

    /**
     * Creates a new instance of SixTuple
     */
    public SixTuple(A first, B second, C third, D fourth, E fifth, F sixth)
    {
        super(first, second, third, fourth, fifth);
        this.sixth = sixth;
    }

    public F getSixth()
    {
        return this.sixth;
    }

    protected boolean equalSixth(SixTuple other)
    {
        return equalObjects(this.getSixth(), other.getSixth());
    }

    @Override
    public int hashCode()
    {
        return 37 * super.hashCode() + (this.getSixth() == null ? 0 : this.getSixth().hashCode());
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof SixTuple)) {
            return false;
        }
        SixTuple other = (SixTuple) o;
        return equalFirst(other) && equalSecond(other) && equalThird(other) && equalFourth(other) && equalFifth(other) && equalSixth(other);
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder("{");
        sb.append(this.getFirst() == null ? "null" : this.getFirst().toString());
        sb.append(", ");
        sb.append(this.getSecond() == null ? "null" : this.getSecond().toString());
        sb.append(", ");
        sb.append(this.getThird() == null ? "null" : this.getThird().toString());
        sb.append(", ");
        sb.append(this.getFourth() == null ? "null" : this.getFourth().toString());
        sb.append(", ");
        sb.append(this.getFifth() == null ? "null" : this.getFifth().toString());
        sb.append(", ");
        sb.append(this.getSixth() == null ? "null" : this.getSixth().toString());
        sb.append("}");
        return sb.toString();
    }
}
