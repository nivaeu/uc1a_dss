package gr.neuropublic.utils;

/**
 * 
 * @author v_asloglou
 * @param <A>
 * @param <B>
 * @param <C> 
 */
public class ThreeTuple<A, B, C> extends TwoTuple<A, B>
{

    private final C third;

    public ThreeTuple(A first, B second, C third)
    {
        super(first, second);
        this.third = third;
    }

    public C getThird()
    {
        return this.third;
    }

    protected boolean equalThird(ThreeTuple other)
    {
        return equalObjects(this.getThird(), other.getThird());
    }

    @Override
    public int hashCode()
    {
        return 37 * super.hashCode() + (this.getThird() == null ? 0 : this.getThird().hashCode());
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof ThreeTuple)) {
            return false;
        }
        ThreeTuple other = (ThreeTuple) o;
        return equalFirst(other) && equalSecond(other) && equalThird(other);
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder("{");
        sb.append(this.getFirst() == null ? "null" : this.getFirst().toString());
        sb.append(", ");
        sb.append(this.getSecond() == null ? "null" : this.getSecond().toString());
        sb.append(", ");
        sb.append(this.getThird() == null ? "null" : this.getThird().toString());
        sb.append("}");
        return sb.toString();
    }
}
