package gr.neuropublic.utils;

/**
 * 
 * @author v_asloglou
 * @param <A>
 * @param <B>
 * @param <C>
 * @param <D> 
 */
public class FourTuple<A, B, C, D> extends ThreeTuple<A, B, C>
{

    private final D fourth;

    /**
     * Creates a new instance of FourTuple
     */
    public FourTuple(A first, B second, C third, D fourth)
    {
        super(first, second, third);
        this.fourth = fourth;
    }

    public D getFourth()
    {
        return this.fourth;
    }

    protected boolean equalFourth(FourTuple other)
    {
        return equalObjects(this.getFourth(), other.getFourth());
    }

    @Override
    public int hashCode()
    {
        return 37 * super.hashCode() + (this.getFourth() == null ? 0 : this.getFourth().hashCode());
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof FourTuple)) {
            return false;
        }
        FourTuple other = (FourTuple) o;
        return equalFirst(other) && equalSecond(other) && equalThird(other) && equalFourth(other);
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder("{");
        sb.append(this.getFirst() == null ? "null" : this.getFirst().toString());
        sb.append(", ");
        sb.append(this.getSecond() == null ? "null" : this.getSecond().toString());
        sb.append(", ");
        sb.append(this.getThird() == null ? "null" : this.getThird().toString());
        sb.append(", ");
        sb.append(this.getFourth() == null ? "null" : this.getFourth().toString());
        sb.append("}");
        return sb.toString();
    }
}
