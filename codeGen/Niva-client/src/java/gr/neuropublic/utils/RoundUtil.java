package gr.neuropublic.utils;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * Contains utility functions for rounding
 */
public class RoundUtil
{

    /**
     * rounds a double by a specified precision<BR> Examples:<UL> <LI>round(1234.5678, 2) = 1234.57 <LI>round(1234.5678, 0) = 1234 <LI>round(1234.5678, -2) =
     * 1200</UL>
     *
     * @param d the double to be rounded
     * @param p round precision (negative integers are allowed)
     * @return the double rounded by the specified precision
     */
    public static double round(double d, int p)
    {
        // see the Javadoc about why we use a String in the constructor
        // http://java.sun.com/j2se/1.5.0/docs/api/java/math/BigDecimal.html#BigDecimal(double)
        BigDecimal bd = new BigDecimal(Double.toString(d));
        bd = bd.setScale(p, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();
    }

    /**
     * @return if d==null, returns null, else same as {@link round(double, int)}
     */
    public static Double round(Double d, int p)
    {
        if (d == null) {
            return null;
        } else {
            return new Double(round(d.doubleValue(), p));
        }
    }

    /**
     * Rounds an amount for a specified currency<BR> The round precision is specified by the default fraction digits of the currency
     *
     * @param d the amount to be rounded
     * @param currencyCode the ISO 4217 code of the currency
     * @return if currencyCode is invalid or null then returns the whole amount, else returns the rounded amount
     */
    public static double round(double d, String currencyCode)
    {
        if (currencyCode == null) {
            return d;
        }
        try {
            Currency currency = Currency.getInstance(currencyCode);
            return round(d, currency.getDefaultFractionDigits());
        } catch (IllegalArgumentException ex) {
            return d;
        }
    }

    /**
     * @return if d==null, returns null, else same as {@link round(double, String)}
     */
    public static Double round(Double d, String currencyCode)
    {
        if (d == null) {
            return null;
        } else {
            return new Double(round(d.doubleValue(), currencyCode));
        }
    }
}
