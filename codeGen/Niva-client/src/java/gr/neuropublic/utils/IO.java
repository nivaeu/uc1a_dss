/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.utils;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKBReader;
import com.vividsolutions.jts.io.WKBWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 * @author g_mamais
 */
public class IO {
    public static void writeByteArray(ObjectOutputStream s, byte[] data) throws IOException {
        s.writeInt(data.length);
        s.write(data);
    }
    
    public static byte[] readByteArray(ObjectInputStream s) throws IOException {
        int len = s.readInt();
        byte[] ret = new byte[len];
        s.readFully(ret);
        return ret;
    }
    
    public static void writeBigDecimal(ObjectOutputStream s, BigDecimal b) throws IOException {
        int scale = b.scale();
        byte[] data = b.unscaledValue().toByteArray();
        s.writeInt(scale);
        writeByteArray(s, data);
    }
    
    public static BigDecimal readBigDecimal(ObjectInputStream s) throws IOException {
        int scale = s.readInt();
        byte[] data = readByteArray(s);
        return new BigDecimal(new BigInteger(data), scale);
    }

    public static void writeString(ObjectOutputStream s, String b) throws IOException {
        byte[] data = b.getBytes();
        writeByteArray(s, data);
    }
    
    public static String readString(ObjectInputStream s) throws IOException {
        byte[] data = readByteArray(s);
        return new String(data);
    }

    public static void writeGeometry(ObjectOutputStream s, Geometry b) throws IOException {
        WKBWriter wkb = new WKBWriter(2, true);
        byte[] data = wkb.write(b);
        writeByteArray(s, data);
    }
    
    public static Geometry readGeometry(ObjectInputStream s) throws IOException, ParseException {
        byte[] data = readByteArray(s);
        WKBReader wkb = new WKBReader();
        return wkb.read(data);
    }

}
