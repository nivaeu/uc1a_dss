/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.utils;

import gr.neuropublic.validators.EntityValidationSubError;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility Class for Ejb modules
 */
public class EjbUtil {

    /**
     * Create an EntityValidationSubError from args
     *
     * @param keyName
     * @param args
     * @return
     */
    public static EntityValidationSubError createEntityValidationSubError(String keyName, Object... args) {

        EntityValidationSubError error = new EntityValidationSubError(keyName);

        for (Object arg : args) {

            if (arg instanceof java.util.Date) {
                
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                String dateAsString = sdf.format(arg);
                
                error.getArgs().add(dateAsString);
            }
            else if(arg != null) {
                error.getArgs().add(arg.toString());
            }
        }

        return error;
    }
    
    
    
    public static int calculateCheckDigitRI3(String vl) {
        char[] digits = vl.toCharArray();
        int sum = 0;
        int curFactor=vl.length()+1;
        for(int i=0; i<digits.length;i++) {
            sum += curFactor*(digits[i]-'0');
            curFactor--;
        }
        int mod = (sum % 11);
        if (mod==0)
            return 1;
        if (mod==1)
            return 0;
        return 11-mod;
    }
    
    public static String calculateDiasPaymentCode(String customerId, int beneficialCode, int paymentMethod, BigDecimal amount) {
        String custId=customerId;
        String benCode;
        String payMethod = "" + paymentMethod;
        String amountStr;
        String res;
        //benefical custom field (used to identify the customer), 15 digits
        
        if (customerId == null || customerId.length()>15)
            throw new RuntimeException("Άκυρος Κωδικός Οφειλέτη");
        for(char c:customerId.toCharArray())
            if (!(c>='0' && c<='9'))
            throw new RuntimeException("Άκυρος Κωδικός Οφειλέτη");
        
        
        
        int custIdLen = customerId.length();
        if (custIdLen<15) {
            char[] chars = new char[15-custIdLen];
            Arrays.fill(chars, '0');
            custId = new String(chars) + customerId;            
        }
        
        NumberFormat frm = NumberFormat.getInstance();
        frm.setMinimumFractionDigits(0);
        frm.setMaximumFractionDigits(0);
        frm.setGroupingUsed(false);
        
        //benefical code part, 3 digits
        if (beneficialCode<0 || beneficialCode>999)
            throw new RuntimeException("Άκυρος Κωδικός Δικαιούχου Οργανισμού");
            
        
        frm.setMinimumIntegerDigits(3);
        frm.setMaximumIntegerDigits(3);
        benCode = frm.format(beneficialCode);
        
        
        //Payment method, 1 digit
        if (!(paymentMethod==0 || paymentMethod==2))
            throw new RuntimeException("Άκυρος Τρόπος πληρωμής");

        //Ammount
        if (amount.compareTo(BigDecimal.ZERO)==-1)
            throw new RuntimeException("Άκυρος ποσό πληρωμής");
        if (amount.compareTo(new BigDecimal("999999999"))==1)
            throw new RuntimeException("Άκυρος ποσό πληρωμής");
            
        frm.setMinimumIntegerDigits(11);
        frm.setMaximumIntegerDigits(11);
        BigDecimal a = amount;
        a = a.multiply(new BigDecimal("100"));
        amountStr = frm.format(a.doubleValue());

        res = custId+benCode+payMethod+amountStr;
        
        return res+calculateCheckDigitRI3(res);
    }
    
    public static String excludeParam(String statement, String prm) {
        String pattern = "^(.*?)\\([^\\)]*?" + prm + "\\b.*?\\)(.*)$";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(statement);
        String res = "";
        String rest = "";
        while (m.find()){
            res = res + m.group(1) + "(5=5)";
            rest = m.group(2);
            m = r.matcher(rest);
        }
        res = res + rest;
        return !"".equals(res) ? res : statement;
    }

    public static String addExcludedIds(String jpql, List<String> excludedEntities) {
        if (excludedEntities != null && !excludedEntities.isEmpty()) {
            String exludedIds = "";
            for(String __id:excludedEntities) {
                exludedIds += "," + __id;
            }
            exludedIds = "(1=1) AND (x.id not in (" + exludedIds.substring(1) + "))";
            jpql = jpql.replace("(1=1)", exludedIds);
        }
        return jpql;
    }
    
}
