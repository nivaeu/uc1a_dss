package gr.neuropublic.utils;

/**
 * 
 * @author v_asloglou
 * @param <A>
 * @param <B>
 * @param <C>
 * @param <D>
 * @param <E> 
 */
public class FiveTuple<A, B, C, D, E>
        extends FourTuple<A, B, C, D>
{

    private final E fifth;

    /**
     * Creates a new instance of FiveTuple
     */
    public FiveTuple(A first, B second, C third, D fourth, E fifth)
    {
        super(first, second, third, fourth);
        this.fifth = fifth;
    }

    public E getFifth()
    {
        return this.fifth;
    }

    protected boolean equalFifth(FiveTuple other)
    {
        return equalObjects(this.getFifth(), other.getFifth());
    }

    @Override
    public int hashCode()
    {
        return 37 * super.hashCode() + (this.getFifth() == null ? 0 : this.getFifth().hashCode());
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof FiveTuple)) {
            return false;
        }
        FiveTuple other = (FiveTuple) o;
        return equalFirst(other) && equalSecond(other) && equalThird(other) && equalFourth(other) && equalFifth(other);
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder("{");
        sb.append(this.getFirst() == null ? "null" : this.getFirst().toString());
        sb.append(", ");
        sb.append(this.getSecond() == null ? "null" : this.getSecond().toString());
        sb.append(", ");
        sb.append(this.getThird() == null ? "null" : this.getThird().toString());
        sb.append(", ");
        sb.append(this.getFourth() == null ? "null" : this.getFourth().toString());
        sb.append(", ");
        sb.append(this.getFifth() == null ? "null" : this.getFifth().toString());
        sb.append("}");
        return sb.toString();
    }
}
