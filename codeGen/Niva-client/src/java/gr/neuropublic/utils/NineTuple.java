package gr.neuropublic.utils;

/**
 * 
 * @author v_asloglou
 * @param <A>
 * @param <B>
 * @param <C>
 * @param <D>
 * @param <E>
 * @param <F>
 * @param <G>
 * @param <H>
 * @param <I> 
 */
public class NineTuple<A, B, C, D, E, F, G, H, I>
        extends EightTuple<A, B, C, D, E, F, G, H>
{

    private final I nineth;

    /**
     * Creates a new instance of NineTuple
     */
    public NineTuple(A first, B second, C third, D fourth, E fifth, F sixth, G seventh, H eighth, I nineth)
    {
        super(first, second, third, fourth, fifth, sixth, seventh, eighth);
        this.nineth = nineth;
    }

    public I getNineth()
    {
        return this.nineth;
    }

    protected boolean equalNineth(NineTuple other)
    {
        return equalObjects(this.getNineth(), other.getNineth());
    }

    @Override
    public int hashCode()
    {
        return 37 * super.hashCode() + (this.getNineth() == null ? 0 : this.getNineth().hashCode());
    }

    @Override
    public boolean equals(Object o)
    {
        if (!(o instanceof NineTuple)) {
            return false;
        }
        NineTuple other = (NineTuple) o;
        return equalFirst(other) && equalSecond(other) && equalThird(other) && equalFourth(other) && equalFifth(other) && equalSixth(other) && equalSeventh(other) && equalEighth(other) && equalNineth(other);
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder("{");
        sb.append(this.getFirst() == null ? "null" : this.getFirst().toString());
        sb.append(", ");
        sb.append(this.getSecond() == null ? "null" : this.getSecond().toString());
        sb.append(", ");
        sb.append(this.getThird() == null ? "null" : this.getThird().toString());
        sb.append(", ");
        sb.append(this.getFourth() == null ? "null" : this.getFourth().toString());
        sb.append(", ");
        sb.append(this.getFifth() == null ? "null" : this.getFifth().toString());
        sb.append(", ");
        sb.append(this.getSixth() == null ? "null" : this.getSixth().toString());
        sb.append(", ");
        sb.append(this.getSeventh() == null ? "null" : this.getSeventh().toString());
        sb.append(", ");
        sb.append(this.getEighth() == null ? "null" : this.getEighth().toString());
        sb.append(", ");
        sb.append(this.getNineth() == null ? "null" : this.getNineth().toString());
        sb.append("}");
        return sb.toString();
    }
}
