/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.base;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.PrecisionModel;
import com.vividsolutions.jts.geom.TopologyException;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import gr.neuropublic.exceptions.GenericApplicationException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.spatial.dialect.oracle.SDOGeometryValueExtractor;
import org.hibernate.spatial.dialect.postgis.PGGeometryValueExtractor;

/**
 *
 * @author g_mamais
 */
public class GeometryHepler {
    public static Geometry GetOraGeometry(ResultSet rs, String colName, int srid) throws SQLException {
        Object geom = rs.getObject(colName);
        if (geom == null)
            return null;

        com.vividsolutions.jts.geom.Geometry jtsGeom;

        SDOGeometryValueExtractor ve = new SDOGeometryValueExtractor();
        jtsGeom = ve.toJTS(geom);

        if (jtsGeom != null) {
            jtsGeom.setSRID(srid);
        }

        return jtsGeom;
    }
    
    public static Geometry GetPgGeometry(ResultSet rs, String colName, int srid) throws SQLException {
        Object geom = rs.getObject(colName);
        if (geom == null)
            return null;

        com.vividsolutions.jts.geom.Geometry jtsGeom;
        PGGeometryValueExtractor ve = new PGGeometryValueExtractor();
        jtsGeom = ve.toJTS(geom);


        if (jtsGeom != null) {
            jtsGeom.setSRID(srid);
        }

        return jtsGeom;
    }
	
	
    public static ArrayList<Polygon> geometryToPolygonsArray(Geometry g) {
        ArrayList<Polygon> polygons = new ArrayList<>();
        if ("Polygon".equals(g.getGeometryType())) {
            polygons.add((Polygon)g);
        } else if ("MultiPolygon".equals(g.getGeometryType())) {
            MultiPolygon mp = (MultiPolygon) g;
            for(int j=0; j< mp.getNumGeometries(); j++) {
                polygons.add((Polygon)mp.getGeometryN(j));
            }
        } else if ("GeometryCollection".equals(g.getGeometryType())) {
            GeometryCollection geoCol = (GeometryCollection)g;
            int len = geoCol.getNumGeometries();
            for(int i=0; i<len; i++) {
                Geometry gi = geoCol.getGeometryN(i);
                if (gi.getGeometryType().equals("Polygon")) {
                    polygons.add((Polygon)gi);
                } else if (gi.getGeometryType().equals("MultiPolygon")) {
                    MultiPolygon mp = (MultiPolygon) gi;
                    for(int j=0; j< mp.getNumGeometries(); j++) {
                        polygons.add((Polygon)mp.getGeometryN(j));
                    }
                } 
            }
            
        }
            
        return polygons;
    }
    
    public static Geometry FastUnion(List<Geometry> geoms){
        if (geoms == null || geoms.isEmpty()) {
            throw new GenericApplicationException("Empty or null collection");
        }
        PrecisionModel precisionModel = geoms.get(0).getPrecisionModel();
        int srid = geoms.get(0).getSRID();
        ArrayList<Polygon> polygons = new ArrayList<>();
        for(Geometry g : geoms) {
            polygons.addAll(geometryToPolygonsArray(g));
        }
        
        Geometry[] polys = polygons.toArray(new Polygon[0]);
        
        GeometryFactory geometryFactory = new GeometryFactory(precisionModel, srid);
        GeometryCollection polygonCollection = geometryFactory.createGeometryCollection(polys);
        Geometry union = polygonCollection.buffer(0);        
        return union;
    }
    
    
    public static Geometry removeGeometryCollection(Geometry jtsGeom) {
        if (jtsGeom != null && "GeometryCollection".equals(jtsGeom.getGeometryType())) {
            GeometryCollection geoCol1 = (GeometryCollection)jtsGeom;
            return GeomCollectionToGeometry(geoCol1);
        }
        return jtsGeom;
    }    
    /*
    Converts a GeometryCollection to Polygon to multipolygon.
    Any line elements within the geometry collection are ignored.
    */
    public static Geometry GeomCollectionToGeometry(GeometryCollection geoCol) {
        ArrayList<Geometry> polygons = new ArrayList<>();
        int len = geoCol.getNumGeometries();
        for(int i=0; i<len; i++) {
            Geometry g = geoCol.getGeometryN(i);
            if (g.getGeometryType().equals("Polygon")) {
                polygons.add(g);
            } else if (g.getGeometryType().equals("MultiPolygon")) {
                //System.err.println("MultiPolygon added");
                MultiPolygon mp = (MultiPolygon) g;
                for(int j=0; j< mp.getNumGeometries(); j++) {
                    polygons.add(mp.getGeometryN(j));
                }
            } 
        }
        if (polygons.isEmpty()) {
            GeometryFactory geometryFactory = new GeometryFactory(geoCol.getPrecisionModel(), geoCol.getSRID());
            return geometryFactory.createPolygon((LinearRing)null);
        } else if (polygons.size() == 1) {
            return polygons.get(0);
        } else {
            GeometryFactory geometryFactory = new GeometryFactory(geoCol.getPrecisionModel(), geoCol.getSRID());
            Polygon[] polys = polygons.toArray(new Polygon[0]);
            return  geometryFactory.createMultiPolygon(polys);

            /*
            GeometryCollection polygonCollection = geometryFactory.createGeometryCollection(polys);
            
            Geometry union = polygonCollection.buffer(0);        
            return union;
            */
        }
    }
    
    public static Geometry difference0(Geometry g1, Geometry g2) {
        try {
            return g1.difference(g2);
        } catch (TopologyException ex) {
            try {
                return g1.buffer(0.01).difference(g2.buffer(0.01));
            } catch (TopologyException ex2) {
                GeometryFactory geometryFactory = new GeometryFactory(g1.getPrecisionModel(), g1.getSRID());
                return geometryFactory.createPolygon((LinearRing)null);
            }
        }
    }
    
    public static Geometry difference(Geometry g1, Geometry g2) {
        if (g1 == null)
            return null;
        if (g2 == null)
            return g1;
        if (!"GeometryCollection".equals(g1.getGeometryType()) &&  !"GeometryCollection".equals(g2.getGeometryType())) {                     
            return difference0(g1,g2);
        } else if (!"GeometryCollection".equals(g1.getGeometryType()) &&  "GeometryCollection".equals(g2.getGeometryType())) {
            GeometryCollection geoCol = (GeometryCollection)g2;
            //Polygon[] polygons = new Polygon[geoCol.getNumGeometries()];
            return difference0(g1, GeomCollectionToGeometry(geoCol)); //g1.difference(GeomCollectionToGeometry(geoCol));
        } else if ("GeometryCollection".equals(g1.getGeometryType()) &&  !"GeometryCollection".equals(g2.getGeometryType())) {
            GeometryCollection geoCol = (GeometryCollection)g1;
            return difference0(GeomCollectionToGeometry(geoCol),g2); //GeomCollectionToGeometry(geoCol).difference(g2);
        } else {
            GeometryCollection geoCol1 = (GeometryCollection)g1;
            GeometryCollection geoCol2 = (GeometryCollection)g2;
            return difference0(GeomCollectionToGeometry(geoCol1), GeomCollectionToGeometry(geoCol2));//GeomCollectionToGeometry(geoCol1).difference(GeomCollectionToGeometry(geoCol2));
        }
    }
    
    
    public static Geometry intersection0(Geometry g1, Geometry g2) {
        try {
            return g1.intersection(g2);
        } catch (TopologyException ex) {
            try {
                return g1.buffer(0.01).intersection(g2.buffer(0.01));
            } catch (TopologyException ex2) {
                GeometryFactory geometryFactory = new GeometryFactory(g1.getPrecisionModel(), g1.getSRID());
                return geometryFactory.createPolygon((LinearRing)null);
            }
        }
    }
    
    public static Geometry intersection(Geometry g1, Geometry g2) {
        return removeGeometryCollection(intersection0(removeGeometryCollection(g1),removeGeometryCollection(g2)));
    }
	
}
