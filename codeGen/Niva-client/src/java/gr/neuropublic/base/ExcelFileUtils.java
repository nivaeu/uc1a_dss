/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.base;

import gr.neuropublic.mutil.base.Pair;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.util.CellRangeAddress;

/**
 *
 * @author st_krommydas
 */
public class ExcelFileUtils {

    public static Pair<Boolean,Integer> validateAndGetCellValue_Integer(Cell cell, Integer minValue, Integer maxValue, Set<Integer> allowedValues){
        int cellType = cell.getCellType();
        
        if (cellType == Cell.CELL_TYPE_BLANK) {
            return new Pair<>(true,null);
        }
        if (cellType == Cell.CELL_TYPE_NUMERIC && !org.apache.poi.ss.usermodel.DateUtil.isCellDateFormatted(cell)){
            BigDecimal bdValue = BigDecimal.valueOf(cell.getNumericCellValue());
            if (bdValue.remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO) != 0) 
                return new Pair<>(false,null);
            BigInteger biValue = bdValue.toBigInteger();
            if (biValue.compareTo(new BigInteger(String.valueOf(Integer.MAX_VALUE))) > 0)
                return new Pair<>(false,null);
            if (biValue.compareTo(new BigInteger(String.valueOf(Integer.MIN_VALUE))) < 0)
                return new Pair<>(false,null);
            if (maxValue != null && biValue.compareTo(new BigInteger(maxValue.toString())) > 0)
                return new Pair<>(false,null);
            if (minValue != null && biValue.compareTo(new BigInteger(minValue.toString())) < 0)
                return new Pair<>(false,null);
            
            Integer ret = biValue.intValue();

            // check allowed values
            if (allowedValues != null && !allowedValues.isEmpty()) {
                if (!allowedValues.contains(ret))
                    return new Pair<>(false,null);
            }
            
            return new Pair<>(true, ret);
        } else {
            return new Pair<>(false,null);
        }
    }
    
    public static Pair<Boolean,Short> validateAndGetCellValue_Short(Cell cell, Short minValue, Short maxValue, Set<Short> allowedValues){
        int cellType = cell.getCellType();
        
        if (cellType == Cell.CELL_TYPE_BLANK) {
            return new Pair<>(true,null);
        }
        if (cellType == Cell.CELL_TYPE_NUMERIC && !org.apache.poi.ss.usermodel.DateUtil.isCellDateFormatted(cell)){
            BigDecimal bdValue = BigDecimal.valueOf(cell.getNumericCellValue());
            if (bdValue.remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO) != 0) 
                return new Pair<>(false,null);
            BigInteger biValue = bdValue.toBigInteger();
            if (biValue.compareTo(new BigInteger(String.valueOf(Short.MAX_VALUE))) > 0)
                return new Pair<>(false,null);
            if (biValue.compareTo(new BigInteger(String.valueOf(Short.MIN_VALUE))) < 0)
                return new Pair<>(false,null);
            if (maxValue != null && biValue.compareTo(new BigInteger(maxValue.toString())) > 0)
                return new Pair<>(false,null);
            if (minValue != null && biValue.compareTo(new BigInteger(minValue.toString())) < 0)
                return new Pair<>(false,null);
            
            Short ret = biValue.shortValue();

            // check allowed values
            if (allowedValues != null && !allowedValues.isEmpty()) {
                if (!allowedValues.contains(ret))
                    return new Pair<>(false,null);
            }
            
            return new Pair<>(true, ret);
        } else {
            return new Pair<>(false,null);
        }
    }
    
    public static Pair<Boolean,Long> validateAndGetCellValue_Long(Cell cell, Long minValue, Long maxValue, Set<Long> allowedValues){
        int cellType = cell.getCellType();
        
        if (cellType == Cell.CELL_TYPE_BLANK) {
            return new Pair<>(true,null);
        }
        if (cellType == Cell.CELL_TYPE_NUMERIC && !org.apache.poi.ss.usermodel.DateUtil.isCellDateFormatted(cell)){
            BigDecimal bdValue = BigDecimal.valueOf(cell.getNumericCellValue());
            if (bdValue.remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO) != 0) 
                return new Pair<>(false,null);
            BigInteger biValue = bdValue.toBigInteger();
            if (biValue.compareTo(BigInteger.valueOf(Long.MAX_VALUE)) > 0)
                return new Pair<>(false,null);
            if (biValue.compareTo(BigInteger.valueOf(Long.MIN_VALUE)) < 0)
                return new Pair<>(false,null);
            if (maxValue != null && biValue.compareTo(BigInteger.valueOf(maxValue)) > 0)
                return new Pair<>(false,null);
            if (minValue != null && biValue.compareTo(BigInteger.valueOf(minValue)) < 0)
                return new Pair<>(false,null);
            
            Long ret = biValue.longValue();

            // check allowed values
            if (allowedValues != null && !allowedValues.isEmpty()) {
                if (!allowedValues.contains(ret))
                    return new Pair<>(false,null);
            }
            
            return new Pair<>(true, ret);
        } else {
            return new Pair<>(false,null);
        }
    }
    
    public static Pair<Boolean,BigInteger> validateAndGetCellValue_BigInteger(Cell cell, BigInteger minValue, BigInteger maxValue, Set<BigInteger> allowedValues){
        int cellType = cell.getCellType();
        
        if (cellType == Cell.CELL_TYPE_BLANK) {
            return new Pair<>(true,null);
        }
        if (cellType == Cell.CELL_TYPE_NUMERIC && !org.apache.poi.ss.usermodel.DateUtil.isCellDateFormatted(cell)){
            BigDecimal bdValue = BigDecimal.valueOf(cell.getNumericCellValue());
            if (bdValue.remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO) != 0) 
                return new Pair<>(false,null);
            BigInteger ret = bdValue.toBigInteger();
            if (maxValue != null && ret.compareTo(maxValue) > 0)
                return new Pair<>(false,null);
            if (minValue != null && ret.compareTo(minValue) < 0)
                return new Pair<>(false,null);
            
            // check allowed values
            if (allowedValues != null && !allowedValues.isEmpty()) {
                if (!allowedValues.contains(ret))
                    return new Pair<>(false,null);
            }
            
            return new Pair<>(true, ret);
        } else {
            return new Pair<>(false,null);
        }
    }
    
    public static Pair<Boolean,BigDecimal> validateAndGetCellValue_BigDecimal(Cell cell, Integer decimals, BigDecimal minValue, BigDecimal maxValue, Set<BigDecimal> allowedValues){
        int cellType = cell.getCellType();
        
        if (cellType == Cell.CELL_TYPE_BLANK) {
            return new Pair<>(true,null);
        }
        if (cellType == Cell.CELL_TYPE_NUMERIC && !org.apache.poi.ss.usermodel.DateUtil.isCellDateFormatted(cell)){
            BigDecimal bdValue = BigDecimal.valueOf(cell.getNumericCellValue());
            if (decimals != null) {
                if (decimals == 0 && bdValue.remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO) != 0)
                    return new Pair<>(false,null);
                if (decimals > 0 && bdValue.scale() > decimals)
                    return new Pair<>(false,null);
            }
            if (maxValue != null && bdValue.compareTo(maxValue) > 0)
                return new Pair<>(false,null);
            if (minValue != null && bdValue.compareTo(minValue) < 0)
                return new Pair<>(false,null);
            
            // check allowed values
            if (allowedValues != null && !allowedValues.isEmpty()) {
                if (!allowedValues.contains(bdValue))
                    return new Pair<>(false,null);
            }
            
            return new Pair<>(true, bdValue);
        } else {
            return new Pair<>(false,null);
        }
    }

    public static Pair<Boolean,Float> validateAndGetCellValue_Float(Cell cell, Integer decimals, Float minValue, Float maxValue, Set<Float> allowedValues){
        int cellType = cell.getCellType();
        
        if (cellType == Cell.CELL_TYPE_BLANK) {
            return new Pair<>(true,null);
        }
        if (cellType == Cell.CELL_TYPE_NUMERIC && !org.apache.poi.ss.usermodel.DateUtil.isCellDateFormatted(cell)){
            BigDecimal bdValue = BigDecimal.valueOf(cell.getNumericCellValue());
            if (decimals != null) {
                if (decimals == 0 && bdValue.remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO) != 0)
                    return new Pair<>(false,null);
                if (decimals > 0 && bdValue.scale() > decimals)
                    return new Pair<>(false,null);
            }
            if (bdValue.compareTo(new BigDecimal(String.valueOf(Float.MAX_VALUE))) > 0)
                return new Pair<>(false,null);
            if (bdValue.compareTo(new BigDecimal(String.valueOf(Float.MIN_VALUE))) < 0)
                return new Pair<>(false,null);
            if (maxValue != null && bdValue.compareTo(new BigDecimal(maxValue.toString())) > 0)
                return new Pair<>(false,null);
            if (minValue != null && bdValue.compareTo(new BigDecimal(minValue.toString())) < 0)
                return new Pair<>(false,null);
            
            Float ret = bdValue.floatValue();
            // check allowed values
            if (allowedValues != null && !allowedValues.isEmpty()) {
                if (!allowedValues.contains(ret))
                    return new Pair<>(false,null);
            }
            
            return new Pair<>(true, ret);
        } else {
            return new Pair<>(false,null);
        }
    }

    public static Pair<Boolean,Double> validateAndGetCellValue_Double(Cell cell, Integer decimals, Double minValue, Double maxValue, Set<Double> allowedValues){
        int cellType = cell.getCellType();
        
        if (cellType == Cell.CELL_TYPE_BLANK) {
            return new Pair<>(true,null);
        }
        if (cellType == Cell.CELL_TYPE_NUMERIC && !org.apache.poi.ss.usermodel.DateUtil.isCellDateFormatted(cell)){
            BigDecimal bdValue = BigDecimal.valueOf(cell.getNumericCellValue());
            if (decimals != null) {
                if (decimals == 0 && bdValue.remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO) != 0)
                    return new Pair<>(false,null);
                if (decimals > 0 && bdValue.scale() > decimals)
                    return new Pair<>(false,null);
            }
            if (bdValue.compareTo(new BigDecimal(String.valueOf(Double.MAX_VALUE))) > 0)
                return new Pair<>(false,null);
            if (bdValue.compareTo(new BigDecimal(String.valueOf(Double.MIN_VALUE))) < 0)
                return new Pair<>(false,null);
            if (maxValue != null && bdValue.compareTo(new BigDecimal(maxValue.toString())) > 0)
                return new Pair<>(false,null);
            if (minValue != null && bdValue.compareTo(new BigDecimal(minValue.toString())) < 0)
                return new Pair<>(false,null);
            
            Double ret = bdValue.doubleValue();
            // check allowed values
            if (allowedValues != null && !allowedValues.isEmpty()) {
                if (!allowedValues.contains(ret))
                    return new Pair<>(false,null);
            }
            
            return new Pair<>(true, ret);
        } else {
            return new Pair<>(false,null);
        }
    }
    
    public static Pair<Boolean,Date> validateAndGetCellValue_Date(Cell cell, Date minValue, Date maxValue, Set<Date> allowedValues) {
        int cellType = cell.getCellType();

        if (cellType == Cell.CELL_TYPE_BLANK) {
                return new Pair<>(true,null);
        }
        if (cellType == Cell.CELL_TYPE_NUMERIC && org.apache.poi.ss.usermodel.DateUtil.isCellDateFormatted(cell)){
            Date date = cell.getDateCellValue();
            if (maxValue != null && date.compareTo(maxValue) > 0)
                return new Pair<>(false,null);
            if (minValue != null && date.compareTo(minValue) < 0)
                return new Pair<>(false,null);
            
            // check allowed values
            if (allowedValues != null && !allowedValues.isEmpty()) {
                if (!allowedValues.contains(date))
                    return new Pair<>(false,null);
            }

            return new Pair<>(true,date);
        } else {
            return new Pair<>(false,null);
        }
    }
    
    public static Pair<Boolean,String> validateAndGetCellValue_String(Cell cell, Integer maxLength, Set<String> allowedValues) {
        int cellType = cell.getCellType();

        if (cellType == Cell.CELL_TYPE_BLANK) {
                return new Pair<>(true,null);
        }
        if (cellType == Cell.CELL_TYPE_STRING) {
            String value = cell.getStringCellValue();
            if (maxLength != null && (new Integer(value.length())).compareTo(maxLength) > 0)
                return new Pair<>(false,null);
            
            // check allowed values
            if (allowedValues != null && !allowedValues.isEmpty()) {
                if (!allowedValues.contains(value))
                    return new Pair<>(false,null);
            }

            return new Pair<>(true,value);
        } else {
            return new Pair<>(false,null);
        }
    }

    public enum ExcelLanguage{el,en};
    public static class ExcelWorkBook {
        private static HSSFWorkbook workbook;
        private static HSSFCellStyle cellStyleBoardered;
        private static HSSFCellStyle cellStyleBoarderedHeader;
        private static HSSFCellStyle cellStyleString;
        private final ExcelLanguage activeLanguage;
        
        public ExcelWorkBook(){
            this("el");
        }
        public ExcelWorkBook(String activeLanguageCode){
            workbook = new HSSFWorkbook();
            
            switch(activeLanguageCode) {
                case "el": 
                    this.activeLanguage = ExcelLanguage.el;
                    break;
                case "en":
                    this.activeLanguage = ExcelLanguage.en;
                    break;
                default:
                    this.activeLanguage = ExcelLanguage.el;
                    break;
            }
            
            
            cellStyleBoardered = workbook.createCellStyle();
            cellStyleBoardered.setFillPattern(HSSFCellStyle.NO_FILL);
            cellStyleBoardered.setBorderBottom(CellStyle.BORDER_THIN);
            cellStyleBoardered.setBorderLeft(CellStyle.BORDER_THIN);
            cellStyleBoardered.setBorderRight(CellStyle.BORDER_THIN);
            cellStyleBoardered.setBorderTop(CellStyle.BORDER_THIN);
            
            cellStyleBoarderedHeader = workbook.createCellStyle();
            cellStyleBoarderedHeader.setFillPattern(HSSFCellStyle.NO_FILL);
            cellStyleBoarderedHeader.setBorderBottom(CellStyle.BORDER_THIN);
            cellStyleBoarderedHeader.setBorderLeft(CellStyle.BORDER_THIN);
            cellStyleBoarderedHeader.setBorderRight(CellStyle.BORDER_THIN);
            cellStyleBoarderedHeader.setBorderTop(CellStyle.BORDER_THIN);
            cellStyleBoarderedHeader.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            cellStyleBoarderedHeader.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            
            cellStyleString = workbook.createCellStyle();
            DataFormat formatForString = workbook.createDataFormat();
            cellStyleString.setDataFormat(formatForString.getFormat("@"));
        }

        public String getActiveLangString(String key) {
            return ExcelWorkBook.getActiveLangString(key, this.activeLanguage);
        }

        private static String getActiveLangString(String key, ExcelLanguage activeLanguage) {
            if (key == null)
                return key;
            String ret;
            switch (activeLanguage) {
                case el:
                    ret = strings_el.get(key);
                    break;
                case en:
                    ret = strings_en.get(key);
                    break;
                default:
                    ret = strings_el.get(key);
            }
            if (ret == null)
                ret = key;
            return ret;
        }

        private static final HashMap<String,String> strings_el;
        static {
            strings_el = new HashMap<>();
            strings_el.put("YES", "ΝΑΙ");
            strings_el.put("NO", "ΟΧΙ");
            strings_el.put("NumberType", "Αριθμός");
            strings_el.put("NumberType_Integer", "Αριθμός (τύπου Integer)");
            strings_el.put("NumberType_BigInteger", "Αριθμός (τύπου BigInteger)");
            strings_el.put("NumberType_BigDecimal", "Αριθμός (τύπου BigDecimal)");
            strings_el.put("NumberType_Short", "Αριθμός (τύπου Short)");
            strings_el.put("NumberType_Long", "Αριθμός (τύπου Long)");
            strings_el.put("NumberType_Float", "Αριθμός (τύπου Float)");
            strings_el.put("NumberType_Double", "Αριθμός (τύπου Double)");
            strings_el.put("StringType", "Κείμενο");
            strings_el.put("DateType", "Ημερομηνία");
            strings_el.put("Template", "Πρότυπο");
            strings_el.put("Specifications", "Προδιαγραφές");
            strings_el.put("Instructions", "Οδηγίες");
            strings_el.put("Instructions_FirstRowRule", "Η πρώτη γραμμή πρέπει να περιλαμβάνει επικεφαλίδες.");
            strings_el.put("Instructions_DataRowNumRule", "Τα δεδομένα πρέπει να ξεκινούν από την γραμμή ");
            strings_el.put("Instructions_ColumnSpecifications", "Προδιαγραφές Στηλών");
            strings_el.put("Column","Στήλη");
            strings_el.put("Title","Τίτλος");
            strings_el.put("Required","Υποχρεωτικό");
            strings_el.put("Type","Τύπος");
            strings_el.put("NumberOfDecimals","Πλήθος Δεκαδικών");
            strings_el.put("MinimumValue","Ελάχιστη Τιμή");
            strings_el.put("MaximumValue","Μέγιστη Τιμή");
            strings_el.put("MaximumLength","Μέγιστο Μήκος");
            strings_el.put("ValidValues","Επιτρεπτές Τιμές");
            strings_el.put("Remarks","Παρατηρήσεις");
            strings_el.put("Today","Σήμερα");
        }

        private static final HashMap<String,String> strings_en;
        static {
            strings_en = new HashMap<>();
            strings_en.put("YES", "YES");
            strings_en.put("NO", "NO");
            strings_en.put("NumberType", "Number");
            strings_en.put("NumberType_Integer", "Number (of type Integer)");
            strings_en.put("NumberType_BigInteger", "Number (of type BigInteger)");
            strings_en.put("NumberType_BigDecimal", "Number (of type BigDecimal)");
            strings_en.put("NumberType_Short", "Number (of type Short)");
            strings_en.put("NumberType_Long", "Number (of type Long)");
            strings_en.put("NumberType_Float", "Number (of type Float)");
            strings_en.put("NumberType_Double", "Number (of type Double)");
            strings_en.put("StringType", "Text");
            strings_en.put("DateType", "Date");
            strings_en.put("Template", "Template");
            strings_en.put("Specifications", "Specifications");
            strings_en.put("Instructions", "Instructions");
            strings_en.put("Instructions_FirstRowRule", "The first row must contain headings.");
            strings_en.put("Instructions_DataRowNumRule", "The data must start from row ");
            strings_en.put("Instructions_ColumnSpecifications", "Column Specifications");
            strings_en.put("Column","Column");
            strings_en.put("Title","Title");
            strings_en.put("Required","Required");
            strings_en.put("Type","Type");
            strings_en.put("NumberOfDecimals","Number of Decimals");
            strings_en.put("MinimumValue","Minimum Value");
            strings_en.put("MaximumValue","Maximum Value");
            strings_en.put("MaximumLength","Maximum Length");
            strings_en.put("ValidValues","Valid Values");
            strings_en.put("Remarks","Remarks");
            strings_en.put("Today","Today");
        }        
        
        public static class ImportExcelSampleCell {
            public String typeDescription = "";
            public String col;
            public String label;
            public boolean required = false;
            public String[] allowedValues;
            public String remarks;
            public HSSFCellStyle cellStyle;
            
            public void createSpecCells(HSSFSheet worksheet, HSSFRow row, ExcelLanguage activeLanguage) {}
            public String getRequiredDescription(){
                return this.required ? "YES" : "NO";
            }
        }
        
        public static class ImportExcelSampleCellNumeric extends ImportExcelSampleCell {
            public Integer decimals;
            public String minVal;
            public String maxVal;
            
            public ImportExcelSampleCellNumeric(String col, String label, String javaType, boolean required, Integer decimals, String minVal, String maxVal, String[] allowedValues, String remarks){
                this.typeDescription = javaType != null ? "NumberType_" + javaType : "NumberType";
                this.col = col;
                this.label = label;
                this.required = required;
                this.decimals = decimals;
                this.minVal = minVal;
                this.maxVal = maxVal;
                this.allowedValues = allowedValues;
                this.remarks = remarks;
                cellStyle = workbook.createCellStyle();
                DataFormat format = workbook.createDataFormat();
                cellStyle.setDataFormat(format.getFormat(getNumberFormat(this.decimals)));
            }
            
            @Override
            public void createSpecCells(HSSFSheet worksheet, HSSFRow row, ExcelLanguage activeLanguage) {
                ExcelWorkBook.createSpecCells(worksheet, row, false, new String[]{
                    this.col, 
                    this.label, 
                    ExcelWorkBook.getActiveLangString(this.getRequiredDescription(), activeLanguage), 
                    ExcelWorkBook.getActiveLangString(this.typeDescription, activeLanguage), 
                    this.decimals != null ? String.valueOf(this.decimals) : "",
                    this.minVal,
                    this.maxVal,
                    "",
                    joinStringArray(this.allowedValues, ','),
                    this.remarks != null ? this.remarks : ""});
            }
        }
        
        public static class ImportExcelSampleCellString extends ImportExcelSampleCell {
            Integer maxLength;
            
            public ImportExcelSampleCellString(String col, String label, boolean required, Integer maxLength, String[] allowedValues, String remarks){
                this.typeDescription = "StringType";
                this.col = col;
                this.label = label;
                this.required = required;
                this.maxLength = maxLength;
                this.allowedValues = allowedValues;
                this.remarks = remarks;
                cellStyle = cellStyleString;
            }
            
            @Override
            public void createSpecCells(HSSFSheet worksheet, HSSFRow row, ExcelLanguage activeLanguage) {
                ExcelWorkBook.createSpecCells(worksheet, row, false, new String[]{
                    this.col, 
                    this.label, 
                    ExcelWorkBook.getActiveLangString(this.getRequiredDescription(), activeLanguage), 
                    ExcelWorkBook.getActiveLangString(this.typeDescription, activeLanguage), 
                    "",
                    "",
                    "",
                    this.maxLength != null? String.valueOf(this.maxLength) : "",
                    joinStringArray(this.allowedValues, ','),
                    this.remarks != null ? this.remarks : ""});
            }
        }
        
        public static class ImportExcelSampleCellDate extends ImportExcelSampleCell {
            public String dateFormat = "dd/mm/yyyy";
            public String minVal;
            public String maxVal;
            
            public ImportExcelSampleCellDate(String col, String label, String dateFormat, boolean required, String minVal, String maxVal, String[] allowedValues, String remarks){
                this.typeDescription = "DateType";
                this.col = col;
                this.label = label;
                this.required = required;
                this.minVal = minVal;
                this.maxVal = maxVal;
                this.allowedValues = allowedValues;
                if (dateFormat != null)
                    this.dateFormat = dateFormat;
                this.remarks = remarks;
                cellStyle = workbook.createCellStyle();
                DataFormat format = workbook.createDataFormat();
                cellStyle.setDataFormat(format.getFormat(this.dateFormat));
            }

            @Override
            public void createSpecCells(HSSFSheet worksheet, HSSFRow row, ExcelLanguage activeLanguage) {
                ExcelWorkBook.createSpecCells(worksheet, row, false, new String[]{
                    this.col, 
                    this.label, 
                    ExcelWorkBook.getActiveLangString(this.getRequiredDescription(), activeLanguage), 
                    ExcelWorkBook.getActiveLangString(this.typeDescription, activeLanguage), 
                    "",
                    this.minVal,
                    this.maxVal,
                    "",
                    joinStringArray(this.allowedValues, ','),
                    this.remarks != null ? this.remarks : ""});
            }            
        }
        
        public HSSFWorkbook createImportExcelSampleWorkBook(boolean hasHeaderRow, ImportExcelSampleCell[] cells) {
            if (cells == null || cells.length == 0) 
                return workbook;
            
            // Create Sample Sheet
            HSSFSheet worksheetSample = workbook.createSheet(getActiveLangString("Template"));
            HSSFRow headersRow = worksheetSample.createRow(0);
           
            HSSFCell cell;
            
            for (int cellIndex = 0; cellIndex < cells.length ; cellIndex++) {
                ImportExcelSampleCell sampleCell = cells[cellIndex];
                cell = headersRow.createCell(cellIndex);
                cell.setCellValue(sampleCell.label);
                cell.setCellStyle(cellStyleBoarderedHeader);
                worksheetSample.autoSizeColumn(cellIndex);
                worksheetSample.setDefaultColumnStyle(cellIndex, sampleCell.cellStyle);
            }
            
            // Create Specs Sheet
            HSSFSheet worksheetSpecs = workbook.createSheet(getActiveLangString("Specifications"));
            // add Instructions
            //1st row
            int row = 0;
            HSSFRow instrRow = worksheetSpecs.createRow(row);
            cell = instrRow.createCell(0);
            cell.setCellValue(getActiveLangString("Instructions") + ":");
            worksheetSpecs.addMergedRegion(new CellRangeAddress(0,0,0,4));
            //2nd row
            if (hasHeaderRow) {
                instrRow = worksheetSpecs.createRow(++row);
                cell = instrRow.createCell(0);
                cell.setCellValue(getActiveLangString("Instructions_FirstRowRule"));
            }
            worksheetSpecs.addMergedRegion(new CellRangeAddress(row,row,0,4));
            //3rd row
            instrRow = worksheetSpecs.createRow(++row);
            cell = instrRow.createCell(0);
            int startRowIndex = hasHeaderRow ? 2 : 1;
            cell.setCellValue(getActiveLangString("Instructions_DataRowNumRule") + startRowIndex + ".");
            worksheetSpecs.addMergedRegion(new CellRangeAddress(row,row,0,4));
            //4th empty row
            ++row;
            worksheetSpecs.addMergedRegion(new CellRangeAddress(row,row,0,4));
            //5th row
            instrRow = worksheetSpecs.createRow(++row);
            cell = instrRow.createCell(0);
            cell.setCellValue(getActiveLangString("Instructions_ColumnSpecifications") + ":");
            worksheetSpecs.addMergedRegion(new CellRangeAddress(row,row,0,4));            
            
            //Specs
            instrRow = worksheetSpecs.createRow(++row);
            createSpecCells(worksheetSpecs, instrRow, true, new String[]{
                getActiveLangString("Column"),
                getActiveLangString("Title"),
                getActiveLangString("Required"),
                getActiveLangString("Type"),
                getActiveLangString("NumberOfDecimals"),
                getActiveLangString("MinimumValue"),
                getActiveLangString("MaximumValue"),
                getActiveLangString("MaximumLength"),
                getActiveLangString("ValidValues"),
                getActiveLangString("Remarks")
            });
            for (int colIndex = 0; colIndex < cells.length ; colIndex++) {
                instrRow = worksheetSpecs.createRow(++row);
                ImportExcelSampleCell sampleCell = cells[colIndex];
                sampleCell.createSpecCells(worksheetSpecs, instrRow, this.activeLanguage);
            }            
            
            return workbook;
        }

        private static void createSpecCells(HSSFSheet worksheet, HSSFRow row, boolean isHeader, String[] cellValues ){

            for (int cellIndex = 0; cellIndex < cellValues.length; cellIndex++){
                HSSFCell cell = row.createCell(cellIndex);
                cell.setCellValue(cellValues[cellIndex]);
                cell.setCellStyle(isHeader? cellStyleBoarderedHeader : cellStyleBoardered);
                worksheet.autoSizeColumn(cellIndex);
            }
        }
        
        private static String getNumberFormat(Integer decimals){
            if (decimals == null)
                return "General";
            
            String ret = "0";
            if (decimals > 0) {
                ret = ret + ".";
                for (int i = 0; i < decimals; i++) {
                    ret = ret + "0";
                }
            }
            
            return ret;
        }
        
        private static String joinStringArray(String[] vals, char separator) {
            String ret = "";
            if (vals == null || vals.length == 0)
                return ret;
            
            for (String val : vals) {
                ret = ret + (ret.equals("") ? "" : separator) + val;
            }
            return ret;
        }
    }
}
