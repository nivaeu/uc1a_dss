/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.base;

import java.lang.annotation.Retention;
import java.lang.annotation.*;
import org.jboss.resteasy.annotations.StringParameterUnmarshallerBinder;
/**
 *
 * @author gmamais
 */

@Retention(RetentionPolicy.RUNTIME)
@StringParameterUnmarshallerBinder(DateFormatter.class)
public @interface DateFormat {
    String value();
}

