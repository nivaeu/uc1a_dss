/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gr.neuropublic.base;

import java.math.BigInteger;

/**
 *
 * @author st_krommydas
 */
public class WsResponseInfo {
    public String path;
    public BigInteger startTs;
    public BigInteger endTs;
    public Boolean success;
    public String errorCode;
}
