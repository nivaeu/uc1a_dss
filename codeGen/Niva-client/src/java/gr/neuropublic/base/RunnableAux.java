/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.base;

import gr.neuropublic.utils.TwoTuple;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author g_mamais
 */
public class RunnableAux<T> {
    
    public void doInParraller(Collection<T> data, gr.neuropublic.functional.Action1<T> action) {
        int nJobs = Runtime.getRuntime().availableProcessors();
        doInParraller(data,action, null, 10, nJobs);
    }    
    public void doInParraller(Collection<T> data, gr.neuropublic.functional.Action1<T> action, gr.neuropublic.functional.Action1<TwoTuple<T,Exception>> onError, int maxWaitInHours) {
        int nJobs = Runtime.getRuntime().availableProcessors();
        doInParraller(data,action, onError, maxWaitInHours, nJobs);
    }
    public void doInParraller(final Collection<T> data, final gr.neuropublic.functional.Action1<T> action, final gr.neuropublic.functional.Action1<TwoTuple<T,Exception>> onError,final  int maxWaitInHours, int nJobs) {
        ThreadPoolExecutor executor0 = (ThreadPoolExecutor) Executors.newFixedThreadPool(nJobs);
        final ConcurrentLinkedDeque<T> queue = new ConcurrentLinkedDeque<>(data);
        final int size = data.size();
        final Integer  size10 = size/10;
        final AtomicInteger l = new AtomicInteger(0);
        
        for (int i = 0; i < nJobs; i++) 
        {
            executor0.execute(new Runnable() {
                @Override
                public void run() {
                    while (!queue.isEmpty()) {
                        T item = queue.pollFirst();
                        if (item != null) {
                            try {
                                int curItem = l.getAndIncrement();
                                if ((size10 >0) && (curItem %  size10 == 0)) {
                                    System.out.println("Progress :" + (100*curItem)/size + "%");
                                }
                                action.lambda(item);
                            } catch(Exception e) {
                                if (onError != null) {
                                    onError.lambda(new TwoTuple(item, e));
                                } else {
                                    throw e;
                                }
                            }
                        } else {
                            try {Thread.sleep(10);} catch (InterruptedException ex) {Logger.getLogger(RunnableAux.class.getName()).log(Level.SEVERE, null, ex);}
                        }
                    }
                }
            });
        }
        executor0.shutdown();      
        
        try {
           executor0.awaitTermination(maxWaitInHours, TimeUnit.HOURS);
        } catch (InterruptedException ex) {
           throw new gr.neuropublic.exceptions.GenericApplicationException(ex.getMessage());
        }
        
    }
    
}
