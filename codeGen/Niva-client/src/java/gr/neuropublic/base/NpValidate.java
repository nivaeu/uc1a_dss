/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gr.neuropublic.base;

import gr.neuropublic.exceptions.GenericApplicationException;
import gr.neuropublic.exceptions.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 *
 * @author st_krommydas
 */
public class NpValidate {
    
    private static final Logger logger = LoggerFactory.getLogger(NpValidate.class);

    public static class ValidationMessages {
        public String errors = null;
        public String warnings = null;
        public String infos = null;
    }

    public static enum DbType{PostgreSql, Oracle}
    
    private static ValidationMessages getValidationMessages (ArrayList<Object[]> validateResults) {
        ValidationMessages ret = new ValidationMessages();
        ArrayList<String> errorList = new ArrayList<>();
        ArrayList<String> warningList = new ArrayList<>();
        ArrayList<String> infoList = new ArrayList<>();
        for (Object[] res : validateResults) {
            if ("error".equals((String) res[0])) 
                errorList.add((String) res[1]);
            
            if ("warning".equals((String) res[0])) 
                warningList.add((String) res[1]);
            
            if ("info".equals((String) res[0])) 
                infoList.add((String) res[1]);
        }

        if (!errorList.isEmpty()) {
            ret.errors = "";
            for (String err : errorList) {
                ret.errors += err + "%%";
            }
        }
        if (!warningList.isEmpty()) {
            ret.warnings = "";
            for (String err : warningList) {
                ret.warnings += err + "%%";
            }
        }
        if (!infoList.isEmpty()) {
            ret.infos = "";
            for (String err : infoList) {
                ret.infos += err + "%%";
            }
        }
        
        return ret;
    }

    public static void executeAfterPostValidations (
                DbType dbType,
                SaveResponse ret, 
                EntityManager em, 
                String validateFunction, 
                Integer level,  
                String appName,
                String groupCodes, 
                String validationKey){
        
        String qry = "";
        if (dbType == DbType.PostgreSql){
            qry = "select * " +
                  "  from " + validateFunction +
                  "(:level, :appName, :groupCodes, :validationKey)";
        } else if (dbType == DbType.Oracle) {
            qry = "select * " +
                  "  from table(" + validateFunction +
                  "(:level, :appName, :groupCodes, :validationKey))";
        }
        
        Query validateQuery = em.createNativeQuery(qry);
        validateQuery.setParameter("level", level);
        validateQuery.setParameter("appName", appName);
        validateQuery.setParameter("groupCodes", groupCodes);
        validateQuery.setParameter("validationKey", validationKey);

        ArrayList<Object[]> validateResults = (ArrayList) validateQuery.getResultList();
        NpValidate.ValidationMessages messages = NpValidate.getValidationMessages(validateResults);

        if (messages.errors != null) 
            throw new ValidationException(messages.errors);
        if (messages.warnings != null) 
            ret.warningMessages = ret.warningMessages != null ? ret.warningMessages + messages.warnings : messages.warnings;
        if (messages.infos != null)
            ret.infoMessages = ret.infoMessages != null ? ret.infoMessages + messages.infos : messages.infos;
    
    }    
}
