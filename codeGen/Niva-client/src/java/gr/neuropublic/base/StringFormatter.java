/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gr.neuropublic.base;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author st_krommydas
 */
public class StringFormatter {

    private static char decimalSeparator = ',';
    
    public static String plainText(Short input){
        if (input == null)
            return null;
        return input.toString();
    }
    public static String plainText(Integer input){
        if (input == null)
            return null;
        return input.toString();
    }
    public static String plainText(Long input){
        if (input == null)
            return null;
        return input.toString();
    }
    public static String plainText(BigInteger input){
        if (input == null)
            return null;
        return input.toString();
    }
    public static String plainText(BigDecimal input){
        if (input == null)
            return null;
        return input.toPlainString().replace('.', decimalSeparator);
    }
    public static String plainText(Float input){
        if (input == null)
            return null;
        return input.toString();
    }
    public static String plainText(Double input){
        if (input == null)
            return null;
        
        if(input.doubleValue() == input.longValue())
            return String.format("%d", input.longValue()).replace('.', decimalSeparator);
        else
            return String.format("%s", input).replace('.', decimalSeparator);
    }
    
    public static String plainText(String input){
        return input;
    }
    public static String plainText(Boolean input){
        if (input == null)
            return null;
        return input.toString();
    }

    public static String plainText(Date input){
        if (input == null)
            return null;
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        return df.format(input);
    }
    public static String plainText(java.sql.Timestamp input){
        if (input == null)
            return null;
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return df.format(input);
    }
}
