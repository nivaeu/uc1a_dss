/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gr.neuropublic.base;

/**
 *
 * @author st_krommydas
 */
public class User {
    public final Integer id;
    public final String internalUserName;
    public final String publicUserName;
    public final String activeEmail;
    public final String tin;
    public final String lastname;
    public final String firstname;
    public final String fathername;
    public final Integer subsId;
    public final String subsCode;
    public final String subsDescription;

    public User(
            final Integer id, 
            final String internalUserName, 
            final String publicUserName, 
            final String activeEmail, 
            final String tin, 
            final String lastname, 
            final String firstname, 
            final String fathername, 
            final Integer subsId, 
            final String subsCode, 
            final String subsDescription) {
        this.id = id;
        this.internalUserName = internalUserName;
        this.publicUserName = publicUserName;
        this.activeEmail = activeEmail;
        this.tin = tin;
        this.lastname = lastname;
        this.firstname = firstname;
        this.fathername = fathername;
        this.subsId = subsId;
        this.subsCode = subsCode;
        this.subsDescription = subsDescription;
    }

    
}
