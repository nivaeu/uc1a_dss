/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gr.neuropublic.base;

import java.util.ArrayList;

/**
 *
 * @author st_krommydas
 */
public class SaveResponse {
    public static  class NewEntityId {
        public String entityName;
        public String tempId;
        public String databaseId;
    }
    public ArrayList<NewEntityId> newEntitiesIds;
    public String warningMessages;
	public String infoMessages;
}
