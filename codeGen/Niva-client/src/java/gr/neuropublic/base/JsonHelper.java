/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.base;

import gr.neuropublic.exceptions.GenericApplicationException;
import java.math.BigDecimal;
import org.json.simple.JSONObject;

public class JsonHelper {
    public static String escapeString(String input) {
        if (input == null)
            return null;
        return
            input.
            replace("\\", "\\\\").
            replace("\"", "\\\"").
            replace("\n", "\\n").
            replace("\b", "\\b").
            replace("\f", "\\f").
            replace("\r", "\\r").
            replace("\t", "\\t");
    }

    public static Double getDoubleFromJsonParams(JSONObject params, String paramName) {
        if (paramName == null)
            return null;

        Object obj = params.get(paramName);
        Double ret = null;
        if (obj != null) {
            if (obj instanceof Double) {
                ret = (Double)obj;
            } else if (obj instanceof Long) {
                ret = ((Long)obj).doubleValue();
            } else {
                throw new GenericApplicationException("Error in getDoubleFromJsonParams. " + paramName + " with value '"+obj.toString()+"' cannot be converted to Double");
            }
        }
        return ret;
    }
    
    public static BigDecimal getBigDecimalFromJsonParams(JSONObject params, String paramName){
        Double retDouble = getDoubleFromJsonParams(params, paramName);
        BigDecimal ret = retDouble != null ? new BigDecimal(retDouble.toString()) : null;
        return ret;
    }

    public static Float getFloatFromJsonParams(JSONObject params, String paramName){
        Double retDouble = getDoubleFromJsonParams(params, paramName);
        Float ret = retDouble != null ? retDouble.floatValue() : null;
        return ret;
    }

}
