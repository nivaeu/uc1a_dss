/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.base;

/**
 *
 * @author gmamais
 */
public class UserBannerMessage {
    public String message;
    public Integer type;
    public Integer location;

    public String toJson() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");

        sb.append("\"message\":"); sb.append("\""); sb.append(JsonHelper.escapeString(message)); sb.append("\"");
        if (type != null){
            sb.append(",");sb.append("\"type\":"); sb.append(type); 
        }
        if (location != null){
            sb.append(",");sb.append("\"location\":"); sb.append(location); 
        }

        sb.append("}"); 
        return sb.toString();
    }

}
