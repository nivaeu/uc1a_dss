package gr.neuropublic.base;

import gr.neuropublic.mutil.base.Pair;
import java.util.List;

public interface IUserManagementService {
    public String getUserLoginNameByPublicEmail(final String publicEmail);
    public void setUserPassword(final Integer userId, final String publicUserName, final String password, final String loggedInUserName);
    public void setUserPassword(final Integer userId, final String publicUserName, final String password, final String loggedInUserName, final boolean isPwdChangedByAdmin, final String clientIp);
    public AuthenticationStatus getUserAuthorizationStatus (final String userSubsCode, final String userEmail, String userPassword, final String app, final String privilege);
    public Pair<AuthenticationStatus, Integer> getUserAuthenticationStatus_WithoutSessionClear(final String email, final String userPassword, final String app, final Boolean checkPwdExpiration, final Boolean setsSsoValue, final Pair<Boolean, String> checkLoginSubscriberInfo);
    public Pair<AuthenticationStatus,Integer> getUserAuthenticationStatus(final String email, final String userPassword, final String app, final Boolean checkPwdExpiration, final Boolean setsSsoValue, final Pair<Boolean, String> checkLoginSubscriberInfo);
    public Pair<Pair<AuthenticationStatus,Integer>,String> getUserAuthenticationStatusBasedOnSso(final String ssoSessionId, final String app);
    public Pair<AuthenticationStatus, Integer> getExternalUserAuthenticationStatus(final String email, final String app);
    public void makeNoteOfSuccessfulLogin(  final String     userName, 
                                            final String     userIp, 
                                            final String     userSessionId, 
                                            final String     ssoSessionId,
                                            final String     appName);
    public void makeNoteOfUnSuccessfulLogin(final String     userName, 
                                            final String     userIp, 
                                            final String     appName,
                                            final String     authenticationStatus,
                                            final boolean    statusLocks); 
    public void logoutUserBySessionId(  final String     sessionId); 
    public void userSessionExpired(final String sessionId);

    public UserSession getUserSession(final String sessionId);
    public Boolean getAppWsResponeInfoEnabled(final String modu_name);
    public void insertWsResponseInfo(final String sessionId, final String modu_name, final Integer subs_id, final List<WsResponseInfo> wsResponseInfoList);
    public List<UserBannerMessage> getUserBannerMessages(final String sessionId, final Integer user_id, final Integer subs_id, final String modu_name);
    public ProducerCheckStatus getProducerStatus(final String producerVat, final String appName, final Integer fromSubsId);
    public void checkProducerStatus(final String producerVat, final String appName, final String appTitle, final Integer fromSubsId);
    public User getUserById(final Integer userId);
    public Integer getUserIdByUserName(final String userName);
    public SqlQueryRow getAuditInfoUsers(final String userInsert, final String userUpdate, final boolean showUsernameIfNotFound, final boolean showUsername);
    public void clearUserSsoSessions(String sessionId);
    public String createExternalUserBy_Tin_SubsCode(final String appName, final String subsCode, final String userRole, final String userTin, final String userInsert, final String userEmail, final String userFirstName, final String userLastName, final String userFatherName, final String userPhone, final String userPhoneCell, final String userIdentityNumber, final String userAddress, final String userAddressNumber, final String userPostalCode, final String userCity, final String userPrefecture, final String userCountry);
    public Pair<AuthenticationStatus, Integer> getUserAuthenticationStatusFromSoap(final String userEmail, final String userPassword, final String appName, final String privilege,final Boolean checkPwdExpiration, final Boolean setsSsoValue); 
    
}
