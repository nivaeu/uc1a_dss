/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.base;

import java.lang.annotation.Annotation;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jboss.resteasy.spi.StringParameterUnmarshaller;
import org.jboss.resteasy.util.FindAnnotation;

/**
 *
 * @author gmamais
 */


public class DateFormatter implements StringParameterUnmarshaller<Date> {

    private SimpleDateFormat formatter;
    private boolean bISO8601;
    public void setAnnotations(Annotation[] annotations) {
        DateFormat format = FindAnnotation.findAnnotation(annotations, DateFormat.class);
        if ("ISO8601".equals(format.value())) {
            bISO8601 = true;
        } else {
            formatter = new SimpleDateFormat(format.value());    
        }
    }
    
    public static Date parseISO8601Date(String str) {
        try {
        
            if (str == null)
                return null;
            String tmp=str;
            if (tmp.startsWith("\""))
                tmp = tmp.substring(1);
            if (tmp.endsWith("\""))
                tmp = tmp.substring(0,tmp.length()-1);
            return gr.neuropublic.utils.DateUtil.parseISO8601Date(tmp);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public Date fromString(String str) {
        try {
            if (bISO8601) {
                return DateFormatter.parseISO8601Date(str);
            } else {
                return formatter.parse(str);
            }
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}