package gr.neuropublic.base;


public interface ISessionsCache {

        public UserSession getEntry(String sessionId);
        public UserSession getEntry(String sessionId, boolean resetMemCacheTimer );
        public UserSession getEntry(String sessionId, String ssoSessionId, boolean resetMemCacheTimer);
        public void removeEntryAndLogoutFromDataBase(String sessionId, String ssoSessionId, Boolean removeSsoKey);
        public boolean sessionBelongToSso(String sessionId, String ssoSessionId);
}
