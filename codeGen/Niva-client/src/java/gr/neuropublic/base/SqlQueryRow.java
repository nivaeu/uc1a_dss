/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.base;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vividsolutions.jts.geom.Geometry;
import org.geotools.geojson.geom.GeometryJSON;


import gr.neuropublic.mutil.base.Pair;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.DataFormat;
import org.geotools.geojson.geom.GeometryJSON;
import org.slf4j.LoggerFactory;

/**
 *
 * @author gmamais
 */
public class SqlQueryRow extends HashMap<String,Object> {

    public String toJson() {
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        
        for (Map.Entry<String, Object> entry : entrySet()) {
            sb.append("\""+entry.getKey().toLowerCase()+"\":");

            if (entry.getValue() != null) {
                Object value = null;

                Date date = entry.getValue() instanceof Date ? (Date)entry.getValue() : null;
                String string = entry.getValue() instanceof String ? (String)entry.getValue() : null;
                Geometry geom = entry.getValue() instanceof Geometry ? (Geometry)entry.getValue() : null;

                if (date != null) {
                    value = date.getTime();
                } else if (string != null) {
                    value = "\"" + JsonHelper.escapeString(string) + "\"";
                } else if (geom != null) {
                    GeometryJSON gjson = new GeometryJSON(6); 
                    value = gjson.toString(geom);
                } else {
                    value = entry.getValue();
                }
                sb.append(value);
            } else {
                sb.append("null");
            }
            sb.append(",");    
        }        

        sb.deleteCharAt(sb.length()-1);
        sb.append('}'); 
        return sb.toString();
    }



    public void toGeoJson(StringBuilder sb) {
        sb.append('{'); 
        sb.append("\"type\": \"Feature\","); 
        //append properties
        sb.append("\"properties\": {");
        
        int propLen = 0;
        
        for (Map.Entry<String, Object> entry : entrySet()) {
            
            if ("geom".equals(entry.getKey())) {
                continue;
            }
            
            sb.append("\""+entry.getKey().toLowerCase()+"\":");

            
            if (entry.getValue() != null) {
                Object value = null;

                Date date = entry.getValue() instanceof Date ? (Date)entry.getValue() : null;
                String string = entry.getValue() instanceof String ? (String)entry.getValue() : null;

                if (date != null) {
                    value = date.getTime();
                } else if (string != null) {
                    value = "\"" + JsonHelper.escapeString(string) + "\"";
                } else {
                    value = entry.getValue();
                }
                sb.append(value);
            } else {
                sb.append("null");
            }
            sb.append(",");   
            propLen++;
        }        

        if (propLen>0) {
            sb.deleteCharAt(sb.length()-1);
        }
        sb.append("},"); 
        //append geometry
        sb.append("\"geometry\":");

        Geometry geom = (Geometry)getGeom();
        
        if (geom != null) {
            GeometryJSON gjson = new GeometryJSON(6); 
            String value = gjson.toString(geom);
            sb.append(value);
        }  else {
            sb.append("null");
        }       
        
        sb.append('}'); 
    }


    @Override
    public Object put(String key, Object value) {
        return super.put(key.toLowerCase(), value);
    }


    public static SqlQueryRow createById(Object id) {
        SqlQueryRow ret = new SqlQueryRow();
        ret.setId(id);
        return ret;
    }
    
    
    public Object getId() {
        if (this.containsKey("id"))
            return this.get("id");
        return null;
    }
    public void setId(Object id) {
        this.put("id", id);
    }
    
    public Object getGeom() {
        if (this.containsKey("geom"))
            return this.get("geom");
        return null;
    }
    public void setGeom(Object geom) {
        this.put("geom", geom);
    }


    public static byte[] ExportToExcel(List<Pair<String,String>> colDefs, List<SqlQueryRow> sqlRows) {
        
        
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet worksheet = workbook.createSheet("Sheet1");

        // add headers
        HSSFRow headersRow = worksheet.createRow(0);
        HSSFCellStyle cellStyleHeader = workbook.createCellStyle();
        cellStyleHeader.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
        cellStyleHeader.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        
        // add headers
        int colIndex=0;
        for(Pair<String,String> colDef:colDefs) {
            HSSFCell cellSubsId_shortName = headersRow.createCell(colIndex);
            cellSubsId_shortName.setCellValue(colDef.b);
            cellSubsId_shortName.setCellStyle(cellStyleHeader);
            worksheet.autoSizeColumn(colIndex);
            colIndex++;
        }
        
        HSSFCellStyle cellStyle = workbook.createCellStyle();
        HSSFCellStyle cellStyleDate = workbook.createCellStyle();
        DataFormat formatForDate = workbook.createDataFormat();
        cellStyleDate = workbook.createCellStyle();
        cellStyleDate.setDataFormat(formatForDate.getFormat("dd/mm/yyyy"));
        if (sqlRows!=null && !sqlRows.isEmpty()) {
            int rowsLength = sqlRows.size();
            for(int i=0;i<rowsLength; i++) {
                HSSFRow entityRow = worksheet.createRow(i+1);
                SqlQueryRow r = sqlRows.get(i);
                int colsLength = colDefs.size();
                for(int j=0;j<colsLength; j++) {
                    String colName = colDefs.get(j).a;
                    Object v = r.get(colName);
                    HSSFCell cell = entityRow.createCell(j);
                    if (v!=null) {
                        Date date = null;
                        String strVal = null;
                        Double dbVal = null;
                        Double IntVal = null;
                        Double BigIntVal = null;
                        Double BigDecVal = null;
                        
                        if ((date = v instanceof Date ? (Date)v : null) != null) {
                            cell.setCellValue(date);
                        } else if ((strVal = v instanceof String ? (String)v : null) != null) {
                            cell.setCellValue(strVal);
                        } else if ((dbVal = v instanceof Double ? (Double)v : null) != null) {
                            cell.setCellValue(dbVal);
                        } else if ((IntVal = v instanceof Integer ? new Double((Integer)v) : null) != null) {
                            cell.setCellValue(IntVal);
                        } else if ((BigIntVal = v instanceof BigInteger ? new Double(((BigInteger)v).longValue()) : null) != null) {
                            cell.setCellValue(BigIntVal);
                        } else if ((BigDecVal = v instanceof BigDecimal ? ((BigDecimal)v).doubleValue() : null) != null) {
                            cell.setCellValue(BigDecVal);
                        }
                    }
                    cell.setCellStyle(cellStyle);
                }                
            }
        }

        // ByteArrayOutput
        java.io.ByteArrayOutputStream strm = new java.io.ByteArrayOutputStream();
        try {
            workbook.write(strm);
        } catch (IOException e) {
            LoggerFactory.getLogger(SqlQueryRow.class).error("ExportToExcel", e);
            throw new RuntimeException(e);
        }
        
        return strm.toByteArray();
    }



}
