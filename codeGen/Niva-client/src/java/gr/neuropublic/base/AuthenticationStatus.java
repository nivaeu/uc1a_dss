/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.base;

/**
 *
 * @author gmamais
 */
public enum AuthenticationStatus {
    SUCCESS, 
    USER_STATUS_NOT_ACTIVE, 
    SUBSCRIBER_NOT_ASSOC_WITH_APP,
    USER_NOT_ASSOC_WITH_APP_ALTHOUGH_SUBSCRPTN_OK,
    USER_NOT_ASSOC_WITH_APP,
    SUBSCRIBER_STATUS_NOT_ACTIVE,
    SUBSCRIBER_MODULEGROUP_NOT_ACTIVE,
    SUBSCRIBER_MODULEGROUP_TEMPRL_MISMATCH,
    UNKNOWN_APP,
    AUTH_ERROR,
    WRONG_PASSWORD, 
    ACCOUNT_DOESNOT_EXIST, 
    CONCURRENT_ACCESS, 
    ALREADY_LOGGEDIN,
    SSO_SESSION_HAS_EXPIRED,
    USER_LOCKED_BY_ATTEMPTS_LIMIT,
    USER_PASSWORD_HAS_EXPIRED,
    INVALID_SUBSCRIBER,
    WRONG_LOGIN_SUBSCRIBER
}        
