/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.base;

import java.util.List;
import gr.neuropublic.validators.ICheckedEntity;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import gr.neuropublic.mutil.base.Pair;
import gr.neuropublic.enums.DataBaseType;
import gr.neuropublic.functional.Action1Ex;
import gr.neuropublic.functional.Func1Ex;
import java.sql.Connection;
import javax.persistence.EntityManager;

/**
 *
 * @author gmamais
 */
public interface IAbstractService {
    public void synchronizeChangesWithDb(String userName, List<ChangeToCommit<ICheckedEntity>> changes);
    public void synchronizeChangesWithDbNoClone(String userName, List<ChangeToCommit<ICheckedEntity>> changes);

    public void removeEntity(String userName, ICheckedEntity entityToBeRemobed);
    public void saveEntityToDB(final String userName, final ICheckedEntity entity);
    public void saveEntityToDB(final UserSession usrSession, final ICheckedEntity entity);
    public gr.neuropublic.validators.ICheckedEntity createEntityFromJson(Map<String,Object> keysMap, String entiyName, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.base.CryptoUtils crypto, Map<String,Set<String>> allowed_entity_fields);
    public List<SqlQueryRow> executeSqlQuery(final String sqlQuery, final Object ... params );
    public List<SqlQueryRow> executeSqlQuery2(final String sqlQuery, final Pair<String, Object> ... params );
    public List<SqlQueryRow> executeSqlQuery2(final String sqlQuery, final ArrayList<Pair<String, Object>> params );
    public List<SqlQueryRow> executeSqlQuery2(final Connection externalJdbcConnection, final String sqlQuery, final ArrayList<Pair<String, Object>> params );
    public List<SqlQueryRow> executeSqlQuery2(
            final String jdbcDriver, 
            final String jdbcConnectionURL,
            final String user, 
            final String pass,
            final String sqlQuery, 
            final ArrayList<Pair<String, Object>> params );
    public Integer executeSqlDml(final String sql, final Object ... params );
    public void executeDbProcedure(final String sql, final Object ... params );
    public void executeDbProcedure(final EntityManager em, final String sql, final Object ... params );
    public void executeToDbGeneric(final Action1Ex<Connection> javaClosure);
    public void executeToDbGeneric(final EntityManager em, final Action1Ex<Connection> javaClosure);
    public void executeToDbGeneric(final EntityManager em, final Action1Ex<Connection> javaClosure, Action1Ex<String> onError);
    public <TRes> TRes executeToDbGeneric(final Func1Ex<Connection, TRes> javaClosure);
    public <TRes> TRes executeToDbGeneric(final EntityManager em, final Func1Ex<Connection, TRes> javaClosure);
    public <TRes> TRes executeToDbGeneric(final EntityManager em, final Func1Ex<Connection, TRes> javaClosure, Action1Ex<String> onError);
    public Object getSingleResultOrNull(final String jpql, final Pair<String, Object> ... params);
    
    
    public void initializeDatabaseSession(UserSession cx);
    
}
