/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.neuropublic.base;

public enum ProducerCheckStatus {
    OK,
    USER_NOT_REGISTERED,
    USER_NOT_REGISTERED_IN_APP,
    USER_NOT_REGISTERED_IN_APP_BY_SUBSCRIBER;
    
    
    @Override
    public String toString(){
       switch (this) {
           case OK : return "OK" ;
           case USER_NOT_REGISTERED : return "USER_NOT_REGISTERED" ;
           case USER_NOT_REGISTERED_IN_APP : return "USER_NOT_REGISTERED_IN_APP" ;
           case USER_NOT_REGISTERED_IN_APP_BY_SUBSCRIBER : return "USER_NOT_REGISTERED_IN_APP_BY_SUBSCRIBER" ;
           default:
               return "";
       }
    }    
}
