package gr.neuropublic.rtlEntities;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.validators.annotations.ValidateMethods;
import gr.neuropublic.validators.annotations.ValidateUniqueKeys;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;


@Entity
@Table(name = "ss_subscribers", schema="subscription" ,  
    uniqueConstraints={
        @UniqueConstraint(columnNames={"subs_short_name"})
    })
@ValidateUniqueKeys
@ValidateMethods
//@org.hibernate.annotations.DynamicUpdate()
@org.hibernate.annotations.Entity(
		dynamicUpdate = true
)
@XmlRootElement
public class RTL_Subscriber implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'subs_id' είναι υποχρεωτικό")
    @Column(name = "subs_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ss_subscribers_subs_id")
    //@SequenceGenerator(name="ss_subscribers_subs_id", sequenceName="subscription.subs_id_seq", allocationSize=1)
    protected Integer id;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'subs_short_name' είναι υποχρεωτικό")
    @Size(min = 1, max = 60, message="Το πεδίο 'subs_short_name' πρέπει νά έχει μέγεθος μεταξύ 1 και 60.")
    @Column(name = "subs_short_name")
    protected String shortName;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'subs_vat' είναι υποχρεωτικό")
    @Size(min = 1, max = 20, message="Το πεδίο 'subs_vat' πρέπει νά έχει μέγεθος μεταξύ 1 και 20.")
    @Column(name = "subs_vat")
    protected String vat;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'subs_legal_name' είναι υποχρεωτικό")
    @Size(min = 1, max = 200, message="Το πεδίο 'subs_legal_name' πρέπει νά έχει μέγεθος μεταξύ 1 και 200.")
    @Column(name = "subs_legal_name")
    protected String legalName;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'subs_tax_office' είναι υποχρεωτικό")
    @Size(min = 1, max = 60, message="Το πεδίο 'subs_tax_office' πρέπει νά έχει μέγεθος μεταξύ 1 και 60.")
    @Column(name = "subs_tax_office")
    protected String taxOffice;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'subs_occupation' είναι υποχρεωτικό")
    @Size(min = 1, max = 200, message="Το πεδίο 'subs_occupation' πρέπει νά έχει μέγεθος μεταξύ 1 και 200.")
    @Column(name = "subs_occupation")
    protected String occupation;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'subs_phone' είναι υποχρεωτικό")
    @Size(min = 1, max = 20, message="Το πεδίο 'subs_phone' πρέπει νά έχει μέγεθος μεταξύ 1 και 20.")
    @Column(name = "subs_phone")
    protected String phone;



    @Basic(optional = true)
    @Size(min = 0, max = 20, message="Το πεδίο 'subs_fax' πρέπει νά έχει μέγεθος μεταξύ 0 και 20.")
    @Column(name = "subs_fax")
    protected String fax;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'subs_address' είναι υποχρεωτικό")
    @Size(min = 1, max = 120, message="Το πεδίο 'subs_address' πρέπει νά έχει μέγεθος μεταξύ 1 και 120.")
    @Column(name = "subs_address")
    protected String address;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'subs_address_number' είναι υποχρεωτικό")
    @Size(min = 1, max = 10, message="Το πεδίο 'subs_address_number' πρέπει νά έχει μέγεθος μεταξύ 1 και 10.")
    @Column(name = "subs_address_number")
    protected String addressNumber;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'subs_postal_code' είναι υποχρεωτικό")
    @Size(min = 1, max = 10, message="Το πεδίο 'subs_postal_code' πρέπει νά έχει μέγεθος μεταξύ 1 και 10.")
    @Column(name = "subs_postal_code")
    protected String postalCode;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'subs_city' είναι υποχρεωτικό")
    @Size(min = 1, max = 60, message="Το πεδίο 'subs_city' πρέπει νά έχει μέγεθος μεταξύ 1 και 60.")
    @Column(name = "subs_city")
    protected String city;



    @Basic(optional = true)
    @Size(min = 0, max = 60, message="Το πεδίο 'subs_prefecture' πρέπει νά έχει μέγεθος μεταξύ 0 και 60.")
    @Column(name = "subs_prefecture")
    protected String prefecture;



    @Basic(optional = true)
    @Size(min = 0, max = 60, message="Το πεδίο 'subs_country' πρέπει νά έχει μέγεθος μεταξύ 0 και 60.")
    @Column(name = "subs_country")
    protected String country;



    @Basic(optional = true)
    @Size(min = 0, max = 120, message="Το πεδίο 'subs_email' πρέπει νά έχει μέγεθος μεταξύ 0 και 120.")
    @Column(name = "subs_email")
    protected String email;



    @Basic(optional = true)
    @Size(min = 0, max = 120, message="Το πεδίο 'subs_web_site' πρέπει νά έχει μέγεθος μεταξύ 0 και 120.")
    @Column(name = "subs_web_site")
    protected String webSite;



    @Basic(optional = true)
    @Size(min = 0, max = 500, message="Το πεδίο 'subs_database' πρέπει νά έχει μέγεθος μεταξύ 0 και 500.")
    @Column(name = "subs_database")
    protected String database;



    @Basic(optional = true)
    @Size(min = 0, max = 60, message="Το πεδίο 'subs_application_url' πρέπει νά έχει μέγεθος μεταξύ 0 και 60.")
    @Column(name = "subs_application_url")
    protected String applicationUrl;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'subs_invoicing_method' είναι υποχρεωτικό")
    @Column(name = "subs_invoicing_method")
    protected Short invoicingMethod;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'subs_company_vat' είναι υποχρεωτικό")
    @Size(min = 1, max = 9, message="Το πεδίο 'subs_company_vat' πρέπει νά έχει μέγεθος μεταξύ 1 και 9.")
    @Column(name = "subs_company_vat")
    protected String companyVat;


    @Basic(optional = true)
    @Column(name = "subs_billing_comp_id")
    protected Integer billingCompId;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'subs_user_auto_inactivate' είναι υποχρεωτικό")
    @Column(name = "subs_user_auto_inactivate")
    protected Boolean userAutoInactivate;


    @Basic(optional = true)
    @Column(name = "subs_login_policy")
    protected Integer loginPolicy;



    @Basic(optional = true)
    @Size(min = 0, max = 60, message="Το πεδίο 'subs_legal_name_short' πρέπει νά έχει μέγεθος μεταξύ 0 και 60.")
    @Column(name = "subs_legal_name_short")
    protected String legalNameShort;



    public RTL_Subscriber() {
    }

    public RTL_Subscriber(Integer id) {
        this.id = id;
    }

    public RTL_Subscriber(Integer id, String shortName, String vat, String legalName, String taxOffice, String occupation, String phone, String fax, String address, String addressNumber, String postalCode, String city, String prefecture, String country, String email, String webSite, String database, String applicationUrl, Short invoicingMethod, Integer suvcId, String companyVat, Integer billingCompId, Integer rowVersion, Boolean userAutoInactivate, Integer loginPolicy, String legalNameShort) {
        this.id = id;
        this.shortName = shortName;
        this.vat = vat;
        this.legalName = legalName;
        this.taxOffice = taxOffice;
        this.occupation = occupation;
        this.phone = phone;
        this.fax = fax;
        this.address = address;
        this.addressNumber = addressNumber;
        this.postalCode = postalCode;
        this.city = city;
        this.prefecture = prefecture;
        this.country = country;
        this.email = email;
        this.webSite = webSite;
        this.database = database;
        this.applicationUrl = applicationUrl;
        this.invoicingMethod = invoicingMethod;
        this.companyVat = companyVat;
        this.billingCompId = billingCompId;
        this.rowVersion = rowVersion;
        this.userAutoInactivate = userAutoInactivate;
        this.loginPolicy = loginPolicy;
        this.legalNameShort = legalNameShort;
    }

    public RTL_Subscriber clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "RTL_Subscriber:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (RTL_Subscriber)alreadyCloned.get(key);

        RTL_Subscriber clone = new RTL_Subscriber();
        alreadyCloned.put(key, clone);

        clone.setId(getId());
        clone.setShortName(getShortName());
        clone.setVat(getVat());
        clone.setLegalName(getLegalName());
        clone.setTaxOffice(getTaxOffice());
        clone.setOccupation(getOccupation());
        clone.setPhone(getPhone());
        clone.setFax(getFax());
        clone.setAddress(getAddress());
        clone.setAddressNumber(getAddressNumber());
        clone.setPostalCode(getPostalCode());
        clone.setCity(getCity());
        clone.setPrefecture(getPrefecture());
        clone.setCountry(getCountry());
        clone.setEmail(getEmail());
        clone.setWebSite(getWebSite());
        clone.setDatabase(getDatabase());
        clone.setApplicationUrl(getApplicationUrl());
        clone.setInvoicingMethod(getInvoicingMethod());
        clone.setCompanyVat(getCompanyVat());
        clone.setBillingCompId(getBillingCompId());
        clone.setRowVersion(getRowVersion());
        clone.setDteinsert(getDteinsert());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteupdate(getDteupdate());
        clone.setUsrupdate(getUsrupdate());
        clone.setUserAutoInactivate(getUserAutoInactivate());
        clone.setLoginPolicy(getLoginPolicy());
        clone.setLegalNameShort(getLegalNameShort());

        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "RTL_Subscriber:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(id) : id); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (id != null) {
            sb.append("\"id\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(id) : id); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"id\":"); sb.append("null");sb.append(",");
        }
        if (shortName != null) {
            sb.append("\"shortName\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(shortName)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"shortName\":"); sb.append("null");sb.append(",");
        }
        if (vat != null) {
            sb.append("\"vat\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(vat)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"vat\":"); sb.append("null");sb.append(",");
        }
        if (legalName != null) {
            sb.append("\"legalName\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(legalName)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"legalName\":"); sb.append("null");sb.append(",");
        }
        if (taxOffice != null) {
            sb.append("\"taxOffice\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(taxOffice)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"taxOffice\":"); sb.append("null");sb.append(",");
        }
        if (occupation != null) {
            sb.append("\"occupation\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(occupation)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"occupation\":"); sb.append("null");sb.append(",");
        }
        if (phone != null) {
            sb.append("\"phone\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(phone)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"phone\":"); sb.append("null");sb.append(",");
        }
        if (fax != null) {
            sb.append("\"fax\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(fax)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"fax\":"); sb.append("null");sb.append(",");
        }
        if (address != null) {
            sb.append("\"address\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(address)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"address\":"); sb.append("null");sb.append(",");
        }
        if (addressNumber != null) {
            sb.append("\"addressNumber\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(addressNumber)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"addressNumber\":"); sb.append("null");sb.append(",");
        }
        if (postalCode != null) {
            sb.append("\"postalCode\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(postalCode)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"postalCode\":"); sb.append("null");sb.append(",");
        }
        if (city != null) {
            sb.append("\"city\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(city)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"city\":"); sb.append("null");sb.append(",");
        }
        if (prefecture != null) {
            sb.append("\"prefecture\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(prefecture)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"prefecture\":"); sb.append("null");sb.append(",");
        }
        if (country != null) {
            sb.append("\"country\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(country)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"country\":"); sb.append("null");sb.append(",");
        }
        if (email != null) {
            sb.append("\"email\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(email)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"email\":"); sb.append("null");sb.append(",");
        }
        if (webSite != null) {
            sb.append("\"webSite\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(webSite)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"webSite\":"); sb.append("null");sb.append(",");
        }
        if (database != null) {
            sb.append("\"database\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(database)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"database\":"); sb.append("null");sb.append(",");
        }
        if (applicationUrl != null) {
            sb.append("\"applicationUrl\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(applicationUrl)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"applicationUrl\":"); sb.append("null");sb.append(",");
        }
        if (invoicingMethod != null) {
            sb.append("\"invoicingMethod\":"); sb.append(invoicingMethod);sb.append(",");
        } else {
            sb.append("\"invoicingMethod\":"); sb.append("null");sb.append(",");
        }
        if (companyVat != null) {
            sb.append("\"companyVat\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(companyVat)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"companyVat\":"); sb.append("null");sb.append(",");
        }
        if (billingCompId != null) {
            sb.append("\"billingCompId\":"); sb.append(billingCompId);sb.append(",");
        } else {
            sb.append("\"billingCompId\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (userAutoInactivate != null) {
            sb.append("\"userAutoInactivate\":"); sb.append(userAutoInactivate);sb.append(",");
        } else {
            sb.append("\"userAutoInactivate\":"); sb.append("null");sb.append(",");
        }
        if (loginPolicy != null) {
            sb.append("\"loginPolicy\":"); sb.append(loginPolicy);sb.append(",");
        } else {
            sb.append("\"loginPolicy\":"); sb.append("null");sb.append(",");
        }
        if (legalNameShort != null) {
            sb.append("\"legalNameShort\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(legalNameShort)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"legalNameShort\":"); sb.append("null");sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    Primary Key
    */
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    /*
    Κωδικός Φορέα
    */
    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
    /*
    Α.Φ.Μ. Φορέα (unique)
    */
    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }
    /*
    Επωνυμία Φορέα
    */
    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }
    /*
    Δ.Ο.Υ. Φορέα
    */
    public String getTaxOffice() {
        return taxOffice;
    }

    public void setTaxOffice(String taxOffice) {
        this.taxOffice = taxOffice;
    }
    /*
    Δραστηριότητα Φορέα
    */
    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }
    /*
    Τηλέφωνο Φορέα
    */
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    /*
    ΦΑΞ Φορέα
    */
    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }
    /*
    Διεύθυνση Φορέα
    */
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    /*
    Αριθμος Διεύθυνσης Φορέα
    */
    public String getAddressNumber() {
        return addressNumber;
    }

    public void setAddressNumber(String addressNumber) {
        this.addressNumber = addressNumber;
    }
    /*
    Τ.Κ. Διεύθυνσης Φορέα
    */
    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }
    /*
    Πόλη Διεύθυνσης Φορέα
    */
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    /*
    */
    public String getPrefecture() {
        return prefecture;
    }

    public void setPrefecture(String prefecture) {
        this.prefecture = prefecture;
    }
    /*
    Χώρα Διεύθυνσης Φορέα
    */
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    /*
    e-mail Φορέα
    */
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    /*
    web site Φορέα
    */
    public String getWebSite() {
        return webSite;
    }

    public void setWebSite(String webSite) {
        this.webSite = webSite;
    }
    /*
    */
    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }
    /*
    */
    public String getApplicationUrl() {
        return applicationUrl;
    }

    public void setApplicationUrl(String applicationUrl) {
        this.applicationUrl = applicationUrl;
    }
    /*
    Τροπος Τιμολογησης του Subscriber στους παραγωγους (1=Με την εφαρμογη Billing της Neuropublic,2=Με δικη του εφαρμογη)
    */
    public Short getInvoicingMethod() {
        return invoicingMethod;
    }

    public void setInvoicingMethod(Short invoicingMethod) {
        this.invoicingMethod = invoicingMethod;
    }
    /*
    Α.Φ.Μ. Εταιρείας Φορέα
    */
    public String getCompanyVat() {
        return companyVat;
    }

    public void setCompanyVat(String companyVat) {
        this.companyVat = companyVat;
    }
    /*
    Εσωτερικος Αριθμος Εταιρειας που θα τιμολογει (NeuroPublic η GAIA Επιχειρειν)
    */
    public Integer getBillingCompId() {
        return billingCompId;
    }

    public void setBillingCompId(Integer billingCompId) {
        this.billingCompId = billingCompId;
    }
    /*
    ΑΑ ενημέρωσης εγγραφής (πεδίο συστήματος για τον έλεγχο κλειδώματος της εγγραφής)
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    Ημερομηνία εισαγωγής της εγγραφής
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    Χρήστης εισαγωγής της εγγραφής
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    Ημερομηνία τελευταίας μεταβολής της εγγραφής
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    Χρήστης τελευταίας μεταβολής της εγγραφής
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    Ένδειξη Αυτόματης Απενεργοποίησης Χρηστών Φορέα
    */
    public Boolean getUserAutoInactivate() {
        return userAutoInactivate;
    }

    public void setUserAutoInactivate(Boolean userAutoInactivate) {
        this.userAutoInactivate = userAutoInactivate;
    }
    /*
    Πολιτική Σύνδεσης χρήστη στις εφαρμογές (
    null ή 0: Η εφαρμογή πρέπει να υπάρχει στον πίνακα των εφαρμογών χρήστη <user_modules>
    1: Οι εφαρμογές του φορέα κληρονομούνται στον χρήστη (δεν απαιτείται η ύπαρξη της εφαρμογής στον πίνακα user_modules)
    )
    */
    public Integer getLoginPolicy() {
        return loginPolicy;
    }

    public void setLoginPolicy(Integer loginPolicy) {
        this.loginPolicy = loginPolicy;
    }
    /*
    Επωνυμία Φορέα (Σύντομη)
    */
    public String getLegalNameShort() {
        return legalNameShort;
    }

    public void setLegalNameShort(String legalNameShort) {
        this.legalNameShort = legalNameShort;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public String getRowKey() {
        return id.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return id;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(id);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.rtlEntities.RTL_Subscriber[ id=" + id + " ]";
    }

    @Override
    public String getEntityName() {
        return "RTL_Subscriber";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
    setUsrinsert(userName);
    setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
    setUsrupdate(userName);
    setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RTL_Subscriber)) {
            return false;
        }
        RTL_Subscriber other = (RTL_Subscriber) object;
        if (this.id == null || other.id == null) 
            return false;
        
        return this.id.equals(other.id);
    }
}
