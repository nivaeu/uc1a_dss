package gr.neuropublic.mutil.base;

import java.util.concurrent.TimeUnit;

public class ExecFuture {
	public ExecFuture(Process p) {
		this.p = p;
	}

	private Process p;
	
	public boolean hasFinished() {
		try {
			p.exitValue();
			return true;
		} catch (IllegalThreadStateException e) {
			return false;
		}
	}
	public int waitAndReturnExitCode() {
		try {
			while (!hasFinished()) {
				TimeUnit.MILLISECONDS.sleep(250);
			}
			return p.exitValue();
		} catch (Exception e) {
			throw new ExceptionAdapter(e);
		}
	}	
}
