package gr.neuropublic.mutil.base;

import java.lang.reflect.Type;

import com.google.gson.JsonParseException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class JsonProvider {
	public static class JsonParseException extends RuntimeException {
		public String offendingString = null;
		public JsonParseException(String offendingString) {
			this.offendingString = offendingString;
		}
		
	}
	private static final Gson gson = new GsonBuilder().serializeNulls().create();

	public static String toJson(Object o) {
		return gson.toJson(o);
	}
	
	public static String toJson(Object o, Type t) {
		return gson.toJson(o, t);
	}
	
	  public static <T> T fromJson(String json, Class<T> classOfT) throws JsonProvider.JsonParseException{
		  return fromJson(gson, json, classOfT);
	  }
	  
	  public static <T> T fromJson(Gson gson, String json, Class<T> classOfT) throws JsonProvider.JsonParseException{
		  try {
		    T target = (T) gson.fromJson(json, (Type) classOfT);
		    return target;
		  }
		  catch (JsonParseException jse) {
			  jse.printStackTrace();
			  throw new JsonParseException(json);
		  }
	  }


	
	  public static <T> T fromJson(String json, Type t) throws JsonProvider.JsonParseException{
		  try {
		    T target = (T) gson.fromJson(json, t);
		    return target;
		  }
		  catch (JsonParseException jse) {
			  jse.printStackTrace();
			  throw new JsonParseException(json);
		  }
	  }
	  
	  

}
