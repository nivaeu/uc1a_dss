package gr.neuropublic.mutil.base;

import java.util.Random;

public class OneIn {

    private Random r;

    public OneIn() {
        this.r = new Random(System.currentTimeMillis());
    }

    public boolean value(int n) {
        return (0 == r.nextInt(n));
    }
}