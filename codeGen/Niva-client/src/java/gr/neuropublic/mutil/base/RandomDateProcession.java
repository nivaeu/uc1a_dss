package gr.neuropublic.mutil.base;

import java.util.Random;
import java.util.Date;

import gr.neuropublic.mutil.base.RandomProcession;

public class RandomDateProcession {

    RandomProcession rp;
    public RandomDateProcession(Date from, Date to, int numOfTakes) {
        rp = new RandomProcession(from.getTime(), to.getTime(), numOfTakes); 
    }

    public Date take() {
        return new Date(rp.take());
    }

}