package gr.neuropublic.mutil.base;

import java.util.Arrays;
import java.util.List;
import java.util.Map;



public class HTTPResponse {
	public Integer						status;
	public Integer 						contentLength;
	public boolean						connectionClose;
	public HTTPResponseChunkedInfo		chunkedInfo;
	public Integer						contentBytesRemaining;
	
	public int remainingBytesToBeRead() {
		if 			(contentBytesRemaining	!=null) 	return contentBytesRemaining;
		else 	if 	(chunkedInfo			!=null) 	return chunkedInfo.lastChunk?0:Integer.MAX_VALUE;
		else    if 	((status!=null) && (status==304)) 	return 0; // Integer.MAX_VALUE; // example header #1
		else    if  ((status!=null) && (status==204))	return 0; // Integer.MAX_VALUE; // example header #3
		else    if  ((status!=null) && (status==501))	return 0; // Integer.MAX_VALUE; // example header #4		
		else 	if  ((contentBytesRemaining == null) && (connectionClose)) return 0; // example header #2
		else	throw new RuntimeException("I can't pronounced remaining bytes on the following response header:\r\n"+this);
	}
	
	public HTTPResponse(Integer status, Integer contentLength,
			boolean connectionClose, HTTPResponseChunkedInfo chunkedInfo,
			Integer contentBytesRemaining) {
		super();
		this.status = status;
		this.contentLength = contentLength;
		this.connectionClose = connectionClose;
		this.chunkedInfo = chunkedInfo;
		this.contentBytesRemaining = contentBytesRemaining;
	}
	@Override
	public String toString() {
		return "HTTPResponse [status=" + status + ", contentLength="
				+ contentLength + ", connectionClose=" + connectionClose
				+ ", chunkedInfo=" + chunkedInfo + ", contentBytesRemaining="
				+ contentBytesRemaining + "]";
	}

	/*
	// example header #1
	HTTP/1.1 304 Use local copy
	Server: Sun-Java-System-Web-Server/7.0
	Date: Fri, 17 Feb 2012 13:17:36 GMT
	Etag: "47cf-4b7aebe1"

	<-end of buffer
	 */
	
	/*
	// example header #2
HTTP/1.0 200 OK
P3P: CP="CURa ADMa DEVa PSAo PSDo OUR BUS UNI PUR INT DEM STA PRE COM NAV OTC NOI DSP COR"
Pragma: no-cache
Cache: expires=0
Expires: 0
Content-type: text/html
Connection: close

<html><head><title>ADMAN : Invalid Request</title></head><body style="font-family: georgia; font-size: 16px;"><p><h1>Invalid ADMAN HTTP Request</h1><p>ADMAN webspace markup code invalid. Please contact <a href="http://adman.gr/">support</a> further information.<br />Internal code : <strong>4</strong></p><p><a href="http://adman.gr/">ADMAN</a> - Phaistos Networks, S.A.</p></body></html>
<-end of buffer
	 */
	
	/*
	// example header #3
HTTP/1.1 204 No Content
Server: nginx/0.8.54
Date: Mon, 20 Feb 2012 11:10:43 GMT
Connection: keep-alive

<-end of buffer
	 */
	
	/*
	// example header #4
HTTP/1.0 501 Not Implemented

<-end of buffer
	 */
}
