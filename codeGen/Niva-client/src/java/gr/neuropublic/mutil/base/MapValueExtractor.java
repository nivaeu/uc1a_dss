package gr.neuropublic.mutil.base;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import static gr.neuropublic.mutil.base.Util.panicIfNull;

public class MapValueExtractor {
	
	public static <K, V> ArrayList<V> extract (Map<K, V> mapOfKsToVs, Collection<K> keysWhoseValuesToExtract) {
		return extract(mapOfKsToVs, keysWhoseValuesToExtract, true);
	}
	
	public static <K, V> ArrayList<V> extract (Map<K, V> mapOfKsToVs, Collection<K> keysWhoseValuesToExtract, boolean panicIfNotFound) {
		ArrayList<V> retValue  = new ArrayList<V>(keysWhoseValuesToExtract.size());
		for (K k : keysWhoseValuesToExtract) {
			V value = mapOfKsToVs.get(k);
			panicIfNull(value);
			retValue.add(mapOfKsToVs.get(k));
		}
		return retValue;
	}

}
