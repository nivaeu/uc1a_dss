package gr.neuropublic.mutil.base;

/**
Allows measuring the start and stop time and (hence the elapsed time) of
some event. ("event" meaning something of interest, for example, a method
call). Can be started/stopped repeatedly. (cumulative time across all such
start/stops are available via the {@link #cumulativeTime} method).
<p>
All times are in <i>milliseconds</i>. 
<p>
Thread Safety: This class is <b>not</b> threadsafe and it's method
do <b>not</b> acquire any locks to reduce any time skew due to
lock acquisition. Multiple threads should use separate Watch
objects or alternatively, higher level synchronization.
<p>
Note: This class <i>used</i> to be called "Timer" but changed to Watch to 
avoid an annoying name conflict with <code>java.util.Timer</code>

@author     Hursh
@version 	1.0, 10/19/2001
@see		NanoWatch	A higher nano-second precision watch.
*/
public class Watch
{
			String 		myname;
volatile	boolean 	running = false;
			long 		startTime = -1;
			long 		stopTime;
			long		cumulativeTime;

public Watch(String name) {
	this.myname = name;
	}

public Watch() {
	this("DefaultWatch/" + Thread.currentThread().getName());
	}

/** 
Start measuring time. Call this method just before calling the
method/code to be instrumented. This method does <b>not</b> reset the
Watch, so the reset method should be called before calling this method
<b>again</b>. Returns <tt>this</tt> so we can say:
<blockquote>
<tt>Watch w = new Watch().start()</tt>
</blockquote>
*/
public Watch start() {
	startTime = System.currentTimeMillis();	
	running = true;
	return this;
	}
	
/** Stop measuring the time */
public void stop() {
	stopTime = System.currentTimeMillis();
	cumulativeTime += (stopTime - startTime);
	running = false;
	}

/** 
Returns the time elapsed since the Watch was started. If the watch was
started and stopped, returns the time (in milliseconds) between the 
start/stop interval.

@throws RuntimeException	if the watch was never started before calling
							this method.
*/
public long time() 
	{
	if (startTime == -1)
		throw new RuntimeException("You need to start the watch at least once before calling this method");
		
	if (running)
		return System.currentTimeMillis() - startTime;
	
	return	stopTime - startTime;
	}

/** 
This method is an alias for {@link #time} method.
*/
public long getTime() 
	{
	return time();
	}

/** 
Useful for showing the elapsed time in seconds. Intervals between,
(<code>0 - 499</code> milliseconds are rounded down and
<code>500 - 999</code> milliseconds are rounded up).
*/
public long timeInSeconds() 
	{
	final long time = getTime();
	
	if (time < 500)	return 0;
	if (time <= 1000) return 1;
	
	long quotient  = time / 1000;
	long remainder = time % 1000;
	return quotient + ((remainder < 500) ? 0 : 1);
	}

/** 
This method is an alias for {@link #timeInSeconds} method.
*/
public long getTimeInSeconds() 
	{
	return timeInSeconds();
	}

/**
Useful in NanoWatch and other subclasses. Not really useful here, returns
the same value as {@link #getTime}/{@link #time} functions instead. 
<p>
This method is defined here so all watches have a common interface and
any instance can be bound to a variable of type <code>Watch</code>.
*/
public double getTimeInMillis()
	{
	return getTime() * 1.0D;
	}

/** 
Returns the total time recorded by this Watch (across several starts/stops)
*/
public long cumulativeTime() 
	{
	if (! running) 
		return cumulativeTime;
	
	return cumulativeTime +	(System.currentTimeMillis() - startTime);
	}

/** 
Reset all values to zero. This method should be called
before this object is used <b>again</b>.
*/
public void reset() 
	{
	startTime = stopTime =  0;
	}

/** Is the Watch currently running ? */
public boolean isRunning() {
	return running;
	}

/** Get the start time (in milliseconds since Jan 1, 1970)*/
protected long getStart() {
	return startTime;
	}

/** Get the stop time, (in milliseconds since Jan 1, 1970) */
protected long getStop() {
	return stopTime;
	}

/** 
Describes the current state of this watch. The exact details of said
description are unspecified and subject to change.
*/	
public String toString() 
	{
	String str = myname;

	str += ": Cum.Time=[" + cumulativeTime() + " ms]" + 
			"; Start=[" + startTime + "]";
	
	if (! running) 
		str += 	"; Stop=[" + stopTime + "]";
	else 
		str += "; Elapsed=[" + time() + " ms]";
		
	return str;
	}

public static void main(String[] args)
	{
	Watch t1 = new Watch("Watch 1");
	t1.start();
	
	new Thread(new Runnable() { 
		public void run() {
			try { 
				Watch t2 = new Watch();
				t2.start(); Thread.currentThread().sleep(20); t2.stop(); 
				System.out.println("t2.toString():" + t2);
				} 
			catch (Exception e) { e.printStackTrace(); }
			} 
		}).start();
	
	//following should return -1
	System.out.println("Watch 1, total time taken:" + t1.time());

	System.out.println("Watch 1, time=" + t1.time());
	System.out.println("Watch 1, before-being-stopped, toString():" + t1);
	t1.stop();
	System.out.println("Watch 1, is running ? " + t1.isRunning() );
	System.out.println("Watch 1, after-being-stopped, toString():" + t1);
	System.out.println("Watch 1, elapsed time:" + t1.time());
	System.out.println("Watch 1, cumulative time taken:" + t1.cumulativeTime());
	System.out.println("Watch 1, elapsed time:" + t1.time());

	new Thread(new Runnable() { 
		public void run() {
			try { 
				Watch t2 = new Watch();
				t2.start(); 
				Thread.currentThread().sleep(250); 
				t2.stop(); 
				System.out.println("After sleeping 250ms: time in seconds:  " + t2.getTimeInSeconds());
				t2.start();
				Thread.currentThread().sleep(500); 
				System.out.println("After sleeping 750ms: time in seconds:  " + t2.getTimeInSeconds());
				Thread.currentThread().sleep(500); 
				System.out.println("After sleeping 1250ms: time in seconds: " + t2.getTimeInSeconds());
				Thread.currentThread().sleep(500); 
				System.out.println("After sleeping 1750ms: time in seconds: " + t2.getTimeInSeconds());
				Thread.currentThread().sleep(1000); 
				System.out.println("After sleeping 2750ms: time in seconds: " + t2.getTimeInSeconds());
				} 
			catch (Exception e) { e.printStackTrace(); }
			} 
		}).start();
	}

} //~class Watch
