package gr.neuropublic.mutil.base;

import java.util.Random;
import java.security.SecureRandom;

public class SecureRandomString {

    private Random random ;
    private String symbols;    

    public SecureRandomString(String symbols) {
        random = new SecureRandom();
        random.setSeed(System.currentTimeMillis());
        this.symbols = symbols;
    }

    public String get(int length) {
        char[] buf = new char[length];
        for (int i = 0 ; i < length ; i++)
            buf[i] = symbols.charAt(random.nextInt(symbols.length()));
        return new String(buf);
    }


}