package gr.neuropublic.mutil.base;

import java.util.Arrays;
import java.util.Collections;



public class NumericUtil {
	
	// create an int array out of a float array by keeping
	// the proportions and ensuring that the sum of the elements
	// in the int array equals FIXEDSUM
	public static int [] proportion (float[] fs, int FIXEDSUM) {
		Util.panicIf(Util.min(fs)<0);
		Util.panicIf(FIXEDSUM<=0);
		// float[] sortedFs = Arrays.copyOf(fs, fs.length);
		float sum = Util.sum(fs);
		int remaining = FIXEDSUM;
		int[] retValue = new int[fs.length];
		int i_ = 0;
		// Arrays.sort(fs);
		boolean lastLoop = false;
		for (float f : fs) {
			if (lastLoop) break;
			int i = (int) ((f*FIXEDSUM) / sum);
			if (i >= remaining) {
				i = remaining;
				lastLoop = true;
			}
			retValue[i_++] = i;
			remaining -= i;
			if (remaining == 0)
				lastLoop = true;
		}
		retValue[Util.first_max_index(fs)] += remaining;
		return retValue;
	}
	
	public static int [] proportion (long[] fs, int FIXEDSUM) {
		return proportion(Util.toFloat(fs), FIXEDSUM);
	}
	
	/*
	public static int [] proportion (long[] fs, int FIXEDSUM) {
		Util.panicIf(Util.min(fs)<0);
		Util.panicIf(FIXEDSUM<=0);
		// float[] sortedFs = Arrays.copyOf(fs, fs.length);
		float sum = Util.sum(fs);
		int remaining = FIXEDSUM;
		int[] retValue = new int[fs.length];
		int i_ = 0;
		// Arrays.sort(fs);
		boolean lastLoop = false;
		for (float f : fs) {
			if (lastLoop) break;
			int i = (int) ((f*FIXEDSUM) / sum);
			if (i >= remaining) {
				i = remaining;
				lastLoop = true;
			}
			retValue[i_++] = i;
			remaining -= i;
			if (remaining == 0)
				lastLoop = true;
		}
		retValue[Util.first_max_index(fs)] += remaining;
		return retValue;
	}*/
	
	public static boolean sameLength(int [][] a) {
		int previousLength = -1;
		for (int i = 0 ; i < a.length ; i++) {
			int l = a[i].length;
			if (previousLength==-1)
				previousLength = l;
			else if (l!=previousLength)
				return false;
		}
		return true;
	}
	

	
	
	
	
}
