package gr.neuropublic.mutil.base;

import java.util.logging.Logger;

public class LogCopyStreamTracker implements ICopyStreamTracker {
	
	private Logger l;
	private long total;
	private int onceEverySoManyThousandthParts;
	private int partReached = 0;
	private boolean atLeastOneMessage = false;
	private long lastMessageEmitedAt = 0;
	private static final int ONE_THOUSAND = 1000;
	
	public LogCopyStreamTracker(Logger l, long total, int onceEverySoManyThousandthParts) {
		this.l = l;
		this.total = total;
		this.onceEverySoManyThousandthParts = onceEverySoManyThousandthParts;
	}

	@Override
	public void track(long numOfBytesCopied) {
		if ((partReached==0) && (!atLeastOneMessage) && (numOfBytesCopied!=0)) {
			l.finest("first "+numOfBytesCopied+" byte copied");
			atLeastOneMessage = true;
		}
		if ( (numOfBytesCopied-lastMessageEmitedAt)*ONE_THOUSAND >= total*onceEverySoManyThousandthParts) {
			l.finest(++partReached +"th part ("+onceEverySoManyThousandthParts+"/"+ONE_THOUSAND+") reached ("+numOfBytesCopied+" bytes copied out of "+total+")");
			lastMessageEmitedAt = numOfBytesCopied;
		}

	}
	
	public static void main(String []args) {
		Logger l = Logger.getLogger(LogCopyStreamTracker.class.getName());
		final long HOW_MANY = 39238978; 
		LogCopyStreamTracker t = new LogCopyStreamTracker(l, HOW_MANY, 200);
		for (long i = 0 ; i < HOW_MANY; i++)
			t.track(i);
	}

}
