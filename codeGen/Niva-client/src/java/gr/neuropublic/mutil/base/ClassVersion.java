package gr.neuropublic.mutil.base;

import java.io.*;

public class ClassVersion {
    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            System.out.printf("usage is: java %s <filename of .class file to report on> \n",
                              ClassVersion.class.getName());
        } else checkClassVersion(args[0]);
    }

    private static void checkClassVersion(String filename)
        throws IOException
    {
        DataInputStream in = new DataInputStream
            (new FileInputStream(filename));

        int magic = in.readInt();
        if(magic != 0xcafebabe) {
            System.out.println(filename + " is not a valid class!");;
        }
        int minor = in.readUnsignedShort();
        int major = in.readUnsignedShort();
        System.out.println(filename + ": " + major + " . " + minor);
        in.close();
    }
}