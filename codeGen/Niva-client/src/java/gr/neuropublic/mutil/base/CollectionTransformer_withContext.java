package gr.neuropublic.mutil.base;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CollectionTransformer_withContext<K, V> {
	private V v;
	public CollectionTransformer_withContext(V v) {
		this.v = v;
	}
	public void dispatch(Collection<K> collectionOfV, IVisitorWithContext<K, V> transformer) {
		for (K k : collectionOfV)
				transformer.visit(k, v);
	}
}
