// Copyright (c) 2001 Hursh Jain (http://www.mollypages.org) 
// The Molly framework is freely distributable under the terms of an
// MIT-style license. For details, see the molly pages web site at:
// http://www.mollypages.org/. Use, modify, have fun !

package gr.neuropublic.mutil.base;

/**
Argcheck failure handlers should implement this interface.
**/
public interface IArgcheckFailHandler
{
/**
This method will be called on by the {@link fc.app.Checker} class in
case of an Argcheck failure. See {@link fc.app.Argcheck}.
<p>
Therefore, implementations can do neat things like writing 
detailed thread/stack information or even paging/sending email.
After doing other handling, Implementations should end this
method by throwing a RuntimeException (unless there is a very
strong reason not to).
**/
public void handle(String msg);
}