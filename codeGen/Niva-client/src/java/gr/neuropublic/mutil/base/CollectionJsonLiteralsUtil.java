package gr.neuropublic.mutil.base;

import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import com.google.gson.reflect.TypeToken;

public class CollectionJsonLiteralsUtil {
	
	public static Map<String, Pair<String, List<String>>> get_mS_pS_lS(String jsonLiteral) {
		final Type templateType = new TypeToken<Map<String, Pair<String, List<String>>>>() {}.getType();
		return JsonProvider.fromJson(jsonLiteral, templateType);
	}

}
