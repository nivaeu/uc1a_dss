package gr.neuropublic.mutil.base;

import java.util.regex.Matcher;
import java.util.regex.Pattern;



public class RegExpUtil {
	
	public static String extract (Pattern pattern, String line, int group) {
		Matcher matcher = pattern.matcher(line);
		if (matcher.matches())
			return matcher.group(group);
		else
			return null;
	}	
	

	public static boolean matches(String aString, String regExp) {
			Pattern filteringPattern   = Pattern.compile(regExp, Pattern.DOTALL);
			return filteringPattern.matcher(aString).matches();
	}
	
	private static final String PROTOCOL = "(\\w+)";
	
    private static final String IPADDRESS_PATTERN = 
    		"(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
    		"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
    		"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
    		"([01]?\\d\\d?|2[0-4]\\d|25[0-5]))";
	
    private static final String PORT_PATTERN = "(:(\\d{1,5}))?";
    
	private static final String URL_PATTERN = "^"+PROTOCOL+"://"+IPADDRESS_PATTERN+PORT_PATTERN+"$";
	
	private static final Pattern filteringPattern   = Pattern.compile(URL_PATTERN, Pattern.DOTALL);
	
	public static String getProtocol(String url) {
		Matcher matcher = filteringPattern.matcher(url);
		if (matcher.matches())
			return matcher.group(1);
		else
			return null;
	}
	
	public static String getIPv4(String url) {
		Matcher matcher = filteringPattern.matcher(url);
		if (matcher.matches())
			return matcher.group(2);
		else
			return null;
	}
	
	public static String getPort(String url) {
		Matcher matcher = filteringPattern.matcher(url);
		if (matcher.matches())
			return matcher.group(8);
		else
			return null;
	}
	
	public static void sanity () {
		CharSequence inputStr = "abbabcd";
		String patternStr = "(a(b*))+(c*)";

		// Compile and use regular expression
		Pattern pattern = Pattern.compile(patternStr);
		Matcher matcher = pattern.matcher(inputStr);
		boolean matchFound = matcher.find();

		if (matchFound) {
		    // Get all groups for this match
		    for (int i=0; i<=matcher.groupCount(); i++) {
		        String groupStr = matcher.group(i);
		        System.out.println("group "+i+" is: "+groupStr);
		    }
		}
	}
	
	public static void main(String args[]) {
		sanity();
		System.out.println(getProtocol	("coast://192.134.33.33:234"));
		System.out.println(getIPv4		("coast://192.134.33.33:234"));
		System.out.println(getPort		("coast://192.134.33.33:234"));
		
	}

}
