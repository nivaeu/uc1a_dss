package gr.neuropublic.mutil.base;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class EasyFileWriter extends FileWriter {

	public EasyFileWriter(File file) throws IOException {
		super(file);
	}
	private static final String ENDL = System.getProperty("line.separator"); 
	public void writeln(String line)  {
		try {
			write(line+ENDL);
		}
		catch (Exception e) {
			throw new ExceptionAdapter(e);
		}
	}
}
