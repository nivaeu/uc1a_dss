package gr.neuropublic.mutil.base;

import java.util.List;
import java.io.File;

import java.util.logging.Logger;

public class ConditionalSpawner {

    private static final String CTRL_FILE_SOURCES = "ctrlFileSources";
    private static final String CTRL_FILE_TARGET  = "ctrlFileTarget";
    private static final String INVOCATION        = "invocation";

    public static List<File> filesFromColumnSeparatedString(String fileNames) {
        List<String> fileNamesList = Array2List.trsnfrm(fileNames.split(File.pathSeparator));
        return ListUtil.transform(fileNamesList, new ITransformer_1_to_1<String, File>() {
            @Override
            public File transform (String name) {
                return new File(name);
            }}
        );
    }


    public static void main(String args[]) throws Exception {
        Logger l = Logger.getLogger(ConditionalSpawner.class.getName());
        Args myargs = new Args(args);
        myargs.setUsage(String.format("expected arguments are: -%s -%s -%s",
                                      CTRL_FILE_SOURCES, CTRL_FILE_TARGET, INVOCATION));
        if (myargs.getFlagCount() < 3)
            myargs.showError();
        String ctrlFileSourceNames= myargs.getRequired(CTRL_FILE_SOURCES);
        String ctrlFileTargetName = myargs.getRequired(CTRL_FILE_TARGET);
        String invocation         = myargs.getRequired(INVOCATION);
        l.info("arguments read as follows:");
        l.info(String.format("%s = %s", CTRL_FILE_SOURCES, ctrlFileSourceNames));
        l.info(String.format("%s = %s", CTRL_FILE_TARGET , ctrlFileTargetName));
        l.info(String.format("%s = %s", INVOCATION, invocation));
        List<File> ctrlFiles = filesFromColumnSeparatedString(ctrlFileSourceNames);
        FileUtil.panicIfNotRegularFiles(ctrlFiles);
        int _i = 0 ;
        for (File f : ctrlFiles)
            l.info(String.format("source control file %d is: %s", _i++, f.getAbsolutePath()));
        long sourceMillis = FileUtil.latestModtime(ctrlFiles);
        l.info(String.format("latest control mod time is %d", sourceMillis));
        File targetFile = new File(ctrlFileTargetName);
        long targetMillis = targetFile.exists()?targetFile.lastModified():Long.MIN_VALUE;
        if (sourceMillis > targetMillis) {
            String[] invocation_tokens = invocation.split(" ");
            ExecFuture execFuture = CommandLineUtil.exec(l, invocation_tokens);
            l.info(String.format("spawned process returned an exit code of: %d",
                                execFuture.waitAndReturnExitCode()));
        } else l.info("not spawning.");
    }
}