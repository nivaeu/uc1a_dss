package gr.neuropublic.mutil.base;

import org.hyperic.sigar.CpuPerc;
import org.hyperic.sigar.Mem;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;


public class SigarUtil_MperdExample {
	
	public static void output(CpuPerc cpu) {
		System.out.println(100-cpu.getWait());
	}
	public static void main(String ... args) throws SigarException, InterruptedException {
		Sigar sigar = new Sigar();
		org.hyperic.sigar.CpuInfo[] infos = sigar.getCpuInfoList();
        CpuPerc[] cpus = sigar.getCpuPercList();

        org.hyperic.sigar.CpuInfo info = infos[0];
        long cacheSize = info.getCacheSize();
        System.out.println("Vendor........." + info.getVendor());
        System.out.println("Model.........." + info.getModel());
        System.out.println("Mhz............" + info.getMhz());
        System.out.println("Total CPUs....." + info.getTotalCores());
        if ((info.getTotalCores() != info.getTotalSockets()) ||
            (info.getCoresPerSocket() > info.getTotalCores()))
        {
            System.out.println("Physical CPUs.." + info.getTotalSockets());
            System.out.println("Cores per CPU.." + info.getCoresPerSocket());
        }

        if (cacheSize != Sigar.FIELD_NOTIMPL) {
            System.out.println("Cache size...." + cacheSize);
        }
        System.out.println("");


        while (true) {
        	Thread.sleep( 1000 );
        	CpuPerc cpuPerc = sigar.getCpuPerc();
        	Mem mem			= sigar.getMem();
        	String names [] = sigar.getNetInterfaceList();
        	/* System.out.println (StringUtils.join(names));
        	for (String name : names) {
        		NetInterfaceConfig niConfig = sigar.getNetInterfaceConfig(name);
        	*/
        	System.out.format("%5.3f %5.3f%n", cpuPerc.getUser()+cpuPerc.getSys(), mem.getFreePercent());
        	
        }
    }
}
