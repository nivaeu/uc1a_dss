package gr.neuropublic.mutil.base;

public interface ITransformer_1_to_1<K, V> {
	public V transform(K k);
}
