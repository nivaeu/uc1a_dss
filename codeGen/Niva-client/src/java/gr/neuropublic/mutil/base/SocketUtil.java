package gr.neuropublic.mutil.base;

import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketUtil {
	public static void printInformation(Socket socket, PrintStream out) {
		out.println("/--------- socket information start ---------------\\");
		out.println("|  isBound()          = "+socket.isBound());
		out.println("|  isConnecter()      = "+socket.isConnected());
		out.println("|  isClosed()         = "+socket.isClosed());
		out.println("|  local  port        = "+socket.getLocalPort());
		out.println("|  local  IP          = "+socket.getLocalAddress().toString());
		out.println("|  remote port        = "+socket.getPort());
		out.println("|  remote IP          = "+socket.getInetAddress().toString());
   try {out.println("|  SO_SNDBUF size     = "+socket.getSendBufferSize());} 		catch (Exception e) {
	    out.println("|  SO_SNDBUF size     = "+e.getMessage());                                          }
   try {out.println("|  SO_RCVBUF size     = "+socket.getReceiveBufferSize());} 		catch (Exception e) {
	    out.println("|  SO_RCVBUF size     = "+e.getMessage());                                          }
   try {out.println("|  socket timeout     = "+socket.getSoTimeout());} 				catch (Exception e) {
	    out.println("|  socket timeout     = "+e.getMessage());		                                    }
   try {out.println("|  socket linger      = "+socket.getSoLinger());} 				catch (Exception e) {
	    out.println("|  socket linger      = "+e.getMessage());		                                    }
		out.println("\\----------- socket information end---------------/");
	}
	
	public static void printInformation(ServerSocket socket, PrintStream out) {
		out.println("/--------- server socket information start ---------------\\");
		out.println("|  isBound()          = "+socket.isBound());
		out.println("|  isClosed()         = "+socket.isClosed());
		out.println("|  local  port        = "+socket.getLocalPort());
		out.println("|  remote IP          = "+socket.getInetAddress().toString());
   try {out.println("|  socket timeout     = "+socket.getSoTimeout());} 				catch (Exception e) {
	    out.println("|  socket timeout     = "+e.getMessage());		                                    }
		out.println("\\----------- server socket information end---------------/");
	}
	
	public static void closeSequence(Socket socket) throws IOException {
		try {socket.shutdownInput	();} catch (Exception e) {}
		try {socket.shutdownOutput	();} catch (Exception e) {}
		try {socket.close			();} catch (Exception e) {}
	}
}
