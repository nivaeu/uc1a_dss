package gr.neuropublic.mutil.base;

public interface ISimpleAcceptor<V> {
	public boolean accept(V v);
}
