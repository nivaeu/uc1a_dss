package gr.neuropublic.mutil.base;

import java.util.HashMap;
import java.util.Map;

public class MapUtil {
	public static long sumValues(Map<?, ? extends Number> map) {
		long retValue = 0 ;
		for (Number n : map.values()) {
			retValue += n.longValue();
		}
		return retValue;
	}
	
	public static double maxValue(Map<?, ? extends Number> map) {
		double maxValue = Double.MIN_VALUE;
		for (Number n : map.values()) {
			maxValue = Util.max(maxValue, n.doubleValue());
		}
		return maxValue;
	}

	
	public static void main(String ... args) {
		Map<String, Short> mss = new HashMap<>();
		mss.put("a", (short) 1);
		mss.put("b", (short) 2);
		System.out.format ("%4d", sumValues(mss));
		Util.panicIf (sumValues (mss)!=3);
		Util.panicIf (maxValue  (mss)!=2);
		
		Map<Integer, Long> mil = new HashMap<>();
		mil.put(1, (long) 3);
		mil.put(2, (long) 5);
		System.out.format ("%4d", sumValues(mil));
		Util.panicIf( sumValues(mil)!=8);
		Util.panicIf( maxValue(mil)!=5);
	}
}
