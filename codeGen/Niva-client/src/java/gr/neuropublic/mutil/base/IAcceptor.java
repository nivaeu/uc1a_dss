package gr.neuropublic.mutil.base;

public interface IAcceptor<K, V, L> {

	public boolean accept(K k, V v, L l);
}
