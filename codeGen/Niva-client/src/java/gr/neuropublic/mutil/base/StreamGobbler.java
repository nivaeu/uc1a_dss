package gr.neuropublic.mutil.base;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.util.logging.Logger;

public class StreamGobbler extends Thread
{
    InputStream is;
    String type;
    Logger l;
    
    public StreamGobbler(Logger l, InputStream is, String type)
    {
        this.is = is;
        this.type = type;
        this.l = l;
    }
    
    public void run()
    {
        try
        {
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line=null;
            while ( (line = br.readLine()) != null)
                if (l!=null) l.finest(type + ">" + line);    
            } catch (IOException ioe)
              {
                ioe.printStackTrace();  
              }
    }
}
