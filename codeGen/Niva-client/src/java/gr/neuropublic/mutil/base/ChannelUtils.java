package gr.neuropublic.mutil.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.WritableByteChannel;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang.StringUtils;

import java.util.logging.Logger;

public class ChannelUtils {
	
	public static long pipe(ReadableByteChannel in, SocketChannel out) {
		return pipe(in, out, 1024);
	}
	
	public static long pipe(ReadableByteChannel in, SocketChannel out, int buffSize) {
		long bcount = 0;
		ByteBuffer buff = ByteBuffer.allocateDirect(buffSize);
		while (true) {
			int bytesRead = 0;
			buff.clear();
			try {
				bytesRead = in.read(buff);
				buff.flip();
				out.write(buff);
				// System.out.println("piped: "+bytesRead+" bytes in the inner loop");
			} catch (Exception e) {
				throw new ExceptionAdapter(e);
			} finally {
				if (bytesRead==-1) {
					try {out.close();} catch (Exception e) {e.printStackTrace();}
					break;
				}
				else
					bcount += bytesRead;
			}
		}
		return bcount;
	}
	
	public static long pipe (InputStream is, SocketChannel out, int buffSize) {
		ReadableByteChannel isChannel = null;
		try {
			isChannel = Channels.newChannel(is);
			return pipe( isChannel, out, buffSize);
		} catch (Exception e) {
			throw new ExceptionAdapter(e);
		} finally {
			try { isChannel.close(); } catch (Exception e) {e.printStackTrace();}
		}
		
	}
	
	public static long pipe(File f, SocketChannel out, int buffSize) {
		ReadableByteChannel fchannel = null;
		try {
			fchannel = (new FileInputStream(f)).getChannel();
			return pipe( fchannel, out, buffSize);
		} catch (Exception e) {
			throw new ExceptionAdapter(e);
		} finally {
			try { fchannel.close(); } catch (Exception e) {e.printStackTrace();}
		}
	}
	
	public static long pipe(File f, InetSocketAddress out, int buffSize) {
		SocketChannel outchannel = null;
		try {
			outchannel = SocketChannel.open(out);
			return pipe(f, outchannel, buffSize);
		} catch (IOException e) {
			throw new ExceptionAdapter(e);
		} finally {
			try { outchannel.close(); } catch (Exception e) {e.printStackTrace();}
		}
	}
	
	public static long pipe(File f, String host, int port, int buffSize) {
		return pipe(f, new InetSocketAddress(host, port), buffSize);
	}
	
	public static long pipe(SocketChannel in, WritableByteChannel out) {
		return pipe(in, out, 1024); 
	}
	
	public static long pipe(SocketChannel in, WritableByteChannel out, int buffSize) {
		long bcount = 0;
		ByteBuffer buff = ByteBuffer.allocateDirect(buffSize);
		while (true) {
			buff.clear();
			int bytesRead = 0;
			try {
				bytesRead = in.read(buff);
				buff.flip();
				out.write(buff);
			} catch (Exception e) {
				throw new ExceptionAdapter(e);
			} finally {
				if (bytesRead==-1) {
					try {out.close();} catch (Exception e) {e.printStackTrace();}
					break;
				}
				else
					bcount += bytesRead;
			}
		}
		return bcount;
	}
	
	public static long pipe(SocketChannel in, File out, int buffSize) {
		WritableByteChannel outchannel = null;
		try {
			outchannel = (new FileOutputStream(out)).getChannel();
			return pipe(in, outchannel, buffSize);
		} catch (FileNotFoundException e) {
			throw new ExceptionAdapter(e);
		} finally {
			try {outchannel.close();} catch (Exception e) {e.printStackTrace();}
		}
	}
	
	public static long pipe(InetSocketAddress serverSocket, File out, int buffSize) {
		ServerSocketChannel ssc 		= null;
		SocketChannel 		schannel 	= null;
		try {
			ssc = ServerSocketChannel.open();
			ssc.bind(serverSocket);
			schannel = ssc.accept();
			return pipe(schannel, out, buffSize);
		} catch (Exception e) {
			throw new ExceptionAdapter(e);
		} finally {
			try {     ssc.close();} catch (Exception e) {e.printStackTrace();}
			try {schannel.close();} catch (Exception e) {e.printStackTrace();}
		}
	}
	
	public static void main (String args[]) throws ParseException, UnknownHostException {
		Logger l 		= Logger.getLogger(ChannelUtils.class.getName());
		CommandLineParser parser = new BasicParser( );
		Options options = new Options();
		options.addOption("p", "-port", true, "local port to listen to");
		options.addOption("f", "-file", true, "file to pipe data in or out from");
		options.addOption("rip", "-ip", true, "remote ip to send file");
		options.addOption("rp", "-rp", true, "remote port to send file");
		CommandLine commandLine = parser.parse( options, args );
		if (commandLine.getArgList().size()>0) {
			l.info("unrecognized options: "+StringUtils.join(commandLine.getArgList(), ","));
			System.exit(1);
		} else {
			if (commandLine.hasOption("p")) { // server mode
				int port = Integer.valueOf(	commandLine.getOptionValue("p"));
				File f   = new File(		commandLine.getOptionValue("f"));
				l.info("listening to port: "+port+" ready to write to file: "+f.getAbsolutePath());
				Watch w = new Watch ("");
				w.start();
				long b = pipe(new InetSocketAddress(InetAddress.getLocalHost(), port), f, 1024);
				w.stop();
				l.info("piped "+b+" bytes in "+w.cumulativeTime()+" millis");
			} else if (commandLine.hasOption("rp")){ // client mode
				String remoteIp = null;
				if (commandLine.hasOption("rip"))
					remoteIp = commandLine.getOptionValue("rip");
				int port = Integer.valueOf(commandLine.getOptionValue("rp"));
				File f   = new File(		commandLine.getOptionValue("f"));
				InetSocketAddress remote = new InetSocketAddress(remoteIp==null?InetAddress.getLocalHost():InetAddress.getByName(remoteIp), port);
				l.info("ready to pipe file: "+f.getAbsolutePath()+" to remote socket: "+remote.toString());
				Watch w = new Watch("");
				w.start();
				long b = pipe(f, remote, 1024);
				w.stop();
				l.info("piped "+b+" bytes in "+w.cumulativeTime()+" millis");				
			} else {
				l.info("unrecognized options");
				System.exit(2);
			}
		}
		
	}
			 

}
