// Copyright (c) 2001 Hursh Jain (http://www.mollypages.org) 
// The Molly framework is freely distributable under the terms of an
// MIT-style license. For details, see the molly pages web site at:
// http://www.mollypages.org/. Use, modify, have fun !

package gr.neuropublic.mutil.base;

/**
A horizontal HAlign enumeration.

@author hursh jain
**/
public class HAlign
{
public static final HAlign LEFT = new HAlign("left");
public static final HAlign RIGHT = new HAlign("right");
public static final HAlign CENTER = new HAlign("center");
		
public String toString() { return "HAlign="+name; }
private String name;		
private HAlign(String name) { this.name = name; }
}          //~class HAlign