package gr.neuropublic.mutil.base;

import java.util.Collection;

public class CollectionAggregator_with_ITransformer_1_to_1_withContext<K, V> {
	
	private V v;
	public CollectionAggregator_with_ITransformer_1_to_1_withContext(V v) {
		this.v = v;
	}
	
	public V aggregate(Collection<K> ks, ITransformer_1_to_1_withContext<K, V, V> transformer) {
		for (K k: ks) {
			v = transformer.transform(k, v);
		}
		return v;
	}

}
