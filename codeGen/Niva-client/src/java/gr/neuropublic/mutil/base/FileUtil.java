package gr.neuropublic.mutil.base;


import gr.neuropublic.mutil.io.IOUtil;
import static gr.neuropublic.mutil.base.Util.panicIf;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.comparator.NameFileComparator;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;

import org.apache.commons.lang.StringUtils;
import org.mozilla.universalchardet.UniversalDetector;




import com.google.common.base.CharMatcher;

import gr.neuropublic.mutil.logging.IFileLogDispatcherCompressorSynch;




public class FileUtil {

    public static long latestModtime(List<File> files) {
        final Holder<Long> l = new Holder(Long.MIN_VALUE);
        return ListUtil.dispatch(files, new IVisitorReporter<File, Long>() {
            @Override
            public void visit(File f) {
                if (l.get() < f.lastModified())
                    l.set(f.lastModified());
            }
            @Override
            public Long report() {
                return l.get();
            } 
        });
    }

    public static void panicIfNotRegularFiles(List<File> files) {
        ListUtil.dispatch(files, new IVisitor<File>() {
            @Override
            public void visit(File f) {
                panicIfNotRegularFile(f);
            }
        });
    }

    public static void panicIfNotRegularFile(File f) {
        if (f==null) throw new RuntimeException("file is null");
        else if ((!f.exists()) || (!f.isFile()))
            throw new RuntimeException(String.format("file %s does not exist or is not regular file",
                                      f.getAbsolutePath()));
    }


	
	public static IOFileFilter noSuffix() {
		return new IOFileFilter() {
			@Override
			public boolean accept(File dir, String name) {
				int dotPos = StringUtils.indexOf(name, '.');
				return dotPos==-1;
			}

			@Override
			public boolean accept(File file) {
				return file.isFile() && accept(file.getParentFile(), file.getName());
			}
		};
	}
	
	public static List<File> lexicographicallyOrderedFilesWithNoExtension(File path) {
		return orderedFiles(path, noSuffix(), NameFileComparator.NAME_COMPARATOR);
	}
	
	public static List<File> lexicographicallyOrderedFiles(File path, String validExtensions[]) {
		SuffixFileFilter filter = new SuffixFileFilter(validExtensions); 
		return orderedFiles(path, filter, NameFileComparator.NAME_COMPARATOR);
	}
	
	public static List<File> orderedFiles(File path, IOFileFilter filter, Comparator<File> comparator) {
		Collection<File> filesCol = FileUtils.listFiles(path, filter, null);
		List<File> files = new ArrayList<>(filesCol);
		Collections.sort(files, comparator);
		return files;
	}
	
	
	public static boolean contentsHashIdentical(File f1, File f2) {
		return calculateMD5(f1).equals(calculateMD5(f2)) &&
				calculateSHA1(f1).equals(calculateSHA1(f2));
	}
	
    public static String calculateSHA1( File file ) {
    	try {
    		MessageDigest sha1 = MessageDigest.getInstance("SHA1");
    		return calculateHash(sha1, file);
    	} catch (Exception e) {
    		throw new ExceptionAdapter(e);
    	}
    }

    
    public static String calculateMD5( File file ) {
    	try {
	        MessageDigest md5  = MessageDigest.getInstance("MD5");        
	        return calculateHash(md5, file);
    	} catch (Exception e) {
    		throw new ExceptionAdapter(e);
    	}
    }
    
    public static Charset guessEncoding(File file) throws IOException, FileNotFoundException {
    	FileInputStream fis = new FileInputStream(file);
    	Charset retValue = guessEncoding(fis);
    	fis.close();
    	return retValue;
    }
    
    public static Charset guessEncoding(InputStream fis) throws IOException {
    	byte[] buf = new byte[4096];
        UniversalDetector detector = new UniversalDetector(null);
        int nread;
        while ((nread = fis.read(buf)) > 0 && !detector.isDone()) {
          detector.handleData(buf, 0, nread);
        }
        detector.dataEnd();
        String encoding = detector.getDetectedCharset();
        detector.reset();
        if (encoding != null) {
        	System.out.println("detected: "+encoding+" encoding");
        	return Charset.forName(encoding);
        } else {
        	System.out.println("failed to detect encoding");
        	return null;
        }
    }

	
	  public static String calculateHash(MessageDigest algorithm, File file) {
		  try {
	        FileInputStream     fis = new FileInputStream(file);
	        BufferedInputStream bis = new BufferedInputStream(fis);
	        DigestInputStream   dis = new DigestInputStream(bis, algorithm);

	        // read the file and update the hash calculation
	        while (dis.read() != -1);

	        // get the hash value as byte array
	        byte[] hash = algorithm.digest();

	        return byteArray2Hex(hash);
		  } catch (Exception e) {
			  throw new ExceptionAdapter(e);
		  }
	    }

	    public static String byteArray2Hex(byte[] hash) {
	        Formatter formatter = new Formatter();
	        for (byte b : hash) {
	            formatter.format("%02x", b);
	        }
	        return formatter.toString();
	    }

	
	public static File getSystemTempFolder() {
		String tempDir = System.getProperty("java.io.tmpdir");
		File tempDirF = new File(tempDir);
		panicIf(!tempDirF.isDirectory());
		return tempDirF;
	}
	
	public static long getFolderSize(File folder) {
		  panicIf(!folder.isDirectory());
		    long foldersize = 0;
	
		    File[] filelist = folder.listFiles();
		    if (filelist != null)
			    for (int i = 0; i < filelist.length; i++) {
			      if (filelist[i].isDirectory()) {
			        foldersize += getFolderSize(filelist[i]);
			      } else {
			        foldersize += filelist[i].length();
			      }
			    }
		    return foldersize;
	  }

	public static char[] stringToCharArray(String s) {
		return s.toCharArray();
	}
	
	public static String charArrayToString(char[] chars) {
		return String.valueOf(chars);
	}

	
	public static boolean inASCII(byte x) {
		if ((x>=0) && (x<=127)) return true;
		else return false;
	}
	
	public static String readASCIIStringFromInputStream(InputStream is, int numOfCharacters) throws IOException, UnexpectedNonASCIIByte{
		DataInputStream dis = new DataInputStream(is);
		char[] characters = new char[numOfCharacters];
		for (int _i = 0 ; _i < numOfCharacters ; _i++) {
			byte valueRead = dis.readByte();
			if (!inASCII(valueRead)) {
				System.out.println(UnexpectedNonASCIIByte.exceptionMessage(valueRead));
				throw new UnexpectedNonASCIIByte(valueRead);
			}
			characters[_i] = (char) valueRead;
		}
		return charArrayToString(characters);
	}
	
	public static String readSizeAndASCIIStringFromInputStream(InputStream is) throws IOException, UnexpectedNonASCIIByte {
		int numOfChars;
		try {
			DataInputStream dis = new DataInputStream(is);
			numOfChars = dis.readInt();
			//System.out.println("num of chars read as: "+numOfChars);
		} catch (EOFException eof) {
			return null;
		}
		String retValue = readASCIIStringFromInputStream(is, numOfChars);
		//System.out.println("string read as:"+retValue);
		return retValue;
	}

	public static boolean writeSizeAndASCIIStringToOutputStream(String s, OutputStream os) throws IOException {
		return writeSizeAndASCIIStringToOutputStream(s, os, true);
	}
	
	public static boolean writeSizeAndASCIIStringToOutputStream(String s, OutputStream os, boolean forceWrite) throws IOException {
		if ((forceWrite) || (CharMatcher.ASCII.matchesAllOf(s))) {
			DataOutputStream dos = new DataOutputStream(os);
			dos.writeInt(s.length());
			dos.flush();
			PrintWriter pw = new PrintWriter(os);
			pw.print(s);
			pw.flush();
		}
		if (CharMatcher.ASCII.matchesAllOf(s))
			return true;
		else
			return false;
	}
	
	public static boolean copyStream(InputStream in, boolean closeAfterReading, long numOfBytes, OutputStream out, boolean closeAfterWriting) throws IOException {
		return copyStream(in, closeAfterReading, numOfBytes, out, closeAfterWriting, null);
	}
	
	// copies up to numOfBytes from the input stream to the output stream
	// returns true if manages to copy that number, false if less were copied when the
	// end of the stream was reached. I had to implement it since the Apache commons
	// IOUtil copies till the end of the streams
	public static boolean copyStream(InputStream in, boolean closeAfterReading, long numOfBytes, OutputStream out, boolean closeAfterWriting, ICopyStreamTracker tracker) 
	throws IOException
	{
		Argcheck.istrue(numOfBytes>=0);
		final BufferedInputStream bin = IOUtil.bufferStream(in);
		final BufferedOutputStream bout = IOUtil.bufferStream(out);
		int i = 0;
		long bytesCopied = 0;
		while ( (bytesCopied < numOfBytes) && (i = bin.read()) > -1) {
			bout.write(i);
			bytesCopied++;
			if (tracker!=null)
				tracker.track(bytesCopied);
			// System.out.println(bytesCopied);
		}
		//FilterStream (like bufferedinputstream etc) close all internal
		//streams when they are closed too !
		if (closeAfterReading) bin.close();
		if (closeAfterWriting) bout.close();
		else bout.flush(); // in any case, flush
		return (bytesCopied == numOfBytes);
	} 

	
	public static void panicIfCannotWrite(File f) {
		if ((!f.exists()) || (f.isDirectory()) || (!f.canWrite())) throw new RuntimeException(f.getAbsolutePath());
	}
	
	public static boolean identical(File f1, File f2) throws IOException {
		if ((f1.getAbsoluteFile()).equals(f2.getAbsoluteFile())) return true;
		else {
			FileInputStream is1 = new FileInputStream(f1);
			FileInputStream is2 = new FileInputStream(f2);
			try {
				if (f1.length() != f2.length()) return false;
				else {
					byte[] bytes1 = new byte[1000];
					byte[] bytes2 = new byte[1000];
		
				    while (true) {
				    	int read1 = is1.read(bytes1);
				    	int read2 = is2.read(bytes2);
				    	if (read1!=read2) throw new RuntimeException(read1+"!="+read2);
				    	if (read1==-1) break;
				    	else if (!Arrays.equals(bytes1, bytes2)) return false;
				    }
				    return true;
				}
			} finally {
				is1.close();
				is2.close();
			}
		}
	}
	
	// Returns the contents of an input stream as a byte array 
	public static byte[] getBytesFromInputStream(InputStream is) throws IOException {

		// Get the size of the file
		long length = is.available();

		panicIf (length > Integer.MAX_VALUE);

		// Create the byte array to hold the data
		byte[] bytes = new byte[(int) length];

		// Read in the bytes
		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length
		&& (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
		offset += numRead;
		}

		// Ensure all the bytes have been read in
		if (offset < bytes.length) {
		throw new IOException("Could not completely read file ");
		}

		// Close the input stream and return bytes
		is.close();
		return bytes;

	}
	
	// Returns the contents of the file in a byte array.
	public static byte[] getBytesFromFile(File file) throws IOException {
	    panicIf(file.length()> Integer.MAX_VALUE);		
	    InputStream is = new FileInputStream(file);
	    return getBytesFromInputStream(is);
	}
	
	public static void writeFile(byte[] bytes, File f) throws IOException {
		OutputStream os = new FileOutputStream(f);
		os.write(bytes);
		os.flush();
		os.close();
	}

	public static String readFileAsSingleString(File f) throws IOException {
            return readFileAsSingleString(f, "");
        }	
	
        public static String readFileAsSingleString(File f, String pad) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(f));
		String line;
		StringBuffer result = new StringBuffer();
		while ((line = br.readLine()) != null) {
			result.append(line+pad);
		}
		return result.toString();
	}

	public static String readUTF8FileAsSingleString(File f) throws IOException {
            return readUTF8FileAsSingleString(f, "");
        }	
	
        public static String readUTF8FileAsSingleString(File f, String pad) throws IOException, UnsupportedEncodingException {
            BufferedReader br = new BufferedReader(new InputStreamReader( new FileInputStream(f), "UTF-8"));
		String line;
		StringBuffer result = new StringBuffer();
		while ((line = br.readLine()) != null) {
			result.append(line+pad);
		}
		return result.toString();
	}



	public static List<String> readFileAsListOfStrings(File f) throws IOException {
		return readFileAsListOfStrings(f, (IStringFilter) null);
	}
	
	public static List<String> readFileAsListOfStrings(File f, IStringFilter stringFilter) throws IOException {
		if (stringFilter == null)
			return readFileAsListOfStrings(f, (List<IStringFilter>) null);
		else {
			List<IStringFilter> stringFilters = new ArrayList<IStringFilter>();
			stringFilters.add(stringFilter);
			return readFileAsListOfStrings(f, stringFilters);
		}
	}
	
	public static void write(File file, String value) throws IOException {
		BufferedWriter out = new BufferedWriter(new FileWriter(file));
		out.write(value);
		out.flush();
		out.close();
	}
	
	public static List<String> readFileAsListOfStrings(File f, List<IStringFilter> stringFilters) throws IOException {
		return readFileAsListOfStrings(f, stringFilters, null); 
	}
	
	public static List<String> readFileAsListOfStrings(File f, List<IStringFilter> stringFilters, List<IStringModifier> modifiers) throws IOException {
		return readFileAsListOfStrings(f, stringFilters, modifiers, null);
	}
	
	public static List<String> readFileAsListOfStrings(File f, List<IStringFilter> stringFilters, List<IStringModifier> modifiers, Charset cs) throws IOException {
        // FileReader fileReader = new FileReader(f);
		InputStream is = new FileInputStream(f);
		/*
        List<String> lines = new ArrayList<String>();
        String line = null;
        while ((line = bufferedReader.readLine()) != null){
        	if (stringFilters==null)
        		if (modifiers==null) {
        			lines.add(line);
        		} else {
        			String temp = line;
        			if (modifiers!=null)
	        			for (IStringModifier modifier : modifiers) {
	        				temp = modifier.modify(temp);
	        			}
        			lines.add(temp);
        		}
        	else {
        		boolean pass = true;
        		for (IStringFilter stringFilter: stringFilters)
        			if (!stringFilter.accept(line))
        				pass = false;
        		if (pass) {
        			String temp = line;
        			if (modifiers != null)
	        			for (IStringModifier modifier : modifiers) {
	        				temp = modifier.modify(temp);
	        			}
        			lines.add(temp);
        		}
        	}
        }
        bufferedReader.close();
        return lines; */
        return readInputStreamAsListOfStrings(is, stringFilters, modifiers, cs);
	}
	
	public static List<String> readInputStreamAsListOfStrings(InputStream is, List<IStringFilter> stringFilters, List<IStringModifier> modifiers, Charset cs) throws IOException {
		InputStreamReader isr = null;
		if (cs==null)
			isr = new InputStreamReader(is);
		else
			isr = new InputStreamReader(is, cs);

        BufferedReader bufferedReader = new BufferedReader(isr);
        return readBufferedReaderAsListOfStrings(bufferedReader, stringFilters, modifiers);

	}
	
	public static InputStream inputStreamFromListOfStrings(List<String> listOfStrings, String separator) {
		return inputStreamFromListOfStrings(listOfStrings, separator, null);
	}
	
	public static InputStream inputStreamFromListOfStrings(List<String> listOfStrings, String separator, Charset cs) {
		String text = StringUtils.join(listOfStrings.toArray(), separator);
		text = text.concat(separator);
		return inputStreamFromString(text, cs);
	}
	
	public static InputStream inputStreamFromString(String s) {
		return inputStreamFromString(s, null);
	}
	public static InputStream inputStreamFromString(String s, Charset cs) {
		if (cs==null)
			return new ByteArrayInputStream(s.getBytes());
		else
			return new ByteArrayInputStream(s.getBytes(cs));
		
	}
	
	private static List<String> readBufferedReaderAsListOfStrings(BufferedReader bufferedReader, List<IStringFilter> stringFilters, List<IStringModifier> modifiers) throws IOException {
        List<String> lines = new ArrayList<String>();
        String line = null;
        while ((line = bufferedReader.readLine()) != null){
        	if (stringFilters==null)
        		if (modifiers==null) {
        			lines.add(line);
        		} else {
        			String temp = line;
        			if (modifiers!=null)
	        			for (IStringModifier modifier : modifiers) {
	        				temp = modifier.modify(temp);
	        			}
        			lines.add(temp);
        		}
        	else {
        		boolean pass = true;
        		for (IStringFilter stringFilter: stringFilters)
        			if (!stringFilter.accept(line))
        				pass = false;
        		if (pass) {
        			String temp = line;
        			if (modifiers != null)
	        			for (IStringModifier modifier : modifiers) {
	        				temp = modifier.modify(temp);
	        			}
        			lines.add(temp);
        		}
        	}
        }
        bufferedReader.close();
        return lines;
	}
	

	
	
	
	
	
	public static List<File> getFilesMatchingAPreffix(File directory, String prefix) {

		class FilenamePreffixFilter implements FilenameFilter
	    {
			private String prefix;
			public FilenamePreffixFilter(String prefix) {
				this.prefix = prefix;
			}
			public boolean accept(File dir, String s)
			{
			    if ( s.startsWith(prefix) ) return true;
			    return false;
			}
	    }
		FilenamePreffixFilter filter =  new FilenamePreffixFilter(prefix);
		File files[] = directory.listFiles(filter);
	    return Array2List.trsnfrm(files);
	}
	// beware of the following fact: the class loader will look in the search path defined by the classpath
	// so if there are multiple copies of the file, you may find it in another location than the one you expected.
	public static File getDirectoryRelativeToRootOfClasspath(String relativePath) throws URISyntaxException {
		final URL directoryURL = FileUtil.class.getResource(relativePath);
		final File directoryAsAFile = new File(directoryURL.toURI());
		return directoryAsAFile;
		
	    /*
	    Apparently the following code also works as expected to get a Java File from a Java URL:
		URL url = new URL( "file:///home/nonet/some%20dir" );
		File f = new File( URLDecoder.decode( url.getFile(), "UTF-8" ) );
		*/
	}

	// UPDATE 2012-01-30: this seems to be problematic when used inside deployed jar files, better call
	// getClass().getResourceAsStream() instead
	@Deprecated
	public static final File getThisClassDirectory(Class klass) throws URISyntaxException {
		// in the call to Class::getResource, the separator is always "/" irrespectively of the OS
		// see http://download.oracle.com/javase/1.5.0/docs/guide/lang/resources.html
		String packageName = klass.getPackage().getName();
		String pathList = "/"+packageName.replace(".", "/"); 
		return getDirectoryRelativeToRootOfClasspath(pathList);
	}
	
	public static boolean identicalLines(File f1, File f2) {
		BufferedReader br1 = null;
		BufferedReader br2 = null;
		try {
			br1 = new BufferedReader(new InputStreamReader(new FileInputStream(f1)));
			br2 = new BufferedReader(new InputStreamReader(new FileInputStream(f2)));
			TwoBufferedReadersIterator brIterator = new TwoBufferedReadersIterator( br1, br2);
			Pair<String, String> nextLine;
			boolean retValue = true;
			while (! (nextLine = brIterator.next()).equals(Pair.create( (String) null, (String) null))) {
				if (StringUtils.indexOfDifference(nextLine.a, nextLine.b)!=-1)
					retValue = false;
			}
			return retValue;
		} catch (Exception e) {
			throw new ExceptionAdapter(e);
		} finally {
			try {
				br1.close();
				br2.close();
			} catch (IOException e) {
				throw new ExceptionAdapter(e);
			}

		}
	}
	
	public static Collection<File> findFiles(File base, boolean recurse) {
		return findFiles(base, recurse, ".*");
	}
	
	public static Collection<File> findFiles(File base, boolean recurse, final String fileNameRegexp) {
		Collection<File> retValue = FileUtils.listFiles(base, new IOFileFilter() {
			
			@Override
			public boolean accept(File dir, String name) {
				return RegExpUtil.matches(name, fileNameRegexp);
			}
			
			@Override
			public boolean accept(File file) {
				if (file.isDirectory())
					return false;
				else
					return accept(file.getParentFile(), file.getName());
			}
		}, recurse?TrueFileFilter.INSTANCE:null);
		return retValue;
	}
	
	public static String translateLine(String in , Map<String, String> translations) {
		String[] keys = translations.keySet().toArray(new String[]{});
		List<String> valuesL = new ArrayList<>();
		for (String key: keys)
			valuesL.add(translations.get(key));
		String[] values = valuesL.toArray(new String[]{});
		return StringUtils.replaceEach(in, keys, values);
	}
	
	public static void translateASCII(BufferedReader in, PrintWriter out, Map<String, String> translations) throws IOException {
		String line;
		while ((line = in.readLine()) != null) {
			out.println(translateLine(line, translations));
		}
	}
   
    public static byte[] readAsByteArray(File f) throws FileNotFoundException, IOException {
        FileInputStream fins = new FileInputStream(f);
        byte fileContent[] = new byte[(int)f.length()];
        fins.read(fileContent);
        return fileContent;
    }

    public static char[] toCharArray(byte[] bytes) {
        return (new String(bytes)).toCharArray();
    }

    public static String getASCIIResourceAsString(Class klassRelativeTo, String resourcePath) throws IOException {
        return getResourceAsString(klassRelativeTo, resourcePath, "US-ASCII");
    }

    public static String getResourceAsString(Class klassRelativeTo, String resourcePath, String encoding) throws IOException {
        StringWriter writer = new StringWriter();
        FileUtil foo = new FileUtil();
        IOUtils.copy(klassRelativeTo.getResourceAsStream(resourcePath), writer, encoding);
        return writer.toString();
    }

    public static void main(String args[]) throws FileNotFoundException, IOException {
	Map<String, String> dictionary = new HashMap<String, String>();
	dictionary.put("will", "shall");
	dictionary.put("at", "att");
	translateASCII(new BufferedReader(new FileReader(new File(args[0]))), new PrintWriter(new FileWriter(new File(args[1]))), dictionary);
    }

    public static File getSingleFile(File reportFolder, String regexp) throws Exception {
        Collection<File> files = findFiles(reportFolder, false, regexp);
        if (files.size()!=1) throw new RuntimeException(String.format("%d files found instead of 1 for pattern: %s in folder: %s", files.size(), regexp, reportFolder.getPath()));
        return (new ArrayList<File>(files)).get(0);
    }
    public static File getSingleFile(File reportFolder, String regexp, String defaultRegexp) throws Exception {
        Collection<File> files = findFiles(reportFolder, false, regexp);
        if (files.isEmpty() )
            files = findFiles(reportFolder, false, defaultRegexp);
        if (files.size()!=1) throw new RuntimeException(String.format("%d files found instead of 1 for pattern: %s in folder: %s", files.size(), regexp, reportFolder.getPath()));
        return (new ArrayList<File>(files)).get(0);
    }

    public static List<File> getMultipleFiles(File reportFolder, String regexp) throws Exception {
        return new ArrayList(findFiles(reportFolder, false, regexp));
    }
    
    
}
