package gr.neuropublic.mutil.base;

import java.nio.ByteBuffer;

import org.apache.commons.lang.ArrayUtils;

public class ByteBufferUtil {
	public static byte[] getBytes(ByteBuffer buf) {
		return ArrayUtils.subarray(buf.array(), 0, buf.position());
	}
}
