package gr.neuropublic.mutil.base;
import java.util.Set;
import java.util.LinkedHashSet;
import java.util.Collection;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.HashMap;
import org.apache.commons.lang3.StringUtils;



public class TreeNode<K, V> {
    public K k;
    public V v;
    public Map<K, TreeNode<K,V>> children;

    public TreeNode(K k, V v) {
        this.k = k;
        this.v = v;
        this.children = new HashMap<K, TreeNode<K,V>>();
    }

    public void addChild(TreeNode<K,V> child) {
        TreeNode<K,V> prevValue = children.put(child.k, child);
        if (prevValue != null) throw new RuntimeException("class was written and "+
                "tested with the assumption that children will be added only once"+
                "\n failed to add: "+child+"\n previous instance was: "+prevValue);
    }

    public TreeNode find(K k) {
        if (this.k.equals(k))
            return this;
        else for (TreeNode child : children.values()) {
                TreeNode childRetValue = child.find(k);
                if (childRetValue!=null) return childRetValue;
            }
        return null;
    }

    public static <K,V> TreeNode find(Collection<TreeNode<K,V>> hayStack, K k) {
        for (TreeNode treeNode: hayStack) {
            TreeNode<K,V> retValue = treeNode.find(k);
            if (retValue!=null) return retValue;
        }
        return null;
    }

    @Override
    public boolean equals (Object otherO) {
        if (!(otherO instanceof TreeNode)) return false;
        TreeNode<K,V> other = (TreeNode<K,V>) otherO;
        if ((this.k==null) && (other.k!=null)) return false;
        else if (this.k.equals(other.k)) {
            if ((this.v==null) && (other.v!=null)) throw new RuntimeException();
            if (!this.v.equals(other.v)) throw new RuntimeException();
            return true;
        } else return false;
    }

    @Override
    public String toString() {
        return "[ "+k+","+v+",("+StringUtils.join(children.values(), ',')+") ]";
    }

    public static <K,V> Set<TreeNode<K,V>> treeify(Map<K, TreeNode<K,V>> flatNodes, Map<K,K> fathers) {
        Set<TreeNode<K,V>> retValue = new LinkedHashSet<TreeNode<K,V>>();
        Set<TreeNode<K,V>>  handled = new LinkedHashSet<TreeNode<K,V>>();
        int nodesTreeified = 0;
        // implement a horrendous, gratuitous Theta(n^4) algorithm
        for (int i = 0 ; i < flatNodes.size()*flatNodes.size() ; i++) {
            for (TreeNode treeNode : flatNodes.values())
                if (!handled.contains(treeNode)) {
                    if (!fathers.containsKey(treeNode.k)) throw new RuntimeException();
                    K treeNodeFatherK = fathers.get(treeNode.k);
                    if (treeNodeFatherK == null) { // we have a new tree in the forest
                        retValue.add(treeNode);
                        handled.add(treeNode);
                        nodesTreeified++;
                    } else {
                        TreeNode<K,V> treeNodeFather = TreeNode.find(retValue, treeNodeFatherK);
                        if (treeNodeFather == null) { // father not yet in the return collection
                            // do nothing
                        } else {
                            treeNodeFather.addChild(treeNode);
                            handled.add(treeNode);
                            nodesTreeified++;
                        }       
                    }
                }
        }
        if (nodesTreeified != flatNodes.size())
            throw new RuntimeException(String.format("%d != %d", nodesTreeified, flatNodes.size()));
        return retValue;
    }
}
