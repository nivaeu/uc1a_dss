package gr.neuropublic.mutil.base;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;



public class ListsOfStrings {
	
	
	// example: "(a,b)(c,d,e,f,g)" - notice no comma between the parenthesis
	public static List<List<String>> create2Level(String literal) {
		return create2Level(literal, String.class);
		/*
		StringTokenizer st = new StringTokenizer(literal, "()", false);
		List<List<String>> retValue = new ArrayList<List<String>>();
		while (st.hasMoreElements()) {
			String next = st.nextToken();
			retValue.add(create1Level(next));
		}
		return retValue;
		*/
	}
	
	public static List<String> create1Level(String literal) {
		return create1Level(literal, String.class);
		/*
		StringTokenizer st = new StringTokenizer(literal, ",", false);
		List<String> retValue = new ArrayList<String>();
		while (st.hasMoreElements()) {
			retValue.add(st.nextToken());
		}
		return retValue;
		*/
	}
	
	public static <T> List<List<T>> create2Level(String literal, Class<T> klass)  {
		return create2Level(literal, new ConstructorProviderFromString<T>(klass));
		/*
		try {
			StringTokenizer st = new StringTokenizer(literal, "()", false);
			List<List<T>> retValue = new ArrayList<List<T>>();
			while (st.hasMoreElements()) {
				String next = st.nextToken();
				retValue.add(create1Level(next, klass));
			}
			return retValue;			
		} catch (Exception e) {
			throw new ExceptionAdapter(e);
		}*/
	}
	

	
	public static <T> List<T> create1Level(String literal, Class<T> klass) {
		return create1Level(literal, new ConstructorProviderFromString<T>(klass));
		/*
		try {
			Constructor<T> klassConstr = klass.getConstructor(new Class[] {String.class});
			StringTokenizer st = new StringTokenizer(literal, ",", false);
			List<T> retValue = new ArrayList<T>();
			while (st.hasMoreElements()) {
				retValue.add(klassConstr.newInstance( (Object[]) new String[]{st.nextToken()}));
			}
			return retValue;
		} catch (Exception e) {
			throw new ExceptionAdapter(e);
		}
		*/
	}
	
	public static <T> List<List<T>> create2Level(String literal, IProviderFromString<T> provider)  {
		try {
			StringTokenizer st = new StringTokenizer(literal, "()", false);
			List<List<T>> retValue = new ArrayList<List<T>>();
			while (st.hasMoreElements()) {
				String next = st.nextToken();
				retValue.add(create1Level(next, provider));
			}
			return retValue;			
		} catch (Exception e) {
			throw new ExceptionAdapter(e);
		}
	}	

	public static <T> List<T> create1Level(String literal, IProviderFromString<T> provider) {
		try {
			StringTokenizer st = new StringTokenizer(literal, ",", false);
			List<T> retValue = new ArrayList<T>();
			while (st.hasMoreElements()) {
				retValue.add(provider.fromString(st.nextToken()));
			}
			return retValue;
		} catch (Exception e) {
			throw new ExceptionAdapter(e);
		}
	}
	
	
	
	
	
	
	

}


