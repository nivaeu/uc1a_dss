package gr.neuropublic.mutil.base;

public interface IVisitor<K> {
    public void visit(K k);
}