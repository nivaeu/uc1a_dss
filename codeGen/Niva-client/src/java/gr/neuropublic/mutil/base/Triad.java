package gr.neuropublic.mutil.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Triad<A, B, C> implements Serializable {
	 
    private static final long serialVersionUID = 1L;
	
    public  A a;
    public  B b;
    public  C c;
 
    public Triad() {
    	// no-args constructor required by Json
    }
  
    public Triad(final A a, final B b, final C c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }
    
    public static <A, B, C> Triad<A, B, C> create(A a, B b, C c) {
        return new Triad<A, B, C>(a, b, c);
    }

    public static <A, B, C> Triad<A, B, C> create(Pair<A, B> ab, C c) {
        return new Triad<A, B, C>(ab.a, ab.b, c);
    }

    public static <A, B, C> Triad<A, B, C> create(A a, Pair<B, C> bc) {
        return new Triad<A, B, C>(a, bc.a, bc.b);
    }
 
    @SuppressWarnings("unchecked")
	public final boolean equals(Object o) {
        if (!(o instanceof Triad))
            return false;
 
        final Triad<?, ?, ?> other = (Triad) o;
        return equal(a, other.a) && equal(b, other.b) && equal(c, other.c);
    }
    
    private static final boolean equal(Object o1, Object o2) {
        if (o1 == null) {
            return o2 == null;
        }
        return o1.equals(o2);
    }
 
    public int hashCode() {
        int ha = a == null ? 0 : a.hashCode();
        int hb = b == null ? 0 : b.hashCode();
        int hc = c == null ? 0 : c.hashCode();
 
        return ha + (57 * hb) + (957 * hc);
    }
    
    public static <A,B,C> List<A> list_a(Collection<Triad<A,B,C>> triads) {
    	List<A> retValue = new ArrayList<A>();
    	for (Triad<A,B,C> triad : triads)
    		retValue.add(triad.a);
    	return retValue;
    }
}