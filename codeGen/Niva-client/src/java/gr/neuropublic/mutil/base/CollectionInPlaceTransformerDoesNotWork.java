package gr.neuropublic.mutil.base;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class CollectionInPlaceTransformerDoesNotWork  {
	// it is pretty obvious why the below code does not work
	public static <K> void dispatch(Collection<K> collectionOfKs, ITransformer_1_to_1<K, K> transformer) {
		for (K k : collectionOfKs) {
			k = transformer.transform(k);
		}
	}
	
}
