package gr.neuropublic.mutil.base;

import java.security.SecureRandom;
import java.math.BigInteger;

public final class SessionIdentifierGenerator
{

    private SecureRandom random;

    public SessionIdentifierGenerator() {
        random = new SecureRandom();
    }

    public SessionIdentifierGenerator(long seed) {
        random = new SecureRandom();
        random.setSeed(seed);    
    }

    public String nextSessionId()
    {
        return new BigInteger(130, random).toString(32);
    }

}