package gr.neuropublic.mutil.base;

import java.io.BufferedReader;
import java.io.IOException;

public class TwoBufferedReadersIterator {
	BufferedReader br1;
	BufferedReader br2;
	
	public TwoBufferedReadersIterator(BufferedReader br1, BufferedReader br2) {
		this.br1 = br1;
		this.br2 = br2;
	}
	
	Pair<String, String> next() {
		try {
			String s1 = br1.readLine();
			String s2 = br2.readLine();
			return Pair.create(s1, s2);
		} catch (IOException e) {
			throw new ExceptionAdapter(e);
		}
	}

}
