package gr.neuropublic.mutil.base;

import java.net.InetSocketAddress;
import java.util.StringTokenizer;

public class PairStringIntegerStringProvider implements IProviderFromString<Pair<String, Integer>> {

	@Override
	public Pair<String, Integer> fromString(String s) {
		// we expect "hostname:port"
		StringTokenizer st = new StringTokenizer(s, ":", false);
		return Pair.create(st.nextToken(), new Integer(st.nextToken()).intValue());
	}



}
