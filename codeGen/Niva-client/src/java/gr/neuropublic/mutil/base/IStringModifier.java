package gr.neuropublic.mutil.base;

public interface IStringModifier {
	public String modify(String aString);

}
