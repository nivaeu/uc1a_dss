package gr.neuropublic.mutil.base;

public enum Rigor {
	
	DEMO, PRODUCTION, DEBUG;
	
	public static boolean demo() {
		return synelixis_rigor_env_equals(DEMO);
	}
	
	public static boolean production() {
		return synelixis_rigor_env_equals(PRODUCTION);
	}
	
	public static boolean debugging() {
		return synelixis_rigor_env_equals(DEBUG);
	}

	private static boolean synelixis_rigor_env_equals(Rigor rigor) {
		return Util.ifNull(System.getProperty("synelixis.rigor"), "NULL").toUpperCase().equals(rigor.name().toUpperCase()); 
	}
	
	public static void main (String ...args) {
		System.out.println(args.length+" args passed.");
		for (String arg : args)
			System.out.println(arg);
		System.out.println("demo =\t\t\t\t"		+demo());
		System.out.println("production =\t\t\t"	+production());
		System.out.println("debugging =\t\t\t"	+debugging());
	}
}
