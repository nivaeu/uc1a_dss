package gr.neuropublic.mutil.base;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class Collection2ListTransformer  {
	
	public static <V, K> List<K> dispatch(Collection<V> collectionOfV, ITransformer_1_to_1<V, K> transformer) {
		List<K> listOfKs = new ArrayList<K>();
		for (V v : collectionOfV) {
			listOfKs.add(transformer.transform(v));
		}
		return listOfKs;
	}
	
}
