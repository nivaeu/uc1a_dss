// Copyright (c) 2001 Hursh Jain (http://www.mollypages.org) 
// The Molly framework is freely distributable under the terms of an
// MIT-style license. For details, see the molly pages web site at:
// http://www.mollypages.org/. Use, modify, have fun !

package gr.neuropublic.mutil.base;

import java.util.*;

/** 
Utility functions related to java.lang.String, i.e 
functions that are useful but not provided by that class.

@author hursh jain
*/
public final class StringUtil
{
/** 
Returns an empty string is the specified argument was null,
otherwise returns the argument itself. 
**/
public static String nullToEmpty(String val) 
	{
	if (val == null) {
		return "";
		}
	return val;
	}

/** 
Returns an empty string is the specified argument was null,
otherwise returns the value of the toString() method invoked
on the specified object. 
**/
public static String nullToEmpty(Object val) 
	{
	if (val == null) {
		return "";
		}
	return val.toString();
	}

/** 
Returns a String containing a string of the specified character concatenated
the specified number of times. Returns an empty string if length
is less than/equal to zero

@param c		the character to be repeated
@param length	the repeat length
*/
public static String repeat(char c, int length)
	{
	StringBuffer b = new StringBuffer();
	for (int n = 0; n < length; n++) {
		b.append(c);
		}
	return b.toString();
	}

/** 
Returns a String containing the specified string concatenated
the specified number of times.

@param str		the string to be repeated
@param length	the repeat length
*/
public static String repeat(String str, int length)
	{
	int len = str.length();
	StringBuffer b = new StringBuffer(len*length);
	for (int n = 0; n < length; n++) {
		b.append(str);
		}
	return b.toString();
	}

/** 
Contatenates the given string so that the maximum length reached
is the specified length. If the specified string does not fit in
the length, then the string is truncated appropriately.

@param	str		the string to repeat 
@param	length	the length of the returned string
**/
public static String repeatToWidth(String str, int length)
	{
	Argcheck.notnull(str, "specified string was null");
		
	int strlen = str.length();	
	
	//strlen.range: 		[0,inf)
	//repeat length range:	(-inf, inf)
	
	if (strlen == length)		
		return str;

	if (strlen > length)
		return str.substring(0, length);


	//strlen.range now		[1, inf)	

	int multiple   = length / strlen;	
	int fractional = length % strlen;

	String result = repeat(str, multiple);

	if (fractional != 0) { 
		result = result.concat(str.substring(0, fractional));
		}
		
	return result;
	}


/** 
Converts the specified String into a fixed width string,
left padding (with a blank space) or truncating on the right as necessary. 
(i.e., the specified string is aligned left w.r.t to the specified width).

@param	str		the target string
@param	width	the fixed width

@return	the transformed string
**/
public static String fixedWidth(String str, int width) 
	{		
	return fixedWidth(str, width, HAlign.LEFT, ' ');
	}
	
/** 
Calls {@link fixedWidth(String, int, HAlign, char)} specifying
the padding character as a blank space.
**/
public static String fixedWidth(String str, int width, HAlign align) 
	{
	return fixedWidth(str, width, align, ' ');
	}
	

/** 
Converts the specified String into a fixed width string. Strings
lesser in size than the fixed width are padded or truncated as
required by the specified alignment.

@param	str			the target string
@param	width		the fixed width
@param	align 		the alignment of the target string within the width
@param	paddingChar the character to pad the string (if necessary);

@return	the transformed string
**/
public static String fixedWidth(
 String str, int width, HAlign align, char paddingChar) 
	{
	if (str == null)
		str = "";
		
	int len = str.length();

	if (len < width) 
		{
		if (align == HAlign.LEFT) {
			str = str + repeat(paddingChar, width-len);
			}
		else if (align == HAlign.RIGHT) {
			str = repeat(paddingChar, width-len) + str;
			}
		else if (align == HAlign.CENTER) {
			//arbitrary tie-break, if the diff is odd then the
			//1 extra space is padded on the right side
	
			int diff = (width-len);		
			String temp = repeat(paddingChar, diff/2);
			str = temp + str + temp + repeat(paddingChar, diff%2);
			}
		else throw new IllegalArgumentException("Do not understand the specified alignment: " + align);
		}


	else if (len > width) {
		str = str.substring(0, width);
		}
		
	return str;	
	}

/** 
Removes all occurences of specified characters from the specified 
string and returns the new resulting string.
@param 	target	the string to remove characters from
@param	chars	an array of characters, each of which is to be removed
*/
public static String remove(String target, char[] chars)
	{
	if (target == null || chars == null) return target;
	int strlen = target.length();
	char[] newstr = new char[strlen];
	char[] oldstr = new char[strlen];
	target.getChars(0,strlen,oldstr,0);
	int replacelen = chars.length;
	
	int oldindex = -1;
	int newindex = -1;
	char c = 0;
	boolean found = false;

	while (++oldindex < strlen) 
		{
		c = oldstr[oldindex];
		found = false;
		for (int j = 0; j < replacelen; j++) 
			{
			if (c == chars[j]) {
				found = true;
				break;
				}
			}	//~for
		if (!found) 
			{
			newstr[++newindex] = c;
			}
		}		//~while
	return new String(newstr, 0, newindex+1);
	}			//~remove()


/**
Removes the last forward or backward slash from the specified
string (if any) and returns the resulting String. Returns <tt>null</tt>
if a null argument is specified. The trailing slash is a slash that
is the last character of the specified string.
**/
public static String removeTrailingSlash(String str)
	{
	String res = str;
	if (str == null) return null;	
	int len = str.length();
	if (len == 0)
		return str;  //str was ""
	char c = str.charAt(len-1);
	if (c == '/' || c == '\\') {
		res = str.substring(0, len-1);  //len-1 will be >= 0
		}
	return res;			
	}

/**
Removes the starting (at the very beginning of the string) forward or 
backward slash from the specified string (if any) and returns the 
resulting String. Returns <tt>null</tt> if  null argument is specified.
**/
public static String removeBeginningSlash(String str)
	{
	if (str == null) return null;	
	int len = str.length();
	if (len == 0) 
		return str;  //str was ""
	char c = str.charAt(0);
	if (c == '/' || c == '\\') 
		{
		if (len > 1) { 
			return str.substring(1);
			}
		else { //str was "/"
			return ""; 	
			}
		}
	return str;
	}

/**
Removes the any file extension (.foo for example) from the specified name
and returns the resulting String. Returns <tt>null</tt> if the specified
path was null. This method takes a String as a parameter, as opposed to a
<tt>java.io.File</tt> because the caller knows best what portion of the
filename should be modified and returned by this method (full path name,
name without path information etc).

@param	name the String denoting the file name to remove the extension from
**/
public static String removeSuffix(String name)
	{
	String result = null;
	if (name == null)
		return result;
	result = name.replaceFirst("(.+)(\\..+)","$1");
	//System.out.println("File " + name + " after removing extension =" + name);	
	return result;
	}

/** 
Returns the path component of the specified filename. If no path exists
or the specified string is null, returns the empty string <tt>""</tt>.
<u>The path separator in the specified string should be <tt>/</tt></u>.
If on a platform where this is not the case, the specified string should
be modified to contain "/" before invoking this method. The returned
pathName <b>does</b>
contain the trailing "/". This ensures that 
<tt>dirName(somepath) + fileName(somepath)</tt> will always be equal to <tt>somepath</tt>.
<p>
<blockquote>
<pre>
The functionality of this method is different than java.io.File.getName()
and getParent(). Also unix dirname, basename are also compared below.

//Using java.io.File (getPath() returns the entire name, identical to
//the input, so is not shown. Sample run on windows:
Name=''         ; getName()='';       getParent()='null'
Name='/'        ; getName()='';       getParent()='null'
Name='/a'       ; getName()='a';      getParent()='\'
Name='a/b'      ; getName()='b';      getParent()='a'
Name='a/b.txt'  ; getName()='b.txt';  getParent()='a'
Name='b.txt'    ; getName()='b.txt';  getParent()='null'

Name='/a/'      ; getName()='a';      getParent()='\'
Name='/a/b/'    ; getName()='b';      getParent()='\a'
Name='a/b/'     ; getName()='b';      getParent()='a'
----------------------------
//Using these methods:
Name=''         ; fileName()='';      dirName()=''
Name='/'        ; fileName()='';      dirName()='/' 
Name='/a'       ; fileName()='a';     dirName()='/' 
Name='a/b'      ; fileName()='b';     dirName()='a/' 
Name='a/b.txt'  ; fileName()='b.txt'; dirName()='a/' 
Name='b.txt'    ; fileName()='b.txt'; dirName()='' 

Name='/a/'      ; fileName()='';      dirName()='/a/'
Name='/a/b/'    ; fileName()='';      dirName()='/a/b/'
Name='a/b/'     ; fileName()='';      dirName()='a/b/'
-----------------------------
//unix basename, dirname
Name=''         ; basename()='';	  dirname()=''
Name='/'        ; basename()='/';	  dirname()='/' 
Name='/a'       ; basename()='a';     dirname()='/' 
Name='a/b'      ; basename()='b';     dirname()='a/' 
Name='a/b.txt'  ; basename()='b.txt'; dirname()='a/' 
Name='b.txt'    ; basename()='b.txt'; dirname()='.' 

Name='/a/'      ; basename()='a';     dirname()='/'
Name='a/b/'     ; basename()='b';     dirname()='a'
Name='/a/b/'	; fileName()='b';     dirName()='a'

-----------------------------
</pre>
Note, the main differences among the 3 approaches above are in the last 
2 statements in each section.
</blockquote>
**/
public static String dirName(String str) 
	{
	String res = "";
	if (str == null)
		return res;
	int pos = str.lastIndexOf("/");
	if (pos == -1)
		return "";
	return str.substring(0, (pos+1));
	}

/** 
Returns the file component of specified filename. If no filename exists
or the specified string is null, returns the empty string <tt>""</tt>.
<u>The path separator in the specified string is always assumed to be
<tt>/</tt></u>. If on a platform where this is not the case, the
specified string should be modified to contain "/" before invoking this
method. The returned file name does <b>not</b> contain the preceding "/".
**/
public static String fileName(String str) 
	{
	String res = "";
	if (str == null)
		return res;
	int pos = str.lastIndexOf("/");
	if (pos == -1)
		return str;
	
	int strlen = str.length();
	if (strlen == 1)
		return res; //we return "" since the string has to be "/" 
	else
		pos++;		//skip "/" in other strings
		
	return str.substring(pos, strlen); //will return "" if str = "/"
	}


/** 
Splits the string using the specified delimiters and
returns the splits parts in a List. Use {@link
java.lang.String#split} instead for greater options.

@param	str		the string to be tokenized
@param	delim	delimiter string, each character in the string will be used 
				as a delimiter to use while tokenizing
*/
public static List split(String str, String delim)
	{		
	int pos = 0;		//current position pointer in the string (str)
	List result = new ArrayList();	
	StringTokenizer st = new StringTokenizer(str,delim);
    while (st.hasMoreTokens()) {
	 					//tokens are stored as lowercase
        result.add(st.nextToken().toLowerCase());  
       	}
	return result;	
	}

/** 
Joins the elements of the specified list, delimited by
the specified delimiter.

@param	list	containing the elements to be joined.
@param	delim	delimits each element from the next
*/
public static String join(List list, String delim)
	{	
	Argcheck.notnull(list, "specified list param was null");
	Argcheck.notnull(delim, "specified delim param was null");

	int size = list.size();
	int size_minus_one = size -1 ;
	StringBuffer buf = new StringBuffer(size * 16);
	
	for (int n = 0; n < size; n++) {
		buf.append(list.get(n).toString());
		if ( n < (size_minus_one)) {
 			buf.append(delim);
 			}
		}
		
	return buf.toString();	
	}	


/* TO DO: LATER
Removes all whitespace in the specified string and returns all words 
with only a single space between them. Uses a Perl regular expression 
to do this.
  public synchronized static void makeSingleSpaced(String target)
	throws Exception
	{
	String regex1 = "s\\?([^\"]+)\\s*(target)?[^>]*>([^>]*)</a>";	
 	MatchResult result;
 	Perl5Pattern pattern = 	(Perl5Pattern)StringUtil.compiler.compile(
								regex1,
 								Perl5Compiler.CASE_INSENSITIVE_MASK | 
 								Perl5Compiler.SINGLELINE_MASK);	
 	
	}
*/

/**
Converts the specified String to start with a capital letter. Only
the first character is made uppercase, the rest of the specified
string is not affected.
**/
public static String capitalWord(String str) 
	{
	int strlen = str.length();
	StringBuffer buf = new StringBuffer(strlen);
	buf.append( str.substring(0,1).toUpperCase() + 
				str.substring(1, strlen) );	
	return buf.toString(); 
	}

/**
Converts the specified String to be in sentence case, whereby
the first letter of each word in the sentence is uppercased
and all other letters are lowercased. The characters in the
delimiter string are used to delimit words in the sentence.
If the delimiter string is <tt>null</tt>, the original string
is returned as-is.
<br>
A runtime exception will be thrown if the specified string
was <tt>null</tt>.
**/
public static String sentenceCase(String str, String delimiters)
	{
	Argcheck.notnull(str, "specified string was null");
	if (delimiters == null) 
		return str;
		
	int strlen = str.length();
	StringBuffer out = new StringBuffer(strlen);
	StringBuffer temp = new StringBuffer(strlen);
	for (int n = 0; n < strlen; n++)
		{
		//System.out.print("["+n+"] ");
		char current_char = str.charAt(n);
		if (delimiters.indexOf(current_char) >= 0) {
			//System.out.println("->"+current_char);
			if (temp.length() > 0 ) {
				out.append( temp.substring(0, 1).toUpperCase() );
				out.append( temp.substring(1, temp.length()).toLowerCase() );
				}
			out.append(current_char);
			//System.out.println("temp="+temp);
			temp = new StringBuffer(strlen);
			continue;
			}
		temp.append(current_char);	
		}
	if (temp.length() > 0 ) {
		out.append( temp.substring(0, 1).toUpperCase() );
		out.append( temp.substring(1, temp.length()).toLowerCase() );
		}
	return out.toString();
	}
	
static final String[] VIEW_ASCII = {
/*0*/	"NUL", "[ascii(1)]", "[ascii(2)]", "[ascii(3)]", "[ascii(4)]",
/*5*/	"[ascii(5)]", "[ascii(6)]", "\\a", "\\b", "\\t",
/*10*/	"\\n", "\\v", "[ascii(12)]", "\\r", "[ascii(14)]",
/*15*/	"[ascii(15)]", "[ascii(16)]", "[ascii(17)", "[ascii(18)]", "[ascii(19)]",
/*20*/	"[ascii(20)]", "[ascii(21)]", "[ascii(22)]", "ascii(23)]", "[ascii(24)]",
/*25*/	"[ascii(25)]", "[ascii(26)]", "\\e", "[ascii(28)]", "[ascii(29)]",
/*30*/	"[ascii(30)]", "[ascii(31)]" 
};	
	
/**
Converts non printable ascii characters in the specified String
to escaped or readable equivalents. For example, a newline is
converted to the sequence <tt>\n</tt> and say, ascii character 29 is 
converted to the sequence of chars: '<tt>ascii(29)</tt>'. 
<p>
If the specified String is <tt>null</tt>, this method returns 
<tt>null</tt>.

@param	str		the String to convert
@return	the converted String
**/			
public static String viewableAscii(String str) 
	{
	if (str == null)
		return null;
	
	int strlen = str.length();
	StringBuffer buf = new StringBuffer(strlen);
	
	//ignore all non ascii data, including UTF-16 surrogate bytes etc.
	//by replacing such data with '?' 	
	for(int n = 0; n < strlen; n++) 
		{
		char c = str.charAt(n);
		if ( c < 32) 
			buf.append(VIEW_ASCII[c]);
 		else if ( c > 255) 
			buf.append('?');
		else
			buf.append(c);
		}	
	return buf.toString();
	}	

/**
A version of {@link viewableAscii(String)} that takes a
single char as a parameter.

@param	char the char to convert
@return	the ascii readable char
**/
public static String viewableAscii(char c) 
	{
	if ( c < 32) 
		return VIEW_ASCII[c];
	else if ( c > 255) 
		return "?";
	else
		return new String(new char[] {c});
	}

/**
Converts a character array into a viewable comma delimited
string. Non-printable/readable characters are shown as
readable equivalents.
*/
public static String arrayToString(char[] array) {
	if (array == null) {
		return "null";
		}
	int arraylen = array.length;
	StringBuffer buf = new StringBuffer(arraylen * 2);
	buf.append("[");
	int n = 0;
	while (n < arraylen) {
		buf.append(viewableAscii(array[n]));
		n++;
		if (n != arraylen)
			buf.append(", ");
		}
	buf.append("]");
	return buf.toString();
	}

/**
Escapes all single quotes in the specified a string with a backslash
character. 
*/
public static String escapeSingleQuotes(final String str)
	{
	return escapeSingleQuotes(str, "\\");
	}

/**
Escapes all single quotes in the specified a string with the specified
escpape character. So, if the specified escape character is <tt>'</tt>, 
all occurrences of <tt>o'tis the ele'phant</tt> becomes 
<tt>o''tis the ele''phant</tt>
*/
public static String escapeSingleQuotes(final String str, String escape)
	{
	if (str == null)
		return null;
	
	final int len = str.length();
	if (len == 0)
		return str;
		
	final StringBuilder buf = new StringBuilder(len * 2);
	for (int n = 0; n < len; n++) 
		{
		char c = str.charAt(n);
		if (c == '\'') {
			buf.append(escape);
			buf.append('\'');
			}
		else{
			buf.append(c);
			}
		}
	return buf.toString();
	}

//unit test
public static void main(String[] args)
{
String teststr = "hahaha, my name is ha";
char[] remove = new char[] {'h','m'};
System.out.println("testing StringUtil.remove(\"" + teststr + "\",'" + String.valueOf(remove) + "')");
String newstr = StringUtil.remove(teststr,remove);
System.out.println("result>>" + newstr + "<<");
System.out.println("Original String length: " + teststr.length() +" ; New String length: " + newstr.length());
System.out.println(":" + repeat(' ',20) + ":");

System.out.println("sentenceCase(\"hello world\", \" \"): ["
					+ sentenceCase("hello world", " ") + "]");
System.out.println("sentenceCase(\"helloworld\", \" \"): ["
					+ sentenceCase("helloworld", " ") + "]");
System.out.println("sentenceCase(\"  hello world\", \" \"): ["
					+ sentenceCase("  hello world", " ") + "]");
System.out.println("sentenceCase(\"hello world  \", \" \"): ["
					+ sentenceCase("hello world  ", " ") + "]");
System.out.println("sentenceCase(\"hello_world  \", \"_\"): ["
					+ sentenceCase("hello_world  ", "_") + "]");
System.out.println("sentenceCase(\"__hello_world_ foo  \", \"_ \"): ["
					+ sentenceCase("__hello_world_ foo  ", "_ ") + "]");
System.out.println("sentenceCase(\"foXbAr\", \"X\"): ["
					+ sentenceCase("foXbAr", "X") + "]");
			

System.out.println("viewableAscii(abc[newline]foo[tab]\\u4234)="+viewableAscii("abc\nfoo\t\u4234"));
for(char c = 0; c < 255; c++) {
	System.out.print(viewableAscii(c)); 	
 	}
System.out.println("");

System.out.println("remove trailing slash on '' = " + removeTrailingSlash(""));
System.out.println("remove trailing slash on '/' = " + removeTrailingSlash(""));
System.out.println("remove trailing slash on 'foo/' = " + removeTrailingSlash("foo/"));
System.out.println("remove beginning slash on '' = " + removeBeginningSlash(""));
System.out.println("remove beginning slash on '/' = " + removeBeginningSlash("/"));
System.out.println("remove beginning slash on '/foo' = " + removeBeginningSlash("/foo"));

System.out.println("====fixed width tests");
String fixedin = "hello";
int width = 15;
System.out.println("fixed width input string: " + fixedin);
System.out.println("fixed width = 15");
System.out.println("align default: [" + fixedWidth(fixedin, width) + "]");
System.out.println("align left: [" + fixedWidth(fixedin, width, HAlign.LEFT) + "]");
System.out.println("align right : [" + fixedWidth(fixedin, width, HAlign.RIGHT) + "]");
System.out.println("align center: [" + fixedWidth(fixedin, width, HAlign.CENTER) + "]");

System.out.println("repeatToWidth('hello', 0)="+repeatToWidth("hello", 0));
System.out.println("repeatToWidth('hello', 1)="+repeatToWidth("hello", 1));
System.out.println("repeatToWidth('hello', 5)="+repeatToWidth("hello", 5));
System.out.println("repeatToWidth('hello', 9)="+repeatToWidth("hello", 9));
System.out.println("repeatToWidth('hello', 10)="+repeatToWidth("hello", 10));
System.out.println("repeatToWidth('hello', 11)="+repeatToWidth("hello", 11));

System.out.println("repeatToWidth('X', 0)=["+repeatToWidth("X", 0) +"]");

System.out.println("escapeSingleQuotes(\"'\")="+escapeSingleQuotes("'"));
System.out.println("escapeSingleQuotes(\"\")="+escapeSingleQuotes(""));
System.out.println("escapeSingleQuotes(\"'foo'bar'\")="+escapeSingleQuotes("'foo'bar'"));
System.out.println("escapeSingleQuotes(\"'''\")="+escapeSingleQuotes("'''"));
System.out.println("escapeSingleQuotes(\"'foo'bar'\", \"'\")="+escapeSingleQuotes("'foo'bar'", "'"));
}

}			//~class StringUtil