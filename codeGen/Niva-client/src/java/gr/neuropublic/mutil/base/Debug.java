package gr.neuropublic.mutil.base;

import java.net.URL;
import java.net.URLClassLoader;

public class Debug {
	 	public static String getClasspathString(Object o) {
	 		StringBuffer classpath = new StringBuffer();
	 		ClassLoader applicationClassLoader = o.getClass().getClassLoader();
	 		if (applicationClassLoader == null) {
	 			applicationClassLoader = ClassLoader.getSystemClassLoader();
	 		}
	 		URL[] urls = ((URLClassLoader)applicationClassLoader).getURLs();
	 		for(int i=0; i < urls.length; i++) {
	 			classpath.append(urls[i].getFile()).append("\r\n");
	 		}
	 		return classpath.toString();  }
}
