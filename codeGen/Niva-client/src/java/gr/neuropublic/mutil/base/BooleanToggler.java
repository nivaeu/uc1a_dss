package gr.neuropublic.mutil.base;

public class BooleanToggler {
    private boolean value = false;
    public boolean getValueAndToggle() {
        boolean retValue = value;
        value = !value;
        return retValue;
    }
}