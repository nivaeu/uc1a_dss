package gr.neuropublic.mutil.base;

public class NumberUnits {
	
	public static final long THOUSAND 		= 1000l;
	public static final long MILLION 		= 1000_000l;
	public static final long BILLION 		= 1000_000_000l;
	public static final long TRILLION 		= 1000_000_000_000l;
	public static final long QUADRILLION 	= 1000_000_000_000_000l;
	public static final long QUINTTRILLION  = 1000_000_000_000_000_000l;
	public static final long GAZILION  	    = 9_200_000_000_000_000_000l;


	public static String units(long l) {
		long l2 = Util.abs(l);
		String s;
		if 		(l2 < THOUSAND)				s = String.valueOf(l2);
		else if (l2 < MILLION) 				s = oneDecimal(l, THOUSAND)       +" K";
		else if (l2 < BILLION) 				s = oneDecimal(l2, MILLION)       +" Mi";
		else if (l2 < TRILLION) 			s = oneDecimal(l2, BILLION)       +" Bi";
		else if (l2 < QUADRILLION) 			s = oneDecimal(l2, TRILLION)      +" Tr";
		else if (l2 < QUINTTRILLION) 		s = oneDecimal(l2, QUADRILLION)   +" Qt";
		else if (l2 < GAZILION) 		    s = oneDecimal(l2, QUINTTRILLION) +" Qd";
		else return "Gazlns";
		return l<0?"-":""+s;
	}
	
	public static String lsd(int n, long l) {
		return lsd("...",n , l); 
	}
	
	public static String lsd(String rep, int n, long l) {
		Util.panicIf("replacement string: "+rep+" is longer than or equal to:"+n, rep.length()>=n);
		String sl = String.valueOf(l);
		return (sl.length()>n)?rep+sl.substring(sl.length()-(n-rep.length()) ):sl; 
	}
	
	public static String oneDecimal(long l, long RULER) {
		Util.panicIf(l<RULER);
		if (l/RULER > 100)
			return String.valueOf(l/RULER); // three digits are enough
		else if ((l - (l/RULER)*RULER > RULER/10))
			return l/RULER+"."+((l - (l/RULER)*RULER)*10)/RULER;
		else
			return String.valueOf(l/RULER);
	}
	
	public static void main(String args[]) {
		System.out.println(oneDecimal(1242, 1000));
		System.out.println(oneDecimal(999000, 1000));
		System.out.println(oneDecimal(999999, 1000));
		System.out.println(oneDecimal(1_000_000, 1_000_000));
		System.out.println(oneDecimal(4_000_000, 1_000_000));
		System.out.println(oneDecimal(4_000_001, 1_000_000));
		System.out.println(oneDecimal(4_300_001, 1_000_000));
		System.out.println(oneDecimal(3_999_999, 1_000_000));
		
		System.out.println(lsd(4, 30));
		System.out.println(lsd(4, 2330));
		System.out.println(lsd(4, 23303));
		for (int i = 4; i < 20 ; i++)
			System.out.println(lsd(i, 234532330l));
	}
}
