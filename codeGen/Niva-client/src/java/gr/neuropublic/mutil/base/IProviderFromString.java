package gr.neuropublic.mutil.base;

public interface IProviderFromString<T> {
		public T fromString(String s);

}
