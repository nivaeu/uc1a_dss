package gr.neuropublic.mutil.base;

import org.apache.commons.lang.StringUtils;

public class BlankLineSnobber implements IStringFilter {

	@Override
	public boolean accept(String s) {
		if (StringUtils.isBlank(s)) return false;
		else return true;
	}

}
