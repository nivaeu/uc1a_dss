package gr.neuropublic.mutil.base;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.ArrayUtils;

import java.util.logging.Logger;
/*
	public Integer						status;
	public Integer 						contentLength;
	public boolean						connectionClose;
	public HTTPResponseChunkedInfo		chunkedInfo;
	public Integer						contentBytesRemaining;
 */

public class HTTPResponseExtractor {
	
	private static final Pattern STATUS_PATTERN         = Pattern.compile("^HTTP/1.[01] (\\d*) .*"					, Pattern.DOTALL);
	private static final Pattern CONTENT_LENGTH_PATTERN = Pattern.compile("^Content-Length:\\s*(\\S*)\\s*?"		, Pattern.DOTALL);
	private static final Pattern CONNECTION_CLOSE 		= Pattern.compile("^Connection: close"					, Pattern.DOTALL);
	private static final Pattern TRANSFER_ENCDNG_CHUNKD = Pattern.compile("^Transfer-[Ee]ncoding:\\s*chunked"   , Pattern.DOTALL);


	
	private static String 	extractStatus         (String line)   {return extract(STATUS_PATTERN             , line, 1)		;}
	private static String 	extractContentLength  (String line)   {return extract(CONTENT_LENGTH_PATTERN     , line, 1)		;}
	private static boolean  connectionClose		  (String line)   {return CONNECTION_CLOSE		.matcher(line).matches()	;}
	private static boolean  chunkedTransferEncodng(String line)   {return TRANSFER_ENCDNG_CHUNKD.matcher(line).matches()	;}

	private static final String NEW_LINE 		= "\\r?\\n";
	private static final byte[] DOUBLE_LINE_BYTE_PATTERN = {'\r','\n', '\r', '\n'};
	
	
	public static void main(String args[]) {
		
		String responses[] = {"HTTP/1.1 200 OK\r\nServer: Sun-Java-System-Web-Server/7.0\r\nDate: Fri, 17 Feb 2012 13:21:33 GMT\r\n"+
							   "Content-type: text/html\r\nLast-modified: Tue, 16 Feb 2010 19:02:57 GMT\r\nEtag: W/\"47cf-4b7aebe1\\r\n" +
							   "Content-encoding: gzip\r\nVary: accept-encoding\r\nTransfer-encoding: chunked\r\n\r\n15e5\r\n<garble>"
							   ,
							   "HTTP/1.1 200 OK\r\nServer: Sun-Java-System-Web-Server/7.0\r\nDate: Fri, 17 Feb 2012 13:21:33 GMT\r\n"+
							   "Content-type: text/html\r\nLast-modified: Tue, 16 Feb 2010 19:02:57 GMT\r\nEtag: W/\"47cf-4b7aebe1\\r\n" +
							   "Content-encoding: gzip\r\nVary: accept-encoding\r\nTransfer-encoding: chunked\r\n\r\n0\r\n<garble>"
							   ,
							   "HTTP/1.1 304 Not Modified"+"\r\n"+"Content-Type: text/html"+"\r\n"+"Last-Modified: Tue, 24 Jan 2012 21:29:25 GMT"+"\r\n"
							   +"ETag: \"68db808d58afc257a404b7e26705c95f:1327446908\""+"\r\n"+"Date: Fri, 17 Feb 2012 13:41:56 GMT"+"\r\n"+"Connection: close"+"\r\n\r\n"
							   ,
							   "HTTP/1.1 200 OK\r\n"+
							   "Server: Microsoft-IIS/7.5\r\n"+
							   "Content-Type: text/html; Charset=windows-1253\r\n"+
							   "Set-Cookie: ASPSESSIONIDAQCDSCBS=CEJFNKCDIHLOFGKLDFINMDBP; path=/\r\n"+
							   "X-Powered-By: ASP.NET\r\n"+
							   "Vary: Accept-Encoding\r\n"+
							   "X-Powered-By: ASP.NET\r\n"+
							   "Content-Encoding: gzip\r\n"+
							   "Expires: Fri, 17 Feb 2012 13:45:37 GMT\r\n"+
							   "Cache-Control: max-age=0, no-cache, no-store\r\n"+
							   "Pragma: no-cache\r\n"+
							   "Date: Fri, 17 Feb 2012 13:45:37 GMT\r\n"+
							   "Transfer-Encoding:  chunked\r\n"+
							   "Connection: close\r\n"+
							   "Connection: Transfer-Encoding\r\n"+
							   "\r\n"+
							   "00004000\r\n"
		};
		for (String response: responses)
			System.out.println("response =\r\n"+extractHeaderInfo(Logger.getGlobal(), response.getBytes()));
	}
	
	private static String extract (Pattern pattern, String line, int group) {
		Matcher matcher = pattern.matcher(line);
		if (matcher.matches())
			return matcher.group(group);
		else
			return null;
	}	
	
	private static String[] splitOnNewLinesButDenoteConsecutiveNewLinesWithNull(String in) {
		String [] linesNotQuiteRight = in.split(NEW_LINE, Integer.MAX_VALUE);
		String [] retValue = linesNotQuiteRight;
		if (linesNotQuiteRight[linesNotQuiteRight.length-1].equals(""))
			retValue = (String []) ArrayUtils.subarray(linesNotQuiteRight, 0, linesNotQuiteRight.length-1);
		return retValue;
	}

	private static int positionOfDoubleNewLine(byte bytes[]) {
		int positionOfDoubleNewLine = Util.findFirst(bytes, DOUBLE_LINE_BYTE_PATTERN);
		return positionOfDoubleNewLine;
	}
	

	public static HTTPResponse extractHeaderInfo(Logger l, ByteBuffer buffer) {
		byte[] contents = ByteBufferUtil.getBytes(buffer);
		return extractHeaderInfo(l, contents);
	}
	
	public static HTTPResponse extractHeaderInfo(Logger l, byte bytes[])
	{
		Integer						status					= null;
		Integer 					contentLength			= null;
		boolean						connectionClose 		= false;
		boolean						chunkedTrsnfrEncdng		= false;
		HTTPResponseChunkedInfo		chunkedInfo				= null;
		Integer						contentBytesRemaining 	= null;
		try {
			String incoming = new String(bytes, "US-ASCII"); 
			String lines[] = splitOnNewLinesButDenoteConsecutiveNewLinesWithNull(incoming);
			for (String line : lines) {
				if (line.equals(""))
					continue;
				else {
					String _status			= extractStatus			(line); if (_status 		!=null) status			= Integer.valueOf(_status		);
					String _contentLength	= extractContentLength	(line);	if (_contentLength 	!=null)	contentLength	= Integer.valueOf(_contentLength);
					if (connectionClose								(line)) connectionClose 	= true; 
					if (chunkedTransferEncodng						(line)) chunkedTrsnfrEncdng = true;
				}
			}
			if (chunkedTrsnfrEncdng) {
				byte[] lastChunkPattern = {0x54, 0x72, 0x61, 0x6e, 0x73, 0x66, 0x65, 0x72, 0x2d, 0x45, 0x6e, 0x63,
						                   0x6f, 0x64, 0x69, 0x6e, 0x67, 0x3a, 0x20, 0x63, 0x68, 0x75, 0x6e, 0x6b,
						                   0x65, 0x64, 0x0d, 0x0a, 0x0d, 0x0a, 0x30, 0x0d, 0x0a};
				int indx = Util.findFirst(bytes, lastChunkPattern);
				boolean lastChunk = indx!=-1;
				chunkedInfo = new HTTPResponseChunkedInfo(lastChunk);
			}
			if (contentLength!=null)
				contentBytesRemaining = contentLength - (bytes.length - positionOfDoubleNewLine(bytes)-DOUBLE_LINE_BYTE_PATTERN.length);
			else if (chunkedInfo!=null)
				if (chunkedInfo.lastChunk)
					contentBytesRemaining = 0;
			return new HTTPResponse(status, contentLength, connectionClose, chunkedInfo, contentBytesRemaining);
		} catch (UnsupportedEncodingException uee) {
			throw new ExceptionAdapter(uee);
		}
	} 
		
	public static HTTPResponseOld extractHeaderInfoOld(Logger l, ByteBuffer buffer) {
		byte[] contents = ByteBufferUtil.getBytes(buffer);
		return extractHeaderInfoOld(l, contents);
	}
	
	public static HTTPResponseOld extractHeaderInfoOld(Logger l, byte bytes[])
	{
		// escape cases
		// HTTP/1.0 501 Not Implemented
		final byte HTTP_1_0_501_Not_Implemented[] = {72,84,84,80,47,49,46,48,32,53,48,49,32,78,111,116,32,73,109,112,108,101,109,101,110,116,101,100,13,10};
		if (Arrays.equals(bytes, HTTP_1_0_501_Not_Implemented))
			return new HTTPResponseOld(0, true, 0);
		Integer 	contentLength	= null;
		boolean encounteredEmptyLine = false;
		Integer		bytesOfHeaderContentMissing = null;		
		try {
			String incoming = new String(bytes, "US-ASCII"); 
			String lines[] = splitOnNewLinesButDenoteConsecutiveNewLinesWithNull(incoming);
			for (String line : lines) {
				if (line.equals(""))
					encounteredEmptyLine = true;
				else {
					String _contentLength= extractContentLength(line);	if (_contentLength 	!=null)	contentLength	= Integer.valueOf(_contentLength);
				}
			}
			if (contentLength != null)
				bytesOfHeaderContentMissing = contentLength - (bytes.length - positionOfDoubleNewLine(bytes)-DOUBLE_LINE_BYTE_PATTERN.length);
			else
				bytesOfHeaderContentMissing = encounteredEmptyLine?0:null;
			if (bytesOfHeaderContentMissing==null) {
				System.out.println("encouteredEmptyLine="+encounteredEmptyLine);
				System.out.println("bytes are:"+ArrayUtils.toString(bytes));
				Util.panic("I am about to send an HTTP Response header with no contentlength for response: "+incoming);
			}
			return new HTTPResponseOld(contentLength, encounteredEmptyLine, bytesOfHeaderContentMissing);
		} catch (UnsupportedEncodingException uee) {
			throw new ExceptionAdapter(uee);
		}
	}
}
