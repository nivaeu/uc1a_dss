package gr.neuropublic.mutil.base;

public interface IStringifier<T> {
    public String toString(T t);
}