package gr.neuropublic.mutil.base;

import java.io.Serializable;

public class Quad<A, B, C, D> implements Serializable {
	 

	private static final long serialVersionUID = 1L;

	
	public A a;
    public B b;
    public C c;
    public D d; 
	
    public Quad() {
    	// no-args constructor required by Json
    }
    
    public Quad(final A a, final B b, final C c, final D d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }
    
    public static <A, B, C, D> Quad<A, B, C, D> create(A a, B b, C c, D d) {
        return new Quad<A, B, C, D>(a, b, c, d);
    }
 
    @SuppressWarnings("unchecked")
	public final boolean equals(Object o) {
        if (!(o instanceof Quad))
            return false;
 
        final Quad<?, ?, ?, ?> other = (Quad) o;
        return equal(a, other.a) && equal(b, other.b) && equal(c, other.c) && equal(d, other.d);
    }
    
    private static final boolean equal(Object o1, Object o2) {
        if (o1 == null) {
            return o2 == null;
        }
        return o1.equals(o2);
    }
 
    public int hashCode() {
        int ha = a == null ? 0 : a.hashCode();
        int hb = b == null ? 0 : b.hashCode();
        int hc = c == null ? 0 : c.hashCode();
        int hd = d == null ? 0 : d.hashCode();        
 
        return ha + (57 * hb) + (957 * hc) + (1453 * hd);
    }
}