package gr.neuropublic.mutil.base;

public interface IVisitorWithContext<T, V> {
	public void visit(T t, V context);
}
