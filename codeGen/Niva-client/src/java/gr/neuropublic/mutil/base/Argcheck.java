// Copyright (c) 2001 Hursh Jain (http://www.mollypages.org) 
// The Molly framework is freely distributable under the terms of an
// MIT-style license. For details, see the molly pages web site at:
// http://www.mollypages.org/. Use, modify, have fun !

package gr.neuropublic.mutil.base;

/** 
This class is intended primarily for checking whether method paramaters
meet various pre-conditions. Throws a <tt>IllegalArgumentException</tt> in 
case of failure. 
<p>
ThreadSafety: This class <b>is</b> thread safe and can be used by multiple threads 
concurrently.

@author		hursh jain
@version 	1.0
@date		5/30/2002
**/
public final class Argcheck 
{
protected static IArgcheckFailHandler handler = new BasicArgcheckFailHandler();

private Argcheck() { /* no instantiation */ }	

/**
Checks that the specified argument is false.
@param	val		value to be tested	
**/
public static void isfalse(boolean val) 
	{ 
	if (val) { 
		fail(null); 
		}
	}

/**
Checks that the specified argument is false.
@param	val		value to be tested	
@param	message	message for the exception
**/
public static void isfalse(boolean val, String message) 
	{ 
	if (val) {
		fail(message);
		}
	}

/**
Checks that the specified argument is true.
@param	val	value to be tested	
**/
public static void istrue(boolean val)
	{ 
	if (! val) {
		fail(null);
		}		
	}

/**
Checks that the specified argument is true.
@param	val	value to be tested	
@param	message	message for the exception
**/	
public static void istrue(boolean val, String message)
	{ 
	if (! val) {
		fail(message);
		}		
	}

/**
Checks that the specified argument is not null.
@param	obj	value to be tested	
**/
public static void notnull(Object obj) 
	{ 
	if (obj == null) {
		fail(null);
		}			
	}

/**
Checks that the specified argument is not null.
@param	obj	value to be tested	
@param	message	message for the exception
**/
public static void notnull(Object obj, String message)
	{ 
	if (obj == null) {
		fail(message);
		}			
	}

/**
Checks that the specified argument is null.
@param	obj	value to be tested	
**/
public static void isnull(Object obj)
	{ 
	if (obj != null) {
		fail(null);
		}			
	}

/**
Checks that the specified argument is null.
@param	obj	value to be tested	
@param	message	message for the exception
**/
public static void isnull(Object obj, String message)
	{ 
	if (obj != null) {
		fail(message);
		}			
	}

/**
Checks that the specified arguments are equal to each other by using 
<tt>Object.equals()</tt>.
@param	a	first value to be tested	
@param	b	second value to be tested	
**/
public static void isequal(Object a, Object b)
	{ 
	isequal(a, b, null);	
	}

/**
Checks that the specified arguments are equal to each other by using 
<tt>Object.equals()</tt>.
@param	a		first value to be tested	
@param	b		second value to be tested	
@param	message	message	message for the exception
**/
public static void isequal(Object a, Object b, String message)
	{ 
	/*	A			B						equals?
	1	null		notnull		forsure no, can also check using b.equals(a)
 	2  null			null		yes, cannot check using equals
 	3  notnull		null		forsure no, can also check using a.equals(b)
 	4  notnull		notnull		have to check using equals()
	*/
	if (a != b && a !=null && !a.equals(b)) { 
		fail(message); 
		}
	}  


/**
Checks that the specified arguments are not equal to each other by using 
<tt>Object.equals()</tt>.
@param	a		first value to be tested	
@param	b		second value to be tested	
**/
public static void notequal(Object a, Object b)
	{ 
	notequal(a,b,null);
	}

/**
Checks that the specified arguments are not equal to each other by using 
<tt>Object.equals()</tt>.
@param	a		first value to be tested	
@param	b		second value to be tested	
@param	message	message	message for the exception
**/
public static void notequal(Object a, Object b, String message)
	{
	/*  A			B						not equal ?
	1	null		notnull		forsure yes, can also check using b.equals(a)
	2 	null		null		no, cannot check using equals
	3 	notnull		null		forsure yes ,can also check using a.equals(b)
	4 	notnull		notnull		have to check using equals()
	*/
	if ( a == b || (a !=null && a.equals(b)) ) { 
		fail(message);
		}
	}

/** 
Checks to see if specified objects are the same object, that is, the
<b>references</b> are identical. This provides a stronger notion of 
equality than the <tt>Object.equals()</tt> method.
@param	a		first value to be tested	
@param	b		second value to be tested	
**/
public static void issame(Object a, Object b)
	{ 
	if (a != b) {
		fail(null);
		}			
	}

/** 
Checks to see if specified objects are the same object, that is, the
<b>references</b> are identical. This provides a stronger notion of 
equality than the <tt>Object.equals()</tt> method.
@param	a		first value to be tested	
@param	b		second value to be tested	
@param	message	message	message for the exception
**/
public static void issame(Object a, Object b, String message)
	{ 
	if (a != b) {
		fail(message);
		}		
	}

/** 
Checks to see if specified objects are not the same object, that is, the
<b>references</b> are not identical. 
@param	a		first value to be tested	
@param	b		second value to be tested	
**/
public static void notsame(Object a, Object b)
	{ 
	if (a == b) {
		fail(null);
		}			
	}

/** 
Checks to see if specified objects are not the same object, that is, the
<b>references</b> are not identical. 
@param	a		first value to be tested	
@param	b		second value to be tested	
@param	message	message	message for the exception
**/
public static void notsame(Object a, Object b, String message)
	{ 
	if (a == b) {
		fail(message);
		}			
	}

/**
Sets a different error handler.
@param	thehandler	the new error handler
@return	<tt>true</tt> is successful, <tt>false</tt> otherwise
**/
public static synchronized boolean setHandler(IArgcheckFailHandler thehandler)
	{
	if (thehandler != null) {
		handler	= thehandler;
		return true;
		}
	new Exception("Checker.setHandler(): could not set new handler because 'thehandler' paramater was null.").printStackTrace(System.err);
	return false;
	}

protected static void fail(String message) {
	handler.handle(message);
	}
}           //~class Argcheck
