package gr.neuropublic.mutil.base;

public class PrefixSnobber implements IStringFilter {

	private String prefix;
	public PrefixSnobber(String prefix) {
		this.prefix = prefix;
	}
	
	@Override
	public boolean accept(String s) {
		if (s.startsWith(prefix))
			return false;
		else return true;
	}


}
