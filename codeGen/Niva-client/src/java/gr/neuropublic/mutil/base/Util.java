package gr.neuropublic.mutil.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.security.SecureRandom;
import java.util.Set;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import java.util.Comparator;
import java.util.zip.GZIPOutputStream;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.DecimalFormatSymbols;
import java.math.BigDecimal;
import java.util.Locale;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.DateTime;

import java.util.logging.Logger;
import java.awt.GraphicsEnvironment;

public class Util {

	public static String safeLongString(Long l) {
		return safeLongString(l, "null");
	}
		
	public static String safeLongString(Long l, String nullValue) {
		if (l==null) return nullValue;
		else return l.toString();
	}

	public static int findFirst (byte[] bytes, byte[] pattern) {
		int i = 0;
		for (int j = 0 ; j < bytes.length ; j++) {
			if (bytes[j]==pattern[i])
				if (i==pattern.length-1)
					return j - pattern.length + 1;
				else
					i++;
			else
				i = 0;
		}
		return -1;
	}
	
	public static boolean allTrue(boolean ... xs) {
		for (boolean x : xs)
			if (!x) return false;
		return true;
	}
	
	public static String readLine (Logger l, InputStream in) {
		return readLine(l, in, null);
	}
	
	// read a line of text from an Input Stream
	public static String readLine (Logger l, InputStream in, ILineSniffer lineSniffer)
	{
		// reads a line of text from an InputStream
		StringBuffer data = new StringBuffer("");
		int c;
		
		try
		{
			// if we have nothing to read, just return null
			in.mark(1);
			if (in.read() == -1) {
				l.finest("about to return null");
				return null;
			}
			else
				in.reset();
			
			while ((c = in.read()) >= 0)
			{
				// check for an end-of-line character
				if ((c == 0) || (c == 10) || (c == 13))
					break;
				else
					data.append((char)c);
			}
		
			// deal with the case where the end-of-line terminator is \r\n
			if (c == 13)
			{
				in.mark(1);
				if (in.read() != 10)
					in.reset();
			}
		}  catch (Exception e)  {
			l.finest("exception inside readLine:"+Util.getCustomStackTrace(e));
		}
		// and return what we have
		l.finest("about to return data of length "+data.length()+":"+data.toString());
		if (lineSniffer!=null)
			lineSniffer.sniffLine(data.toString());
		return data.toString();
	}
	
	public static Object nullStr(Object a, String nullSubstitute) {
		if (a==null) return nullSubstitute;
		else return a;
	}
	
	public static String convertToHex(byte[] data) {
		return convertToHex(data, true);
	}
	
	public static String convertToHex(byte[] data, boolean lowerCaseHex) {
		return convertToHex(data, "", lowerCaseHex);
	}


	public static byte[] hexToBytes(String hexS) {
		return hexToBytes(hexS.toCharArray());
	}


	  public static byte[] hexToBytes(char[] hex) {
	    int length = hex.length / 2;
	    byte[] raw = new byte[length];
	    for (int i = 0; i < length; i++) {
	      int high = Character.digit(hex[i * 2], 16);
	      int low = Character.digit(hex[i * 2 + 1], 16);
	      int value = (high << 4) | low;
	      if (value > 127)
	        value -= 256;
	      raw[i] = (byte) value;
	    }
	    return raw;
	  }
	  
	public static String getStackTrace(Throwable aThrowable) {
	    final Writer result = new StringWriter();
	    final PrintWriter printWriter = new PrintWriter(result);
	    aThrowable.printStackTrace(printWriter);
	    return result.toString();
	  }

	public static String getCustomStackTrace(Throwable aThrowable) {
	    //add the class name and any message passed to constructor
	    final StringBuilder result = new StringBuilder( "Throwable: " );
	    result.append(aThrowable.toString());
	    final String NEW_LINE = System.getProperty("line.separator");
	    result.append(NEW_LINE);

	    //add each element of the stack trace
	    for (StackTraceElement element : aThrowable.getStackTrace() ){
	      result.append( element );
	      result.append( NEW_LINE );
	    }
	    return result.toString();
	  }
	
	public static String convertToHex(byte[] data, String betweenHexNumbers, boolean lowerCaseHex) { 
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) { 
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do { 
                if ((0 <= halfbyte) && (halfbyte <= 9)) 
                    buf.append((char) ('0' + halfbyte));
                else 
                    buf.append((char) ( (lowerCaseHex?'a':'A') + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while(two_halfs++ < 1);
            buf.append(betweenHexNumbers);
        } 
        return buf.toString();
	}
	
	public static byte[] sha(byte[] data) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA");
		md.update(data);
		return md.digest();
	}
	
	public static byte[] byteArray(int length, byte value) {
		byte[] retValue = new byte[length];
		for (int i = 0 ; i < retValue.length ; i++)
			retValue[i]=value;
		return retValue;
	}
	
	public static byte[] byteArray(int length, Random r) {
		byte[] retValue = new byte[length];
		for (int i = 0 ; i < retValue.length ; i++)
			retValue[i]=(byte) r.nextInt();
		return retValue;
	}
	
	public static String encodeBase64(byte[] in) {
		//return Base64.encodeBase64String(in);
		return Base64.encodeBase64URLSafeString(in);
	}
	
	public static byte[] decodeBase64(String s) {
		return Base64.decodeBase64(s);
	}
	
	public static boolean fileExistsInCurrentDirectory(Logger l, String filename) {
		
		File f = new File(System.getProperty("user.dir"), filename);
		if (f.exists()) {
			String msg = "file : "+f.getAbsolutePath()+" exists";
			if (l!=null) l.info(msg);
			else System.out.println(msg);
			return true;
		} else {
			String msg = "file : "+f.getAbsolutePath()+" does not exist";
			if (l!=null) l.info(msg);
			else System.out.println(msg);
			return false;
		}
	}
	

	public static int[] multiply(float a, int b[]) {
		int retValue[] = new int[b.length];
		for (int i = 0 ; i < retValue.length ; i++)
			retValue[i] = (int) (a*b[i]);
		return retValue;
	}
	
	public static void panicIfNotWithin(int a, int b, int x) {
		if (max2(a,b,x)!=max(a,b)) throw new RuntimeException(String.valueOf(a)+","+String.valueOf(b)+","+String.valueOf(x));
		if (min2(a,b,x)!=min(a,b)) throw new RuntimeException(String.valueOf(a)+","+String.valueOf(b)+","+String.valueOf(x));
	}
	
	public static void throwRuntimeExceptionIfNotTheSame(int ... values) {
		Integer prevValue = null; 
		for (int value : values) {
			if ((prevValue != null) && (!prevValue.equals(value))) throw new RuntimeException(Arrays.toString(values));
			prevValue = value;
		}
	}
	
	public static void throwRuntimeExceptionIfAnyIsNull(String msg, Object ... objects) {
		for (Object object : objects)
			if (object == null) throw new RuntimeException(msg);
	}
	
	public static boolean sameValues(int a[]) {
		for (int i = 0; i < a.length ; i++)
			if ((i>0) && (a[i-1]!=a[i])) return false;
		return true;
	}

    public static boolean anyIsNull(Object ... os) {
        for (Object o : os)
            if (o==null) return true;
        return false;
    }

    public static boolean allAreNull(Object ... os) {
        for (Object o : os)
            if (o!=null) return false;
        return true;
    }

    public static boolean theSame(Object obj1, Object obj2) {
        Object[] os = {obj1, obj2};
        return allTheSame(os);
    }

    public static boolean allTheSame(Object ...os) {
        if (anyIsNull(os))
            return allAreNull(os);
        else {
            if (os.length==0)
                throw new RuntimeException("collection is of zero size");
            Object refO = os[0];
            for (Object o : os)
                if (!refO.equals(o))
                    return false;
            return true;
        }
    }
	
	public static int[] multiply(int a[], int x) {
		int[] retValue = new int[a.length];
		for (int i = 0 ; i < a.length ; i++) {
			retValue[i] = a[i]*x;
		}
		return retValue;
	}
	
	public static void multiplyInPlaceDoesNotWork(List<Integer> list, final int n) {
		CollectionInPlaceTransformerDoesNotWork.dispatch(list, new ITransformer_1_to_1<Integer, Integer>() {

			@Override
			public Integer transform(Integer k) {
				return k*n;
			}

		});
	}
	
	public static List<Integer> multiply(List<Integer> list, final int n) {
		return Collection2ListTransformer.dispatch(list, new ITransformer_1_to_1<Integer, Integer>() {

			@Override
			public Integer transform(Integer k) {
				return k*n;
			}
		});
	}

	public static byte clip(byte x, byte floor, byte ceiling) {
		if (floor>ceiling) throw new RuntimeException(Util.join(x, floor, ceiling));
		if 				(x<floor) return floor;
		else if 		(x>ceiling) return ceiling;
		else return 	 x;
	}

	public static double clip(double x, double floor, double ceiling) {
		if (floor>ceiling) throw new RuntimeException();
		if 				(x<floor) return floor;
		else if 		(x>ceiling) return ceiling;
		else return 	 x;
	}

		
	public static int clip(int x, int floor, int ceiling) {
		if (floor>ceiling) throw new RuntimeException(Util.join(x, floor, ceiling));
		if 				(x<floor) return floor;
		else if 		(x>ceiling) return ceiling;
		else return 	 x;
	}
	
	public static void clip(byte[] xs, byte max) {
		for (int i = 0 ; i < xs.length ; i++)
			xs[i] = min(xs[i], max);
	}
	

	// tests that two positive integers are within a given percentage
	// more specically, that abs((a-b)/b) <= percentage
	public static boolean within(int value, int referenceToCalculatePercentageFrom, double percentage) {
		if ((value < 0) || (referenceToCalculatePercentageFrom < 0) || (percentage<0)) throw new RuntimeException();
		else if ( abs((double)value-(double)referenceToCalculatePercentageFrom) <= ((double) referenceToCalculatePercentageFrom)*percentage) return true;
		else return false;
	}
	
	public static boolean ordered(int a, int b, int c) {
		return ((b>=a) && (c>=b));
	}
	
	// divide x by n and round up
	public static long divRoundUp(long x, long n) {
		if (n<=0) throw new RuntimeException("has been conceived with positive dividers (was:"+n+")");
		long ret = (x+(n-1)*(x>0?1:0))/n;
		return ret;
	}
	
	// divide x by n and round up
	public static int divRoundUp(int x, int n) {
		return castToIntWithChecks(divRoundUp((long) x, (long) n));
	}
	
	// divide x by n and round down
	public static long divRoundDown(long x, long n) {
		if (n<=0) throw new RuntimeException("has been conceived with positive dividers (was:"+n+")");
		long ret = (x+(n-1)*(x<0?-1:0))/n;
		return ret;
	}
	
	// divide x by n and round down
	public static int divRoundDown(int x, int n) {
		return castToIntWithChecks(divRoundDown((long) x, (long) n));
	}
	
	// round x up to to the lowest integer x2 such that x2>=x x2%n==0
	public static long roundUp(long x, long n) {
		long ret = n*divRoundUp(x, n);
		return ret;
	}
	
	
	// round x up to to the lowest integer x2 such that x2>=x x2%n==0
	public static int roundUp(int x, int n) {
		return castToIntWithChecks(roundUp((long) x, (long) n));
	}
	
	// round x down to to the lowest integer x2 such that x2>=x x2%n==0
	public static long roundDown(long x, long n) {
		long ret = n*divRoundDown(x, n);
		return ret;
	}
	
	// round x down to to the lowest integer x2 such that x2>=x x2%n==0
	public static int roundDown(int x, int n) {
		return castToIntWithChecks(roundDown((long) x, (long) n));
	}
	
	public static <T> T[] Array(T...elems){
		return elems;
	}
	
	public static <T> boolean isEqualToOneOf(T a, T ...ts) {
		for (T t: ts)
			if (a.equals(t))
				return true;
		return false;
	}
	
    public static int proportionMethod(Integer currentValue, double maximumValue, double maximum) {
        return (int) (maximum * (double) ((double) currentValue / maximumValue));
    }

	public static int[] mirror(int[] series) {
		int[] retValue = new int[series.length];
		int _i = 0 ;
		for (int i : series) {
			retValue[_i++] = -i;
		}
		return retValue;
	}
	
	public static int[] stringArraytoIntArray(String[] sarray) throws Exception {
		if (sarray != null) {
			int intarray[] = new int[sarray.length];
			for (int i = 0; i < sarray.length; i++) {
				intarray[i] = Integer.parseInt(sarray[i]);
			}
			return intarray;
		}
		return null;
	}

	
	public static String msse_to_yyyy_mm_dd(long msse) {
		Date date = new Date(msse);
		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd");
		return sdfDestination.format(date);
	}

	public static int[] split(int a [][], int colNo) {
		int[] retValue = new int[a.length];
		for (int i = 0 ; i < a.length ; i++)
			retValue[i]=a[i][colNo];
		return retValue;
	}
	public static Pair<Integer, Integer> divideInTwo(int a) {
		int a1 = a/2;
		int a2 = a - a1;
		return Pair.create(a1, a2);
	}
	public static String join(int i, int j) {
				  return join(    i,     j,              ",");}
	public static String join(int i, int j, String separator) {
		return StringUtils.join(objectify(new int[]{i, j}), separator);
	}
	public static String join(int i, int j, int k) {
		return 			 join(    i,     j,     k,              ",");}
	public static String join(int i, int j, int k, String separator) {
		return StringUtils.join(objectify(new int[]{i, j, k}), separator);
	}
	
	public static String join(int i, int j, int k, int l) {
				  return join(    i,     j,     k,     l,              ",");	}
	public static String join(int i, int j, int k, int l, String separator) {
		return StringUtils.join(objectify(new int[]{i, j, k, l}), separator);
	}
	
	public static String join(int i, int j, int k, int l, int m) {
		  return join(    i,     j,     k,     l,                             ",");	}
	public static String join(int i, int j, int k, int l, int m, String separator) {
		return StringUtils.join(objectify(new int[]{i, j, k, l, m}), separator);
	}
	
	public static String join(int i, int j, int k, int l, int m, int n) {
		  return join(    i,     j,     k,     l,   n,                              ",");	}
	public static String join(int i, int j, int k, int l, int m, int n, String separator) {
		return StringUtils.join(objectify(new int[]{i, j, k, l, m, n}), separator);
	}
	
	public static String join(                  int i, int j, int k, int l, int m, int n, int o) {
		  return         join(                      i,     j,     k,     l,     m,     n,     o , ",");	}
	public static String join(                  int i, int j, int k, int l, int m, int n, int o, String separator) {
		return StringUtils.join(objectify(new int[]{i,     j,     k,     l,     m,     n,     o}), separator);
	}
	
	public static String join(                  int i, int j, int k, int l, int m, int n, int o, int p) {
		  return         join(                      i,     j,     k,     l,     m,     n,     o , p, ",");	}
	public static String join(                  int i, int j, int k, int l, int m, int n, int o, int p, String separator) {
		return StringUtils.join(objectify(new int[]{i,     j,     k,     l,     m,     n,     o,     p}), separator);
	}


	
	public static String[] list2array(List<String> strings) {
		String[] retValue = new String[strings.size()];
		int __i = 0;
		for (String string : strings) 
			retValue[__i++] = string;
		return retValue;
	}
	
	public static void stringToFileAsMultipleLines(String multilineString, File f) {
		final String LINE_SEPARATOR = System.getProperty("line.separator");
		try {
			FileWriter fWr= new FileWriter(f);
			for (String aLine : multilineString.split(LINE_SEPARATOR)) { 
				fWr.write(aLine);
				fWr.write(LINE_SEPARATOR);
			}
			fWr.close();
		} catch (Exception e) {
			throw new ExceptionAdapter(e);
		}		
	}
	
	public static int max_0 (int [][] a) {
		int retValue = Integer.MIN_VALUE;
		for (int i = 0 ; i < a.length ; i ++)
			if (a[i][0] > retValue)
				retValue = a[i][0];
		return retValue;
	}
	
	public static int max_1 (int [][] a) {
		int retValue = Integer.MIN_VALUE;
		for (int i = 0 ; i < a.length ; i ++)
			if (a[i][1] > retValue)
				retValue = a[i][1];
		return retValue;
	}
	
	
	public static int max(int a[]) {
		if (a.length == 0) throw new RuntimeException();
		else {
			int maxSoFar = Integer.MIN_VALUE;
			for (int i = 0 ; i < a.length ; i++)
				maxSoFar = max (maxSoFar, a[i]);
			return maxSoFar;
		}
	}
	
	public static byte max(byte a[]) {
		if (a.length == 0) throw new RuntimeException();
		else {
			byte maxSoFar = Byte.MIN_VALUE;
			for (int i = 0 ; i < a.length ; i++)
				maxSoFar = max (maxSoFar, a[i]);
			return maxSoFar;
		}
	}
	
	public static long max(long a[]) {
		if (a.length == 0) throw new RuntimeException();
		else {
			long maxSoFar = Long.MIN_VALUE;
			for (int i = 0 ; i < a.length ; i++)
				maxSoFar = max (maxSoFar, a[i]);
			return maxSoFar;
		}
	}
	
	public static float max(float a[]) {
		if (a.length == 0) throw new RuntimeException();
		else {
			float maxSoFar = Float.NEGATIVE_INFINITY;
			for (int i = 0 ; i < a.length ; i++)
				maxSoFar = max (maxSoFar, a[i]);
			return maxSoFar;
		}
	}
	
	public static int[][] listOfPairsInt_2_2DArray(List<Pair<Integer, Integer>> listOfPairsInt) {
		int[][] retValue = new int[listOfPairsInt.size()][2];
		int __i = 0;
		for (Pair<Integer, Integer> aPair : listOfPairsInt) {
			retValue[ __i	][ 0 ] = aPair.a;
			retValue[ __i++	][ 1 ] = aPair.b;
		}
		return retValue;
	}
	
	public static int max(int a[], int b[]) {
		return max(max(a), max(b));
	}
	
	public static int max(int a[], int b[], int c[]) {
		return max(max(a, b), max(c));
	}
	
	public static int max(int a[], int b[], int c[], int d[]) {
		return max(max(a, b, c), max(d));
	}

	
	public static int min(int a[]) {
		if (a.length == 0) throw new RuntimeException();
		else {
			int minSoFar = Integer.MAX_VALUE;
			for (int i = 0 ; i < a.length ; i++)
				minSoFar = min(minSoFar, a[i]);
			return minSoFar;
		}
	}
	
	public static byte min(byte a[]) {
		if (a.length == 0) throw new RuntimeException();
		else {
			byte minSoFar = Byte.MAX_VALUE;
			for (int i = 0 ; i < a.length ; i++)
				minSoFar = min(minSoFar, a[i]);
			return minSoFar;
		}
	}
	
	public static float min(float a[]) {
		if (a.length == 0) throw new RuntimeException();
		else {
			float minSoFar = Float.POSITIVE_INFINITY;
			for (int i = 0 ; i < a.length ; i++)
				minSoFar = min(minSoFar, a[i]);
			return minSoFar;
		}
	}
	
	public static long min(long a[]) {
		if (a.length == 0) throw new RuntimeException();
		else {
			long minSoFar = Long.MAX_VALUE;
			for (int i = 0 ; i < a.length ; i++)
				minSoFar = min(minSoFar, a[i]);
			return minSoFar;
		}
	}
	
	public static int min(int a[][]) {
		if (a.length == 0) throw new RuntimeException();
		else {
			int minSoFar = Integer.MAX_VALUE;
			for (int i = 0 ; i < a.length ; i++)
				minSoFar = min(minSoFar, min(a[i]));
			return minSoFar;
		}
	}
	
	public static int first_max_index(int a[]) {
		int max = max(a);
		for (int i = 0 ; i<a.length ; i++)
			if (a[i]==max)
				return i;
		throw new RuntimeException("should never be here");
		// Util.panic("should never be here"); //TODO: a bug in the Java compiler doesn't allow me to call the panic method
	}
	
	public static int first_max_index(long a[]) {
		long max = max(a);
		for (int i = 0 ; i<a.length ; i++)
			if (a[i]==max)
				return i;
		throw new RuntimeException("should never be here");
		// Util.panic("should never be here"); //TODO: a bug in the Java compiler doesn't allow me to call the panic method
	}

	
	public static int first_max_index(float a[]) {
		float max = max(a);
		for (int i = 0 ; i<a.length ; i++)
			if (a[i]==max)
				return i;
		throw new RuntimeException("should never be here");
		// Util.panic("should never be here"); //TODO: a bug in the Java compiler doesn't allow me to call the panic method
	}
	
	public static long max(long a, long b) {
		if (a>b) return a;
		else return b;
	}
	
	public static float max(float a, float b) {
		if (a>b) return a;
		else return b;
	}


	public static Pair<Boolean, Integer> arraysHaveTheSameLength(int[] t1, int[] t2) {
		return arraysHaveTheSameLength(objectify(t1), objectify(t2));
	}
	
	public static <T1, T2> Pair<Boolean, Integer> arraysHaveTheSameLength(T1[] t1, T2[] t2) {
		return Pair.create((t1.length == t2.length), t1.length);
	}

	public static Pair<Boolean, Integer> arraysHaveTheSameLength(int[] t1, int[] t2, int[] t3) {
		return arraysHaveTheSameLength(objectify(t1), objectify(t2), objectify(t3));
	}

	public static <T1, T2, T3> Pair<Boolean, Integer> arraysHaveTheSameLength(T1[] t1, T2[] t2, T3[] t3) {
		return Pair.create((t1.length == arraysHaveTheSameLength(t2, t3).b), t1.length);
	}
	
	public static <T1, T2, T3, T4> Pair<Boolean, Integer> arraysHaveTheSameLength(T1[] t1, T2[] t2, T3[] t3, T4[] t4) {
		return Pair.create((t1.length == arraysHaveTheSameLength(t2, t3, t4).b), t1.length);
	}

	public static <T1, T2, T3, T4, T5> Pair<Boolean, Integer> arraysHaveTheSameLength(T1[] t1, T2[] t2, T3[] t3, T4[] t4, T5[] t5) {
		return Pair.create((t1.length == arraysHaveTheSameLength(t2, t3, t4, t5).b), t1.length);
	}

	public static Pair<Boolean, Integer> integersAreTheSame(int i1, int i2, int ... otherIs) {
		if (i1!=i2) return Pair.create(false, null);
		else
			for (int otherI : otherIs) {
				if (i2!=otherI)
					return Pair.create(false, null);
			}
		return Pair.create(true, i2);
	}
	
	public static Pair<Boolean, Integer> integersAreTheSame(Pair<Boolean, Integer> prev, int ...otherIs) {
		if (!prev.a) return prev;
		else {
			panicIfNull(prev.b);
			for (int i : otherIs) {
				if (prev.b != i)
					return Pair.create(false, null);
			}
		}
		return prev;
	}
	/*
	public static Pair<Boolean, Integer> integersAreTheSame(int i1, int i2) {
		return Pair.create((i1==i2),
				           (i1==i2)?i1:null);
	}
	
	public static Pair<Boolean, Integer> integersAreTheSame(int i1, int i2, int i3) {
		return Pair.create((i1 == integersAreTheSame(i2, i3).b),
				           (i1 == integersAreTheSame(i2, i3).b)?i1:null); 
	}
	
	public static Pair<Boolean, Integer> integersAreTheSame(int i1, int i2, int i3, int i4) {
		return Pair.create((i1 == integersAreTheSame(i2, i3, i4).b),
				           (i1 == integersAreTheSame(i2, i3, i4).b)?i1:null); 
	}
	
	public static Pair<Boolean, Integer> integersAreTheSame(int i1, int i2, int i3, int i4, int i5) {
		return Pair.create((i1 == integersAreTheSame(i2, i3, i4, i5).b),(
				            i1 == integersAreTheSame(i2, i3, i4, i5).b)?i1:null); 
	}
	
	public static Pair<Boolean, Integer> integersAreTheSame(int i1, int i2, int i3, int i4, int i5, int i6) {
		return Pair.create((i1 == integersAreTheSame(i2, i3, i4, i5, i6).b),(
				            i1 == integersAreTheSame(i2, i3, i4, i5, i6).b)?i1:null); 
	}
	
	public static Pair<Boolean, Integer> integersAreTheSame(int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
		return Pair.create((i1 == integersAreTheSame(i2, i3, i4, i5, i6, i7).b),(
				            i1 == integersAreTheSame(i2, i3, i4, i5, i6, i7).b)?i1:null); 
	}
	*/
	
	public static boolean noNegativeElements(int[] a) {
		for (int i = 0 ; i < a.length ; i++)
			if (a[i] < 0) return false;
		return true;
	}
	
	public static boolean isDominatedBy(int[] dominated, int[] dominus) {
		if (dominated.length != dominus.length) throw new RuntimeException(dominated.length+"!="+dominus.length);
		for (int i = 0 ; i < dominated.length ; i++)
			if (!(dominated[i]<=dominus[i]))
			return false;
		return true;
	}
	
	public static void subtractInPlaceNotBelowZero(final int[] a, final int[] b) {
		if (a.length != b.length) throw new RuntimeException();
		for (int i = 0 ; i < a.length ; i++)
			a[i] -= b[i]<a[i]?b[i]:a[i];
	}
	
	public static int[] subtract(final int a[], final int b[]) {
		if (a.length != b.length) throw new RuntimeException();
		int[] retValue = new int[a.length];
		for (int i = 0 ; i < a.length ; i++)
			retValue[i] = a[i]-b[i];
		return retValue;
	}
	
	public static int[] copy(int[] a) {
		return copyWtAdd(a, 0);
	}
	
	public static int[] copyWtAdd (int[] a, int add) {
		int[] retValue = new int[a.length];
		for (int i = 0 ; i < a.length ; i++)
			retValue[i] = a[i]+add;
		return retValue; 
	}
	
	
	public static int[] crArrWt3LinearSegments(int overallLength,
									int baseA,	  // at index 0 of the returned array the value should be baseA
									int	stepA,
									int thresA_B, // index of the returned array where value should be baseB
									int baseB,
									int stepB,
									int thresB_C, // index of the returned array where value should be baseC
									int baseC,
									int stepC) {
		// sanity checks
		if (!(thresA_B >	    0)) 		throw new RuntimeException(String.valueOf(thresA_B));
		if (!(thresB_C > thresA_B)) 		throw new RuntimeException(String.valueOf(thresB_C)+"!>"+String.valueOf(thresA_B));
		if (  thresB_C >=overallLength)		throw new RuntimeException(String.valueOf(thresB_C)+">="+String.valueOf(overallLength));

		int[] retValue = new int[overallLength];
		for (int i = 0 		  ; i < thresA_B 		; i++)	retValue[i]=baseA+(i-       0)*stepA;
		for (int i = thresA_B ; i < thresB_C 		; i++)	retValue[i]=baseB+(i-thresA_B)*stepB;
		for (int i = thresB_C ; i < overallLength 	; i++)	retValue[i]=baseC+(i-thresB_C)*stepC;
		return retValue;
	}
	
	public static <T extends Comparable<? super T>> T getImmediatelyGreaterInSet(T aT, Set<T> setOfTs) {
		List<T > listOfTs = new ArrayList<T>(setOfTs);
		Collections.sort(listOfTs);
		for (T t : listOfTs) {
			if (t.compareTo(aT)>0) return t;
		}
		return null;
	}
	
	public static int abs(int x) {
		if (x < 0)
			return -x;
		else return x;
	}
	
	public static int[] abs(int[] x) {
		int[] retValue = new int[x.length];
		for (int i = 0; i < x.length ; i++)
			retValue[i] = abs(x[i]);
		return retValue;
	}


	public static double abs(double x) {
		if (x < 0)
			return -x;
		else return x;
	}
	
	public static float abs(float x) {
		if (x < 0)
			return -x;
		else return x;
	}
	
	public static long abs(long x) {
		if (x < 0)
			return -x;
		else return x;
	}
	
	public static long aNNLong (long range, Random r) { // returns a non-negative long
		return (long) (r.nextDouble()*range);
	}
	
	public static Triad<Long, Long, Long> randomlySplit(long x) {
		panicIf(x<0);
		Random r = new Random(System.currentTimeMillis());
		long l1 = aNNLong(x, r);
		long l2 = aNNLong(x - l1, r);
		long l3 = x - l1 -l2;
		panicIf(x!=l1+l2+l3);
		return Triad.create(l1, l2, l3);
	}
	
	public static int[] linearArray(int length) 					 {return linearArray(length, 0, 1)   ;}
	public static int[] linearArray(int length, int step) 			 {return linearArray(length, 0, step);} 
	/*public static int[] linearArray(int length, int start, int step) {
		List<Integer> list = new ArrayList<Integer>();
		int value = start;
		for (int i = 0 ; i < length ; i++) {
			list.add(value);
			value += step;
		}
		return ArrayUtils.toPrimitive(list.toArray(new Integer[0]));
	}*/
	public static int[] linearArray(int length, int start, int step) {
		int retValue[] = new int[length];
		int value = start;
		for (int i = 0 ; i < length ; i++) {
			retValue[i]=value;
			value += step;
		}
		return retValue;
	}
	
	public static int[] randomArray(long seed, int length, int from, int to, int start, int every, int upTo) {
		Random r = new Random(seed);
		int retValue[] = new int[length];
		int value = start;
		for (int i = 0 ; i < length ; i++) {
			if (i%every==0) {
				value += r.nextInt(upTo)-upTo/2;
				value = clip(value, from, to);
			}
			retValue[i]=value;
		}
		return retValue;
	}
	
	
	public static byte[] randomByteArray(long seed, int length, byte from, byte to, byte start, int every, byte upTo) {
		Random r = new Random(seed);
		byte retValue[] = new byte[length];
		byte value = start;
		for (int i = 0 ; i < length ; i++) {
			if (i%every==0) {
				value += r.nextInt(upTo)-upTo/2;
				value = clip(value, from, to);
			}
			retValue[i]=value;
		}
		return retValue;
	}

	public static double[] randomDoubleArray(long seed, int length, double from, double to, double start, int every, double upTo) {
		Random r = new Random(seed);
		double retValue[] = new double [length];
		double value = start;
		for (int i = 0 ; i < length ; i++) {
			if (i%every==0) {
				double nextD = r.nextDouble();
				value += (nextD-0.5)*upTo;
				value = clip(value, from, to);
			}
			retValue[i]=value;
		}
		return retValue;
	}


	
	public static int[] linearArray(int length, int start, int step, int addStepEverySoManyCells) {
		int retValue[] = new int[length];
		int value = start;
		int addStep = 1;
		for (int i = 0 ; i < length ; i++) {
			retValue[i]=value;
			if (addStep++%addStepEverySoManyCells==0) value += step;
		}
		return retValue;
	}

	public static boolean startsIn(String value, String[] bag) {
		for (String s: bag) {
			if (value.startsWith(s))
				return true;
		}
		return false;
	}
	
	public static Integer[] objectify(int[] is) {
		Integer[] Is = new Integer[is.length];
		int __i=0;
		for (int i: is) {
			Is[__i++] = i;
		}
		return Is;
	}
	
	public static Byte[] objectify(byte[] is) {
		Byte[] Is = new Byte[is.length];
		int __i=0;
		for (byte i: is) {
			Is[__i++] = i;
		}
		return Is;
	}

	public static Character[] objectify(char[] is) {
		Character[] Is = new Character[is.length];
		int __i=0;
		for (char i: is) {
			Is[__i++] = i;
		}
		return Is;
	}	
	// the null-lenient version of objectify (hence the trailing '_n'
	public static Integer[] objectify_n(int[] is) {
		if (is==null)return null;
		else return objectify(is);
	}
	
	public static String[] adornWith(List<String> strings, char c) {
		String[] unadorned = new String[strings.size()];
		int _i = 0;
		for (String string : strings)
			unadorned[_i++] = c+string+c;
		return unadorned;
	}
	

	public static int sum(int[] intervals, boolean lenient) {
		long retValue = 0;
		for (int interval : intervals)
			retValue += interval;
		if (lenient) return (int) retValue;
		else return castToIntWithChecks(retValue);
	}
	

	public static int sum(int[] intervals) {
		return sum(intervals, false);
	}
	
	public static long sum(long [] xs) {
		return sum(xs, xs.length-1);
	}

	// note: overflow possible and goes undetected
	public static long sum(long [] xs, long upTo) {
		panicIf(upTo > xs.length-1);
		long retValue = 0;
		for (int _i = 0 ; _i <= upTo ; _i++)
			retValue += xs[_i];
		return retValue;
	}


	
	public static float sum(float[] fs) {
		float sum = 0;
		for (float f:fs)
			sum += f;
		return sum;
	}
	
	public static int[] add(int[] a, int b) {
		int[] retValue = new int[a.length];
		for (int i = 0; i < retValue.length ; i++)
			retValue[i] = a[i]+b;
		return retValue;
	}
	
	public static int[] add(int[] a, int[] b) {
		return add(a, b, false);
	}
	
	// add b[] on top of a[]. Allow a[] to be longer (only if lenient)
	public static int[] add(int[] a, int[] b, boolean lenient) {
		if ((a.length != b.length) && (!lenient)) throw new RuntimeException();
		int[] retValue = new int[a.length];
		for (int i = 0 ; i < b.length ; i++)
			retValue[i] = a[i]+b[i];
		return retValue;
	}

	
	public static int[] add(int[] a, int shift, int [] b) {
		if (shift < 0) throw new RuntimeException();
		if (shift+b.length > a.length) throw new RuntimeException();
		int[] retValue = new int[a.length];
		for (int i = 0; i < retValue.length ; i++)
			retValue[i] = a[i] + (i<shift?
									0:
									(i-shift<b.length?
											b[i-shift]:
											0
											));
		return retValue;
	}
	
	public static int[] addInPlace(int[] a, int shift, int [] b) {
		if (shift < 0) throw new RuntimeException();
		if (shift+b.length > a.length) throw new RuntimeException();
		for (int i = 0; i < a.length ; i++)
			a[i] += 				(i<shift?
									0:
									(i-shift<b.length?
											b[i-shift]:
											0
											));
		return a;
	}

	// this function implicitly returns a 0 if passed a null array
	public static int sumz(int[] intervals) {
		if (intervals == null)
			return 0;
		else 
			return sum(intervals);
	}

	public static long min(long a, long b) {
		return a<b?a:b;
	}
	
	public static int min(int a, int b) {
		return a<b?a:b;
	}
	
	
	public static byte min(byte a, byte b) {
		return a<b?a:b;
	}
	
	public static float min(float a, float b) {
		return a<b?a:b;
	}
	
	public static int max(int a, int b) {
		return a>b?a:b;
	}
	
	public static byte max(byte a, byte b) {
		return a>b?a:b;
	}
	
	public static double max(double a, double b) {
		return a>b?a:b;
	}
	
	
	
	public static int max2(int ... xs) {
		int retValue = Integer.MIN_VALUE;
		for (int x : xs)
			retValue = max(retValue, x);
		return retValue;
	}
	
	public static int min2(int ... xs) {
		int retValue = Integer.MAX_VALUE;
		for (int x : xs)
			retValue = min(retValue, x);
		return retValue;
	}
	
	public static <T> T checkNotNullAndReturn(T t) {
		if (t==null)
			throw new RuntimeException();
		else
			return t;
	}
	
	public static void panicIfNot(boolean x) {
		panicIf(!x);
	}
	
	public static void panicIf(boolean x) {
		panicIf("tested value was true", x);
	}
	
	public static void panicIf(String msg, boolean x) {
		panicIfAny(msg, x);
	}
        public static void panicIf(boolean x, String msg) {
            if (x) throw new RuntimeException(msg);
	}
	public static void panicIfAny(boolean ...xs) {
		panicIfAny((String) null, xs);
	}
	public static void panicIfAny(String msg, boolean ...xs) {
		for (boolean x : xs)
			if (x) panic(msg);
	}
	
	public static void panic() {
		panic(null);
	}
	
	public static void panic(String msg) {
		if (msg!=null)
			throw new RuntimeException(msg);
		else
			throw new RuntimeException();
	}
	
	public static <T> void panicIfNull(T t) {
		if (t==null)
			throw new RuntimeException();
	}
	
	public static <T> void panicIfNotNull(T t, String msg) {
		if (t!=null)
			throw new RuntimeException(msg);
	}


	public static void panicIfZero(int ... xs) {
		for (int x : xs)
			if (x==0) throw new RuntimeException(String.valueOf(x));
	}
	

	public static void panicIfNotZeroL(long ... xs) {
		for (long x : xs)
			if (x!=0L) throw new RuntimeException(String.valueOf(x));
	}

	
	public static void panicIfNotZero(int ... xs) {
		for (int x : xs)
			if (x!=0) throw new RuntimeException(String.valueOf(x));
	}
	
	public static void panicIfEqual(int ... xs) {
		int min = min(xs);
		int max = max(xs);
		panicIfZero(min-max);
	}

	
	public static void panicIfNotEqual(int ... xs) {
		int min = min(xs);
		int max = max(xs);
		panicIfNotZero((int) (min-max));
	}
	
	public static void panicIfNotEqual(String ... xs) {
		String prevx = null;
		for (String x : xs) {
			if (prevx == null)
				prevx = x;
			else if (!prevx.equals(x)) throw new RuntimeException(prevx+" not equal to "+x);
		}
	}
	
	public static void panicIfNotEqualL(long ... xs) {
		long min = min(xs);
		long max = max(xs);
		panicIfNotZeroL((min-max));
	}
	
	public static void panicIfLessThanZero(int ... xs) {
		panicIfLessThan(0, xs);
	}
	
	public static void panicIfLessThanOrEqualToZero(int ... xs) {
		panicIfLessThan(1, xs);
	}
	
	public static void panicIfLessThan(int atLeast, int ... xs) {
		for (int x : xs)
			if (x<atLeast) throw new RuntimeException(String.valueOf(x)+"<"+String.valueOf(atLeast)+"!");
			
	}
	
	public static void panicIfMoreThan(int atTheMost, int ... xs) {
		for (int x : xs)
			if (x>atTheMost) throw new RuntimeException(String.valueOf(x)+">"+String.valueOf(atTheMost)+"!");
			
	}
	
	public static <T> void panicIfNotNull(T...ts) {
		for (T t : ts)
			if (t!=null)
				throw new RuntimeException(t.toString());
	}
	
	public static <T> void panicIfNotOneOf(T actual, T ... hasToBeOneOfUs) {
		for (T oneOf : hasToBeOneOfUs)
			if (actual.equals(oneOf)) return;
		throw new RuntimeException(actual+" was not one of: "+StringUtils.join(hasToBeOneOfUs, ','));
	}
	

	public static int checkGreaterThanOrEqualTo(int a, int threshold) {
		return checkGreaterThanOrEqualTo(a, threshold, false);
	}
	
	public static int checkGreaterThanOrEqualTo(int a, int threshold, boolean strict) {
		if (strict && (!(a >= threshold)))
			throw new RuntimeException ("value of "+a+" is not greater than or equal to"+threshold);
		else return a;
	}
	
	public static String stringifyListOfPairs(List<Pair<Integer, Integer>> lp) {
		StringBuilder sb = new StringBuilder();
		sb.append('{');
		Iterator<Pair<Integer, Integer>> IIArrIt = lp.iterator();
		while (IIArrIt.hasNext()) {
			Pair<Integer, Integer> pair_a_b = IIArrIt.next();
			sb.append("["+pair_a_b.a+","+pair_a_b.b+")]");
			if (IIArrIt.hasNext()) sb.append(',');
		}
		sb.append('}');
		return sb.toString();
	}
	
	public static String stringify(List<Pair<Integer, Integer[]>> lp) {
		StringBuilder sb = new StringBuilder();
		sb.append('{');
		Iterator<Pair<Integer, Integer[]>> IIArrIt = lp.iterator();
		while (IIArrIt.hasNext()) {
			Pair<Integer, Integer[]> pair_a_b = IIArrIt.next();
			sb.append("["+pair_a_b.a+",("+StringUtils.join(pair_a_b.b, ',')+")]");
			if (IIArrIt.hasNext()) sb.append(',');
		}
		sb.append('}');
		return sb.toString();
	}
	
	// it has to be named differently cause the erasure is the same with the previous stringify
	public static String stringify2(List<Triad<String, Integer, Integer[]>> lp) {
		StringBuilder sb = new StringBuilder();
		sb.append('{');
		Iterator<Triad<String, Integer, Integer[]>> IIArrIt = lp.iterator();
		while (IIArrIt.hasNext()) {
			Triad<String, Integer, Integer[]> triad_a_b_c = IIArrIt.next();
			sb.append("["+triad_a_b_c.a+','+triad_a_b_c.b+",("+StringUtils.join(triad_a_b_c.c, ',')+")]");
			if (IIArrIt.hasNext()) sb.append(',');
		}
		sb.append('}');
		return sb.toString();
	}
	
	
	public static String stringify3(List<Triad<String, Integer, List<Integer>>> lp) {
		StringBuilder sb = new StringBuilder();
		sb.append('{');
		Iterator<Triad<String, Integer, List<Integer>>> IIArrIt = lp.iterator();
		while (IIArrIt.hasNext()) {
			Triad<String, Integer, List<Integer>> triad_a_b_c = IIArrIt.next();
			sb.append("["+triad_a_b_c.a+','+triad_a_b_c.b+",("+StringUtils.join(triad_a_b_c.c, ',')+")]");
			if (IIArrIt.hasNext()) sb.append(',');
		}
		sb.append('}');
		return sb.toString();
	}

    public static <T> String stringify4(List<T> foos, String separator) {
        StringBuilder sb = new StringBuilder();
        for (T foo : foos) {
            sb.append(String.format("%s%s", foo.toString(), separator));
        }
        return sb.toString();
    }

    public static <T> String stringify4(List<T> foos, IStringifier<T> stringifier, String separator) {
        StringBuilder sb = new StringBuilder();
        for (T foo : foos) {
            sb.append(String.format("%s%s", stringifier.toString(foo), separator));
        }
        return sb.toString();
    }
	
	
	public static int[] normalize(int[] a, int weight) {
		return normalize(a, weight, null);
	}

	public static int[] normalize(int[] a, int weight, Random r) {
		for (int i = 0 ; i < a.length ; i++)
			if (a[i] < 0) throw new RuntimeException();
		if (weight == 0)
			return new int[a.length];
		else {
			int s0 = sum(a); // original sum
			if (s0==0) return a; // if original array all in zeros cannot normalize
			else {
				int[] normalized = new int[a.length]; 
				for (int i = 0 ; i < a.length ; i++) {
					normalized[i] = (a[i]*weight/s0);
				}
				int s = sum(normalized);
				if (s > weight) throw new RuntimeException();
				if (s < weight) {
					int delta = weight-s;
					if (delta > a.length) {
						Logger l = Logger.getGlobal();
						l.severe  ("["+StringUtils.join(objectify(a), ',')+"], "+weight+"-->["+StringUtils.join(objectify(normalized))+"], "+delta);
						throw new RuntimeException();
					}
					if (r==null) // don't randomize: allocate the remaining delta starting at the start of the array
						for (int i = 0 ; i < delta ; i++)
							normalized[i] += 1;
					else {		// randomize: allocate the remaining delta randomly within the array but 
						 		// not twice in the same position
						boolean usedPos[] = new boolean[normalized.length];
						while (delta-- > 0) {
							while (true) {
								int 		     pos = r.nextInt(normalized.length);
								if (!usedPos	[pos]) {
									normalized	[pos]	+= 1;
									usedPos		[pos] 	=  true;
									break;
								}
							}
						}
					}
				}
				return normalized;
			}
		}
	}
	
    public static void addArrays(int base[], int top[]) {
    	addArrays(base, top, true);
    }
    public static void addArrays(int base[], int top[], boolean strictOnTopOverruns) {
    	final int BASE_LENGTH = base.length; 
    	for (int i = 0 ; i < top.length ; i ++)
    		if (i>BASE_LENGTH-1) {
    			if (strictOnTopOverruns) throw new RuntimeException();
    			else break;
    		}
    		else
    			base[i]+=top[i];
    }
    
    public static int[] toPrimitiveArray(Integer[] objArr) {
    	int[] retValue = new int[objArr.length];
    	int __i = 0;
    	for (Integer iObj : objArr)
    		retValue[__i++] = iObj;
    	return retValue;
    }
    
    public static char[] toPrimitiveArray(Character[] objArr) {
    	char[] retValue = new char[objArr.length];
    	int __i = 0;
    	for (Character iObj : objArr)
    		retValue[__i++] = iObj;
    	return retValue;
    }

    
    public static int[] cumulative(int base[], int zeroPoints[]) {
    	return cumulative(base, zeroPoints, true);
    }
    
    // the parameter 'strict' governs whether we mind if a zero point is provided that
    // exceeds the base array. In such a case the said zero point wouldn't matter at all
    // and we shouldn't really mind except that it might indicate a bug in the calling context.
    public static int[] cumulative(int base[], int restartPoints[], boolean strict) {
    	int[] retValue = new int[base.length];
    	Set<Integer> setOfZeroPoints = new HashSet<Integer>();
    	if (restartPoints != null)
	    	for (int i: restartPoints) {
	    		if (i < 0)
	    			throw new RuntimeException("i ("+i+") <0");
	    		else if ((i > base.length) && (strict))
	    			throw new RuntimeException("i ("+i+")>base.length ("+base.length+")");
	    		else
	    			setOfZeroPoints.add(i);
	    	}
    	for (int i = 0 ; i < retValue.length ; i++)
    		if (setOfZeroPoints.contains(i))
    			retValue[i] = base[i];
    		else
    			retValue[i] = base[i]+ (i>0?retValue[i-1]:0);
    	return retValue;
    }
	
	private static String zeroPreffixedIntegerUpToTwoDigits(int i) {
		return zeroPreffixedIntegerUpToNDigits(2, i);
	}
	
	private static String zeroPreffixedIntegerUpToFourDigits(int i) {
		return zeroPreffixedIntegerUpToNDigits(4, i);
	}
	
	private static String zeroPreffixedIntegerUpToNDigits(int digits, int i) {
		String preffixed = "0"+ Integer.toString(i);
		return preffixed.substring(preffixed.length()-digits, preffixed.length());
	}
	
	public static long now() {
		return System.currentTimeMillis();
	}
	
	private static String romanMonth(int i) {
		panicIf((i>12)||(i<1));
		switch (i) {
		case 1: return "I";
		case 2: return "II";
		case 3: return "III";
		case 4: return "IV";
		case 5: return "V";
		case 6: return "VI";
		case 7: return "VII";
		case 8: return "VIII";
		case 9: return "IX";
		case 10:return "X";
		case 11:return "XI";
		case 12:return "XII";
		}
		return "impossible and yet weirdly requested by the compiler";
	}

	public static String timestamp_RomanMonthDDHH24MISS_forDisplay() {
		Calendar rightNow 	= Calendar.getInstance();
		return timestamp_RomanMonthDDHH24MISS_forDisplay(rightNow);
	}
	
	public static String timestamp_RomanMonthDDHH24MISS_forDisplay(Calendar rightNow) {
		String month 		= romanMonth(rightNow.get(Calendar.MONTH)+1);
		String day			= zeroPreffixedIntegerUpToTwoDigits(rightNow.get(Calendar.DAY_OF_MONTH));
		String hour			= zeroPreffixedIntegerUpToTwoDigits(rightNow.get(Calendar.HOUR_OF_DAY));
		String minute		= zeroPreffixedIntegerUpToTwoDigits(rightNow.get(Calendar.MINUTE));
		String seconds      = zeroPreffixedIntegerUpToTwoDigits(rightNow.get(Calendar.SECOND));
		return month+"."+day+" "+hour+":"+minute+":"+seconds;
	}


	public static String timestamp_RomanMonthDDHH24MI_forDisplay() {
		Calendar rightNow 	= Calendar.getInstance();
		String month 		= romanMonth(rightNow.get(Calendar.MONTH)+1);
		String day			= zeroPreffixedIntegerUpToTwoDigits(rightNow.get(Calendar.DAY_OF_MONTH));
		String hour			= zeroPreffixedIntegerUpToTwoDigits(rightNow.get(Calendar.HOUR_OF_DAY));
		String minute		= zeroPreffixedIntegerUpToTwoDigits(rightNow.get(Calendar.MINUTE));
		return month+"."+day+" "+hour+":"+minute;
	}
	
	public static String timestamp_YYYYMMDDHH24MI() {
		Calendar rightNow 	= Calendar.getInstance();
		String year 		= Integer.toString(rightNow.get(Calendar.YEAR));
		String month 		= zeroPreffixedIntegerUpToTwoDigits(rightNow.get(Calendar.MONTH)+1);
		String day			= zeroPreffixedIntegerUpToTwoDigits(rightNow.get(Calendar.DAY_OF_MONTH));
		String hour			= zeroPreffixedIntegerUpToTwoDigits(rightNow.get(Calendar.HOUR_OF_DAY));
		String minute		= zeroPreffixedIntegerUpToTwoDigits(rightNow.get(Calendar.MINUTE));
		return year+"_"+month+"_"+day+"___"+hour+"_"+minute;
	}
	
	public static String timestamp_YYYYMMDDHH24MISS() {
		Calendar rightNow 	= Calendar.getInstance();
		String year 		= Integer.toString(rightNow.get(Calendar.YEAR));
		String month 		= zeroPreffixedIntegerUpToTwoDigits(rightNow.get(Calendar.MONTH)+1);
		String day			= zeroPreffixedIntegerUpToTwoDigits(rightNow.get(Calendar.DAY_OF_MONTH));
		String hour			= zeroPreffixedIntegerUpToTwoDigits(rightNow.get(Calendar.HOUR_OF_DAY));
		String minute		= zeroPreffixedIntegerUpToTwoDigits(rightNow.get(Calendar.MINUTE));
		String seconds      = zeroPreffixedIntegerUpToTwoDigits(rightNow.get(Calendar.SECOND));
		return year+"_"+month+"_"+day+"___"+hour+"_"+minute+"_"+seconds;
	}
	
	public static String timestamp_YYYYMMDDHH24MISS(long msse) {
		Calendar c 	= Calendar.getInstance();
		c.setTimeInMillis(msse);
		String year 		= Integer.toString(c.get(Calendar.YEAR));
		String month 		= zeroPreffixedIntegerUpToTwoDigits(c.get(Calendar.MONTH)+1);
		String day			= zeroPreffixedIntegerUpToTwoDigits(c.get(Calendar.DAY_OF_MONTH));
		String hour			= zeroPreffixedIntegerUpToTwoDigits(c.get(Calendar.HOUR_OF_DAY));
		String minute		= zeroPreffixedIntegerUpToTwoDigits(c.get(Calendar.MINUTE));
		String seconds      = zeroPreffixedIntegerUpToTwoDigits(c.get(Calendar.SECOND));
		return year+"_"+month+"_"+day+"___"+hour+"_"+minute+"_"+seconds;
	}
	
	public static String timestamp_YYYYMMDDHH24MISS_v2(long msse) {
		Calendar c 	= Calendar.getInstance();
		c.setTimeInMillis(msse);
		String year 		= Integer.toString(c.get(Calendar.YEAR));
		String month 		= zeroPreffixedIntegerUpToTwoDigits(c.get(Calendar.MONTH)+1);
		String day			= zeroPreffixedIntegerUpToTwoDigits(c.get(Calendar.DAY_OF_MONTH));
		String hour			= zeroPreffixedIntegerUpToTwoDigits(c.get(Calendar.HOUR_OF_DAY));
		String minute		= zeroPreffixedIntegerUpToTwoDigits(c.get(Calendar.MINUTE));
		String seconds      = zeroPreffixedIntegerUpToTwoDigits(c.get(Calendar.SECOND));
		return year+"-"+month+"-"+day+" "+hour+":"+minute+":"+seconds;
	}


	public static int castToIntWithChecks(float f) {
		if (f > Integer.MAX_VALUE)
			throw new RuntimeException(f+" can't be cast to int");
		else return (int) f;
	}

	public static int castToIntWithChecks(double f) {
		if (f > Integer.MAX_VALUE)
			throw new RuntimeException(f+" can't be cast to int");
		else return (int) f;
	}

	public static int castToIntWithChecks(long l) {
		if (l > Integer.MAX_VALUE)
			throw new RuntimeException(l+" can't be cast to int");
		else {
			int i = (int) l;
			if (i!=l)
				throw new RuntimeException("weird:"+i+"!="+l);
			else
				return i;
		}
	}
	
	public static short castToShortWithChecks(int i) {
		if (i > Short.MAX_VALUE)
			throw new RuntimeException(i+" can't be cast to short");
		else {
			short s = (short) i;
			if (s!=i)
				throw new RuntimeException("weird:"+s+"!="+i);
			else
				return s;
		}
	}

	
	public static int hashCode(Object[] objects) {
		StringBuilder sb = new StringBuilder();
		for (Object object : objects) {
			String s = object.toString();
			sb.append(s);
		}
		String toBeHashed = sb.toString();
		return toBeHashed.hashCode();
	}
	
	public static int secondInTheDayHardWayAtLocale(int secondSinceTheEpoch) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(TimeUnit.SECONDS.toMillis(secondSinceTheEpoch));
		int hour  	=	c.get(Calendar.HOUR_OF_DAY);
		int minute 	=	c.get(Calendar.MINUTE);
		int second  =   c.get(Calendar.SECOND);
		int secondInTheDay = castToIntWithChecks(
						TimeUnit.HOURS	.toSeconds(hour)+
						TimeUnit.MINUTES.toSeconds(minute)+
						TimeUnit.SECONDS.toSeconds(second));
		return secondInTheDay;
	}
	
	public static int secondInTheDaysAtUTC(int secondSinceTheEpoch) {
		return secondSinceTheEpoch % (int) TimeUnit.DAYS.toSeconds(1);
	}
	
	public static int [][] transposeMatrix(int [][] m){
		  int r = m.length;
		  int c = m[r-1].length;
		  panicIfNotEqual(m[0].length, c);
		  int [][] t = new int [c][r];
		  for(int i = 0; i < r; ++i){
		    for(int j = 0; j < c; ++j){
		      t[j][i] = m[i][j];
		    }
		  }
		  return t;
		}
	
  public static void gzipFile(String from, String to) throws IOException {
	    FileInputStream in = new FileInputStream(from);
	    GZIPOutputStream out = new GZIPOutputStream(new FileOutputStream(to));
	    byte[] buffer = new byte[4096];
	    int bytesRead;
	    while ((bytesRead = in.read(buffer)) != -1)
	      out.write(buffer, 0, bytesRead);
	    in.close();
	    out.close();
	  }
	
    
  public static String ifNull(String a, String defaultValue) {
	  if (a==null) return defaultValue;
	  else return a;
  }
	public static void main(String[] args) {
		// for testing only
		System.out.println(timestamp_YYYYMMDDHH24MI());
		System.out.println(bips(1, 10_000));
		System.out.println(bips(1, 11_000));
		System.out.println(bips(1, 2));
                System.out.println("1 in 1");
                for (int i = 0 ; i < 10 ; i++)
                    System.out.println(oneIn(1));
                System.out.println("1 in 2");
                for (int i = 0 ; i < 10 ; i++)
                    System.out.println(oneIn(2));
	}
	
	public static float[] toFloat(long[] ls) {
		if (ls==null) return null;
		else {
			float[] ret = new float[ls.length];
			for (int i = 0 ; i < ls.length ; i++)
				ret[i] = (float) ls[i];
			return ret;
		}
	}
	
	public static int howManyTrue(boolean ...values) {
		int trueSoFar = 0;
		for (boolean value : values)
			if (value) trueSoFar++;
		return trueSoFar;
	}
	
	public static boolean exactlyNTrue(int n, boolean ...values) {
		return (n==howManyTrue(values));
	}
	
	public static boolean exactlyOneIsTrue(boolean ...values) {
		return exactlyNTrue(1, values);
	}
	
	public static boolean exactlyTwoAreTrue(boolean ...values) {
		return exactlyNTrue(2, values);
	}
	
	public static boolean allAreTrue(boolean ...values) {
		return exactlyNTrue(values.length, values);
	}
	
	public static int bips(int a, long l) {
		return (int) (a*10_000 / l);
	}
	
	public static int bips(long a, long l) {
		return (int) (a*10_000 / l);
	}
	
	public static void panicIfDoesntThrow(Runnable code, Class<? extends Exception> exceptionClass ) {
		try {
			code.run();
			panic("no exception thrown");
		} catch (Exception e) {
			if (!e.getClass().equals(exceptionClass))
				panic(e.getClass().getCanonicalName()+" is not the expected exception ("+exceptionClass.getCanonicalName()+")");
		}
		
	}

    public static Date dateFromYYYYMMdd_type0(String str) { // format is "YYYY-MM-dd"
        return dateFromString("YYYY-MM-dd", str);
    }

    public static Date dateFromYYYYMMdd_type1(String str) { // format is "YYYYMMdd"
        return dateFromString("YYYYMMdd", str);
    }

    public static Date dateFromString(String format, String str) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(format);
        DateTime dt = formatter.parseDateTime(str);
        return dt.toDate();
    }

    public static BigDecimal bdFromElLocaleString(String str) throws ParseException {
        return bdFromLocaleString("el", str);
    }

    public static BigDecimal bdFromEnLocaleString(String str) throws ParseException {
        return bdFromLocaleString("en", str);
    }

    public static BigDecimal bdFromLocaleString(String locale, String str) throws ParseException {
        Locale loc = new Locale(locale);
        NumberFormat nf = NumberFormat.getInstance(loc);
        if (!(nf instanceof DecimalFormat)) throw new RuntimeException("no decimal format availale");
        else {
            DecimalFormat df = (DecimalFormat) nf;
            df.setParseBigDecimal(true);
            return (BigDecimal) df.parse(str);
        }
    }

    public static String numberToString(Number n, char decimalSep, char groupingSep, int minimumDecimal) {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator(decimalSep);
        dfs.setGroupingSeparator(groupingSep);
        DecimalFormat df = new DecimalFormat("###,###.##", dfs);
        df.setGroupingSize(3);
        df.setMinimumFractionDigits(minimumDecimal);
        return df.format(n);
    }

    public static String amountToStringELnoCurr(Number n) {
        if (n == null) return "";
        else return numberToString(n, ',', '.', 2);
    }

    public static String amountToStringELnoCurrMDashForNull(Number n) {
        if (n == null) return "—";
        else return numberToString(n, ',', '.', 2);
    }

    public static String amountToStringELnoCurrEmptyForNull(Number n) {
        if (n == null) return "";
        else return numberToString(n, ',', '.', 2);
    }

    public static String toELnoCurrMDashForNullOrZero(BigDecimal bd) { 
        if      (bd == null) return "—";
        else if (bd.compareTo(new BigDecimal(0.0))==0) return "—";
        else return numberToString(bd, ',', '.', 2);
    }

    public static String dateToElType1(Date d) {
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	return sdf.format(d);
    }

    public static String dateToElType2(Date d) {
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	return sdf.format(d);
    }

    public static int secondsSinceTheEpoch() {
        return castToIntWithChecks( System.currentTimeMillis() / 1000l );
    }

    public static <E> Iterable<E> iterableOnce(final Enumeration<E> enumeration) {
        if (enumeration == null) {
            throw new NullPointerException();
        }
        return new Iterable<E>() {
            public Iterator<E> iterator() {
                return new Iterator<E>() {
                    public boolean hasNext() {
                        return enumeration.hasMoreElements();
                    }
                    public E next() {
                        return enumeration.nextElement();
                    }
                    public void remove() {
                        throw new UnsupportedOperationException();
                    }
                };
            }
        };
    }

    public static boolean isInteger( String input ) {  
       try {  
               Integer.parseInt( input );  
               return true;  
       }  
       catch (Exception e) {  
               return false;  
       }  
    }  

    public static <T> T NOT_NULL(T t) {
        if (t==null) throw new RuntimeException(t.toString());
        else return t;
    }

    public static String randomString(String symbols, int length) {
        Random random = new Random();
        char[] buf = new char[length];
        for (int i = 0 ; i < length ; i++)
            buf[i] = symbols.charAt(random.nextInt(symbols.length()));
        return new String(buf);
    }

    public static String randomAlphanumString(int length) {
        char[] symbols = new char[36];
        for (int idx = 0; idx < 10; ++idx)
            symbols[idx] = (char) ('0' + idx);
        for (int idx = 10; idx < 36; ++idx)
            symbols[idx] = (char) ('a' + idx - 10);
        return randomString(new String(symbols), length);
    }

    public static String secureRandomString(String symbols, int length) {
        Random random = new SecureRandom();
        random.setSeed(System.currentTimeMillis());
        char[] buf = new char[length];
        for (int i = 0 ; i < length ; i++)
            buf[i] = symbols.charAt(random.nextInt(symbols.length()));
        return new String(buf);
    }

    public static String secureRandomAlphanumString(int length) {
        char[] symbols = new char[36];
        for (int idx = 0; idx < 10; ++idx)
            symbols[idx] = (char) ('0' + idx);
        for (int idx = 10; idx < 36; ++idx)
            symbols[idx] = (char) ('a' + idx - 10);
        return secureRandomString(new String(symbols), length);
    }

    public static int random(int upperBoundary) {
        return (int) (Math.random()*upperBoundary);
    }

    public static <T> T[] array(T... elems) {
        return elems;
    }

    public static String random(String[] someStrings) {
        return someStrings[random(someStrings.length)];
    }

    public static String random2(String ... someStrings) {
        return random(array(someStrings));
    }
    
    public static Integer random(Integer[] someInts) {
        return someInts[random(someInts.length)];
    }

    public static Integer random(List<Integer> someInts) {
        return random(someInts.toArray(new Integer[0]));
    }

    public static String[] availableFontFamilyNames() {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        return ge.getAvailableFontFamilyNames();
    }

    public static String greekText(int n) {
        String[] seed = {"Πιστεύω", "εις", "έναν", "Θεό", "πατέρα", "παντοκράτορα", "ποιητή", "ούρανού", "και", "γης", "ορατών", "τε", "πάντων",
                         "και", "αοράτων.", "Και", "εις", "έναν", "Κύριον", "Ιησούν", "Χριστόν", "τον", "υιό", "του", "Θεού", "τον", "μονογενή",
                         ",", "τον", "εκ", "του", "πατρός", "γεννηθέντα", "προ", "πάντων", "των", "αιώνων.", "Φως", "εκ", "φωτός", "θεόν",
                         "αληθινόν", "εκ", "θεού", "αληθινού", "γεννηθέντα", "ου", "ποιηθέντα", "ομοούσιο", "τω", "Πατρί", "δι", "ου", "τα", "πάντα", "εγένετο."};
        StringBuilder sb = new StringBuilder();
        for (int i = 0 ; i < n ; i++)
            sb.append(seed[i % seed.length]+" ");
        return sb.toString()+"Αμήν.";
    }

    public static <T> T exactlyOne (List<T> ts) {
        if (ts.size()!=1) throw new RuntimeException(String.valueOf(ts.size()));
        else return ts.get(0);
    }

    public static String arabicToRoman(int aNumber){
        if(aNumber < 1 || aNumber > 3999)
            throw new RuntimeException(String.valueOf(aNumber));
        int[] aArray = {1000,900,500,400,100,90,50,40,10,9,5,4,1};
        String[] rArray = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};
        String rNumber = "";
     
        for(int i=0; i<aArray.length; i++){
            while(aNumber >= aArray[i]){
                rNumber += rArray[i];
                aNumber -= aArray[i];
            }
        }
        return rNumber;
    }

    private static final String RARE_STRING="use a rare string, @#$645ksd9872kh-dsv";
    private static String replacePerc(String s) {
        return s.replace("%", "n"+RARE_STRING);
    }
    public static boolean unescapedSQLPercentageExists(String s) {

         return StringEscapeUtils.unescapeJava(replacePerc(s)).indexOf("n"+RARE_STRING)!=-1;       
    }

    public static <T> boolean superSet(List<T> as, List<T> bs) {
        for (T b :bs)
            if (!as.contains(b))
                return false;
        return true;
    }

    public static <T> boolean sameElements(List<T> as, List<T> bs) {
        return superSet(as, bs) && superSet(bs, as);
    }

    public static <T> boolean uniqueElements(List<T> alist) {
        return (new HashSet<T>(alist)).size()==alist.size();
    }

    public static <T> List<T> sortLike(List<T> sortUs, List<T> modelSort) throws DuplicateElementsInModelSort {
        if (!uniqueElements(modelSort))
            throw new DuplicateElementsInModelSort();
        else {
            List<T> sortUsCopy = new ArrayList<>(sortUs);
            Collections.copy(sortUsCopy, sortUs);
            Collections.sort(sortUsCopy, new ModelComparator(modelSort));
            return sortUsCopy;
        }
    }

    public static boolean oneIn(int n) {
        Random r = new Random(System.currentTimeMillis());
	return (0 == r.nextInt(n));
    }
}

class ModelComparator<T> implements Comparator<T> {

    private List<T> modelList;
    public ModelComparator(List<T> modelList) throws DuplicateElementsInModelSort {
        if (!Util.uniqueElements(modelList))
            throw new DuplicateElementsInModelSort();
        this.modelList = modelList;
    }

    @Override
    public int compare(T a, T b) {
        int a_idx = modelList.indexOf(a);
        if (a_idx == -1) throw new RuntimeException();
        int b_idx = modelList.indexOf(b);
        if (b_idx == -1) throw new RuntimeException();
        return a_idx - b_idx;
    }

}