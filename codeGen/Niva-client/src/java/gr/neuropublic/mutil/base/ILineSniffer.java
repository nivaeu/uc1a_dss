package gr.neuropublic.mutil.base;

public interface ILineSniffer {
	public void sniffLine(String line);
}
