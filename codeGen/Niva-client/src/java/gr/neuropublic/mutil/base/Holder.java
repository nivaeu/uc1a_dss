package gr.neuropublic.mutil.base;

public class Holder<T> {
	public T t;
	public Holder(T t) {
		this.t = t;
	}
	public void set(T t) {
		this.t = t;
	}
        public T get() {
            return t;
        }
}
