package gr.neuropublic.mutil.base;

import java.lang.reflect.Field;

public class ReflctUtil {

	@SuppressWarnings("unchecked")
	public static <T> T getFieldValue (Object obj, String fieldName, Class<T> klassExp) {
		try {
			Class<?> klass = obj.getClass();
			Field field = klass.getField(fieldName);
			Object retVal = field.get(obj);
			if (!(retVal.getClass().equals(klassExp))) throw new IllegalStateException("return value is of type: "+retVal.getClass().getCanonicalName()+" (whereas expected type was: "+klassExp.getCanonicalName()+")");
			return (T) retVal;
		} catch (Exception e) {
			throw new ReflctUtilException(e);
		}
	}
	
	static class Test {
		public long l;
		public int i;
		public Long ll;
		public Test(long l, int i, Long ll ) {
			this.l=l;this.i=i;this.ll = ll;
		}
	}
	
	public static void main (String ...args) {
		ReflctUtil.Test t = new ReflctUtil.Test(1L,2,3L);
		System.out.println(getFieldValue(t, "l", Long.class));
		System.out.println(getFieldValue(t, "i", Integer.class));
		System.out.println(getFieldValue(t, "ll", Long.class));
		System.out.println(getFieldValue(t, "ll", Integer.class));
	}
}
