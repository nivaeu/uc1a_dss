package gr.neuropublic.mutil.base;


public class PrefixSnobberQualified implements IStringFilter {
	
	private String prefix;
	private String except;
	
	public PrefixSnobberQualified(String prefix, String except) {
		this.prefix = prefix;
		this.except = except;
	}

	@Override
	public boolean accept(String s) {
		if (s.startsWith(prefix) && (!s.startsWith(except)))
			return false;
		else return true;
	}

}



