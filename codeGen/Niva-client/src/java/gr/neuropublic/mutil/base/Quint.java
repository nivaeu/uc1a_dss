package gr.neuropublic.mutil.base;

import java.io.Serializable;

public class Quint<A, B, C, D, E> implements Serializable {
	 

    private static final long serialVersionUID = 1L;
	
    public final A a;
    public final B b;
    public final C c;
    public final D d; 
    public final E e;
    
    public Quint(final A a, final B b, final C c, final D d, final E e) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
    }
    
    public static <A, B, C, D, E> Quint<A, B, C, D, E> create(A a, B b, C c, D d, E e) {
        return new Quint<A, B, C, D, E>(a, b, c, d, e);
    }
 
    @SuppressWarnings("unchecked")
	public final boolean equals(Object o) {
        if (!(o instanceof Quint))
            return false;
 
        final Quint<?, ?, ?, ?, ?> other = (Quint) o;
        return equal(a, other.a) && equal(b, other.b) && equal(c, other.c) && equal(d, other.d) && equal(e, other.e);
    }
    
    private static final boolean equal(Object o1, Object o2) {
        if (o1 == null) {
            return o2 == null;
        }
        return o1.equals(o2);
    }
 
    public int hashCode() {
        int ha = a == null ? 0 : a.hashCode();
        int hb = b == null ? 0 : b.hashCode();
        int hc = c == null ? 0 : c.hashCode();
        int hd = d == null ? 0 : d.hashCode();
        int he = e == null ? 0 : e.hashCode();        
 
        return ha + (57 * hb) + (957 * hc) + (1453 * hd) + (1071 * he);
    }
}