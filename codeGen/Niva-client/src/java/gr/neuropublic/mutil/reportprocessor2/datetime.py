import time as _time
import math as _math
import sys as _sys

if _sys.platform.startswith('java'):
    from java.lang import Object
    from java.sql import Date, Timestamp, Time
    from java.util import Calendar
    from org.python.core import Py


MINYEAR = 1
MAXYEAR = 9999

_DAYS_IN_MONTH = [None, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

_DAYS_BEFORE_MONTH = [None]
dbm = 0
for dim in _DAYS_IN_MONTH[1:]:
    _DAYS_BEFORE_MONTH.append(dbm)
    dbm += dim
del dbm, dim

def _is_leap(year):
    return year % 4 == 0 and (year % 100 != 0 or year % 400 == 0)

def _days_in_year(year):
    return 365 + _is_leap(year)

def _days_before_year(year):
    y = year - 1
    return y*365 + y//4 - y//100 + y//400

def _days_in_month(year, month):
    assert 1 <= month <= 12, month
    if month == 2 and _is_leap(year):
        return 29
    return _DAYS_IN_MONTH[month]

def _days_before_month(year, month):
    if not 1 <= month <= 12:
        raise ValueError('month must be in 1..12', month)
    return _DAYS_BEFORE_MONTH[month] + (month > 2 and _is_leap(year))

def _ymd2ord(year, month, day):
    if not 1 <= month <= 12:
        raise ValueError('month must be in 1..12', month)
    dim = _days_in_month(year, month)
    if not 1 <= day <= dim:
        raise ValueError('day must be in 1..%d' % dim, day)
    return (_days_before_year(year) +
            _days_before_month(year, month) +
            day)

_DI400Y = _days_before_year(401)    # number of days in 400 years
_DI100Y = _days_before_year(101)    #    "    "   "   " 100   "
_DI4Y   = _days_before_year(5)      #    "    "   "   "   4   "

assert _DI4Y == 4 * 365 + 1

assert _DI400Y == 4 * _DI100Y + 1

assert _DI100Y == 25 * _DI4Y - 1

def _ord2ymd(n):
    "ordinal -> (year, month, day), considering 01-Jan-0001 as day 1."


    n -= 1
    n400, n = divmod(n, _DI400Y)
    year = n400 * 400 + 1   # ..., -399, 1, 401, ...

    n100, n = divmod(n, _DI100Y)

    n4, n = divmod(n, _DI4Y)

    n1, n = divmod(n, 365)

    year += n100 * 100 + n4 * 4 + n1
    if n1 == 4 or n100 == 4:
        assert n == 0
        return year-1, 12, 31

    leapyear = n1 == 3 and (n4 != 24 or n100 == 3)
    assert leapyear == _is_leap(year)
    month = (n + 50) >> 5
    preceding = _DAYS_BEFORE_MONTH[month] + (month > 2 and leapyear)
    if preceding > n:  # estimate is too large
        month -= 1
        preceding -= _DAYS_IN_MONTH[month] + (month == 2 and leapyear)
    n -= preceding
    assert 0 <= n < _days_in_month(year, month)

    return year, month, n+1

_MONTHNAMES = [None, "Jan", "Feb", "Mar", "Apr", "May", "Jun",
                     "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
_DAYNAMES = [None, "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]


def _build_struct_time(y, m, d, hh, mm, ss, dstflag):
    wday = (_ymd2ord(y, m, d) + 6) % 7
    dnum = _days_before_month(y, m) + d
    return _time.struct_time((y, m, d, hh, mm, ss, wday, dnum, dstflag))

def _format_time(hh, mm, ss, us):
    # Skip trailing microseconds when us==0.
    result = "%02d:%02d:%02d" % (hh, mm, ss)
    if us:
        result += ".%06d" % us
    return result

def _wrap_strftime(object, format, timetuple):
    year = timetuple[0]
    if year < 1900:
        raise ValueError("year=%d is before 1900; the datetime strftime() "
                         "methods require year >= 1900" % year)
    zreplace = None # the string to use for %z
    Zreplace = None # the string to use for %Z

    newformat = []
    push = newformat.append
    i, n = 0, len(format)
    while i < n:
        ch = format[i]
        i += 1
        if ch == '%':
            if i < n:
                ch = format[i]
                i += 1
                if ch == 'z':
                    if zreplace is None:
                        zreplace = ""
                        if hasattr(object, "_utcoffset"):
                            offset = object._utcoffset()
                            if offset is not None:
                                sign = '+'
                                if offset < 0:
                                    offset = -offset
                                    sign = '-'
                                h, m = divmod(offset, 60)
                                zreplace = '%c%02d%02d' % (sign, h, m)
                    assert '%' not in zreplace
                    newformat.append(zreplace)
                elif ch == 'Z':
                    if Zreplace is None:
                        Zreplace = ""
                        if hasattr(object, "tzname"):
                            s = object.tzname()
                            if s is not None:
                                # strftime is going to have at this: escape %
                                Zreplace = s.replace('%', '%%')
                    newformat.append(Zreplace)
                else:
                    push('%')
                    push(ch)
            else:
                push('%')
        else:
            push(ch)
    newformat = "".join(newformat)
    return _time.strftime(newformat, timetuple)

def _call_tzinfo_method(tzinfo, methname, tzinfoarg):
    if tzinfo is None:
        return None
    return getattr(tzinfo, methname)(tzinfoarg)

def _check_tzname(name):
    if name is not None and not isinstance(name, str):
        raise TypeError("tzinfo.tzname() must return None or string, "
                        "not '%s'" % type(name))

def _check_utc_offset(name, offset):
    assert name in ("utcoffset", "dst")
    if offset is None:
        return None
    if not isinstance(offset, timedelta):
        raise TypeError("tzinfo.%s() must return None "
                        "or timedelta, not '%s'" % (name, type(offset)))
    days = offset.days
    if days < -1 or days > 0:
        offset = 1440  # trigger out-of-range
    else:
        seconds = days * 86400 + offset.seconds
        minutes, seconds = divmod(seconds, 60)
        if seconds or offset.microseconds:
            raise ValueError("tzinfo.%s() must return a whole number "
                             "of minutes" % name)
        offset = minutes
    if -1440 < offset < 1440:
        return offset
    raise ValueError("%s()=%d, must be in -1439..1439" % (name, offset))

def _check_date_fields(year, month, day):
    if not MINYEAR <= year <= MAXYEAR:
        raise ValueError('year must be in %d..%d' % (MINYEAR, MAXYEAR), year)
    if not 1 <= month <= 12:
        raise ValueError('month must be in 1..12', month)
    dim = _days_in_month(year, month)
    if not 1 <= day <= dim:
        raise ValueError('day must be in 1..%d' % dim, day)

def _check_time_fields(hour, minute, second, microsecond):
    if not 0 <= hour <= 23:
        raise ValueError('hour must be in 0..23', hour)
    if not 0 <= minute <= 59:
        raise ValueError('minute must be in 0..59', minute)
    if not 0 <= second <= 59:
        raise ValueError('second must be in 0..59', second)
    if not 0 <= microsecond <= 999999:
        raise ValueError('microsecond must be in 0..999999', microsecond)

def _check_tzinfo_arg(tz):
    if tz is not None and not isinstance(tz, tzinfo):
        raise TypeError("tzinfo argument must be None or of a tzinfo subclass")


def _cmperror(x, y):
    raise TypeError("can't compare '%s' to '%s'" % (
                    type(x).__name__, type(y).__name__))

_ORD1970 = _ymd2ord(1970, 1, 1) # base ordinal for UNIX epoch

class tmxxx:

    ordinal = None

    def __init__(self, year, month, day, hour=0, minute=0, second=0,
                 microsecond=0):
        # Normalize all the inputs, and store the normalized values.
        if not 0 <= microsecond <= 999999:
            carry, microsecond = divmod(microsecond, 1000000)
            second += carry
        if not 0 <= second <= 59:
            carry, second = divmod(second, 60)
            minute += carry
        if not 0 <= minute <= 59:
            carry, minute = divmod(minute, 60)
            hour += carry
        if not 0 <= hour <= 23:
            carry, hour = divmod(hour, 24)
            day += carry

        if not 1 <= month <= 12:
            carry, month = divmod(month-1, 12)
            year += carry
            month += 1
            assert 1 <= month <= 12

        dim = _days_in_month(year, month)
        if not 1 <= day <= dim:
            if day == 0:    # move back a day
                month -= 1
                if month > 0:
                    day = _days_in_month(year, month)
                else:
                    year, month, day = year-1, 12, 31
            elif day == dim + 1:    # move forward a day
                month += 1
                day = 1
                if month > 12:
                    month = 1
                    year += 1
            else:
                self.ordinal = _ymd2ord(year, month, 1) + (day - 1)
                year, month, day = _ord2ymd(self.ordinal)

        self.year, self.month, self.day = year, month, day
        self.hour, self.minute, self.second = hour, minute, second
        self.microsecond = microsecond

    def toordinal(self):
        if self.ordinal is None:
            self.ordinal = _ymd2ord(self.year, self.month, self.day)
        return self.ordinal

    def time(self):
        days = self.toordinal() - _ORD1970   # convert to UNIX epoch
        seconds = ((days * 24. + self.hour)*60. + self.minute)*60.
        return seconds + self.second + self.microsecond / 1e6

    def ctime(self):
        weekday = self.toordinal() % 7 or 7
        return "%s %s %2d %02d:%02d:%02d %04d" % (
            _DAYNAMES[weekday],
            _MONTHNAMES[self.month],
            self.day,
            self.hour, self.minute, self.second,
            self.year)

class timedelta(object):
    def __new__(cls, days=0, seconds=0, microseconds=0,
                # XXX The following should only be used as keyword args:
                milliseconds=0, minutes=0, hours=0, weeks=0):
        d = s = us = 0

        days += weeks*7
        seconds += minutes*60 + hours*3600
        microseconds += milliseconds*1000

        if isinstance(days, float):
            dayfrac, days = _math.modf(days)
            daysecondsfrac, daysecondswhole = _math.modf(dayfrac * (24.*3600.))
            assert daysecondswhole == int(daysecondswhole)  # can't overflow
            s = int(daysecondswhole)
            assert days == long(days)
            d = long(days)
        else:
            daysecondsfrac = 0.0
            d = days
        assert isinstance(daysecondsfrac, float)
        assert abs(daysecondsfrac) <= 1.0
        assert isinstance(d, (int, long))
        assert abs(s) <= 24 * 3600

        if isinstance(seconds, float):
            secondsfrac, seconds = _math.modf(seconds)
            assert seconds == long(seconds)
            seconds = long(seconds)
            secondsfrac += daysecondsfrac
            assert abs(secondsfrac) <= 2.0
        else:
            secondsfrac = daysecondsfrac
        assert isinstance(secondsfrac, float)
        assert abs(secondsfrac) <= 2.0

        assert isinstance(seconds, (int, long))
        days, seconds = divmod(seconds, 24*3600)
        d += days
        s += int(seconds)    # can't overflow
        assert isinstance(s, int)
        assert abs(s) <= 2 * 24 * 3600

        usdouble = secondsfrac * 1e6
        assert abs(usdouble) < 2.1e6    # exact value not critical

        if isinstance(microseconds, float):
            microseconds += usdouble
            microseconds = round(microseconds)
            seconds, microseconds = divmod(microseconds, 1e6)
            assert microseconds == int(microseconds)
            assert seconds == long(seconds)
            days, seconds = divmod(seconds, 24.*3600.)
            assert days == long(days)
            assert seconds == int(seconds)
            d += long(days)
            s += int(seconds)   # can't overflow
            assert isinstance(s, int)
            assert abs(s) <= 3 * 24 * 3600
        else:
            seconds, microseconds = divmod(microseconds, 1000000)
            days, seconds = divmod(seconds, 24*3600)
            d += days
            s += int(seconds)    # can't overflow
            assert isinstance(s, int)
            assert abs(s) <= 3 * 24 * 3600
            microseconds = float(microseconds)
            microseconds += usdouble
            microseconds = round(microseconds)
        assert abs(s) <= 3 * 24 * 3600
        assert abs(microseconds) < 3.1e6

        assert isinstance(microseconds, float)
        assert int(microseconds) == microseconds
        us = int(microseconds)
        seconds, us = divmod(us, 1000000)
        s += seconds    # cant't overflow
        assert isinstance(s, int)
        days, s = divmod(s, 24*3600)
        d += days

        assert isinstance(d, (int, long))
        assert isinstance(s, int) and 0 <= s < 24*3600
        assert isinstance(us, int) and 0 <= us < 1000000

        self = object.__new__(cls)

        self.__days = d
        self.__seconds = s
        self.__microseconds = us
        if abs(d) > 999999999:
            raise OverflowError("timedelta # of days is too large: %d" % d)

        return self

    def __repr__(self):
        if self.__microseconds:
            return "%s(%d, %d, %d)" % ('datetime.' + self.__class__.__name__,
                                       self.__days,
                                       self.__seconds,
                                       self.__microseconds)
        if self.__seconds:
            return "%s(%d, %d)" % ('datetime.' + self.__class__.__name__,
                                   self.__days,
                                   self.__seconds)
        return "%s(%d)" % ('datetime.' + self.__class__.__name__, self.__days)

    def __str__(self):
        mm, ss = divmod(self.__seconds, 60)
        hh, mm = divmod(mm, 60)
        s = "%d:%02d:%02d" % (hh, mm, ss)
        if self.__days:
            def plural(n):
                return n, abs(n) != 1 and "s" or ""
            s = ("%d day%s, " % plural(self.__days)) + s
        if self.__microseconds:
            s = s + ".%06d" % self.__microseconds
        return s

    days = property(lambda self: self.__days, doc="days")
    seconds = property(lambda self: self.__seconds, doc="seconds")
    microseconds = property(lambda self: self.__microseconds,
                            doc="microseconds")

    def __add__(self, other):
        if isinstance(other, timedelta):
            # for CPython compatibility, we cannot use
            # our __class__ here, but need a real timedelta
            return timedelta(self.__days + other.__days,
                             self.__seconds + other.__seconds,
                             self.__microseconds + other.__microseconds)
        return NotImplemented

    __radd__ = __add__

    def __sub__(self, other):
        if isinstance(other, timedelta):
            return self + -other
        return NotImplemented

    def __rsub__(self, other):
        if isinstance(other, timedelta):
            return -self + other
        return NotImplemented

    def __neg__(self):
            # for CPython compatibility, we cannot use
            # our __class__ here, but need a real timedelta
        return timedelta(-self.__days,
                         -self.__seconds,
                         -self.__microseconds)

    def __pos__(self):
        return self

    def __abs__(self):
        if self.__days < 0:
            return -self
        else:
            return self

    def __mul__(self, other):
        if isinstance(other, (int, long)):
            # for CPython compatibility, we cannot use
            # our __class__ here, but need a real timedelta
            return timedelta(self.__days * other,
                             self.__seconds * other,
                             self.__microseconds * other)
        return NotImplemented

    __rmul__ = __mul__

    def __div__(self, other):
        if isinstance(other, (int, long)):
            usec = ((self.__days * (24*3600L) + self.__seconds) * 1000000 +
                    self.__microseconds)
            return timedelta(0, 0, usec // other)
        return NotImplemented

    __floordiv__ = __div__

    def __eq__(self, other):
        if isinstance(other, timedelta):
            return self.__cmp(other) == 0
        else:
            return False

    def __ne__(self, other):
        if isinstance(other, timedelta):
            return self.__cmp(other) != 0
        else:
            return True

    def __le__(self, other):
        if isinstance(other, timedelta):
            return self.__cmp(other) <= 0
        else:
            _cmperror(self, other)

    def __lt__(self, other):
        if isinstance(other, timedelta):
            return self.__cmp(other) < 0
        else:
            _cmperror(self, other)

    def __ge__(self, other):
        if isinstance(other, timedelta):
            return self.__cmp(other) >= 0
        else:
            _cmperror(self, other)

    def __gt__(self, other):
        if isinstance(other, timedelta):
            return self.__cmp(other) > 0
        else:
            _cmperror(self, other)

    def __cmp(self, other):
        assert isinstance(other, timedelta)
        return cmp(self.__getstate(), other.__getstate())

    def __hash__(self):
        return hash(self.__getstate())

    def __nonzero__(self):
        return (self.__days != 0 or
                self.__seconds != 0 or
                self.__microseconds != 0)

    # Pickle support.

    __safe_for_unpickling__ = True      # For Python 2.2

    def __getstate(self):
        return (self.__days, self.__seconds, self.__microseconds)

    def __reduce__(self):
        return (self.__class__, self.__getstate())

timedelta.min = timedelta(-999999999)
timedelta.max = timedelta(days=999999999, hours=23, minutes=59, seconds=59,
                          microseconds=999999)
timedelta.resolution = timedelta(microseconds=1)

class date(object):

    def __new__(cls, year, month=None, day=None):
        if isinstance(year, str):
            # Pickle support
            self = object.__new__(cls)
            self.__setstate(year)
            return self
        _check_date_fields(year, month, day)
        self = object.__new__(cls)
        self.__year = year
        self.__month = month
        self.__day = day
        return self


    def fromtimestamp(cls, t):
        y, m, d, hh, mm, ss, weekday, jday, dst = _time.localtime(t)
        return cls(y, m, d)
    fromtimestamp = classmethod(fromtimestamp)

    def today(cls):
        t = _time.time()
        return cls.fromtimestamp(t)
    today = classmethod(today)

    def fromordinal(cls, n):
        y, m, d = _ord2ymd(n)
        return cls(y, m, d)
    fromordinal = classmethod(fromordinal)

    # Conversions to string

    def __repr__(self):
        return "%s(%d, %d, %d)" % ('datetime.' + self.__class__.__name__,
                                   self.__year,
                                   self.__month,
                                   self.__day)
    def ctime(self):
        return tmxxx(self.__year, self.__month, self.__day).ctime()

    def strftime(self, fmt):
        return _wrap_strftime(self, fmt, self.timetuple())

    def isoformat(self):
        return "%04d-%02d-%02d" % (self.__year, self.__month, self.__day)

    __str__ = isoformat

    year = property(lambda self: self.__year,
                    doc="year (%d-%d)" % (MINYEAR, MAXYEAR))
    month = property(lambda self: self.__month, doc="month (1-12)")
    day = property(lambda self: self.__day, doc="day (1-31)")

    def timetuple(self):
        return _build_struct_time(self.__year, self.__month, self.__day,
                                  0, 0, 0, -1)

    def toordinal(self):
        return _ymd2ord(self.__year, self.__month, self.__day)

    def replace(self, year=None, month=None, day=None):
        if year is None:
            year = self.__year
        if month is None:
            month = self.__month
        if day is None:
            day = self.__day
        _check_date_fields(year, month, day)
        return date(year, month, day)

    def __eq__(self, other):
        if isinstance(other, date):
            return self.__cmp(other) == 0
        elif hasattr(other, "timetuple"):
            return NotImplemented
        else:
            return False

    def __ne__(self, other):
        if isinstance(other, date):
            return self.__cmp(other) != 0
        elif hasattr(other, "timetuple"):
            return NotImplemented
        else:
            return True

    def __le__(self, other):
        if isinstance(other, date):
            return self.__cmp(other) <= 0
        elif hasattr(other, "timetuple"):
            return NotImplemented
        else:
            _cmperror(self, other)

    def __lt__(self, other):
        if isinstance(other, date):
            return self.__cmp(other) < 0
        elif hasattr(other, "timetuple"):
            return NotImplemented
        else:
            _cmperror(self, other)

    def __ge__(self, other):
        if isinstance(other, date):
            return self.__cmp(other) >= 0
        elif hasattr(other, "timetuple"):
            return NotImplemented
        else:
            _cmperror(self, other)

    def __gt__(self, other):
        if isinstance(other, date):
            return self.__cmp(other) > 0
        elif hasattr(other, "timetuple"):
            return NotImplemented
        else:
            _cmperror(self, other)

    def __cmp(self, other):
        assert isinstance(other, date)
        y, m, d = self.__year, self.__month, self.__day
        y2, m2, d2 = other.__year, other.__month, other.__day
        return cmp((y, m, d), (y2, m2, d2))

    def __hash__(self):
        "Hash."
        return hash(self.__getstate())


    def _checkOverflow(self, year):
        if not MINYEAR <= year <= MAXYEAR:
            raise OverflowError("date +/-: result year %d not in %d..%d" %
                                (year, MINYEAR, MAXYEAR))

    def __add__(self, other):
        "Add a date to a timedelta."
        if isinstance(other, timedelta):
            t = tmxxx(self.__year,
                      self.__month,
                      self.__day + other.days)
            self._checkOverflow(t.year)
            result = date(t.year, t.month, t.day)
            return result
        raise TypeError
    __radd__ = __add__

    def __sub__(self, other):
        """Subtract two dates, or a date and a timedelta."""
        if isinstance(other, timedelta):
            return self + timedelta(-other.days)
        if isinstance(other, date):
            days1 = self.toordinal()
            days2 = other.toordinal()
            return timedelta(days1 - days2)
        return NotImplemented

    def weekday(self):
        return (self.toordinal() + 6) % 7


    def isoweekday(self):
        return self.toordinal() % 7 or 7

    def isocalendar(self):
        year = self.__year
        week1monday = _isoweek1monday(year)
        today = _ymd2ord(self.__year, self.__month, self.__day)
        week, day = divmod(today - week1monday, 7)
        if week < 0:
            year -= 1
            week1monday = _isoweek1monday(year)
            week, day = divmod(today - week1monday, 7)
        elif week >= 52:
            if today >= _isoweek1monday(year+1):
                year += 1
                week = 0
        return year, week+1, day+1


    __safe_for_unpickling__ = True      # For Python 2.2

    def __getstate(self):
        yhi, ylo = divmod(self.__year, 256)
        return ("%c%c%c%c" % (yhi, ylo, self.__month, self.__day), )

    def __setstate(self, string):
        if len(string) != 4 or not (1 <= ord(string[2]) <= 12):
            raise TypeError("not enough arguments")
        yhi, ylo, self.__month, self.__day = map(ord, string)
        self.__year = yhi * 256 + ylo

    def __reduce__(self):
        return (self.__class__, self.__getstate())

    if _sys.platform.startswith('java'):
        def __tojava__(self, java_class):
            if java_class not in (Calendar, Date, Object):
                return Py.NoConversion

            calendar = Calendar.getInstance()
            calendar.clear()
            calendar.set(self.year, self.month - 1, self.day)
            if java_class == Calendar:
                return calendar
            else:
                return Date(calendar.getTimeInMillis())


_date_class = date  # so functions w/ args named "date" can get at the class

date.min = date(1, 1, 1)
date.max = date(9999, 12, 31)
date.resolution = timedelta(days=1)

class tzinfo(object):
    def tzname(self, dt):
        raise NotImplementedError("tzinfo subclass must override tzname()")

    def utcoffset(self, dt):
        raise NotImplementedError("tzinfo subclass must override utcoffset()")

    def dst(self, dt):
        raise NotImplementedError("tzinfo subclass must override dst()")

    def fromutc(self, dt):
        if not isinstance(dt, datetime):
            raise TypeError("fromutc() requires a datetime argument")
        if dt.tzinfo is not self:
            raise ValueError("dt.tzinfo is not self")

        dtoff = dt.utcoffset()
        if dtoff is None:
            raise ValueError("fromutc() requires a non-None utcoffset() "
                             "result")

        dtdst = dt.dst()
        if dtdst is None:
            raise ValueError("fromutc() requires a non-None dst() result")
        delta = dtoff - dtdst
        if delta:
            dt += delta
            dtdst = dt.dst()
            if dtdst is None:
                raise ValueError("fromutc(): dt.dst gave inconsistent "
                                 "results; cannot convert")
        if dtdst:
            return dt + dtdst
        else:
            return dt

    __safe_for_unpickling__ = True      # For Python 2.2

    def __reduce__(self):
        getinitargs = getattr(self, "__getinitargs__", None)
        if getinitargs:
            args = getinitargs()
        else:
            args = ()
        getstate = getattr(self, "__getstate__", None)
        if getstate:
            state = getstate()
        else:
            state = getattr(self, "__dict__", None) or None
        if state is None:
            return (self.__class__, args)
        else:
            return (self.__class__, args, state)

_tzinfo_class = tzinfo   # so functions w/ args named "tinfo" can get at it

class time(object):

    def __new__(cls, hour=0, minute=0, second=0, microsecond=0, tzinfo=None):
        self = object.__new__(cls)
        if isinstance(hour, str):
            self.__setstate(hour, minute or None)
            return self
        _check_tzinfo_arg(tzinfo)
        _check_time_fields(hour, minute, second, microsecond)
        self.__hour = hour
        self.__minute = minute
        self.__second = second
        self.__microsecond = microsecond
        self._tzinfo = tzinfo
        return self

    hour = property(lambda self: self.__hour, doc="hour (0-23)")
    minute = property(lambda self: self.__minute, doc="minute (0-59)")
    second = property(lambda self: self.__second, doc="second (0-59)")
    microsecond = property(lambda self: self.__microsecond,
                           doc="microsecond (0-999999)")
    tzinfo = property(lambda self: self._tzinfo, doc="timezone info object")

    def __eq__(self, other):
        if isinstance(other, time):
            return self.__cmp(other) == 0
        else:
            return False

    def __ne__(self, other):
        if isinstance(other, time):
            return self.__cmp(other) != 0
        else:
            return True

    def __le__(self, other):
        if isinstance(other, time):
            return self.__cmp(other) <= 0
        else:
            _cmperror(self, other)

    def __lt__(self, other):
        if isinstance(other, time):
            return self.__cmp(other) < 0
        else:
            _cmperror(self, other)

    def __ge__(self, other):
        if isinstance(other, time):
            return self.__cmp(other) >= 0
        else:
            _cmperror(self, other)

    def __gt__(self, other):
        if isinstance(other, time):
            return self.__cmp(other) > 0
        else:
            _cmperror(self, other)

    def __cmp(self, other):
        assert isinstance(other, time)
        mytz = self._tzinfo
        ottz = other._tzinfo
        myoff = otoff = None

        if mytz is ottz:
            base_compare = True
        else:
            myoff = self._utcoffset()
            otoff = other._utcoffset()
            base_compare = myoff == otoff

        if base_compare:
            return cmp((self.__hour, self.__minute, self.__second,
                        self.__microsecond),
                       (other.__hour, other.__minute, other.__second,
                        other.__microsecond))
        if myoff is None or otoff is None:
            raise TypeError("cannot compare naive and aware times")
        myhhmm = self.__hour * 60 + self.__minute - myoff
        othhmm = other.__hour * 60 + other.__minute - otoff
        return cmp((myhhmm, self.__second, self.__microsecond),
                   (othhmm, other.__second, other.__microsecond))

    def __hash__(self):
        """Hash."""
        tzoff = self._utcoffset()
        if not tzoff: # zero or None
            return hash(self.__getstate()[0])
        h, m = divmod(self.hour * 60 + self.minute - tzoff, 60)
        if 0 <= h < 24:
            return hash(time(h, m, self.second, self.microsecond))
        return hash((h, m, self.second, self.microsecond))

    def _tzstr(self, sep=":"):
        """Return formatted timezone offset (+xx:xx) or None."""
        off = self._utcoffset()
        if off is not None:
            if off < 0:
                sign = "-"
                off = -off
            else:
                sign = "+"
            hh, mm = divmod(off, 60)
            assert 0 <= hh < 24
            off = "%s%02d%s%02d" % (sign, hh, sep, mm)
        return off

    def __repr__(self):
        """Convert to formal string, for repr()."""
        if self.__microsecond != 0:
            s = ", %d, %d" % (self.__second, self.__microsecond)
        elif self.__second != 0:
            s = ", %d" % self.__second
        else:
            s = ""
        s= "%s(%d, %d%s)" % ('datetime.' + self.__class__.__name__,
                             self.__hour, self.__minute, s)
        if self._tzinfo is not None:
            assert s[-1:] == ")"
            s = s[:-1] + ", tzinfo=%r" % self._tzinfo + ")"
        return s

    def isoformat(self):
        s = _format_time(self.__hour, self.__minute, self.__second,
                         self.__microsecond)
        tz = self._tzstr()
        if tz:
            s += tz
        return s

    __str__ = isoformat

    def strftime(self, fmt):
        timetuple = (1900, 1, 1,
                     self.__hour, self.__minute, self.__second,
                     0, 1, -1)
        return _wrap_strftime(self, fmt, timetuple)

    def utcoffset(self):
        offset = _call_tzinfo_method(self._tzinfo, "utcoffset", None)
        offset = _check_utc_offset("utcoffset", offset)
        if offset is not None:
            offset = timedelta(minutes=offset)
        return offset

    def _utcoffset(self):
        offset = _call_tzinfo_method(self._tzinfo, "utcoffset", None)
        offset = _check_utc_offset("utcoffset", offset)
        return offset

    def tzname(self):
        name = _call_tzinfo_method(self._tzinfo, "tzname", None)
        _check_tzname(name)
        return name

    def dst(self):
        offset = _call_tzinfo_method(self._tzinfo, "dst", None)
        offset = _check_utc_offset("dst", offset)
        if offset is not None:
            offset = timedelta(minutes=offset)
        return offset

    def replace(self, hour=None, minute=None, second=None, microsecond=None,
                tzinfo=True):
        if hour is None:
            hour = self.hour
        if minute is None:
            minute = self.minute
        if second is None:
            second = self.second
        if microsecond is None:
            microsecond = self.microsecond
        if tzinfo is True:
            tzinfo = self.tzinfo
        _check_time_fields(hour, minute, second, microsecond)
        _check_tzinfo_arg(tzinfo)
        return time(hour, minute, second, microsecond, tzinfo)

    def _dst(self):
        offset = _call_tzinfo_method(self._tzinfo, "dst", None)
        offset = _check_utc_offset("dst", offset)
        return offset

    def __nonzero__(self):
        if self.second or self.microsecond:
            return 1
        offset = self._utcoffset() or 0
        return self.hour * 60 + self.minute - offset != 0

    __safe_for_unpickling__ = True      # For Python 2.2

    def __getstate(self):
        us2, us3 = divmod(self.__microsecond, 256)
        us1, us2 = divmod(us2, 256)
        basestate = ("%c" * 6) % (self.__hour, self.__minute, self.__second,
                                  us1, us2, us3)
        if self._tzinfo is None:
            return (basestate,)
        else:
            return (basestate, self._tzinfo)

    def __setstate(self, string, tzinfo):
        if len(string) != 6 or ord(string[0]) >= 24:
            raise TypeError("an integer is required")
        self.__hour, self.__minute, self.__second, us1, us2, us3 = \
                                                            map(ord, string)
        self.__microsecond = (((us1 << 8) | us2) << 8) | us3
        self._tzinfo = tzinfo

    def __reduce__(self):
        return (time, self.__getstate())

    if _sys.platform.startswith('java'):
        def __tojava__(self, java_class):
            # TODO, if self.tzinfo is not None, convert time to UTC
            if java_class not in (Calendar, Time, Object):
                return Py.NoConversion

            calendar = Calendar.getInstance()
            calendar.clear()
            calendar.set(Calendar.HOUR_OF_DAY, self.hour)
            calendar.set(Calendar.MINUTE, self.minute)
            calendar.set(Calendar.SECOND, self.second)
            calendar.set(Calendar.MILLISECOND, self.microsecond // 1000)
            if java_class == Calendar:
                return calendar
            else:
                return Time(calendar.getTimeInMillis())


_time_class = time  # so functions w/ args named "time" can get at the class

time.min = time(0, 0, 0)
time.max = time(23, 59, 59, 999999)
time.resolution = timedelta(microseconds=1)

class datetime(date):

    def __new__(cls, year, month=None, day=None, hour=0, minute=0, second=0,
                microsecond=0, tzinfo=None):
        if isinstance(year, str):
            # Pickle support
            self = date.__new__(cls, year[:4])
            self.__setstate(year, month)
            return self
        _check_tzinfo_arg(tzinfo)
        _check_time_fields(hour, minute, second, microsecond)
        self = date.__new__(cls, year, month, day)
        # XXX This duplicates __year, __month, __day for convenience :-(
        self.__year = year
        self.__month = month
        self.__day = day
        self.__hour = hour
        self.__minute = minute
        self.__second = second
        self.__microsecond = microsecond
        self._tzinfo = tzinfo
        return self

    # Read-only field accessors
    hour = property(lambda self: self.__hour, doc="hour (0-23)")
    minute = property(lambda self: self.__minute, doc="minute (0-59)")
    second = property(lambda self: self.__second, doc="second (0-59)")
    microsecond = property(lambda self: self.__microsecond,
                           doc="microsecond (0-999999)")
    tzinfo = property(lambda self: self._tzinfo, doc="timezone info object")

    def fromtimestamp(cls, t, tz=None):

        _check_tzinfo_arg(tz)
        if tz is None:
            converter = _time.localtime
        else:
            converter = _time.gmtime
        y, m, d, hh, mm, ss, weekday, jday, dst = converter(t)
        us = int((t % 1.0) * 1000000)

        if us == 1000001 or us == 999999:
            us = 0
            rounded = True
        else:
            rounded = False

        ss = min(ss, 59)    # clamp out leap seconds if the platform has them
        result = cls(y, m, d, hh, mm, ss, us, tz)
        if rounded:
            result += timedelta(seconds=1)
        if tz is not None:
            result = tz.fromutc(result)
        return result
    fromtimestamp = classmethod(fromtimestamp)

    def utcfromtimestamp(cls, t):
        y, m, d, hh, mm, ss, weekday, jday, dst = _time.gmtime(t)
        us = int((t % 1.0) * 1000000)
        ss = min(ss, 59)    # clamp out leap seconds if the platform has them
        return cls(y, m, d, hh, mm, ss, us)
    utcfromtimestamp = classmethod(utcfromtimestamp)

    def now(cls, tz=None):
        t = _time.time()
        return cls.fromtimestamp(t, tz)
    now = classmethod(now)

    def utcnow(cls):
        t = _time.time()
        return cls.utcfromtimestamp(t)
    utcnow = classmethod(utcnow)

    def combine(cls, date, time):
        if not isinstance(date, _date_class):
            raise TypeError("date argument must be a date instance")
        if not isinstance(time, _time_class):
            raise TypeError("time argument must be a time instance")
        return cls(date.year, date.month, date.day,
                   time.hour, time.minute, time.second, time.microsecond,
                   time.tzinfo)
    combine = classmethod(combine)

    def strptime(cls, date_string, format):
        return cls(*(_time.strptime(date_string, format))[0:6])

    strptime = classmethod(strptime)

    def timetuple(self):
        dst = self._dst()
        if dst is None:
            dst = -1
        elif dst:
            dst = 1
        return _build_struct_time(self.year, self.month, self.day,
                                  self.hour, self.minute, self.second,
                                  dst)

    def utctimetuple(self):
        y, m, d = self.year, self.month, self.day
        hh, mm, ss = self.hour, self.minute, self.second
        offset = self._utcoffset()
        if offset:  # neither None nor 0
            tm = tmxxx(y, m, d, hh, mm - offset)
            y, m, d = tm.year, tm.month, tm.day
            hh, mm = tm.hour, tm.minute
        return _build_struct_time(y, m, d, hh, mm, ss, 0)

    def date(self):
        return date(self.__year, self.__month, self.__day)

    def time(self):
        return time(self.hour, self.minute, self.second, self.microsecond)

    def timetz(self):
        return time(self.hour, self.minute, self.second, self.microsecond,
                    self._tzinfo)

    def replace(self, year=None, month=None, day=None, hour=None,
                minute=None, second=None, microsecond=None, tzinfo=True):
        if year is None:
            year = self.year
        if month is None:
            month = self.month
        if day is None:
            day = self.day
        if hour is None:
            hour = self.hour
        if minute is None:
            minute = self.minute
        if second is None:
            second = self.second
        if microsecond is None:
            microsecond = self.microsecond
        if tzinfo is True:
            tzinfo = self.tzinfo
        _check_date_fields(year, month, day)
        _check_time_fields(hour, minute, second, microsecond)
        _check_tzinfo_arg(tzinfo)
        return datetime(year, month, day, hour, minute, second,
                          microsecond, tzinfo)

    def astimezone(self, tz):
        if not isinstance(tz, tzinfo):
            raise TypeError("tz argument must be an instance of tzinfo")

        mytz = self.tzinfo
        if mytz is None:
            raise ValueError("astimezone() requires an aware datetime")

        if tz is mytz:
            return self

        myoffset = self.utcoffset()
        if myoffset is None:
            raise ValueError("astimezone() requires an aware datetime")
        utc = (self - myoffset).replace(tzinfo=tz)

        return tz.fromutc(utc)

    def ctime(self):
        "Format a la ctime()."
        t = tmxxx(self.__year, self.__month, self.__day, self.__hour,
                  self.__minute, self.__second)
        return t.ctime()

    def isoformat(self, sep='T'):
        s = ("%04d-%02d-%02d%c" % (self.__year, self.__month, self.__day,
                                  sep) +
                _format_time(self.__hour, self.__minute, self.__second,
                             self.__microsecond))
        off = self._utcoffset()
        if off is not None:
            if off < 0:
                sign = "-"
                off = -off
            else:
                sign = "+"
            hh, mm = divmod(off, 60)
            s += "%s%02d:%02d" % (sign, hh, mm)
        return s

    def __repr__(self):
        "Convert to formal string, for repr()."
        L = [self.__year, self.__month, self.__day, # These are never zero
             self.__hour, self.__minute, self.__second, self.__microsecond]
        if L[-1] == 0:
            del L[-1]
        if L[-1] == 0:
            del L[-1]
        s = ", ".join(map(str, L))
        s = "%s(%s)" % ('datetime.' + self.__class__.__name__, s)
        if self._tzinfo is not None:
            assert s[-1:] == ")"
            s = s[:-1] + ", tzinfo=%r" % self._tzinfo + ")"
        return s

    def __str__(self):
        return self.isoformat(sep=' ')

    def utcoffset(self):
        offset = _call_tzinfo_method(self._tzinfo, "utcoffset", self)
        offset = _check_utc_offset("utcoffset", offset)
        if offset is not None:
            offset = timedelta(minutes=offset)
        return offset

    def _utcoffset(self):
        offset = _call_tzinfo_method(self._tzinfo, "utcoffset", self)
        offset = _check_utc_offset("utcoffset", offset)
        return offset

    def tzname(self):
        name = _call_tzinfo_method(self._tzinfo, "tzname", self)
        _check_tzname(name)
        return name

    def dst(self):
        offset = _call_tzinfo_method(self._tzinfo, "dst", self)
        offset = _check_utc_offset("dst", offset)
        if offset is not None:
            offset = timedelta(minutes=offset)
        return offset

    def _dst(self):
        offset = _call_tzinfo_method(self._tzinfo, "dst", self)
        offset = _check_utc_offset("dst", offset)
        return offset

    def __eq__(self, other):
        if isinstance(other, datetime):
            return self.__cmp(other) == 0
        elif hasattr(other, "timetuple") and not isinstance(other, date):
            return NotImplemented
        else:
            return False

    def __ne__(self, other):
        if isinstance(other, datetime):
            return self.__cmp(other) != 0
        elif hasattr(other, "timetuple") and not isinstance(other, date):
            return NotImplemented
        else:
            return True

    def __le__(self, other):
        if isinstance(other, datetime):
            return self.__cmp(other) <= 0
        elif hasattr(other, "timetuple") and not isinstance(other, date):
            return NotImplemented
        else:
            _cmperror(self, other)

    def __lt__(self, other):
        if isinstance(other, datetime):
            return self.__cmp(other) < 0
        elif hasattr(other, "timetuple") and not isinstance(other, date):
            return NotImplemented
        else:
            _cmperror(self, other)

    def __ge__(self, other):
        if isinstance(other, datetime):
            return self.__cmp(other) >= 0
        elif hasattr(other, "timetuple") and not isinstance(other, date):
            return NotImplemented
        else:
            _cmperror(self, other)

    def __gt__(self, other):
        if isinstance(other, datetime):
            return self.__cmp(other) > 0
        elif hasattr(other, "timetuple") and not isinstance(other, date):
            return NotImplemented
        else:
            _cmperror(self, other)

    def __cmp(self, other):
        assert isinstance(other, datetime)
        mytz = self._tzinfo
        ottz = other._tzinfo
        myoff = otoff = None

        if mytz is ottz:
            base_compare = True
        else:
            if mytz is not None:
                myoff = self._utcoffset()
            if ottz is not None:
                otoff = other._utcoffset()
            base_compare = myoff == otoff

        if base_compare:
            return cmp((self.__year, self.__month, self.__day,
                        self.__hour, self.__minute, self.__second,
                        self.__microsecond),
                       (other.__year, other.__month, other.__day,
                        other.__hour, other.__minute, other.__second,
                        other.__microsecond))
        if myoff is None or otoff is None:
            raise TypeError("cannot compare naive and aware datetimes")
        diff = self - other     # this will take offsets into account
        if diff.days < 0:
            return -1
        return diff and 1 or 0

    def __add__(self, other):
        if not isinstance(other, timedelta):
            return NotImplemented
        t = tmxxx(self.__year,
                  self.__month,
                  self.__day + other.days,
                  self.__hour,
                  self.__minute,
                  self.__second + other.seconds,
                  self.__microsecond + other.microseconds)
        self._checkOverflow(t.year)
        result = datetime(t.year, t.month, t.day,
                                t.hour, t.minute, t.second,
                                t.microsecond, tzinfo=self._tzinfo)
        return result

    __radd__ = __add__

    def __sub__(self, other):
        if not isinstance(other, datetime):
            if isinstance(other, timedelta):
                return self + -other
            return NotImplemented

        days1 = self.toordinal()
        days2 = other.toordinal()
        secs1 = self.__second + self.__minute * 60 + self.__hour * 3600
        secs2 = other.__second + other.__minute * 60 + other.__hour * 3600
        base = timedelta(days1 - days2,
                         secs1 - secs2,
                         self.__microsecond - other.__microsecond)
        if self._tzinfo is other._tzinfo:
            return base
        myoff = self._utcoffset()
        otoff = other._utcoffset()
        if myoff == otoff:
            return base
        if myoff is None or otoff is None:
            raise TypeError, "cannot mix naive and timezone-aware time"
        return base + timedelta(minutes = otoff-myoff)

    def __hash__(self):
        tzoff = self._utcoffset()
        if tzoff is None:
            return hash(self.__getstate()[0])
        days = _ymd2ord(self.year, self.month, self.day)
        seconds = self.hour * 3600 + (self.minute - tzoff) * 60 + self.second
        return hash(timedelta(days, seconds, self.microsecond))

    __safe_for_unpickling__ = True      # For Python 2.2

    def __getstate(self):
        yhi, ylo = divmod(self.__year, 256)
        us2, us3 = divmod(self.__microsecond, 256)
        us1, us2 = divmod(us2, 256)
        basestate = ("%c" * 10) % (yhi, ylo, self.__month, self.__day,
                                   self.__hour, self.__minute, self.__second,
                                   us1, us2, us3)
        if self._tzinfo is None:
            return (basestate,)
        else:
            return (basestate, self._tzinfo)

    def __setstate(self, string, tzinfo):
        (yhi, ylo, self.__month, self.__day, self.__hour,
         self.__minute, self.__second, us1, us2, us3) = map(ord, string)
        self.__year = yhi * 256 + ylo
        self.__microsecond = (((us1 << 8) | us2) << 8) | us3
        self._tzinfo = tzinfo

    def __reduce__(self):
        return (self.__class__, self.__getstate())

    if _sys.platform.startswith('java'):
        def __tojava__(self, java_class):
            if java_class not in (Calendar, Timestamp, Object):
                return Py.NoConversion

            calendar = Calendar.getInstance()
            calendar.clear()
            calendar.set(self.year, self.month - 1, self.day,
                         self.hour, self.minute, self.second)

            if java_class == Calendar:
                calendar.set(Calendar.MILLISECOND, self.microsecond // 1000)
                return calendar
            else:
                timestamp = Timestamp(calendar.getTimeInMillis())
                timestamp.setNanos(self.microsecond * 1000)
                return timestamp


datetime.min = datetime(1, 1, 1)
datetime.max = datetime(9999, 12, 31, 23, 59, 59, 999999)
datetime.resolution = timedelta(microseconds=1)


def _isoweek1monday(year):
    THURSDAY = 3
    firstday = _ymd2ord(year, 1, 1)
    firstweekday = (firstday + 6) % 7 # See weekday() above
    week1monday = firstday - firstweekday
    if firstweekday > THURSDAY:
        week1monday += 7
    return week1monday
