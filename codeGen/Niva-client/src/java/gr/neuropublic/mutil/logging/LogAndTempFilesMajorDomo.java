package gr.neuropublic.mutil.logging;

import java.io.File;
import java.util.Random;

import gr.neuropublic.mutil.base.Util;



public class LogAndTempFilesMajorDomo {

	private static final File BASE_BW_DIR = new File(System.getProperty("java.io.tmpdir"), LoggingConstants.BEYWATCH_TMP_DIR_NAME);
	private static Random r = new Random(0);
	public static File getLoggingDirectory() {
		File logDir = new File(BASE_BW_DIR, LoggingConstants.LOG_DIR_NAME);
		logDir.mkdirs();
		return logDir;
	}
	
	private static File getTempDirectory() {
		File tmpDir = new File(BASE_BW_DIR, LoggingConstants.EXRCS_DIR_NAME);
		tmpDir.mkdirs();
		return tmpDir;
	}
	
	public static File createExerciseInputFile() throws Exception {
		while(true) { 
			File retValue = new File(LogAndTempFilesMajorDomo.getTempDirectory(), "beywatch-"+Util.timestamp_YYYYMMDDHH24MISS()+".input");
			if (retValue.exists())
				Thread.sleep(1000+ r.nextInt(500));
			else
				return retValue;
		}
	}

}
