package gr.neuropublic.mutil.logging;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import gr.neuropublic.mutil.base.ExceptionAdapter;
import gr.neuropublic.mutil.base.Util;


public class FileLogCompressor implements Runnable {
	
	private final IFileLogDispatcherCompressorSynch synch;
	private final File directory;
	private final String preffix;
	public FileLogCompressor(IFileLogDispatcherCompressorSynch synch, File directory, String preffix) {
		this.synch = synch;
		this.directory = directory;
		this.preffix = preffix;
	}
	public static final String GZ = "gz";

	@Override
	public void run() {
		try {
			String[] files = directory.list(new FilenameFilter() {
				@Override
				public boolean accept(File directory2, String fileName) {
					if (!directory2.equals(directory)) throw new RuntimeException(); // impossible
					if (!fileName.startsWith(preffix)) return false;
					File file = new File (directory2, fileName);
					if (!file.exists()) return false;
					if (!file.canRead()) return false;
					if (!file.canWrite()) return false;
					long lastModified = file.lastModified();
					long now = System.currentTimeMillis();
					if (!(  now > lastModified + TimeUnit.HOURS.toMillis(3))) return false;
					if (fileName.endsWith(GZ)) return false;
					return true;
				}
			});
			for (String fileS : files) {
				File file = new File(directory, fileS);
				String from = file.getAbsolutePath();
				String to = from +"."+GZ;
				Util.gzipFile(from, to);
				file.delete();
			}
			synch.setCompressorRunning(false);
		} catch (Exception e) {
			throw new ExceptionAdapter(e);
		}
	}

}
