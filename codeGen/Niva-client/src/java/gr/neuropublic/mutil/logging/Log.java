// Copyright (c) 2001 Hursh Jain (http://www.mollypages.org) 
// The Molly framework is freely distributable under the terms of an
// MIT-style license. For details, see the molly pages web site at:
// http://www.mollypages.org/. Use, modify, have fun !

package gr.neuropublic.mutil.logging;

import gr.neuropublic.mutil.io.IOUtil;
import java.text.*;
import java.util.*;
import java.lang.reflect.*;


import gr.neuropublic.mutil.base.Argcheck;
import gr.neuropublic.mutil.base.StringUtil;
/** 
A system wide logging facility. Clearer and more elegant 
semantics/nomenclature than <tt>java.util.logging</tt>.
<p>
Logs messages to some user specified destination(s). Each log has a
<b>name</b> and a <b>log level</b>.
<p>
The names are arbitrary (hopefully descriptive) names, per your liking. Many
different logs can be created and then later retrieved by their
name as needed.
<p>
The levels provided by this class have the order:
	<blockquote>
	<tt>
	OFF <b>&lt;</b> ERROR <b>&lt; </b>WARN <b>&lt;</b> INFO <b>&lt;</b> DEBUG
	</tt>
	</blockquote>
The above levels are self-explanatory. 
<p>
Convenience methods with names equal to a <b>level</b> name are provided.
So for example, instead of saying:
	<blockquote>
	<tt>log(<b>LogLevel.warn</b>, "the message");</tt>
	</blockquote>
one can say:
	<blockquote>
	<tt>log.<b>warn</b>("the message");</tt>
	</blockquote>
<p>
<div style="border: 1px solid #ccc; padding: 1em;">
A little historical quirk. For the debug level <tt>LogLevel.debug</b></tt>,
one can say:
	<blockquote>
	<pre>
	<tt>log.<b>debug</b>("the message");</tt> 
	   --<b>or</b>--
	<tt>log.<b>bug</b>("the message");</tt> 
	</pre>
 	</blockquote>
</div>
<p>
A default logger of type {@link fc.io.SystemLog} is provided for
convenience in this class and can be retrieved by calling the {@link
#getDefault()} method.
<p>
The {@link #closeLog(String)} method is called on all logs
at JVM shutdown.
<p>
<b>Note:</b>To log a full stack traces, pass the string obtained by the
{@link IOUtil#throwableToString} method. (by default, an exception simply
prints its message but not the full stack trace)
<p>
<b>Implementation Note</b>: subclasses should implement static
<tt>getLog(..)</tt> type methods. These that create, as needed, and
return a new log object of that subclass type. These getter methods
should be static for convenience. Also, the implementation of the
<tt>subclass.getLog(...)</tt> methods in subclasses is expected by
convention.
<p>
Thread Safety: This class <tt>is</tt> Threadsafe and all it's methods can
be used concurrently.

@author hursh jain
**/
public abstract class Log 
{
//--static variables--
public static final LogLevel OFF	= new LogLevel("OFF", 	0);
public static final LogLevel ERROR	= new LogLevel("ERROR", 1);
public static final LogLevel WARN	= new LogLevel("WARN",  2);
public static final LogLevel INFO	= new LogLevel("INFO",  3);
public static final LogLevel DEBUG	= new LogLevel("DEBUG", 4);

/**
The default level used for new logs. Subclasses should (by default) 
create logs using this level.
*/
public static LogLevel DEFAULT_LEVEL = INFO;

protected   static final byte[] linesepbytes = 
            System.getProperty("line.separator").getBytes();

            //synchronized map needed
protected   static          SystemLog   defaultlog;
protected   static final    Map         logs = new Hashtable(); 

//--instance variables apply to a particular logger
protected   String      name;
protected   LogLevel    currentLevel;
protected   long        startTime;  
protected   String      startTimeString; //for toString()
protected   boolean     printLevelName          = true;
protected   boolean     printTimestamp          = false;
protected   boolean     timestampIsRelative     = false;
protected   LogLevel    printMethodInfoAtLevel  = DEBUG;

//default timestamp data
protected   final   SimpleDateFormat df 
                        = new SimpleDateFormat("MMM dd H:m:s z");
protected   final   NumberFormat nf     
                        = NumberFormat.getNumberInstance();

protected   Date        date        = new Date();
protected   long        last_time   = date.getTime();
protected   String      timestr     = df.format(date);

//Add shutdown hook to close all open logs
static 
	{
	Runtime.getRuntime().addShutdownHook(new Thread("fc.io.Log.Shutdown") {
		public void run() 
			{
			// mperdikeas commented the following one (1) line 2011-10-24
			//System.out.println("JVM shutdown: fc.io.Log - closing all logs...");
			Iterator i = logs.values().iterator();
			while (i.hasNext())
				((Log)i.next()).close();
			// mperdikeas commented the following one (1) line 2011-10-24
			// System.out.println();			
			}
		});
	}


/**
Constructs a new Log. Can only be called from subclasses.

@param	name			the name for the log -- any arbitrary
						string denoting a conceptual category, 
						destination, whatever
@param  level			the initial logging level for this log
*/
protected Log(String name, LogLevel level) 
	{
	assert name != null : "name was null";
	assert level != null : "level was null" ;
	this.name = name;
	this.currentLevel 	 = level;		
	this.startTime	     = System.currentTimeMillis();
	this.startTimeString = new Date(startTime).toString();
	}


/**
Returns a {@link SystemLog} with the specified name. If the log does not already
exist, creates and returns a new SystemLog with that name.
<p>
The system log created will have a default destination of <tt>System.err</tt>
and a default level of {@link Log#INFO}. 
<p>
To obtain logs with a different destination, create a SystemLog directly.
*/
public static Log get(String name)
	{
	Log log = (Log) logs.get(name);
	if (log != null)
		return log;
		
	synchronized(logs) {
		log = new SystemLog(name);
		logs.put(name, log);	
		}
		
	return log;
	}

/**
Convenience method that returns the log named after the <b>package</b> that
the specified class belong to. If 2 classes <pre>a.b.Class1</pre> and
<pre>a.b.Class2</pre> call this method, they will get the same logger
(named <tt>a.b</tt>).
<p>
If the log does not already exist, creates and returns a new {@link SystemLog} 
with that name.

@param	c	a non-null class
*/
public final static Log get(Class c) 
	{
	Argcheck.notnull(c, "class parameter was null");	
	final Package p = c.getPackage();
	final String name = (p == null) ? "default_pkg" : p.toString();
	return get(name);
	}

/**
Convenience method that returns a log named after the <b>package</b> that
the specified object's class belong to. If 2 objects of class
<pre>a.b.Class1</pre> and <pre>a.b.Class2</pre> call this method, they
will get the same logger (named <tt>a.b</tt>).
<p>
If the log does not already exist, creates and returns a new {@link SystemLog} 
with that name.

@param	obj		a non-null object
*/
public final static Log get(Object obj) 
	{
	Argcheck.notnull(obj, "class parameter was null");	
	return get(obj.getClass().getPackage().toString());
	}

/**
Returns the default system log. This system log writes to
<tt>System.err</tt> and has it's level set to {@link SystemLog#INFO}. This
level can be changed to some other level if desired by invoking {@link
setLevel()} on the returned object.
*/
public static SystemLog getDefault()
	{
	synchronized (Log.class)
		{
		if (defaultlog == null) {
			defaultlog = new SystemLog(
				"_defaultlog", System.out, SystemLog.INFO);
			}
		}		
	return defaultlog;
	}


/**
Returns an iteration containing level names for this log. The names can be in
any order.
*/
public Iterator getLevelNames() 
	{
	Class myclass = getClass();
	Field[] fields = myclass.getDeclaredFields();
	Field levelfield = null;
	List l = new ArrayList();
	for (int n = 0; n < fields.length; n++) 
		{
		Field f = fields[n];
		if (! f.getType().isAssignableFrom(LogLevel.class))
			continue;
		l.add(f.getName());
		}
	return l.iterator();
	}

/*
Manually adds the specified log to the list of all logs. If a log with
that name already exists, an <tt>IllegalArgumentException</tt> is thrown.
<p> This method is useful when creating a new logging object manaually.
So for example:
	<blockquote>
	<pre>
	MyLogClass mylog = new MyLogClass("foo.bar");
	Log.addLog(mylog)
	</pre>
	</blockquote>
Contrast that with the more usual:
	<blockquote>
	<pre>
	Log.getLog("foo.bar");
	</pre>
	</blockquote>
<p>
<u>Custom log implementations should always call this method in their
constructor to add themselves to the list of all logs.</u>
*/
protected static void addLog(Log log)
	{
	synchronized(logs)
		{
		if (! logs.containsKey(log.name)) {
			logs.put(log.name, log);	
			}	
		else throw new IllegalArgumentException("Log already exists: " + log);	
		}
	}

/**
Closes and removes the log with the specified name if it exists
*/
public static void closeLog(String name) 
	{
	if (logs.containsKey(name)) {
		Log l = (Log) logs.get(name);
		l.close();
		logs.remove(name);
		}
	}

/**
Returns the method name, file number and line number
of the calling method. Useful for logging/code tracing.

@param	level		the level for which this logging call was invoked.	
@param  framenum 	the method to examine. This method itself
					has frame number 0, the calling method
					has frame number 1, it's parent 2 and so on.
**/
public final String getDebugContext(LogLevel level, int framenum) 
	{
	if (level.intval <  printMethodInfoAtLevel.intval) {
		return "";
		}
		
	StackTraceElement ste[] = new Exception().getStackTrace();
	if (framenum >= ste.length)
		throw new IllegalArgumentException(
		 "framenum [" + framenum 
		 + "] too large. Max number of record in stack = "
		 + (ste.length - 1)); 

	//get method that called us, we are ste[0]
	StackTraceElement st = ste[framenum];
	String file = st.getFileName();
	int line = st.getLineNumber();
	String method = st.getMethodName();
	String threadname = Thread.currentThread().getName();
	//String classn = st.getClassName();
	return method + "() [" + file + ":" + line + "/thread:" + threadname + "]";   
	}


/**
If set to true, will print the level name before the logging
message. For example, if the level is <code>INFO</code>, the 
message is <code>foo</code>, then 
	<blockquote>
	INFO foo
	</blockquote>
will be printed. 
<p>
This is set to <tt>true</tt> by default.
*/
public void printLevelName(boolean printName)
	{
	printLevelName = printName;
	}

/**
Prints a time stamp with every message. By default this
is <tt>false</tt>

@param	val		 specify true to print time stamps, false
				 to not
*/
public void printTimestamp(boolean val) {
	printTimestamp = val;
	}

/**
Prints a relative time stamp with every message. By 
default, printing any timestamp is <tt>false</tt>.
<b>
Timestamps must first be enabled via the {@link printTimestamp} 
method before this method can have any effect.
</b>

@param	val 	if true, prints a <b>relative</b> time
				stamp. An initial timestamp is printed and
				all succeeding timestamps are second
				increments from the initial timestamp
*/
public void printRelativeTimestamp(boolean val) {
	timestampIsRelative = val;
	last_time = new Date().getTime();
	}

/**
By default, method, line and thread information is printed wich each
logging statement at the DEBUG level. Other levels print only the log
message but skip the method/stack information.
<p>
This method allows method information to be printed at all levels greater
than or equal to the specified level.
*/
public void printMethodInfoAtLevel(LogLevel level) {
	this.printMethodInfoAtLevel = level;
	}

/** 
A default implementation that returns an appropriate timestamp based on the
timestamp setttings. <b> Multiple threads must synchronize access to this
method </b>. Subclasses should call this method in their logging method
implementations in the following way.
<blockquote>
Suppose the subclass uses an out object as a printwriter. Then
<br>
<code>
<b>...in a synchronized block, typically around the output stream....</b>
if (printTimestamp) {
	out.print(getTS());
	out.print(" ");
	}
.....
</code>
</blockquote>
Of course subclasses are free to not call this method or 
print timestamps in some other fashion.
*/
protected final String getTS()
	{
	date = new Date();
	long now = date.getTime();

	if (timestampIsRelative)
		{
		return nf.format( ((now - last_time) / 1000) ) ;	
		}
	else  //non relative ts
		{
		if (now - last_time >= 1000) {
			last_time = now;
			timestr = df.format(date);
			}
		}
		
	return timestr;
	}

/**
Sets the current logging level for this logger. Each log has a logging
level. A message is printed only if the message level is equal to or lower
than the current maximum level for that log.
<p>
Typically, classes that implement a log will define a bunch of static
variables of type {@link LogLevel} that list the available levels for that
implementation. Clients of a particular log class should use levels
defined within only that class.
*/
public void setLevel(LogLevel level) {
	assert level != null : "specified level was null";
	currentLevel = level;
	}
	
/**
Sets the level of this log based on a level description. This is
convenient for when levels are specified in a configuration file. If the
specified name cannot be converted into a level, then no change is made.

@param	levelname	the level name. For example, 
					<tt>"info"</tt> would set the level of 
					this log to <tt>INFO</tt>. The name
					is case-<b>in</b>sensitive. 
*/
public void setLevel(String levelname) 
	{
	if (levelname == null) {
		warn("specified levelname was null, log level will not be changed");
		return;
		}
		
	try {
		Field levelfield = stringToLevel(levelname);
		if (levelfield == null) {		
			warn("Specified level", levelname, "is not valid/could not be resolved");
			return;
			}

		Method method = Log.class.getMethod("setLevel", new Class[] { LogLevel.class });
		//System.out.println("got method="+method);
		method.invoke(this, new Object[] { levelfield.get(this) });
		info("New log level set to: ", currentLevel);
		}
	catch (Exception e) {
		warn(e);
		}
	}

/*
Returns a level field corresponding to the specified case-insensitive
string.

this is kinda overkill but hey: It takes about 5ms so speed
isn't an issue and if we ever add more levels, we won't have to
update this method.
*/
protected static Field stringToLevel(String levelname)
	{
	Field[] fields = Log.class.getDeclaredFields();
	Field levelfield = null;
	for (int n = 0; n < fields.length; n++) 
		{
		Field f = fields[n];
		if (! f.getType().isAssignableFrom(LogLevel.class))
			continue;
		if (f.getName().equalsIgnoreCase(levelname))
			levelfield = f;
		}
	return levelfield;
	}
	
/**
Sets the level for all logs whose name <b>contain</b> the specified name.
This is convenient when changing log levels for package heirarchies. A
empty string (non-null) "" sets the level for all logs.

@param	levelname	the level name. For example, 
					<tt>"info"</tt> would set the level of 
					this log to <tt>INFO</tt>. The name
					is case-<b>in</b>sensitive. 
*/
public static void setLevelForAll(String name, LogLevel level)
	{
	Iterator i = logs.keySet().iterator();
	while (i.hasNext()) {
		String logname = (String) i.next();
		if (logname.contains(logname))	{
			((Log)logs.get(logname)).setLevel(level);
			}
		}
	}

/**
Sets the new default logging level for all new instances of loggers
(that are created after this method is invoked).
*/
public static void setDefaultLevel(LogLevel level)
	{
	DEFAULT_LEVEL = level;
	}

/**
Sets the new default logging level for all new instances of loggers
(created after this method is invoked).
*/
public static void setDefaultLevel(String level)
	{
	if (level == null) {
		new Exception("the specified level was null, log level will not be changed").printStackTrace();
		return;
		}
		
	try {
		Field levelfield = stringToLevel(level);
		if (levelfield == null)
			return;
			
		Method method = Log.class.getMethod("setDefaultLevel", new Class[] { LogLevel.class });
		//System.out.println("got method="+method);
		method.invoke(null, new Object[] { levelfield.get(null) });
		}
	catch (Exception e) {
		e.printStackTrace();
		}
	}

/**
Returns <tt>true</tt> if the log's current level will allow logging
messages at the specified logging level.
<p>
Implementation Note: If the currentLevel is lesser or equal to the
specified level returns true, else false. Subclasses can override this
method if needed.

@param	level	the specified logging level
*/
public boolean canLog(LogLevel level) 
	{
	assert level != null : "specified level was null";
	if (level.intval > currentLevel.intval)
		return false;
	return true;
	}

/**
Returns the name of this log.
*/
public String getName() {
	return name;
	}

/**
Returns the current level set for this log. Useful when
printing out debugging info.
*/
public LogLevel getLevel() {
	return currentLevel;
	}
	
public void logSystemInfo() 
	{	
	StringBuffer buf = new StringBuffer(1024);
	Properties p = System.getProperties();
	Enumeration e = p.propertyNames();
	while (e.hasMoreElements()) {
		buf.append(IOUtil.LINE_SEP);
		String name = (String) e.nextElement();		
		
		buf.append(name).append("=");
		
		if (name.equals("line.separator"))
			buf.append(StringUtil.viewableAscii(
							p.getProperty(name)));
		else
			buf.append(p.getProperty(name));		
		}
	buf.append(IOUtil.LINE_SEP);		
	info(buf.toString());
	}

public String toString()
	{
	return name + " [" + getClass().getName() + 
			"/currentlevel:" + currentLevel.desc + 
			"/started:" + startTimeString + "]";
	}

//--methods for various levels
public final void error(final Object str1) {
	doLog(ERROR, str1);	
	}
	
public final void error(final Object str1, final Object str2) {
	doLog(ERROR, str1, str2);	
	}
	
public final void error(final Object str1, final Object str2, final Object str3) {
	doLog(ERROR, str1, str2, str3);	
	}

public final void error(final Object str1, final Object str2, final Object str3, final Object str4) {
	doLog(ERROR, str1, str2, str3, str4);	
	}

public final void error(final Object str1, final Object str2, final Object str3, final Object str4, final Object str5) {
	doLog(ERROR, str1, str2, str3, str4, str5);	
	}

public final void error(final Object str1, final Object str2, final Object str3, final Object str4, final Object str5, final Object str6) {
	doLog(ERROR, str1, str2, str3, str4, str5, str6);	
	}

public final void error(final Object str1, final Object str2, final Object str3, final Object str4, final Object str5, final Object str6, final Object str7) {
	doLog(ERROR, str1, str2, str3, str4, str5, str6, str7);	
	}

public final void error(final Object str1, final Object str2, final Object str3, final Object str4, final Object str5, final Object str6, final Object str7, Object... args) {
	doLog(ERROR, str1, str2, str3, str4, str5, str6, str7, args);	
	}
	
public final void warn(final Object str1) {
	doLog(WARN, str1);	
	}
	
public final void warn(final Object str1, final Object str2) {
	doLog(WARN, str1, str2);	
	}
	
public final void warn(final Object str1, final Object str2, final Object str3) {
	doLog(WARN, str1, str2, str3);	
	}

public final void warn(final Object str1, final Object str2, final Object str3, final Object str4) {
	doLog(WARN, str1, str2, str3, str4);	
	}

public final void warn(final Object str1, final Object str2, final Object str3, final Object str4, final Object str5) {
	doLog(WARN, str1, str2, str3, str4, str5);	
	}

public final void warn(final Object str1, final Object str2, final Object str3, final Object str4, final Object str5, final Object str6) {
	doLog(WARN, str1, str2, str3, str4, str5, str6);	
	}

public final void warn(final Object str1, final Object str2, final Object str3, final Object str4, final Object str5, final Object str6, final Object str7) {
	doLog(WARN, str1, str2, str3, str4, str5, str6, str7);	
	}

public final void warn(final Object str1, final Object str2, final Object str3, final Object str4, final Object str5, final Object str6, final Object str7, Object... args) {
	doLog(WARN, str1, str2, str3, str4, str5, str6, str7, args);	
	}

public final void info(final Object str1) {
	doLog(INFO, str1);	
	}
	
public final void info(final Object str1, final Object str2) {
	doLog(INFO, str1, str2);	
	}
	
public final void info(final Object str1, final Object str2, final Object str3) {
	doLog(INFO, str1, str2, str3);	
	}

public final void info(final Object str1, final Object str2, final Object str3, final Object str4) {
	doLog(INFO, str1, str2, str3, str4);	
	}
	
public final void info(final Object str1, final Object str2, final Object str3, final Object str4, final Object str5) {
	doLog(INFO, str1, str2, str3, str4, str5);	
	}

public final void info(final Object str1, final Object str2, final Object str3, final Object str4, final Object str5, final Object str6) {
	doLog(INFO, str1, str2, str3, str4, str5, str6);	
	}

public final void info(final Object str1, final Object str2, final Object str3, final Object str4, final Object str5, final Object str6, final Object str7) {
	doLog(INFO, str1, str2, str3, str4, str5, str6, str7);	
	}

public final void info(final Object str1, final Object str2, final Object str3, final Object str4, final Object str5, final Object str6, final Object str7, Object... args) {
	doLog(INFO, str1, str2, str3, str4, str5, str6, str7, args);	
	}

public final void debug(final Object str1) {
	doLog(DEBUG, str1);	
	}

public final void debug(final Object str1, final Object str2) {
	doLog(DEBUG, str1, str2);	
	}
	
public final void debug(final Object str1, final Object str2, final Object str3) {
	doLog(DEBUG, str1, str2, str3);	
	}

public final void debug(final Object str1, final Object str2, final Object str3, final Object str4) {
	doLog(DEBUG, str1, str2, str3, str4);	
	}

public final void debug(final Object str1, final Object str2, final Object str3, final Object str4, final Object str5) {
	doLog(DEBUG, str1, str2, str3, str4, str5);	
	}

public final void debug(final Object str1, final Object str2, final Object str3, final Object str4, final Object str5, final Object str6) {
	doLog(DEBUG, str1, str2, str3, str4, str5, str6);	
	}

public final void debug(final Object str1, final Object str2, final Object str3, final Object str4, final Object str5, final Object str6, final Object str7) {
	doLog(DEBUG, str1, str2, str3, str4, str5, str6, str7);	
	}

public final void debug(final Object str1, final Object str2, final Object str3, final Object str4, final Object str5, final Object str6, final Object str7, Object... args) {
	doLog(DEBUG, str1, str2, str3, str4, str5, str6, str7, args);	
	}

public final void bug(final Object str1) {
	doLog(DEBUG, str1);	
	}

public final void bug(final Object str1, final Object str2) {
	doLog(DEBUG, str1, str2);	
	}
	
public final void bug(final Object str1, final Object str2, final Object str3) {
	doLog(DEBUG, str1, str2, str3);	
	}

public final void bug(final Object str1, final Object str2, final Object str3, final Object str4) {
	doLog(DEBUG, str1, str2, str3, str4);	
	}

public final void bug(final Object str1, final Object str2, final Object str3, final Object str4, final Object str5) {
	doLog(DEBUG, str1, str2, str3, str4, str5);	
	}

public final void bug(final Object str1, final Object str2, final Object str3, final Object str4, final Object str5, final Object str6) {
	doLog(DEBUG, str1, str2, str3, str4, str5, str6);	
	}

public final void bug(final Object str1, final Object str2, final Object str3, final Object str4, final Object str5, final Object str6, final Object str7) {
	doLog(DEBUG, str1, str2, str3, str4, str5, str6, str7);	
	}

public final void bug(final Object str1, final Object str2, final Object str3, final Object str4, final Object str5, final Object str6, final Object str7, Object... args) {
	doLog(DEBUG, str1, str2, str3, str4, str5, str6, str7, args);	
	}
	
final void doLog(final LogLevel level, final Object str1) 
	{
	if (level.intval > currentLevel.intval)
		return;
	
	log(level, getDebugContext(level, 3), str1);
	}

final void doLog(final LogLevel level, final Object str1, final Object str2) 
	{
	if (level.intval > currentLevel.intval)
		return;
	
	log(level, getDebugContext(level, 3), str1, str2);
	}

final void doLog(final LogLevel level, final Object str1, 
				 final Object str2, final Object str3) 
	{
	if (level.intval > currentLevel.intval)
		return;
	
	log(level, getDebugContext(level, 3), str1, str2, str3);
	}

final void doLog(final LogLevel level, final Object str1, final Object str2, 
				 final Object str3, final Object str4) 
	{
	if (level.intval > currentLevel.intval)
		return;
	
	log(level, getDebugContext(level, 3), str1, str2, str3, str4);
	}

final void doLog(final LogLevel level, final Object str1, final Object str2, 
				 final Object str3, final Object str4, final Object str5) 
	{
	if (level.intval > currentLevel.intval)
		return;
	
	log(level, getDebugContext(level, 3), str1, str2, str3, str4, str5);
	}

final void doLog(final LogLevel level, final Object str1, final Object str2, 
				 final Object str3, final Object str4, final Object str5, 
				 final Object str6) 
	{
	if (level.intval > currentLevel.intval)
		return;
	
	log(level, getDebugContext(level, 3), str1, str2, str3, str4, str5, str6);
	}

final void doLog(final LogLevel level, final Object str1, final Object str2, 
				 final Object str3, final Object str4, final Object str5, 
				 final Object str6, final Object str7) 
	{
	if (level.intval > currentLevel.intval)
		return;
	
	log(level, getDebugContext(level, 3), str1, str2, str3, str4, str5, str6, str7);
	}

final void doLog(final LogLevel level, final Object str1, final Object str2, 
				 final Object str3, final Object str4, final Object str5, 
				 final Object str6, final Object str7, Object... args) 
	{
	if (level.intval > currentLevel.intval)
		return;
	
	log(level, getDebugContext(level, 3), str1, str2, str3, str4, str5, str6, str7, args);
	}


//--abstract methods--

public abstract void close();


/**
@param	level	the current log level. This can be logged
				as well.
@param	str1	unless overridden in a subclass, this is the
				value returned by {@link getDebugContext} and
				is generated automatically by the warn(), 
				info(), debug() etc., methods
*/
public abstract void log(LogLevel level, Object str1); 

/**
@param	level	the current log level. This can be logged
				as well.
@param	str1	unless overridden in a subclass, this is the
				value returned by {@link getDebugContext} and
				is generated automatically by the warn(), 
				info(), debug() etc., methods
@param	str2_onwards	
				some arbitrary object
*/
public abstract void log(LogLevel level, final Object str1, final Object str2);

/**
@param	level	the current log level. This can be logged
				as well.
@param	str1	unless overridden in a subclass, this is the
				value returned by {@link getDebugContext} and
				is generated automatically by the warn(), 
				info(), debug() etc., methods
@param	str2_onwards	
				some arbitrary object
*/
public abstract void log(LogLevel level, final Object str1, final Object str2, final Object str3);

/**
@param	level	the current log level. This can be logged
				as well.
@param	str1	unless overridden in a subclass, this is the
				value returned by {@link getDebugContext} and
				is generated automatically by the warn(), 
				info(), debug() etc., methods
@param	str2_onwards	
				some arbitrary object
*/
public abstract void log(LogLevel level, final Object str1, final Object str2, final Object str3, final Object str4);

/**
@param	level	the current log level. This can be logged
				as well.
@param	str1	unless overridden in a subclass, this is the
				value returned by {@link getDebugContext} and
				is generated automatically by the warn(), 
				info(), debug() etc., methods
@param	str2_onwards	
				some arbitrary object
*/
public abstract void log(LogLevel level, final Object str1, final Object str2, final Object str3, final Object str4, final Object str5);

/**
@param	level	the current log level. This can be logged
				as well.
@param	str1	unless overridden in a subclass, this is the
				value returned by {@link getDebugContext} and
				is generated automatically by the warn(), 
				info(), debug() etc., methods
@param	str2_onwards	
				some arbitrary object
*/
public abstract void log(LogLevel level, final Object str1, final Object str2, final Object str3, final Object str4, final Object str5, final Object str6);

/**
@param	level	the current log level. This can be logged
				as well.
@param	str1	unless overridden in a subclass, this is the
				value returned by {@link getDebugContext} and
				is generated automatically by the warn(), 
				info(), debug() etc., methods
@param	str2_onwards	
				some arbitrary object
*/
public abstract void log(LogLevel level, final Object str1, final Object str2, final Object str3, final Object str4, final Object str5, final Object str6, final Object str7);

/**
@param	level	the current log level. This can be logged
				as well.
@param	str1	unless overridden in a subclass, this is the
				value returned by {@link getDebugContext} and
				is generated automatically by the warn(), 
				info(), debug() etc., methods
@param	str2_onwards	
				some arbitrary object
*/
public abstract void log(LogLevel level, final Object str1, final Object str2, final Object str3, final Object str4, final Object str5, final Object str6, final Object str7, Object str8);

/**
@param	level	the current log level. This can be logged
				as well.
@param	str1	unless overridden in a subclass, this is the
				value returned by {@link getDebugContext} and
				is generated automatically by the warn(), 
				info(), debug() etc., methods
@param	str2_onwards	
				some arbitrary object
*/
public abstract void log(LogLevel level, final Object str1, final Object str2, final Object str3, final Object str4, final Object str5, final Object str6, final Object str7, Object str8, Object... args);
}          //~class Log


