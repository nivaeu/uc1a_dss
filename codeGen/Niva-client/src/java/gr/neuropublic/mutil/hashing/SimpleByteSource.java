package gr.neuropublic.mutil.hashing;


import java.io.File;
import java.io.InputStream;
import java.util.Arrays;

public class SimpleByteSource implements ByteSource {

    private final byte[] bytes;
    private String cachedHex;
    private String cachedBase64;

    public SimpleByteSource(byte[] bytes) {
        this.bytes = bytes;
    }

    // Creates an instance by converting the characters to a byte array (assumes UTF-8 encoding).
    public SimpleByteSource(char[] chars) {
        this.bytes = CodecSupport.toBytes(chars);
    }

    // Creates an instance by converting the String to a byte array (assumes UTF-8 encoding).
    public SimpleByteSource(String string) {
        this.bytes = CodecSupport.toBytes(string);
    }


    // Creates an instance using the sources bytes directly - it does not create a copy of the argument's byte array.
    public SimpleByteSource(ByteSource source) {
        this.bytes = source.getBytes();
    }

    // Creates an instance by converting the file to a byte array.
    public SimpleByteSource(File file) {
        this.bytes = new BytesHelper().getBytes(file);
    }

    // Creates an instance by converting the stream to a byte array.
    public SimpleByteSource(InputStream stream) {
        this.bytes = new BytesHelper().getBytes(stream);
    }

    /*
     * Returns true if the specified object is a recognized data type that can be easily converted to
     * bytes by instances of this class, false otherwise.
     * This implementation returns true IFF the specified object is an instance of one of the following
     * types:
     * - byte[]
     * - char[]
     * - ByteSource
     * - String
     * - File
     * - InputStream
     */
    public static boolean isCompatible(Object o) {
        return o instanceof byte[] || o instanceof char[] || o instanceof String ||
                o instanceof ByteSource || o instanceof File || o instanceof InputStream;
    }

    public byte[] getBytes() {
        return this.bytes;
    }

    public boolean isEmpty() {
        return this.bytes == null || this.bytes.length == 0;
    }

    public String toHex() {
        if ( this.cachedHex == null ) {
            this.cachedHex = Hex.encodeToString(getBytes());
        }
        return this.cachedHex;
    }

    public String toBase64() {
        if ( this.cachedBase64 == null ) {
            this.cachedBase64 = Base64.encodeToString(getBytes());
        }
        return this.cachedBase64;
    }

    public String toString() {
        return toBase64();
    }

    public int hashCode() {
        if (this.bytes == null || this.bytes.length == 0) {
            return 0;
        }
        return Arrays.hashCode(this.bytes);
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o instanceof ByteSource) {
            ByteSource bs = (ByteSource) o;
            return Arrays.equals(getBytes(), bs.getBytes());
        }
        return false;
    }

    private static final class BytesHelper extends CodecSupport {
        public byte[] getBytes(File file) {
            return toBytes(file);
        }

        public byte[] getBytes(InputStream stream) {
            return toBytes(stream);
        }
    }
}
