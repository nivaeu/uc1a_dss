package gr.neuropublic.mutil.hashing;

import java.security.*;

enum DigestAlgorithm { MD5("MD5"), SHA1("SHA-1"), SHA256("SHA-256");

    private DigestAlgorithm(final String text) {
        this.text = text;
    }

    private final String text;

    @Override
    public String toString() {
        return text;
    }    

}



public class HashAlgorithms {

    public static final String sha1Hash(String msg) throws NoSuchAlgorithmException {
        return sha1Hash( (new SimpleByteSource(msg)).getBytes() );
    }

    public static final String sha1Hash(byte[] buf) throws NoSuchAlgorithmException {
        return digestHash(DigestAlgorithm.SHA1, buf);
    }


    public static final String sha256Hash(String msg) throws NoSuchAlgorithmException {
        return sha256Hash(1, msg);
    }

    public static final String sha256Hash(int iterations, String msg) throws NoSuchAlgorithmException {
        return sha256Hash( iterations, (new SimpleByteSource(msg)).getBytes() );
    }

    public static final String sha256Hash(int iterations, byte[] buf) throws NoSuchAlgorithmException {
        return digestHash(DigestAlgorithm.SHA256, iterations, buf);
    }



    public static final String md5Hash(String msg) throws NoSuchAlgorithmException {
        return md5Hash( (new SimpleByteSource(msg)).getBytes() );
    }

    public static final String md5Hash(byte[] buf) throws NoSuchAlgorithmException {
        return digestHash(DigestAlgorithm.MD5, buf);
    }

    private static final String digestHash(DigestAlgorithm algorithm, byte[] buf) throws NoSuchAlgorithmException {
        return digestHash(algorithm, 1, buf);
    }

    private static final String digestHash(DigestAlgorithm algorithm, int iterations, byte[] buf) throws NoSuchAlgorithmException {
	MessageDigest md = MessageDigest.getInstance(algorithm.toString());
        byte[] digest;
        digest = md.digest(buf);
        iterations--;
        for (int i = 0 ; i < iterations ; i++) { // hash the remaining iterations
            md.reset();
            digest = md.digest(digest);
        }
        return hexString(digest);
    }

    public static String hexString(byte[] bytes) {
	StringBuilder sb = new StringBuilder();
	for (byte b: bytes) {
            String hex = Integer.toHexString((int)0xff & b);
            if (hex.length()==1) sb.append("0");
            sb.append(hex);
	}
	return sb.toString();        
    }


}