package gr.neuropublic.mutil.python;

import java.sql.Connection;

import gr.neuropublic.mutil.base.Triad;
import gr.neuropublic.mutil.base.Pair;
import gr.neuropublic.mutil.base.Quad;

import java.io.File;
import java.io.FileOutputStream;
import java.io.BufferedReader;
import java.io.StringReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.regex.Matcher; 
import java.util.regex.Pattern;
import java.util.Properties;
import java.util.SortedSet;
import java.util.TreeSet;
import java.io.InputStream;
import java.io.FileInputStream;

import org.apache.commons.lang3.StringUtils;

import gr.neuropublic.mutil.base.ExceptionAdapter;
import gr.neuropublic.mutil.base.Holder;
import gr.neuropublic.mutil.base.FileUtil;
import gr.neuropublic.mutil.python.PythonExposer;
import gr.neuropublic.mutil.python.JythonUtils;

/***   NOTE: at some I would have to move QueryJythonHolder to reportprocessor2 ***/
import gr.neuropublic.mutil.reportprocessor2.QueryJythonHolder;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.python.core.PyException;

import static gr.neuropublic.mutil.base.FileUtil.getASCIIResourceAsString;
import java.io.StringWriter;

/***
 NOTE: this is a simplified version of the reportprocessor2.DBPythonExposer class without the
       reports-related baggage. When I find the time I would have to merge the two into one.
 ***/
public class SimpleDBPythonExposer {

    private static final Logger                 l = LoggerFactory.getLogger(PythonExposer.class);
    private static final String SQLHOLDER_BINDING = "sql";


    public static Quad<Map<String, Object>, String, String, PyException> prepareParameters(Connection conn, Map<String, Object> initParams, String script, List<String> preffixesOfInterest) throws IOException, SQLException {
        String scriptFuncs = StringUtils.join(PYTHON_HEADER, "\n"); //+ "\n" + script;
        String scriptWtFuncsAndPreample = null;
        Triad<Map<String, Object>, String, PyException> pythonExposerOut = null;
        QueryJythonHolder sql = new QueryJythonHolder(conn);

        String scriptFuncsAndPreamble = getASCIIResourceAsString(gr.neuropublic.mutil.reportprocessor2.DBPythonExposer.class, "datetime.py")+"\n" + scriptFuncs;
        Map<String, Object> bindings = new HashMap<String, Object>();
        bindings.put(SQLHOLDER_BINDING, sql);
        pythonExposerOut = PythonExposer.exposeParams(bindings, scriptFuncsAndPreamble, initParams, script, true, true);
        //printParameters(pythonExposerOut.a);
        return Quad.create(weed(pythonExposerOut.a, preffixesOfInterest), pythonExposerOut.b, sql.getQueries(), pythonExposerOut.c);
    }

    public static void printParameters(Map<String, Object> params) {
        if (params!=null) {
            SortedSet<String> keys = new TreeSet<String>(params.keySet());
            for (String key : keys)
                l.info(String.format("%s --> %s", key, params.get(key)==null?"[null value]":String.valueOf(params.get(key))));
        }
    }

    private static Map<String, Object> weed(Map<String, Object> globals, List<String> preffixesOfInterest) {
        Map<String, Object> retValue = new HashMap<String, Object>();
        for (String global : globals.keySet()) {
            if (startsWithAny(global, preffixesOfInterest))
                retValue.put(global, globals.get(global));
        }
        return retValue;
    }

    private static boolean startsWithAny(String a, List<String> bs) {
        for (String b : bs)
            if (a.startsWith(b)) return true;
        return false;
    }

    private static final String PYTHON_HEADER[] = {
        //        "# -*- coding: utf-8 -*-",
        "import sys",
        "import math",
        "",
        "",
        "",
        "def eq(x):",
        "    return x",
        "",
        "def ceiling(x):",
        "    return int(math.ceil(x))",
        "",
        "",
        "def N2N(v, *f):",
        "  if v is None:",
        "    return 'null'",
        "  elif type(v) is str:",
        "    return '\\''+v+'\\''",
        "  elif type(v) is unicode:",
        "    return '\\''+v+'\\''",
        "  elif not f:",
        "    if type(v) not in (date, datetime):",
        "      return v",
        "    else:",
        "      if type(v) is datetime:",
        "         return '\\''+v.strftime(\'%Y-%m-%dT%H:%M:%S\')+'\\''",
        "      else:",
        "         return '\\''+v.strftime(\'%Y-%m-%d\')+'\\''",
        "  else:",
        "    if type(v) in (date, datetime):",
        "        return '\\''+v.strftime(f[0])+'\\''",
        "    else:",
        "        return f[0]%v",
        "",
        "",
        "def NVL(v, default):",
        " if (v==None):",
        "  return default",
        " else:",
        "  return v",
        "",
        "def NVLZ(v):",
        " return NVL(v, 0)",
        "",
        "class MyRecord:",
        "    def __init__(self, dictData, calledFromSqlS=False):",
        "        self._dictData = dictData",
        "        if calledFromSqlS:",
        "            for k in dictData.internal.keySet():",
        "                kUpper = k.upper()",
        "                globals()[kUpper]=dictData.f(k)",
        "    def __getattr__(self, nameOfField):",
        "        return self._dictData.f(nameOfField)",
        "    def __getitem__(self, idx):",
        "        return self._dictData[idx]",
        "    def keys(self):",
        "        return self._dictData.keySet()",
        "    def keysN(self):",
        "        return self._dictData.internal.keySet()",
        "    def f(self, name):",
        "        return self._dictData.f(name)",
        "",
        "class MySQLrecords:",
        "    def __init__(self, sqlData):",
        "        self._results = sqlData",
        "    def __getitem__(self, idx):",
        "        return MyRecord(self._results[idx])",
        "    def len(self):",
        "        return self._results.size()",
        "",
        "def sqlm(query, num=200000, panicIfLess=False, panicIfMore=False):",
        "    return MySQLrecords("+SQLHOLDER_BINDING+".sqlm(query, num, panicIfLess, panicIfMore, True))",
        "",
        "def sqls(query):",
        "    return MyRecord("+SQLHOLDER_BINDING+".sqls(query, True), True)",
        "def AnyToString(foo):",
        "    if foo is None:",
        "        return None",
        "    else:",
        "        return unicode(foo)",    //, 'iso8859-7')",
        "",
        "def sql_exec(stmtStr):",
        "    return "+SQLHOLDER_BINDING+".sqlExec(stmtStr)",
        "",
        "def accum(d, k, v):",
        "    if k not in d.keys():",  // 'if k not in d' is equally correct (by default implies 'd.keys()')
        "        d[k]=0",             // an even more idiomatic way is: 'd[k] = d.get(k, 0) + v'
        "    d[k]+=v",                // to use a default value as an lvalue and enable the 'a[i]+=b' syntax one has to use 'from collections import defaultdict'
        ""
        };


 }