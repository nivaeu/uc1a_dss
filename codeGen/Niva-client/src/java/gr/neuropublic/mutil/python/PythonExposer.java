package gr.neuropublic.mutil.python;

import gr.neuropublic.mutil.base.Triad;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import java.util.SortedSet;
import java.util.TreeSet;
import java.math.BigDecimal;
import java.math.BigInteger;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.StringEscapeUtils;

import gr.neuropublic.mutil.base.ExceptionAdapter;
import gr.neuropublic.mutil.base.Holder;

import org.python.core.PyCode;
import org.python.core.PyException;
import org.python.core.PyObject;
import org.python.util.PythonInterpreter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class PythonExposer {

    private static final Logger l = LoggerFactory.getLogger(PythonExposer.class);


    private static String paramsStr(Map<String, Object> params) {
        if (params == null) return "\n";
        else {
            StringBuilder strBldr = new StringBuilder();
            for (String param : params.keySet()) {
                Object value = params.get(param);
                if (value == null)
                    strBldr.append(String.format("%s = None\n", param));
                else {
                    if (value.getClass().equals(String.class))
                        strBldr.append(String.format("%s = '%s'\n", param, ((String)value).replace("'", "\\'")));
                    //                     else if (value.getClass().equals(java.util.Date.class)) // see email from Christos Kareliotis 14.III.2013 apparently according to George Mamais Hibernate returns java.sql.Date objects even when asked for java.util.Date - NB: this breaks the contract established so far for the python exposer to handle only java.util.Date
                    else if (value instanceof java.util.Date)
                        strBldr.append(String.format("%s = datetime.fromtimestamp(%d).date()\n", param, ((java.util.Date) value).getTime()/1000));
                    else if (value.getClass().equals(Boolean.class))
                        strBldr.append(String.format("%s = %s\n", param, ((Boolean) value)?"True":"False"));
                    else
                        strBldr.append(String.format("%s = %s\n", param, value));
                }
            }
            String retValue = strBldr.toString();
            l.info("about to return: "+retValue);
            return retValue;
        }
    }


    private static String escapeUTFForPythonScript(String script, boolean doubleSlashEscape) {
        String escapedScript                  = StringEscapeUtils.escapeJava(script);
        String escapedScriptNewLinesCorrected = escapedScript.replaceAll("\\\\n", "\n");
        String escapedScriptQuotesCorrected   = escapedScriptNewLinesCorrected.replaceAll("\\\\'", "\'");
               escapedScriptQuotesCorrected   = escapedScriptQuotesCorrected.replaceAll("\\\\\"", "\"");
        if (doubleSlashEscape)
               escapedScriptQuotesCorrected   = escapedScriptQuotesCorrected.replaceAll("\\\\\\\\", "\\\\");
        return escapedScriptQuotesCorrected;
    }

    private static void unescapeStringParameters(Map<String, Object> params) {
        if (params != null)
            for (String key : params.keySet())
                if (key.startsWith("STR_")) {
                    Object o = params.get(key);
                    if (o == null) {
                        System.out.println(String.format("warning: key %s is null (is that intentional?)", key));
                        params.put(key, null);
                    } else {
                        if (! (o instanceof String))
                            throw new RuntimeException(String.format("parameter: '%s' is not a String but a %s", key, o.getClass())); // is a %s and not a String", key, o.getClass()));
                        else
                            params.put(key, StringEscapeUtils.unescapeJava((String)params.get(key)));
                    }
                }
    }


    public static Triad<Map<String, Object>, String, PyException> exposeParams(Map<String, Object> bindings,   String scriptFuncsAndPreamble,
                                                                               Map<String, Object> paramsInit, String script)  {
        return exposeParams(bindings,  scriptFuncsAndPreamble, paramsInit, script, true, false); // true is the default behaviour currently used in web reports
    }

    public static Triad<Map<String, Object>, String, PyException> exposeParams(Map<String, Object> bindings,   String scriptFuncsAndPreamble,
                                                                               Map<String, Object> paramsInit, String script, boolean escape, boolean doubleSlashEscape)  {
        final String PARAMS_BINDING  = "params";
        final String JUTILS_BINDING  = "u";
        script = paramsStr(paramsInit)+"\n"+script;    
        String scriptUTFReady = null;
        if (escape) {
            scriptUTFReady = escapeUTFForPythonScript(script, doubleSlashEscape);
            script = (scriptFuncsAndPreamble==null?"":scriptFuncsAndPreamble+"\n")+scriptUTFReady  +"\n" + String.format(StringUtils.join(PYTHON_FOOTER, "\n"), "%s", "%.20f", PARAMS_BINDING);
        } else {  // this branch breaks with a message like "encoding in UTF8 string"
            scriptUTFReady = script;
            String encodingDeclaration = "# -*- coding: utf-8 -*-"+"\n";
            script = (scriptFuncsAndPreamble==null?"":scriptFuncsAndPreamble+"\n")+scriptUTFReady  +"\n" + String.format(StringUtils.join(PYTHON_FOOTER, "\n"), "%s", "%.20f", PARAMS_BINDING);
        }
        Map<String, Object> params = new HashMap<String, Object>();
        PyException pyExc = null;
        {
            PythonInterpreter pi = new PythonInterpreter();
            if (bindings!=null)
                for (String binding: bindings.keySet()) {
                    if (binding.equals(PARAMS_BINDING) || binding.equals(JUTILS_BINDING))
                        throw new RuntimeException(String.format("duplicate binding for params", binding));
                    pi.set(binding, bindings.get(binding));
                }
            pi.set(PARAMS_BINDING, params);
            pi.set(JUTILS_BINDING, new JythonUtils(l));
            PyObject result = null;
            try {
                PyCode code = pi.compile(script);
                result = pi.eval(code);
                l.info("evalution result = "+result);
            } catch (PyException e) {
                l.error("exception: ", e);
                pyExc = e;
            }
        }
        if (escape)
            unescapeStringParameters(params);

        // If a BD_ parameter is passed into Python, it will return back as either an integer or a double.
        // This will crash Jasper.
        //
        // We therefore create a new map... where BD_ values are cast into java.math.BigDecimal
        Map<String, Object> newmap = new HashMap<String, Object>();
        for(String key : params.keySet()) {   
            if (key.startsWith("BD_")) {
                Object s = params.get(key);
                if (s instanceof Integer) {
                    BigDecimal bd = new BigDecimal ((Integer)s);
                    newmap.put(key, bd);
                } else if (s instanceof Double) {
                    BigDecimal bd = new BigDecimal ((Double)s);
                    newmap.put(key, bd);
                } else if (s instanceof BigInteger) {
                    BigDecimal bd = new BigDecimal ((BigInteger)s);
                    newmap.put(key, bd);
                } else {
                    newmap.put(key, s);
                }
            } else {
                newmap.put(key, params.get(key));
            }
        }

        return Triad.create(newmap, script, pyExc);
    }


    private static final String PYTHON_FOOTER[] = {
        "",
        "for (k,v) in globals().items():",
        "    if (type(v)==float):",
        "       u.printf(\"******************** %s = %s\", k, v)",
        "    %s.put(k, v)"
        };

}
