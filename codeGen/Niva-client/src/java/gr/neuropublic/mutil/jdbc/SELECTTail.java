package gr.neuropublic.mutil.jdbc;
import java.util.List; import java.util.ArrayList; import java.util.Arrays;
import org.apache.commons.lang3.StringUtils;
import gr.neuropublic.mutil.base.ListUtil;
import gr.neuropublic.mutil.base.Pair;
import gr.neuropublic.mutil.base.ITransformer_1_to_1;



public class SELECTTail {

    private List<Pair<String, OrderByDirection>> orderBy;
    private Integer offset;
    private Integer limit;

    public SELECTTail(List<Pair<String, OrderByDirection>> orderBy, Integer offset, Integer limit) {
        this.orderBy = orderBy;
        if (((offset==null)&(limit!=null))|((offset!=null)&(limit==null))) throw new RuntimeException();
        if (offset!=null) {
            if (offset < 0) throw new RuntimeException();
            if (limit < 0) throw new RuntimeException();
        }
        this.offset = offset;
        this.limit = limit;
    }

    public SELECTTail(List<Pair<String, OrderByDirection>> orderBy) {
        this(orderBy, null, null);
    }

    public SELECTTail(Integer offset, Integer limit, List<String> orderByColumns) {
        this(ListUtil.transform(orderByColumns, new ITransformer_1_to_1<String, Pair<String, OrderByDirection>>() {
                @Override
                public Pair<String, OrderByDirection> transform(String column) {
                    return Pair.create(column, OrderByDirection.DEFAULT());
                }
                }), offset, limit);
    }

    public static SELECTTail fromColumnList (List<String> orderByColumns) {
        return new SELECTTail( (Integer) null, (Integer) null, orderByColumns);
    }
    
    public static SELECTTail fromColumnList(String ... columns) {
        return new SELECTTail((Integer) null, (Integer) null, Arrays.asList(columns));
    }

    public static SELECTTail fromColumnList(int offset, int limit, String ... columns) {
        return new SELECTTail (offset, limit, Arrays.asList(columns));
    }

    public static SELECTTail nil() {
        return new SELECTTail(new ArrayList<Pair<String, OrderByDirection>>(), null, null);
    }

    public String postgresql() {
        String header = null;
        if (!orderBy.isEmpty()) {
            List<String> columnsOrderingList = ListUtil.transform(orderBy, new ITransformer_1_to_1<Pair<String, OrderByDirection>, String>() {
                    @Override
                    public String transform (Pair<String, OrderByDirection> column) {
                        return column.a+" "+column.b;
                    }
                });
            header = String.format("ORDER BY %s ", StringUtils.join(columnsOrderingList, ","));
        }
        String trailer = null;
        if (offset!=null)
            trailer = String.format("OFFSET %d LIMIT %d", offset, limit);
        List<String> parts = new ArrayList<String>();
        if (header  != null) parts.add(header);
        if (trailer != null) parts.add(trailer);
        return StringUtils.join( parts, " ");
    }
}