package gr.neuropublic.mutil.jdbc;

public enum OrderByDirection {
    ASC, DESC;

    public static OrderByDirection DEFAULT() {
        return ASC;
    }
}

