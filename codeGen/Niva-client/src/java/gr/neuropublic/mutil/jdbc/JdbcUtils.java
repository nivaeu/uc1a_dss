package gr.neuropublic.mutil.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


// Contains utility methods to close the given JDBC Connection, Statements or
// ResultSet and ignore any thrown exception.
// This is useful for typical finally blocks in manual JDBC code.


public class JdbcUtils {

    private static final Logger log = LoggerFactory.getLogger(JdbcUtils.class);

    // Private constructor to prevent instantiation.
    private JdbcUtils() {
    }

    public static Integer rs_getInt(ResultSet rs, int n) throws SQLException {
        Integer retValue = rs.getInt(n);
        if (rs.wasNull())
            return null;
        else
            return retValue;
    }

    @Deprecated // use ps_setInt instead
    public static void ps_setInteger(PreparedStatement ps, int idx, Integer value) throws SQLException {
        if (value!=null)
            ps.setInt(idx, value);
        else
            ps.setNull(idx, java.sql.Types.INTEGER);
    }

    public static void ps_setInt(PreparedStatement ps, int idx, Integer value) throws SQLException {
        if (value!=null)
            ps.setInt(idx, value);
        else
            ps.setNull(idx, java.sql.Types.INTEGER);
    }

    public static void ps_setBoolean(PreparedStatement ps, int idx, Boolean value) throws SQLException {
        if (value!=null)
            ps.setBoolean(idx, value);
        else
            ps.setNull(idx, java.sql.Types.BOOLEAN);
    }

    public static void closeConnection(Connection connection) {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException ex) {
                if (log.isDebugEnabled()) {
                    log.debug("Could not close JDBC Connection", ex);
                }


















            } catch (Throwable ex) {
                if (log.isDebugEnabled()) {
                    log.debug("Unexpected exception on closing JDBC Connection", ex);
                }
            }
        }
    }

    public static void closeStatement(Statement statement) {
        if (statement != null) {
            try {
                statement.close();
            } catch (SQLException ex) {
                if (log.isDebugEnabled()) {
                    log.debug("Could not close JDBC Statement", ex);
                }
            } catch (Throwable ex) {
                if (log.isDebugEnabled()) {
                    log.debug("Unexpected exception on closing JDBC Statement", ex);
                }
            }
        }
    }

    public static void closeResultSet(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                if (log.isDebugEnabled()) {
                    log.debug("Could not close JDBC ResultSet", ex);
                }
            } catch (Throwable ex) {
                if (log.isDebugEnabled()) {
                    log.debug("Unexpected exception on closing JDBC ResultSet", ex);
                }
            }
        }
    }
}
