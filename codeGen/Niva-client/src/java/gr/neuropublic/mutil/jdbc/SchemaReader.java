package gr.neuropublic.mutil.jdbc;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.util.List;
import java.util.Arrays;
import java.util.Properties;

import org.apache.ddlutils.Platform;
import org.apache.ddlutils.PlatformFactory;
import org.apache.ddlutils.model.Database;
import org.apache.ddlutils.model.Table;
import org.apache.ddlutils.model.Column;

import gr.neuropublic.mutil.base.ITransformer_1_to_1;
import gr.neuropublic.mutil.base.ListUtil;
import gr.neuropublic.mutil.base.Quad;
import gr.neuropublic.mutil.base.ExceptionAdapter;
import static gr.neuropublic.mutil.base.Util.panicIf;

public class SchemaReader {

    final String dbName;
    final private Database database;
    final private Connection connection;
    public SchemaReader(String jdbcDriver, String jdbcConnectionURL, String dbName, String user, String pass) {
        this.dbName = dbName;
        Platform platform = PlatformFactory.createNewPlatformInstance(jdbcDriver, jdbcConnectionURL);
        try {
            Class.forName(jdbcDriver);
            Properties props = new Properties();
            props.setProperty("user", user);
            props.setProperty("password", pass);
            this.connection = DriverManager.getConnection(jdbcConnectionURL, props);
            this.database = platform.readModelFromDatabase(this.connection, this.dbName);
        } catch (Exception e) {
            throw new ExceptionAdapter(e);
        }
    }


    public List<String> getTables() {
        return ListUtil.transform(Arrays.asList(database.getTables()),
            new ITransformer_1_to_1<Table, String>() {
                 @Override
                 public String transform(Table table) {
                     return table.getName();
                 }
                                  });
    }

    public List<Quad<Boolean, String, String, String>> getColumns(final String tableName) {
        Table table = database.findTable(tableName);
        final String dbName = this.dbName;
        return ListUtil.transform(Arrays.asList(table.getColumns()),
                                  new ITransformer_1_to_1<Column, Quad<Boolean, String, String, String>>() {
                                    @Override
                                        public Quad<Boolean, String, String, String> transform(Column column) {
                                        return Quad.create(column.isPrimaryKey(),
                                                           getPostgreSQLSequenceForColumn(dbName, tableName, column.getName()),
                                                           column.getType(),
                                                           column.getName());
                                    }
                                  });
    }

    public List<String> getPointedTableS(String table) {
        throw new UnsupportedOperationException();
    }

    public String getPostgreSQLSequenceForColumn(String database, String table, String column) {
        try {
            Statement stmt = connection.createStatement();
            String sql="select column_default from information_schema.columns where "+
                       "table_catalog='"+database+"' and "                           +
                       "table_schema='public' and "                                  +
                       "table_name='"+table+"' and "                                 +
                       "column_name='"+column+"'";
            ResultSet rs = stmt.executeQuery(sql);
            rs.next();
            String sequenceName = rs.getString(1);
            panicIf(rs.next());
            return sequenceName;
        } catch (Exception e) {
            throw new ExceptionAdapter(e);
        }
    }

    public static void main(String args[]) {
        String jdbcDriver=args[0];
        String jdbcConnectionUrl = args[1];
        String dbName = args[2];
        String user = args[3];
        String passwd = args[4];
        System.out.println("\n\ndriver is: "+jdbcDriver+", connection is: "+jdbcConnectionUrl+", database name is: "+dbName);
        System.out.println("user is: "+user+", passwd is: "+passwd+"\n\n");
        SchemaReader sr = new SchemaReader(jdbcDriver, jdbcConnectionUrl, dbName, user, passwd);
        List<String> tables = sr.getTables();
        for (String table: tables) {
            System.out.println("****** TABLE: "+table+" *******");
            for (Quad<Boolean, String, String, String> column : sr.getColumns(table)) {
                System.out.println("    column: "+column.d+" of type: "+column.c+" "+(column.a?"PK("+column.b+")":""));
            }
        }
    }

}