package gr.neuropublic.mutil.jpapersutil;

import gr.neuropublic.mutil.base.Pair;

public class WhereComponent extends NonValueBearingJPQLComponent {
    
    @Override
    public String queryCompNoParameter() {
        return " WHERE";
    }

}