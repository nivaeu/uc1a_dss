package gr.neuropublic.mutil.jpapersutil;

import gr.neuropublic.mutil.base.Pair;

public class AndComponent extends NonValueBearingJPQLComponent {
    @Override
    public String queryCompNoParameter() {
        return " and";
    }
}