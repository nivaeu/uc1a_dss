package gr.neuropublic.mutil.jpapersutil;

import gr.neuropublic.mutil.base.Pair;

public interface IJPQLQueryComponent {
    public String queryCompNoParameter();
    public Pair<String, Pair<String, Object>> queryCompAndParameter();
 }