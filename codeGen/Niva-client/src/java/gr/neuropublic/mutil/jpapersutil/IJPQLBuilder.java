package gr.neuropublic.mutil.jpapersutil;

public interface IJPQLBuilder {
    public IJPQLBuilder where(String fieldName);
}