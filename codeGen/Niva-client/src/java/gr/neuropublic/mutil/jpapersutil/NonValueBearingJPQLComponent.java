package gr.neuropublic.mutil.jpapersutil;

import gr.neuropublic.mutil.base.Pair;

public abstract class NonValueBearingJPQLComponent implements IJPQLQueryComponent {

    @Override
    public Pair<String, Pair<String, Object>> queryCompAndParameter() {
        return Pair.create(queryCompNoParameter(), null);
    }


}