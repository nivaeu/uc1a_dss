package gr.neuropublic.notifications;

/**
 *
 * @author st_krommydas
 */
public abstract class AbstractFcmTarget {
    public String name;
    public String value;

    public AbstractFcmTarget(String name, String value) {
        this.name = name;
        this.value = value;
    }
}
