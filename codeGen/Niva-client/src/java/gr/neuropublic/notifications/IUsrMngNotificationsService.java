package gr.neuropublic.notifications;

import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;

/**
 *
 * @author st_krommydas
 */
public interface IUsrMngNotificationsService {
    @Local
    public interface ILocal extends IUsrMngNotificationsService {}

    @Remote
    public interface IRemote extends IUsrMngNotificationsService {}

    public void saveFcmUserTokenToDb(final Integer userId, final String userName, final String appName, final String token);
    public List<FcmUserToken> getUserTokensByUserEmail(final String userEmail, final String appName);
    public String getUserNameByTin(final String userSubsCode, final String userTin);
}
