package gr.neuropublic.notifications;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 *
 * @author st_krommydas
 */
public class FcmResponse {
    private int code;
    private String message;
    private String responseString;

    public FcmResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public FcmResponse(int code, String message, String responseString) {
        this.code = code;
        this.message = message;
        this.responseString = responseString;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponseString() {
        return responseString;
    }

    public void setResponseString(String responseString) {
        this.responseString = responseString;
    }

   
    public boolean isSuccess() {
        return code == 200;
    }

    public boolean isNetworkError() {
        return !isSuccess() && getJsonResponse()==null;
    }
    
    public boolean isTokenNotFoundError() {
        if (isSuccess())
            return false;
        
        JsonObject jRsp = getJsonResponse();
        if (jRsp == null)
            return false;
        
        if (code == 404) {
            try {
                JsonObject rError = jRsp.getAsJsonObject("error");
                int rCode = rError.getAsJsonPrimitive("code").getAsInt();
                String rStatus = rError.getAsJsonPrimitive("status").getAsString();
                
                if (rCode == 404 && rStatus.equals("NOT_FOUND"))
                    return true;
                
            } catch(Exception e) {
                return false;
            }
        
        }
        
        return false;
    }

    public JsonObject getErrorAsJsonObject() {
        if (isSuccess())
            return null;
        
        JsonObject jRet = new JsonObject();
        
        JsonObject jRsp = getJsonResponse();
        if (jRsp == null) {
            jRet.addProperty("code", code);
            jRet.addProperty("message", message);
            
            return jRet;
        }
        
        try {
            JsonObject rError = jRsp.getAsJsonObject("error");

            int rCode = rError.getAsJsonPrimitive("code").getAsInt();
            jRet.addProperty("code", rCode);

            String rMessage = rError.getAsJsonPrimitive("message").getAsString();
            jRet.addProperty("message", rMessage);

            String rStatus = rError.getAsJsonPrimitive("status").getAsString();
            jRet.addProperty("status", rStatus);


        } catch(Exception e) {
            return jRet;
        }
        
        return jRet;
    }
    
    public JsonObject getJsonResponse() {
        if (this.responseString == null)
            return null;
        
        JsonObject jResp;
        try {
            jResp = new JsonParser().parse(this.responseString).getAsJsonObject();
        } catch(Exception e) {
            return null;
        }

        return jResp;
    }
}
