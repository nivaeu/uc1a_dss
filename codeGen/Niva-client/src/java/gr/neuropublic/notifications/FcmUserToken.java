package gr.neuropublic.notifications;

/**
 *
 * @author st_krommydas
 */
public class FcmUserToken {
    public Integer id;
    public String token;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
