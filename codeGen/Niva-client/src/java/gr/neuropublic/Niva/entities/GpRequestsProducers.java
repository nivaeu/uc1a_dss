//9066C4B4329F3F3C1A91E0C7A0B5B372
package gr.neuropublic.Niva.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import gr.neuropublic.validators.annotations.ValidateMethods;
import gr.neuropublic.validators.annotations.ValidateUniqueKeys;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.validators.annotations.MethodValidator;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
import gr.neuropublic.Niva.entitiesBase.GpRequestsProducersBase;



@Entity
@Table(name = "gp_requests_producers", schema="niva" ,  
    uniqueConstraints={
        @UniqueConstraint(columnNames={"gp_requests_id", "producers_id"})
    })
@ValidateUniqueKeys
@ValidateMethods
//@org.hibernate.annotations.DynamicUpdate()
@org.hibernate.annotations.Entity(
		dynamicUpdate = true
)
public class GpRequestsProducers  extends GpRequestsProducersBase {

    public GpRequestsProducers() {
        super();
    }
    public GpRequestsProducers(Integer gpRequestsProducersId) {
        super(gpRequestsProducersId);
    }
    public GpRequestsProducers(Integer gpRequestsProducersId, Integer rowVersion, String notes) {
        super(gpRequestsProducersId, rowVersion, notes);
    }
    public GpRequestsProducers(Integer rowVersion, String notes, GpRequests gpRequestsId, Producers producersId) {
        super(rowVersion, notes, gpRequestsId, producersId);
    }

    // Add your custom methods here
    // This file will not be regenarated, so it is safe to edit it.
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
