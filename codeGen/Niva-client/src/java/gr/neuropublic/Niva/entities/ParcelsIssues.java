//AF48B6987EAAC7AAD610A16B688017DC
package gr.neuropublic.Niva.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import gr.neuropublic.validators.annotations.ValidateMethods;
import gr.neuropublic.validators.annotations.ValidateUniqueKeys;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.validators.annotations.MethodValidator;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
import gr.neuropublic.Niva.entitiesBase.ParcelsIssuesBase;



@Entity
@Table(name = "parcels_issues", schema="niva" )
@ValidateUniqueKeys
@ValidateMethods
//@org.hibernate.annotations.DynamicUpdate()
@org.hibernate.annotations.Entity(
		dynamicUpdate = true
)
public class ParcelsIssues  extends ParcelsIssuesBase {

    public ParcelsIssues() {
        super();
    }
    public ParcelsIssues(Integer parcelsIssuesId) {
        super(parcelsIssuesId);
    }
    public ParcelsIssues(Integer parcelsIssuesId, Date dteCreated, Short status, Date dteStatusUpdate, Integer rowVersion, Short typeOfIssue, Boolean active) {
        super(parcelsIssuesId, dteCreated, status, dteStatusUpdate, rowVersion, typeOfIssue, active);
    }
    public ParcelsIssues(Date dteCreated, Short status, Date dteStatusUpdate, Integer rowVersion, Short typeOfIssue, Boolean active, ParcelClas pclaId) {
        super(dteCreated, status, dteStatusUpdate, rowVersion, typeOfIssue, active, pclaId);
    }

    // Add your custom methods here
    // This file will not be regenarated, so it is safe to edit it.
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
