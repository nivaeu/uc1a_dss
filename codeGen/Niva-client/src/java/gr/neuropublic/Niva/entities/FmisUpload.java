//4A2B349E527424B3ECAAB1488BF6E566
package gr.neuropublic.Niva.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import gr.neuropublic.validators.annotations.ValidateMethods;
import gr.neuropublic.validators.annotations.ValidateUniqueKeys;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.validators.annotations.MethodValidator;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
import gr.neuropublic.Niva.entitiesBase.FmisUploadBase;



@Entity
@Table(name = "fmis_uploads", schema="niva" )
@ValidateUniqueKeys
@ValidateMethods
//@org.hibernate.annotations.DynamicUpdate()
@org.hibernate.annotations.Entity(
		dynamicUpdate = true
)
public class FmisUpload  extends FmisUploadBase {

    public FmisUpload() {
        super();
    }
    public FmisUpload(Integer fmisUploadsId) {
        super(fmisUploadsId);
    }
    public FmisUpload(Integer fmisUploadsId, String metadata, Date dteUpload, byte[] docfile, String usrUpload) {
        super(fmisUploadsId, metadata, dteUpload, docfile, usrUpload);
    }
    public FmisUpload(String metadata, Date dteUpload, byte[] docfile, String usrUpload, ParcelsIssues parcelsIssuesId) {
        super(metadata, dteUpload, docfile, usrUpload, parcelsIssuesId);
    }

    // Add your custom methods here
    // This file will not be regenarated, so it is safe to edit it.
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
