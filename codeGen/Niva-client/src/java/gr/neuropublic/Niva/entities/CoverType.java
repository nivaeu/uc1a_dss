//05131C3941B9C735652E4E4B83D0D9AD
package gr.neuropublic.Niva.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import gr.neuropublic.validators.annotations.ValidateMethods;
import gr.neuropublic.validators.annotations.ValidateUniqueKeys;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.validators.annotations.MethodValidator;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
import gr.neuropublic.Niva.entitiesBase.CoverTypeBase;



@Entity
@Table(name = "cover_type", schema="niva" ,  
    uniqueConstraints={
        @UniqueConstraint(columnNames={"code", "name"}),
        @UniqueConstraint(columnNames={"code"}),
        @UniqueConstraint(columnNames={"name"})
    })
@ValidateUniqueKeys
@ValidateMethods
//@org.hibernate.annotations.DynamicUpdate()
@org.hibernate.annotations.Entity(
		dynamicUpdate = true
)
public class CoverType  extends CoverTypeBase {

    public CoverType() {
        super();
    }
    public CoverType(Integer cotyId) {
        super(cotyId);
    }
    public CoverType(Integer cotyId, Integer code, String name, Integer rowVersion) {
        super(cotyId, code, name, rowVersion);
    }
    public CoverType(Integer code, String name, Integer rowVersion, ExcelFile exfiId) {
        super(code, name, rowVersion, exfiId);
    }

    // Add your custom methods here
    // This file will not be regenarated, so it is safe to edit it.
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
