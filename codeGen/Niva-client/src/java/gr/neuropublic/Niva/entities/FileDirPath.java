//6A0801F6A1C6A6867D26C6CED3F0114C
package gr.neuropublic.Niva.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import gr.neuropublic.validators.annotations.ValidateMethods;
import gr.neuropublic.validators.annotations.ValidateUniqueKeys;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.validators.annotations.MethodValidator;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
import gr.neuropublic.Niva.entitiesBase.FileDirPathBase;



@Entity
@Table(name = "file_dir_path", schema="niva" ,  
    uniqueConstraints={
        @UniqueConstraint(columnNames={"file_type"})
    })
@ValidateUniqueKeys
@ValidateMethods
//@org.hibernate.annotations.DynamicUpdate()
@org.hibernate.annotations.Entity(
		dynamicUpdate = true
)
public class FileDirPath  extends FileDirPathBase {

    public FileDirPath() {
        super();
    }
    public FileDirPath(Integer fidrId) {
        super(fidrId);
    }
    public FileDirPath(Integer fidrId, String directory, Integer rowVersion, Integer fileType) {
        super(fidrId, directory, rowVersion, fileType);
    }
    public FileDirPath(String directory, Integer rowVersion, Integer fileType) {
        super(directory, rowVersion, fileType);
    }

    // Add your custom methods here
    // This file will not be regenarated, so it is safe to edit it.
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
