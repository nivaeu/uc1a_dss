//29B5F390FDB2C0A1B954ECF2A3054452
package gr.neuropublic.Niva.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import gr.neuropublic.validators.annotations.ValidateMethods;
import gr.neuropublic.validators.annotations.ValidateUniqueKeys;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.validators.annotations.MethodValidator;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
import gr.neuropublic.Niva.entitiesBase.AgrisnapUsersBase;



@Entity
@Table(name = "agrisnap_users", schema="niva" )
@ValidateUniqueKeys
@ValidateMethods
//@org.hibernate.annotations.DynamicUpdate()
@org.hibernate.annotations.Entity(
		dynamicUpdate = true
)
public class AgrisnapUsers  extends AgrisnapUsersBase {

    public AgrisnapUsers() {
        super();
    }
    public AgrisnapUsers(Integer agrisnapUsersId) {
        super(agrisnapUsersId);
    }
    public AgrisnapUsers(Integer agrisnapUsersId, String agrisnapName, Integer rowVersion, String agrisnapUid) {
        super(agrisnapUsersId, agrisnapName, rowVersion, agrisnapUid);
    }
    public AgrisnapUsers(String agrisnapName, Integer rowVersion, String agrisnapUid) {
        super(agrisnapName, rowVersion, agrisnapUid);
    }

    // Add your custom methods here
    // This file will not be regenarated, so it is safe to edit it.
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
