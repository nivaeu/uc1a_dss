//F9ACB34D10507B473022620507A43306
package gr.neuropublic.Niva.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import gr.neuropublic.validators.annotations.ValidateMethods;
import gr.neuropublic.validators.annotations.ValidateUniqueKeys;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.validators.annotations.MethodValidator;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
import gr.neuropublic.Niva.entitiesBase.EcGroupCopyBase;



@Entity
@Table(name = "ec_group_copy", schema="niva" )
@ValidateUniqueKeys
@ValidateMethods
//@org.hibernate.annotations.DynamicUpdate()
@org.hibernate.annotations.Entity(
		dynamicUpdate = true
)
public class EcGroupCopy  extends EcGroupCopyBase {

    public EcGroupCopy() {
        super();
    }
    public EcGroupCopy(Integer ecgcId) {
        super(ecgcId);
    }
    public EcGroupCopy(Integer ecgcId, String description, String name, Integer rowVersion, Integer recordtype, Short cropLevel) {
        super(ecgcId, description, name, rowVersion, recordtype, cropLevel);
    }
    public EcGroupCopy(String description, String name, Integer rowVersion, Integer recordtype, Short cropLevel, DecisionMaking demaId) {
        super(description, name, rowVersion, recordtype, cropLevel, demaId);
    }

    // Add your custom methods here
    // This file will not be regenarated, so it is safe to edit it.
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
