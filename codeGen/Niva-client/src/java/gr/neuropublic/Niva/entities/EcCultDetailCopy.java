//CABD354BA53D3F585D9B6ECABDC25DB9
package gr.neuropublic.Niva.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import gr.neuropublic.validators.annotations.ValidateMethods;
import gr.neuropublic.validators.annotations.ValidateUniqueKeys;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.validators.annotations.MethodValidator;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
import gr.neuropublic.Niva.entitiesBase.EcCultDetailCopyBase;



@Entity
@Table(name = "ec_cult_detail_copy", schema="niva" )
@ValidateUniqueKeys
@ValidateMethods
//@org.hibernate.annotations.DynamicUpdate()
@org.hibernate.annotations.Entity(
		dynamicUpdate = true
)
public class EcCultDetailCopy  extends EcCultDetailCopyBase {

    public EcCultDetailCopy() {
        super();
    }
    public EcCultDetailCopy(Integer ecdcId) {
        super(ecdcId);
    }
    public EcCultDetailCopy(Integer ecdcId, Integer orderingNumber, Integer agreesDeclar, Integer comparisonOper, BigDecimal probabThres, Integer decisionLight, Integer rowVersion, Integer agreesDeclar2, Integer comparisonOper2, BigDecimal probabThres2, BigDecimal probabThresSum, Integer comparisonOper3) {
        super(ecdcId, orderingNumber, agreesDeclar, comparisonOper, probabThres, decisionLight, rowVersion, agreesDeclar2, comparisonOper2, probabThres2, probabThresSum, comparisonOper3);
    }
    public EcCultDetailCopy(Integer orderingNumber, Integer agreesDeclar, Integer comparisonOper, BigDecimal probabThres, Integer decisionLight, Integer rowVersion, Integer agreesDeclar2, Integer comparisonOper2, BigDecimal probabThres2, BigDecimal probabThresSum, Integer comparisonOper3, EcCultCopy ecccId) {
        super(orderingNumber, agreesDeclar, comparisonOper, probabThres, decisionLight, rowVersion, agreesDeclar2, comparisonOper2, probabThres2, probabThresSum, comparisonOper3, ecccId);
    }

    // Add your custom methods here
    // This file will not be regenarated, so it is safe to edit it.
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
