//D7EBA42B6C331A27F44D9585002B8593
package gr.neuropublic.Niva.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import gr.neuropublic.validators.annotations.ValidateMethods;
import gr.neuropublic.validators.annotations.ValidateUniqueKeys;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.validators.annotations.MethodValidator;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
import gr.neuropublic.Niva.entitiesBase.ParcelClasBase;

// Extras by MT
import gr.neuropublic.base.JsonHelper;

@Entity
@Table(name = "parcel_class", schema="niva" )
@ValidateUniqueKeys
@ValidateMethods
//@org.hibernate.annotations.DynamicUpdate()
@org.hibernate.annotations.Entity(
		dynamicUpdate = true
)
public class ParcelClas  extends ParcelClasBase {


    public ParcelClas() {
        super();
    }
    public ParcelClas(Integer pclaId) {
        super(pclaId);
    }
    public ParcelClas(Integer pclaId, Double probPred, Double probPred2, Integer prodCode, String parcIdentifier, String parcCode, Geometry geom4326) {
        super(pclaId, probPred, probPred2, prodCode, parcIdentifier, parcCode, geom4326);
    }
    public ParcelClas(Double probPred, Double probPred2, Integer prodCode, String parcIdentifier, String parcCode, Geometry geom4326, Classification clasId, Cultivation cultIdDecl, Cultivation cultIdPred, CoverType cotyIdDecl, CoverType cotyIdPred, Cultivation cultIdPred2) {
        super(probPred, probPred2, prodCode, parcIdentifier, parcCode, geom4326, clasId, cultIdDecl, cultIdPred, cotyIdDecl, cotyIdPred, cultIdPred2);
    }

 
    // Add your custom methods here
    // This file will not be regenarated, so it is safe to edit it.

 

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
