package gr.neuropublic.Niva.jdbcQueriesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import gr.neuropublic.base.NamedParameterPreparedStatement;
import gr.neuropublic.exceptions.GenericApplicationException;
import gr.neuropublic.enums.DataBaseType;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.io.IOException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.charset.Charset;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;


import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
//import org.geotools.geojson.geom.GeometryJSON;
import org.hibernate.spatial.dialect.oracle.SDOGeometryValueExtractor;
import org.hibernate.spatial.dialect.postgis.PGGeometryValueExtractor;
import com.vividsolutions.jts.io.WKBWriter;
import gr.neuropublic.base.GeometryHepler;

public class testInserterBase implements Serializable {
    
    final String sql = "insert into /*+ APPEND_VALUES */ niva.test_parc_tmp  \n "  +
"        (parc_id, cult_desc, cult_id, geom)  \n "  +
"    values  \n "  +
"        (:parc_id, :cult_desc, :cult_id, :geom)  \n "  ;            
    protected String getSql() {return sql;}
    protected NamedParameterPreparedStatement prepStatement = null;
    protected ResultSet rs = null;

    public void prepareStatement(Connection conn) throws SQLException {
        this.conn = conn;
        if (prepStatement != null) {
            close();
        }
        prepStatement = NamedParameterPreparedStatement.createNamedParameterPreparedStatement(conn, getSql());
    }

    public int executeStatement(Integer parc_id, String cult_desc, Integer cult_id, Geometry geom) throws SQLException {
        if (parc_id == null) {
            prepStatement.setNull("parc_id", java.sql.Types.INTEGER);
        } else {
            prepStatement.setInt("parc_id", parc_id);
        }
        prepStatement.setString("cult_desc", cult_desc);
        if (cult_id == null) {
            prepStatement.setNull("cult_id", java.sql.Types.INTEGER);
        } else {
            prepStatement.setInt("cult_id", cult_id);
        }
        prepStatement.setGeometry("geom", geom);
        
        return prepStatement.executeUpdate();
    }

    public void addBatch(Integer parc_id, String cult_desc, Integer cult_id, Geometry geom) throws SQLException {
        if (parc_id == null) {
            prepStatement.setNull("parc_id", java.sql.Types.INTEGER);
        } else {
            prepStatement.setInt("parc_id", parc_id);
        }
        prepStatement.setString("cult_desc", cult_desc);
        if (cult_id == null) {
            prepStatement.setNull("cult_id", java.sql.Types.INTEGER);
        } else {
            prepStatement.setInt("cult_id", cult_id);
        }
        prepStatement.setGeometry("geom", geom);
            
        prepStatement.addBatch();
    }

    public int executeStatement(Input input) throws SQLException {
        
        return executeStatement(input.parc_id, input.cult_desc, input.cult_id, input.geom);
    }

    public void addBatch(Input input) throws SQLException {
            
        addBatch(input.parc_id, input.cult_desc, input.cult_id, input.geom);
    }

    SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");

    public void appendPgRow(Input input) throws IOException {
        if (input.parc_id != null)
            sw.append(input.parc_id.toString());
        else
            sw.append("\\N");

        sw.append('|');

        if (input.cult_desc != null)
            sw.append(input.cult_desc.replace("\\", "\\\\").replace("\r\n", "\n").replace("\r", "\n").replace("\n", "\\n"));
        else
            sw.append("\\N");

        sw.append('|');

        if (input.cult_id != null)
            sw.append(input.cult_id.toString());
        else
            sw.append("\\N");

        sw.append('|');

        if (input.geom != null) {
            String vl = javax.xml.bind.DatatypeConverter.printHexBinary(vw.write(input.geom));
            sw.append(vl);
        } else
            sw.append("\\N");
        sw.append('\n');
    }



        
    public void executeBatch() throws SQLException {
        prepStatement.executeBatch();
    }

    public void appendPgBatch() throws SQLException, IOException {
        CopyManager cm = new CopyManager((BaseConnection) conn);
            
        sw.flush();
            
            
        ByteArrayInputStream bais = new  ByteArrayInputStream(buffer.toByteArray());
        cm.copyIn("COPY niva.test_parc_tmp (parc_id, cult_desc, cult_id, geom)  FROM STDIN WITH DELIMITER AS '|'", bais);         
            
        buffer.reset();
    }





    int srid = 2100;
    public void setSRID(int srid) {
        this.srid = srid;
    }
    public int getSRID() {
       return this.srid;
    }

    ByteArrayOutputStream buffer = new ByteArrayOutputStream();
    OutputStreamWriter sw = new OutputStreamWriter(buffer   , Charset.forName("UTF-8"));
    WKBWriter vw = new WKBWriter(2, true);

    public testInserterBase() {
        buffer = new ByteArrayOutputStream();
        sw = new OutputStreamWriter(buffer   , Charset.forName("UTF-8"));
    }

    public void close() {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                System.err.println(ex.getMessage());
            }
            rs = null;
        }
        if (prepStatement != null) {
            try {
                prepStatement.close();
                
            } catch (SQLException ex) {
                System.err.println(ex.getMessage());
            }
            prepStatement = null;
        }
        try {
            sw.close();
            buffer.close();
        } catch (IOException ex) {
        }

    }

    public void setFetchSize(int size) throws SQLException {
        prepStatement.setFetchSize(size);
    }

    public static class Input {
        public Integer parc_id;
        public String cult_desc;
        public Integer cult_id;
        public Geometry geom;
    }


    protected Connection conn;
    public Connection getConnection(){
        return conn;
    }
    public void setConnection(Connection com) {
        this.conn = conn;
    }
    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
