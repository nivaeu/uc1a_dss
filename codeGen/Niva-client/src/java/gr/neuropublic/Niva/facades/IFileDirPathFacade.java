//22E19C121795499A7F730DE60F43C22D
package gr.neuropublic.Niva.facades;
import gr.neuropublic.Niva.facadesBase.IFileDirPathBaseFacade;
import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IFileDirPathFacade extends IFileDirPathBaseFacade {
    @Local
    public interface ILocal extends IFileDirPathFacade {}

    //@Remote
    //public interface IRemote extends IFileDirPathFacade {}


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
