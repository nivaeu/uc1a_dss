//19CFD2A86E56B64542D1654C92C5D232
package gr.neuropublic.Niva.facades;
import gr.neuropublic.Niva.facadesBase.IGpRequestsContextsBaseFacade;
import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IGpRequestsContextsFacade extends IGpRequestsContextsBaseFacade {
    @Local
    public interface ILocal extends IGpRequestsContextsFacade {}

    //@Remote
    //public interface IRemote extends IGpRequestsContextsFacade {}


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
