//76E17D8EC11ECD5A061E6E853A968803
package gr.neuropublic.Niva.facades;
import gr.neuropublic.Niva.facadesBase.IParcelsIssuesActivitiesBaseFacade;
import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IParcelsIssuesActivitiesFacade extends IParcelsIssuesActivitiesBaseFacade {
    @Local
    public interface ILocal extends IParcelsIssuesActivitiesFacade {}

    //@Remote
    //public interface IRemote extends IParcelsIssuesActivitiesFacade {}


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
