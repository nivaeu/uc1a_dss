//E5636AB8F7B6CC418507D692F5A71445
package gr.neuropublic.Niva.services;

import javax.ejb.Local;
import javax.ejb.Remote;

public interface ISessionsCacheService extends gr.neuropublic.Niva.servicesBase.ISessionsCacheServiceBase {
    @Local
    public interface ILocal extends ISessionsCacheService {}

    @Remote
    public interface IRemote extends ISessionsCacheService {}
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
