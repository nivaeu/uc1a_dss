//CC393CC45D5CDFA11CFBC237F42C8A17
package gr.neuropublic.Niva.services;

import javax.ejb.Local;
import javax.ejb.Remote;

public interface IUserManagementService extends gr.neuropublic.Niva.servicesBase.IUserManagementServiceBase {
    @Local
    public interface ILocal extends IUserManagementService {}

    @Remote
    public interface IRemote extends IUserManagementService {}
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
