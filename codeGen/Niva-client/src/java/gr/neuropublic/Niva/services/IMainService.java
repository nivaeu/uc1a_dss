//6B76540444230F460D06AD9AC042190E
package gr.neuropublic.Niva.services;

import javax.ejb.Local;
import javax.ejb.Remote;

public interface IMainService extends gr.neuropublic.Niva.servicesBase.IMainServiceBase {

    @Local
    public interface ILocal extends IMainService {}

    @Remote
    public interface IRemote extends IMainService {}

}

 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
