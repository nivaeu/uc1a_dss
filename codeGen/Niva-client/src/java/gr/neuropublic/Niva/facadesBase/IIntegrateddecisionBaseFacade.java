package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IIntegrateddecisionBaseFacade extends IFacade<Integrateddecision> {
/*
    @Local
    public interface ILocal extends IIntegrateddecisionBaseFacade {}

    //@Remote
    //public interface IRemote extends IIntegrateddecisionBaseFacade {}
*/
    public Integrateddecision initRow();

    public void getTransientFields(List<Integrateddecision> entities);

    public Integrateddecision createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.IParcelClasFacade.ILocal parcelClasFacade, gr.neuropublic.Niva.facades.IDecisionMakingFacade.ILocal decisionMakingFacade, Set<String> allowedFields);

    public List<Integrateddecision> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public Integrateddecision findByPclaId_DemaId(ParcelClas pclaId, DecisionMaking demaId);

    public Integrateddecision findByPclaId_DemaId(Integer pclaId, Integer demaId);

    public int delByPclaId_DemaId(ParcelClas pclaId, DecisionMaking demaId);

    public int delByPclaId_DemaId(Integer pclaId, Integer demaId);

    public Integrateddecision findByIntegrateddecisionsId(Integer integrateddecisionsId);

    public int delByIntegrateddecisionsId(Integer integrateddecisionsId);

    public List<Integrateddecision> findLazyIntegrateddecision(Short decisionCode, Date dteUpdate, String usrUpdate, Integer pclaId_pclaId, Integer demaId_demaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<Integrateddecision> findLazyIntegrateddecision(boolean noTransient, Short decisionCode, Date dteUpdate, String usrUpdate, Integer pclaId_pclaId, Integer demaId_demaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findLazyIntegrateddecision_count(Short decisionCode, Date dteUpdate, String usrUpdate, Integer pclaId_pclaId, Integer demaId_demaId, List<String> excludedEntities);


    public List<Integrateddecision> findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision(Integer demaId_demaId, Short fsch_decisionCode, Integer fsch_prodCode, String fsch_Code, String fsch_Parcel_Identifier, Integer fsch_CultDeclCode_cultId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<Integrateddecision> findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision(boolean noTransient, Integer demaId_demaId, Short fsch_decisionCode, Integer fsch_prodCode, String fsch_Code, String fsch_Parcel_Identifier, Integer fsch_CultDeclCode_cultId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_DecisionMakingROGrpIntegrateddecision_count(Integer demaId_demaId, Short fsch_decisionCode, Integer fsch_prodCode, String fsch_Code, String fsch_Parcel_Identifier, Integer fsch_CultDeclCode_cultId, List<String> excludedEntities);


    public List<Integrateddecision> findAllByCriteriaRange_DashboardGrpIntegrateddecision(Integer pclaId_pclaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<Integrateddecision> findAllByCriteriaRange_DashboardGrpIntegrateddecision(boolean noTransient, Integer pclaId_pclaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_DashboardGrpIntegrateddecision_count(Integer pclaId_pclaId, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
