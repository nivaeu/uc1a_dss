package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface ICoverTypeBaseFacade extends IFacade<CoverType> {
/*
    @Local
    public interface ILocal extends ICoverTypeBaseFacade {}

    //@Remote
    //public interface IRemote extends ICoverTypeBaseFacade {}
*/
    public CoverType initRow();

    public void getTransientFields(List<CoverType> entities);

    public CoverType createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.IExcelFileFacade.ILocal excelFileFacade, Set<String> allowedFields);

    public List<CoverType> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public CoverType findByCode_Name(Integer code, String name);

    public int delByCode_Name(Integer code, String name);

    public CoverType findByCode(Integer code);

    public int delByCode(Integer code);

    public CoverType findByName(String name);

    public int delByName(String name);

    public CoverType findByCotyId(Integer cotyId);

    public int delByCotyId(Integer cotyId);

    public List<CoverType> findAllByCriteriaRange_forLov(Integer fsch_CoverTypeLov_code, String fsch_CoverTypeLov_name, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<CoverType> findAllByCriteriaRange_forLov(boolean noTransient, Integer fsch_CoverTypeLov_code, String fsch_CoverTypeLov_name, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_forLov_count(Integer fsch_CoverTypeLov_code, String fsch_CoverTypeLov_name, List<String> excludedEntities);


    public List<CoverType> findAllByCriteriaRange_CoverTypeGrpCoverType(Integer fsch_code, String fsch_name, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<CoverType> findAllByCriteriaRange_CoverTypeGrpCoverType(boolean noTransient, Integer fsch_code, String fsch_name, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_CoverTypeGrpCoverType_count(Integer fsch_code, String fsch_name, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
