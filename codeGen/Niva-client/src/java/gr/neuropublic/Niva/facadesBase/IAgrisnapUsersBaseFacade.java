package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IAgrisnapUsersBaseFacade extends IFacade<AgrisnapUsers> {
/*
    @Local
    public interface ILocal extends IAgrisnapUsersBaseFacade {}

    //@Remote
    //public interface IRemote extends IAgrisnapUsersBaseFacade {}
*/
    public AgrisnapUsers initRow();

    public void getTransientFields(List<AgrisnapUsers> entities);

    public AgrisnapUsers createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, Set<String> allowedFields);

    public List<AgrisnapUsers> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public AgrisnapUsers findByAgrisnapUsersId(Integer agrisnapUsersId);

    public int delByAgrisnapUsersId(Integer agrisnapUsersId);

    public List<AgrisnapUsers> findLazyAgrisnapUsers(String agrisnapName, String agrisnapUid, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<AgrisnapUsers> findLazyAgrisnapUsers(boolean noTransient, String agrisnapName, String agrisnapUid, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findLazyAgrisnapUsers_count(String agrisnapName, String agrisnapUid, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
