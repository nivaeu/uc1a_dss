package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IDecisionMakingBaseFacade extends IFacade<DecisionMaking> {
/*
    @Local
    public interface ILocal extends IDecisionMakingBaseFacade {}

    //@Remote
    //public interface IRemote extends IDecisionMakingBaseFacade {}
*/
    public DecisionMaking initRow();

    public void getTransientFields(List<DecisionMaking> entities);

    public DecisionMaking createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.IClassificationFacade.ILocal classificationFacade, gr.neuropublic.Niva.facades.IEcGroupFacade.ILocal ecGroupFacade, Set<String> allowedFields);

    public List<DecisionMaking> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public DecisionMaking findByDemaId(Integer demaId);

    public int delByDemaId(Integer demaId);

    public List<DecisionMaking> findLazyDecisionMaking(String description, Date dateTime, Integer clasId_clasId, Integer ecgrId_ecgrId, Boolean hasBeenRun, Boolean hasPopulatedIntegratedDecisionsAndIssues, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<DecisionMaking> findLazyDecisionMaking(boolean noTransient, String description, Date dateTime, Integer clasId_clasId, Integer ecgrId_ecgrId, Boolean hasBeenRun, Boolean hasPopulatedIntegratedDecisionsAndIssues, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findLazyDecisionMaking_count(String description, Date dateTime, Integer clasId_clasId, Integer ecgrId_ecgrId, Boolean hasBeenRun, Boolean hasPopulatedIntegratedDecisionsAndIssues, List<String> excludedEntities);


    public List<DecisionMaking> findLazyDecisionMakingRO(String demaId_description, Date dateTime, Integer clasId_clasId, Integer ecgrId_ecgrId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<DecisionMaking> findLazyDecisionMakingRO(boolean noTransient, String demaId_description, Date dateTime, Integer clasId_clasId, Integer ecgrId_ecgrId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findLazyDecisionMakingRO_count(String demaId_description, Date dateTime, Integer clasId_clasId, Integer ecgrId_ecgrId, List<String> excludedEntities);


    public List<DecisionMaking> findAllByCriteriaRange_forLov(String fsch_DecisionMakingLov_description, Date fsch_DecisionMakingLov_dateTime, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<DecisionMaking> findAllByCriteriaRange_forLov(boolean noTransient, String fsch_DecisionMakingLov_description, Date fsch_DecisionMakingLov_dateTime, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_forLov_count(String fsch_DecisionMakingLov_description, Date fsch_DecisionMakingLov_dateTime, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
