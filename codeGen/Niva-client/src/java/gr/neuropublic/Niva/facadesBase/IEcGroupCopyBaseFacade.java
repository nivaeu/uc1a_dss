package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IEcGroupCopyBaseFacade extends IFacade<EcGroupCopy> {
/*
    @Local
    public interface ILocal extends IEcGroupCopyBaseFacade {}

    //@Remote
    //public interface IRemote extends IEcGroupCopyBaseFacade {}
*/
    public EcGroupCopy initRow();

    public void getTransientFields(List<EcGroupCopy> entities);

    public EcGroupCopy createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.IDecisionMakingFacade.ILocal decisionMakingFacade, Set<String> allowedFields);

    public List<EcGroupCopy> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public EcGroupCopy findByEcgcId(Integer ecgcId);

    public int delByEcgcId(Integer ecgcId);

    public List<EcGroupCopy> findAllByCriteriaRange_DecisionMakingGrpEcGroupCopy(Integer demaId_demaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<EcGroupCopy> findAllByCriteriaRange_DecisionMakingGrpEcGroupCopy(boolean noTransient, Integer demaId_demaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_DecisionMakingGrpEcGroupCopy_count(Integer demaId_demaId, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
