package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IParcelsIssuesBaseFacade extends IFacade<ParcelsIssues> {
/*
    @Local
    public interface ILocal extends IParcelsIssuesBaseFacade {}

    //@Remote
    //public interface IRemote extends IParcelsIssuesBaseFacade {}
*/
    public ParcelsIssues initRow();

    public void getTransientFields(List<ParcelsIssues> entities);

    public ParcelsIssues createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.IParcelClasFacade.ILocal parcelClasFacade, Set<String> allowedFields);

    public List<ParcelsIssues> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public ParcelsIssues findByParcelsIssuesId(Integer parcelsIssuesId);

    public int delByParcelsIssuesId(Integer parcelsIssuesId);

    public List<ParcelsIssues> findLazyParcelsIssues(Date dteCreated, Short status, Date dteStatusUpdate, Integer pclaId_pclaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<ParcelsIssues> findLazyParcelsIssues(boolean noTransient, Date dteCreated, Short status, Date dteStatusUpdate, Integer pclaId_pclaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findLazyParcelsIssues_count(Date dteCreated, Short status, Date dteStatusUpdate, Integer pclaId_pclaId, List<String> excludedEntities);


    public List<ParcelsIssues> findAllByCriteriaRange_ParcelGPGrpParcelsIssues(Integer pclaId_pclaId, Date fsch_dteCreated, Short fsch_status, Date fsch_dteStatusUpdate, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<ParcelsIssues> findAllByCriteriaRange_ParcelGPGrpParcelsIssues(boolean noTransient, Integer pclaId_pclaId, Date fsch_dteCreated, Short fsch_status, Date fsch_dteStatusUpdate, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_ParcelGPGrpParcelsIssues_count(Integer pclaId_pclaId, Date fsch_dteCreated, Short fsch_status, Date fsch_dteStatusUpdate, List<String> excludedEntities);


    public List<ParcelsIssues> findAllByCriteriaRange_DashboardGrpParcelsIssues(Integer pclaId_pclaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<ParcelsIssues> findAllByCriteriaRange_DashboardGrpParcelsIssues(boolean noTransient, Integer pclaId_pclaId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_DashboardGrpParcelsIssues_count(Integer pclaId_pclaId, List<String> excludedEntities);


    public List<ParcelsIssues> findAllByCriteriaRange_ParcelFMISGrpParcelsIssues(Integer pclaId_pclaId, Date fsch_dteCreated, Short fsch_status, Date fsch_dteStatusUpdate, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<ParcelsIssues> findAllByCriteriaRange_ParcelFMISGrpParcelsIssues(boolean noTransient, Integer pclaId_pclaId, Date fsch_dteCreated, Short fsch_status, Date fsch_dteStatusUpdate, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_ParcelFMISGrpParcelsIssues_count(Integer pclaId_pclaId, Date fsch_dteCreated, Short fsch_status, Date fsch_dteStatusUpdate, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
