package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IClassificationBaseFacade extends IFacade<Classification> {
/*
    @Local
    public interface ILocal extends IClassificationBaseFacade {}

    //@Remote
    //public interface IRemote extends IClassificationBaseFacade {}
*/
    public Classification initRow();

    public void getTransientFields(List<Classification> entities);

    public Classification createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.IClassifierFacade.ILocal classifierFacade, gr.neuropublic.Niva.facades.IFileTemplateFacade.ILocal fileTemplateFacade, Set<String> allowedFields);

    public List<Classification> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public Classification findByClasId(Integer clasId);

    public int delByClasId(Integer clasId);

    public List<Classification> findLazyClassification(String name, Date dateTime, Integer clfrId_clfrId, Short year, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<Classification> findLazyClassification(boolean noTransient, String name, Date dateTime, Integer clfrId_clfrId, Short year, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findLazyClassification_count(String name, Date dateTime, Integer clfrId_clfrId, Short year, List<String> excludedEntities);


    public List<Classification> findAllByCriteriaRange_forLov(String fsch_ClassificationLov_name, String fsch_ClassificationLov_description, Date fsch_ClassificationLov_dateTime, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<Classification> findAllByCriteriaRange_forLov(boolean noTransient, String fsch_ClassificationLov_name, String fsch_ClassificationLov_description, Date fsch_ClassificationLov_dateTime, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_forLov_count(String fsch_ClassificationLov_name, String fsch_ClassificationLov_description, Date fsch_ClassificationLov_dateTime, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
