package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface ICultivationBaseFacade extends IFacade<Cultivation> {
/*
    @Local
    public interface ILocal extends ICultivationBaseFacade {}

    //@Remote
    //public interface IRemote extends ICultivationBaseFacade {}
*/
    public Cultivation initRow();

    public void getTransientFields(List<Cultivation> entities);

    public Cultivation createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.ICoverTypeFacade.ILocal coverTypeFacade, gr.neuropublic.Niva.facades.IExcelFileFacade.ILocal excelFileFacade, Set<String> allowedFields);

    public List<Cultivation> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public Cultivation findByCode(Integer code);

    public int delByCode(Integer code);

    public Cultivation findByName(String name);

    public int delByName(String name);

    public Cultivation findByCultId(Integer cultId);

    public int delByCultId(Integer cultId);

    public List<Cultivation> findAllByCriteriaRange_forLov(String fsch_CultivationLov_name, Integer fsch_CultivationLov_code, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<Cultivation> findAllByCriteriaRange_forLov(boolean noTransient, String fsch_CultivationLov_name, Integer fsch_CultivationLov_code, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_forLov_count(String fsch_CultivationLov_name, Integer fsch_CultivationLov_code, List<String> excludedEntities);


    public List<Cultivation> findAllByCriteriaRange_CultivationGrpCultivation(String fsch_name, Integer fsch_code, Integer fsch_cotyId_cotyId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<Cultivation> findAllByCriteriaRange_CultivationGrpCultivation(boolean noTransient, String fsch_name, Integer fsch_code, Integer fsch_cotyId_cotyId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_CultivationGrpCultivation_count(String fsch_name, Integer fsch_code, Integer fsch_cotyId_cotyId, List<String> excludedEntities);


    public List<Integer> findAllByCriteriaRange_CultivationGrpCultivation_getIds(String fsch_name, Integer fsch_code, Integer fsch_cotyId_cotyId, List<String> excludedEntities);

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
