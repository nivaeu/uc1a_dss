package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IExcelFileBaseFacade extends IFacade<ExcelFile> {
/*
    @Local
    public interface ILocal extends IExcelFileBaseFacade {}

    //@Remote
    //public interface IRemote extends IExcelFileBaseFacade {}
*/
    public ExcelFile initRow();

    public void getTransientFields(List<ExcelFile> entities);

    public ExcelFile createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, Set<String> allowedFields);

    public List<ExcelFile> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public ExcelFile findById(Integer id);

    public int delById(Integer id);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
