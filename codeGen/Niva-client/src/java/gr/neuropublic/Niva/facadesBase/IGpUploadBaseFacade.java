package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IGpUploadBaseFacade extends IFacade<GpUpload> {
/*
    @Local
    public interface ILocal extends IGpUploadBaseFacade {}

    //@Remote
    //public interface IRemote extends IGpUploadBaseFacade {}
*/
    public GpUpload initRow();

    public void getTransientFields(List<GpUpload> entities);

    public GpUpload createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.IGpRequestsContextsFacade.ILocal gpRequestsContextsFacade, Set<String> allowedFields);

    public List<GpUpload> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public GpUpload findByGpUploadsId(Integer gpUploadsId);

    public int delByGpUploadsId(Integer gpUploadsId);

    public List<GpUpload> findAllByCriteriaRange_ParcelGPGrpGpUpload(Integer gpRequestsContextsId_gpRequestsContextsId, Date fsch_dteUpload, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<GpUpload> findAllByCriteriaRange_ParcelGPGrpGpUpload(boolean noTransient, Integer gpRequestsContextsId_gpRequestsContextsId, Date fsch_dteUpload, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_ParcelGPGrpGpUpload_count(Integer gpRequestsContextsId_gpRequestsContextsId, Date fsch_dteUpload, List<String> excludedEntities);


    public List<GpUpload> findAllByCriteriaRange_ParcelsIssuesGrpGpUpload(Integer gpRequestsContextsId_gpRequestsContextsId, String fsch_data, String fsch_environment, Date fsch_dteUpload, String fsch_hash, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<GpUpload> findAllByCriteriaRange_ParcelsIssuesGrpGpUpload(boolean noTransient, Integer gpRequestsContextsId_gpRequestsContextsId, String fsch_data, String fsch_environment, Date fsch_dteUpload, String fsch_hash, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_ParcelsIssuesGrpGpUpload_count(Integer gpRequestsContextsId_gpRequestsContextsId, String fsch_data, String fsch_environment, Date fsch_dteUpload, String fsch_hash, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
