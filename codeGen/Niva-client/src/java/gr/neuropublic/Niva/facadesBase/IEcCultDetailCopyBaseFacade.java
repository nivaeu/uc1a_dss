package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IEcCultDetailCopyBaseFacade extends IFacade<EcCultDetailCopy> {
/*
    @Local
    public interface ILocal extends IEcCultDetailCopyBaseFacade {}

    //@Remote
    //public interface IRemote extends IEcCultDetailCopyBaseFacade {}
*/
    public EcCultDetailCopy initRow();

    public void getTransientFields(List<EcCultDetailCopy> entities);

    public EcCultDetailCopy createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.IEcCultCopyFacade.ILocal ecCultCopyFacade, Set<String> allowedFields);

    public List<EcCultDetailCopy> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public EcCultDetailCopy findByEcdcId(Integer ecdcId);

    public int delByEcdcId(Integer ecdcId);

    public List<EcCultDetailCopy> findAllByCriteriaRange_DecisionMakingGrpEcCultDetailCopy(Integer ecccId_ecccId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<EcCultDetailCopy> findAllByCriteriaRange_DecisionMakingGrpEcCultDetailCopy(boolean noTransient, Integer ecccId_ecccId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_DecisionMakingGrpEcCultDetailCopy_count(Integer ecccId_ecccId, List<String> excludedEntities);


    public List<EcCultDetailCopy> findAllByCriteriaRange_DecisionMakingEcCultDetailCopyCover(Integer ecccId_ecccId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<EcCultDetailCopy> findAllByCriteriaRange_DecisionMakingEcCultDetailCopyCover(boolean noTransient, Integer ecccId_ecccId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_DecisionMakingEcCultDetailCopyCover_count(Integer ecccId_ecccId, List<String> excludedEntities);


    public List<EcCultDetailCopy> findAllByCriteriaRange_DecisionMakingEcCultDetailCopySuper(Integer ecccId_ecccId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<EcCultDetailCopy> findAllByCriteriaRange_DecisionMakingEcCultDetailCopySuper(boolean noTransient, Integer ecccId_ecccId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_DecisionMakingEcCultDetailCopySuper_count(Integer ecccId_ecccId, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
