package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IEcCultDetailBaseFacade extends IFacade<EcCultDetail> {
/*
    @Local
    public interface ILocal extends IEcCultDetailBaseFacade {}

    //@Remote
    //public interface IRemote extends IEcCultDetailBaseFacade {}
*/
    public EcCultDetail initRow();

    public void getTransientFields(List<EcCultDetail> entities);

    public EcCultDetail createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.IEcCultFacade.ILocal ecCultFacade, Set<String> allowedFields);

    public List<EcCultDetail> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public EcCultDetail findByEccuId_OrderingNumber(EcCult eccuId, Integer orderingNumber);

    public EcCultDetail findByEccuId_OrderingNumber(Integer eccuId, Integer orderingNumber);

    public int delByEccuId_OrderingNumber(EcCult eccuId, Integer orderingNumber);

    public int delByEccuId_OrderingNumber(Integer eccuId, Integer orderingNumber);

    public EcCultDetail findByEccdId(Integer eccdId);

    public int delByEccdId(Integer eccdId);

    public List<EcCultDetail> findAllByCriteriaRange_EcGroupGrpEcCultDetail(Integer eccuId_eccuId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<EcCultDetail> findAllByCriteriaRange_EcGroupGrpEcCultDetail(boolean noTransient, Integer eccuId_eccuId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_EcGroupGrpEcCultDetail_count(Integer eccuId_eccuId, List<String> excludedEntities);


    public List<EcCultDetail> findAllByCriteriaRange_EcGroupEcCultDetailCover(Integer eccuId_eccuId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<EcCultDetail> findAllByCriteriaRange_EcGroupEcCultDetailCover(boolean noTransient, Integer eccuId_eccuId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_EcGroupEcCultDetailCover_count(Integer eccuId_eccuId, List<String> excludedEntities);


    public List<EcCultDetail> findAllByCriteriaRange_EcGroupEcCultDetailSuper(Integer eccuId_eccuId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<EcCultDetail> findAllByCriteriaRange_EcGroupEcCultDetailSuper(boolean noTransient, Integer eccuId_eccuId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_EcGroupEcCultDetailSuper_count(Integer eccuId_eccuId, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
