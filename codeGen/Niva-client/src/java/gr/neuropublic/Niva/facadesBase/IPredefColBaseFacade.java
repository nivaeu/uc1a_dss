package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IPredefColBaseFacade extends IFacade<PredefCol> {
/*
    @Local
    public interface ILocal extends IPredefColBaseFacade {}

    //@Remote
    //public interface IRemote extends IPredefColBaseFacade {}
*/
    public PredefCol initRow();

    public void getTransientFields(List<PredefCol> entities);

    public PredefCol createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, Set<String> allowedFields);

    public List<PredefCol> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public PredefCol findByColumnName(String columnName);

    public int delByColumnName(String columnName);

    public PredefCol findByPrcoId(Integer prcoId);

    public int delByPrcoId(Integer prcoId);

    public List<PredefCol> findAllByCriteriaRange_FileTemplate_GrpTemplateColumn_itm__prcoId(String fsch_PredefColLov_columnName, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<PredefCol> findAllByCriteriaRange_FileTemplate_GrpTemplateColumn_itm__prcoId(boolean noTransient, String fsch_PredefColLov_columnName, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_FileTemplate_GrpTemplateColumn_itm__prcoId_count(String fsch_PredefColLov_columnName, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
