package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IGpRequestsBaseFacade extends IFacade<GpRequests> {
/*
    @Local
    public interface ILocal extends IGpRequestsBaseFacade {}

    //@Remote
    //public interface IRemote extends IGpRequestsBaseFacade {}
*/
    public GpRequests initRow();

    public void getTransientFields(List<GpRequests> entities);

    public GpRequests createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.IAgrisnapUsersFacade.ILocal agrisnapUsersFacade, Set<String> allowedFields);

    public List<GpRequests> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public GpRequests findByGpRequestsId(Integer gpRequestsId);

    public int delByGpRequestsId(Integer gpRequestsId);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
