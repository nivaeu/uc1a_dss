package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IClassifierBaseFacade extends IFacade<Classifier> {
/*
    @Local
    public interface ILocal extends IClassifierBaseFacade {}

    //@Remote
    //public interface IRemote extends IClassifierBaseFacade {}
*/
    public Classifier initRow();

    public void getTransientFields(List<Classifier> entities);

    public Classifier createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, Set<String> allowedFields);

    public List<Classifier> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public Classifier findByClfrId(Integer clfrId);

    public int delByClfrId(Integer clfrId);

    public List<Classifier> findAllByCriteriaRange_forLov(String fsch_ClassifierLov_name, String fsch_ClassifierLov_description, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<Classifier> findAllByCriteriaRange_forLov(boolean noTransient, String fsch_ClassifierLov_name, String fsch_ClassifierLov_description, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_forLov_count(String fsch_ClassifierLov_name, String fsch_ClassifierLov_description, List<String> excludedEntities);


    public List<Classifier> findAllByCriteriaRange_ClassifierGrpClassifier(String fsch_name, String fsch_description, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<Classifier> findAllByCriteriaRange_ClassifierGrpClassifier(boolean noTransient, String fsch_name, String fsch_description, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_ClassifierGrpClassifier_count(String fsch_name, String fsch_description, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
