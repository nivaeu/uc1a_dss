package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IEcCultBaseFacade extends IFacade<EcCult> {
/*
    @Local
    public interface ILocal extends IEcCultBaseFacade {}

    //@Remote
    //public interface IRemote extends IEcCultBaseFacade {}
*/
    public EcCult initRow();

    public void getTransientFields(List<EcCult> entities);

    public EcCult createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.ICultivationFacade.ILocal cultivationFacade, gr.neuropublic.Niva.facades.IEcGroupFacade.ILocal ecGroupFacade, gr.neuropublic.Niva.facades.ICoverTypeFacade.ILocal coverTypeFacade, gr.neuropublic.Niva.facades.ISuperClasFacade.ILocal superClasFacade, Set<String> allowedFields);

    public List<EcCult> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public EcCult findByEccuId(Integer eccuId);

    public int delByEccuId(Integer eccuId);

    public List<EcCult> findAllByCriteriaRange_EcGroupGrpEcCult(Integer ecgrId_ecgrId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<EcCult> findAllByCriteriaRange_EcGroupGrpEcCult(boolean noTransient, Integer ecgrId_ecgrId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_EcGroupGrpEcCult_count(Integer ecgrId_ecgrId, List<String> excludedEntities);


    public List<EcCult> findAllByCriteriaRange_EcGroupEcCultCover(Integer ecgrId_ecgrId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<EcCult> findAllByCriteriaRange_EcGroupEcCultCover(boolean noTransient, Integer ecgrId_ecgrId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_EcGroupEcCultCover_count(Integer ecgrId_ecgrId, List<String> excludedEntities);


    public List<EcCult> findAllByCriteriaRange_EcGroupEcCultSuper(Integer ecgrId_ecgrId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<EcCult> findAllByCriteriaRange_EcGroupEcCultSuper(boolean noTransient, Integer ecgrId_ecgrId, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_EcGroupEcCultSuper_count(Integer ecgrId_ecgrId, List<String> excludedEntities);


}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
