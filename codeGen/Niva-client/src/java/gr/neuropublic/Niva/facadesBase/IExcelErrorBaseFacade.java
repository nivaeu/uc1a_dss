package gr.neuropublic.Niva.facadesBase;

import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;


public interface IExcelErrorBaseFacade extends IFacade<ExcelError> {
/*
    @Local
    public interface ILocal extends IExcelErrorBaseFacade {}

    //@Remote
    //public interface IRemote extends IExcelErrorBaseFacade {}
*/
    public ExcelError initRow();

    public void getTransientFields(List<ExcelError> entities);

    public ExcelError createFromJson(Map<String,Object> keysMap, gr.neuropublic.base.CryptoUtils crypto, gr.neuropublic.base.ChangeToCommit.ChangeStatus _changeStatus, org.json.simple.JSONObject json, gr.neuropublic.Niva.facades.IExcelFileFacade.ILocal excelFileFacade, Set<String> allowedFields);

    public List<ExcelError> findAllByIds(List<Integer> ids);

    public int delAllByIds(List<Integer> ids);

    public ExcelError findById(Integer id);

    public int delById(Integer id);

    public List<ExcelError> findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError(Integer exfiId_id, Integer fsch_row, String fsch_errMessage, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public List<ExcelError> findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError(boolean noTransient, Integer exfiId_id, Integer fsch_row, String fsch_errMessage, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);
    public int findAllByCriteriaRange_ExcelFileImportResultsGrpExcelError_count(Integer exfiId_id, Integer fsch_row, String fsch_errMessage, List<String> excludedEntities);

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
