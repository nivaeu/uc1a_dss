/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
State paying Agency


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class AgencyBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'agen_id' είναι υποχρεωτικό")
    @Column(name = "agen_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="agency_agen_id")
    //@SequenceGenerator(name="agency_agen_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer agenId;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'name' είναι υποχρεωτικό")
    @Size(min = 1, max = 60, message="Το πεδίο 'name' πρέπει νά έχει μέγεθος μεταξύ 1 και 60.")
    @Column(name = "name")
    protected String name;



    @Basic(optional = true)
    @Size(min = 0, max = 60, message="Το πεδίο 'country' πρέπει νά έχει μέγεθος μεταξύ 0 και 60.")
    @Column(name = "country")
    protected String country;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @Basic(optional = true)
    @Column(name = "srid")
    protected Short srid;



    public AgencyBase() {
    }

    public AgencyBase(Integer agenId) {
        this.agenId = agenId;
    }

    public AgencyBase(Integer agenId, String name, String country, Integer rowVersion, Short srid) {
        this.agenId = agenId;
        this.name = name;
        this.country = country;
        this.rowVersion = rowVersion;
        this.srid = srid;
    }

    public AgencyBase(String name, String country, Integer rowVersion, Short srid) {
        this.name = name;
        this.country = country;
        this.rowVersion = rowVersion;
        this.srid = srid;
    }

    public Agency clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "Agency:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (Agency)alreadyCloned.get(key);

        Agency clone = new Agency();
        alreadyCloned.put(key, clone);

        clone.setAgenId(getAgenId());
        clone.setName(getName());
        clone.setCountry(getCountry());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteinsert(getDteinsert());
        clone.setUsrupdate(getUsrupdate());
        clone.setDteupdate(getDteupdate());
        clone.setRowVersion(getRowVersion());
        clone.setSrid(getSrid());
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "Agency:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(agenId) : agenId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (agenId != null) {
            sb.append("\"agenId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(agenId) : agenId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"agenId\":"); sb.append("null");sb.append(",");
        }
        if (name != null) {
            sb.append("\"name\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(name)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"name\":"); sb.append("null");sb.append(",");
        }
        if (country != null) {
            sb.append("\"country\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(country)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"country\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (srid != null) {
            sb.append("\"srid\":"); sb.append(srid);sb.append(",");
        } else {
            sb.append("\"srid\":"); sb.append("null");sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    primary key
    */
    public Integer getAgenId() {
        return agenId;
    }

    public void setAgenId(Integer agenId) {
        this.agenId = agenId;
    }
    /*
    name of agency
    */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    /*
    country of agency
    */
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    /*
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    The Spatial Reference Identifier used by the Agency.
    */
    public Short getSrid() {
        return srid;
    }

    public void setSrid(Short srid) {
        this.srid = srid;
    }


    @Override
    public int hashCode() {
        return agenId.hashCode();
    }

    public String getRowKey() {
        return agenId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return agenId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(agenId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.Agency[ agenId=" + agenId + " ]";
    }

    @Override
    public String getEntityName() {
        return "Agency";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
        setUsrinsert(userName);
        setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
        setUsrupdate(userName);
        setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the agenId fields are not set
        if (!(object instanceof AgencyBase)) {
            return false;
        }
        AgencyBase other = (AgencyBase) object;
        if (this.agenId == null || other.agenId == null) 
            return false;
        
        return this.agenId.equals(other.agenId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
