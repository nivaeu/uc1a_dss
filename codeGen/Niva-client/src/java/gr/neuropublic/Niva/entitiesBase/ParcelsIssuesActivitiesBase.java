/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
Contains activities related to each Parcel Issue, e.g. AgriSpan interaction, etc.


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class ParcelsIssuesActivitiesBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'parcels_issues_activities_id' είναι υποχρεωτικό")
    @Column(name = "parcels_issues_activities_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="parcels_issues_activities_parcels_issues_activities_id")
    //@SequenceGenerator(name="parcels_issues_activities_parcels_issues_activities_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer parcelsIssuesActivitiesId;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'type' είναι υποχρεωτικό")
    @Column(name = "type")
    protected Short type;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'dte_timestamp' είναι υποχρεωτικό")
    @Column(name = "dte_timestamp")
    @Temporal(TemporalType.DATE)
    protected Date dteTimestamp;



    @Basic(optional = true)
    @Size(min = 0, max = 2147483647, message="Το πεδίο 'type_extrainfo' πρέπει νά έχει μέγεθος μεταξύ 0 και 2147483647.")
    @Column(name = "type_extrainfo")
    protected String typeExtrainfo;


    @JoinColumn(name = "parcels_issues_id", referencedColumnName = "parcels_issues_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'parcels_issues_id' είναι υποχρεωτικό")
    protected ParcelsIssues parcelsIssuesId;

    public boolean isParcelsIssuesIdPresent() {
        if (parcelsIssuesId==null || parcelsIssuesId.parcelsIssuesId == null)
            return false;
        return true;
    }



    public ParcelsIssuesActivitiesBase() {
    }

    public ParcelsIssuesActivitiesBase(Integer parcelsIssuesActivitiesId) {
        this.parcelsIssuesActivitiesId = parcelsIssuesActivitiesId;
    }

    public ParcelsIssuesActivitiesBase(Integer parcelsIssuesActivitiesId, Integer rowVersion, Short type, Date dteTimestamp, String typeExtrainfo) {
        this.parcelsIssuesActivitiesId = parcelsIssuesActivitiesId;
        this.rowVersion = rowVersion;
        this.type = type;
        this.dteTimestamp = dteTimestamp;
        this.typeExtrainfo = typeExtrainfo;
    }

    public ParcelsIssuesActivitiesBase(Integer rowVersion, Short type, Date dteTimestamp, String typeExtrainfo, ParcelsIssues parcelsIssuesId) {
        this.rowVersion = rowVersion;
        this.type = type;
        this.dteTimestamp = dteTimestamp;
        this.typeExtrainfo = typeExtrainfo;
        this.parcelsIssuesId = parcelsIssuesId;
    }

    public ParcelsIssuesActivities clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "ParcelsIssuesActivities:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (ParcelsIssuesActivities)alreadyCloned.get(key);

        ParcelsIssuesActivities clone = new ParcelsIssuesActivities();
        alreadyCloned.put(key, clone);

        clone.setParcelsIssuesActivitiesId(getParcelsIssuesActivitiesId());
        clone.setRowVersion(getRowVersion());
        clone.setType(getType());
        clone.setDteTimestamp(getDteTimestamp());
        clone.setTypeExtrainfo(getTypeExtrainfo());
        if (parcelsIssuesId == null || parcelsIssuesId.parcelsIssuesId == null) {
            clone.setParcelsIssuesId(parcelsIssuesId);
        } else {
            clone.setParcelsIssuesId(getParcelsIssuesId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "ParcelsIssuesActivities:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(parcelsIssuesActivitiesId) : parcelsIssuesActivitiesId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (parcelsIssuesActivitiesId != null) {
            sb.append("\"parcelsIssuesActivitiesId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(parcelsIssuesActivitiesId) : parcelsIssuesActivitiesId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"parcelsIssuesActivitiesId\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (type != null) {
            sb.append("\"type\":"); sb.append(type);sb.append(",");
        } else {
            sb.append("\"type\":"); sb.append("null");sb.append(",");
        }
        if (dteTimestamp != null) {
            sb.append("\"dteTimestamp\":"); sb.append(dteTimestamp.getTime()); sb.append(",");
        } else {
            sb.append("\"dteTimestamp\":"); sb.append("null");sb.append(",");
        }
        if (typeExtrainfo != null) {
            sb.append("\"typeExtrainfo\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(typeExtrainfo)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"typeExtrainfo\":"); sb.append("null");sb.append(",");
        }
        if (parcelsIssuesId != null && parcelsIssuesId.parcelsIssuesId != null) {
            sb.append("\"parcelsIssuesId\":"); sb.append(getParcelsIssuesId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (parcelsIssuesId == null) {
            sb.append("\"parcelsIssuesId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    */
    public Integer getParcelsIssuesActivitiesId() {
        return parcelsIssuesActivitiesId;
    }

    public void setParcelsIssuesActivitiesId(Integer parcelsIssuesActivitiesId) {
        this.parcelsIssuesActivitiesId = parcelsIssuesActivitiesId;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    */
    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }
    /*
    */
    public Date getDteTimestamp() {
        return dteTimestamp;
    }

    public void setDteTimestamp(Date dteTimestamp) {
        this.dteTimestamp = dteTimestamp;
    }
    /*
    */
    public String getTypeExtrainfo() {
        return typeExtrainfo;
    }

    public void setTypeExtrainfo(String typeExtrainfo) {
        this.typeExtrainfo = typeExtrainfo;
    }
    /*
    */
    public ParcelsIssues getParcelsIssuesId() {
        return parcelsIssuesId;
    }

    public void setParcelsIssuesId(ParcelsIssues parcelsIssuesId) {
    //    if (this.parcelsIssuesId != null) 
    //        this.parcelsIssuesId.internalRemoveParcelsIssuesActivitiesCollection(this);
        this.parcelsIssuesId = parcelsIssuesId;
    //    if (parcelsIssuesId != null)
    //        parcelsIssuesId.internalAddParcelsIssuesActivitiesCollection(this);
    }



    @Override
    public int hashCode() {
        return parcelsIssuesActivitiesId.hashCode();
    }

    public String getRowKey() {
        return parcelsIssuesActivitiesId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return parcelsIssuesActivitiesId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(parcelsIssuesActivitiesId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.ParcelsIssuesActivities[ parcelsIssuesActivitiesId=" + parcelsIssuesActivitiesId + " ]";
    }

    @Override
    public String getEntityName() {
        return "ParcelsIssuesActivities";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the parcelsIssuesActivitiesId fields are not set
        if (!(object instanceof ParcelsIssuesActivitiesBase)) {
            return false;
        }
        ParcelsIssuesActivitiesBase other = (ParcelsIssuesActivitiesBase) object;
        if (this.parcelsIssuesActivitiesId == null || other.parcelsIssuesActivitiesId == null) 
            return false;
        
        return this.parcelsIssuesActivitiesId.equals(other.parcelsIssuesActivitiesId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
