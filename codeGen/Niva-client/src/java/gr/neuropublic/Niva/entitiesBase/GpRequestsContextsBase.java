/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
Contains context details about each request & producer.


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class GpRequestsContextsBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'gp_requests_contexts_id' είναι υποχρεωτικό")
    @Column(name = "gp_requests_contexts_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="gp_requests_contexts_gp_requests_contexts_id")
    //@SequenceGenerator(name="gp_requests_contexts_gp_requests_contexts_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer gpRequestsContextsId;



    @Basic(optional = true)
    @Size(min = 0, max = 2147483647, message="Το πεδίο 'type' πρέπει νά έχει μέγεθος μεταξύ 0 και 2147483647.")
    @Column(name = "type")
    protected String type;



    @Basic(optional = true)
    @Size(min = 0, max = 2147483647, message="Το πεδίο 'label' πρέπει νά έχει μέγεθος μεταξύ 0 και 2147483647.")
    @Column(name = "label")
    protected String label;



    @Basic(optional = true)
    @Size(min = 0, max = 2147483647, message="Το πεδίο 'comment' πρέπει νά έχει μέγεθος μεταξύ 0 και 2147483647.")
    @Column(name = "comment")
    protected String comment;



    @Basic(optional = true)
    @Size(min = 0, max = 2147483647, message="Το πεδίο 'geom_hexewkb' πρέπει νά έχει μέγεθος μεταξύ 0 και 2147483647.")
    @Column(name = "geom_hexewkb")
    protected String geomHexewkb;



    @Basic(optional = true)
    @Size(min = 0, max = 2147483647, message="Το πεδίο 'referencepoint' πρέπει νά έχει μέγεθος μεταξύ 0 και 2147483647.")
    @Column(name = "referencepoint")
    protected String referencepoint;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'hash' είναι υποχρεωτικό")
    @Size(min = 1, max = 32, message="Το πεδίο 'hash' πρέπει νά έχει μέγεθος μεταξύ 1 και 32.")
    @Column(name = "hash")
    protected String hash;


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "gpRequestsContextsId", orphanRemoval=false)
    @OneToMany(mappedBy = "gpRequestsContextsId")
    protected List<GpUpload> gpUploadCollection = new ArrayList<GpUpload>();


    @JoinColumn(name = "gp_requests_producers_id", referencedColumnName = "gp_requests_producers_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'gp_requests_producers_id' είναι υποχρεωτικό")
    protected GpRequestsProducers gpRequestsProducersId;

    public boolean isGpRequestsProducersIdPresent() {
        if (gpRequestsProducersId==null || gpRequestsProducersId.gpRequestsProducersId == null)
            return false;
        return true;
    }


    @JoinColumn(name = "parcels_issues_id", referencedColumnName = "parcels_issues_id")
    @ManyToOne(optional = true, fetch= FetchType.LAZY)
    protected ParcelsIssues parcelsIssuesId;

    public boolean isParcelsIssuesIdPresent() {
        if (parcelsIssuesId==null || parcelsIssuesId.parcelsIssuesId == null)
            return false;
        return true;
    }



    public GpRequestsContextsBase() {
    }

    public GpRequestsContextsBase(Integer gpRequestsContextsId) {
        this.gpRequestsContextsId = gpRequestsContextsId;
    }

    public GpRequestsContextsBase(Integer gpRequestsContextsId, String type, String label, String comment, String geomHexewkb, String referencepoint, Integer rowVersion, String hash) {
        this.gpRequestsContextsId = gpRequestsContextsId;
        this.type = type;
        this.label = label;
        this.comment = comment;
        this.geomHexewkb = geomHexewkb;
        this.referencepoint = referencepoint;
        this.rowVersion = rowVersion;
        this.hash = hash;
    }

    public GpRequestsContextsBase(String type, String label, String comment, String geomHexewkb, String referencepoint, Integer rowVersion, String hash, GpRequestsProducers gpRequestsProducersId, ParcelsIssues parcelsIssuesId) {
        this.type = type;
        this.label = label;
        this.comment = comment;
        this.geomHexewkb = geomHexewkb;
        this.referencepoint = referencepoint;
        this.rowVersion = rowVersion;
        this.hash = hash;
        this.gpRequestsProducersId = gpRequestsProducersId;
        this.parcelsIssuesId = parcelsIssuesId;
    }

    public GpRequestsContexts clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "GpRequestsContexts:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (GpRequestsContexts)alreadyCloned.get(key);

        GpRequestsContexts clone = new GpRequestsContexts();
        alreadyCloned.put(key, clone);

        clone.setGpRequestsContextsId(getGpRequestsContextsId());
        clone.setType(getType());
        clone.setLabel(getLabel());
        clone.setComment(getComment());
        clone.setGeomHexewkb(getGeomHexewkb());
        clone.setReferencepoint(getReferencepoint());
        clone.setRowVersion(getRowVersion());
        clone.setHash(getHash());
        clone.setGpUploadCollection(getGpUploadCollection());
        if (gpRequestsProducersId == null || gpRequestsProducersId.gpRequestsProducersId == null) {
            clone.setGpRequestsProducersId(gpRequestsProducersId);
        } else {
            clone.setGpRequestsProducersId(getGpRequestsProducersId().clone(alreadyCloned));
        }
        if (parcelsIssuesId == null || parcelsIssuesId.parcelsIssuesId == null) {
            clone.setParcelsIssuesId(parcelsIssuesId);
        } else {
            clone.setParcelsIssuesId(getParcelsIssuesId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "GpRequestsContexts:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(gpRequestsContextsId) : gpRequestsContextsId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (gpRequestsContextsId != null) {
            sb.append("\"gpRequestsContextsId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(gpRequestsContextsId) : gpRequestsContextsId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"gpRequestsContextsId\":"); sb.append("null");sb.append(",");
        }
        if (type != null) {
            sb.append("\"type\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(type)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"type\":"); sb.append("null");sb.append(",");
        }
        if (label != null) {
            sb.append("\"label\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(label)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"label\":"); sb.append("null");sb.append(",");
        }
        if (comment != null) {
            sb.append("\"comment\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(comment)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"comment\":"); sb.append("null");sb.append(",");
        }
        if (geomHexewkb != null) {
            sb.append("\"geomHexewkb\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(geomHexewkb)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"geomHexewkb\":"); sb.append("null");sb.append(",");
        }
        if (referencepoint != null) {
            sb.append("\"referencepoint\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(referencepoint)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"referencepoint\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (hash != null) {
            sb.append("\"hash\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(hash)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"hash\":"); sb.append("null");sb.append(",");
        }
        if (gpRequestsProducersId != null && gpRequestsProducersId.gpRequestsProducersId != null) {
            sb.append("\"gpRequestsProducersId\":"); sb.append(getGpRequestsProducersId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (gpRequestsProducersId == null) {
            sb.append("\"gpRequestsProducersId\":"); sb.append("null");  sb.append(",");
        }
        if (parcelsIssuesId != null && parcelsIssuesId.parcelsIssuesId != null) {
            sb.append("\"parcelsIssuesId\":"); sb.append(getParcelsIssuesId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (parcelsIssuesId == null) {
            sb.append("\"parcelsIssuesId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    */
    public Integer getGpRequestsContextsId() {
        return gpRequestsContextsId;
    }

    public void setGpRequestsContextsId(Integer gpRequestsContextsId) {
        this.gpRequestsContextsId = gpRequestsContextsId;
    }
    /*
    */
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    /*
    */
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    /*
    */
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    /*
    Contains the geometry (with irs SRID) in HEXEWKB format (as text).
    */
    public String getGeomHexewkb() {
        return geomHexewkb;
    }

    public void setGeomHexewkb(String geomHexewkb) {
        this.geomHexewkb = geomHexewkb;
    }
    /*
    */
    public String getReferencepoint() {
        return referencepoint;
    }

    public void setReferencepoint(String referencepoint) {
        this.referencepoint = referencepoint;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    Hash identification.
    */
    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
    @XmlTransient
    public List<GpUpload> getGpUploadCollection() {
        return gpUploadCollection;
    }

    public void setGpUploadCollection(List<GpUpload> gpUploadCollection) {
        this.gpUploadCollection = gpUploadCollection;
    }

    public void removeGpUploadCollection(GpUpload child) {
        if (child != null)
            child.setGpRequestsContextsId(null);
    }

    public void addGpUploadCollection(GpUpload child) {
        if (child != null)
            child.setGpRequestsContextsId((GpRequestsContexts)this);
    }

    void internalRemoveGpUploadCollection(GpUpload child) {
        if (child != null)
            this.gpUploadCollection.remove(child);
    }

    void internalAddGpUploadCollection(GpUpload child) {
        if (child != null)
            this.gpUploadCollection.add(child);
    }

    /*
    */
    public GpRequestsProducers getGpRequestsProducersId() {
        return gpRequestsProducersId;
    }

    public void setGpRequestsProducersId(GpRequestsProducers gpRequestsProducersId) {
    //    if (this.gpRequestsProducersId != null) 
    //        this.gpRequestsProducersId.internalRemoveGpRequestsContextsCollection(this);
        this.gpRequestsProducersId = gpRequestsProducersId;
    //    if (gpRequestsProducersId != null)
    //        gpRequestsProducersId.internalAddGpRequestsContextsCollection(this);
    }

    /*
    */
    public ParcelsIssues getParcelsIssuesId() {
        return parcelsIssuesId;
    }

    public void setParcelsIssuesId(ParcelsIssues parcelsIssuesId) {
    //    if (this.parcelsIssuesId != null) 
    //        this.parcelsIssuesId.internalRemoveGpRequestsContextsCollection(this);
        this.parcelsIssuesId = parcelsIssuesId;
    //    if (parcelsIssuesId != null)
    //        parcelsIssuesId.internalAddGpRequestsContextsCollection(this);
    }



    @Override
    public int hashCode() {
        return gpRequestsContextsId.hashCode();
    }

    public String getRowKey() {
        return gpRequestsContextsId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return gpRequestsContextsId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(gpRequestsContextsId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.GpRequestsContexts[ gpRequestsContextsId=" + gpRequestsContextsId + " ]";
    }

    @Override
    public String getEntityName() {
        return "GpRequestsContexts";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the gpRequestsContextsId fields are not set
        if (!(object instanceof GpRequestsContextsBase)) {
            return false;
        }
        GpRequestsContextsBase other = (GpRequestsContextsBase) object;
        if (this.gpRequestsContextsId == null || other.gpRequestsContextsId == null) 
            return false;
        
        return this.gpRequestsContextsId.equals(other.gpRequestsContextsId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
