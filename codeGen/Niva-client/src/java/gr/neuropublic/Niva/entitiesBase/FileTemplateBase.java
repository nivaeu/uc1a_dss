/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
the template  of the file of input data


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
@NamedQuery(name = "FileTemplate.findFileTemplate", query = "SELECT x FROM FileTemplate x WHERE (x.name like :name)")})
public abstract class FileTemplateBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'fite_id' είναι υποχρεωτικό")
    @Column(name = "fite_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="file_template_fite_id")
    //@SequenceGenerator(name="file_template_fite_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer fiteId;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'name' είναι υποχρεωτικό")
    @Size(min = 1, max = 60, message="Το πεδίο 'name' πρέπει νά έχει μέγεθος μεταξύ 1 και 60.")
    @Column(name = "name")
    protected String name;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @OneToMany(cascade = {CascadeType.REMOVE}, mappedBy = "fiteId", orphanRemoval=false)
    protected List<TemplateColumn> templateColumnCollection = new ArrayList<TemplateColumn>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "fiteId", orphanRemoval=false)
    @OneToMany(mappedBy = "fiteId")
    protected List<Classification> classificationCollection = new ArrayList<Classification>();



    public FileTemplateBase() {
    }

    public FileTemplateBase(Integer fiteId) {
        this.fiteId = fiteId;
    }

    public FileTemplateBase(Integer fiteId, String name, Integer rowVersion) {
        this.fiteId = fiteId;
        this.name = name;
        this.rowVersion = rowVersion;
    }

    public FileTemplateBase(String name, Integer rowVersion) {
        this.name = name;
        this.rowVersion = rowVersion;
    }

    public FileTemplate clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "FileTemplate:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (FileTemplate)alreadyCloned.get(key);

        FileTemplate clone = new FileTemplate();
        alreadyCloned.put(key, clone);

        clone.setFiteId(getFiteId());
        clone.setName(getName());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteinsert(getDteinsert());
        clone.setUsrupdate(getUsrupdate());
        clone.setDteupdate(getDteupdate());
        clone.setRowVersion(getRowVersion());
        clone.setTemplateColumnCollection(getTemplateColumnCollection());
        clone.setClassificationCollection(getClassificationCollection());
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "FileTemplate:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(fiteId) : fiteId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (fiteId != null) {
            sb.append("\"fiteId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(fiteId) : fiteId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"fiteId\":"); sb.append("null");sb.append(",");
        }
        if (name != null) {
            sb.append("\"name\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(name)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"name\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    primary key
    */
    public Integer getFiteId() {
        return fiteId;
    }

    public void setFiteId(Integer fiteId) {
        this.fiteId = fiteId;
    }
    /*
    template general name
    */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    /*
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    @XmlTransient
    public List<TemplateColumn> getTemplateColumnCollection() {
        return templateColumnCollection;
    }

    public void setTemplateColumnCollection(List<TemplateColumn> templateColumnCollection) {
        this.templateColumnCollection = templateColumnCollection;
    }

    public void removeTemplateColumnCollection(TemplateColumn child) {
        if (child != null)
            child.setFiteId(null);
    }

    public void addTemplateColumnCollection(TemplateColumn child) {
        if (child != null)
            child.setFiteId((FileTemplate)this);
    }

    void internalRemoveTemplateColumnCollection(TemplateColumn child) {
        if (child != null)
            this.templateColumnCollection.remove(child);
    }

    void internalAddTemplateColumnCollection(TemplateColumn child) {
        if (child != null)
            this.templateColumnCollection.add(child);
    }

    @XmlTransient
    public List<Classification> getClassificationCollection() {
        return classificationCollection;
    }

    public void setClassificationCollection(List<Classification> classificationCollection) {
        this.classificationCollection = classificationCollection;
    }

    public void removeClassificationCollection(Classification child) {
        if (child != null)
            child.setFiteId(null);
    }

    public void addClassificationCollection(Classification child) {
        if (child != null)
            child.setFiteId((FileTemplate)this);
    }

    void internalRemoveClassificationCollection(Classification child) {
        if (child != null)
            this.classificationCollection.remove(child);
    }

    void internalAddClassificationCollection(Classification child) {
        if (child != null)
            this.classificationCollection.add(child);
    }



    @Override
    public int hashCode() {
        return fiteId.hashCode();
    }

    public String getRowKey() {
        return fiteId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return fiteId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(fiteId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.FileTemplate[ fiteId=" + fiteId + " ]";
    }

    @Override
    public String getEntityName() {
        return "FileTemplate";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
        setUsrinsert(userName);
        setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
        setUsrupdate(userName);
        setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the fiteId fields are not set
        if (!(object instanceof FileTemplateBase)) {
            return false;
        }
        FileTemplateBase other = (FileTemplateBase) object;
        if (this.fiteId == null || other.fiteId == null) 
            return false;
        
        return this.fiteId.equals(other.fiteId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
