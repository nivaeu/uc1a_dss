/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
excel errors


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class ExcelErrorBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'id' είναι υποχρεωτικό")
    @Column(name = "id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="excel_errors_id")
    //@SequenceGenerator(name="excel_errors_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer id;


    @Basic(optional = true)
    @Column(name = "excel_row_num")
    protected Integer excelRowNum;



    @Basic(optional = true)
    @Size(min = 0, max = 500, message="Το πεδίο 'err_message' πρέπει νά έχει μέγεθος μεταξύ 0 και 500.")
    @Column(name = "err_message")
    protected String errMessage;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @JoinColumn(name = "exfi_id", referencedColumnName = "id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'exfi_id' είναι υποχρεωτικό")
    protected ExcelFile exfiId;

    public boolean isExfiIdPresent() {
        if (exfiId==null || exfiId.id == null)
            return false;
        return true;
    }



    public ExcelErrorBase() {
    }

    public ExcelErrorBase(Integer id) {
        this.id = id;
    }

    public ExcelErrorBase(Integer id, Integer excelRowNum, String errMessage, Integer rowVersion) {
        this.id = id;
        this.excelRowNum = excelRowNum;
        this.errMessage = errMessage;
        this.rowVersion = rowVersion;
    }

    public ExcelErrorBase(Integer excelRowNum, String errMessage, Integer rowVersion, ExcelFile exfiId) {
        this.excelRowNum = excelRowNum;
        this.errMessage = errMessage;
        this.rowVersion = rowVersion;
        this.exfiId = exfiId;
    }

    public ExcelError clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "ExcelError:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (ExcelError)alreadyCloned.get(key);

        ExcelError clone = new ExcelError();
        alreadyCloned.put(key, clone);

        clone.setId(getId());
        clone.setExcelRowNum(getExcelRowNum());
        clone.setErrMessage(getErrMessage());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteinsert(getDteinsert());
        clone.setUsrupdate(getUsrupdate());
        clone.setDteupdate(getDteupdate());
        clone.setRowVersion(getRowVersion());
        if (exfiId == null || exfiId.id == null) {
            clone.setExfiId(exfiId);
        } else {
            clone.setExfiId(getExfiId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "ExcelError:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(id) : id); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (id != null) {
            sb.append("\"id\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(id) : id); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"id\":"); sb.append("null");sb.append(",");
        }
        if (excelRowNum != null) {
            sb.append("\"excelRowNum\":"); sb.append(excelRowNum);sb.append(",");
        } else {
            sb.append("\"excelRowNum\":"); sb.append("null");sb.append(",");
        }
        if (errMessage != null) {
            sb.append("\"errMessage\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(errMessage)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"errMessage\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (exfiId != null && exfiId.id != null) {
            sb.append("\"exfiId\":"); sb.append(getExfiId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (exfiId == null) {
            sb.append("\"exfiId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    primary key
    */
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    /*
    excel_row_num
    */
    public Integer getExcelRowNum() {
        return excelRowNum;
    }

    public void setExcelRowNum(Integer excelRowNum) {
        this.excelRowNum = excelRowNum;
    }
    /*
    err_message
    */
    public String getErrMessage() {
        return errMessage;
    }

    public void setErrMessage(String errMessage) {
        this.errMessage = errMessage;
    }
    /*
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    fk to excel_files
    */
    public ExcelFile getExfiId() {
        return exfiId;
    }

    public void setExfiId(ExcelFile exfiId) {
    //    if (this.exfiId != null) 
    //        this.exfiId.internalRemoveExcelErrorCollection(this);
        this.exfiId = exfiId;
    //    if (exfiId != null)
    //        exfiId.internalAddExcelErrorCollection(this);
    }



    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public String getRowKey() {
        return id.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return id;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(id);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.ExcelError[ id=" + id + " ]";
    }

    @Override
    public String getEntityName() {
        return "ExcelError";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
        setUsrinsert(userName);
        setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
        setUsrupdate(userName);
        setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExcelErrorBase)) {
            return false;
        }
        ExcelErrorBase other = (ExcelErrorBase) object;
        if (this.id == null || other.id == null) 
            return false;
        
        return this.id.equals(other.id);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
