/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class EcGroupCopyBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'ecgc_id' είναι υποχρεωτικό")
    @Column(name = "ecgc_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ec_group_copy_ecgc_id")
    //@SequenceGenerator(name="ec_group_copy_ecgc_id", sequenceName="niva.ec_group_copy_ecgc_id_seq", allocationSize=1)
    protected Integer ecgcId;



    @Basic(optional = true)
    @Size(min = 0, max = 60, message="Το πεδίο 'description' πρέπει νά έχει μέγεθος μεταξύ 0 και 60.")
    @Column(name = "description")
    protected String description;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'name' είναι υποχρεωτικό")
    @Size(min = 1, max = 60, message="Το πεδίο 'name' πρέπει νά έχει μέγεθος μεταξύ 1 και 60.")
    @Column(name = "name")
    protected String name;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'recordtype' είναι υποχρεωτικό")
    @Column(name = "recordtype")
    protected Integer recordtype;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'crop_level' είναι υποχρεωτικό")
    @Column(name = "crop_level")
    protected Short cropLevel;


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "ecgcId", orphanRemoval=false)
    @OneToMany(mappedBy = "ecgcId")
    protected List<EcCultCopy> ecCultCopyCollection = new ArrayList<EcCultCopy>();


    @JoinColumn(name = "dema_id", referencedColumnName = "dema_id")
    @ManyToOne(optional = true, fetch= FetchType.LAZY)
    protected DecisionMaking demaId;

    public boolean isDemaIdPresent() {
        if (demaId==null || demaId.demaId == null)
            return false;
        return true;
    }



    public EcGroupCopyBase() {
    }

    public EcGroupCopyBase(Integer ecgcId) {
        this.ecgcId = ecgcId;
    }

    public EcGroupCopyBase(Integer ecgcId, String description, String name, Integer rowVersion, Integer recordtype, Short cropLevel) {
        this.ecgcId = ecgcId;
        this.description = description;
        this.name = name;
        this.rowVersion = rowVersion;
        this.recordtype = recordtype;
        this.cropLevel = cropLevel;
    }

    public EcGroupCopyBase(String description, String name, Integer rowVersion, Integer recordtype, Short cropLevel, DecisionMaking demaId) {
        this.description = description;
        this.name = name;
        this.rowVersion = rowVersion;
        this.recordtype = recordtype;
        this.cropLevel = cropLevel;
        this.demaId = demaId;
    }

    public EcGroupCopy clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "EcGroupCopy:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (EcGroupCopy)alreadyCloned.get(key);

        EcGroupCopy clone = new EcGroupCopy();
        alreadyCloned.put(key, clone);

        clone.setEcgcId(getEcgcId());
        clone.setDescription(getDescription());
        clone.setName(getName());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteinsert(getDteinsert());
        clone.setUsrupdate(getUsrupdate());
        clone.setDteupdate(getDteupdate());
        clone.setRowVersion(getRowVersion());
        clone.setRecordtype(getRecordtype());
        clone.setCropLevel(getCropLevel());
        clone.setEcCultCopyCollection(getEcCultCopyCollection());
        if (demaId == null || demaId.demaId == null) {
            clone.setDemaId(demaId);
        } else {
            clone.setDemaId(getDemaId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "EcGroupCopy:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(ecgcId) : ecgcId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (ecgcId != null) {
            sb.append("\"ecgcId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(ecgcId) : ecgcId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"ecgcId\":"); sb.append("null");sb.append(",");
        }
        if (description != null) {
            sb.append("\"description\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(description)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"description\":"); sb.append("null");sb.append(",");
        }
        if (name != null) {
            sb.append("\"name\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(name)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"name\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (recordtype != null) {
            sb.append("\"recordtype\":"); sb.append(recordtype);sb.append(",");
        } else {
            sb.append("\"recordtype\":"); sb.append("null");sb.append(",");
        }
        if (cropLevel != null) {
            sb.append("\"cropLevel\":"); sb.append(cropLevel);sb.append(",");
        } else {
            sb.append("\"cropLevel\":"); sb.append("null");sb.append(",");
        }
        if (demaId != null && demaId.demaId != null) {
            sb.append("\"demaId\":"); sb.append(getDemaId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (demaId == null) {
            sb.append("\"demaId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    */
    public Integer getEcgcId() {
        return ecgcId;
    }

    public void setEcgcId(Integer ecgcId) {
        this.ecgcId = ecgcId;
    }
    /*
    */
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    /*
    */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    /*
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    */
    public Integer getRecordtype() {
        return recordtype;
    }

    public void setRecordtype(Integer recordtype) {
        this.recordtype = recordtype;
    }
    /*
    */
    public Short getCropLevel() {
        return cropLevel;
    }

    public void setCropLevel(Short cropLevel) {
        this.cropLevel = cropLevel;
    }
    @XmlTransient
    public List<EcCultCopy> getEcCultCopyCollection() {
        return ecCultCopyCollection;
    }

    public void setEcCultCopyCollection(List<EcCultCopy> ecCultCopyCollection) {
        this.ecCultCopyCollection = ecCultCopyCollection;
    }

    public void removeEcCultCopyCollection(EcCultCopy child) {
        if (child != null)
            child.setEcgcId(null);
    }

    public void addEcCultCopyCollection(EcCultCopy child) {
        if (child != null)
            child.setEcgcId((EcGroupCopy)this);
    }

    void internalRemoveEcCultCopyCollection(EcCultCopy child) {
        if (child != null)
            this.ecCultCopyCollection.remove(child);
    }

    void internalAddEcCultCopyCollection(EcCultCopy child) {
        if (child != null)
            this.ecCultCopyCollection.add(child);
    }

    /*
    */
    public DecisionMaking getDemaId() {
        return demaId;
    }

    public void setDemaId(DecisionMaking demaId) {
    //    if (this.demaId != null) 
    //        this.demaId.internalRemoveEcGroupCopyCollection(this);
        this.demaId = demaId;
    //    if (demaId != null)
    //        demaId.internalAddEcGroupCopyCollection(this);
    }



    @Override
    public int hashCode() {
        return ecgcId.hashCode();
    }

    public String getRowKey() {
        return ecgcId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return ecgcId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(ecgcId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.EcGroupCopy[ ecgcId=" + ecgcId + " ]";
    }

    @Override
    public String getEntityName() {
        return "EcGroupCopy";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
        setUsrinsert(userName);
        setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
        setUsrupdate(userName);
        setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the ecgcId fields are not set
        if (!(object instanceof EcGroupCopyBase)) {
            return false;
        }
        EcGroupCopyBase other = (EcGroupCopyBase) object;
        if (this.ecgcId == null || other.ecgcId == null) 
            return false;
        
        return this.ecgcId.equals(other.ecgcId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
