/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
decision making


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
@NamedQuery(name = "DecisionMaking.findDecisionMaking", query = "SELECT x FROM DecisionMaking x WHERE (x.description like :description) AND (x.dateTime = :dateTime) AND (x.clasId.clasId = :clasId_clasId) AND (x.ecgrId.ecgrId = :ecgrId_ecgrId) "),
@NamedQuery(name = "DecisionMaking.findDecisionMakingRO", query = "SELECT x FROM DecisionMaking x WHERE (x.description like :demaId_description) AND (x.dateTime = :dateTime) AND (x.clasId.clasId = :clasId_clasId) AND (x.ecgrId.ecgrId = :ecgrId_ecgrId) ")})
public abstract class DecisionMakingBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'dema_id' είναι υποχρεωτικό")
    @Column(name = "dema_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="decision_making_dema_id")
    //@SequenceGenerator(name="decision_making_dema_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer demaId;



    @Basic(optional = true)
    @Size(min = 0, max = 60, message="Το πεδίο 'description' πρέπει νά έχει μέγεθος μεταξύ 0 και 60.")
    @Column(name = "description")
    protected String description;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'date_time' είναι υποχρεωτικό")
    @Column(name = "date_time")
    @Temporal(TemporalType.DATE)
    protected Date dateTime;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @Basic(optional = true)
    @Column(name = "recordtype")
    protected Integer recordtype;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'has_been_run' είναι υποχρεωτικό")
    @Column(name = "has_been_run")
    protected Boolean hasBeenRun;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'has_populated_integrated_decisions_and_issues' είναι υποχρεωτικό")
    @Column(name = "has_populated_integrated_decisions_and_issues")
    protected Boolean hasPopulatedIntegratedDecisionsAndIssues;


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "demaId", orphanRemoval=false)
    @OneToMany(mappedBy = "demaId")
    protected List<Integrateddecision> integrateddecisionCollection = new ArrayList<Integrateddecision>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "demaId", orphanRemoval=false)
    @OneToMany(mappedBy = "demaId")
    protected List<EcGroupCopy> ecGroupCopyCollection = new ArrayList<EcGroupCopy>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "demaId", orphanRemoval=false)
    @OneToMany(mappedBy = "demaId")
    protected List<ParcelDecision> parcelDecisionCollection = new ArrayList<ParcelDecision>();


    @JoinColumn(name = "clas_id", referencedColumnName = "clas_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'clas_id' είναι υποχρεωτικό")
    protected Classification clasId;

    public boolean isClasIdPresent() {
        if (clasId==null || clasId.clasId == null)
            return false;
        return true;
    }


    @JoinColumn(name = "ecgr_id", referencedColumnName = "ecgr_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'ecgr_id' είναι υποχρεωτικό")
    protected EcGroup ecgrId;

    public boolean isEcgrIdPresent() {
        if (ecgrId==null || ecgrId.ecgrId == null)
            return false;
        return true;
    }



    public DecisionMakingBase() {
    }

    public DecisionMakingBase(Integer demaId) {
        this.demaId = demaId;
    }

    public DecisionMakingBase(Integer demaId, String description, Date dateTime, Integer rowVersion, Integer recordtype, Boolean hasBeenRun, Boolean hasPopulatedIntegratedDecisionsAndIssues) {
        this.demaId = demaId;
        this.description = description;
        this.dateTime = dateTime;
        this.rowVersion = rowVersion;
        this.recordtype = recordtype;
        this.hasBeenRun = hasBeenRun;
        this.hasPopulatedIntegratedDecisionsAndIssues = hasPopulatedIntegratedDecisionsAndIssues;
    }

    public DecisionMakingBase(String description, Date dateTime, Integer rowVersion, Integer recordtype, Boolean hasBeenRun, Boolean hasPopulatedIntegratedDecisionsAndIssues, Classification clasId, EcGroup ecgrId) {
        this.description = description;
        this.dateTime = dateTime;
        this.rowVersion = rowVersion;
        this.recordtype = recordtype;
        this.hasBeenRun = hasBeenRun;
        this.hasPopulatedIntegratedDecisionsAndIssues = hasPopulatedIntegratedDecisionsAndIssues;
        this.clasId = clasId;
        this.ecgrId = ecgrId;
    }

    public DecisionMaking clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "DecisionMaking:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (DecisionMaking)alreadyCloned.get(key);

        DecisionMaking clone = new DecisionMaking();
        alreadyCloned.put(key, clone);

        clone.setDemaId(getDemaId());
        clone.setDescription(getDescription());
        clone.setDateTime(getDateTime());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteinsert(getDteinsert());
        clone.setUsrupdate(getUsrupdate());
        clone.setDteupdate(getDteupdate());
        clone.setRowVersion(getRowVersion());
        clone.setRecordtype(getRecordtype());
        clone.setHasBeenRun(getHasBeenRun());
        clone.setHasPopulatedIntegratedDecisionsAndIssues(getHasPopulatedIntegratedDecisionsAndIssues());
        clone.setIntegrateddecisionCollection(getIntegrateddecisionCollection());
        clone.setEcGroupCopyCollection(getEcGroupCopyCollection());
        clone.setParcelDecisionCollection(getParcelDecisionCollection());
        if (clasId == null || clasId.clasId == null) {
            clone.setClasId(clasId);
        } else {
            clone.setClasId(getClasId().clone(alreadyCloned));
        }
        if (ecgrId == null || ecgrId.ecgrId == null) {
            clone.setEcgrId(ecgrId);
        } else {
            clone.setEcgrId(getEcgrId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "DecisionMaking:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(demaId) : demaId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (demaId != null) {
            sb.append("\"demaId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(demaId) : demaId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"demaId\":"); sb.append("null");sb.append(",");
        }
        if (description != null) {
            sb.append("\"description\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(description)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"description\":"); sb.append("null");sb.append(",");
        }
        if (dateTime != null) {
            sb.append("\"dateTime\":"); sb.append(dateTime.getTime()); sb.append(",");
        } else {
            sb.append("\"dateTime\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (recordtype != null) {
            sb.append("\"recordtype\":"); sb.append(recordtype);sb.append(",");
        } else {
            sb.append("\"recordtype\":"); sb.append("null");sb.append(",");
        }
        if (hasBeenRun != null) {
            sb.append("\"hasBeenRun\":"); sb.append(hasBeenRun);sb.append(",");
        } else {
            sb.append("\"hasBeenRun\":"); sb.append("null");sb.append(",");
        }
        if (hasPopulatedIntegratedDecisionsAndIssues != null) {
            sb.append("\"hasPopulatedIntegratedDecisionsAndIssues\":"); sb.append(hasPopulatedIntegratedDecisionsAndIssues);sb.append(",");
        } else {
            sb.append("\"hasPopulatedIntegratedDecisionsAndIssues\":"); sb.append("null");sb.append(",");
        }
        if (clasId != null && clasId.clasId != null) {
            sb.append("\"clasId\":"); sb.append(getClasId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (clasId == null) {
            sb.append("\"clasId\":"); sb.append("null");  sb.append(",");
        }
        if (ecgrId != null && ecgrId.ecgrId != null) {
            sb.append("\"ecgrId\":"); sb.append(getEcgrId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (ecgrId == null) {
            sb.append("\"ecgrId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    primary key
    */
    public Integer getDemaId() {
        return demaId;
    }

    public void setDemaId(Integer demaId) {
        this.demaId = demaId;
    }
    /*
    */
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    /*
    */
    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
    /*
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    */
    public Integer getRecordtype() {
        return recordtype;
    }

    public void setRecordtype(Integer recordtype) {
        this.recordtype = recordtype;
    }
    /*
    Flag True if Decision Making has been run.
    */
    public Boolean getHasBeenRun() {
        return hasBeenRun;
    }

    public void setHasBeenRun(Boolean hasBeenRun) {
        this.hasBeenRun = hasBeenRun;
    }
    /*
    A flag about the creation of integrated decisions & issues for a parcel, according to the decision making results.
    */
    public Boolean getHasPopulatedIntegratedDecisionsAndIssues() {
        return hasPopulatedIntegratedDecisionsAndIssues;
    }

    public void setHasPopulatedIntegratedDecisionsAndIssues(Boolean hasPopulatedIntegratedDecisionsAndIssues) {
        this.hasPopulatedIntegratedDecisionsAndIssues = hasPopulatedIntegratedDecisionsAndIssues;
    }
    @XmlTransient
    public List<Integrateddecision> getIntegrateddecisionCollection() {
        return integrateddecisionCollection;
    }

    public void setIntegrateddecisionCollection(List<Integrateddecision> integrateddecisionCollection) {
        this.integrateddecisionCollection = integrateddecisionCollection;
    }

    public void removeIntegrateddecisionCollection(Integrateddecision child) {
        if (child != null)
            child.setDemaId(null);
    }

    public void addIntegrateddecisionCollection(Integrateddecision child) {
        if (child != null)
            child.setDemaId((DecisionMaking)this);
    }

    void internalRemoveIntegrateddecisionCollection(Integrateddecision child) {
        if (child != null)
            this.integrateddecisionCollection.remove(child);
    }

    void internalAddIntegrateddecisionCollection(Integrateddecision child) {
        if (child != null)
            this.integrateddecisionCollection.add(child);
    }

    @XmlTransient
    public List<EcGroupCopy> getEcGroupCopyCollection() {
        return ecGroupCopyCollection;
    }

    public void setEcGroupCopyCollection(List<EcGroupCopy> ecGroupCopyCollection) {
        this.ecGroupCopyCollection = ecGroupCopyCollection;
    }

    public void removeEcGroupCopyCollection(EcGroupCopy child) {
        if (child != null)
            child.setDemaId(null);
    }

    public void addEcGroupCopyCollection(EcGroupCopy child) {
        if (child != null)
            child.setDemaId((DecisionMaking)this);
    }

    void internalRemoveEcGroupCopyCollection(EcGroupCopy child) {
        if (child != null)
            this.ecGroupCopyCollection.remove(child);
    }

    void internalAddEcGroupCopyCollection(EcGroupCopy child) {
        if (child != null)
            this.ecGroupCopyCollection.add(child);
    }

    @XmlTransient
    public List<ParcelDecision> getParcelDecisionCollection() {
        return parcelDecisionCollection;
    }

    public void setParcelDecisionCollection(List<ParcelDecision> parcelDecisionCollection) {
        this.parcelDecisionCollection = parcelDecisionCollection;
    }

    public void removeParcelDecisionCollection(ParcelDecision child) {
        if (child != null)
            child.setDemaId(null);
    }

    public void addParcelDecisionCollection(ParcelDecision child) {
        if (child != null)
            child.setDemaId((DecisionMaking)this);
    }

    void internalRemoveParcelDecisionCollection(ParcelDecision child) {
        if (child != null)
            this.parcelDecisionCollection.remove(child);
    }

    void internalAddParcelDecisionCollection(ParcelDecision child) {
        if (child != null)
            this.parcelDecisionCollection.add(child);
    }

    /*
    fk to classification
    */
    public Classification getClasId() {
        return clasId;
    }

    public void setClasId(Classification clasId) {
    //    if (this.clasId != null) 
    //        this.clasId.internalRemoveDecisionMakingCollection(this);
        this.clasId = clasId;
    //    if (clasId != null)
    //        clasId.internalAddDecisionMakingCollection(this);
    }

    /*
    fk to ec_group
    */
    public EcGroup getEcgrId() {
        return ecgrId;
    }

    public void setEcgrId(EcGroup ecgrId) {
    //    if (this.ecgrId != null) 
    //        this.ecgrId.internalRemoveDecisionMakingCollection(this);
        this.ecgrId = ecgrId;
    //    if (ecgrId != null)
    //        ecgrId.internalAddDecisionMakingCollection(this);
    }



    @Override
    public int hashCode() {
        return demaId.hashCode();
    }

    public String getRowKey() {
        return demaId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return demaId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(demaId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.DecisionMaking[ demaId=" + demaId + " ]";
    }

    @Override
    public String getEntityName() {
        return "DecisionMaking";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
        setUsrinsert(userName);
        setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
        setUsrupdate(userName);
        setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the demaId fields are not set
        if (!(object instanceof DecisionMakingBase)) {
            return false;
        }
        DecisionMakingBase other = (DecisionMakingBase) object;
        if (this.demaId == null || other.demaId == null) 
            return false;
        
        return this.demaId.equals(other.demaId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
