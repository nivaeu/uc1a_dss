/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
super_class_detail


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class SuperClassDetailBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'sucd_id' είναι υποχρεωτικό")
    @Column(name = "sucd_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="super_class_detail_sucd_id")
    //@SequenceGenerator(name="super_class_detail_sucd_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer sucdId;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @JoinColumn(name = "suca_id", referencedColumnName = "suca_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'suca_id' είναι υποχρεωτικό")
    protected SuperClas sucaId;

    public boolean isSucaIdPresent() {
        if (sucaId==null || sucaId.sucaId == null)
            return false;
        return true;
    }


    @JoinColumn(name = "cult_id", referencedColumnName = "cult_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'cult_id' είναι υποχρεωτικό")
    protected Cultivation cultId;

    public boolean isCultIdPresent() {
        if (cultId==null || cultId.cultId == null)
            return false;
        return true;
    }



    public SuperClassDetailBase() {
    }

    public SuperClassDetailBase(Integer sucdId) {
        this.sucdId = sucdId;
    }

    public SuperClassDetailBase(Integer sucdId, Integer rowVersion) {
        this.sucdId = sucdId;
        this.rowVersion = rowVersion;
    }

    public SuperClassDetailBase(Integer rowVersion, SuperClas sucaId, Cultivation cultId) {
        this.rowVersion = rowVersion;
        this.sucaId = sucaId;
        this.cultId = cultId;
    }

    public SuperClassDetail clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "SuperClassDetail:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (SuperClassDetail)alreadyCloned.get(key);

        SuperClassDetail clone = new SuperClassDetail();
        alreadyCloned.put(key, clone);

        clone.setSucdId(getSucdId());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteinsert(getDteinsert());
        clone.setUsrupdate(getUsrupdate());
        clone.setDteupdate(getDteupdate());
        clone.setRowVersion(getRowVersion());
        if (sucaId == null || sucaId.sucaId == null) {
            clone.setSucaId(sucaId);
        } else {
            clone.setSucaId(getSucaId().clone(alreadyCloned));
        }
        if (cultId == null || cultId.cultId == null) {
            clone.setCultId(cultId);
        } else {
            clone.setCultId(getCultId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "SuperClassDetail:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(sucdId) : sucdId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (sucdId != null) {
            sb.append("\"sucdId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(sucdId) : sucdId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"sucdId\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (sucaId != null && sucaId.sucaId != null) {
            sb.append("\"sucaId\":"); sb.append(getSucaId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (sucaId == null) {
            sb.append("\"sucaId\":"); sb.append("null");  sb.append(",");
        }
        if (cultId != null && cultId.cultId != null) {
            sb.append("\"cultId\":"); sb.append(getCultId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (cultId == null) {
            sb.append("\"cultId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    primary key
    */
    public Integer getSucdId() {
        return sucdId;
    }

    public void setSucdId(Integer sucdId) {
        this.sucdId = sucdId;
    }
    /*
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    fk to super_class
    */
    public SuperClas getSucaId() {
        return sucaId;
    }

    public void setSucaId(SuperClas sucaId) {
    //    if (this.sucaId != null) 
    //        this.sucaId.internalRemoveSuperClassDetailCollection(this);
        this.sucaId = sucaId;
    //    if (sucaId != null)
    //        sucaId.internalAddSuperClassDetailCollection(this);
    }

    /*
    fk to cultivation
    */
    public Cultivation getCultId() {
        return cultId;
    }

    public void setCultId(Cultivation cultId) {
    //    if (this.cultId != null) 
    //        this.cultId.internalRemoveSuperClassDetailCollection(this);
        this.cultId = cultId;
    //    if (cultId != null)
    //        cultId.internalAddSuperClassDetailCollection(this);
    }



    @Override
    public int hashCode() {
        return sucdId.hashCode();
    }

    public String getRowKey() {
        return sucdId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return sucdId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(sucdId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.SuperClassDetail[ sucdId=" + sucdId + " ]";
    }

    @Override
    public String getEntityName() {
        return "SuperClassDetail";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
        setUsrinsert(userName);
        setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
        setUsrupdate(userName);
        setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the sucdId fields are not set
        if (!(object instanceof SuperClassDetailBase)) {
            return false;
        }
        SuperClassDetailBase other = (SuperClassDetailBase) object;
        if (this.sucdId == null || other.sucdId == null) 
            return false;
        
        return this.sucdId.equals(other.sucdId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
