/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
Contains the uploaded FMIS files.


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class GpUploadBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'gp_uploads_id' είναι υποχρεωτικό")
    @Column(name = "gp_uploads_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="gp_uploads_gp_uploads_id")
    //@SequenceGenerator(name="gp_uploads_gp_uploads_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer gpUploadsId;



    @Basic(optional = true)
    @Size(min = 0, max = 2147483647, message="Το πεδίο 'data' πρέπει νά έχει μέγεθος μεταξύ 0 και 2147483647.")
    @Column(name = "data")
    protected String data;



    @Basic(optional = true)
    @Size(min = 0, max = 2147483647, message="Το πεδίο 'environment' πρέπει νά έχει μέγεθος μεταξύ 0 και 2147483647.")
    @Column(name = "environment")
    protected String environment;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'dte_upload' είναι υποχρεωτικό")
    @Column(name = "dte_upload")
    @Temporal(TemporalType.DATE)
    protected Date dteUpload;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'hash' είναι υποχρεωτικό")
    @Size(min = 1, max = 32, message="Το πεδίο 'hash' πρέπει νά έχει μέγεθος μεταξύ 1 και 32.")
    @Column(name = "hash")
    protected String hash;


    @Basic(optional = true, fetch=FetchType.LAZY)
    @Column(name = "image")

    protected byte[] image;


    @JoinColumn(name = "gp_requests_contexts_id", referencedColumnName = "gp_requests_contexts_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'gp_requests_contexts_id' είναι υποχρεωτικό")
    protected GpRequestsContexts gpRequestsContextsId;

    public boolean isGpRequestsContextsIdPresent() {
        if (gpRequestsContextsId==null || gpRequestsContextsId.gpRequestsContextsId == null)
            return false;
        return true;
    }



    public GpUploadBase() {
    }

    public GpUploadBase(Integer gpUploadsId) {
        this.gpUploadsId = gpUploadsId;
    }

    public GpUploadBase(Integer gpUploadsId, String data, String environment, Date dteUpload, String hash, byte[] image) {
        this.gpUploadsId = gpUploadsId;
        this.data = data;
        this.environment = environment;
        this.dteUpload = dteUpload;
        this.hash = hash;
        this.image = image;
    }

    public GpUploadBase(String data, String environment, Date dteUpload, String hash, byte[] image, GpRequestsContexts gpRequestsContextsId) {
        this.data = data;
        this.environment = environment;
        this.dteUpload = dteUpload;
        this.hash = hash;
        this.image = image;
        this.gpRequestsContextsId = gpRequestsContextsId;
    }

    public GpUpload clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "GpUpload:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (GpUpload)alreadyCloned.get(key);

        GpUpload clone = new GpUpload();
        alreadyCloned.put(key, clone);

        clone.setGpUploadsId(getGpUploadsId());
        clone.setData(getData());
        clone.setEnvironment(getEnvironment());
        clone.setDteUpload(getDteUpload());
        clone.setHash(getHash());
        clone.setImage(getImage());
        if (gpRequestsContextsId == null || gpRequestsContextsId.gpRequestsContextsId == null) {
            clone.setGpRequestsContextsId(gpRequestsContextsId);
        } else {
            clone.setGpRequestsContextsId(getGpRequestsContextsId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "GpUpload:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(gpUploadsId) : gpUploadsId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (gpUploadsId != null) {
            sb.append("\"gpUploadsId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(gpUploadsId) : gpUploadsId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"gpUploadsId\":"); sb.append("null");sb.append(",");
        }
        if (data != null) {
            sb.append("\"data\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(data)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"data\":"); sb.append("null");sb.append(",");
        }
        if (environment != null) {
            sb.append("\"environment\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(environment)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"environment\":"); sb.append("null");sb.append(",");
        }
        if (dteUpload != null) {
            sb.append("\"dteUpload\":"); sb.append(dteUpload.getTime()); sb.append(",");
        } else {
            sb.append("\"dteUpload\":"); sb.append("null");sb.append(",");
        }
        if (hash != null) {
            sb.append("\"hash\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(hash)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"hash\":"); sb.append("null");sb.append(",");
        }
        if (gpRequestsContextsId != null && gpRequestsContextsId.gpRequestsContextsId != null) {
            sb.append("\"gpRequestsContextsId\":"); sb.append(getGpRequestsContextsId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (gpRequestsContextsId == null) {
            sb.append("\"gpRequestsContextsId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    */
    public Integer getGpUploadsId() {
        return gpUploadsId;
    }

    public void setGpUploadsId(Integer gpUploadsId) {
        this.gpUploadsId = gpUploadsId;
    }
    /*
    */
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
    /*
    */
    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }
    /*
    */
    public Date getDteUpload() {
        return dteUpload;
    }

    public void setDteUpload(Date dteUpload) {
        this.dteUpload = dteUpload;
    }
    /*
    */
    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
    /*
    */
    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
    /*
    */
    public GpRequestsContexts getGpRequestsContextsId() {
        return gpRequestsContextsId;
    }

    public void setGpRequestsContextsId(GpRequestsContexts gpRequestsContextsId) {
    //    if (this.gpRequestsContextsId != null) 
    //        this.gpRequestsContextsId.internalRemoveGpUploadCollection(this);
        this.gpRequestsContextsId = gpRequestsContextsId;
    //    if (gpRequestsContextsId != null)
    //        gpRequestsContextsId.internalAddGpUploadCollection(this);
    }



    @Override
    public int hashCode() {
        return gpUploadsId.hashCode();
    }

    public String getRowKey() {
        return gpUploadsId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return gpUploadsId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(gpUploadsId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.GpUpload[ gpUploadsId=" + gpUploadsId + " ]";
    }

    @Override
    public String getEntityName() {
        return "GpUpload";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the gpUploadsId fields are not set
        if (!(object instanceof GpUploadBase)) {
            return false;
        }
        GpUploadBase other = (GpUploadBase) object;
        if (this.gpUploadsId == null || other.gpUploadsId == null) 
            return false;
        
        return this.gpUploadsId.equals(other.gpUploadsId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
