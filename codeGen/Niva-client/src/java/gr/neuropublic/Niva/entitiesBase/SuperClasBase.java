/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
super_class


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class SuperClasBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'suca_id' είναι υποχρεωτικό")
    @Column(name = "suca_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="super_class_suca_id")
    //@SequenceGenerator(name="super_class_suca_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer sucaId;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'code' είναι υποχρεωτικό")
    @Size(min = 1, max = 30, message="Το πεδίο 'code' πρέπει νά έχει μέγεθος μεταξύ 1 και 30.")
    @Column(name = "code")
    protected String code;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'name' είναι υποχρεωτικό")
    @Size(min = 1, max = 60, message="Το πεδίο 'name' πρέπει νά έχει μέγεθος μεταξύ 1 και 60.")
    @Column(name = "name")
    protected String name;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "sucaId", orphanRemoval=false)
    @OneToMany(mappedBy = "sucaId")
    protected List<EcCult> ecCultCollection = new ArrayList<EcCult>();


    @OneToMany(cascade = {CascadeType.REMOVE}, mappedBy = "sucaId", orphanRemoval=false)
    protected List<SuperClassDetail> superClassDetailCollection = new ArrayList<SuperClassDetail>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "sucaId", orphanRemoval=false)
    @OneToMany(mappedBy = "sucaId")
    protected List<EcCultCopy> ecCultCopyCollection = new ArrayList<EcCultCopy>();



    public SuperClasBase() {
    }

    public SuperClasBase(Integer sucaId) {
        this.sucaId = sucaId;
    }

    public SuperClasBase(Integer sucaId, String code, String name, Integer rowVersion) {
        this.sucaId = sucaId;
        this.code = code;
        this.name = name;
        this.rowVersion = rowVersion;
    }

    public SuperClasBase(String code, String name, Integer rowVersion) {
        this.code = code;
        this.name = name;
        this.rowVersion = rowVersion;
    }

    public SuperClas clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "SuperClas:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (SuperClas)alreadyCloned.get(key);

        SuperClas clone = new SuperClas();
        alreadyCloned.put(key, clone);

        clone.setSucaId(getSucaId());
        clone.setCode(getCode());
        clone.setName(getName());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteinsert(getDteinsert());
        clone.setUsrupdate(getUsrupdate());
        clone.setDteupdate(getDteupdate());
        clone.setRowVersion(getRowVersion());
        clone.setEcCultCollection(getEcCultCollection());
        clone.setSuperClassDetailCollection(getSuperClassDetailCollection());
        clone.setEcCultCopyCollection(getEcCultCopyCollection());
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "SuperClas:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(sucaId) : sucaId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (sucaId != null) {
            sb.append("\"sucaId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(sucaId) : sucaId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"sucaId\":"); sb.append("null");sb.append(",");
        }
        if (code != null) {
            sb.append("\"code\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(code)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"code\":"); sb.append("null");sb.append(",");
        }
        if (name != null) {
            sb.append("\"name\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(name)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"name\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    primary key
    */
    public Integer getSucaId() {
        return sucaId;
    }

    public void setSucaId(Integer sucaId) {
        this.sucaId = sucaId;
    }
    /*
    code super class
    */
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    /*
    name of super class
    */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    /*
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    @XmlTransient
    public List<EcCult> getEcCultCollection() {
        return ecCultCollection;
    }

    public void setEcCultCollection(List<EcCult> ecCultCollection) {
        this.ecCultCollection = ecCultCollection;
    }

    public void removeEcCultCollection(EcCult child) {
        if (child != null)
            child.setSucaId(null);
    }

    public void addEcCultCollection(EcCult child) {
        if (child != null)
            child.setSucaId((SuperClas)this);
    }

    void internalRemoveEcCultCollection(EcCult child) {
        if (child != null)
            this.ecCultCollection.remove(child);
    }

    void internalAddEcCultCollection(EcCult child) {
        if (child != null)
            this.ecCultCollection.add(child);
    }

    @XmlTransient
    public List<SuperClassDetail> getSuperClassDetailCollection() {
        return superClassDetailCollection;
    }

    public void setSuperClassDetailCollection(List<SuperClassDetail> superClassDetailCollection) {
        this.superClassDetailCollection = superClassDetailCollection;
    }

    public void removeSuperClassDetailCollection(SuperClassDetail child) {
        if (child != null)
            child.setSucaId(null);
    }

    public void addSuperClassDetailCollection(SuperClassDetail child) {
        if (child != null)
            child.setSucaId((SuperClas)this);
    }

    void internalRemoveSuperClassDetailCollection(SuperClassDetail child) {
        if (child != null)
            this.superClassDetailCollection.remove(child);
    }

    void internalAddSuperClassDetailCollection(SuperClassDetail child) {
        if (child != null)
            this.superClassDetailCollection.add(child);
    }

    @XmlTransient
    public List<EcCultCopy> getEcCultCopyCollection() {
        return ecCultCopyCollection;
    }

    public void setEcCultCopyCollection(List<EcCultCopy> ecCultCopyCollection) {
        this.ecCultCopyCollection = ecCultCopyCollection;
    }

    public void removeEcCultCopyCollection(EcCultCopy child) {
        if (child != null)
            child.setSucaId(null);
    }

    public void addEcCultCopyCollection(EcCultCopy child) {
        if (child != null)
            child.setSucaId((SuperClas)this);
    }

    void internalRemoveEcCultCopyCollection(EcCultCopy child) {
        if (child != null)
            this.ecCultCopyCollection.remove(child);
    }

    void internalAddEcCultCopyCollection(EcCultCopy child) {
        if (child != null)
            this.ecCultCopyCollection.add(child);
    }



    @Override
    public int hashCode() {
        return sucaId.hashCode();
    }

    public String getRowKey() {
        return sucaId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return sucaId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(sucaId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.SuperClas[ sucaId=" + sucaId + " ]";
    }

    @Override
    public String getEntityName() {
        return "SuperClas";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
        setUsrinsert(userName);
        setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
        setUsrupdate(userName);
        setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the sucaId fields are not set
        if (!(object instanceof SuperClasBase)) {
            return false;
        }
        SuperClasBase other = (SuperClasBase) object;
        if (this.sucaId == null || other.sucaId == null) 
            return false;
        
        return this.sucaId.equals(other.sucaId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
