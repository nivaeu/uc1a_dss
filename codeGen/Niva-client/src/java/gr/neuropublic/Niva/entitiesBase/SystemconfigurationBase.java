/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
Contains parameters about the configuration, mainly according to the selected Agency.


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class SystemconfigurationBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'agency_id' είναι υποχρεωτικό")
    @Column(name = "agency_id")
    protected Integer agencyId;


    @Basic(optional = true)
    @Column(name = "srid")
    protected Short srid;



    @Basic(optional = true)
    @Size(min = 0, max = 2, message="Το πεδίο 'language' πρέπει νά έχει μέγεθος μεταξύ 0 και 2.")
    @Column(name = "language")
    protected String language;



    public SystemconfigurationBase() {
    }

    public SystemconfigurationBase(Integer agencyId) {
        this.agencyId = agencyId;
    }

    public SystemconfigurationBase(Integer agencyId, Short srid, String language) {
        this.agencyId = agencyId;
        this.srid = srid;
        this.language = language;
    }

    public Systemconfiguration clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "Systemconfiguration:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (Systemconfiguration)alreadyCloned.get(key);

        Systemconfiguration clone = new Systemconfiguration();
        alreadyCloned.put(key, clone);

        clone.setAgencyId(getAgencyId());
        clone.setSrid(getSrid());
        clone.setLanguage(getLanguage());
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "Systemconfiguration:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(agencyId) : agencyId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (agencyId != null) {
            sb.append("\"agencyId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(agencyId) : agencyId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"agencyId\":"); sb.append("null");sb.append(",");
        }
        if (srid != null) {
            sb.append("\"srid\":"); sb.append(srid);sb.append(",");
        } else {
            sb.append("\"srid\":"); sb.append("null");sb.append(",");
        }
        if (language != null) {
            sb.append("\"language\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(language)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"language\":"); sb.append("null");sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    */
    public Integer getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(Integer agencyId) {
        this.agencyId = agencyId;
    }
    /*
    */
    public Short getSrid() {
        return srid;
    }

    public void setSrid(Short srid) {
        this.srid = srid;
    }
    /*
    */
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }


    @Override
    public int hashCode() {
        return agencyId.hashCode();
    }

    public String getRowKey() {
        return agencyId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return agencyId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(agencyId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.Systemconfiguration[ agencyId=" + agencyId + " ]";
    }

    @Override
    public String getEntityName() {
        return "Systemconfiguration";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the agencyId fields are not set
        if (!(object instanceof SystemconfigurationBase)) {
            return false;
        }
        SystemconfigurationBase other = (SystemconfigurationBase) object;
        if (this.agencyId == null || other.agencyId == null) 
            return false;
        
        return this.agencyId.equals(other.agencyId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
