/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class FmisUploadBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'fmis_uploads_id' είναι υποχρεωτικό")
    @Column(name = "fmis_uploads_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="fmis_uploads_fmis_uploads_id")
    //@SequenceGenerator(name="fmis_uploads_fmis_uploads_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer fmisUploadsId;



    @Basic(optional = true)
    @Size(min = 0, max = 2147483647, message="Το πεδίο 'metadata' πρέπει νά έχει μέγεθος μεταξύ 0 και 2147483647.")
    @Column(name = "metadata")
    protected String metadata;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'dte_upload' είναι υποχρεωτικό")
    @Column(name = "dte_upload")
    @Temporal(TemporalType.DATE)
    protected Date dteUpload;


    @Basic(optional = true, fetch=FetchType.LAZY)
    @Column(name = "docfile")

    protected byte[] docfile;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usr_upload' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usr_upload")
    protected String usrUpload;


    @JoinColumn(name = "parcels_issues_id", referencedColumnName = "parcels_issues_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'parcels_issues_id' είναι υποχρεωτικό")
    protected ParcelsIssues parcelsIssuesId;

    public boolean isParcelsIssuesIdPresent() {
        if (parcelsIssuesId==null || parcelsIssuesId.parcelsIssuesId == null)
            return false;
        return true;
    }



    public FmisUploadBase() {
    }

    public FmisUploadBase(Integer fmisUploadsId) {
        this.fmisUploadsId = fmisUploadsId;
    }

    public FmisUploadBase(Integer fmisUploadsId, String metadata, Date dteUpload, byte[] docfile, String usrUpload) {
        this.fmisUploadsId = fmisUploadsId;
        this.metadata = metadata;
        this.dteUpload = dteUpload;
        this.docfile = docfile;
        this.usrUpload = usrUpload;
    }

    public FmisUploadBase(String metadata, Date dteUpload, byte[] docfile, String usrUpload, ParcelsIssues parcelsIssuesId) {
        this.metadata = metadata;
        this.dteUpload = dteUpload;
        this.docfile = docfile;
        this.usrUpload = usrUpload;
        this.parcelsIssuesId = parcelsIssuesId;
    }

    public FmisUpload clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "FmisUpload:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (FmisUpload)alreadyCloned.get(key);

        FmisUpload clone = new FmisUpload();
        alreadyCloned.put(key, clone);

        clone.setFmisUploadsId(getFmisUploadsId());
        clone.setMetadata(getMetadata());
        clone.setDteUpload(getDteUpload());
        clone.setDocfile(getDocfile());
        clone.setUsrUpload(getUsrUpload());
        if (parcelsIssuesId == null || parcelsIssuesId.parcelsIssuesId == null) {
            clone.setParcelsIssuesId(parcelsIssuesId);
        } else {
            clone.setParcelsIssuesId(getParcelsIssuesId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "FmisUpload:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(fmisUploadsId) : fmisUploadsId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (fmisUploadsId != null) {
            sb.append("\"fmisUploadsId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(fmisUploadsId) : fmisUploadsId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"fmisUploadsId\":"); sb.append("null");sb.append(",");
        }
        if (metadata != null) {
            sb.append("\"metadata\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(metadata)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"metadata\":"); sb.append("null");sb.append(",");
        }
        if (dteUpload != null) {
            sb.append("\"dteUpload\":"); sb.append(dteUpload.getTime()); sb.append(",");
        } else {
            sb.append("\"dteUpload\":"); sb.append("null");sb.append(",");
        }
        if (usrUpload != null) {
            sb.append("\"usrUpload\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(usrUpload)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"usrUpload\":"); sb.append("null");sb.append(",");
        }
        if (parcelsIssuesId != null && parcelsIssuesId.parcelsIssuesId != null) {
            sb.append("\"parcelsIssuesId\":"); sb.append(getParcelsIssuesId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (parcelsIssuesId == null) {
            sb.append("\"parcelsIssuesId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    */
    public Integer getFmisUploadsId() {
        return fmisUploadsId;
    }

    public void setFmisUploadsId(Integer fmisUploadsId) {
        this.fmisUploadsId = fmisUploadsId;
    }
    /*
    */
    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }
    /*
    */
    public Date getDteUpload() {
        return dteUpload;
    }

    public void setDteUpload(Date dteUpload) {
        this.dteUpload = dteUpload;
    }
    /*
    */
    public byte[] getDocfile() {
        return docfile;
    }

    public void setDocfile(byte[] docfile) {
        this.docfile = docfile;
    }
    /*
    */
    public String getUsrUpload() {
        return usrUpload;
    }

    public void setUsrUpload(String usrUpload) {
        this.usrUpload = usrUpload;
    }
    /*
    */
    public ParcelsIssues getParcelsIssuesId() {
        return parcelsIssuesId;
    }

    public void setParcelsIssuesId(ParcelsIssues parcelsIssuesId) {
    //    if (this.parcelsIssuesId != null) 
    //        this.parcelsIssuesId.internalRemoveFmisUploadCollection(this);
        this.parcelsIssuesId = parcelsIssuesId;
    //    if (parcelsIssuesId != null)
    //        parcelsIssuesId.internalAddFmisUploadCollection(this);
    }



    @Override
    public int hashCode() {
        return fmisUploadsId.hashCode();
    }

    public String getRowKey() {
        return fmisUploadsId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return fmisUploadsId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(fmisUploadsId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.FmisUpload[ fmisUploadsId=" + fmisUploadsId + " ]";
    }

    @Override
    public String getEntityName() {
        return "FmisUpload";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the fmisUploadsId fields are not set
        if (!(object instanceof FmisUploadBase)) {
            return false;
        }
        FmisUploadBase other = (FmisUploadBase) object;
        if (this.fmisUploadsId == null || other.fmisUploadsId == null) 
            return false;
        
        return this.fmisUploadsId.equals(other.fmisUploadsId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
