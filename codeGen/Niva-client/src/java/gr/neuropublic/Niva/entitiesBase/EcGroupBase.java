/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
eligibility Criteria group


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
@NamedQuery(name = "EcGroup.findEcGroup", query = "SELECT x FROM EcGroup x WHERE (x.description like :description) AND (x.name like :name) ")})
public abstract class EcGroupBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'ecgr_id' είναι υποχρεωτικό")
    @Column(name = "ecgr_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ec_group_ecgr_id")
    //@SequenceGenerator(name="ec_group_ecgr_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer ecgrId;



    @Basic(optional = true)
    @Size(min = 0, max = 60, message="Το πεδίο 'description' πρέπει νά έχει μέγεθος μεταξύ 0 και 60.")
    @Column(name = "description")
    protected String description;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'name' είναι υποχρεωτικό")
    @Size(min = 1, max = 60, message="Το πεδίο 'name' πρέπει νά έχει μέγεθος μεταξύ 1 και 60.")
    @Column(name = "name")
    protected String name;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'recordtype' είναι υποχρεωτικό")
    @Column(name = "recordtype")
    protected Integer recordtype;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'crop_level' είναι υποχρεωτικό")
    @Column(name = "crop_level")
    protected Short cropLevel;


    @OneToMany(cascade = {CascadeType.REMOVE}, mappedBy = "ecgrId", orphanRemoval=false)
    protected List<EcCult> ecCultCollection = new ArrayList<EcCult>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "ecgrId", orphanRemoval=false)
    @OneToMany(mappedBy = "ecgrId")
    protected List<DecisionMaking> decisionMakingCollection = new ArrayList<DecisionMaking>();



    public EcGroupBase() {
    }

    public EcGroupBase(Integer ecgrId) {
        this.ecgrId = ecgrId;
    }

    public EcGroupBase(Integer ecgrId, String description, String name, Integer rowVersion, Integer recordtype, Short cropLevel) {
        this.ecgrId = ecgrId;
        this.description = description;
        this.name = name;
        this.rowVersion = rowVersion;
        this.recordtype = recordtype;
        this.cropLevel = cropLevel;
    }

    public EcGroupBase(String description, String name, Integer rowVersion, Integer recordtype, Short cropLevel) {
        this.description = description;
        this.name = name;
        this.rowVersion = rowVersion;
        this.recordtype = recordtype;
        this.cropLevel = cropLevel;
    }

    public EcGroup clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "EcGroup:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (EcGroup)alreadyCloned.get(key);

        EcGroup clone = new EcGroup();
        alreadyCloned.put(key, clone);

        clone.setEcgrId(getEcgrId());
        clone.setDescription(getDescription());
        clone.setName(getName());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteinsert(getDteinsert());
        clone.setUsrupdate(getUsrupdate());
        clone.setDteupdate(getDteupdate());
        clone.setRowVersion(getRowVersion());
        clone.setRecordtype(getRecordtype());
        clone.setCropLevel(getCropLevel());
        clone.setEcCultCollection(getEcCultCollection());
        clone.setDecisionMakingCollection(getDecisionMakingCollection());
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "EcGroup:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(ecgrId) : ecgrId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (ecgrId != null) {
            sb.append("\"ecgrId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(ecgrId) : ecgrId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"ecgrId\":"); sb.append("null");sb.append(",");
        }
        if (description != null) {
            sb.append("\"description\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(description)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"description\":"); sb.append("null");sb.append(",");
        }
        if (name != null) {
            sb.append("\"name\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(name)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"name\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (recordtype != null) {
            sb.append("\"recordtype\":"); sb.append(recordtype);sb.append(",");
        } else {
            sb.append("\"recordtype\":"); sb.append("null");sb.append(",");
        }
        if (cropLevel != null) {
            sb.append("\"cropLevel\":"); sb.append(cropLevel);sb.append(",");
        } else {
            sb.append("\"cropLevel\":"); sb.append("null");sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    primary key
    */
    public Integer getEcgrId() {
        return ecgrId;
    }

    public void setEcgrId(Integer ecgrId) {
        this.ecgrId = ecgrId;
    }
    /*
    */
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    /*
    ec_group name
    */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    /*
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    */
    public Integer getRecordtype() {
        return recordtype;
    }

    public void setRecordtype(Integer recordtype) {
        this.recordtype = recordtype;
    }
    /*
    0: Cultivation, 1: Crop Cover(General type)
    */
    public Short getCropLevel() {
        return cropLevel;
    }

    public void setCropLevel(Short cropLevel) {
        this.cropLevel = cropLevel;
    }
    @XmlTransient
    public List<EcCult> getEcCultCollection() {
        return ecCultCollection;
    }

    public void setEcCultCollection(List<EcCult> ecCultCollection) {
        this.ecCultCollection = ecCultCollection;
    }

    public void removeEcCultCollection(EcCult child) {
        if (child != null)
            child.setEcgrId(null);
    }

    public void addEcCultCollection(EcCult child) {
        if (child != null)
            child.setEcgrId((EcGroup)this);
    }

    void internalRemoveEcCultCollection(EcCult child) {
        if (child != null)
            this.ecCultCollection.remove(child);
    }

    void internalAddEcCultCollection(EcCult child) {
        if (child != null)
            this.ecCultCollection.add(child);
    }

    @XmlTransient
    public List<DecisionMaking> getDecisionMakingCollection() {
        return decisionMakingCollection;
    }

    public void setDecisionMakingCollection(List<DecisionMaking> decisionMakingCollection) {
        this.decisionMakingCollection = decisionMakingCollection;
    }

    public void removeDecisionMakingCollection(DecisionMaking child) {
        if (child != null)
            child.setEcgrId(null);
    }

    public void addDecisionMakingCollection(DecisionMaking child) {
        if (child != null)
            child.setEcgrId((EcGroup)this);
    }

    void internalRemoveDecisionMakingCollection(DecisionMaking child) {
        if (child != null)
            this.decisionMakingCollection.remove(child);
    }

    void internalAddDecisionMakingCollection(DecisionMaking child) {
        if (child != null)
            this.decisionMakingCollection.add(child);
    }



    @Override
    public int hashCode() {
        return ecgrId.hashCode();
    }

    public String getRowKey() {
        return ecgrId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return ecgrId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(ecgrId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.EcGroup[ ecgrId=" + ecgrId + " ]";
    }

    @Override
    public String getEntityName() {
        return "EcGroup";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
        setUsrinsert(userName);
        setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
        setUsrupdate(userName);
        setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the ecgrId fields are not set
        if (!(object instanceof EcGroupBase)) {
            return false;
        }
        EcGroupBase other = (EcGroupBase) object;
        if (this.ecgrId == null || other.ecgrId == null) 
            return false;
        
        return this.ecgrId.equals(other.ecgrId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
