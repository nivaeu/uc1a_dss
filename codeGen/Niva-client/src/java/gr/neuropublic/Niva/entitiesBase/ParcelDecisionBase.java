/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
parcel_class decision


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class ParcelDecisionBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'pade_id' είναι υποχρεωτικό")
    @Column(name = "pade_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="parcel_decision_pade_id")
    //@SequenceGenerator(name="parcel_decision_pade_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer padeId;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'decision_light' είναι υποχρεωτικό")
    @Column(name = "decision_light")
    protected Integer decisionLight;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @JoinColumn(name = "dema_id", referencedColumnName = "dema_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'dema_id' είναι υποχρεωτικό")
    protected DecisionMaking demaId;

    public boolean isDemaIdPresent() {
        if (demaId==null || demaId.demaId == null)
            return false;
        return true;
    }


    @JoinColumn(name = "pcla_id", referencedColumnName = "pcla_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'pcla_id' είναι υποχρεωτικό")
    protected ParcelClas pclaId;

    public boolean isPclaIdPresent() {
        if (pclaId==null || pclaId.pclaId == null)
            return false;
        return true;
    }



    public ParcelDecisionBase() {
    }

    public ParcelDecisionBase(Integer padeId) {
        this.padeId = padeId;
    }

    public ParcelDecisionBase(Integer padeId, Integer decisionLight, Integer rowVersion) {
        this.padeId = padeId;
        this.decisionLight = decisionLight;
        this.rowVersion = rowVersion;
    }

    public ParcelDecisionBase(Integer decisionLight, Integer rowVersion, DecisionMaking demaId, ParcelClas pclaId) {
        this.decisionLight = decisionLight;
        this.rowVersion = rowVersion;
        this.demaId = demaId;
        this.pclaId = pclaId;
    }

    public ParcelDecision clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "ParcelDecision:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (ParcelDecision)alreadyCloned.get(key);

        ParcelDecision clone = new ParcelDecision();
        alreadyCloned.put(key, clone);

        clone.setPadeId(getPadeId());
        clone.setDecisionLight(getDecisionLight());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteinsert(getDteinsert());
        clone.setUsrupdate(getUsrupdate());
        clone.setDteupdate(getDteupdate());
        clone.setRowVersion(getRowVersion());
        if (demaId == null || demaId.demaId == null) {
            clone.setDemaId(demaId);
        } else {
            clone.setDemaId(getDemaId().clone(alreadyCloned));
        }
        if (pclaId == null || pclaId.pclaId == null) {
            clone.setPclaId(pclaId);
        } else {
            clone.setPclaId(getPclaId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "ParcelDecision:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(padeId) : padeId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (padeId != null) {
            sb.append("\"padeId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(padeId) : padeId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"padeId\":"); sb.append("null");sb.append(",");
        }
        if (decisionLight != null) {
            sb.append("\"decisionLight\":"); sb.append(decisionLight);sb.append(",");
        } else {
            sb.append("\"decisionLight\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (demaId != null && demaId.demaId != null) {
            sb.append("\"demaId\":"); sb.append(getDemaId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (demaId == null) {
            sb.append("\"demaId\":"); sb.append("null");  sb.append(",");
        }
        if (pclaId != null && pclaId.pclaId != null) {
            sb.append("\"pclaId\":"); sb.append(getPclaId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (pclaId == null) {
            sb.append("\"pclaId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    primary key
    */
    public Integer getPadeId() {
        return padeId;
    }

    public void setPadeId(Integer padeId) {
        this.padeId = padeId;
    }
    /*
    1: green, 2: yellow, 3:red
    */
    public Integer getDecisionLight() {
        return decisionLight;
    }

    public void setDecisionLight(Integer decisionLight) {
        this.decisionLight = decisionLight;
    }
    /*
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    fk to decision_making
    */
    public DecisionMaking getDemaId() {
        return demaId;
    }

    public void setDemaId(DecisionMaking demaId) {
    //    if (this.demaId != null) 
    //        this.demaId.internalRemoveParcelDecisionCollection(this);
        this.demaId = demaId;
    //    if (demaId != null)
    //        demaId.internalAddParcelDecisionCollection(this);
    }

    /*
    fk to parcel_class
    */
    public ParcelClas getPclaId() {
        return pclaId;
    }

    public void setPclaId(ParcelClas pclaId) {
    //    if (this.pclaId != null) 
    //        this.pclaId.internalRemoveParcelDecisionCollection(this);
        this.pclaId = pclaId;
    //    if (pclaId != null)
    //        pclaId.internalAddParcelDecisionCollection(this);
    }



    @Override
    public int hashCode() {
        return padeId.hashCode();
    }

    public String getRowKey() {
        return padeId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return padeId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(padeId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.ParcelDecision[ padeId=" + padeId + " ]";
    }

    @Override
    public String getEntityName() {
        return "ParcelDecision";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
        setUsrinsert(userName);
        setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
        setUsrupdate(userName);
        setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the padeId fields are not set
        if (!(object instanceof ParcelDecisionBase)) {
            return false;
        }
        ParcelDecisionBase other = (ParcelDecisionBase) object;
        if (this.padeId == null || other.padeId == null) 
            return false;
        
        return this.padeId.equals(other.padeId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
