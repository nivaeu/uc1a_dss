/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
excel files


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class ExcelFileBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'id' είναι υποχρεωτικό")
    @Column(name = "id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="excel_files_id")
    //@SequenceGenerator(name="excel_files_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer id;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'filename' είναι υποχρεωτικό")
    @Size(min = 1, max = 120, message="Το πεδίο 'filename' πρέπει νά έχει μέγεθος μεταξύ 1 και 120.")
    @Column(name = "filename")
    protected String filename;


    @Basic(optional = true, fetch=FetchType.LAZY)
    @Column(name = "data")

    protected byte[] data;


    @Basic(optional = true)
    @Column(name = "total_rows")
    protected Integer totalRows;


    @Basic(optional = true)
    @Column(name = "total_error_rows")
    protected Integer totalErrorRows;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "exfiId", orphanRemoval=false)
    @OneToMany(mappedBy = "exfiId")
    protected List<CoverType> coverTypeCollection = new ArrayList<CoverType>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "exfiId", orphanRemoval=false)
    @OneToMany(mappedBy = "exfiId")
    protected List<Cultivation> cultivationCollection = new ArrayList<Cultivation>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "exfiId", orphanRemoval=false)
    @OneToMany(mappedBy = "exfiId")
    protected List<ExcelError> excelErrorCollection = new ArrayList<ExcelError>();



    public ExcelFileBase() {
    }

    public ExcelFileBase(Integer id) {
        this.id = id;
    }

    public ExcelFileBase(Integer id, String filename, byte[] data, Integer totalRows, Integer totalErrorRows, Integer rowVersion) {
        this.id = id;
        this.filename = filename;
        this.data = data;
        this.totalRows = totalRows;
        this.totalErrorRows = totalErrorRows;
        this.rowVersion = rowVersion;
    }

    public ExcelFileBase(String filename, byte[] data, Integer totalRows, Integer totalErrorRows, Integer rowVersion) {
        this.filename = filename;
        this.data = data;
        this.totalRows = totalRows;
        this.totalErrorRows = totalErrorRows;
        this.rowVersion = rowVersion;
    }

    public ExcelFile clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "ExcelFile:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (ExcelFile)alreadyCloned.get(key);

        ExcelFile clone = new ExcelFile();
        alreadyCloned.put(key, clone);

        clone.setId(getId());
        clone.setFilename(getFilename());
        clone.setData(getData());
        clone.setTotalRows(getTotalRows());
        clone.setTotalErrorRows(getTotalErrorRows());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteinsert(getDteinsert());
        clone.setUsrupdate(getUsrupdate());
        clone.setDteupdate(getDteupdate());
        clone.setRowVersion(getRowVersion());
        clone.setCoverTypeCollection(getCoverTypeCollection());
        clone.setCultivationCollection(getCultivationCollection());
        clone.setExcelErrorCollection(getExcelErrorCollection());
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "ExcelFile:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(id) : id); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (id != null) {
            sb.append("\"id\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(id) : id); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"id\":"); sb.append("null");sb.append(",");
        }
        if (filename != null) {
            sb.append("\"filename\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(filename)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"filename\":"); sb.append("null");sb.append(",");
        }
        if (totalRows != null) {
            sb.append("\"totalRows\":"); sb.append(totalRows);sb.append(",");
        } else {
            sb.append("\"totalRows\":"); sb.append("null");sb.append(",");
        }
        if (totalErrorRows != null) {
            sb.append("\"totalErrorRows\":"); sb.append(totalErrorRows);sb.append(",");
        } else {
            sb.append("\"totalErrorRows\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    primary key
    */
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    /*
    name of file
    */
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
    /*
    upload file
    */
    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
    /*
    total rows
    */
    public Integer getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(Integer totalRows) {
        this.totalRows = totalRows;
    }
    /*
    total rows
    */
    public Integer getTotalErrorRows() {
        return totalErrorRows;
    }

    public void setTotalErrorRows(Integer totalErrorRows) {
        this.totalErrorRows = totalErrorRows;
    }
    /*
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    @XmlTransient
    public List<CoverType> getCoverTypeCollection() {
        return coverTypeCollection;
    }

    public void setCoverTypeCollection(List<CoverType> coverTypeCollection) {
        this.coverTypeCollection = coverTypeCollection;
    }

    public void removeCoverTypeCollection(CoverType child) {
        if (child != null)
            child.setExfiId(null);
    }

    public void addCoverTypeCollection(CoverType child) {
        if (child != null)
            child.setExfiId((ExcelFile)this);
    }

    void internalRemoveCoverTypeCollection(CoverType child) {
        if (child != null)
            this.coverTypeCollection.remove(child);
    }

    void internalAddCoverTypeCollection(CoverType child) {
        if (child != null)
            this.coverTypeCollection.add(child);
    }

    @XmlTransient
    public List<Cultivation> getCultivationCollection() {
        return cultivationCollection;
    }

    public void setCultivationCollection(List<Cultivation> cultivationCollection) {
        this.cultivationCollection = cultivationCollection;
    }

    public void removeCultivationCollection(Cultivation child) {
        if (child != null)
            child.setExfiId(null);
    }

    public void addCultivationCollection(Cultivation child) {
        if (child != null)
            child.setExfiId((ExcelFile)this);
    }

    void internalRemoveCultivationCollection(Cultivation child) {
        if (child != null)
            this.cultivationCollection.remove(child);
    }

    void internalAddCultivationCollection(Cultivation child) {
        if (child != null)
            this.cultivationCollection.add(child);
    }

    @XmlTransient
    public List<ExcelError> getExcelErrorCollection() {
        return excelErrorCollection;
    }

    public void setExcelErrorCollection(List<ExcelError> excelErrorCollection) {
        this.excelErrorCollection = excelErrorCollection;
    }

    public void removeExcelErrorCollection(ExcelError child) {
        if (child != null)
            child.setExfiId(null);
    }

    public void addExcelErrorCollection(ExcelError child) {
        if (child != null)
            child.setExfiId((ExcelFile)this);
    }

    void internalRemoveExcelErrorCollection(ExcelError child) {
        if (child != null)
            this.excelErrorCollection.remove(child);
    }

    void internalAddExcelErrorCollection(ExcelError child) {
        if (child != null)
            this.excelErrorCollection.add(child);
    }



    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public String getRowKey() {
        return id.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return id;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(id);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.ExcelFile[ id=" + id + " ]";
    }

    @Override
    public String getEntityName() {
        return "ExcelFile";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
        setUsrinsert(userName);
        setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
        setUsrupdate(userName);
        setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExcelFileBase)) {
            return false;
        }
        ExcelFileBase other = (ExcelFileBase) object;
        if (this.id == null || other.id == null) 
            return false;
        
        return this.id.equals(other.id);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
