/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
cultivation


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class CultivationBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'cult_id' είναι υποχρεωτικό")
    @Column(name = "cult_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="cultivation_cult_id")
    //@SequenceGenerator(name="cultivation_cult_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer cultId;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'name' είναι υποχρεωτικό")
    @Size(min = 1, max = 200, message="Το πεδίο 'name' πρέπει νά έχει μέγεθος μεταξύ 1 και 200.")
    @Column(name = "name")
    protected String name;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'code' είναι υποχρεωτικό")
    @Column(name = "code")
    protected Integer code;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "cultId", orphanRemoval=false)
    @OneToMany(mappedBy = "cultId")
    protected List<EcCult> ecCultCollection = new ArrayList<EcCult>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "cultId", orphanRemoval=false)
    @OneToMany(mappedBy = "cultId")
    protected List<FmisDecision> fmisDecisionCollection = new ArrayList<FmisDecision>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "cultId", orphanRemoval=false)
    @OneToMany(mappedBy = "cultId")
    protected List<SuperClassDetail> superClassDetailCollection = new ArrayList<SuperClassDetail>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "cultId", orphanRemoval=false)
    @OneToMany(mappedBy = "cultId")
    protected List<GpDecision> gpDecisionCollection = new ArrayList<GpDecision>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "cultId", orphanRemoval=false)
    @OneToMany(mappedBy = "cultId")
    protected List<EcCultCopy> ecCultCopyCollection = new ArrayList<EcCultCopy>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "cultIdDecl", orphanRemoval=false)
    @OneToMany(mappedBy = "cultIdDecl")
    protected List<ParcelClas> parcelClascult_id_declCollection = new ArrayList<ParcelClas>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "cultIdPred", orphanRemoval=false)
    @OneToMany(mappedBy = "cultIdPred")
    protected List<ParcelClas> parcelClascult_id_predCollection = new ArrayList<ParcelClas>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "cultIdPred2", orphanRemoval=false)
    @OneToMany(mappedBy = "cultIdPred2")
    protected List<ParcelClas> parcelClascult_id_pred2Collection = new ArrayList<ParcelClas>();


    @JoinColumn(name = "coty_id", referencedColumnName = "coty_id")
    @ManyToOne(optional = true, fetch= FetchType.LAZY)
    protected CoverType cotyId;

    public boolean isCotyIdPresent() {
        if (cotyId==null || cotyId.cotyId == null)
            return false;
        return true;
    }


    @JoinColumn(name = "exfi_id", referencedColumnName = "id")
    @ManyToOne(optional = true, fetch= FetchType.LAZY)
    protected ExcelFile exfiId;

    public boolean isExfiIdPresent() {
        if (exfiId==null || exfiId.id == null)
            return false;
        return true;
    }



    public CultivationBase() {
    }

    public CultivationBase(Integer cultId) {
        this.cultId = cultId;
    }

    public CultivationBase(Integer cultId, String name, Integer code, Integer rowVersion) {
        this.cultId = cultId;
        this.name = name;
        this.code = code;
        this.rowVersion = rowVersion;
    }

    public CultivationBase(String name, Integer code, Integer rowVersion, CoverType cotyId, ExcelFile exfiId) {
        this.name = name;
        this.code = code;
        this.rowVersion = rowVersion;
        this.cotyId = cotyId;
        this.exfiId = exfiId;
    }

    public Cultivation clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "Cultivation:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (Cultivation)alreadyCloned.get(key);

        Cultivation clone = new Cultivation();
        alreadyCloned.put(key, clone);

        clone.setCultId(getCultId());
        clone.setName(getName());
        clone.setCode(getCode());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteinsert(getDteinsert());
        clone.setUsrupdate(getUsrupdate());
        clone.setDteupdate(getDteupdate());
        clone.setRowVersion(getRowVersion());
        clone.setEcCultCollection(getEcCultCollection());
        clone.setFmisDecisionCollection(getFmisDecisionCollection());
        clone.setSuperClassDetailCollection(getSuperClassDetailCollection());
        clone.setGpDecisionCollection(getGpDecisionCollection());
        clone.setEcCultCopyCollection(getEcCultCopyCollection());
        clone.setParcelClascult_id_declCollection(getParcelClascult_id_declCollection());
        clone.setParcelClascult_id_predCollection(getParcelClascult_id_predCollection());
        clone.setParcelClascult_id_pred2Collection(getParcelClascult_id_pred2Collection());
        if (cotyId == null || cotyId.cotyId == null) {
            clone.setCotyId(cotyId);
        } else {
            clone.setCotyId(getCotyId().clone(alreadyCloned));
        }
        if (exfiId == null || exfiId.id == null) {
            clone.setExfiId(exfiId);
        } else {
            clone.setExfiId(getExfiId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "Cultivation:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(cultId) : cultId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (cultId != null) {
            sb.append("\"cultId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(cultId) : cultId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"cultId\":"); sb.append("null");sb.append(",");
        }
        if (name != null) {
            sb.append("\"name\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(name)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"name\":"); sb.append("null");sb.append(",");
        }
        if (code != null) {
            sb.append("\"code\":"); sb.append(code);sb.append(",");
        } else {
            sb.append("\"code\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (cotyId != null && cotyId.cotyId != null) {
            sb.append("\"cotyId\":"); sb.append(getCotyId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (cotyId == null) {
            sb.append("\"cotyId\":"); sb.append("null");  sb.append(",");
        }
        if (exfiId != null && exfiId.id != null) {
            sb.append("\"exfiId\":"); sb.append(getExfiId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (exfiId == null) {
            sb.append("\"exfiId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    primary key
    */
    public Integer getCultId() {
        return cultId;
    }

    public void setCultId(Integer cultId) {
        this.cultId = cultId;
    }
    /*
    cultivation name
    */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    /*
    code of cultivation
    */
    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
    /*
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    @XmlTransient
    public List<EcCult> getEcCultCollection() {
        return ecCultCollection;
    }

    public void setEcCultCollection(List<EcCult> ecCultCollection) {
        this.ecCultCollection = ecCultCollection;
    }

    public void removeEcCultCollection(EcCult child) {
        if (child != null)
            child.setCultId(null);
    }

    public void addEcCultCollection(EcCult child) {
        if (child != null)
            child.setCultId((Cultivation)this);
    }

    void internalRemoveEcCultCollection(EcCult child) {
        if (child != null)
            this.ecCultCollection.remove(child);
    }

    void internalAddEcCultCollection(EcCult child) {
        if (child != null)
            this.ecCultCollection.add(child);
    }

    @XmlTransient
    public List<FmisDecision> getFmisDecisionCollection() {
        return fmisDecisionCollection;
    }

    public void setFmisDecisionCollection(List<FmisDecision> fmisDecisionCollection) {
        this.fmisDecisionCollection = fmisDecisionCollection;
    }

    public void removeFmisDecisionCollection(FmisDecision child) {
        if (child != null)
            child.setCultId(null);
    }

    public void addFmisDecisionCollection(FmisDecision child) {
        if (child != null)
            child.setCultId((Cultivation)this);
    }

    void internalRemoveFmisDecisionCollection(FmisDecision child) {
        if (child != null)
            this.fmisDecisionCollection.remove(child);
    }

    void internalAddFmisDecisionCollection(FmisDecision child) {
        if (child != null)
            this.fmisDecisionCollection.add(child);
    }

    @XmlTransient
    public List<SuperClassDetail> getSuperClassDetailCollection() {
        return superClassDetailCollection;
    }

    public void setSuperClassDetailCollection(List<SuperClassDetail> superClassDetailCollection) {
        this.superClassDetailCollection = superClassDetailCollection;
    }

    public void removeSuperClassDetailCollection(SuperClassDetail child) {
        if (child != null)
            child.setCultId(null);
    }

    public void addSuperClassDetailCollection(SuperClassDetail child) {
        if (child != null)
            child.setCultId((Cultivation)this);
    }

    void internalRemoveSuperClassDetailCollection(SuperClassDetail child) {
        if (child != null)
            this.superClassDetailCollection.remove(child);
    }

    void internalAddSuperClassDetailCollection(SuperClassDetail child) {
        if (child != null)
            this.superClassDetailCollection.add(child);
    }

    @XmlTransient
    public List<GpDecision> getGpDecisionCollection() {
        return gpDecisionCollection;
    }

    public void setGpDecisionCollection(List<GpDecision> gpDecisionCollection) {
        this.gpDecisionCollection = gpDecisionCollection;
    }

    public void removeGpDecisionCollection(GpDecision child) {
        if (child != null)
            child.setCultId(null);
    }

    public void addGpDecisionCollection(GpDecision child) {
        if (child != null)
            child.setCultId((Cultivation)this);
    }

    void internalRemoveGpDecisionCollection(GpDecision child) {
        if (child != null)
            this.gpDecisionCollection.remove(child);
    }

    void internalAddGpDecisionCollection(GpDecision child) {
        if (child != null)
            this.gpDecisionCollection.add(child);
    }

    @XmlTransient
    public List<EcCultCopy> getEcCultCopyCollection() {
        return ecCultCopyCollection;
    }

    public void setEcCultCopyCollection(List<EcCultCopy> ecCultCopyCollection) {
        this.ecCultCopyCollection = ecCultCopyCollection;
    }

    public void removeEcCultCopyCollection(EcCultCopy child) {
        if (child != null)
            child.setCultId(null);
    }

    public void addEcCultCopyCollection(EcCultCopy child) {
        if (child != null)
            child.setCultId((Cultivation)this);
    }

    void internalRemoveEcCultCopyCollection(EcCultCopy child) {
        if (child != null)
            this.ecCultCopyCollection.remove(child);
    }

    void internalAddEcCultCopyCollection(EcCultCopy child) {
        if (child != null)
            this.ecCultCopyCollection.add(child);
    }

    @XmlTransient
    public List<ParcelClas> getParcelClascult_id_declCollection() {
        return parcelClascult_id_declCollection;
    }

    public void setParcelClascult_id_declCollection(List<ParcelClas> parcelClascult_id_declCollection) {
        this.parcelClascult_id_declCollection = parcelClascult_id_declCollection;
    }

    public void removeParcelClascult_id_declCollection(ParcelClas child) {
        if (child != null)
            child.setCultIdDecl(null);
    }

    public void addParcelClascult_id_declCollection(ParcelClas child) {
        if (child != null)
            child.setCultIdDecl((Cultivation)this);
    }

    void internalRemoveParcelClascult_id_declCollection(ParcelClas child) {
        if (child != null)
            this.parcelClascult_id_declCollection.remove(child);
    }

    void internalAddParcelClascult_id_declCollection(ParcelClas child) {
        if (child != null)
            this.parcelClascult_id_declCollection.add(child);
    }

    @XmlTransient
    public List<ParcelClas> getParcelClascult_id_predCollection() {
        return parcelClascult_id_predCollection;
    }

    public void setParcelClascult_id_predCollection(List<ParcelClas> parcelClascult_id_predCollection) {
        this.parcelClascult_id_predCollection = parcelClascult_id_predCollection;
    }

    public void removeParcelClascult_id_predCollection(ParcelClas child) {
        if (child != null)
            child.setCultIdPred(null);
    }

    public void addParcelClascult_id_predCollection(ParcelClas child) {
        if (child != null)
            child.setCultIdPred((Cultivation)this);
    }

    void internalRemoveParcelClascult_id_predCollection(ParcelClas child) {
        if (child != null)
            this.parcelClascult_id_predCollection.remove(child);
    }

    void internalAddParcelClascult_id_predCollection(ParcelClas child) {
        if (child != null)
            this.parcelClascult_id_predCollection.add(child);
    }

    @XmlTransient
    public List<ParcelClas> getParcelClascult_id_pred2Collection() {
        return parcelClascult_id_pred2Collection;
    }

    public void setParcelClascult_id_pred2Collection(List<ParcelClas> parcelClascult_id_pred2Collection) {
        this.parcelClascult_id_pred2Collection = parcelClascult_id_pred2Collection;
    }

    public void removeParcelClascult_id_pred2Collection(ParcelClas child) {
        if (child != null)
            child.setCultIdPred2(null);
    }

    public void addParcelClascult_id_pred2Collection(ParcelClas child) {
        if (child != null)
            child.setCultIdPred2((Cultivation)this);
    }

    void internalRemoveParcelClascult_id_pred2Collection(ParcelClas child) {
        if (child != null)
            this.parcelClascult_id_pred2Collection.remove(child);
    }

    void internalAddParcelClascult_id_pred2Collection(ParcelClas child) {
        if (child != null)
            this.parcelClascult_id_pred2Collection.add(child);
    }

    /*
    fk to cover_type
    */
    public CoverType getCotyId() {
        return cotyId;
    }

    public void setCotyId(CoverType cotyId) {
    //    if (this.cotyId != null) 
    //        this.cotyId.internalRemoveCultivationCollection(this);
        this.cotyId = cotyId;
    //    if (cotyId != null)
    //        cotyId.internalAddCultivationCollection(this);
    }

    /*
    fk to excel_files
    */
    public ExcelFile getExfiId() {
        return exfiId;
    }

    public void setExfiId(ExcelFile exfiId) {
    //    if (this.exfiId != null) 
    //        this.exfiId.internalRemoveCultivationCollection(this);
        this.exfiId = exfiId;
    //    if (exfiId != null)
    //        exfiId.internalAddCultivationCollection(this);
    }



    @Override
    public int hashCode() {
        return cultId.hashCode();
    }

    public String getRowKey() {
        return cultId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return cultId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(cultId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.Cultivation[ cultId=" + cultId + " ]";
    }

    @Override
    public String getEntityName() {
        return "Cultivation";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
        setUsrinsert(userName);
        setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
        setUsrupdate(userName);
        setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the cultId fields are not set
        if (!(object instanceof CultivationBase)) {
            return false;
        }
        CultivationBase other = (CultivationBase) object;
        if (this.cultId == null || other.cultId == null) 
            return false;
        
        return this.cultId.equals(other.cultId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
