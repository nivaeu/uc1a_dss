/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class FileDirPathBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'fidr_id' είναι υποχρεωτικό")
    @Column(name = "fidr_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="file_dir_path_fidr_id")
    //@SequenceGenerator(name="file_dir_path_fidr_id", sequenceName="niva.file_dir_path_fidr_id_seq", allocationSize=1)
    protected Integer fidrId;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'directory' είναι υποχρεωτικό")
    @Size(min = 1, max = 150, message="Το πεδίο 'directory' πρέπει νά έχει μέγεθος μεταξύ 1 και 150.")
    @Column(name = "directory")
    protected String directory;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'file_type' είναι υποχρεωτικό")
    @Column(name = "file_type")
    protected Integer fileType;



    public FileDirPathBase() {
    }

    public FileDirPathBase(Integer fidrId) {
        this.fidrId = fidrId;
    }

    public FileDirPathBase(Integer fidrId, String directory, Integer rowVersion, Integer fileType) {
        this.fidrId = fidrId;
        this.directory = directory;
        this.rowVersion = rowVersion;
        this.fileType = fileType;
    }

    public FileDirPathBase(String directory, Integer rowVersion, Integer fileType) {
        this.directory = directory;
        this.rowVersion = rowVersion;
        this.fileType = fileType;
    }

    public FileDirPath clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "FileDirPath:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (FileDirPath)alreadyCloned.get(key);

        FileDirPath clone = new FileDirPath();
        alreadyCloned.put(key, clone);

        clone.setFidrId(getFidrId());
        clone.setDirectory(getDirectory());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteinsert(getDteinsert());
        clone.setUsrupdate(getUsrupdate());
        clone.setDteupdate(getDteupdate());
        clone.setRowVersion(getRowVersion());
        clone.setFileType(getFileType());
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "FileDirPath:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(fidrId) : fidrId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (fidrId != null) {
            sb.append("\"fidrId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(fidrId) : fidrId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"fidrId\":"); sb.append("null");sb.append(",");
        }
        if (directory != null) {
            sb.append("\"directory\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(directory)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"directory\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (fileType != null) {
            sb.append("\"fileType\":"); sb.append(fileType);sb.append(",");
        } else {
            sb.append("\"fileType\":"); sb.append("null");sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    */
    public Integer getFidrId() {
        return fidrId;
    }

    public void setFidrId(Integer fidrId) {
        this.fidrId = fidrId;
    }
    /*
    */
    public String getDirectory() {
        return directory;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }
    /*
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    */
    public Integer getFileType() {
        return fileType;
    }

    public void setFileType(Integer fileType) {
        this.fileType = fileType;
    }


    @Override
    public int hashCode() {
        return fidrId.hashCode();
    }

    public String getRowKey() {
        return fidrId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return fidrId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(fidrId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.FileDirPath[ fidrId=" + fidrId + " ]";
    }

    @Override
    public String getEntityName() {
        return "FileDirPath";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
        setUsrinsert(userName);
        setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
        setUsrupdate(userName);
        setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the fidrId fields are not set
        if (!(object instanceof FileDirPathBase)) {
            return false;
        }
        FileDirPathBase other = (FileDirPathBase) object;
        if (this.fidrId == null || other.fidrId == null) 
            return false;
        
        return this.fidrId.equals(other.fidrId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
