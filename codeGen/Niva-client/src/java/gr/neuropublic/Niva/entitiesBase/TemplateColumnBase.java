/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
the columns of the template file


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class TemplateColumnBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'teco_id' είναι υποχρεωτικό")
    @Column(name = "teco_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="template_columns_teco_id")
    //@SequenceGenerator(name="template_columns_teco_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer tecoId;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'clfier_name' είναι υποχρεωτικό")
    @Size(min = 1, max = 60, message="Το πεδίο 'clfier_name' πρέπει νά έχει μέγεθος μεταξύ 1 και 60.")
    @Column(name = "clfier_name")
    protected String clfierName;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @JoinColumn(name = "fite_id", referencedColumnName = "fite_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'fite_id' είναι υποχρεωτικό")
    protected FileTemplate fiteId;

    public boolean isFiteIdPresent() {
        if (fiteId==null || fiteId.fiteId == null)
            return false;
        return true;
    }


    @JoinColumn(name = "prco_id", referencedColumnName = "prco_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'prco_id' είναι υποχρεωτικό")
    protected PredefCol prcoId;

    public boolean isPrcoIdPresent() {
        if (prcoId==null || prcoId.prcoId == null)
            return false;
        return true;
    }



    public TemplateColumnBase() {
    }

    public TemplateColumnBase(Integer tecoId) {
        this.tecoId = tecoId;
    }

    public TemplateColumnBase(Integer tecoId, String clfierName, Integer rowVersion) {
        this.tecoId = tecoId;
        this.clfierName = clfierName;
        this.rowVersion = rowVersion;
    }

    public TemplateColumnBase(String clfierName, Integer rowVersion, FileTemplate fiteId, PredefCol prcoId) {
        this.clfierName = clfierName;
        this.rowVersion = rowVersion;
        this.fiteId = fiteId;
        this.prcoId = prcoId;
    }

    public TemplateColumn clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "TemplateColumn:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (TemplateColumn)alreadyCloned.get(key);

        TemplateColumn clone = new TemplateColumn();
        alreadyCloned.put(key, clone);

        clone.setTecoId(getTecoId());
        clone.setClfierName(getClfierName());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteinsert(getDteinsert());
        clone.setUsrupdate(getUsrupdate());
        clone.setDteupdate(getDteupdate());
        clone.setRowVersion(getRowVersion());
        if (fiteId == null || fiteId.fiteId == null) {
            clone.setFiteId(fiteId);
        } else {
            clone.setFiteId(getFiteId().clone(alreadyCloned));
        }
        if (prcoId == null || prcoId.prcoId == null) {
            clone.setPrcoId(prcoId);
        } else {
            clone.setPrcoId(getPrcoId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "TemplateColumn:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(tecoId) : tecoId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (tecoId != null) {
            sb.append("\"tecoId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(tecoId) : tecoId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"tecoId\":"); sb.append("null");sb.append(",");
        }
        if (clfierName != null) {
            sb.append("\"clfierName\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(clfierName)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"clfierName\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (fiteId != null && fiteId.fiteId != null) {
            sb.append("\"fiteId\":"); sb.append(getFiteId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (fiteId == null) {
            sb.append("\"fiteId\":"); sb.append("null");  sb.append(",");
        }
        if (prcoId != null && prcoId.prcoId != null) {
            sb.append("\"prcoId\":"); sb.append(getPrcoId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (prcoId == null) {
            sb.append("\"prcoId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    primary key
    */
    public Integer getTecoId() {
        return tecoId;
    }

    public void setTecoId(Integer tecoId) {
        this.tecoId = tecoId;
    }
    /*
    classifier name -inputs file name
    */
    public String getClfierName() {
        return clfierName;
    }

    public void setClfierName(String clfierName) {
        this.clfierName = clfierName;
    }
    /*
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    fk to file template
    */
    public FileTemplate getFiteId() {
        return fiteId;
    }

    public void setFiteId(FileTemplate fiteId) {
    //    if (this.fiteId != null) 
    //        this.fiteId.internalRemoveTemplateColumnCollection(this);
        this.fiteId = fiteId;
    //    if (fiteId != null)
    //        fiteId.internalAddTemplateColumnCollection(this);
    }

    /*
    fk to file predefine columns
    */
    public PredefCol getPrcoId() {
        return prcoId;
    }

    public void setPrcoId(PredefCol prcoId) {
    //    if (this.prcoId != null) 
    //        this.prcoId.internalRemoveTemplateColumnCollection(this);
        this.prcoId = prcoId;
    //    if (prcoId != null)
    //        prcoId.internalAddTemplateColumnCollection(this);
    }



    @Override
    public int hashCode() {
        return tecoId.hashCode();
    }

    public String getRowKey() {
        return tecoId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return tecoId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(tecoId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.TemplateColumn[ tecoId=" + tecoId + " ]";
    }

    @Override
    public String getEntityName() {
        return "TemplateColumn";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
        setUsrinsert(userName);
        setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
        setUsrupdate(userName);
        setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the tecoId fields are not set
        if (!(object instanceof TemplateColumnBase)) {
            return false;
        }
        TemplateColumnBase other = (TemplateColumnBase) object;
        if (this.tecoId == null || other.tecoId == null) 
            return false;
        
        return this.tecoId.equals(other.tecoId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
