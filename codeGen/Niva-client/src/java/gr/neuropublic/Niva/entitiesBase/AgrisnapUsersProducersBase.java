/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
Contains records with producers for each agrispan user.


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class AgrisnapUsersProducersBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Basic(optional = true)
    @Column(name = "dte_rel_created")
    @Temporal(TemporalType.DATE)
    protected Date dteRelCreated;


    @Basic(optional = true)
    @Column(name = "dte_rel_canceled")
    @Temporal(TemporalType.DATE)
    protected Date dteRelCanceled;


    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'agrisnap_users_producers_id' είναι υποχρεωτικό")
    @Column(name = "agrisnap_users_producers_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="agrisnap_users_producers_agrisnap_users_producers_id")
    //@SequenceGenerator(name="agrisnap_users_producers_agrisnap_users_producers_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer agrisnapUsersProducersId;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @JoinColumn(name = "agrisnap_users_id", referencedColumnName = "agrisnap_users_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'agrisnap_users_id' είναι υποχρεωτικό")
    protected AgrisnapUsers agrisnapUsersId;

    public boolean isAgrisnapUsersIdPresent() {
        if (agrisnapUsersId==null || agrisnapUsersId.agrisnapUsersId == null)
            return false;
        return true;
    }


    @JoinColumn(name = "producers_id", referencedColumnName = "producers_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'producers_id' είναι υποχρεωτικό")
    protected Producers producersId;

    public boolean isProducersIdPresent() {
        if (producersId==null || producersId.producersId == null)
            return false;
        return true;
    }



    public AgrisnapUsersProducersBase() {
    }

    public AgrisnapUsersProducersBase(Integer agrisnapUsersProducersId) {
        this.agrisnapUsersProducersId = agrisnapUsersProducersId;
    }

    public AgrisnapUsersProducersBase(Integer agrisnapUsersProducersId, Date dteRelCreated, Date dteRelCanceled, Integer rowVersion) {
        this.agrisnapUsersProducersId = agrisnapUsersProducersId;
        this.dteRelCreated = dteRelCreated;
        this.dteRelCanceled = dteRelCanceled;
        this.rowVersion = rowVersion;
    }

    public AgrisnapUsersProducersBase(Date dteRelCreated, Date dteRelCanceled, Integer rowVersion, AgrisnapUsers agrisnapUsersId, Producers producersId) {
        this.dteRelCreated = dteRelCreated;
        this.dteRelCanceled = dteRelCanceled;
        this.rowVersion = rowVersion;
        this.agrisnapUsersId = agrisnapUsersId;
        this.producersId = producersId;
    }

    public AgrisnapUsersProducers clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "AgrisnapUsersProducers:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (AgrisnapUsersProducers)alreadyCloned.get(key);

        AgrisnapUsersProducers clone = new AgrisnapUsersProducers();
        alreadyCloned.put(key, clone);

        clone.setDteRelCreated(getDteRelCreated());
        clone.setDteRelCanceled(getDteRelCanceled());
        clone.setAgrisnapUsersProducersId(getAgrisnapUsersProducersId());
        clone.setRowVersion(getRowVersion());
        if (agrisnapUsersId == null || agrisnapUsersId.agrisnapUsersId == null) {
            clone.setAgrisnapUsersId(agrisnapUsersId);
        } else {
            clone.setAgrisnapUsersId(getAgrisnapUsersId().clone(alreadyCloned));
        }
        if (producersId == null || producersId.producersId == null) {
            clone.setProducersId(producersId);
        } else {
            clone.setProducersId(getProducersId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "AgrisnapUsersProducers:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(agrisnapUsersProducersId) : agrisnapUsersProducersId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (dteRelCreated != null) {
            sb.append("\"dteRelCreated\":"); sb.append(dteRelCreated.getTime()); sb.append(",");
        } else {
            sb.append("\"dteRelCreated\":"); sb.append("null");sb.append(",");
        }
        if (dteRelCanceled != null) {
            sb.append("\"dteRelCanceled\":"); sb.append(dteRelCanceled.getTime()); sb.append(",");
        } else {
            sb.append("\"dteRelCanceled\":"); sb.append("null");sb.append(",");
        }
        if (agrisnapUsersProducersId != null) {
            sb.append("\"agrisnapUsersProducersId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(agrisnapUsersProducersId) : agrisnapUsersProducersId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"agrisnapUsersProducersId\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (agrisnapUsersId != null && agrisnapUsersId.agrisnapUsersId != null) {
            sb.append("\"agrisnapUsersId\":"); sb.append(getAgrisnapUsersId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (agrisnapUsersId == null) {
            sb.append("\"agrisnapUsersId\":"); sb.append("null");  sb.append(",");
        }
        if (producersId != null && producersId.producersId != null) {
            sb.append("\"producersId\":"); sb.append(getProducersId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (producersId == null) {
            sb.append("\"producersId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    */
    public Date getDteRelCreated() {
        return dteRelCreated;
    }

    public void setDteRelCreated(Date dteRelCreated) {
        this.dteRelCreated = dteRelCreated;
    }
    /*
    */
    public Date getDteRelCanceled() {
        return dteRelCanceled;
    }

    public void setDteRelCanceled(Date dteRelCanceled) {
        this.dteRelCanceled = dteRelCanceled;
    }
    /*
    */
    public Integer getAgrisnapUsersProducersId() {
        return agrisnapUsersProducersId;
    }

    public void setAgrisnapUsersProducersId(Integer agrisnapUsersProducersId) {
        this.agrisnapUsersProducersId = agrisnapUsersProducersId;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    */
    public AgrisnapUsers getAgrisnapUsersId() {
        return agrisnapUsersId;
    }

    public void setAgrisnapUsersId(AgrisnapUsers agrisnapUsersId) {
    //    if (this.agrisnapUsersId != null) 
    //        this.agrisnapUsersId.internalRemoveAgrisnapUsersProducersCollection(this);
        this.agrisnapUsersId = agrisnapUsersId;
    //    if (agrisnapUsersId != null)
    //        agrisnapUsersId.internalAddAgrisnapUsersProducersCollection(this);
    }

    /*
    */
    public Producers getProducersId() {
        return producersId;
    }

    public void setProducersId(Producers producersId) {
    //    if (this.producersId != null) 
    //        this.producersId.internalRemoveAgrisnapUsersProducersCollection(this);
        this.producersId = producersId;
    //    if (producersId != null)
    //        producersId.internalAddAgrisnapUsersProducersCollection(this);
    }



    @Override
    public int hashCode() {
        return agrisnapUsersProducersId.hashCode();
    }

    public String getRowKey() {
        return agrisnapUsersProducersId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return agrisnapUsersProducersId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(agrisnapUsersProducersId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.AgrisnapUsersProducers[ agrisnapUsersProducersId=" + agrisnapUsersProducersId + " ]";
    }

    @Override
    public String getEntityName() {
        return "AgrisnapUsersProducers";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the agrisnapUsersProducersId fields are not set
        if (!(object instanceof AgrisnapUsersProducersBase)) {
            return false;
        }
        AgrisnapUsersProducersBase other = (AgrisnapUsersProducersBase) object;
        if (this.agrisnapUsersProducersId == null || other.agrisnapUsersProducersId == null) 
            return false;
        
        return this.agrisnapUsersProducersId.equals(other.agrisnapUsersProducersId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
