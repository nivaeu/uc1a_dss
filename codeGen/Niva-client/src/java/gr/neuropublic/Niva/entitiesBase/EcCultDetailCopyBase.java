/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
})
public abstract class EcCultDetailCopyBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'ecdc_id' είναι υποχρεωτικό")
    @Column(name = "ecdc_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ec_cult_detail_copy_ecdc_id")
    //@SequenceGenerator(name="ec_cult_detail_copy_ecdc_id", sequenceName="niva.ec_cult_detail_copy_ecdc_id_seq", allocationSize=1)
    protected Integer ecdcId;


    @Basic(optional = true)
    @Column(name = "ordering_number")
    protected Integer orderingNumber;


    @Basic(optional = true)
    @Column(name = "agrees_declar")
    protected Integer agreesDeclar;


    @Basic(optional = true)
    @Column(name = "comparison_oper")
    protected Integer comparisonOper;


    @Basic(optional = true)
    @Column(name = "probab_thres")
    protected BigDecimal probabThres;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'decision_light' είναι υποχρεωτικό")
    @Column(name = "decision_light")
    protected Integer decisionLight;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrinsert' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrinsert")
    protected String usrinsert;


    @Basic(optional = true)
    @Column(name = "dteinsert")
    protected java.sql.Timestamp dteinsert;



    @Basic(optional = true)
    @Size(min = 0, max = 150, message="Το πεδίο 'usrupdate' πρέπει νά έχει μέγεθος μεταξύ 0 και 150.")
    @Column(name = "usrupdate")
    protected String usrupdate;


    @Basic(optional = true)
    @Column(name = "dteupdate")
    protected java.sql.Timestamp dteupdate;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @Basic(optional = true)
    @Column(name = "agrees_declar2")
    protected Integer agreesDeclar2;


    @Basic(optional = true)
    @Column(name = "comparison_oper2")
    protected Integer comparisonOper2;


    @Basic(optional = true)
    @Column(name = "probab_thres2")
    protected BigDecimal probabThres2;


    @Basic(optional = true)
    @Column(name = "probab_thres_sum")
    protected BigDecimal probabThresSum;


    @Basic(optional = true)
    @Column(name = "comparison_oper3")
    protected Integer comparisonOper3;


    @JoinColumn(name = "eccc_id", referencedColumnName = "eccc_id")
    @ManyToOne(optional = false, fetch= FetchType.LAZY)
    @NotNull(message="Το πεδίο 'eccc_id' είναι υποχρεωτικό")
    protected EcCultCopy ecccId;

    public boolean isEcccIdPresent() {
        if (ecccId==null || ecccId.ecccId == null)
            return false;
        return true;
    }



    public EcCultDetailCopyBase() {
    }

    public EcCultDetailCopyBase(Integer ecdcId) {
        this.ecdcId = ecdcId;
    }

    public EcCultDetailCopyBase(Integer ecdcId, Integer orderingNumber, Integer agreesDeclar, Integer comparisonOper, BigDecimal probabThres, Integer decisionLight, Integer rowVersion, Integer agreesDeclar2, Integer comparisonOper2, BigDecimal probabThres2, BigDecimal probabThresSum, Integer comparisonOper3) {
        this.ecdcId = ecdcId;
        this.orderingNumber = orderingNumber;
        this.agreesDeclar = agreesDeclar;
        this.comparisonOper = comparisonOper;
        this.probabThres = probabThres;
        this.decisionLight = decisionLight;
        this.rowVersion = rowVersion;
        this.agreesDeclar2 = agreesDeclar2;
        this.comparisonOper2 = comparisonOper2;
        this.probabThres2 = probabThres2;
        this.probabThresSum = probabThresSum;
        this.comparisonOper3 = comparisonOper3;
    }

    public EcCultDetailCopyBase(Integer orderingNumber, Integer agreesDeclar, Integer comparisonOper, BigDecimal probabThres, Integer decisionLight, Integer rowVersion, Integer agreesDeclar2, Integer comparisonOper2, BigDecimal probabThres2, BigDecimal probabThresSum, Integer comparisonOper3, EcCultCopy ecccId) {
        this.orderingNumber = orderingNumber;
        this.agreesDeclar = agreesDeclar;
        this.comparisonOper = comparisonOper;
        this.probabThres = probabThres;
        this.decisionLight = decisionLight;
        this.rowVersion = rowVersion;
        this.agreesDeclar2 = agreesDeclar2;
        this.comparisonOper2 = comparisonOper2;
        this.probabThres2 = probabThres2;
        this.probabThresSum = probabThresSum;
        this.comparisonOper3 = comparisonOper3;
        this.ecccId = ecccId;
    }

    public EcCultDetailCopy clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "EcCultDetailCopy:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (EcCultDetailCopy)alreadyCloned.get(key);

        EcCultDetailCopy clone = new EcCultDetailCopy();
        alreadyCloned.put(key, clone);

        clone.setEcdcId(getEcdcId());
        clone.setOrderingNumber(getOrderingNumber());
        clone.setAgreesDeclar(getAgreesDeclar());
        clone.setComparisonOper(getComparisonOper());
        clone.setProbabThres(getProbabThres());
        clone.setDecisionLight(getDecisionLight());
        clone.setUsrinsert(getUsrinsert());
        clone.setDteinsert(getDteinsert());
        clone.setUsrupdate(getUsrupdate());
        clone.setDteupdate(getDteupdate());
        clone.setRowVersion(getRowVersion());
        clone.setAgreesDeclar2(getAgreesDeclar2());
        clone.setComparisonOper2(getComparisonOper2());
        clone.setProbabThres2(getProbabThres2());
        clone.setProbabThresSum(getProbabThresSum());
        clone.setComparisonOper3(getComparisonOper3());
        if (ecccId == null || ecccId.ecccId == null) {
            clone.setEcccId(ecccId);
        } else {
            clone.setEcccId(getEcccId().clone(alreadyCloned));
        }
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "EcCultDetailCopy:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(ecdcId) : ecdcId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (ecdcId != null) {
            sb.append("\"ecdcId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(ecdcId) : ecdcId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"ecdcId\":"); sb.append("null");sb.append(",");
        }
        if (orderingNumber != null) {
            sb.append("\"orderingNumber\":"); sb.append(orderingNumber);sb.append(",");
        } else {
            sb.append("\"orderingNumber\":"); sb.append("null");sb.append(",");
        }
        if (agreesDeclar != null) {
            sb.append("\"agreesDeclar\":"); sb.append(agreesDeclar);sb.append(",");
        } else {
            sb.append("\"agreesDeclar\":"); sb.append("null");sb.append(",");
        }
        if (comparisonOper != null) {
            sb.append("\"comparisonOper\":"); sb.append(comparisonOper);sb.append(",");
        } else {
            sb.append("\"comparisonOper\":"); sb.append("null");sb.append(",");
        }
        if (probabThres != null) {
            sb.append("\"probabThres\":"); sb.append(probabThres);sb.append(",");
        } else {
            sb.append("\"probabThres\":"); sb.append("null");sb.append(",");
        }
        if (decisionLight != null) {
            sb.append("\"decisionLight\":"); sb.append(decisionLight);sb.append(",");
        } else {
            sb.append("\"decisionLight\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (agreesDeclar2 != null) {
            sb.append("\"agreesDeclar2\":"); sb.append(agreesDeclar2);sb.append(",");
        } else {
            sb.append("\"agreesDeclar2\":"); sb.append("null");sb.append(",");
        }
        if (comparisonOper2 != null) {
            sb.append("\"comparisonOper2\":"); sb.append(comparisonOper2);sb.append(",");
        } else {
            sb.append("\"comparisonOper2\":"); sb.append("null");sb.append(",");
        }
        if (probabThres2 != null) {
            sb.append("\"probabThres2\":"); sb.append(probabThres2);sb.append(",");
        } else {
            sb.append("\"probabThres2\":"); sb.append("null");sb.append(",");
        }
        if (probabThresSum != null) {
            sb.append("\"probabThresSum\":"); sb.append(probabThresSum);sb.append(",");
        } else {
            sb.append("\"probabThresSum\":"); sb.append("null");sb.append(",");
        }
        if (comparisonOper3 != null) {
            sb.append("\"comparisonOper3\":"); sb.append(comparisonOper3);sb.append(",");
        } else {
            sb.append("\"comparisonOper3\":"); sb.append("null");sb.append(",");
        }
        if (ecccId != null && ecccId.ecccId != null) {
            sb.append("\"ecccId\":"); sb.append(getEcccId().toJson(alreadySearialized, crypto));  sb.append(",");
        } else if (ecccId == null) {
            sb.append("\"ecccId\":"); sb.append("null");  sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    */
    public Integer getEcdcId() {
        return ecdcId;
    }

    public void setEcdcId(Integer ecdcId) {
        this.ecdcId = ecdcId;
    }
    /*
    */
    public Integer getOrderingNumber() {
        return orderingNumber;
    }

    public void setOrderingNumber(Integer orderingNumber) {
        this.orderingNumber = orderingNumber;
    }
    /*
    */
    public Integer getAgreesDeclar() {
        return agreesDeclar;
    }

    public void setAgreesDeclar(Integer agreesDeclar) {
        this.agreesDeclar = agreesDeclar;
    }
    /*
    */
    public Integer getComparisonOper() {
        return comparisonOper;
    }

    public void setComparisonOper(Integer comparisonOper) {
        this.comparisonOper = comparisonOper;
    }
    /*
    */
    public BigDecimal getProbabThres() {
        return probabThres;
    }

    public void setProbabThres(BigDecimal probabThres) {
        this.probabThres = probabThres;
    }
    /*
    */
    public Integer getDecisionLight() {
        return decisionLight;
    }

    public void setDecisionLight(Integer decisionLight) {
        this.decisionLight = decisionLight;
    }
    /*
    */
    public String getUsrinsert() {
        return usrinsert;
    }

    public void setUsrinsert(String usrinsert) {
        this.usrinsert = usrinsert;
    }
    /*
    */
    public java.sql.Timestamp getDteinsert() {
        return dteinsert;
    }

    public void setDteinsert(java.sql.Timestamp dteinsert) {
        this.dteinsert = dteinsert;
    }
    /*
    */
    public String getUsrupdate() {
        return usrupdate;
    }

    public void setUsrupdate(String usrupdate) {
        this.usrupdate = usrupdate;
    }
    /*
    */
    public java.sql.Timestamp getDteupdate() {
        return dteupdate;
    }

    public void setDteupdate(java.sql.Timestamp dteupdate) {
        this.dteupdate = dteupdate;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    */
    public Integer getAgreesDeclar2() {
        return agreesDeclar2;
    }

    public void setAgreesDeclar2(Integer agreesDeclar2) {
        this.agreesDeclar2 = agreesDeclar2;
    }
    /*
    */
    public Integer getComparisonOper2() {
        return comparisonOper2;
    }

    public void setComparisonOper2(Integer comparisonOper2) {
        this.comparisonOper2 = comparisonOper2;
    }
    /*
    */
    public BigDecimal getProbabThres2() {
        return probabThres2;
    }

    public void setProbabThres2(BigDecimal probabThres2) {
        this.probabThres2 = probabThres2;
    }
    /*
    */
    public BigDecimal getProbabThresSum() {
        return probabThresSum;
    }

    public void setProbabThresSum(BigDecimal probabThresSum) {
        this.probabThresSum = probabThresSum;
    }
    /*
    */
    public Integer getComparisonOper3() {
        return comparisonOper3;
    }

    public void setComparisonOper3(Integer comparisonOper3) {
        this.comparisonOper3 = comparisonOper3;
    }
    /*
    */
    public EcCultCopy getEcccId() {
        return ecccId;
    }

    public void setEcccId(EcCultCopy ecccId) {
    //    if (this.ecccId != null) 
    //        this.ecccId.internalRemoveEcCultDetailCopyCollection(this);
        this.ecccId = ecccId;
    //    if (ecccId != null)
    //        ecccId.internalAddEcCultDetailCopyCollection(this);
    }



    @Override
    public int hashCode() {
        return ecdcId.hashCode();
    }

    public String getRowKey() {
        return ecdcId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return ecdcId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(ecdcId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.EcCultDetailCopy[ ecdcId=" + ecdcId + " ]";
    }

    @Override
    public String getEntityName() {
        return "EcCultDetailCopy";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
        setUsrinsert(userName);
        setDteinsert(new java.sql.Timestamp(System.currentTimeMillis()));
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
        setUsrupdate(userName);
        setDteupdate(new java.sql.Timestamp(System.currentTimeMillis()));
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the ecdcId fields are not set
        if (!(object instanceof EcCultDetailCopyBase)) {
            return false;
        }
        EcCultDetailCopyBase other = (EcCultDetailCopyBase) object;
        if (this.ecdcId == null || other.ecdcId == null) 
            return false;
        
        return this.ecdcId.equals(other.ecdcId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
