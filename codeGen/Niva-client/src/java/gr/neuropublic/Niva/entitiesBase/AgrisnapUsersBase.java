/*
*/
package gr.neuropublic.Niva.entitiesBase;

import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.*;
import javax.validation.constraints.Null;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import gr.neuropublic.validators.ICheckedEntity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import gr.neuropublic.Niva.entities.*;
import gr.neuropublic.validators.EntityValidationError;
import gr.neuropublic.validators.EntityValidationSubError;
import gr.neuropublic.base.JsonHelper;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.geotools.geojson.geom.GeometryJSON;
/*
AgrisnapUser


*/

@MappedSuperclass
@XmlRootElement
@NamedQueries({
@NamedQuery(name = "AgrisnapUsers.findAgrisnapUsers", query = "SELECT x FROM AgrisnapUsers x WHERE (x.agrisnapName like :agrisnapName) AND (x.agrisnapUid like :agrisnapUid)")})
public abstract class AgrisnapUsersBase implements Serializable, ICheckedEntity {    
    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'agrisnap_name' είναι υποχρεωτικό")
    @Size(min = 1, max = 40, message="Το πεδίο 'agrisnap_name' πρέπει νά έχει μέγεθος μεταξύ 1 και 40.")
    @Column(name = "agrisnap_name")
    protected String agrisnapName;


    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'row_version' είναι υποχρεωτικό")
    @Version
    @Column(name = "row_version")
    protected Integer rowVersion;


    @Id
    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'agrisnap_users_id' είναι υποχρεωτικό")
    @Column(name = "agrisnap_users_id")
    //@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="agrisnap_users_agrisnap_users_id")
    //@SequenceGenerator(name="agrisnap_users_agrisnap_users_id", sequenceName="niva.niva_sq", allocationSize=1)
    protected Integer agrisnapUsersId;



    @Basic(optional = false)
    @NotNull(message="Το πεδίο 'agrisnap_uid' είναι υποχρεωτικό")
    @Size(min = 1, max = 5, message="Το πεδίο 'agrisnap_uid' πρέπει νά έχει μέγεθος μεταξύ 1 και 5.")
    @Column(name = "agrisnap_uid")
    protected String agrisnapUid;


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "agrisnapUsersId", orphanRemoval=false)
    @OneToMany(mappedBy = "agrisnapUsersId")
    protected List<AgrisnapUsersProducers> agrisnapUsersProducersCollection = new ArrayList<AgrisnapUsersProducers>();


    //@OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE, CascadeType.DETACH}, mappedBy = "agrisnapUsersId", orphanRemoval=false)
    @OneToMany(mappedBy = "agrisnapUsersId")
    protected List<GpRequests> gpRequestsCollection = new ArrayList<GpRequests>();



    public AgrisnapUsersBase() {
    }

    public AgrisnapUsersBase(Integer agrisnapUsersId) {
        this.agrisnapUsersId = agrisnapUsersId;
    }

    public AgrisnapUsersBase(Integer agrisnapUsersId, String agrisnapName, Integer rowVersion, String agrisnapUid) {
        this.agrisnapUsersId = agrisnapUsersId;
        this.agrisnapName = agrisnapName;
        this.rowVersion = rowVersion;
        this.agrisnapUid = agrisnapUid;
    }

    public AgrisnapUsersBase(String agrisnapName, Integer rowVersion, String agrisnapUid) {
        this.agrisnapName = agrisnapName;
        this.rowVersion = rowVersion;
        this.agrisnapUid = agrisnapUid;
    }

    public AgrisnapUsers clone(Map<String, ICheckedEntity> alreadyCloned) {
        String key = "AgrisnapUsers:" + getPrimaryKeyValue().toString();
        if (alreadyCloned.containsKey(key))
            return (AgrisnapUsers)alreadyCloned.get(key);

        AgrisnapUsers clone = new AgrisnapUsers();
        alreadyCloned.put(key, clone);

        clone.setAgrisnapName(getAgrisnapName());
        clone.setRowVersion(getRowVersion());
        clone.setAgrisnapUsersId(getAgrisnapUsersId());
        clone.setAgrisnapUid(getAgrisnapUid());
        clone.setAgrisnapUsersProducersCollection(getAgrisnapUsersProducersCollection());
        clone.setGpRequestsCollection(getGpRequestsCollection());
        return clone;
    }

    // Override this method to encode transient fields
    protected void toJsonExtend(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto, StringBuilder sb) {
    }
    public String toJson(java.util.Set<String> alreadySearialized, gr.neuropublic.base.CryptoUtils crypto) {
        String key = "AgrisnapUsers:" + getPrimaryKeyValue().toString();
        StringBuilder sb = new StringBuilder();
        sb.append('{'); 
        if (alreadySearialized.contains(key)) {
            sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");
            sb.append("\"$refId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(agrisnapUsersId) : agrisnapUsersId); 
            sb.append('"'); sb.append("}");
            return sb.toString();
        }

        alreadySearialized.add(key);
        sb.append("\"$entityName\":"); sb.append('"'); sb.append(this.getEntityName()); sb.append('"'); sb.append(",");

        if (agrisnapName != null) {
            sb.append("\"agrisnapName\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(agrisnapName)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"agrisnapName\":"); sb.append("null");sb.append(",");
        }
        if (rowVersion != null) {
            sb.append("\"rowVersion\":"); sb.append(rowVersion);sb.append(",");
        } else {
            sb.append("\"rowVersion\":"); sb.append("null");sb.append(",");
        }
        if (agrisnapUsersId != null) {
            sb.append("\"agrisnapUsersId\":"); sb.append('"'); 
            sb.append(crypto != null ? crypto.EncryptNumber(agrisnapUsersId) : agrisnapUsersId); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"agrisnapUsersId\":"); sb.append("null");sb.append(",");
        }
        if (agrisnapUid != null) {
            sb.append("\"agrisnapUid\":"); 
            sb.append('"'); 
            sb.append(JsonHelper.escapeString(agrisnapUid)); 
            sb.append('"'); sb.append(",");
        } else {
            sb.append("\"agrisnapUid\":"); sb.append("null");sb.append(",");
        }
        sb.deleteCharAt(sb.length()-1);

        // encode transient fields
        toJsonExtend(alreadySearialized, crypto, sb);

        sb.append('}'); 
        return sb.toString();
    }

    /*
    */
    public String getAgrisnapName() {
        return agrisnapName;
    }

    public void setAgrisnapName(String agrisnapName) {
        this.agrisnapName = agrisnapName;
    }
    /*
    */
    public Integer getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(Integer rowVersion) {
        this.rowVersion = rowVersion;
    }
    /*
    */
    public Integer getAgrisnapUsersId() {
        return agrisnapUsersId;
    }

    public void setAgrisnapUsersId(Integer agrisnapUsersId) {
        this.agrisnapUsersId = agrisnapUsersId;
    }
    /*
    Contains a 5 chars human-readable User Id.
    */
    public String getAgrisnapUid() {
        return agrisnapUid;
    }

    public void setAgrisnapUid(String agrisnapUid) {
        this.agrisnapUid = agrisnapUid;
    }
    @XmlTransient
    public List<AgrisnapUsersProducers> getAgrisnapUsersProducersCollection() {
        return agrisnapUsersProducersCollection;
    }

    public void setAgrisnapUsersProducersCollection(List<AgrisnapUsersProducers> agrisnapUsersProducersCollection) {
        this.agrisnapUsersProducersCollection = agrisnapUsersProducersCollection;
    }

    public void removeAgrisnapUsersProducersCollection(AgrisnapUsersProducers child) {
        if (child != null)
            child.setAgrisnapUsersId(null);
    }

    public void addAgrisnapUsersProducersCollection(AgrisnapUsersProducers child) {
        if (child != null)
            child.setAgrisnapUsersId((AgrisnapUsers)this);
    }

    void internalRemoveAgrisnapUsersProducersCollection(AgrisnapUsersProducers child) {
        if (child != null)
            this.agrisnapUsersProducersCollection.remove(child);
    }

    void internalAddAgrisnapUsersProducersCollection(AgrisnapUsersProducers child) {
        if (child != null)
            this.agrisnapUsersProducersCollection.add(child);
    }

    @XmlTransient
    public List<GpRequests> getGpRequestsCollection() {
        return gpRequestsCollection;
    }

    public void setGpRequestsCollection(List<GpRequests> gpRequestsCollection) {
        this.gpRequestsCollection = gpRequestsCollection;
    }

    public void removeGpRequestsCollection(GpRequests child) {
        if (child != null)
            child.setAgrisnapUsersId(null);
    }

    public void addGpRequestsCollection(GpRequests child) {
        if (child != null)
            child.setAgrisnapUsersId((AgrisnapUsers)this);
    }

    void internalRemoveGpRequestsCollection(GpRequests child) {
        if (child != null)
            this.gpRequestsCollection.remove(child);
    }

    void internalAddGpRequestsCollection(GpRequests child) {
        if (child != null)
            this.gpRequestsCollection.add(child);
    }



    @Override
    public int hashCode() {
        return agrisnapUsersId.hashCode();
    }

    public String getRowKey() {
        return agrisnapUsersId.toString();
    }

    @Override
    public Object getPrimaryKeyValue() {
        return agrisnapUsersId;
    }

    @Override
    public String getPrimaryKeyValueEncrypted(gr.neuropublic.base.CryptoUtils crypto) {
        return crypto.EncryptNumber(agrisnapUsersId);
    }

    @Override
    public String toString() {
        return "gr.neuropublic.Niva.entitiesBase.AgrisnapUsers[ agrisnapUsersId=" + agrisnapUsersId + " ]";
    }

    @Override
    public String getEntityName() {
        return "AgrisnapUsers";
    }

    public EntityValidationError checkUniqueConstraints(Connection connection) throws SQLException {
           
        EntityValidationError ret = new EntityValidationError();
        EntityValidationSubError cur;

        return ret;
    }    
    @Override
    public void setInsertUser(String userName, String clientIp)
    {
    }

    @Override    
    public void setUpdateUser(String userName, String clientIp)
    {
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the agrisnapUsersId fields are not set
        if (!(object instanceof AgrisnapUsersBase)) {
            return false;
        }
        AgrisnapUsersBase other = (AgrisnapUsersBase) object;
        if (this.agrisnapUsersId == null || other.agrisnapUsersId == null) 
            return false;
        
        return this.agrisnapUsersId.equals(other.agrisnapUsersId);
    }
} 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
