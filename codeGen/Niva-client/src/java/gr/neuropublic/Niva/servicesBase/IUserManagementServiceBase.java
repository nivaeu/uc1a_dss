package gr.neuropublic.Niva.servicesBase;

import java.util.List;

import gr.neuropublic.rtlEntities.RTL_Subscriber;

public interface IUserManagementServiceBase extends gr.neuropublic.base.IUserManagementService {
    public List<RTL_Subscriber> findByCode_Login_subscriber(String shortName);
    public List<RTL_Subscriber> findAllByCriteriaRange_Login_subscriber(String srch_subs_short_name, String srch_subs_vat, String srch_subs_legal_name, int[] range, int[] recordCount, String sortField, boolean sortOrder, List<String> excludedEntities);    
    public int findAllByCriteriaRange_Login_subscriber_count(String srch_subs_short_name, String srch_subs_vat, String srch_subs_legal_name, List<String> excludedEntities);
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
