package gr.neuropublic.Niva.servicesBase;

public interface ISessionsCacheServiceBase extends gr.neuropublic.base.ISessionsCache {
    
}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
