package gr.neuropublic.Niva.servicesBase;
import gr.neuropublic.base.IAbstractService;
import gr.neuropublic.base.IFacade;
import gr.neuropublic.Niva.entities.*;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;
import java.util.Date;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import javax.persistence.Query;

// imports for geom
import com.vividsolutions.jts.geom.Geometry;

import gr.neuropublic.Niva.services.UserSession;
import gr.neuropublic.base.SaveResponse;
import java.util.ArrayList;

public interface IMainServiceBase  extends IAbstractService {

    public String getLogoProvider(UserSession usrSession);
    public gr.neuropublic.Niva.facades.IAgencyFacade.ILocal getAgencyFacade();

    public gr.neuropublic.Niva.facades.IAgrisnapUsersFacade.ILocal getAgrisnapUsersFacade();

    public gr.neuropublic.Niva.facades.IAgrisnapUsersProducersFacade.ILocal getAgrisnapUsersProducersFacade();

    public gr.neuropublic.Niva.facades.IClassificationFacade.ILocal getClassificationFacade();

    public gr.neuropublic.Niva.facades.IClassifierFacade.ILocal getClassifierFacade();

    public gr.neuropublic.Niva.facades.ICoverTypeFacade.ILocal getCoverTypeFacade();

    public gr.neuropublic.Niva.facades.ICultivationFacade.ILocal getCultivationFacade();

    public gr.neuropublic.Niva.facades.IDbmanagementlogFacade.ILocal getDbmanagementlogFacade();

    public gr.neuropublic.Niva.facades.IDecisionMakingFacade.ILocal getDecisionMakingFacade();

    public gr.neuropublic.Niva.facades.IDocumentFacade.ILocal getDocumentFacade();

    public gr.neuropublic.Niva.facades.IEcCultFacade.ILocal getEcCultFacade();

    public gr.neuropublic.Niva.facades.IEcCultCopyFacade.ILocal getEcCultCopyFacade();

    public gr.neuropublic.Niva.facades.IEcCultDetailFacade.ILocal getEcCultDetailFacade();

    public gr.neuropublic.Niva.facades.IEcCultDetailCopyFacade.ILocal getEcCultDetailCopyFacade();

    public gr.neuropublic.Niva.facades.IEcGroupFacade.ILocal getEcGroupFacade();

    public gr.neuropublic.Niva.facades.IEcGroupCopyFacade.ILocal getEcGroupCopyFacade();

    public gr.neuropublic.Niva.facades.IExcelErrorFacade.ILocal getExcelErrorFacade();

    public gr.neuropublic.Niva.facades.IExcelFileFacade.ILocal getExcelFileFacade();

    public gr.neuropublic.Niva.facades.IFileDirPathFacade.ILocal getFileDirPathFacade();

    public gr.neuropublic.Niva.facades.IFileTemplateFacade.ILocal getFileTemplateFacade();

    public gr.neuropublic.Niva.facades.IFmisDecisionFacade.ILocal getFmisDecisionFacade();

    public gr.neuropublic.Niva.facades.IFmisUploadFacade.ILocal getFmisUploadFacade();

    public gr.neuropublic.Niva.facades.IFmisUserFacade.ILocal getFmisUserFacade();

    public gr.neuropublic.Niva.facades.IGpDecisionFacade.ILocal getGpDecisionFacade();

    public gr.neuropublic.Niva.facades.IGpRequestsFacade.ILocal getGpRequestsFacade();

    public gr.neuropublic.Niva.facades.IGpRequestsContextsFacade.ILocal getGpRequestsContextsFacade();

    public gr.neuropublic.Niva.facades.IGpRequestsProducersFacade.ILocal getGpRequestsProducersFacade();

    public gr.neuropublic.Niva.facades.IGpUploadFacade.ILocal getGpUploadFacade();

    public gr.neuropublic.Niva.facades.IIntegrateddecisionFacade.ILocal getIntegrateddecisionFacade();

    public gr.neuropublic.Niva.facades.IParcelClasFacade.ILocal getParcelClasFacade();

    public gr.neuropublic.Niva.facades.IParcelDecisionFacade.ILocal getParcelDecisionFacade();

    public gr.neuropublic.Niva.facades.IParcelsIssuesFacade.ILocal getParcelsIssuesFacade();

    public gr.neuropublic.Niva.facades.IParcelsIssuesActivitiesFacade.ILocal getParcelsIssuesActivitiesFacade();

    public gr.neuropublic.Niva.facades.IParcelsIssuesStatusPerDemaFacade.ILocal getParcelsIssuesStatusPerDemaFacade();

    public gr.neuropublic.Niva.facades.IPredefColFacade.ILocal getPredefColFacade();

    public gr.neuropublic.Niva.facades.IProducersFacade.ILocal getProducersFacade();

    public gr.neuropublic.Niva.facades.IStatisticFacade.ILocal getStatisticFacade();

    public gr.neuropublic.Niva.facades.ISuperClasFacade.ILocal getSuperClasFacade();

    public gr.neuropublic.Niva.facades.ISuperClassDetailFacade.ILocal getSuperClassDetailFacade();

    public gr.neuropublic.Niva.facades.ISystemconfigurationFacade.ILocal getSystemconfigurationFacade();

    public gr.neuropublic.Niva.facades.ITemplateColumnFacade.ILocal getTemplateColumnFacade();

    public gr.neuropublic.Niva.facades.ITmpBlobFacade.ILocal getTmpBlobFacade();   

    public List<String> finalizationEcGroup(Integer ecgrId, UserSession usrSession);

    public List<String> runningDecisionMaking(Integer demaId, UserSession usrSession);

    public List<String> importingClassification(Integer classId, UserSession usrSession);

    public String getPhotoRequests(String agrisnapUid);

    public String uploadPhoto(String hash, String attachment, String data, String environment, Integer id);

    public String getBRERunsResultsList(Boolean info, UserSession usrSession);

    public String getBRERunResults(Boolean info, Integer id, UserSession usrSession);

    public String getFMISRequests(String fmisUid);

    public String uploadFMIS(String fmisUid, Integer id, String attachment, String metadata);

    public List<String> updateIntegratedDecisionsNIssues(Integer pclaId, UserSession usrSession);

    public List<String> updateIntegratedDecisionsNIssuesFromDema(Integer demaId, UserSession usrSession);

    public List<String> actionPushToCommonsAPI(Integer demaId, UserSession usrSession);
    
    
    public SaveResponse synchronizeChangesWithDbJS_Agency(String userName, String jsonData, UserSession usrSession);

    public SaveResponse synchronizeChangesWithDbJS_Classification(String userName, String jsonData, UserSession usrSession);

    public SaveResponse synchronizeChangesWithDbJS_Classifier(String userName, String jsonData, UserSession usrSession);

    public SaveResponse synchronizeChangesWithDbJS_CoverType(String userName, String jsonData, UserSession usrSession);

    public SaveResponse synchronizeChangesWithDbJS_Cultivation(String userName, String jsonData, UserSession usrSession);

    public SaveResponse synchronizeChangesWithDbJS_DecisionMaking(String userName, String jsonData, UserSession usrSession);

    public SaveResponse synchronizeChangesWithDbJS_DecisionMakingRO(String userName, String jsonData, UserSession usrSession);

    public SaveResponse synchronizeChangesWithDbJS_EcGroup(String userName, String jsonData, UserSession usrSession);

    public SaveResponse synchronizeChangesWithDbJS_FileTemplate(String userName, String jsonData, UserSession usrSession);

    public SaveResponse synchronizeChangesWithDbJS_Document(String userName, String jsonData, UserSession usrSession);

    public SaveResponse synchronizeChangesWithDbJS_FileDirPath(String userName, String jsonData, UserSession usrSession);

    public SaveResponse synchronizeChangesWithDbJS_Integrateddecision(String userName, String jsonData, UserSession usrSession);

    public SaveResponse synchronizeChangesWithDbJS_ParcelGP(String userName, String jsonData, UserSession usrSession);

    public SaveResponse synchronizeChangesWithDbJS_AgrisnapUsers(String userName, String jsonData, UserSession usrSession);

    public SaveResponse synchronizeChangesWithDbJS_ParcelsIssues(String userName, String jsonData, UserSession usrSession);

    public SaveResponse synchronizeChangesWithDbJS_Producers(String userName, String jsonData, UserSession usrSession);

    public SaveResponse synchronizeChangesWithDbJS_Dashboard(String userName, String jsonData, UserSession usrSession);

    public SaveResponse synchronizeChangesWithDbJS_ParcelFMIS(String userName, String jsonData, UserSession usrSession);

    public SaveResponse synchronizeChangesWithDbJS_FmisUser(String userName, String jsonData, UserSession usrSession);    
    
    public ExcelFile importExcel_CoverType_impCotys_id(byte[] excelData, String excelFilename, UserSession usrSession);

    public ExcelFile importExcel_Cultivation_impCults_id(byte[] excelData, String excelFilename, UserSession usrSession);

}
 
/**
*
* © 2021 OPEKEPE & NEUROPUBLIC S.A.
*
* This file belongs to subproject UC1a of project NIVA (www.niva4cap.eu)
* All rights reserved
*
* Project and code are made available under the EU-PL v 1.2 license
*
**/
