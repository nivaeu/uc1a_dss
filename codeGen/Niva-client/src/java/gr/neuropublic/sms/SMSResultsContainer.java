package gr.neuropublic.sms;

public class SMSResultsContainer {

    private String acknowledgement;
    private String id;
    private Float charge;

    public SMSResultsContainer(String acknowledgement, String id, Float charge) {
        this.acknowledgement = acknowledgement;
        this.id = id;
        this.charge = charge;
    }

    public String getAcknowledgement() {
        return acknowledgement;
    }

    public String getId() {
        return id;
    }

    public Float getCharge() {
        return charge;
    }

    @Override
    public String toString() {
        return " ACK :"+this.getAcknowledgement()+" id :"+this.getId()+" charge :"+this.getCharge();
    }
}
