package gr.neuropublic.sms;


public class SMSStubFactory {

    public static SMSInterface createSMSStub(String name, final String authKey){

        switch (name) {
            case "MSTAT": return new MStatSMSStub(authKey);
            default: throw new IllegalArgumentException(name+" is not supported.");
        }
    }
}
