package gr.neuropublic.sms;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public interface SMSInterface {

    
    public Map<String,SMSResultsContainer> sendSMS_custome_key(String message, String senderText, Map<String, String> idsMap, SMSInterface.ENCODING ENC);
    
    /**
     * Send SMS to receiverNumbers, id are created by this method.
     *
     * @param message The text message to be sent
     * @param senderText The sender displayed in receivers
     * @param receiverNumbers The phone numbers of receivers
     * @param ENC encoding of SMS message
     * @return Map of Phone Numbers as keys and SMSResultsContainer as values
     */
    Map<String,SMSResultsContainer> sendSMS(String message, String senderText, String[] receiverNumbers, ENCODING ENC);

    /**
     * Send SMS to predefined id,Number pairs defined in idsMap
     *
     * @param message The text message to be sent
     * @param senderText The sender displayed in receivers
     * @param idsMap LinkedHashMap<Id,Number>  Id is the key, Phone Number is the value, we use LinkedHashMap to ensure ordering
     * @param ENC  encoding of SMS message
     * @return  Map of Phone Numbers as keys and SMSResultsContainer as values
     */
    Map<String,SMSResultsContainer> sendSMS(String message, String senderText, LinkedHashMap<String, String> idsMap, ENCODING ENC);

    /**
     * Request a Delivery Receipt for specific ids
     *
     * @param receiverIds the ids
     * @return list of results
     */
    List<DLRResultsContainer> sendDLR(String[] receiverIds);

    /**
     * Encodings of SMS message
     */
    enum ENCODING {

        GSM(0),
        UNICODE(1);

        private Integer code;

        ENCODING(Integer code){
            this.code = code;
        }

        public Integer getCode(){
            return this.code;
        }
    }

}
